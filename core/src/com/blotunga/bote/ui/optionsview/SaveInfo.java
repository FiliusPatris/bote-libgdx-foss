/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.optionsview;

import java.util.Date;

import com.blotunga.bote.constants.Difficulties;

public class SaveInfo implements Comparable<SaveInfo> {
    static public boolean sortByName;
    public String fileName;
    public String majorID;
    public int width;
    public int height;
    public Date date;
    public String shape;
    public int currentTurn;
    public Difficulties difficulty;

    public SaveInfo() {
        sortByName = true;
    }

    @Override
    public int compareTo(SaveInfo o) {
        if (sortByName)
            return fileName.compareTo(o.fileName);
        else
            return date.compareTo(o.date);
    }
}
