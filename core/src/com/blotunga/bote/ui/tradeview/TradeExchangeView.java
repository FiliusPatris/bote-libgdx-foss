/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.tradeview;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.ResourceTypes;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.starsystem.StarSystem;

public class TradeExchangeView {
    private StarSystem starSystem;
    private Color normalColor;
    private Color markColor;
    private Major playerRace;
    private Table nameTable;
    private Table systemInfo;
    private TradeWidget[] widgetTable;
    private Table taxInfo;
    private Table repeatTable;
    private TextButton repeatButton;

    public TradeExchangeView(ScreenManager manager, Stage stage, Skin skin, float xOffset, float yOffset) {
        playerRace = manager.getRaceController().getPlayerRace();
        normalColor = playerRace.getRaceDesign().clrNormalText;
        markColor = playerRace.getRaceDesign().clrListMarkTextColor;

        nameTable = new Table();
        Rectangle rect = GameConstants.coordsToRelative(400, 800, 400, 40);
        nameTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        nameTable.align(Align.center);
        nameTable.setSkin(skin);
        nameTable.add(StringDB.getString("GLOBAL_TRADE_MENUE"), "hugeFont", normalColor);
        stage.addActor(nameTable);
        nameTable.setVisible(false);

        systemInfo = new Table();
        rect = GameConstants.coordsToRelative(400, 540, 400, 25);
        systemInfo.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        systemInfo.align(Align.center);
        systemInfo.setSkin(skin);
        stage.addActor(systemInfo);
        systemInfo.setVisible(false);

        widgetTable = new TradeWidget[ResourceTypes.IRIDIUM.getType() + 1];
        for (int i = ResourceTypes.TITAN.getType(); i < widgetTable.length; i++) {
            rect = GameConstants.coordsToRelative(75 + i * 225, 500, 140, 280);
            rect.x += xOffset;
            rect.y += yOffset;
            widgetTable[i] = new TradeWidget(rect, manager, stage, skin, ResourceTypes.fromResourceTypes(i));
        }

        taxInfo = new Table();
        rect = GameConstants.coordsToRelative(400, 207, 400, 20);
        taxInfo.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        taxInfo.align(Align.center);
        taxInfo.setSkin(skin);
        stage.addActor(taxInfo);
        taxInfo.setVisible(false);

        repeatTable = new Table();
        rect = GameConstants.coordsToRelative(400, 175, 400, 20);
        repeatTable.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        repeatTable.align(Align.center);
        repeatTable.setSkin(skin);
        repeatTable.add(StringDB.getString("REPEAT_ACTIVITY"), "mediumFont", normalColor);
        stage.addActor(repeatTable);
        repeatTable.setVisible(false);

        TextButtonStyle smallButtonStyle = new TextButtonStyle(skin.get("small-buttons", TextButtonStyle.class));
        repeatButton = new TextButton(String.format("%dx", playerRace.getTrade().getQuantity() / 100), smallButtonStyle);
        rect = GameConstants.coordsToRelative(537, 144, 120, 30);
        repeatButton.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        repeatButton.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                TextButton button = (TextButton) event.getListenerActor();
                if (playerRace.getTrade().getQuantity() < 10000)
                    playerRace.getTrade().setQuantity(playerRace.getTrade().getQuantity() * 10);
                else
                    playerRace.getTrade().setQuantity(100);
                button.setText(String.format("%dx", playerRace.getTrade().getQuantity() / 100));
            }
        });
        stage.addActor(repeatButton);
        repeatButton.setVisible(false);
    }

    public void show() {
        nameTable.setVisible(true);
        String text = String.format("%s: %s", StringDB.getString("TRADE_IN_SYSTEM"), starSystem.getName());
        systemInfo.clear();
        systemInfo.add(text, "largeFont", markColor);
        systemInfo.setVisible(true);
        for (int i = ResourceTypes.TITAN.getType(); i < widgetTable.length; i++)
            widgetTable[i].show();
        taxInfo.clear();
        String tax = String.format("%.0f", (playerRace.getTrade().getTax() - 1) * 100);
        text = StringDB.getString("ALL_PRICES_INCL_TAX", false, tax);
        taxInfo.add(text, "mediumFont", normalColor);
        taxInfo.setVisible(true);
        repeatTable.setVisible(true);
        repeatButton.setVisible(true);
    }

    public void hide() {
        nameTable.setVisible(false);
        systemInfo.setVisible(false);
        for (int i = ResourceTypes.TITAN.getType(); i < widgetTable.length; i++)
            widgetTable[i].hide();
        taxInfo.setVisible(false);
        repeatTable.setVisible(false);
        repeatButton.setVisible(false);
    }

    public void setStarSystem(StarSystem ss) {
        this.starSystem = ss;
        for (int i = 0; i < widgetTable.length; i++)
            widgetTable[i].setStarSystem(ss);
    }
}
