/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.diplomacyview;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.PlayerRaces;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.races.Minor;
import com.blotunga.bote.ships.ShipInfo;
import com.blotunga.bote.starsystem.BuildingInfo;
import com.blotunga.bote.utils.ui.BaseTooltip;

public class DiplomacyBottomView {
    private Skin skin;
    private Major playerRace;
    private Color normalColor;
    private Color markColor;
    private Table bottomTable;
    private ScreenManager manager;
    private Array<String> loadedTextures;
    private float headHeight;
    //tooltip
    protected Color tooltipHeaderColor;
    protected Color tooltipTextColor;
    protected String tooltipHeaderFont = "xlFont";
    protected String tooltipTextFont = "largeFont";
    protected Texture tooltipTexture;

    public DiplomacyBottomView(ScreenManager manager, Stage stage, float xOffset) {
        this.manager = manager;
        this.skin = manager.getSkin();
        playerRace = manager.getRaceController().getPlayerRace();
        normalColor = playerRace.getRaceDesign().clrNormalText;
        markColor = playerRace.getRaceDesign().clrListMarkTextColor;

        tooltipTexture = manager.getAssetManager().get(GameConstants.UI_BG_ROUNDED);
        tooltipHeaderColor = manager.getRaceController().getPlayerRace().getRaceDesign().clrListMarkTextColor;
        tooltipTextColor = manager.getRaceController().getPlayerRace().getRaceDesign().clrNormalText;

        bottomTable = new Table();
        bottomTable.align(Align.left);
        Rectangle rect = GameConstants.coordsToRelative(40, 170, 915, 150);
        bottomTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        stage.addActor(bottomTable);
        bottomTable.setVisible(false);

        loadedTextures = new Array<String>();
    }

    public void show() {
        show("", "", "");
    }

    public void show(String headline, String text) {
        show(headline, text, "");
    }

    public void show(String headline, String text, String raceID) {
        headHeight = GameConstants.hToRelative(30);
        bottomTable.clear();
        if (headline.isEmpty())
            headline = StringDB.getString("NO_DIPLOMATIC_NEWS");
        Label head = new Label(headline, skin, "xlFont", markColor);
        bottomTable.add(head).align(Align.topLeft).height((int) headHeight);
        bottomTable.row();
        Minor contactedRace = Minor.toMinor(manager.getRaceController().getRace(raceID));

        if (!text.isEmpty() || contactedRace == null || contactedRace.isAlien()) {
            Label txt = new Label(text, skin, "normalFont", normalColor);
            txt.setWrap(true);
            bottomTable.add(txt).width((int) bottomTable.getWidth()).height((int) (bottomTable.getHeight() - headHeight));
        } else if (contactedRace != null && !contactedRace.isAlien()) {
            drawRaceSpecial(contactedRace);
        }
        bottomTable.setVisible(true);
    }

    private void drawRaceSpecial(Minor contactedRace) {
        Table imageTable = new Table();
        imageTable.align(Align.left);
        bottomTable.add(imageTable).width(bottomTable.getWidth());

        String path;
        for (int i = 0; i < manager.getBuildingInfos().size; i++) {
            BuildingInfo bi = manager.getBuildingInfos().get(i);
            if (bi.getOwnerOfBuilding() == PlayerRaces.NOBODY.getType())
                if (bi.isOnlyMinorRace())
                    if (bi.getOnlyInSystemWithName().equals(contactedRace.getHomeSystemName())) {
                        path = "graphics/buildings/" + bi.getGraphicFileName() + ".png";
                        loadedTextures.add(path);
                        drawImage(imageTable, new TextureRegion(manager.loadTextureImmediate(path)), bi.getBuildingName(), bi, null);
                    }
        }
        for (int i = 0; i < manager.getShipInfos().size; i++) {
            ShipInfo si = manager.getShipInfos().get(i);
            if (si.getRace() == PlayerRaces.MINORNUMBER.getType())
                if (si.getOnlyInSystem().equals(contactedRace.getHomeSystemName())) {
                    path = "graphics/ships/" + si.getShipImageName() + ".png";
                    loadedTextures.add(path);
                    drawImage(imageTable, new TextureRegion(manager.loadTextureImmediate(path)),
                            si.getShipClass() + "-" + StringDB.getString("CLASS") + " (" + si.getShipTypeAsString() + ")", null,
                            si);
                }
        }
    }

    private void drawImage(Table table, TextureRegion region, String name, BuildingInfo bi, ShipInfo si) {
        float buttonWidth = GameConstants.wToRelative(160);
        float imageWidth = GameConstants.wToRelative(100);
        float imgHeight = GameConstants.hToRelative(75);
        Button.ButtonStyle bs = new Button.ButtonStyle();
        Button button = new Button(bs);
        button.align(Align.center);
        button.setSkin(skin);

        Image image = new Image(new TextureRegionDrawable(region));
        button.add(image).width(imageWidth).height(imgHeight);
        button.row();
        Label itemLabel = new Label(name, skin, "normalFont", normalColor);
        itemLabel.setAlignment(Align.center);
        itemLabel.setEllipsis(true);
        button.add(itemLabel).width(buttonWidth);
        button.setUserObject(itemLabel);
        if (bi != null)
            bi.getTooltip(BaseTooltip.createTableTooltip(button, tooltipTexture).getActor(), skin, tooltipHeaderFont,
                    tooltipHeaderColor, tooltipTextFont, tooltipTextColor);
        else if (si != null)
            si.getTooltip(null, BaseTooltip.createTableTooltip(button, tooltipTexture).getActor(), skin, tooltipHeaderFont,
                    tooltipHeaderColor, tooltipTextFont, tooltipTextColor);

        table.add(button).width(buttonWidth).height(bottomTable.getHeight() - headHeight);
    }

    private void unloadTextures() {
        for (String path : loadedTextures)
            if (manager.getAssetManager().isLoaded(path))
                manager.getAssetManager().unload(path);
        loadedTextures.clear();
    }

    public void hide() {
        bottomTable.setVisible(false);
        unloadTextures();
    }
}
