/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.diplomacyview;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.ArrayMap;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.DiplomacyInfoType;
import com.blotunga.bote.constants.DiplomaticAgreement;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.DiplomacyInfo;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.races.Minor;
import com.blotunga.bote.races.Race;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.ui.screens.DiplomacyScreen;
import com.blotunga.bote.utils.IntPoint;
import com.blotunga.bote.utils.Pair;

public class DiplomacyInfoView {
    private Skin skin;
    private Stage stage;
    private ScreenManager manager;
    private Major playerRace;
    private Color normalColor;
    private Color markColor;
    private Table nameTable;
    private ScrollPane descPane;
    private Table descTable;
    private Table infoTable;
    private TextButton cancelButton;

    public DiplomacyInfoView(final ScreenManager manager, Stage stage, float xOffset, float yOffset) {
        this.manager = manager;
        this.stage = stage;
        this.skin = manager.getSkin();
        playerRace = manager.getRaceController().getPlayerRace();
        normalColor = playerRace.getRaceDesign().clrNormalText;
        markColor = playerRace.getRaceDesign().clrListMarkTextColor;

        nameTable = new Table();
        Rectangle rect = GameConstants.coordsToRelative(200, 800, 800, 40);
        nameTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        nameTable.align(Align.center);
        nameTable.setSkin(skin);
        stage.addActor(nameTable);
        nameTable.add(StringDB.getString("DIPLOMACY_MENUE_INFO"), "hugeFont", normalColor);
        nameTable.setVisible(false);

        descTable = new Table();
        descTable.align(Align.topLeft);
        descTable.setSkin(skin);

        descPane = new ScrollPane(descTable, skin);
        descPane.setVariableSizeKnobs(false);
        descPane.setFadeScrollBars(false);
        rect = GameConstants.coordsToRelative(212, 230, 941, 155);
        descPane.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        descPane.setScrollingDisabled(true, false);
        stage.addActor(descPane);
        descPane.setVisible(false);

        infoTable = new Table();
        rect = GameConstants.coordsToRelative(240, 530, 305, 230);
        infoTable.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        infoTable.align(Align.top);
        infoTable.setSkin(skin);
        stage.addActor(infoTable);
        infoTable.setVisible(false);

        TextButtonStyle smallButtonStyle = new TextButtonStyle(skin.get("small-buttons", TextButtonStyle.class));
        cancelButton = new TextButton(StringDB.getString("BTN_CANCEL"), smallButtonStyle);
        rect = GameConstants.coordsToRelative(325, 300, 135, 25);
        cancelButton.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        stage.addActor(cancelButton);
        cancelButton.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                Race race = ((DiplomacyScreen) manager.getScreen()).getSelectedRace();
                DiplomacyInfo pending = (DiplomacyInfo) event.getListenerActor().getUserObject();
                if (playerRace.getPendingOffer(race.getRaceId()) == null) {
                    DiplomacyInfo di = new DiplomacyInfo(pending);
                    di.flag = DiplomacyInfoType.DIPLOMACY_OFFER.getType();
                    di.sendRound = manager.getCurrentRound();
                    di.type = DiplomaticAgreement.NONE;
                    di.fromRace = playerRace.getRaceId();
                    di.toRace = race.getRaceId();
                    playerRace.getOutgoingDiplomacyNews().add(di);
                } else {
                    if (pending.type == DiplomaticAgreement.NONE) {
                        for (int i = 0; i < playerRace.getOutgoingDiplomacyNews().size; i++) {
                            DiplomacyInfo di = playerRace.getOutgoingDiplomacyNews().get(i);
                            if (di.type == DiplomaticAgreement.NONE
                                    && di.flag == DiplomacyInfoType.DIPLOMACY_OFFER.getType()
                                    && di.toRace.equals(race.getRaceId()) && di.fromRace.equals(playerRace.getRaceId())) {
                                playerRace.getOutgoingDiplomacyNews().removeIndex(i);
                                break;
                            }
                        }
                    }
                }
                show();
            }
        });
        cancelButton.setVisible(false);
    }

    public void show() {
        cancelButton.setVisible(false);

        nameTable.setVisible(true);
        Race currentRace = ((DiplomacyScreen) manager.getScreen()).getSelectedRace();
        if (currentRace != null) {
            descTable.clear();
            descPane.setVisible(true);
            Label descLabel = new Label(currentRace.getDescription(), skin, "mediumFont", normalColor);
            descLabel.setWrap(true);
            descTable.add(descLabel).width(descPane.getWidth() - descPane.getStyle().vScrollKnob.getMinWidth());

            infoTable.clear();
            infoTable.setVisible(true);
            infoTable.row();
            addInfo(StringDB.getString("NAME").toUpperCase() + ":", currentRace.getName());
            String text = currentRace.getHomeSystemName();
            if (!text.isEmpty()) {
                String add = String.format(" (%s)", StringDB.getString("UNKNOWN"));
                if (!currentRace.getCoordinates().equals(new IntPoint())) {
                    StarSystem ss = manager.getUniverseMap().getStarSystemAt(currentRace.getCoordinates());
                    if (ss.getName().equals(text) && ss.getKnown(playerRace.getRaceId())) {
                        add = String.format(" (%s)",ss.getCoordinates().toString());
                    }
                }
                text += add;
            }
            addInfo(StringDB.getString("HOMESYSTEM").toUpperCase() + ":", text);
            Pair<String, Color> diplomacyStatus = ((DiplomacyScreen) manager.getScreen()).printDiplomacyStatus(
                    playerRace, currentRace);
            addInfo(StringDB.getString("RELATIONSHIP").toUpperCase() + ":", diplomacyStatus.getFirst(), markColor,
                    diplomacyStatus.getSecond());
            String s1 = "";
            text = "";
            Color color1 = new Color(markColor);
            Color color2 = new Color(normalColor);
            if (currentRace.isMinor()) {
                s1 = StringDB.getString("ACCEPTANCE").toUpperCase() + ":";
                text = String.format("%d%%",
                        (int) (((Minor) currentRace).getAcceptancePoints(playerRace.getRaceId()) / 50));
            } else if (currentRace.isMajor() && playerRace.getDefencePact(currentRace.getRaceId())) {
                if (playerRace.getDefencePactDuration(currentRace.getRaceId()) != 0)
                    s1 = String.format("%s (%d)", StringDB.getString("DEFENCE_PACT"),
                            playerRace.getDefencePactDuration(currentRace.getRaceId()));
                else
                    s1 = StringDB.getString("DEFENCE_PACT");
                color1 = new Color(226 / 255.0f, 44 / 255.0f, 250 / 255.0f, 1.0f);
                color2 = new Color(226 / 255.0f, 44 / 255.0f, 250 / 255.0f, 1.0f);
            }
            addInfo(s1, text, color1, color2);
            s1 = StringDB.getString("KNOWN_EMPIRES").toUpperCase() + ":";
            addInfo(s1, "");
            if (playerRace.getAgreement(currentRace.getRaceId()).getType() >= DiplomaticAgreement.FRIENDSHIP.getType()) {
                ArrayMap<String, Major> majors = manager.getRaceController().getMajors();
                for (int i = 0; i < majors.size; i++) {
                    Major major = majors.getValueAt(i);
                    String majorId = majors.getKeyAt(i);
                    if (!majorId.equals(currentRace.getRaceId()) && !majorId.equals(playerRace.getRaceId()))
                        if (currentRace.isRaceContacted(majorId)) {
                            s1 = major.getName();
                            Pair<String, Color> status = ((DiplomacyScreen) manager.getScreen()).printDiplomacyStatus(
                                    major, currentRace);
                            addInfo(s1, status.getFirst(), normalColor, status.getSecond());
                        }
                }
            } else {
                s1 = StringDB.getString("NO_SPECS");
                addInfo(s1, "", normalColor, normalColor);
            }
            if (currentRace.isMinor()) {
                DiplomacyInfo old = playerRace.getPendingOffer(currentRace.getRaceId());

                if (playerRace.getAgreement(currentRace.getRaceId()).getType() > DiplomaticAgreement.NONE.getType()) {
                    DiplomacyInfo di;
                    if (old != null)
                        di = new DiplomacyInfo(old);
                    else
                        di = new DiplomacyInfo();
                    di.headLine = StringDB.getString("CANCEL_AGREEMENT");
                    Pair<String, Color> pair = ((DiplomacyScreen) manager.getScreen()).printDiplomacyStatus(playerRace,
                            currentRace);
                    di.text = StringDB
                            .getString("CANCEL_AGREEMENT_TEXT", false, pair.getFirst(), currentRace.getName());
                    if (old != null)
                        cancelButton.setText(StringDB.getString("BTN_CANCEL"));
                    else
                        cancelButton.setText(StringDB.getString("BTN_ANNUL"));
                    cancelButton.setUserObject(di);
                    cancelButton.setVisible(true);
                }

                if (old != null && old.type == DiplomaticAgreement.NONE && old.toRace.equals(currentRace.getRaceId())
                        && old.fromRace.equals(playerRace.getRaceId())) {
                    ((DiplomacyScreen) manager.getScreen()).getBottomView().show(old.headLine, old.text);
                } else {
                    ((DiplomacyScreen) manager.getScreen()).getBottomView().show("", "", currentRace.getRaceId());
                }
            }
        }
    }

    private void addInfo(String s1, String s2) {
        addInfo(s1, s2, markColor, normalColor);
    }

    private void addInfo(String s1, String s2, Color color1, Color color2) {
        if (infoTable.getCells().size != 0)
            infoTable.row();
        Button.ButtonStyle bs = new Button.ButtonStyle();
        Button button = new Button(bs);
        button.setSkin(skin);
        BitmapFont font = skin.getFont("mediumFont");
        GlyphLayout layout = new GlyphLayout(font, s1);
        float textWidth = layout.width;
        button.add(s1, "mediumFont", color1).align(Align.left);
        layout.setText(font, s2);
        float sp = infoTable.getWidth() - textWidth - layout.width;
        button.add(s2, "mediumFont", color2).spaceLeft(sp).align(Align.right);
        if (s1.isEmpty() && s2.isEmpty() && infoTable.getCells().size != 0) {
            stage.draw(); //validate so that we can calulate height
            infoTable.add(button).height(infoTable.getCells().get(0).getActorHeight());
        } else
            infoTable.add(button);
    }

    public void hide() {
        nameTable.setVisible(false);
        descPane.setVisible(false);
        infoTable.setVisible(false);
        cancelButton.setVisible(false);
    }
}
