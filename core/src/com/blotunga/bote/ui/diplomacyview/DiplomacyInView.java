/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.diplomacyview;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane.ScrollPaneStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.AnswerStatus;
import com.blotunga.bote.constants.DiplomacyInfoType;
import com.blotunga.bote.constants.DiplomaticAgreement;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.ResourceTypes;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.DiplomacyInfo;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.ui.screens.DiplomacyScreen;
import com.blotunga.bote.utils.IntPoint;
import com.blotunga.bote.utils.ui.ScrollEvent;
import com.blotunga.bote.utils.ui.VerticalScroller;

public class DiplomacyInView implements ScrollEvent {
    private ScreenManager manager;
    private Stage stage;
    private Skin skin;
    private Major playerRace;
    private Table nameTable;
    private VerticalScroller infoListPane;
    private Table infoListTable;
    private Color normalColor;
    private Color markColor;
    private Color oldColor;
    private Button infoSelection = null;
    private TextureRegion selectTexture;
    private DiplomacyInfo clickedInfo;
    private Array<Button> infoItems;
    private Table systemSelect;
    private IntPoint resourceFromSystem;
    private boolean showAcceptButton;
    private boolean showDeclineButton;
    private TextButton acceptButton;
    private TextButton declineButton;

    public DiplomacyInView(final ScreenManager manager, Stage stage, float xOffset, float yOffset) {
        this.manager = manager;
        this.stage = stage;
        this.skin = manager.getSkin();
        playerRace = manager.getRaceController().getPlayerRace();
        normalColor = playerRace.getRaceDesign().clrNormalText;
        markColor = playerRace.getRaceDesign().clrListMarkTextColor;

        nameTable = new Table();
        Rectangle rect = GameConstants.coordsToRelative(200, 800, 800, 40);
        nameTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        nameTable.align(Align.center);
        nameTable.setSkin(skin);
        stage.addActor(nameTable);
        nameTable.add(StringDB.getString("DIPLOMACY_MENUE_RECEIPTS"), "hugeFont", normalColor);
        nameTable.setVisible(false);

        infoListTable = new Table();
        infoListTable.align(Align.topLeft);

        infoListPane = new VerticalScroller(infoListTable);
        infoListPane.setStyle(skin.get("default", ScrollPaneStyle.class));
        infoListPane.setVariableSizeKnobs(false);
        infoListPane.setFadeScrollBars(false);
        infoListPane.setScrollbarsOnTop(true);
        infoListPane.setEventHandler(this);
        rect = GameConstants.coordsToRelative(200, 535, 138, 450);
        infoListPane.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        infoListPane.setScrollingDisabled(true, false);
        stage.addActor(infoListPane);
        infoListPane.setVisible(false);

        resourceFromSystem = new IntPoint();
        TextButtonStyle smallButtonStyle = new TextButtonStyle(skin.get("small-buttons", TextButtonStyle.class));
        systemSelect = new Table();
        rect = GameConstants.coordsToRelative(567, 145, 134, 50);
        float height = GameConstants.hToRelative(25);
        systemSelect.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        systemSelect.setSkin(skin);
        systemSelect.align(Align.center);
        stage.addActor(systemSelect);
        systemSelect.add(StringDB.getString("FROM_SYSTEM"), "mediumFont", normalColor).height((int) height);
        systemSelect.row();
        TextButton systemButton = new TextButton("", smallButtonStyle);
        systemSelect.setUserObject(systemButton);
        systemButton.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                TextButton button = (TextButton) event.getListenerActor();
                int current = -1;
                Array<IntPoint> systemList = playerRace.getEmpire().getSystemList();
                for (int i = 0; i < systemList.size; i++)
                    if (systemList.get(i).equals(resourceFromSystem)) {
                        current = i;
                        break;
                    }
                if (current != -1)
                    current++;
                if (current == systemList.size)
                    current = 0;
                resourceFromSystem = systemList.get(current);
                button.setText(manager.getUniverseMap().getStarSystemAt(resourceFromSystem).getName());
                show();
            }
        });
        systemSelect.add(systemButton).height((int) height);
        systemSelect.setVisible(false);

        selectTexture = manager.getUiTexture("listselect");
        infoItems = new Array<Button>();
        showAcceptButton = false;
        showDeclineButton = false;

        TextButton.TextButtonStyle style = skin.get("default", TextButton.TextButtonStyle.class);
        acceptButton = new TextButton(StringDB.getString("BTN_ACCEPT"), style);
        rect = GameConstants.coordsToRelative(945, 220, 180, 35);
        acceptButton.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        stage.addActor(acceptButton);
        acceptButton.setVisible(false);
        acceptButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                TextButton button = (TextButton) event.getListenerActor();
                if (!button.isDisabled()) {
                    clickedInfo.answerStatus = AnswerStatus.ACCEPTED;
                    if (clickedInfo.type == DiplomaticAgreement.REQUEST) {
                        ((DiplomacyScreen) manager.getScreen()).takeorGetBackResLat(clickedInfo, true);
                    }
                    show();
                }
            }
        });

        declineButton = new TextButton(StringDB.getString("BTN_DECLINE"), style);
        rect = GameConstants.coordsToRelative(945, 125, 180, 35);
        declineButton
                .setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        stage.addActor(declineButton);
        declineButton.setVisible(false);
        declineButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                TextButton button = (TextButton) event.getListenerActor();
                if (!button.isDisabled()) {
                    if (clickedInfo.type == DiplomaticAgreement.REQUEST) {
                        if (clickedInfo.answerStatus == AnswerStatus.ACCEPTED)
                            ((DiplomacyScreen) manager.getScreen()).takeorGetBackResLat(clickedInfo, false);
                    }
                    clickedInfo.answerStatus = AnswerStatus.DECLINED;
                    show();
                }
            }
        });
    }

    public void show() {
        hide();

        nameTable.setVisible(true);
        infoListTable.clear();
        infoItems.clear();
        int pad = (int) GameConstants.wToRelative(7);
        Array<DiplomacyInfo> incomingInfos = playerRace.getIncomingDiplomacyNews();
        for (int i = 0; i < incomingInfos.size; i++) {
            DiplomacyInfo info = incomingInfos.get(i);
            Button.ButtonStyle bs = new Button.ButtonStyle();
            Button button = new Button(bs);
            button.align(Align.left);
            Label rlabel;
            String text = "";
            if (info.flag == DiplomacyInfoType.DIPLOMACY_OFFER.getType()) {
                if (info.type == DiplomaticAgreement.PRESENT)
                    text = StringDB.getString("PRESENT");
                else if (info.type == DiplomaticAgreement.REQUEST)
                    text = StringDB.getString("REQUEST");
                else if (info.type == DiplomaticAgreement.WAR)
                    text = StringDB.getString("WAR");
                else
                    text = StringDB.getString("SUGGESTION");
            } else if (info.flag == DiplomacyInfoType.DIPLOMACY_ANSWER.getType())
                text = StringDB.getString("ANSWER");
            rlabel = new Label(text, skin, "normalFont", Color.WHITE);
            rlabel.setColor(normalColor);
            rlabel.setUserObject(incomingInfos.get(i));
            button.setUserObject(rlabel);
            button.add().width(pad);
            button.add(rlabel).width((int) (infoListPane.getWidth() - pad * 2));
            button.add().width(pad);
            button.addListener(new ActorGestureListener() {
                @Override
                public void tap(InputEvent event, float x, float y, int count, int button) {
                    Button b = (Button) event.getListenerActor();
                    markDiploListSelected(b);
                    clickedInfo = getDiploInfo(b);
                    ((DiplomacyScreen) manager.getScreen()).getRaceList().setDiploInfo(clickedInfo);
                    manager.getScreen().show();
                }
            });
            infoListTable.add(button);
            infoListTable.row();
            infoItems.add(button);
        }
        infoListPane.setVisible(true);

        stage.draw();
        clickedInfo = ((DiplomacyScreen) manager.getScreen()).getRaceList().getSelectedDiploInfo();
        Button btn = null;

        if (clickedInfo != null) {
            for (Button b : infoItems) {
                DiplomacyInfo info = getDiploInfo(b);
                if (info.type == clickedInfo.type && info.flag == clickedInfo.flag
                        && info.fromRace.equals(clickedInfo.fromRace) && info.toRace.equals(clickedInfo.toRace)) {
                    btn = b;
                    break;
                }
            }
            ((DiplomacyScreen) manager.getScreen()).getBottomView().show(clickedInfo.headLine, clickedInfo.text);
        } else {
            ((DiplomacyScreen) manager.getScreen()).getBottomView().show();
        }

        if (btn != null)
            markDiploListSelected(btn);

        if (clickedInfo != null) {
            showAcceptButton = true;
            showDeclineButton = true;

            if (clickedInfo.flag == DiplomacyInfoType.DIPLOMACY_OFFER.getType()) {
                //if one was clicked then disable it
                if (clickedInfo.answerStatus == AnswerStatus.ACCEPTED)
                    showAcceptButton = false;
                else if (clickedInfo.answerStatus == AnswerStatus.DECLINED)
                    showDeclineButton = false;

                //by war or present we have nothing to choose from
                if (clickedInfo.type == DiplomaticAgreement.WAR || clickedInfo.type == DiplomaticAgreement.PRESENT) {
                    showAcceptButton = false;
                    showDeclineButton = false;
                }
            } else if (clickedInfo.flag == DiplomacyInfoType.DIPLOMACY_ANSWER.getType()) {
                showAcceptButton = false;
                showDeclineButton = false;
            }

            //check for resources
            if (clickedInfo.answerStatus != AnswerStatus.ACCEPTED && clickedInfo.type == DiplomaticAgreement.REQUEST) {
                if (clickedInfo.resources[ResourceTypes.TITAN.getType()] > 0
                        || clickedInfo.resources[ResourceTypes.DEUTERIUM.getType()] > 0
                        || clickedInfo.resources[ResourceTypes.DURANIUM.getType()] > 0
                        || clickedInfo.resources[ResourceTypes.CRYSTAL.getType()] > 0
                        || clickedInfo.resources[ResourceTypes.IRIDIUM.getType()] > 0
                        || clickedInfo.resources[ResourceTypes.DERITIUM.getType()] > 0) {

                    if (resourceFromSystem.equals(new IntPoint())
                            || !manager.getUniverseMap().getStarSystemAt(resourceFromSystem).getOwnerId()
                                    .equals(playerRace.getRaceId())) {
                        IntPoint coord = playerRace.getCoordinates();
                        if (!coord.equals(new IntPoint())
                                && manager.getUniverseMap().getStarSystemAt(coord).getOwnerId()
                                        .equals(playerRace.getRaceId()))
                            resourceFromSystem = coord;
                        else if (playerRace.getEmpire().getSystemList().size > 0)
                            resourceFromSystem = playerRace.getEmpire().getSystemList().get(0);
                    }
                    ((TextButton) systemSelect.getUserObject()).setText(manager.getUniverseMap()
                            .getStarSystemAt(resourceFromSystem).getName());
                    systemSelect.setVisible(true);
                    clickedInfo.coord = resourceFromSystem;

                    StarSystem resSystem = manager.getUniverseMap().getStarSystemAt(resourceFromSystem);
                    for (int r = ResourceTypes.TITAN.getType(); r <= ResourceTypes.DERITIUM.getType(); r++)
                        if (clickedInfo.resources[r] > 0 && resSystem.getResourceStore(r) < clickedInfo.resources[r])
                            showAcceptButton = false;
                }
                if (playerRace.getEmpire().getCredits() < clickedInfo.credits)
                    showAcceptButton = false;
            }

            acceptButton.setDisabled(!showAcceptButton);
            skin.setEnabled(acceptButton, showAcceptButton);
            declineButton.setDisabled(!showDeclineButton);
            skin.setEnabled(declineButton, showDeclineButton);
            acceptButton.setVisible(true);
            declineButton.setVisible(true);
        }
    }

    public void hide() {
        nameTable.setVisible(false);
        infoListPane.setVisible(false);
        acceptButton.setVisible(false);
        declineButton.setVisible(false);
        systemSelect.setVisible(false);
    }

    @Override
    public void scrollEventY(float pixelsY) {
        ((DiplomacyScreen) manager.getScreen()).getRaceList().setScrollY(pixelsY);
    }

    public void setScrollY(float pixelsY) {
        infoListPane.setScrollY(pixelsY);
    }

    private void markDiploListSelected(Button b) {
        if (infoSelection != null) {
            infoSelection.getStyle().up = null;
            infoSelection.getStyle().down = null;
            ((Label) infoSelection.getUserObject()).setColor(oldColor);
        }
        if (b == null)
            b = infoSelection;
        Image itemSelection = new Image(selectTexture);
        b.getStyle().up = itemSelection.getDrawable();
        b.getStyle().down = itemSelection.getDrawable();
        oldColor = new Color(((Label) b.getUserObject()).getColor());
        ((Label) b.getUserObject()).setColor(markColor);
        infoSelection = b;
    }

    private DiplomacyInfo getDiploInfo(Button b) {
        return ((DiplomacyInfo) ((Label) b.getUserObject()).getUserObject());
    }
}
