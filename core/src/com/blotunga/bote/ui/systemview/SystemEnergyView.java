/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.systemview;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectSet;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.starsystem.Building;
import com.blotunga.bote.starsystem.BuildingInfo;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.utils.ui.BaseTooltip;

public class SystemEnergyView {
    private Skin skin;
    final private ScreenManager manager;
    private Stage stage;
    private StarSystem starSystem;
    private Table nameTable;
    private Major race;
    private float xOffset;
    private Table usableEnergyTable;
    private Array<EnergyStruct> energyList;
    private int pageNr;
    private Array<Button> buildingsTable;
    private ObjectSet<String> loadedTextures;
    private Color normalColor;
    private TextButton plusButton;
    private TextButton minusButton;
    //tooltip
    private Color tooltipHeaderColor;
    private Color tooltipTextColor;
    private String tooltipHeaderFont = "xlFont";
    private String tooltipTextFont = "largeFont";
    private Texture tooltipTexture;

    private class EnergyStruct {
        public int index;
        public boolean status;
    }

    public SystemEnergyView(final ScreenManager manager, Stage stage, Skin skin, Rectangle position) {
        this.manager = manager;
        this.skin = skin;
        this.stage = stage;

        race = manager.getRaceController().getPlayerRace();
        normalColor = race.getRaceDesign().clrNormalText;
        tooltipHeaderColor = race.getRaceDesign().clrListMarkTextColor;
        tooltipTextColor = race.getRaceDesign().clrNormalText;
        tooltipTexture = manager.getAssetManager().get(GameConstants.UI_BG_ROUNDED);

        pageNr = 0;

        nameTable = new Table();
        xOffset = position.getWidth();
        Rectangle rect = GameConstants.coordsToRelative(285, 800, 602, 60);
        nameTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        nameTable.align(Align.center);
        nameTable.setSkin(skin);

        usableEnergyTable = new Table();
        rect = GameConstants.coordsToRelative(495, 735, 176, 20);
        usableEnergyTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        usableEnergyTable.align(Align.center);
        usableEnergyTable.setSkin(skin);

        energyList = new Array<EnergyStruct>();
        buildingsTable = new Array<Button>();

        rect = GameConstants.coordsToRelative(1130, 300, 60, 60);
        TextButtonStyle minusStyle = new TextButtonStyle(skin.get("default", TextButtonStyle.class));
        minusStyle.up = skin.getDrawable("buttonminus");
        minusStyle.down = skin.getDrawable("buttonminusa");
        minusStyle.over = skin.getDrawable("buttonminus");
        minusButton = new TextButton("", minusStyle);
        minusButton.setVisible(false);
        minusButton.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        minusButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                pageNr--;
                show();
            }
        });

        rect = GameConstants.coordsToRelative(1130, 600, 60, 60);
        TextButtonStyle plusStyle = new TextButtonStyle(skin.get("default", TextButtonStyle.class));
        plusStyle.up = skin.getDrawable("buttonplus");
        plusStyle.down = skin.getDrawable("buttonplusa");
        plusStyle.over = skin.getDrawable("buttonplus");
        plusButton = new TextButton("", plusStyle);
        plusButton.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        plusButton.setVisible(false);
        plusButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                pageNr++;
                show();
            }
        });

        stage.addActor(nameTable);
        stage.addActor(usableEnergyTable);
        stage.addActor(plusButton);
        stage.addActor(minusButton);

        loadedTextures = new ObjectSet<String>();
        hide();
    }

    public void show() {
        nameTable.clear();
        nameTable.add(StringDB.getString("ENERGY_MENUE") + " " + starSystem.getName(), "hugeFont",
                race.getRaceDesign().clrNormalText);
        nameTable.setVisible(true);

        drawEnergyBuildings();

        usableEnergyTable.clear();
        int energy = starSystem.getProduction().getEnergyProd();
        String text = StringDB.getString("USABLE_ENERGY") + ": " + energy;
        Color color;
        if (energy < 0)
            color = new Color(200 / 255.0f, 0, 0, 1.0f);
        else if (energy == 0)
            color = new Color(200 / 255.0f, 200 / 255.0f, 0, 1.0f);
        else
            color = new Color(0, 200 / 255.0f, 0, 1.0f);
        usableEnergyTable.add(text, "normalFont", color);
        usableEnergyTable.setVisible(true);
    }

    public void hide() {
        plusButton.setVisible(false);
        minusButton.setVisible(false);

        nameTable.setVisible(false);
        usableEnergyTable.setVisible(false);
        for (Button building : buildingsTable)
            building.setVisible(false);
        for (String path : loadedTextures)
            if (manager.getAssetManager().isLoaded(path))
                manager.getAssetManager().unload(path);
        loadedTextures.clear();
    }

    private void drawEnergyBuildings() {
        for (int i = 0; i < buildingsTable.size; i++) {
            buildingsTable.get(i).remove();
        }
        buildingsTable.clear();
        energyList.clear();
        int numberOfBuildings = starSystem.getAllBuildings().size;
        for (int i = 0; i < numberOfBuildings; i++) {
            BuildingInfo buildingInfo = manager.getBuildingInfos().get(
                    starSystem.getAllBuildings().get(i).getRunningNumber() - 1);
            if (buildingInfo.getNeededEnergy() > 0) {
                EnergyStruct es = new EnergyStruct();
                es.index = i;
                es.status = starSystem.getAllBuildings().get(i).getIsBuildingOnline();
                energyList.add(es);
            }
        }

        plusButton.setVisible(false);
        minusButton.setVisible(false);
        if (pageNr * GameConstants.NOBIEL > energyList.size)
            pageNr = 0;
        if (energyList.size > pageNr * GameConstants.NOBIEL + GameConstants.NOBIEL) {
            plusButton.setVisible(true);
        }
        if (pageNr > 0) {
            minusButton.setVisible(true);
        }

        int spaceX = 0;
        int spaceY = 0;
        for (int i = pageNr * GameConstants.NOBIEL; i < energyList.size; i++) {
            //check that we are on the correct page
            if (i < pageNr * GameConstants.NOBIEL + GameConstants.NOBIEL) {
                if (i % 3 == 0 && i != pageNr * GameConstants.NOBIEL) {
                    spaceX++;
                    spaceY = 0;
                }
                Rectangle rect = GameConstants.coordsToRelative(90 + (spaceX) * 355, 705 - (spaceY) * 184, 275, 165);
                drawBuilding(rect, i);
                spaceY++;
            }
        }
    }

    private void drawBuilding(Rectangle rect, int index) {
        Button.ButtonStyle bs = new Button.ButtonStyle();
        Texture texture = manager.getAssetManager().get(GameConstants.UI_BG_ROUNDED);
        TextureRegion dr = new TextureRegion(texture);
        Sprite sprite = new Sprite(dr);
        sprite.setColor(Color.BLACK);
        Drawable tr = new SpriteDrawable(sprite);
        bs.up = tr;
        bs.down = tr;
        bs.over = tr;

        Button button = new Button(bs);
        button.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        int id = starSystem.getAllBuildings().get(energyList.get(index).index).getRunningNumber();
        BuildingInfo bi = manager.getBuildingInfos().get(id - 1);
        String path = "graphics/buildings/" + bi.getGraphicFileName() + ".png";
        Texture tex = manager.loadTextureImmediate(path);
        loadedTextures.add(path);

        Label buildingName = new Label(bi.getBuildingName(), skin, "normalFont", normalColor);
        button.add(buildingName);
        button.row();
        Table imageAndText = new Table();
        Image buildingImage = new Image(tex);
        imageAndText.add(buildingImage);

        Table text = new Table();
        Label buildingEnergyUse = new Label(bi.getNeededEnergy() + " EP", skin, "normalFont", normalColor);
        text.add(buildingEnergyUse);
        text.row();
        if (energyList.get(index).status) {
            Label online = new Label("online", skin, "normalFont", Color.GREEN);
            text.add(online);
        } else {
            Label offline = new Label("offline", skin, "normalFont", Color.RED);
            text.add(offline);
        }
        imageAndText.add(text);
        button.add(imageAndText);
        button.setUserObject(index);
        for (Actor a : button.getChildren())
            a.setTouchable(Touchable.disabled);

        button.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                int idx = (Integer) event.getListenerActor().getUserObject();
                Building building = starSystem.getAllBuildings().get(energyList.get(idx).index);
                BuildingInfo info = manager.getBuildingInfos().get(building.getRunningNumber() - 1);
                boolean status = !energyList.get(idx).status && starSystem.canTakeOnline(info);
                starSystem.setIsBuildingOnline(energyList.get(idx).index, status);

                race.reflectPossibleResearchOrSecurityWorkerChange(starSystem.getCoordinates(), true, false);
                if (info.isShipYard())
                    starSystem.calculateBuildableShips();
                show();
            }
        });
        bi.getTooltip(BaseTooltip.createTableTooltip(button, tooltipTexture).getActor(), skin, tooltipHeaderFont, tooltipHeaderColor, tooltipTextFont, tooltipTextColor);

        stage.addActor(button);
        buildingsTable.add(button);
    }

    public void setStarSystem(StarSystem system) {
        this.starSystem = system;
    }
}
