/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.systemview;

import com.badlogic.gdx.utils.IntMap;

public enum SystemViewButtonType {
    BUILD_LIST_BUTTON("BTN_BUILDMENUE", "buildmenu", 0),
    PRODUCTION_BUTTON("BTN_WORKERSMENUE", "workmenu", 1),
    ENERGY_BUTTON("BTN_ENERGYMENUE", "energymenu", 2),
    BUILDING_OVERVIEW_BUTTON("BTN_BUILDING_OVERVIEWMENUE", "overviewmenu", 3),
    TRADE_BUTTON("BTN_TRADEMENUE", "systrademenu", 4),
    SYSTEM_MANAGER_BUTTON("BTN_SYSTEMMANAGER", "top5menu", 5);

    private String label;
    private String bgImage;
    private int ord;
    private static final IntMap<SystemViewButtonType> intToTypeMap = new IntMap<SystemViewButtonType>();

    SystemViewButtonType(String label, String bgImage, int ord) {
        this.label = label;
        this.bgImage = bgImage;
        this.ord = ord;
    }

    static {
        for (SystemViewButtonType st : SystemViewButtonType.values()) {
            intToTypeMap.put(st.ord, st);
        }
    }

    public String getLabel() {
        return label;
    }

    public String getBgImage() {
        return bgImage;
    }

    public int getId() {
        return ord;
    }

    public static SystemViewButtonType fromInt(int i) {
        SystemViewButtonType st = intToTypeMap.get(i);
        return st;
    }
}