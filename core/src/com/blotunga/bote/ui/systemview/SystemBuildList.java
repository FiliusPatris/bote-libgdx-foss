/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.systemview;

import java.util.Arrays;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.InputEvent.Type;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.ObjectSet;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.ResearchComplexType;
import com.blotunga.bote.constants.ResourceTypes;
import com.blotunga.bote.constants.SndMgrValue;
import com.blotunga.bote.constants.ViewTypes;
import com.blotunga.bote.constants.WorkerType;
import com.blotunga.bote.galaxy.ResourceRoute;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.races.ResearchInfo;
import com.blotunga.bote.ships.Ship;
import com.blotunga.bote.ships.ShipInfo;
import com.blotunga.bote.starsystem.BuildingInfo;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.starsystem.SystemProd;
import com.blotunga.bote.troops.TroopInfo;
import com.blotunga.bote.ui.screens.ZoomableScreen;
import com.blotunga.bote.utils.IntPoint;
import com.blotunga.bote.utils.ui.BaseTooltip;

public class SystemBuildList {
    private StarSystem starSystem;
    private Table buildScrollTable;
    private ScrollPane buildScroller;
    private Image buildScrollerbg;
    private Table assemblyListGroup;
    private Table productionTable;
    private ScrollPane productionScroller;
    private Table starSystemNameTable;
    private Table populationTable;
    private Table resourcesTable;
    private Table workerTable;
    private TextureRegion selectTexture;
    private Skin skin;
    private IntArray buildList; //holds a pair of integers (index and building id)
    final private ScreenManager manager;
    private TextureAtlas uiAtlas;
    private Button buildSelection = null;
    private int selectedProduction;
    private float xOffset;
    private Color normalColor;
    private Color secondColor;
    private Color markColor;
    private Color markPenColor;
    private Color oldColor;
    private Label productionLabel;
    private Label storageLabel;
    private Label orderLabel;
    private Label turnLabel;
    private Image infoImage;
    private Table infoTable;
    private ScrollPane infoScroller;
    private TextButton structuresButton;
    private TextButton shipyardButton;
    private TextButton barrackButton;
    private TextButton buyButton;
    private TextButton deleteButton;
    private TextButton infoButton;
    private TextButton descButton;
    private TextButton okButton;
    private TextButton cancelButton;
    private Label buyDelete;
    private boolean clickedOnBuyButton;
    private boolean clickedOnDeleteButton;
    private boolean infoOrDescription; //true if info, false if description
    private Major playerRace;
    private Stage stage;
    private ObjectSet<String> loadedTextures;

    //tooltip
    private Color tooltipHeaderColor;
    private Color tooltipTextColor;
    private String tooltipHeaderFont = "xlFont";
    private String tooltipTextFont = "largeFont";
    private Texture tooltipTexture;
    private boolean visible;

    public SystemBuildList(final ScreenManager manager, Stage stage, Skin skin, Rectangle position) {
        this.manager = manager;
        this.skin = skin;
        this.stage = stage;
        buildList = new IntArray();
        uiAtlas = manager.getAssetManager().get("graphics/ui/general_ui.pack");
        playerRace = manager.getRaceController().getPlayerRace();
        buildScrollTable = new Table();
        buildScrollTable.align(Align.top);
        buildScroller = new ScrollPane(buildScrollTable, skin);
        buildScroller.setVariableSizeKnobs(false);
        buildScroller.setFadeScrollBars(false);
        buildScroller.setScrollbarsOnTop(true);
        buildScroller.setTouchable(Touchable.childrenOnly);
        buildScrollerbg = new Image(new TextureRegion((Texture) manager.getAssetManager().get(GameConstants.UI_BG_SIMPLE)));
        buildScrollerbg.setColor(new Color(0, 0, 0, 0.5f));
        buildScrollerbg.setVisible(false);

        productionTable = new Table();
        productionTable.align(Align.left);
        productionScroller = new ScrollPane(productionTable, skin);
        productionScroller.setVariableSizeKnobs(false);
        productionScroller.setFadeScrollBars(false);
        productionScroller.setScrollbarsOnTop(true);
        productionScroller.setTouchable(Touchable.childrenOnly);
        starSystemNameTable = new Table();
        xOffset = position.getWidth();
        Rectangle rect = GameConstants.coordsToRelative(366, 810, 425, 60);
        starSystemNameTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        starSystemNameTable.align(Align.center);
        starSystemNameTable.setSkin(skin);

        populationTable = new Table();
        rect = GameConstants.coordsToRelative(366, 732, 425, 25);
        populationTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        populationTable.setVisible(false);

        selectTexture = manager.getUiTexture("listselect");
        normalColor = playerRace.getRaceDesign().clrNormalText;
        secondColor = playerRace.getRaceDesign().clrSecondText;
        markPenColor = playerRace.getRaceDesign().clrListMarkPenColor;
        markColor = playerRace.getRaceDesign().clrListMarkTextColor;
        tooltipHeaderColor = playerRace.getRaceDesign().clrListMarkTextColor;
        tooltipTextColor = playerRace.getRaceDesign().clrNormalText;

        buildScroller.setScrollingDisabled(true, false);

        selectedProduction = 0;

        String text = "";
        text = StringDB.getString("PRODUCTION");
        productionLabel = new Label(text, skin, "normalFont", markPenColor);
        productionLabel.setPosition((int) (GameConstants.wToRelative(960) + xOffset),
                (int) GameConstants.hToRelative(765));
        productionLabel.setAlignment(Align.center);
        text = StringDB.getString("STORAGE");
        storageLabel = new Label(text, skin, "normalFont", markPenColor);
        storageLabel.setPosition((int) (GameConstants.wToRelative(1076) + xOffset), (int) productionLabel.getY());
        storageLabel.setAlignment(Align.center);
        text = StringDB.getString("JOB");
        orderLabel = new Label(text, skin, "normalFont", markColor);
        orderLabel.setPosition((int) (GameConstants.wToRelative(478) + xOffset), (int) GameConstants.hToRelative(670));
        orderLabel.setAlignment(Align.center);
        text = StringDB.getString("ROUND");
        turnLabel = new Label(text, skin, "normalFont", markColor);
        turnLabel.setPosition((int) (GameConstants.wToRelative(750) + xOffset), (int) orderLabel.getY());
        turnLabel.setAlignment(Align.center);

        TextButtonStyle style = new TextButtonStyle(skin.get("small-buttons", TextButtonStyle.class));

        structuresButton = new TextButton(StringDB.getString("BTN_BAUHOF"), style);
        rect = GameConstants.coordsToRelative(363, 260, 133, 30);
        float buttonpadding = GameConstants.wToRelative(15);
        structuresButton.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        shipyardButton = new TextButton(StringDB.getString("BTN_DOCKYARD"), style);
        shipyardButton.setBounds((int) (structuresButton.getX() + structuresButton.getWidth() + buttonpadding),
                (int) rect.y, (int) rect.width, (int) rect.height);
        barrackButton = new TextButton(StringDB.getString("BTN_BARRACK"), style);
        barrackButton.setBounds((int) (shipyardButton.getX() + shipyardButton.getWidth() + buttonpadding),
                (int) rect.y, (int) rect.width, (int) rect.height);
        ClickListener structuresListener = new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                TextButton tb = (TextButton) event.getListenerActor();
                if (!tb.isDisabled()) {
                    if (tb.equals(structuresButton))
                        selectedProduction = 0;
                    else if (tb.equals(shipyardButton))
                        selectedProduction = 1;
                    else
                        selectedProduction = 2;
                    drawBuildingList();
                    drawResourceTable();
                    drawSelectionInfo();
                    drawProduction();
                }
                return false;
            }
        };
        structuresButton.addListener(structuresListener);
        shipyardButton.addListener(structuresListener);
        barrackButton.addListener(structuresListener);

        assemblyListGroup = new Table();
        assemblyListGroup.setTouchable(Touchable.childrenOnly);
        assemblyListGroup.align(Align.top);
        buyButton = new TextButton(StringDB.getString("BTN_BUY"), style);
        rect = GameConstants.coordsToRelative(855, 130, 133, 30);
        buyButton.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        buyButton.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                int entry = starSystem.getAssemblyList().getAssemblyListEntry(0).id;
                if (!starSystem.getAssemblyList().getWasBuildingBought() && entry != 0 && !clickedOnBuyButton) {
                    if ((entry < 0) || (entry > 0 && entry < 10000 && !manager.getBuildingInfo(entry).getNeverReady())
                            || (entry >= 10000 && entry < 20000 && starSystem.getProduction().isShipyard())
                            || (entry >= 20000 && starSystem.getProduction().isBarrack())) {
                        ///SPECIAL RESEARCH///
                        int bonus = playerRace.getEmpire().getResearch().getResearchInfo()
                                .isResearchedThenGetBonus(ResearchComplexType.FINANCES, 1);
                        starSystem.getAssemblyList().calculateBuildCosts(
                                playerRace.getTrade().getResourcePriceAtRoundStart(), bonus);
                        drawInquiry(true);
                        clickedOnBuyButton = true;
                    }
                }
                return false;
            }
        });
        deleteButton = new TextButton(StringDB.getString("BTN_CANCEL"), style);
        rect = GameConstants.coordsToRelative(1002, 130, 133, 30);
        deleteButton.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        deleteButton.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                boolean enquire = false;
                int assemblyListEntry = starSystem.getAssemblyList().getAssemblyListEntry(0).id;
                int entryCount = starSystem.getAssemblyList().getAssemblyListEntry(0).count;
                if (assemblyListEntry != 0 && entryCount == 1) {
                    int roundsMax = starSystem.neededRoundsToBuild(assemblyListEntry, false, false);
                    int roundsRemain = starSystem.neededRoundsToBuild(0, true);
                    enquire = (roundsMax != roundsRemain) || starSystem.getAssemblyList().getWasBuildingBought();
                }
                if (enquire) {
                    drawInquiry(false);
                    clickedOnDeleteButton = true;
                } else
                    deleteAssemblyListEntry(0);
                return false;
            }
        });

        resourcesTable = new Table();
        workerTable = new Table();
        infoImage = new Image();
        tooltipTexture = manager.getAssetManager().get(GameConstants.UI_BG_ROUNDED);
        infoImage.setUserObject(BaseTooltip.createTableTooltip(infoImage, tooltipTexture).getActor());
        infoTable = new Table();
        infoTable.align(Align.top);
        infoScroller = new ScrollPane(infoTable, skin);
        infoScroller.setVariableSizeKnobs(false);
        infoScroller.setFadeScrollBars(false);
        infoScroller.setScrollbarsOnTop(true);
        infoScroller.setScrollingDisabled(true, false);
        infoScroller.setTouchable(Touchable.childrenOnly);
        Rectangle tableRect = GameConstants.coordsToRelative(40, 370, 280, 230);
        infoScroller.setBounds((int) (tableRect.x + xOffset), (int) tableRect.y, (int) tableRect.width,
                (int) tableRect.height);
        infoOrDescription = true; //usually show info
        infoButton = new TextButton(StringDB.getString("BTN_INFORMATION"), style);
        infoButton.setName("info");
        rect = GameConstants.coordsToRelative(35, 130, 133, 30);
        infoButton.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        descButton = new TextButton(StringDB.getString("BTN_DESCRIPTION"), style);
        descButton.setName("desc");
        rect = GameConstants.coordsToRelative(185, 130, 133, 30);
        descButton.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        ClickListener infoListener = new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                TextButton tb = (TextButton) event.getListenerActor();
                infoOrDescription = tb.getName().equals("info");
                drawSelectionInfo();
                return false;
            }
        };
        infoButton.addListener(infoListener);
        descButton.addListener(infoListener);

        rect = GameConstants.coordsToRelative(430, 175, 290, 40);
        buyDelete = new Label(StringDB.getString("CANCEL_PROJECT"), skin, "normalFont", normalColor);
        buyDelete.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        buyDelete.setAlignment(Align.center);
        buyDelete.setWrap(true);

        okButton = new TextButton(StringDB.getString("BTN_OKAY"), style);
        okButton.setName("ok");
        rect = GameConstants.coordsToRelative(400, 120, 133, 30);
        okButton.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);

        cancelButton = new TextButton(StringDB.getString("BTN_CANCEL"), style);
        cancelButton.setName("cancel");
        rect = GameConstants.coordsToRelative(620, 120, 133, 30);
        cancelButton.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);

        ClickListener okCancelListener = new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (event.getListenerActor().getName().equals("ok")) {
                    if (clickedOnBuyButton) {
                        int costs = starSystem.getAssemblyList().buyBuilding(playerRace.getEmpire().getCredits());
                        if (costs != 0) {
                            starSystem.getAssemblyList().setWasBuildingBought(true);
                            playerRace.getEmpire().setCredits(-costs);
                            //Adjust prices on the market, notAtMarket has to be TRUE!!!
                            for (int res = ResourceTypes.TITAN.getType(); res < ResourceTypes.DERITIUM.getType(); res++)
                                playerRace.getTrade().buyResource(res,
                                        starSystem.getAssemblyList().getNeededResourceInAssemblyList(0, res),
                                        starSystem.getCoordinates(), playerRace.getEmpire().getCredits(), true);
                            drawAssemblyList();
                            drawProduction();
                            buyButton.setVisible(false);
                        }
                        clickedOnBuyButton = false;
                    } else if (clickedOnDeleteButton) {
                        deleteAssemblyListEntry(0);
                    }
                    //update info on left with credits etc
                    ZoomableScreen screen = (ZoomableScreen) manager.getScreen();
                    screen.getLeftSideBar().showInfo();
                }
                buyDelete.setVisible(false);
                clickedOnDeleteButton = false;
                clickedOnBuyButton = false;
                cancelButton.setVisible(false);
                okButton.setVisible(false);
                resourcesTable.setVisible(true);
                return false;
            }
        };
        okButton.addListener(okCancelListener);
        cancelButton.addListener(okCancelListener);

        stage.addActor(buildScrollerbg);
        stage.addActor(buildScroller);
        stage.addActor(productionScroller);
        stage.addActor(starSystemNameTable);
        stage.addActor(populationTable);
        stage.addActor(productionLabel);
        stage.addActor(storageLabel);
        stage.addActor(orderLabel);
        stage.addActor(turnLabel);
        stage.addActor(structuresButton);
        stage.addActor(shipyardButton);
        stage.addActor(barrackButton);
        stage.addActor(assemblyListGroup);
        stage.addActor(buyButton);
        stage.addActor(deleteButton);
        stage.addActor(resourcesTable);
        stage.addActor(workerTable);
        stage.addActor(infoImage);
        stage.addActor(infoScroller);
        stage.addActor(infoButton);
        stage.addActor(descButton);
        stage.addActor(buyDelete);
        stage.addActor(okButton);
        stage.addActor(cancelButton);

        clickedOnDeleteButton = false;
        clickedOnBuyButton = false;
        loadedTextures = new ObjectSet<String>();
        visible = false;
    }

    /**
     * Function draws the buildable building/ships/troops
     */
    private void drawBuildingList() {
        starSystemNameTable.clear();
        starSystemNameTable.add(starSystem.getName(), "hugeFont", secondColor);
        populationTable.clear();
        float lheight = GameConstants.wToRelative(25);
        populationTable.add(new Image(uiAtlas.findRegion("populationSmall"))).height(lheight).width(lheight*1.25f);
        String text = StringDB.getString("CURRENT_HABITANTS") + String.format("%.3f", starSystem.getCurrentInhabitants()) + StringDB.getString("MRD") + " /";
        populationTable.add(new Label(text, skin, "normalFont", normalColor));
        populationTable.add(new Image(uiAtlas.findRegion("popmaxSmall"))).height(lheight).width(lheight*1.25f);
        text = StringDB.getString("MAX_HABITANTS") + String.format("%.3f", starSystem.getMaxInhabitants()) + StringDB.getString("MRD");
        populationTable.add(new Label(text, skin, "normalFont", normalColor));
        BitmapFont font = skin.getFont("normalFont");
        Rectangle rect = GameConstants.coordsToRelative(360, 640, 440, 360);
        rect.x += xOffset;
        buildScrollerbg.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        buildScroller.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        float hpadding = GameConstants.wToRelative(30);
        float vpadding = GameConstants.hToRelative(10);
        if (starSystem.getBuildableShips().size > 0)
            shipyardButton.setDisabled(false);
        else
            shipyardButton.setDisabled(true);
        if (starSystem.getBuildableTroops().size > 0)
            barrackButton.setDisabled(false);
        else
            barrackButton.setDisabled(true);

        buildList.clear();
        buildScrollTable.clear();

        if (selectedProduction == 0) {
            for (int i = 0; i < starSystem.getBuildableUpdates().size; i++) {
                int a = starSystem.getBuildableUpdates().get(i) * (-1);
                buildList.add(a);
            }
            for (int i = 0; i < starSystem.getBuildableBuildings().size; i++)
                buildList.add(starSystem.getBuildableBuildings().get(i));
        } else if (selectedProduction == 1) {
            for (int i = 0; i < starSystem.getBuildableShips().size; i++)
                buildList.add(starSystem.getBuildableShips().get(i));
        } else {
            for (int i = 0; i < starSystem.getBuildableTroops().size; i++)
                buildList.add(starSystem.getBuildableTroops().get(i) + 20000);
        }
        Button sel = null;

        for (int i = 0; i < buildList.size; i++) {
            Button.ButtonStyle bs = new Button.ButtonStyle();
            Button button = new Button(bs);
            button.align(Align.center);
            button.setSkin(skin);
            Image symbol = new Image();
            symbol.setTouchable(Touchable.disabled);
            text = "";
            if (selectedProduction == 0) {
                BuildingInfo bi = manager.getBuildingInfo(Math.abs(buildList.get(i)));
                if (!bi.getNeverReady())
                    if (bi.getFoodProd() > 0 || bi.getFoodBonus() > 0)
                        symbol.setDrawable(new TextureRegionDrawable(uiAtlas.findRegion("foodSmall")));
                    else if (bi.getIndustryPointsProd() > 0 || bi.getIndustryBonus() > 0)
                        symbol.setDrawable(new TextureRegionDrawable(uiAtlas.findRegion("industrySmall")));
                    else if (bi.getEnergyProd() > 0 || bi.getEnergyBonus() > 0)
                        symbol.setDrawable(new TextureRegionDrawable(uiAtlas.findRegion("energySmall")));
                    else if (bi.getSecurityPointsProd() > 0 || bi.getSecurityBonus() > 0)
                        symbol.setDrawable(new TextureRegionDrawable(uiAtlas.findRegion("securitySmall")));
                    else if (bi.getResearchPointsProd() > 0 || bi.getResearchBonus() > 0)
                        symbol.setDrawable(new TextureRegionDrawable(uiAtlas.findRegion("researchSmall")));
                    else if (bi.getTitanProd() > 0 || bi.getTitanBonus() > 0)
                        symbol.setDrawable(new TextureRegionDrawable(uiAtlas.findRegion("titanSmall")));
                    else if (bi.getDeuteriumProd() > 0 || bi.getDeuteriumBonus() > 0)
                        symbol.setDrawable(new TextureRegionDrawable(uiAtlas.findRegion("deuteriumSmall")));
                    else if (bi.getDuraniumProd() > 0 || bi.getDuraniumBonus() > 0)
                        symbol.setDrawable(new TextureRegionDrawable(uiAtlas.findRegion("duraniumSmall")));
                    else if (bi.getCrystalProd() > 0 || bi.getCrystalBonus() > 0)
                        symbol.setDrawable(new TextureRegionDrawable(uiAtlas.findRegion("crystalSmall")));
                    else if (bi.getIridiumProd() > 0 || bi.getIridiumBonus() > 0)
                        symbol.setDrawable(new TextureRegionDrawable(uiAtlas.findRegion("iridiumSmall")));
                    else if (bi.getDeritiumProd() > 0 || bi.getDeritiumBonus() > 0)
                        symbol.setDrawable(new TextureRegionDrawable(uiAtlas.findRegion("Deritium")));
                    else if (bi.getCredits() > 0 || bi.getCreditsBonus() > 0)
                        symbol.setDrawable(new TextureRegionDrawable(uiAtlas.findRegion("creditsSmall")));
                if (buildList.get(i) < 0)
                    text = StringDB.getString("UPGRADING", false, bi.getBuildingName());
                else
                    text = bi.getBuildingName();
            } else if (selectedProduction == 1) {
                ShipInfo si = manager.getShipInfos().get(buildList.get(i) - 10000);
                text = si.getShipClass() + "-" + StringDB.getString("CLASS");
            } else if (selectedProduction == 2) {
                TroopInfo ti = manager.getTroopInfos().get(buildList.get(i) - 20000);
                text = ti.getName();
            }

            int nrounds = starSystem.neededRoundsToBuild(buildList.get(i), false, starSystem.getManager().isActive());
            boolean canAddToAssemblyList = starSystem.getAssemblyList().makeEntry(buildList.get(i),
                    starSystem.getCoordinates(), manager, true);
            Color fontColor = canAddToAssemblyList ? normalColor : new Color(1.0f, 1.0f, 1.0f, 100 / 255.0f);
            button.add().width(hpadding / 2);
            button.setName(i + "");
            button.add(symbol).height(font.getLineHeight()).width(font.getLineHeight() * 1.25f)
                    .spaceRight(hpadding / 4).align(Align.left);
            Label info = new Label(text, skin, "normalFont", Color.WHITE);
            info.setColor(fontColor);
            info.setTouchable(Touchable.disabled);
            button.add(info).align(Align.left);
            String number = "";
            if (nrounds != 0)
                number += nrounds;
            Label turnsNeeded = new Label(number, skin, "normalFont", Color.WHITE);
            turnsNeeded.setColor(fontColor);
            turnsNeeded.setTouchable(Touchable.disabled);
            GlyphLayout layout = new GlyphLayout(font, text);
            float textWith = layout.width;
            layout.setText(font, number);
            float numberWidth = layout.width;
            int spaceLeft = (int) (rect.width - textWith - numberWidth - hpadding * 2 - symbol.getWidth() - hpadding / 4);
            button.add(turnsNeeded).pad(0).spaceLeft(spaceLeft < 0 ? 0 : spaceLeft).align(Align.right);
            button.add().width(hpadding / 2).height(font.getLineHeight() + vpadding);
            button.setUserObject(info);
            info.setUserObject(turnsNeeded);
            if (buildSelection != null) {
                int oldIdx = Integer.parseInt(buildSelection.getName());
                if (oldIdx == i)
                    sel = button;
            }
            if (i == 0 && sel == null) {
                sel = button;
            }

            ActorGestureListener gestureListener = new ActorGestureListener() {
                @Override
                public void tap(InputEvent event, float x, float y, int count, int button) {
                    Button b = (Button) event.getListenerActor();
                    int i = Integer.parseInt(b.getName());

                    markBuildListSelected(b);

                    if (count >= 2 || button == 1) {
                        tryAddToAssemblyList(i);
                    }
                }

            };
            button.addListener(gestureListener);
            buildScrollTable.add(button);
            buildScrollTable.row();
        }

        buildScrollTable.addListener(new InputListener() {
            @Override
            public boolean keyDown (InputEvent event, final int keycode) {
                if (buildSelection == null)
                    return false;
                int selectedItem = Integer.parseInt(buildSelection.getName());
                if (buildList.size < selectedItem)
                    selectedItem = buildList.size - 1;
                if (selectedItem < 0)
                    return false;
                Button b = buildScrollTable.findActor(buildSelection.getName());
                switch (keycode) {
                    case Keys.DOWN:
                        selectedItem++;
                        break;
                    case Keys.UP:
                        selectedItem--;
                        break;
                    case Keys.HOME:
                        selectedItem = 0;
                        break;
                    case Keys.END:
                        selectedItem = buildList.size - 1;
                        break;
                    case Keys.PAGE_DOWN:
                        selectedItem += buildScroller.getScrollHeight() / b.getHeight();
                        break;
                    case Keys.PAGE_UP:
                        selectedItem -= buildScroller.getScrollHeight() / b.getHeight();
                        break;
                    case Keys.ENTER:
                        tryAddToAssemblyList(selectedItem);
                        return true;
                }

                if (SystemBuildList.this.isVisible()) {
                    if (selectedItem >= buildList.size)
                        selectedItem = buildList.size - 1;
                    if (selectedItem < 0)
                        selectedItem = 0;

                    b = buildScrollTable.findActor("" + selectedItem);
                    markBuildListSelected(b);

                    Thread th = new Thread() {
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(150);
                            } catch (InterruptedException e) {
                            }
                            if (Gdx.input.isKeyPressed(keycode)) {
                                Gdx.app.postRunnable(new Runnable() {
                                    @Override
                                    public void run() {
                                        InputEvent event = new InputEvent();
                                        event.setType(Type.keyDown);
                                        event.setKeyCode(keycode);
                                        buildScrollTable.fire(event);
                                    }
                                });
                            }
                        }
                    };
                    th.start();
                }

                return false;
            }
        });

        stage.draw(); //this is needed to validate layout before marking selection
        if (sel != null)
            markBuildListSelected(sel);
    }

    private void markBuildListSelected(Button b) {
        if (buildSelection != null) {
            buildSelection.getStyle().up = null;
            buildSelection.getStyle().down = null;
            ((Label) buildSelection.getUserObject()).setColor(oldColor);
            ((Label) ((Label) buildSelection.getUserObject()).getUserObject()).setColor(oldColor);
        }
        if (b == null)
            b = buildSelection;
        Image itemSelection = new Image(selectTexture);
        b.getStyle().up = itemSelection.getDrawable();
        b.getStyle().down = itemSelection.getDrawable();
        oldColor = new Color(((Label) b.getUserObject()).getColor());
        ((Label) b.getUserObject()).setColor(markColor);
        ((Label) ((Label) b.getUserObject()).getUserObject()).setColor(markColor);
        buildSelection = b;

        float scrollerHeight = buildScroller.getScrollHeight();
        float scrollerPos = buildScroller.getScrollY();
        int selectedItem = Integer.parseInt(buildSelection.getName());
        float buttonPos = b.getHeight() * selectedItem;
        if (buttonPos + b.getHeight() / 2 - scrollerPos < 0)
            buildScroller.setScrollY(b.getHeight() * selectedItem - buildScroller.getScrollHeight() * 2);
        else if (buttonPos + b.getHeight() / 2 - scrollerPos > scrollerHeight)
            buildScroller.setScrollY(b.getHeight() * (selectedItem - buildScroller.getScrollHeight() / b.getHeight() + 1));

        drawResourceTable();
        drawSelectionInfo();
    }

    private void tryAddToAssemblyList(int index) {
        int id = buildList.get(index);
        int runningNumber = Math.abs(id);

        if (id < 10000)
            starSystem.getAssemblyList().calculateNeededResources(
                    manager.getBuildingInfo(runningNumber), null, null, starSystem.getAllBuildings(),
                    id, playerRace.getEmpire().getResearch().getResearchInfo());
        else if (id < 20000 && starSystem.getBuildableShips().size > 0)
            starSystem.getAssemblyList().calculateNeededResources(null,
                    manager.getShipInfos().get(id - 10000), null, starSystem.getAllBuildings(), id,
                    playerRace.getEmpire().getResearch().getResearchInfo());
        else if (starSystem.getBuildableTroops().size > 0)
            starSystem.getAssemblyList().calculateNeededResources(null, null,
                    manager.getTroopInfos().get(id - 20000), starSystem.getAllBuildings(), id,
                    playerRace.getEmpire().getResearch().getResearchInfo());

        if (starSystem.getAssemblyList().makeEntry(id, starSystem.getCoordinates(), manager, false)) {
            if (runningNumber < 10000 && manager.getBuildingInfo(runningNumber).getMaxInEmpire() > 0) {
                manager.getGlobalBuildings().addGlobalBuilding(playerRace.getRaceId(), runningNumber);

                for (int k = 0; k < playerRace.getEmpire().getSystemList().size; k++) {
                    StarSystem other = manager.getUniverseMap().getStarSystemAt(
                            playerRace.getEmpire().getSystemList().get(k));
                    other.assemblyListCheck(manager.getBuildingInfos());
                }
            } else if (id < 0 || runningNumber < 10000
                    && manager.getBuildingInfo(runningNumber).getMaxInSystem().number > 0)
                starSystem.assemblyListCheck(manager.getBuildingInfos());

            playerRace.reflectPossibleResearchOrSecurityWorkerChange(starSystem.getCoordinates(), true, true);
            Gdx.app.postRunnable(new Runnable() {
                @Override
                public void run() {
                    show();
                }
            });
        } else
            manager.getSoundManager().playSound(SndMgrValue.SNDMGR_SOUND_ERROR);
    }

    /**
     * Helper function to add an item in the productiontable
     */
    private void addToProdTable(String image, String info, int prod, String store) {
        BitmapFont font = skin.getFont("normalFont");
        Label prodLabel;
        Label storeLabel;
        float hpadding = GameConstants.wToRelative(80);
        String text;
        Color c1 = normalColor;
        Color c2 = normalColor;
        productionTable.add(new Image(new TextureRegion(uiAtlas.findRegion(image)))).height(font.getLineHeight())
                .width(font.getLineHeight() * 1.25f).align(Align.left).spaceRight((int) (hpadding / 4));
        productionTable.add(new Label(StringDB.getString(info) + ":", skin, "normalFont", normalColor)).width(
                (int) (GameConstants.wToRelative(90)));
        if (info.equals("FOOD") || info.equals("ENERGY"))
            c1 = prod < 0 ? Color.RED : normalColor;
        if (info.equals("MORAL")) {
            if (prod > 0)
                c1 = Color.GREEN;
            int morale = starSystem.getMorale();
            if (morale > 174)
                c2 = new Color(0, 250 / 255.0f, 0, 1.0f); //Fanatic
            else if (morale > 154)
                c2 = new Color(20 / 255.0f, 150 / 255.0f, 20 / 255.0f, 1.0f); //Loyal
            else if (morale > 130)
                c2 = new Color(20 / 255.0f, 150 / 255.0f, 100 / 255.0f, 1.0f); //Pleased
            else if (morale > 99)
                c2 = new Color(150 / 255.0f, 150 / 255.0f, 200 / 255.0f, 1.0f); //Satisfied
            else if (morale > 75)
                c2 = new Color(160 / 255.0f, 160 / 255.0f, 160 / 255.0f, 1.0f); //Apathetic
            else if (morale > 49)
                c2 = new Color(200 / 255.0f, 100 / 255.0f, 50 / 255.0f, 1.0f); //Angry
            else if (morale > 29)
                c2 = new Color(210 / 255.0f, 80 / 255.0f, 50 / 255.0f, 1.0f); //Furious
            else
                c1 = Color.RED; //Rebellious
        }
        text = prod + "";
        prodLabel = new Label(text, skin, "normalFont", c1);
        prodLabel.setAlignment(Align.center);
        prodLabel.setWidth(productionLabel.getWidth());
        productionTable.add(prodLabel).width(productionLabel.getWidth()).align(Align.center)
                .spaceRight((int) (storageLabel.getX() - productionLabel.getX() - productionLabel.getWidth()));
        storeLabel = new Label(store, skin, "normalFont", c2);
        storeLabel.setAlignment(Align.center);
        storeLabel.setWidth(storeLabel.getWidth());
        productionTable.add(storeLabel).width(storageLabel.getWidth()).align(Align.center);
        productionTable.row();
    }

    /**
     * Function draws the production table - info about the system's production
     */
    public void drawProduction() {
        productionScroller.setVisible(true);
        productionLabel.setVisible(true);
        storageLabel.setVisible(true);

        Rectangle rect = GameConstants.coordsToRelative(821, 755, 313, 350);
        rect.x += xOffset;

        productionScroller.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        productionTable.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        productionTable.clear();
        SystemProd prod = starSystem.getProduction();
        addToProdTable("foodSmall", "FOOD", prod.getFoodProd(), starSystem.getFoodStore() + "");
        String text;
        if (selectedProduction == 0) { //industry menu
            if (prod.getUpdateBuildSpeed() == prod.getBuildingBuildSpeed())
                text = (100 + prod.getBuildingBuildSpeed()) + "%";
            else
                text = (100 + prod.getBuildingBuildSpeed()) + "% " + (100 + prod.getUpdateBuildSpeed()) + "%";
        } else if (selectedProduction == 1) { //ship menu
            text = (prod.getShipYardEfficiency() * (100 + prod.getShipBuildSpeed()) / 100) + "%";
        } else { //troop menu
            text = (prod.getBarrackEfficiency() * (100 + prod.getTroopBuildSpeed()) / 100) + "%";
        }
        addToProdTable("industrySmall", "INDUSTRY", prod.getIndustrialProductionByType(selectedProduction), text);
        addToProdTable("energySmall", "ENERGY", prod.getEnergyProd(), "");
        addToProdTable("securitySmall", "SECURITY", prod.getSecurityProd(), "");
        addToProdTable("researchSmall", "RESEARCH", prod.getResearchProd(), "");
        int[] resFromRoutes = new int[ResourceTypes.DERITIUM.getType() + 1];
        Arrays.fill(resFromRoutes, 0);
        int[] resInDistSys = new int[ResourceTypes.DERITIUM.getType() + 1];
        Arrays.fill(resInDistSys, 0);
        if (starSystem.getBlockade() == 0)
            for (int j = 0; j < playerRace.getEmpire().getSystemList().size; j++) {
                StarSystem system = manager.getUniverseMap().getStarSystemAt(
                        playerRace.getEmpire().getSystemList().get(j));
                if (!system.getCoordinates().equals(starSystem.getCoordinates())) {
                    if (system.getBlockade() > 0)
                        continue;
                    for (int i = 0; i < system.getResourceRoutes().size; i++) {
                        IntPoint target = system.getResourceRoutes().get(i).getCoord();
                        if (target.equals(starSystem.getCoordinates())) {
                            int res = system.getResourceRoutes().get(i).getResource().getType();
                            resFromRoutes[res] += system.getResourceStore(res);
                        }
                    }
                    for (int res = ResourceTypes.TITAN.getType(); res <= ResourceTypes.DERITIUM.getType(); res++)
                        if (system.getProduction().getResourceDistributor(res))
                            resInDistSys[res] = system.getResourceStore(res);
                }
            }

        for (int res = ResourceTypes.TITAN.getType(); res <= ResourceTypes.DERITIUM.getType(); res++) {
            ResourceTypes rt = ResourceTypes.fromResourceTypes(res);
            if (resInDistSys[res] > resFromRoutes[res] + starSystem.getResourceStore(res))
                text = "[" + resInDistSys[res] + "]";
            else if (resFromRoutes[res] > 0)
                text = "(" + (starSystem.getResourceStore(res) + resFromRoutes[res]) + ")";
            else
                text = "" + starSystem.getResourceStore(res);
            addToProdTable(rt.getImgName(), rt.getName(), prod.getResourceProd(rt), text);
        }
        addToProdTable("moralSmall", "MORAL", prod.getMoraleProd(), starSystem.getMorale() + "");
        addToProdTable("creditsSmall", "CREDITS", prod.getCreditsProd(), "");
    }

    /**
     * Helper
     * @param index
     */
    private void deleteAssemblyListEntry(int index) {
        int assemblyListEntry = starSystem.getAssemblyList().getAssemblyListEntry(index).id;
        int entryCount = starSystem.getAssemblyList().getAssemblyListEntry(index).count;
        int runningNumber = Math.abs(assemblyListEntry);

        if (index == 0 && entryCount == 1) {
            for (int j = ResourceTypes.TITAN.getType(); j <= ResourceTypes.DERITIUM.getType(); j++) {
                int getBackRes = starSystem.getAssemblyList().getNeededResourceInAssemblyList(0, j);
                for (int k = 0; k < playerRace.getEmpire().getSystemList().size; k++) {
                    IntPoint coord = playerRace.getEmpire().getSystemList().get(k);
                    if (!coord.equals(starSystem.getCoordinates())) {
                        StarSystem other = manager.getUniverseMap().getStarSystemAt(coord);
                        for (int l = 0; l < other.getResourceRoutes().size; l++) {
                            ResourceRoute rl = other.getResourceRoutes().get(l);
                            if (rl.getCoord().equals(starSystem.getCoordinates()))
                                if (rl.getResource().getType() == j)
                                    if (rl.getPercent() > 0) {
                                        int back = starSystem.getAssemblyList().getNeededResourceInAssemblyList(0, j)
                                                * rl.getPercent() / 100;
                                        other.addResourceStore(j, back);
                                        getBackRes -= back;
                                    }
                        }
                    }
                }
                starSystem.addResourceStore(j, getBackRes);
            }
            if (starSystem.getAssemblyList().getWasBuildingBought()) {
                playerRace.getEmpire().setCredits(starSystem.getAssemblyList().getBuildCosts());
                //Correct the prices on the market
                //also call with notAtMarket == true!!!
                for (int j = ResourceTypes.TITAN.getType(); j <= ResourceTypes.IRIDIUM.getType(); j++)
                    playerRace.getTrade().sellResource(j,
                            starSystem.getAssemblyList().getNeededResourceInAssemblyList(0, j),
                            starSystem.getCoordinates(), true);
                starSystem.getAssemblyList().setWasBuildingBought(false);
            }
            starSystem.getAssemblyList().clearAssemblyList(starSystem.getCoordinates(), manager);
            playerRace.reflectPossibleResearchOrSecurityWorkerChange(starSystem.getCoordinates(), true, true);
        } else {
            starSystem.getAssemblyList().adjustAssemblyList(index);
        }

        if (runningNumber < 10000 && runningNumber != 0) { //if a building was removed
            if (manager.getBuildingInfo(runningNumber).getMaxInEmpire() > 0) {
                manager.getGlobalBuildings().deleteGlobalBuilding(playerRace.getRaceId(), runningNumber);
                for (int i = 0; i < playerRace.getEmpire().getSystemList().size; i++) {
                    StarSystem other = manager.getUniverseMap().getStarSystemAt(
                            playerRace.getEmpire().getSystemList().get(i));
                    other.assemblyListCheck(manager.getBuildingInfos());
                }

            } else if (assemblyListEntry < 0 || manager.getBuildingInfo(runningNumber).getMaxInSystem().number > 0)
                starSystem.assemblyListCheck(manager.getBuildingInfos());
        }

        show();
    }

    private void drawAssemblyList() {
        drawAssemblyList(false);
    }

    /**
     * Function draws the current build queue
     */
    public void drawAssemblyList(boolean onlyInfo) {
        assemblyListGroup.setVisible(true);
        BitmapFont font = skin.getFont("smallFont");
        float hpadding = GameConstants.wToRelative(20);
        float vpadding = GameConstants.hToRelative(30);
        Rectangle rect = GameConstants.coordsToRelative(830, 375, 320, 240);
        rect.x += xOffset;
        assemblyListGroup.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        assemblyListGroup.clear();
        for (int i = 0; i < GameConstants.ALE; i++) {
            int assemblyListEntry = starSystem.getAssemblyList().getAssemblyListEntry(i).id;
            int entryCount = starSystem.getAssemblyList().getAssemblyListEntry(i).count;
            String graphicName = "";
            String text = "";
            if (entryCount > 1)
                text = "(" + entryCount + ") ";
            String turns = "";
            Image symbol = new Image();
            symbol.setTouchable(Touchable.disabled);

            if (assemblyListEntry != 0) {
                if (i == 0) {
                    if (!starSystem.getAssemblyList().getWasBuildingBought())
                        if ((assemblyListEntry < 0)
                                || (assemblyListEntry > 0 && assemblyListEntry < 10000 && !manager.getBuildingInfo(
                                        assemblyListEntry).getNeverReady())
                                || (assemblyListEntry >= 10000 && assemblyListEntry < 20000 && starSystem
                                        .getProduction().isShipyard())
                                || (assemblyListEntry >= 20000 && starSystem.getProduction().isBarrack()))
                            buyButton.setVisible(true);
                        else
                            buyButton.setVisible(false);
                    deleteButton.setVisible(true);
                }
                String path = "";
                if (assemblyListEntry < 0) {
                    text += StringDB.getString("UPGRADING", false, manager.getBuildingInfo(Math.abs(assemblyListEntry))
                            .getBuildingName());
                    graphicName = manager.getBuildingInfo(Math.abs(assemblyListEntry)).getGraphicFileName();
                    path = "graphics/buildings/" + graphicName + ".png";
                } else if (assemblyListEntry < 10000) {
                    text += manager.getBuildingInfo(assemblyListEntry).getBuildingName();
                    graphicName = manager.getBuildingInfo(assemblyListEntry).getGraphicFileName();
                    path = "graphics/buildings/" + graphicName + ".png";
                } else if (assemblyListEntry < 20000) {
                    text += manager.getShipInfos().get(assemblyListEntry - 10000).getShipClass() + "-"
                            + StringDB.getString("CLASS");
                    graphicName = manager.getShipInfos().get(assemblyListEntry - 10000).getShipImageName();
                    path = "graphics/ships/" + graphicName + ".png";
                } else {
                    text += manager.getTroopInfos().get(assemblyListEntry - 20000).getName();
                    graphicName = manager.getTroopInfos().get(assemblyListEntry - 20000).getGraphicFile();
                    path = "graphics/troops/" + graphicName + ".png";
                }
                turns = "" + starSystem.neededRoundsToBuild(i, true);
                Texture tex = manager.loadTextureImmediate(path);
                loadedTextures.add(path);
                symbol.setDrawable(new TextureRegionDrawable(new TextureRegion(tex)));
            } else if (i == 0) {
                text = StringDB.getString("COMMODITIES");
                buyButton.setVisible(false);
                deleteButton.setVisible(false);
            }
            Button.ButtonStyle bs = new Button.ButtonStyle();
            Button button = new Button(bs);
            button.setName(i + "");
            button.align(Align.center);
            button.setSkin(skin);
            button.add(symbol).size(vpadding).spaceLeft(hpadding).align(Align.left);
            Color fontColor = i != 0 ? normalColor : markColor;
            Label textLabel = new Label(text, skin, "smallFont", fontColor);
            textLabel.setTouchable(Touchable.disabled);
            button.add(textLabel).spaceLeft(hpadding / 4);
            Label turnLabel = new Label(turns, skin, "smallFont", fontColor);
            turnLabel.setTouchable(Touchable.disabled);
            GlyphLayout layout = new GlyphLayout(font, text);
            float textWidth = layout.width;
            layout.setText(font, turns);
            float numberWidth = layout.width;
            int spaceLeft = (int) (rect.width - textWidth - hpadding * 2 - numberWidth - symbol.getWidth());
            button.add(turnLabel).pad(0).spaceLeft(spaceLeft < 0 ? 0 : spaceLeft).align(Align.right);

            ActorGestureListener gestureListener = new ActorGestureListener() {
                @Override
                public void tap(InputEvent event, float x, float y, int count, int button) {
                    Button b = (Button) event.getListenerActor();
                    int index = Integer.parseInt(b.getName());
                    int assemblyListEntry = starSystem.getAssemblyList().getAssemblyListEntry(index).id;
                    int entryCount = starSystem.getAssemblyList().getAssemblyListEntry(index).count;
                    boolean enquire = false;
                    if (assemblyListEntry != 0 && index == 0 && entryCount == 1) {
                        int roundsMax = starSystem.neededRoundsToBuild(assemblyListEntry, false, false);
                        int roundsRemain = starSystem.neededRoundsToBuild(index, true);
                        enquire = (roundsMax != roundsRemain) || starSystem.getAssemblyList().getWasBuildingBought();
                    }

                    if (count >= 2 || button == 1) {
                        if (assemblyListEntry != 0) {
                            if (index == 0 && enquire) {
                                clickedOnDeleteButton = true;
                                clickedOnBuyButton = false;
                                drawInquiry(false);
                            } else {
                                deleteAssemblyListEntry(index);
                            }
                        }
                    }
                }
            };
            if (!onlyInfo)
                button.addListener(gestureListener);
            assemblyListGroup.add(button);
            assemblyListGroup.row();
        }
        if (onlyInfo) {
            buyButton.setVisible(false);
            deleteButton.setVisible(false);
        }
    }

    private void drawResourceTable() {
        resourcesTable.clear();
        if (buildSelection != null) {
            int index = Integer.parseInt(buildSelection.getName());
            if (index < buildList.size) {
                int runningNumber = Math.abs(buildList.get(index));
                if (buildList.get(index) < 10000) {
                    starSystem.getAssemblyList().calculateNeededResources(manager.getBuildingInfo(runningNumber), null,
                            null, starSystem.getAllBuildings(), buildList.get(index),
                            playerRace.getEmpire().getResearch().getResearchInfo());
                } else if (buildList.get(index) < 20000 && starSystem.getBuildableShips().size > 0) {
                    starSystem.getAssemblyList().calculateNeededResources(null,
                            manager.getShipInfos().get(runningNumber - 10000), null, starSystem.getAllBuildings(),
                            buildList.get(index), playerRace.getEmpire().getResearch().getResearchInfo());
                } else if (starSystem.getBuildableTroops().size > 0) {
                    starSystem.getAssemblyList().calculateNeededResources(null, null,
                            manager.getTroopInfos().get(runningNumber - 20000), starSystem.getAllBuildings(),
                            buildList.get(index), playerRace.getEmpire().getResearch().getResearchInfo());
                }
                Rectangle rect = GameConstants.coordsToRelative(380, 205, 395, 100);
                resourcesTable.setBounds((int) rect.x + xOffset, (int) rect.y, (int) rect.width, (int) rect.height);
                resourcesTable.align(Align.top);
                resourcesTable.setSkin(skin);
                Label bc = new Label(StringDB.getString("BUILD_COSTS"), skin, "normalFont", markColor);
                bc.setAlignment(Align.center);
                resourcesTable.add(bc).width(rect.width).align(Align.center);
                resourcesTable.row();
                Table container = new Table();
                container.setSkin(skin);
                String text;
                text = StringDB.getString("INDUSTRY") + ": " + starSystem.getAssemblyList().getNeededIndustryForBuild();
                container.add(text, "normalFont", normalColor).align(Align.center).expand();
                text = StringDB.getString("TITAN") + ": " + starSystem.getAssemblyList().getNeededTitanForBuild();
                container.add(text, "normalFont", normalColor).align(Align.center).expand();
                text = StringDB.getString("DEUTERIUM") + ": "
                        + starSystem.getAssemblyList().getNeededDeuteriumForBuild();
                container.add(text, "normalFont", normalColor).align(Align.center).expand();
                container.row();
                text = StringDB.getString("DURANIUM") + ": " + starSystem.getAssemblyList().getNeededDuraniumForBuild();
                container.add(text, "normalFont", normalColor);
                text = StringDB.getString("CRYSTAL") + ": " + starSystem.getAssemblyList().getNeededCrystalForBuild();
                container.add(text, "normalFont", normalColor);
                text = StringDB.getString("IRIDIUM") + ": " + starSystem.getAssemblyList().getNeededIridiumForBuild();
                container.add(text, "normalFont", normalColor);
                resourcesTable.add(container).fill();
                text = "";
                if (starSystem.getAssemblyList().getNeededDeritiumForBuild() > 0) {
                    text = StringDB.getString("DERITIUM") + ": "
                            + starSystem.getAssemblyList().getNeededDeritiumForBuild();
                }
                resourcesTable.row();
                resourcesTable.add(text, "normalFont", normalColor);
            }
        }

        if(resourcesTable.getCells().size == 0){
            for(int i = 0; i < 4; i++) {
                resourcesTable.add("", "normalFont", normalColor);
                resourcesTable.row();
            }
        }
        //system manager
        resourcesTable.row();

        Table table = new Table();
        table.add().width(GameConstants.wToRelative(30));
        String text = StringDB.getString("BTN_SYSTEMMANAGER") + " " + (starSystem.getManager().isActive() ? StringDB.getString("ON") : StringDB.getString("OFF"));
        Label managerInfo = new Label(text, skin, "normalFont", Color.WHITE);
        managerInfo.setColor(starSystem.getManager().isActive() ? Color.GREEN : Color.RED);
        table.add(managerInfo).width(GameConstants.wToRelative(133)).spaceRight(GameConstants.wToRelative(75));

        TextButtonStyle style = new TextButtonStyle(skin.get("small-buttons", TextButtonStyle.class));
        if (!starSystem.getBuildTargetCoord().equals(new IntPoint())) {
            StarSystem ss = manager.getUniverseMap().getStarSystemAt(starSystem.getBuildTargetCoord());
            text = ss.getScanned(playerRace.getRaceId()) ? ss.getName() : ss.coordsName(false);
        } else
            text = StringDB.getString("BTN_NO_TARGET");


        TextButton setBuildTargetCoordButton = new TextButton(text, style);
        setBuildTargetCoordButton.setVisible(true);
        setBuildTargetCoordButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                IntPoint target = starSystem.getBuildTargetCoord().equals(new IntPoint()) ? starSystem.getCoordinates() : starSystem.getBuildTargetCoord();
                manager.getUniverseMap().setSelectedCoordValue(target);
                manager.setView(ViewTypes.GALAXY_VIEW);
                manager.getUniverseMap().getRenderer().setBuildTargetCoord(starSystem.getCoordinates());
            }
        });
        table.add(setBuildTargetCoordButton).width(GameConstants.wToRelative(133)).height(GameConstants.hToRelative(30));
        resourcesTable.add(table);
    }

    private void drawWorkerTable() {
        BitmapFont font = skin.getFont("normalFont");
        Rectangle rect = GameConstants.coordsToRelative(78, 562, 221, 125);
        float hpadding = GameConstants.wToRelative(40);
        workerTable.setBounds((int) rect.x + xOffset, (int) rect.y, (int) rect.width, (int) rect.height);
        workerTable.clear();
        workerTable.align(Align.center);
        workerTable.setSkin(skin);
        workerTable.clear();
        for (int i = WorkerType.FOOD_WORKER.getType(); i <= WorkerType.IRIDIUM_WORKER.getType(); i++) {
            WorkerType worker = WorkerType.fromWorkerType(i);
            workerTable.add(new Image(new TextureRegion(uiAtlas.findRegion(worker.getGraphic()))))
                    .height(font.getLineHeight()).width(font.getLineHeight() * 1.25f).align(Align.left)
                    .spaceRight(hpadding / 4);
            workerTable.add(
                    new Label(starSystem.getWorker(worker) + "/" + starSystem.getNumberOfWorkBuildings(worker, 0),
                            skin, "normalFont", normalColor)).spaceRight(i % 2 == 0 ? hpadding : 0);
            if (i % 2 != 0)
                workerTable.row().fill();
        }
    }

    private void drawSelectionInfo() {
        infoTable.clear();
        infoImage.setDrawable(null);
        infoImage.setVisible(false);
        if (buildSelection != null) {
            infoImage.setVisible(true);
            int index = Integer.parseInt(buildSelection.getName());
            if (index < buildList.size) {
                int runningNumber = Math.abs(buildList.get(index));
                float vpadding = GameConstants.hToRelative(10);
                String graphicName;
                Rectangle imageRect = GameConstants.coordsToRelative(75, 745, 230, 160);
                infoImage.setBounds((int) (imageRect.x + xOffset), (int) imageRect.y, (int) imageRect.width,
                        (int) imageRect.height);
                infoScroller.setScrollY(0);
                infoTable.align(Align.center);
                String desc = "";
                if (buildList.get(index) < 10000) {//building
                    BuildingInfo bi = manager.getBuildingInfo(runningNumber);
                    if (infoOrDescription) {
                        if (buildList.get(index) > 0 || !bi.getWorker()) {
                            bi.drawBuildingInfo(infoTable, skin, normalColor, markColor, infoScroller.getWidth(), vpadding);
                        } else if (buildList.get(index) < 0) {
                            //Current building which has to be upgraded
                            BuildingInfo b1 = manager.getBuildingInfo(manager.getBuildingInfos().get(runningNumber - 1)
                                    .getPredecessorId());
                            if (b1.getWorker()) {
                                String text = StringDB.getString("UPGRADE_FROM_TO", false, b1.getBuildingName(),
                                        bi.getBuildingName());
                                Label nameLabel = new Label(text, skin, "normalFont", markColor);
                                nameLabel.setWrap(true);
                                nameLabel.setAlignment(Align.center);
                                infoTable.add(nameLabel).width(infoScroller.getWidth()).spaceBottom(vpadding);
                                infoTable.row();
                                Label prodLabel = new Label(StringDB.getString("RELATIVE_PROFIT") + ":", skin,
                                        "normalFont", markColor);
                                prodLabel.setWrap(true);
                                prodLabel.setAlignment(Align.center);
                                infoTable.add(prodLabel).width(infoScroller.getWidth());
                                infoTable.row();
                                text = "";
                                int number = 0;
                                if (b1.getFoodProd() > 0) {
                                    number = starSystem.getNumberOfWorkBuildings(WorkerType.FOOD_WORKER, 0);
                                    text = (bi.getFoodProd() * number - b1.getFoodProd() * number) + " "
                                            + StringDB.getString("FOOD") + "\n";
                                } else if (b1.getIndustryPointsProd() > 0) {
                                    number = starSystem.getNumberOfWorkBuildings(WorkerType.INDUSTRY_WORKER, 0);
                                    text = (bi.getIndustryPointsProd() * number - b1.getIndustryPointsProd() * number)
                                            + " " + StringDB.getString("INDUSTRY") + "\n";
                                } else if (b1.getEnergyProd() > 0) {
                                    number = starSystem.getNumberOfWorkBuildings(WorkerType.ENERGY_WORKER, 0);
                                    text = (bi.getEnergyProd() * number - b1.getEnergyProd() * number) + " "
                                            + StringDB.getString("ENERGY") + "\n";
                                } else if (b1.getSecurityPointsProd() > 0) {
                                    number = starSystem.getNumberOfWorkBuildings(WorkerType.SECURITY_WORKER, 0);
                                    text = (bi.getSecurityPointsProd() * number - b1.getSecurityPointsProd() * number)
                                            + " " + StringDB.getString("SECURITY") + "\n";
                                } else if (b1.getResearchPointsProd() > 0) {
                                    number = starSystem.getNumberOfWorkBuildings(WorkerType.RESEARCH_WORKER, 0);
                                    text = (bi.getResearchPointsProd() * number - b1.getResearchPointsProd() * number)
                                            + " " + StringDB.getString("RESEARCH") + "\n";
                                } else if (b1.getTitanProd() > 0) {
                                    number = starSystem.getNumberOfWorkBuildings(WorkerType.TITAN_WORKER, 0);
                                    text = (bi.getTitanProd() * number - b1.getTitanProd() * number) + " "
                                            + StringDB.getString("TITAN") + "\n";
                                } else if (b1.getDeuteriumProd() > 0) {
                                    number = starSystem.getNumberOfWorkBuildings(WorkerType.DEUTERIUM_WORKER, 0);
                                    text = (bi.getDeuteriumProd() * number - b1.getDeuteriumProd() * number) + " "
                                            + StringDB.getString("DEUTERIUM") + "\n";
                                } else if (b1.getDuraniumProd() > 0) {
                                    number = starSystem.getNumberOfWorkBuildings(WorkerType.DURANIUM_WORKER, 0);
                                    text = (bi.getDuraniumProd() * number - b1.getDuraniumProd() * number) + " "
                                            + StringDB.getString("DURANIUM") + "\n";
                                } else if (b1.getCrystalProd() > 0) {
                                    number = starSystem.getNumberOfWorkBuildings(WorkerType.CRYSTAL_WORKER, 0);
                                    text = (bi.getCrystalProd() * number - b1.getCrystalProd() * number) + " "
                                            + StringDB.getString("CRYSTAL") + "\n";
                                } else if (b1.getIridiumProd() > 0) {
                                    number = starSystem.getNumberOfWorkBuildings(WorkerType.IRIDIUM_WORKER, 0);
                                    text = (bi.getIridiumProd() * number - b1.getIridiumProd() * number) + " "
                                            + StringDB.getString("IRIDIUM") + "\n";
                                }
                                text += StringDB.getString("NUMBER_OF_UPGRADEABLE_BUILDINGS") + ": " + number;
                                Label prodInfoLabel = new Label(text, skin, "normalFont", normalColor);
                                prodInfoLabel.setWrap(true);
                                prodInfoLabel.setAlignment(Align.center);
                                infoTable.add(prodInfoLabel).width(infoScroller.getWidth());
                            }
                        }
                    } else {
                        desc = bi.getBuildingDescription();
                    }
                    graphicName = bi.getGraphicFileName();
                    String path = "graphics/buildings/" + graphicName + ".png";
                    loadedTextures.add(path);
                    TextureRegion tex = new TextureRegion(manager.loadTextureImmediate(path));
                    infoImage.setDrawable(new TextureRegionDrawable(tex));
                    bi.getTooltip((Table) infoImage.getUserObject(), skin, tooltipHeaderFont, tooltipHeaderColor, tooltipTextFont, tooltipTextColor);
                } else if (buildList.get(index) < 20000) { //ship
                    ShipInfo si = manager.getShipInfos().get(runningNumber - 10000);
                    Ship ship = new Ship(si);
                    ship.addSpecialResearchBoni(playerRace);
                    si = new ShipInfo(ship, si);
                    graphicName = si.getShipImageName();
                    String path = "graphics/ships/" + graphicName + ".png";
                    loadedTextures.add(path);
                    TextureRegion tex = new TextureRegion(manager.loadTextureImmediate(path));
                    infoImage.setDrawable(new TextureRegionDrawable(tex));
                    if (infoOrDescription)
                        si.drawShipInfo(infoTable, skin, markColor, normalColor, infoScroller.getWidth(), vpadding, 0);
                    else
                        desc = si.getDescription();
                    si.getTooltip(null, (Table) infoImage.getUserObject(), skin, tooltipHeaderFont, tooltipHeaderColor, tooltipTextFont, tooltipTextColor);
                } else { //troop
                    TroopInfo ti = manager.getTroopInfos().get(runningNumber - 20000);
                    if (infoOrDescription) {
                        ResearchInfo ri = playerRace.getEmpire().getResearch().getResearchInfo();
                        ti.drawTroopInfo(ri, infoTable, skin, normalColor, markColor, infoScroller.getWidth(), vpadding);
                    } else
                        desc = ti.getDescription();
                    graphicName = ti.getGraphicFile();
                    String path = "graphics/troops/" + graphicName + ".png";
                    loadedTextures.add(path);
                    TextureRegion tex = new TextureRegion(manager.loadTextureImmediate(path));
                    infoImage.setDrawable(new TextureRegionDrawable(tex));
                    ti.getTooltip(playerRace.getEmpire().getResearch().getResearchInfo(), (Table) infoImage.getUserObject(), skin, tooltipHeaderFont, tooltipHeaderColor, tooltipTextFont, tooltipTextColor);
                }
                if (!infoOrDescription) {
                    Label descLabel = new Label(desc, skin, "normalFont", normalColor);
                    descLabel.setWrap(true);
                    descLabel.setAlignment(Align.center);
                    infoTable.add(descLabel).width(infoScroller.getWidth());
                }
            }
        }

        //the following is needed to fix the text from jumping around in case it has to be scrolled
        stage.draw();
        if (infoTable.getHeight() > infoScroller.getHeight())
            infoTable.align(Align.top);
    }

    private void drawInquiry(boolean buyOrDelete) {
        resourcesTable.setVisible(false);
        buyDelete.setVisible(true);
        okButton.setVisible(true);
        cancelButton.setVisible(true);
        if (buyOrDelete) {
            clickedOnDeleteButton = false;
            int id = starSystem.getAssemblyList().getAssemblyListEntry(0).id;
            int runningNumber = Math.abs(starSystem.getAssemblyList().getAssemblyListEntry(0).id);
            String text = "";
            if (id < 0)
                text = StringDB.getString("BUY_UPGRADE", false,
                        manager.getBuildingInfo(manager.getBuildingInfos().get(runningNumber - 1).getPredecessorId())
                                .getBuildingName(), manager.getBuildingInfos().get(runningNumber - 1).getBuildingName());
            else if (id < 10000)
                text = StringDB.getString("BUY_BUILDING", false, manager.getBuildingInfo(runningNumber)
                        .getBuildingName());
            else if (id < 20000)
                text = StringDB.getString("BUY_SHIP", false, manager.getShipInfos().get(runningNumber - 10000)
                        .getShipTypeAsString(), manager.getShipInfos().get(runningNumber - 10000).getShipClass());
            else if (id < 30000)
                text = StringDB.getString("BUY_BUILDING", false, manager.getTroopInfos().get(runningNumber - 20000)
                        .getName());
            text += "\n"
                    + StringDB.getString("CREDITS_COSTS", false, "" + starSystem.getAssemblyList().getBuildCosts());
            buyDelete.setText(text);
        } else {
            clickedOnBuyButton = false;
            buyDelete.setText(StringDB.getString("CANCEL_PROJECT"));
        }
    }

    public void show() {
        visible = true;
        if (selectedProduction == 2 && starSystem.getBuildableTroops().size == 0)
            selectedProduction = 1;
        if (selectedProduction == 1 && starSystem.getBuildableShips().size == 0)
            selectedProduction = 0;
        stage.setScrollFocus(buildScroller);
        stage.setKeyboardFocus(buildScrollTable);
        clickedOnBuyButton = false;
        starSystem.calculateBuildableBuildings(); // do this check to update for changes in global buildings
        starSystemNameTable.setVisible(true);
        populationTable.setVisible(true);
        productionScroller.setVisible(true);
        buildScroller.setVisible(true);
        if(playerRace.getRaceId().equals("MAJOR5")) //HACK to make it more visible
            buildScrollerbg.setVisible(true);
        productionLabel.setVisible(true);
        storageLabel.setVisible(true);
        orderLabel.setVisible(true);
        turnLabel.setVisible(true);
        structuresButton.setVisible(true);
        shipyardButton.setVisible(true);
        barrackButton.setVisible(true);
        assemblyListGroup.setVisible(true);
        deleteButton.setVisible(true);
        resourcesTable.setVisible(true);
        workerTable.setVisible(true);
        infoImage.setVisible(true);
        infoScroller.setVisible(true);
        infoButton.setVisible(true);
        descButton.setVisible(true);
        buyDelete.setVisible(false); //invisible by default
        cancelButton.setVisible(false);
        okButton.setVisible(false);
        drawBuildingList();
        drawProduction();
        drawAssemblyList();
        drawResourceTable();
        drawWorkerTable();
        drawSelectionInfo();
    }

    public void hide() {
        visible = false;
        starSystemNameTable.setVisible(false);
        populationTable.setVisible(false);
        productionScroller.setVisible(false);
        buildScroller.setVisible(false);
        buildScrollerbg.setVisible(false);
        productionLabel.setVisible(false);
        storageLabel.setVisible(false);
        orderLabel.setVisible(false);
        turnLabel.setVisible(false);
        structuresButton.setVisible(false);
        shipyardButton.setVisible(false);
        barrackButton.setVisible(false);
        assemblyListGroup.setVisible(false);
        buyButton.setVisible(false);
        deleteButton.setVisible(false);
        resourcesTable.setVisible(false);
        workerTable.setVisible(false);
        infoImage.setVisible(false);
        infoScroller.setVisible(false);
        infoButton.setVisible(false);
        descButton.setVisible(false);
        buyDelete.setVisible(false); //invisible by default
        cancelButton.setVisible(false);
        okButton.setVisible(false);
        clickedOnBuyButton = false;
        clickedOnDeleteButton = false;
        for (String path : loadedTextures)
            if (manager.getAssetManager().isLoaded(path))
                manager.getAssetManager().unload(path);
        loadedTextures.clear();
    }

    public boolean isVisible() {
        return visible;
    }

    public void setStarSystem(StarSystem system) {
        this.starSystem = system;
    }
}