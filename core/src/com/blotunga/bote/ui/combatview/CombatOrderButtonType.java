/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.combatview;

import com.blotunga.bote.constants.CombatTactics;

public enum CombatOrderButtonType {
    ATTACK_BUTTON("BTN_ATTACK", CombatTactics.CT_ATTACK),
    AVOID_BUTTON("BTN_AVOID", CombatTactics.CT_AVOID),
    RETREAT_BUTTON("BTN_RETREAT", CombatTactics.CT_RETREAT);

    private String label;
    private CombatTactics tactic;

    CombatOrderButtonType(String label, CombatTactics tactic) {
        this.label = label;
        this.tactic = tactic;
    }

    public String getLabel() {
        return label;
    }

    public CombatTactics getCombatTactics() {
        return tactic;
    }
}
