/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.blotunga.bote.DefaultScreen;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.ViewTypes;
import com.blotunga.bote.ui.screens.UniverseRenderer;

public class UIButtonListeners extends ClickListener {
    private ViewTypes type;

    public UIButtonListeners(ViewTypes type) {
        this.type = type;
    }

    @Override
    public void clicked(InputEvent event, float x, float y) {
        TextButton button = (TextButton) event.getListenerActor();
        if (!button.isDisabled()) {
            final ScreenManager manager = (ScreenManager) event.getListenerActor().getUserObject();
            Gdx.app.postRunnable(new Runnable() {
                @Override
                public void run() {
                    DefaultScreen screen = manager.setView(type, false);
                    if (type == ViewTypes.GALAXY_VIEW) {
                        boolean clearSelection = !((UniverseRenderer) screen).isShipMove();
                        ((UniverseRenderer) screen).showPlanets(clearSelection);
                    }
                }
            });
        }
    }

    static class EndTurnButtonListener extends ActorGestureListener {
        @Override
        public void touchDown (InputEvent event, float x, float y, int pointer, int button) {
            final TextButton btn = (TextButton) event.getListenerActor();
            final ScreenManager manager = (ScreenManager) btn.getUserObject();
            manager.endTurn();
        }
    }
}