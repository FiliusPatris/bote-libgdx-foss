/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.mainmenu;

public enum MainMenuButtonType {
    BTN_NEW_GAME("NEWGAME", "NEWGAMETT"),
    BTN_LOAD_GAME("LOADGAME", "LOADGAMETT"),
    BTN_ACHIEVEMENTS("ACHIEVEMENTS", "ACHIEVEMENTSTT"),
    BTN_CREDITS("CREDITS", "CREDITSTT"),
    BTN_EXIT("LEAVE", "LEAVETT");

    private String name;
    private String tooltip;

    MainMenuButtonType(String name, String tooltip) {
        this.name = name;
        this.tooltip = tooltip;
    }

    public String getName() {
        return name;
    }

    public String getTooltip() {
        return tooltip;
    }
}
