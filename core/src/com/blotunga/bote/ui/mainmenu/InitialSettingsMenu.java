/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.mainmenu;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Filter;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.PixmapTextureData;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldFilter;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Disposable;
import com.blotunga.bote.DefaultScreen;
import com.blotunga.bote.GamePreferences;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.Difficulties;
import com.blotunga.bote.constants.GalaxyShapes;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.VictoryType;
import com.blotunga.bote.constants.ViewTypes;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.races.starmap.ExpansionSpeed;
import com.blotunga.bote.ui.screens.MainMenu;
import com.blotunga.bote.utils.RandUtil;
import com.blotunga.bote.utils.ShapeReader;
import com.blotunga.bote.utils.ui.HelpWidget;
import com.blotunga.bote.utils.ui.PercentageWidget;
import com.blotunga.bote.utils.ui.TextFieldWithPopup;
import com.blotunga.bote.utils.ui.ValueChangedEvent;

public class InitialSettingsMenu implements Disposable, ValueChangedEvent {
    private ScreenManager manager;
    private Skin skin;
    private TextButton backBtn;
    private TextButton contBtn;
    private Image galaxyDemo;
    private Pixmap galaxyPixmap;
    private TextureRegion galaxyTex;
    private Table galaxyShapeSelector;
    private GamePreferences oldPref;
    private Table galaxySizeSelector;
    private TextureRegion lineDrawable;
    private Table difficultyAndNeighborTable;
    private Table densitiesTable;
    private TextButtonStyle styleSmall;
    private Table victoryTable;
    private HelpWidget helpWidget;
    private Table randomSettings;

    public InitialSettingsMenu(final ScreenManager manager, Stage stage) {
        this.manager = manager;
        Major playerRace = manager.getRaceController().getPlayerRace();
        String path = "graphics/ui/" + playerRace.getPrefix() + "ui.pack";
        if (!manager.getAssetManager().isLoaded(path))
            manager.getAssetManager().load(path, TextureAtlas.class);
        manager.getAssetManager().finishLoading();
        lineDrawable = new TextureRegion(manager.getAssetManager().get(GameConstants.UI_BG_SIMPLE, Texture.class));

        manager.initSkin();
        skin = manager.getSkin();
        styleSmall = skin.get("small-buttons", TextButtonStyle.class);

        Rectangle rect = GameConstants.coordsToRelative(1150, 140, 150, 35);
        contBtn = new TextButton(StringDB.getString("BTN_NEXT", true), styleSmall);
        contBtn.setBounds(rect.x, rect.y, rect.width, rect.height);
        contBtn.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                String text = ((TextField) randomSettings.findActor("SEED")).getText().trim();
                long seed = text.isEmpty() ? 0 : Long.parseLong(text);
                RandUtil.setSeed(seed);
                for (int i = 0; i < manager.getRaceController().getMajors().size; i++) {
                    Major major = manager.getRaceController().getMajors().getValueAt(i);
                    if (major.isDisabled()) {
                        manager.getGamePreferences().achievementsEnabled = false;
                        manager.getRaceController().removeRace(major.getRaceId());
                        i--;
                    }
                }
                manager.createUniverse();
                manager.setView(ViewTypes.LOADING_SCREEN, true);
            }
        });
        stage.addActor(contBtn);

        rect = GameConstants.coordsToRelative(140, 140, 150, 35);
        backBtn = new TextButton(StringDB.getString("BTN_BACK", true), styleSmall);
        backBtn.setBounds(rect.x, rect.y, rect.width, rect.height);
        backBtn.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                if (oldPref != null) {
                    manager.setGamePreferences(new GamePreferences(oldPref));
                    oldPref = null;
                }
                ((MainMenu) manager.getScreen()).showRaceSelection();
            }
        });
        stage.addActor(backBtn);

        galaxyDemo = new Image();
        rect = GameConstants.coordsToRelative(40, 650, 420, 280);
        galaxyDemo.setBounds(rect.x, rect.y, rect.width, rect.height);

        galaxyPixmap = new Pixmap(300, 200, Format.RGBA8888);
        galaxyTex = new TextureRegion(new Texture(new PixmapTextureData(galaxyPixmap, Format.RGBA8888, false, false, true)));
        galaxyDemo.setDrawable(new TextureRegionDrawable(galaxyTex));
        manager.getDynamicTextureManager().register(galaxyTex, galaxyPixmap);
        stage.addActor(galaxyDemo);
        galaxyDemo.setVisible(false);

        int buttonWidth = (int) GameConstants.wToRelative(130);
        int buttonHeight = (int) GameConstants.hToRelative(35);
        galaxyShapeSelector = new Table();
        galaxyShapeSelector.setSkin(skin);
        rect = GameConstants.coordsToRelative(500, 660, 420, 60);
        galaxyShapeSelector.setBounds(rect.x, rect.y, rect.width, rect.height);
        float pad = GameConstants.wToRelative(50);
        galaxyShapeSelector.add(StringDB.getString("SELECT_GALAXY_SHAPE") + ":", "normalFont", Color.YELLOW).height(
                GameConstants.hToRelative(25));
        galaxyShapeSelector.add(StringDB.getString("SELECT_EXPANSION_SPEED") + ":", "normalFont", Color.YELLOW)
                .height(GameConstants.hToRelative(25)).spaceLeft((int) pad);
        galaxyShapeSelector.row();
        TextButton galaxyShapSelectBtn = new TextButton(manager.getGamePreferences().generateFromShape.getName(), styleSmall);
        galaxyShapeSelector.add(galaxyShapSelectBtn).height(buttonHeight).width(buttonWidth);
        galaxyShapSelectBtn.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                int i = manager.getGamePreferences().generateFromShape.ordinal();
                if (i < GalaxyShapes.values().length - 1)
                    i++;
                else
                    i = 0;
                manager.getGamePreferences().generateFromShape = GalaxyShapes.values()[i];
                TextButton btn = (TextButton) event.getListenerActor();
                btn.setText(manager.getGamePreferences().generateFromShape.getName());
                show();
            }
        });
        TextButton expansionSpeedBtn = new TextButton(manager.getGamePreferences().expansionSpeed.getName(), styleSmall);
        galaxyShapeSelector.add(expansionSpeedBtn).height(buttonHeight).width(buttonWidth).spaceLeft((int) pad);
        expansionSpeedBtn.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                int i = manager.getGamePreferences().expansionSpeed.ordinal();
                if (i < ExpansionSpeed.values().length - 1)
                    i++;
                else
                    i = 0;
                manager.getGamePreferences().expansionSpeed = ExpansionSpeed.values()[i];
                TextButton btn = (TextButton) event.getListenerActor();
                btn.setText(manager.getGamePreferences().expansionSpeed.getName());
            }
        });
        stage.addActor(galaxyShapeSelector);
        galaxyShapeSelector.setVisible(false);

        galaxySizeSelector = new Table();
        galaxySizeSelector.setSkin(skin);
        rect = GameConstants.coordsToRelative(500, 595, 420, 140);
        galaxySizeSelector.setBounds(rect.x, rect.y, rect.width, rect.height);
        galaxySizeSelector.align(Align.top);
        stage.addActor(galaxySizeSelector);
        galaxySizeSelector.setVisible(false);

        difficultyAndNeighborTable = new Table();
        difficultyAndNeighborTable.setSkin(skin);
        rect = GameConstants.coordsToRelative(500, 405, 420, 55);
        difficultyAndNeighborTable.setBounds(rect.x, rect.y, rect.width, rect.height);
        difficultyAndNeighborTable.add(StringDB.getString("SET_DIFFICULTY") + ":", "normalFont", Color.YELLOW).height(
                GameConstants.hToRelative(25));
        difficultyAndNeighborTable.add(StringDB.getString("SET_MINIMUM_NEIGHBORS") + ":", "normalFont", Color.YELLOW)
                .height(GameConstants.hToRelative(25)).spaceLeft((int) pad);
        difficultyAndNeighborTable.row();
        TextButton difficultySelectBtn = new TextButton(manager.getGamePreferences().difficulty.getName(), styleSmall);
        difficultyAndNeighborTable.add(difficultySelectBtn).height(buttonHeight).width(buttonWidth);
        difficultySelectBtn.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                int i = manager.getGamePreferences().difficulty.ordinal();
                if (i < Difficulties.values().length - 1)
                    i++;
                else
                    i = 0;
                manager.getGamePreferences().difficulty = Difficulties.values()[i];
                TextButton btn = (TextButton) event.getListenerActor();
                btn.setText(manager.getGamePreferences().difficulty.getName());
            }
        });
        TextButton neighborCntBtn = new TextButton("" + manager.getGamePreferences().nearbySystems, styleSmall);
        difficultyAndNeighborTable.add(neighborCntBtn).height(buttonHeight).width(buttonWidth).spaceLeft((int) pad);
        neighborCntBtn.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                int i = manager.getGamePreferences().nearbySystems;
                if (i < 8)
                    i++;
                else
                    i = 2;
                manager.getGamePreferences().nearbySystems = i;
                TextButton btn = (TextButton) event.getListenerActor();
                btn.setText("" + i);
            }
        });
        stage.addActor(difficultyAndNeighborTable);
        difficultyAndNeighborTable.setVisible(false);

        densitiesTable = new Table();
        densitiesTable.setSkin(skin);
        rect = GameConstants.coordsToRelative(960, 660, 420, 170);
        densitiesTable.setBounds(rect.x, rect.y, rect.width, rect.height);
        densitiesTable.align(Align.top);
        stage.addActor(densitiesTable);
        densitiesTable.setVisible(false);

        victoryTable = new Table();
        victoryTable.setSkin(skin);
        rect = GameConstants.coordsToRelative(420, 320, 600, 170);
        victoryTable.setBounds(rect.x, rect.y, rect.width, rect.height);
        victoryTable.align(Align.top);
        stage.addActor(victoryTable);
        victoryTable.setVisible(false);
        float textHeight = GameConstants.hToRelative(35);
        float vPad = GameConstants.hToRelative(10);
        pad = GameConstants.wToRelative(10);
        victoryTable.add(StringDB.getString("VICTORY_CONDITIONS_MENUE"), "normalFont", Color.YELLOW).height(textHeight)
                .spaceBottom((int) vPad);
        ButtonStyle bs = new ButtonStyle();
        Button b2 = new Button(bs);
        for (int i = 0; i < manager.getGamePreferences().victoryTypes.length; i++) {
            if (i % 2 == 0) {
                b2 = new Button(bs);
                victoryTable.row();
                victoryTable.add(b2).height(textHeight).spaceBottom((int) vPad);
            }
            Button button = new Button(bs);
            button.setSkin(skin);
            String text = StringDB.getString(VictoryType.values()[i].getDBName());
            button.add(text, "normalFont", Color.YELLOW).width((int) (victoryTable.getWidth() / 4 - pad));
            text = manager.getGamePreferences().victoryTypes[i] ? StringDB.getString("DISABLE", true) : StringDB.getString(
                    "ENABLE", true);
            TextButton onOffButton = new TextButton(text, styleSmall);
            onOffButton.setUserObject(VictoryType.values()[i]);
            button.add(onOffButton).spaceLeft((int) pad).width((int) (victoryTable.getWidth() / 4 - pad)).height(textHeight);
            onOffButton.addListener(new ActorGestureListener() {
                @Override
                public void tap(InputEvent event, float x, float y, int count, int button) {
                    VictoryType vt = (VictoryType) event.getListenerActor().getUserObject();
                    manager.getGamePreferences().victoryTypes[vt.ordinal()] = !manager.getGamePreferences().victoryTypes[vt
                            .ordinal()];
                    TextButton btn = (TextButton) event.getListenerActor();
                    String text = manager.getGamePreferences().victoryTypes[vt.ordinal()] ? StringDB.getString("DISABLE", true)
                            : StringDB.getString("ENABLE", true);
                    btn.setText(text);
                }
            });
            b2.add(button).spaceLeft((int) pad);
        }

        randomSettings = new Table();
        randomSettings.setSkin(skin);
        rect = GameConstants.coordsToRelative(90, 350, 320, 180);
        randomSettings.setBounds(rect.x, rect.y, rect.width, rect.height);
        randomSettings.align(Align.top);
        stage.addActor(randomSettings);
        randomSettings.setVisible(false);
        Label l = new Label(StringDB.getString("RANDOM_SEED"), skin, "normalFont", Color.YELLOW);
        l.setAlignment(Align.center);
        randomSettings.add(l);
        randomSettings.row();

        Color normalColor = manager.getRaceController().getPlayerRace().getRaceDesign().clrNormalText;
        TextFieldWithPopup tf = new TextFieldWithPopup("", skin, (Texture) manager.getAssetManager().get(GameConstants.UI_BG_ROUNDED), normalColor);
        tf.setName("SEED");
        tf.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
        tf.setMaxLength(18); //long has support for 19 digits, but 18 ought to be enough
        tf.setText("0");
        tf.getStyle().font = skin.getFont("normalFont");
        randomSettings.add(tf).width(randomSettings.getWidth() / 2).spaceBottom((int) vPad);
        randomSettings.row();
        l = new Label(StringDB.getString("SAVE_SEED"), skin, "normalFont", Color.YELLOW);
        l.setAlignment(Align.center);
        randomSettings.add(l);
        randomSettings.row();
        String text = manager.shouldSaveSeed() ? StringDB.getString("DISABLE", true) : StringDB.getString("ENABLE", true);
        TextButton onOffButton = new TextButton(text, styleSmall);
        onOffButton.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                TextButton btn = (TextButton) event.getListenerActor();
                manager.setSaveSeed(!manager.shouldSaveSeed());
                String text = manager.shouldSaveSeed() ? StringDB.getString("DISABLE", true) : StringDB.getString("ENABLE", true);
                btn.setText(text);
            }
        });
        randomSettings.add(onOffButton).spaceLeft((int) pad).width((int) (randomSettings.getWidth() / 2 - pad))
                .height(textHeight);

        rect = GameConstants.coordsToRelative(0, 42, 42, 42);
        helpWidget = new HelpWidget(manager, rect, (DefaultScreen) manager.getScreen(), stage);
        helpWidget.hide();
    }

    public void show(boolean redrawGalaxy) {
        oldPref = new GamePreferences(manager.getGamePreferences());
        contBtn.setVisible(true);
        backBtn.setVisible(true);
        galaxyDemo.setVisible(true);
        galaxyShapeSelector.setVisible(true);
        difficultyAndNeighborTable.setVisible(true);
        drawGalaxySizeSelector();
        if (redrawGalaxy)
            drawGalaxy();
        drawDensitiesTable();
        victoryTable.setVisible(true);
        randomSettings.setVisible(true);
        helpWidget.show();
    }

    public void show() {
        show(true);
    }

    public void hide() {
        helpWidget.hide();
        contBtn.setVisible(false);
        backBtn.setVisible(false);
        galaxyDemo.setVisible(false);
        galaxyShapeSelector.setVisible(false);
        galaxySizeSelector.setVisible(false);
        difficultyAndNeighborTable.setVisible(false);
        densitiesTable.setVisible(false);
        victoryTable.setVisible(false);
        randomSettings.setVisible(false);
    }

    private void drawDensitiesTable() {
        densitiesTable.clear();
        densitiesTable.add(
                StringDB.getString("SET_MINOR_DENSITY") + " (" + StringDB.getString("CURRENT") + ": "
                        + manager.getGamePreferences().minorDensity + ")", "normalFont", Color.YELLOW).height(
                GameConstants.hToRelative(25));
        densitiesTable.row();
        float widgetHeight = GameConstants.hToRelative(25);
        float widgetPad = GameConstants.wToRelative(3);
        float widgetWidth = GameConstants.wToRelative(12);
        PercentageWidget widget = new PercentageWidget(3, skin, widgetWidth, widgetHeight, widgetPad, 20, 0, 100,
                manager.getGamePreferences().minorDensity, lineDrawable, this);
        densitiesTable.add(widget.getWidget()).height((int) (widgetHeight * 1.5f));
        densitiesTable.row();
        densitiesTable.add(
                StringDB.getString("SET_ANOMALY_DENSITY") + " (" + StringDB.getString("CURRENT") + ": "
                        + manager.getGamePreferences().anomalyDensity + ")", "normalFont", Color.YELLOW).height(
                GameConstants.hToRelative(25));
        densitiesTable.row();
        widget = new PercentageWidget(4, skin, widgetWidth, widgetHeight, widgetPad, 20, 0, 100,
                manager.getGamePreferences().anomalyDensity, lineDrawable, this);
        densitiesTable.add(widget.getWidget()).height((int) (widgetHeight * 1.5f));
        densitiesTable.row();
        densitiesTable.add(
                StringDB.getString("SET_ALIENS_FREQ") + " (" + StringDB.getString("CURRENT") + ": "
                        + manager.getGamePreferences().alienFrequency + ")", "normalFont", Color.YELLOW).height(
                GameConstants.hToRelative(25));
        densitiesTable.row();
        widget = new PercentageWidget(5, skin, widgetWidth, widgetHeight, widgetPad, 20, 0, 60,
                manager.getGamePreferences().alienFrequency, lineDrawable, this);
        densitiesTable.add(widget.getWidget()).height((int) (widgetHeight * 1.5f));
        densitiesTable.row();
        densitiesTable.add(StringDB.getString("ENABLE_RANDOM"), "normalFont", Color.YELLOW).height(GameConstants.hToRelative(25))
                .spaceTop(GameConstants.hToRelative(5));
        densitiesTable.row();
        String text = manager.getGamePreferences().randomIsActivated ? StringDB.getString("DISABLE", true) : StringDB.getString(
                "ENABLE", true);
        TextButton randomOnBtn = new TextButton(text, styleSmall);
        densitiesTable.add(randomOnBtn).height((int) GameConstants.hToRelative(35)).width((int) GameConstants.wToRelative(130));
        randomOnBtn.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                manager.getGamePreferences().randomIsActivated = !manager.getGamePreferences().randomIsActivated;
                TextButton btn = (TextButton) event.getListenerActor();
                String text = manager.getGamePreferences().randomIsActivated ? StringDB.getString("DISABLE", true) : StringDB
                        .getString("ENABLE", true);
                btn.setText(text);
            }
        });
        densitiesTable.row();
        densitiesTable.add(StringDB.getString("SET_MAJORS_JOINING") + ":", "normalFont", Color.YELLOW)
                .height(GameConstants.hToRelative(25)).spaceTop(GameConstants.hToRelative(5));
        densitiesTable.row();
        TextButton majorJoinValue = new TextButton("" + manager.getGamePreferences().majorJoinTimesShouldOccur, styleSmall);
        densitiesTable.add(majorJoinValue).height((int) GameConstants.hToRelative(35))
                .width((int) GameConstants.wToRelative(130));
        majorJoinValue.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                manager.getGamePreferences().majorJoinTimesShouldOccur++;
                if (manager.getGamePreferences().majorJoinTimesShouldOccur > manager.getRaceController().getMajors().size - 2)
                    manager.getGamePreferences().majorJoinTimesShouldOccur = 0;
                TextButton btn = (TextButton) event.getListenerActor();
                btn.setText("" + manager.getGamePreferences().majorJoinTimesShouldOccur);
            }
        });
        densitiesTable.setVisible(true);
    }

    private void drawGalaxySizeSelector() {
        galaxySizeSelector.clear();
        galaxySizeSelector.add(
                StringDB.getString("SET_GALAXY_WIDTH") + " (" + StringDB.getString("CURRENT") + ": " + manager.getGridSizeX()
                        + ")", "normalFont", Color.YELLOW).height(GameConstants.hToRelative(25));
        galaxySizeSelector.row();
        float widgetHeight = GameConstants.hToRelative(25);
        float widgetPad = GameConstants.wToRelative(3);
        float widgetWidth = GameConstants.wToRelative(9);
        int maxSize = (Gdx.app.getType() == ApplicationType.Desktop) ? 70 : 40;
        PercentageWidget sizeWidget = new PercentageWidget(0, skin, widgetWidth, widgetHeight, widgetPad, 30, 10, maxSize,
                manager.getGamePreferences().gridSizeX, lineDrawable, this);
        galaxySizeSelector.add(sizeWidget.getWidget()).height((int) (widgetHeight * 1.5f));
        galaxySizeSelector.row();
        galaxySizeSelector.add(
                StringDB.getString("SET_GALAXY_HEIGHT") + " (" + StringDB.getString("CURRENT") + ": " + manager.getGridSizeY()
                        + ")", "normalFont", Color.YELLOW).height(GameConstants.hToRelative(25));
        galaxySizeSelector.row();
        sizeWidget = new PercentageWidget(1, skin, widgetWidth, widgetHeight, widgetPad, 30, 10, maxSize,
                manager.getGamePreferences().gridSizeY, lineDrawable, this);
        galaxySizeSelector.add(sizeWidget.getWidget()).height((int) (widgetHeight * 1.5f));
        galaxySizeSelector.row();
        galaxySizeSelector.add(
                StringDB.getString("SET_STAR_DENSITY") + " (" + StringDB.getString("CURRENT") + ": "
                        + manager.getGamePreferences().starDensity + ")", "normalFont", Color.YELLOW).height(
                GameConstants.hToRelative(25));
        galaxySizeSelector.row();
        sizeWidget = new PercentageWidget(2, skin, widgetWidth, widgetHeight, widgetPad, 30, 1, 100,
                manager.getGamePreferences().starDensity, lineDrawable, this);
        galaxySizeSelector.add(sizeWidget.getWidget()).height((int) (widgetHeight * 1.5f));
        galaxySizeSelector.setVisible(true);
    }

    private void drawGalaxy() {
        int[][] shape = new int[manager.getGridSizeX()][manager.getGridSizeY()];
        if (!manager.getGamePreferences().generateFromShape.getImage().isEmpty()) {
            shape = ShapeReader.getShapeFromFile("data/galaxy/" + manager.getGamePreferences().generateFromShape.getImage()
                    + ".png", manager);
        } else
            for (int j = 0; j < manager.getGridSizeY(); j++)
                for (int i = 0; i < manager.getGridSizeX(); i++)
                    shape[i][j] = 1;

        int density = manager.getGamePreferences().starDensity;
        if (!manager.getGamePreferences().generateFromShape.getImage().isEmpty())
            density = Math.min(density * 2, 100);
        int multiX = (int) (GameConstants.wToRelative(10) * 30 / manager.getGridSizeX());
        int multiY = (int) (GameConstants.hToRelative(10) * 20 / manager.getGridSizeY());
        int radius = Math.min(multiX, multiY) / 4;
        manager.getDynamicTextureManager().unregisterAndDispose(galaxyTex);
        galaxyPixmap = new Pixmap(manager.getGridSizeX() * multiX, manager.getGridSizeY() * multiY, Format.RGBA8888);
        galaxyPixmap.setFilter(Filter.BiLinear);
        galaxyPixmap.setColor(new Color(0.0f, 0.0f, 0.0f, 1.0f));
        galaxyPixmap.fill();

        Color colors[] = { Color.ORANGE, Color.RED, Color.WHITE, Color.YELLOW };
        for (int y = 0; y < manager.getGridSizeY(); y++)
            for (int x = 0; x < manager.getGridSizeX(); x++)
                if ((int) (MathUtils.random() * 100) >= (100 - density) && shape[x][y] == 1) {
                    galaxyPixmap.setColor(colors[(int) (MathUtils.random() * colors.length)]);
                    galaxyPixmap.fillCircle(x * multiX + multiX / 2, y * multiY + multiY / 2, radius);
                }
        galaxyTex = new TextureRegion(new Texture(new PixmapTextureData(galaxyPixmap, Format.RGBA8888, false, false, true)));
        galaxyDemo.setDrawable(new TextureRegionDrawable(galaxyTex));
        manager.getDynamicTextureManager().register(galaxyTex, galaxyPixmap);
    }

    @Override
    public void dispose() {
        helpWidget.dispose();
        manager.getDynamicTextureManager().unregisterAndDispose(galaxyTex);
        manager.clearSkin();
        String path = "graphics/ui/" + manager.getRaceController().getPlayerRace().getPrefix() + "ui.pack";
        if (manager.getAssetManager().isLoaded(path))
            manager.getAssetManager().unload(path);
    }

    @Override
    public void valueChanged(int typeID, int value) {
        switch (typeID) {
            case 0://grid size X
                if (manager.getGamePreferences().gridSizeX != value) {
                    manager.getGamePreferences().gridSizeX = value;
                    show();
                }
                break;
            case 1://grid size Y
                if (manager.getGamePreferences().gridSizeY != value) {
                    manager.getGamePreferences().gridSizeY = value;
                    show();
                }
                break;
            case 2://star density
                if (manager.getGamePreferences().starDensity != value) {
                    manager.getGamePreferences().starDensity = value;
                    show();
                }
                break;
            case 3: //minor density
                if (manager.getGamePreferences().minorDensity != value) {
                    manager.getGamePreferences().minorDensity = value;
                    show(false);
                }
                break;
            case 4: //anomaly density
                if (manager.getGamePreferences().anomalyDensity != value) {
                    manager.getGamePreferences().anomalyDensity = value;
                    show(false);
                }
                break;
            case 5: //alien frequency
                if (manager.getGamePreferences().alienFrequency != value) {
                    manager.getGamePreferences().alienFrequency = value;
                    show(false);
                }
                break;
        }
    }
}
