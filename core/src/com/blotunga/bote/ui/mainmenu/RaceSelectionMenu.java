/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.mainmenu;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Disposable;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.SndMgrValue;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.ui.screens.MainMenu;

public class RaceSelectionMenu implements Disposable {
    private ScreenManager manager;
    private Skin skin;
    private Image raceImage;
    private Image raceSymbol;
    private Table table;
    private TextButton backBtn;
    private TextButton contBtn;
    private Table raceDescriptionTable;
    private ScrollPane selectScroller;

    public RaceSelectionMenu(final ScreenManager manager, Stage stage, Skin skin) {
        this.manager = manager;
        this.skin = skin;

        table = new Table();
        Rectangle rect = GameConstants.coordsToRelative(30, 650, 360, 600);
        table.setBounds(rect.x, rect.y, rect.width, rect.height);
        table.align(Align.topLeft);
        selectScroller = new ScrollPane(table, skin);
        selectScroller.setVariableSizeKnobs(false);
        selectScroller.setScrollingDisabled(true, false);
        selectScroller.setBounds(rect.x, rect.y, rect.width, rect.height);
        stage.addActor(selectScroller);

        raceImage = new Image();
        rect = GameConstants.coordsToRelative(400, 555, 300, 300);
        raceImage.setBounds(rect.x, rect.y, rect.width, rect.height);
        raceImage.setVisible(false);
        raceSymbol = new Image();
        rect = GameConstants.coordsToRelative(500, 660, 100, 100);
        raceSymbol.setBounds(rect.x, rect.y, rect.width, rect.height);
        raceSymbol.setVisible(false);
        rect = GameConstants.coordsToRelative(740, 605, 660, 420);

        SpriteDrawable dr = manager.getScreen().getTintedDrawable(GameConstants.UI_BG_SIMPLE,
                                                                  new Color(0.1f, 0.1f, 0.1f, 0.5f));

        raceDescriptionTable = new Table();
        raceDescriptionTable.setBackground(dr);
        raceDescriptionTable.setBounds(rect.x, rect.y, rect.width, rect.height);
        raceDescriptionTable.setVisible(false);

        stage.addActor(raceImage);
        stage.addActor(raceSymbol);
        stage.addActor(raceDescriptionTable);

        backBtn = new TextButton(StringDB.getString("BTN_BACK", true), skin);
        backBtn.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                ((MainMenu) manager.getScreen()).showMainMenu();
            }
        });

        rect = GameConstants.coordsToRelative(425, 240, 250, 55);
        contBtn = new TextButton(StringDB.getString("BTN_NEXT", true), skin);
        contBtn.setBounds(rect.x, rect.y, rect.width, rect.height);
        contBtn.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                ((MainMenu) manager.getScreen()).showInitialSettingsMenu();
            }
        });
        contBtn.setVisible(false);
        stage.addActor(contBtn);
    }

    public void show() {
        backBtn.setVisible(true);
        table.clear();
        for (int i = 0; i < manager.getRaceController().getMajors().size; i++) {
            Major race = manager.getRaceController().getMajors().getValueAt(i);
            TextButtonStyle style = new TextButtonStyle(skin.get("default", TextButtonStyle.class));
            style.checkedFontColor = Color.BLACK;
            style.disabledFontColor = Color.RED;
            TextButton rb = new TextButton(race.getEmpireName(), style);
            rb.setName(race.getRaceId());
            rb.setUserObject(race.getRaceId());
            rb.align(Align.left);
            rb.setDisabled(race.isDisabled());
            skin.setEnabled(rb, !race.isDisabled());
            rb.addListener(new ActorGestureListener() {
                @Override
                public void tap(InputEvent event, float x, float y, int count, int button) {
                    TextButton btn = (TextButton) event.getListenerActor();
                    if (button == 0 && !btn.isDisabled()) {
                        String selectedRace = (String) btn.getUserObject();
                        String old = manager.getRaceController().getPlayerRaceString();

                        if (btn.isChecked()) {
                            manager.getRaceController().setPlayerRace(selectedRace);
                            Major race = manager.getRaceController().getPlayerRace();
                            raceImage.setVisible(true);
                            String path = "graphics/races/" + race.getGraphicFileName() + ".jpg";
                            raceImage.setDrawable(new TextureRegionDrawable(new TextureRegion(manager.loadTextureImmediate(path))));
                            raceSymbol.setDrawable(new TextureRegionDrawable(manager.getSymbolTextures(race.getRaceId())));
                            manager.getSoundManager().playSound(SndMgrValue.SNDMGR_MSG_RACESELECT, race.getPrefix());

                            raceDescriptionTable.clear();
                            float vPad = GameConstants.hToRelative(10);
                            float hPad = GameConstants.wToRelative(10);
                            LabelStyle lstyle = new Label.LabelStyle();
                            lstyle.font = manager.getScaledFont();
                            Label raceDescription = new Label(race.getDescription(), lstyle);
                            raceDescription.setWrap(true);
                            raceSymbol.setVisible(true);
                            raceDescriptionTable.add(raceDescription).space(vPad, hPad, vPad, hPad)
                                    .width(raceDescriptionTable.getWidth() - hPad * 2);
                            raceDescriptionTable.setVisible(true);
                            contBtn.setVisible(true);
                        } else if (manager.getRaceController().getPlayerRaceString().equals(btn.getName())) {
                            raceImage.setVisible(false);
                            raceDescriptionTable.setVisible(false);
                            raceSymbol.setVisible(false);
                            contBtn.setVisible(false);
                            selectedRace = "";
                            manager.getRaceController().setPlayerRace(selectedRace);
                        }

                        TextButton tb = table.findActor(old);
                        if (tb != null && tb.isChecked() && !selectedRace.equals(old)) { // player race is initialized to MAJOR1 so it can happen
                            tb.toggle();
                        }
                    } else if (button == 1){
                        disableRace(btn, !btn.isDisabled());
                    }
                }

                @Override
                public boolean longPress (Actor actor, float x, float y) {
                    TextButton btn = (TextButton) actor;
                    disableRace(btn, !btn.isDisabled());
                    return false;
                }
            });
            table.add(rb).width(GameConstants.wToRelative(340)).height(GameConstants.wToRelative(55))
                    .pad(GameConstants.hToRelative(16));
            table.row();
        }
        table.add(backBtn).width(GameConstants.wToRelative(250)).height(GameConstants.wToRelative(55))
                .pad(GameConstants.hToRelative(16));
        table.setVisible(true);
        selectScroller.setVisible(true);
    }

    private void disableRace(TextButton btn, boolean disable) {
        btn.setDisabled(disable);
        skin.setEnabled(btn, !disable);
        String id = (String) btn.getUserObject();
        manager.getRaceController().getMajors().get(id).setDisabled(disable);
        if (manager.getRaceController().getPlayerRaceString().equals(id)) {
            raceImage.setVisible(false);
            raceDescriptionTable.setVisible(false);
            raceSymbol.setVisible(false);
            contBtn.setVisible(false);
            btn.setChecked(false);
            manager.getRaceController().setPlayerRace("");
        }
    }

    public void hide() {
        if(raceImage.isVisible()) {
            String path = "graphics/races/" + manager.getRaceController().getPlayerRace().getGraphicFileName() + ".jpg";
            if(manager.getAssetManager().isLoaded(path))
                manager.getAssetManager().unload(path);
        }
        table.setVisible(false);
        raceImage.setVisible(false);
        raceSymbol.setVisible(false);
        raceDescriptionTable.setVisible(false);
        backBtn.setVisible(false);
        contBtn.setVisible(false);
        selectScroller.setVisible(false);
    }

    @Override
    public void dispose() {
    }
}
