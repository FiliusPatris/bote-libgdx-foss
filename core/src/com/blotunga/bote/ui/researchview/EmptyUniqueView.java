/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.researchview;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.ResearchComplexType;
import com.blotunga.bote.constants.ResearchStatus;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.races.ResearchComplex;
import com.blotunga.bote.races.ResearchInfo;

public class EmptyUniqueView {
    private Table nameTable;
    private Table infoTable;
    private Color normalColor;
    private Color markColor;
    private Major playerRace;

    public EmptyUniqueView(ScreenManager manager, Stage stage, Skin skin, float xOffset, float yOffset) {
        playerRace = manager.getRaceController().getPlayerRace();
        normalColor = playerRace.getRaceDesign().clrNormalText;
        markColor = playerRace.getRaceDesign().clrListMarkTextColor;

        nameTable = new Table();
        Rectangle rect = GameConstants.coordsToRelative(400, 800, 400, 40);
        nameTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        nameTable.align(Align.center);
        nameTable.setSkin(skin);
        stage.addActor(nameTable);
        nameTable.add(StringDB.getString("SPECIAL_RESEARCH"), "hugeFont", normalColor);
        nameTable.setVisible(false);

        infoTable = new Table();
        rect = GameConstants.coordsToRelative(165, 550, 580, 445);
        infoTable.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        infoTable.align(Align.center);
        infoTable.setSkin(skin);
        stage.addActor(infoTable);
        infoTable.setVisible(false);
    }

    public void show() {
        nameTable.setVisible(true);
        infoTable.clear();
        infoTable.add(StringDB.getString("NO_SPECIAL_RESEARCH_AVAILABLE"), "hugeFont", normalColor).height(
                GameConstants.hToRelative(70));
        infoTable.row();
        infoTable.add(StringDB.getString("RESEARCHED_SPECIALTECHS"), "largeFont", markColor).height(
                GameConstants.hToRelative(40));
        int count = 0;
        for (int i = 0; i < ResearchComplexType.COMPLEX_COUNT.getType(); i++) {
            ResearchComplexType type = ResearchComplexType.fromInt(i);
            ResearchInfo info = playerRace.getEmpire().getResearch().getResearchInfo();
            ResearchComplex complex = info.getResearchComplex(type);
            if (complex.getComplexStatus() == ResearchStatus.RESEARCHED) {
                for (int j = 1; j <= 3; j++) {
                    if (complex.getFieldStatus(j) == ResearchStatus.RESEARCHED) {
                        infoTable.row();
                        String text = String.format("%s: %s", complex.getComplexName(), complex.getFieldName(j));
                        infoTable.add(text, "normalFont", normalColor);
                        count++;
                        break;
                    }
                }
            }
        }
        if (count == 0) {
            infoTable.row();
            String text = StringDB.getString("NONE");
            infoTable.add(text, "normalFont", normalColor);
        }
        infoTable.setVisible(true);
    }

    public void hide() {
        nameTable.setVisible(false);
        infoTable.setVisible(false);
    }
}
