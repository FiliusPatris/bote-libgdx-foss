/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.researchview;

import com.badlogic.gdx.utils.IntMap;

public enum ResearchButtonType {
    NORMAL_BUTTON("BTN_NORMAL", "researchmenu", 0),
    SPECIAL_BUTTON("BTN_SPECIAL", "urmenu", 1),
    SHIPDESIGN_BUTTON("BTN_SHIPDESIGN", "designmenu", 2),
    DATABASE_BUTTON("BTN_DATABASE", "demomenu", 3);

    private String label;
    private String bgImage;
    private int ord;
    private static final IntMap<ResearchButtonType> intToTypeMap = new IntMap<ResearchButtonType>();

    ResearchButtonType(String label, String bgImage, int ord) {
        this.label = label;
        this.bgImage = bgImage;
        this.ord = ord;
    }

    static {
        for (ResearchButtonType rt : ResearchButtonType.values()) {
            intToTypeMap.put(rt.ord, rt);
        }
    }

    public String getLabel() {
        return label;
    }

    public String getBgImage() {
        return bgImage;
    }

    public int getId() {
        return ord;
    }

    public static ResearchButtonType fromInt(int i) {
        ResearchButtonType rt = intToTypeMap.get(i);
        return rt;
    }
}
