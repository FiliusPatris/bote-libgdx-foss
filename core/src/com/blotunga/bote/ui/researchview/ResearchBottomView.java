/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.researchview;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.ResearchType;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.races.Research;

public class ResearchBottomView {
    private ScreenManager manager;
    private Skin skin;
    private Major playerRace;
    private Color normalColor;
    private Color markColor;
    private Table bottomTable;

    public ResearchBottomView(ScreenManager manager, Stage stage, Skin skin, float xOffset) {
        this.manager = manager;
        this.skin = skin;
        playerRace = manager.getRaceController().getPlayerRace();
        normalColor = playerRace.getRaceDesign().clrNormalText;
        markColor = playerRace.getRaceDesign().clrListMarkTextColor;

        bottomTable = new Table();
        bottomTable.align(Align.left);
        Rectangle rect = GameConstants.coordsToRelative(40, 170, 1115, 150);
        bottomTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        stage.addActor(bottomTable);
        bottomTable.setVisible(false);
    }

    public void show(ResearchType type) {
        Research research = playerRace.getEmpire().getResearch();
        bottomTable.clear();
        Table info = new Table();
        String text;
        if (type == ResearchType.UNIQUE)
            text = research.getResearchInfo().getCurrentResearchComplex().getComplexName();
        else
            text = research.getResearchInfo().getTechName(type.getType());
        Label head = new Label(text, skin, "largeFont", markColor);
        info.add(head).align(Align.center);
        info.row();
        if (type == ResearchType.UNIQUE)
            text = research.getResearchInfo().getCurrentResearchComplex().getComplexDescription();
        else
            text = research.getResearchInfo().getTechDescription(type.getType());
        float imgWidth = bottomTable.getHeight() * 1.2f;
        Label txt = new Label(text, skin, "mediumFont", normalColor);
        txt.setWrap(true);
        info.add(txt).align(Align.left).width(bottomTable.getWidth() - imgWidth);
        bottomTable.add(info);
        String path = "graphics/research/" + type.getImgName() + ".png";
        Image image = new Image(new TextureRegionDrawable(new TextureRegion(manager.loadTextureImmediate(path))));
        bottomTable.add(image).width(imgWidth).height(bottomTable.getHeight());
        bottomTable.setVisible(true);
    }

    public void hide() {
        bottomTable.setVisible(false);
    }
}
