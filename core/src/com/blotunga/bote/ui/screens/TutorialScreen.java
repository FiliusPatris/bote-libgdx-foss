/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.screens;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.utils.Align;
import com.blotunga.bote.DefaultScreen;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;

public class TutorialScreen extends ZoomableScreen {
    private ScreenManager manager;
    private Table nameTable;
    private Table textTable;
    private ScrollPane textScroller;
    private Color normalColor;
    private TextButton okButton;

    public TutorialScreen(ScreenManager screenManager) {
        super(screenManager, "top5menu", "");
        float xOffset = sidebarLeft.getPosition().width;
        Major playerRace = screenManager.getRaceController().getPlayerRace();
        normalColor = playerRace.getRaceDesign().clrNormalText;
        this.manager = screenManager;

        nameTable = new Table();
        Rectangle rect = GameConstants.coordsToRelative(200, 790, 800, 40);
        nameTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        nameTable.align(Align.center);
        nameTable.setSkin(skin);
        mainStage.addActor(nameTable);
        String s = StringDB.getString("TUTORIAL");
        nameTable.add(s, "hugeFont", normalColor);
        nameTable.setVisible(false);

        textTable = new Table();
        textScroller = new ScrollPane(textTable, skin);
        textScroller.setVariableSizeKnobs(false);
        textScroller.setFadeScrollBars(false);
        textScroller.setScrollingDisabled(true, false);
        textTable.setTouchable(Touchable.disabled);
        rect = GameConstants.coordsToRelative(100, 650, 1000, 500);
        textTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        textScroller.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        textTable.align(Align.top);
        textTable.setSkin(skin);
        mainStage.addActor(textScroller);
        textScroller.setVisible(false);

        TextButtonStyle style = skin.get("default", TextButtonStyle.class);
        okButton = new TextButton(StringDB.getString("BTN_OKAY"), style);
        rect = GameConstants.coordsToRelative(550, 60, 150, 35);
        okButton.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        mainStage.addActor(okButton);

        okButton.addListener(new InputListener() {
                @Override
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    boolean universe = manager.getPreviousScreen() instanceof UniverseRenderer;
                    boolean mainmenu = manager.getPreviousScreen() instanceof MainMenu;
                    boolean showShips = false;
                    if(universe)
                        showShips = !manager.getUniverseMap().getRenderer().getRightSideBar().isStarSystemShown();
                    manager.showScreen(manager.getPreviousScreen().getClass());
                    if(universe && showShips)
                        manager.getUniverseMap().getRenderer().showShips();
                    else if(manager.getUniverseMap() != null && manager.getUniverseMap().getRenderer() != null)
                        manager.getUniverseMap().getRenderer().showPlanets();
                    if(mainmenu)
                        ((MainMenu) manager.getScreen()).showInitialSettingsMenu();
                    return false;
                }
        });
    }

    @Override
    public void show() {
        super.show();
        nameTable.setVisible(true);
        textTable.clear();
        String text = "";
        DefaultScreen previous = manager.getPreviousScreen();
        if (previous instanceof UniverseRenderer) {
            text = manager.getUniverseMap().getRenderer().getRightSideBar().isStarSystemShown() ?
                        StringDB.getString("TGALAXYMAP") :
                        StringDB.getString("TSHIPORDER");
        } else if (previous instanceof SystemBuildScreen)
            text = StringDB.getString("TSYSTEMBUILDMENU");
        else if (previous instanceof ResearchScreen)
            text = StringDB.getString("TRESEARCHMENU");
        else if (previous instanceof DiplomacyScreen)
            text = StringDB.getString("TDIPLOMACYMENU");
        else if (previous instanceof EmpireScreen)
            text = StringDB.getString("TEMPIREMENU");
        else if (previous instanceof IntelScreen)
            text = StringDB.getString("TINTELMENU");
        else if(previous instanceof ShipDesignScreen)
            text = StringDB.getString("TSHIPDESIGN");
        else if(previous instanceof MainMenu)
            text = StringDB.getString("TINITIALSETTINGS");

        Label l = new Label(text, skin, "xlFont", normalColor);
        l.setWrap(true);
        textTable.add(l).width(textScroller.getWidth() - textScroller.getStyle().vScrollKnob.getMinWidth());
        textScroller.setVisible(true);
        okButton.setVisible(true);
    }

    @Override
    public void hide() {
        okButton.setVisible(false);
        nameTable.setVisible(false);
        textScroller.setVisible(false);
        super.hide();
    }
}
