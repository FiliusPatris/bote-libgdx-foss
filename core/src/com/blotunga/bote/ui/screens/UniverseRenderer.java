/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.screens;

import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.PixmapIO;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFontCache;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.PixmapTextureData;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.maps.MapLayers;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.maps.tiled.tiles.StaticTiledMapTile;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.tests.utils.OrthoCamController;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.ObjectMap.Entry;
import com.badlogic.gdx.utils.ObjectSet;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ScalingViewport;
import com.blotunga.bote.DefaultScreen;
import com.blotunga.bote.GamePreferences;
import com.blotunga.bote.GameSettings.GalaxyShowState;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.DiplomaticAgreement;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.ResourceTypes;
import com.blotunga.bote.constants.ScanPower;
import com.blotunga.bote.constants.ShipOrder;
import com.blotunga.bote.constants.ShipRange;
import com.blotunga.bote.constants.SndMgrValue;
import com.blotunga.bote.constants.ViewTypes;
import com.blotunga.bote.galaxy.ResourceRoute;
import com.blotunga.bote.galaxy.Sector;
import com.blotunga.bote.galaxy.UniverseMap;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.races.Minor;
import com.blotunga.bote.races.Race;
import com.blotunga.bote.races.ResearchInfo;
import com.blotunga.bote.races.starmap.StarMap;
import com.blotunga.bote.ships.Combat;
import com.blotunga.bote.ships.ShipMap;
import com.blotunga.bote.ships.Ships;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.trade.TradeRoute;
import com.blotunga.bote.ui.LeftSideBar;
import com.blotunga.bote.ui.UISidebar;
import com.blotunga.bote.ui.universemap.RightSideBar;
import com.blotunga.bote.ui.universemap.ShipRenderer;
import com.blotunga.bote.utils.IntPoint;
import com.blotunga.bote.utils.Pair;
import com.blotunga.bote.utils.ui.GestureCameraController;
import com.blotunga.bote.utils.ui.UniverseTooltip;
import com.blotunga.bote.utils.ui.GestureCameraController.GestureEvent;

public class UniverseRenderer extends DefaultScreen implements GestureEvent {
    public enum RouteType {
        NONE, TRADE, RESOURCE
    }

    public enum GalaxyState {
        NORMAL(Color.BLUE),
        MOVE(Color.RED),
        SET_ROUTE(Color.ORANGE),
        SET_BUILD_TARGET(Color.GOLD),
        TERRAFORM(Color.RED);

        Color color;

        GalaxyState(Color color) {
            this.color = color;
        }
    }

    private GalaxyState state;
    private TiledMap map;
    private TiledMapRenderer renderer;
    private OrthographicCamera camera;
    private OrthographicCamera uicamera;
    private OrthographicCamera uiZoomCamera;
    private GestureCameraController cameraControllerGesture;
    private OrthoCamController cameraControllerOrtho;
    private GestureCameraController cameraControllerGestureUI;
    private OrthoCamController cameraControllerOrthoUI;
    private InputMultiplexer inputs;
    private BitmapFont font;
    private BitmapFontCache cache;
    private SpriteBatch batchfixed;
    private RightSideBar sidebarRight;
    //selection stuff
    private Group selectStack;
    private TextureRegion selectTexture;
    private Color selectColor;
    private Image selectImage;
    private Label selectDistLabel;
    private Label winningChanceLabel;

    private Stage universeStage;
    private Texture universeBackgroundTexture;
    private Sprite universeBackground;
    private boolean needstomove;
    private IntPoint oldPosition;
    private TextureRegion gridTex;
    private TextureAtlas atlas;
    private ObjectMap<String, TextureRegion> ownerMark;
    private Array<Image> sunImgs;
    private Array<Image> rangeLines;
    private int oldShipRange;
    private IntPoint target;
    private IntPoint oldPos;
    private IntPoint origin;
    private RouteType drawRoute;
    private ResourceTypes resourceType;
    private Image currentRoute;
    private Array<Image> resRoutes;
    private UniverseMap universeMap;

    private Image lockImageLeft;
    private Image lockImageRight;
    private TextureRegion lockTexture;
    private TextureRegion unlockTexture;
    private TextureRegion blackTex;
    private int renderCnt = 0;

    public UniverseRenderer(final ScreenManager screenManager) {
        super(screenManager);
        universeMap = screenManager.getUniverseMap();
        universeMap.setRetRenderer(this);

        drawRoute = RouteType.NONE;
        state = GalaxyState.NORMAL;
        resourceType = ResourceTypes.TITAN;
        needstomove = false;
        target = new IntPoint();
        oldPos = new IntPoint();
        origin = new IntPoint();
        resRoutes = new Array<Image>();

        universeStage = new Stage(new ScalingViewport(Scaling.fit, GamePreferences.sceneWidth, GamePreferences.sceneHeight));
        uicamera = new OrthographicCamera();
        screenManager.initSideBar(uicamera); //has to be initialized before the right one to load the skin also
        sidebarRight = new RightSideBar(screenManager, this, uicamera);
        uicamera.update();

        uiZoomCamera = new OrthographicCamera();
        uiZoomCamera.update();
        cameraControllerGestureUI = new GestureCameraController(uiZoomCamera, 0.4f, 1.0f);
        cameraControllerGestureUI.setEnabled(false);
        cameraControllerOrthoUI = new OrthoCamController(uiZoomCamera, 0.4f, 0.8f);
        cameraControllerOrthoUI.setEnabled(false);

        camera = new OrthographicCamera();
        camera.update();

        cameraControllerGesture = new GestureCameraController(camera);
        cameraControllerGesture.setGestureHandler(this);
        cameraControllerOrtho = new OrthoCamController(camera);
        cameraControllerOrtho.setZoomHandler(this);
        inputs = new InputMultiplexer();
        inputs.addProcessor(new GestureDetector(cameraControllerGestureUI));
        inputs.addProcessor(cameraControllerOrthoUI);
        inputs.addProcessor(screenManager.getSidebarLeft().getStage());
        inputs.addProcessor(sidebarRight.getStage());
        inputs.addProcessor(new GestureDetector(cameraControllerGesture));
        inputs.addProcessor(cameraControllerOrtho);
        inputs.addProcessor(universeStage);

        setInputProcessor(inputs);

        batchfixed = new SpriteBatch();

        universeBackgroundTexture = screenManager.getAssetManager().get(
                "graphics/galaxy/" + game.getGameSettings().backgroundMod
                        + getResourceManager().getRaceController().getPlayerRace().getPrefix() + "galaxy.jpg");
        universeBackground = new Sprite(universeBackgroundTexture);
        universeBackground.setBounds(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        String fontName = getResourceManager().getRaceController().getPlayerRace().getRaceDesign().fontName;
        font = screenManager.getRacialFont(fontName, 20);

        atlas = game.getAssetManager().get("graphics/star/StarMaps.pack");
        game.initSunPixmaps(atlas);

        // generate random galaxy
        cache = new BitmapFontCache(font, true);
        map = new TiledMap();

        ownerMark = new ObjectMap<String, TextureRegion>();

        Pixmap blackpixmap = new Pixmap(GamePreferences.spriteSize, GamePreferences.spriteSize, Format.RGBA8888);
        blackpixmap.setColor(0, 0, 0, 0);
        blackpixmap.fill();
        blackTex = new TextureRegion(new Texture(new PixmapTextureData(blackpixmap, Format.RGBA8888, false, false, true)));
        game.getDynamicTextureManager().register(blackTex, blackpixmap);
        ownerMark.put("BLACK_TEX", blackTex);

        for (Iterator<Entry<String, Major>> iter = screenManager.getRaceController().getMajors().entries().iterator(); iter
                .hasNext();) {
            Entry<String, Major> e = iter.next();
            Pixmap racialPixMap = new Pixmap(GamePreferences.spriteSize, GamePreferences.spriteSize, Format.RGBA8888);
            Color color = e.value.getRaceDesign().clrSector;
            racialPixMap.setColor(color.r, color.g, color.b, 0.25f);
            racialPixMap.fill();
            TextureRegion racialTex = new TextureRegion(new Texture(new PixmapTextureData(racialPixMap, Format.RGBA8888, false,
                    false, true)));
            game.getDynamicTextureManager().register(racialTex, racialPixMap);
            ownerMark.put(e.key, racialTex);
        }

        Pixmap fogOfWarMap = new Pixmap(GamePreferences.spriteSize, GamePreferences.spriteSize, Format.RGBA8888);
        fogOfWarMap.setColor(0, 0, 0, 160 / 256.0f);
        fogOfWarMap.fill();
        TextureRegion fogOfWarTex = new TextureRegion(new Texture(new PixmapTextureData(fogOfWarMap, Format.RGBA8888, false,
                false, true)));
        game.getDynamicTextureManager().register(fogOfWarTex, fogOfWarMap);
        ownerMark.put("FOG_OF_WAR", fogOfWarTex);

        Pixmap minorRaceMap = new Pixmap(GamePreferences.spriteSize, GamePreferences.spriteSize, Format.RGBA8888);
        minorRaceMap.setColor(Color.valueOf("C8C8C864"));
        minorRaceMap.fill();
        TextureRegion minorRaceTex = new TextureRegion(new Texture(new PixmapTextureData(minorRaceMap, Format.RGBA8888, false,
                false, true)));
        game.getDynamicTextureManager().register(minorRaceTex, minorRaceMap);
        ownerMark.put("_MINOR_", minorRaceTex);

        for (ScanPower pw : ScanPower.values()) {
            Pixmap scanPowerMap = new Pixmap(GamePreferences.spriteSize, GamePreferences.spriteSize, Format.RGBA8888);
            scanPowerMap.setColor(ScanPower.getColor(pw.getLimit() - 1, 0.25f));
            scanPowerMap.fill();
            TextureRegion scanPowerTex = new TextureRegion(new Texture(new PixmapTextureData(scanPowerMap, Format.RGBA8888,
                    false, false, true)));
            game.getDynamicTextureManager().register(scanPowerTex, scanPowerMap);
            ownerMark.put(ScanPower.getName(pw.getLimit() - 1), scanPowerTex);
        }

        for (int i = DiplomaticAgreement.WAR.getType(); i <= DiplomaticAgreement.MEMBERSHIP.getType(); i++) {
            Pixmap diploMap = new Pixmap(GamePreferences.spriteSize, GamePreferences.spriteSize, Format.RGBA8888);
            Pair<String, Color> status = DiplomaticAgreement.getDiplomacyStatus(DiplomaticAgreement.fromInt(i));
            Color color = status.getSecond();
            String colorName = "DIPLO_" + status.getFirst();
            diploMap.setColor(color);
            diploMap.fill();
            TextureRegion diploTex = new TextureRegion(new Texture(new PixmapTextureData(diploMap, Format.RGBA8888,
                    false, false, true)));
            game.getDynamicTextureManager().register(diploTex, diploMap);
            ownerMark.put(colorName, diploTex);
        }
        {
            Pixmap diploMap = new Pixmap(GamePreferences.spriteSize, GamePreferences.spriteSize, Format.RGBA8888);
            Color color = new Color(178 / 255.0f, 0, 1.0f, 0.25f);
            String colorName = "DIPLO_SUBJUGATED";
            diploMap.setColor(color);
            diploMap.fill();
            TextureRegion diploTex = new TextureRegion(new Texture(new PixmapTextureData(diploMap, Format.RGBA8888,
                    false, false, true)));
            game.getDynamicTextureManager().register(diploTex, diploMap);
            ownerMark.put(colorName, diploTex);
        }
        rangeLines = new Array<Image>();

        updateScreen();

        for (int i = 0; i < getResourceManager().getRaceController().getMajors().size; i++) {
            if (getResourceManager().getRaceController().getMajors().getValueAt(i).isHumanPlayer())
                oldPosition = getResourceManager().getRaceController().getMajors().getValueAt(i).getCoordinates();
        }

        renderer = new OrthogonalTiledMapRenderer(map);

        // selection marker
        selectStack = new Group();
        Pixmap pixmap = new Pixmap(GamePreferences.spriteSize, GamePreferences.spriteSize, Format.RGBA8888);
        pixmap.setColor(Color.WHITE);
        for (int i = 0; i < 4; i++)
            pixmap.drawRectangle(i, i, GamePreferences.spriteSize - i * 2, GamePreferences.spriteSize - i * 2);
        Texture texture = new Texture(new PixmapTextureData(pixmap, Format.RGBA8888, game.useMipMaps(), false, true));
        texture.setFilter(game.useMipMaps() ? TextureFilter.MipMapLinearLinear : TextureFilter.Linear, TextureFilter.Linear);
        selectTexture = new TextureRegion(texture);
        selectImage = new Image(selectTexture);
        setState(GalaxyState.NORMAL);
        game.getDynamicTextureManager().register(selectTexture, pixmap);

        selectDistLabel = new Label("", screenManager.getSkin(), "largeFont", Color.WHITE);
        winningChanceLabel = new Label("", screenManager.getSkin(), "largeFont", Color.WHITE);

        selectStack.setBounds(selectImage.getX(), selectImage.getY(), selectImage.getWidth(), selectImage.getHeight());
        selectDistLabel.setPosition((int) (selectStack.getWidth() / 2 + 15), selectStack.getHeight() / 2);
        winningChanceLabel.setPosition((int) (selectStack.getWidth() / 2 - 50), selectStack.getHeight() / 2);
        selectStack.addActor(selectImage);
        selectStack.addActor(selectDistLabel);
        selectStack.addActor(winningChanceLabel);
        selectStack.setPosition(oldPosition.x * GamePreferences.spriteSize, oldPosition.y * GamePreferences.spriteSize);
        selectDistLabel.setVisible(false);
        winningChanceLabel.setVisible(false);

        universeMap.setSelectedCoordValue(new IntPoint((int) selectStack.getX() / GamePreferences.spriteSize, (int) selectStack
                .getY() / GamePreferences.spriteSize));
        sidebarRight.setStarSystemInfo(universeMap.getStarSystemAt((int) selectStack.getX() / GamePreferences.spriteSize,
                (int) selectStack.getY() / GamePreferences.spriteSize));
        sidebarRight.showStarSystemInfo();

        universeStage.addActor(selectStack);

        universeStage.getViewport().setCamera(camera);

        universeStage.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (button == 1) { //right button cancels stuff
                    needstomove = false;
                    cancelShipMove();
                    showPlanets(true);
                    return true;
                }

                if (getResourceManager().processingTurn())
                    return true;

                //System.out.println("event=" + event + " x=" + x + " y=" + y + " pointer=" + pointer + " button=" + button);
                int newX = ((int) x / GamePreferences.spriteSize) * GamePreferences.spriteSize;
                int newY = ((int) y / GamePreferences.spriteSize) * GamePreferences.spriteSize;
                IntPoint newPos = new IntPoint(newX / GamePreferences.spriteSize, newY / GamePreferences.spriteSize);
                Vector2 newPosition = new Vector2(newX, newY);
                Rectangle universeRect = new Rectangle(0, 0, GamePreferences.spriteSize
                        * (getResourceManager().getGridSizeX() - 1), GamePreferences.spriteSize
                        * (getResourceManager().getGridSizeY() - 1));
                target = new IntPoint();
                if (universeRect.contains(newPosition)) {
                    //System.out.println(universeMap.getStarSystemAt(newX/GamePreferences.spriteSize, newY/GamePreferences.spriteSize).getCoordinates().toString());
                    selectStack.setPosition(newPosition.x, newPosition.y);
                    sidebarRight.setStarSystemInfo(universeMap.getStarSystemAt(newX / GamePreferences.spriteSize, newY
                            / GamePreferences.spriteSize));
                    sidebarRight.showStarSystemInfo();
                    target = new IntPoint((int) (newPosition.x / GamePreferences.spriteSize),
                            (int) (newPosition.y / GamePreferences.spriteSize));

                    if (state == GalaxyState.MOVE || state == GalaxyState.NORMAL) {
                        universeMap.setSelectedCoordValue(target);
                        if (button == 2) {
                            ((ScreenManager) game).setView(ViewTypes.SYSTEM_VIEW);
                            return true;
                        }

                        if (sidebarRight.getShipRenderer().isShipMove()) {
                            drawShipMove();
                        } else {
                            ((ScreenManager) game).getSidebarLeft().showInfo(true);
                        }

                        if (needstomove == false && oldPosition.equals(newPos)) {
                            if (sidebarRight.getShipRenderer().isShipMove()) {
                                universeStage.cancelTouchFocus();
                                Ships s = universeMap.getCurrentShip();
                                if (s.getOwnerId().equals(game.getRaceController().getPlayerRaceString())) {
                                    if (!target.equals(s.getCoordinates()) && s.getPath().size != 0)
                                        s.setTargetCoord(target);
                                    else {
                                        s.setTargetCoord(new IntPoint());
                                        s.getPath().clear();
                                    }
                                }
                                sidebarRight.clearSelectedShip();
                                if (s.getPath().size != 0
                                        && s.getOwnerId().equals(game.getRaceController().getPlayerRaceString()))
                                    game.getSoundManager().playSound(SndMgrValue.SNDMGR_SOUND_SHIPTARGET);
                                else
                                    game.getSoundManager().playSound(SndMgrValue.SNDMGR_SOUND_ERROR);
                                sidebarRight.getShipRenderer().drawSidebarInfo(s, screenManager.getSkin());
                                showPlanets(true);
                            } else {
                                showShips();
                            }
                        } else if (needstomove == true) {
                            showPlanets();
                        }
                        if (sidebarRight.getShipRenderer().isShipMove() || needstomove) {
                            setState(GalaxyState.MOVE);
                        } else {
                            setState(GalaxyState.NORMAL);
                        }
                        oldPosition = new IntPoint(newPos);
                        showPathImages();
                    } else if (state == GalaxyState.SET_ROUTE) {
                        showResourceRoute();
                        oldPos = new IntPoint(target);
                    } else if (state == GalaxyState.SET_BUILD_TARGET) {
                        confirmBuildTargetCoord();
                        oldPos = new IntPoint(target);
                    }
                }

                requestRendering();
                return false;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                requestRendering();
            }

            @Override
            public void touchDragged(InputEvent event, float x, float y, int pointer) {
                requestRendering();
            }

            @Override
            public boolean keyDown(InputEvent event, int keycode) {
                switch(keycode) {
                    case Keys.N:
                        screenManager.getUniverseMap().searchNextIdleShipAndJumpToIt(ShipOrder.NONE);
                        break;
                    case Keys.W:
                    case Keys.S:
                        if (!sidebarRight.isStarSystemShown())
                            if (sidebarRight.getShipRenderer().getSelectedShip() != null) {
                                Ships ship = sidebarRight.getShipRenderer().getSelectedShip();
                                if (ship.getOwnerId().equals(screenManager.getRaceController().getPlayerRaceString())
                                        && ship.getCurrentOrder() == ShipOrder.NONE
                                        && ship.getTargetCoord().equals(new IntPoint())) {
                                    if (keycode == Keys.W)
                                        ship.setCurrentOrder(ShipOrder.WAIT_SHIP_ORDER);
                                    else if (keycode == Keys.S)
                                        ship.setCurrentOrder(ShipOrder.SENTRY_SHIP_ORDER);
                                    sidebarRight.getShipRenderer().clearSelectedShip();
                                }
                            }
                        break;
                    case Keys.ENTER:
                    case Keys.SPACE:
                        if (Gdx.input.isKeyPressed(Keys.SHIFT_LEFT) || Gdx.input.isKeyPressed(Keys.SHIFT_RIGHT)
                                || !screenManager.getUniverseMap().searchNextIdleShipAndJumpToIt(ShipOrder.NONE))
                            screenManager.endTurn();
                        break;
                    case Keys.RIGHT:
                        if (Gdx.input.isKeyPressed(Keys.SHIFT_LEFT) || Gdx.input.isKeyPressed(Keys.SHIFT_RIGHT))
                            camera.translate(GamePreferences.spriteSize, 0);
                        break;
                    case Keys.LEFT:
                        if (Gdx.input.isKeyPressed(Keys.SHIFT_LEFT) || Gdx.input.isKeyPressed(Keys.SHIFT_RIGHT))
                            camera.translate(-GamePreferences.spriteSize, 0);
                        break;
                    case Keys.UP:
                        if (Gdx.input.isKeyPressed(Keys.SHIFT_LEFT) || Gdx.input.isKeyPressed(Keys.SHIFT_RIGHT))
                            camera.translate(0, GamePreferences.spriteSize);
                        break;
                    case Keys.DOWN:
                        if (Gdx.input.isKeyPressed(Keys.SHIFT_LEFT) || Gdx.input.isKeyPressed(Keys.SHIFT_RIGHT))
                            camera.translate(0, -GamePreferences.spriteSize);
                        break;
                    default:
                        break;
                }
                return false;
            }
        });
        camera.translate(-camera.position.x + oldPosition.x * GamePreferences.spriteSize, -camera.position.y + oldPosition.y
                * GamePreferences.spriteSize, 0);
        camera.zoom = 2; //* (float) 1280.0f / Gdx.graphics.getWidth();
        initLocks();
        UniverseTooltip tooltip = new UniverseTooltip(screenManager, screenManager.getSkin());
        tooltip.setTargetActor(sidebarRight.getBackground());
        universeStage.addListener(tooltip.getListener());
    }

    public void setState(GalaxyState state) {
        this.state = state;
        selectColor = state.color;
        selectImage.setDrawable(new SpriteDrawable(new Sprite(selectTexture)).tint(selectColor));
    }

    public GalaxyState getState() {
        return state;
    }

    public void cancelShipMove() {
        if (sidebarRight.getShipRenderer().isShipMove()) {
            target = sidebarRight.getShipRenderer().getSelectedShip().getTargetCoord();
            if (target.equals(new IntPoint()))
                target = sidebarRight.getShipRenderer().getSelectedShip().getCoordinates();
            drawShipMove();
            selectDistLabel.setVisible(false);
            sidebarRight.getShipRenderer().clearSelectedShip();
        }
    }

    public void drawShipMove() {
        Pair<IntPoint, Array<IntPoint>> p = null;
        Ships s = universeMap.getCurrentShip();
        if (!s.isStation() && s.getOwnerId().equals(game.getRaceController().getPlayerRaceString())) {
            drawRangeLines(s.getRange().getRange());
            Major playerRace = game.getRaceController().getPlayerRace();
            StarSystem ss = universeMap.getCurrentStarSystem();
            p = playerRace.getStarMap().calcPath(s.getCoordinates(), target, 3 - s.getRange(true).getRange(), s.getSpeed(true));
            drawPath(s, p.getSecond());
            if (shouldShowWinningChance(playerRace, s, ss)) {
                Array<Ships> involvedShips = new Array<Ships>();
                involvedShips.add(s);
                ArrayMap<String, ShipMap> attacked = ss.getShipsInSector();
                for (int i = 0; i < attacked.size; i++)
                    for (int j = 0; j < attacked.getValueAt(i).getSize(); j++)
                        involvedShips.add(attacked.getValueAt(i).getAt(j));
                ObjectSet<String> dummy1 = new ObjectSet<String>();
                ObjectSet<String> dummy2 = new ObjectSet<String>();
                double chance = Combat.getWinningChance(playerRace, involvedShips, game.getRaceController(), dummy1, dummy2,
                        ss.getAnomaly(), true, true);
                winningChanceLabel.setText(String.format("%.0f%%", chance * 100));
                Color color = Color.WHITE;
                if (chance < 0.4)
                    color = new Color(200 / 255.0f, 0, 0, 1.0f);
                else if (chance < 0.6)
                    color = new Color(255 / 255.0f, 100 / 255.0f, 0, 1.0f);
                else if (chance < 0.75)
                    color = new Color(255 / 255.0f, 225 / 255.0f, 0, 1.0f);
                else if (chance < 0.9)
                    color = new Color(200 / 255.0f, 225 / 255.0f, 0, 1.0f);
                else
                    color = new Color(0, 200 / 255.0f, 0, 1.0f);
                winningChanceLabel.setColor(color);
                winningChanceLabel.setVisible(true);
            } else
                winningChanceLabel.setVisible(false);
            s.setPath(p.getSecond());
            String text = "";
            if (s.getSpeed(s.hasFleet()) != 0)
                text = String.format("%d", Math.round(Math.ceil(s.getPath().size / (float) s.getSpeed(s.hasFleet()))));
            if (!text.isEmpty()) {
                selectDistLabel.setText(text);
                selectDistLabel.setVisible(true);
            }
            sidebarRight.getShipRenderer().drawSidebarInfo(s, game.getSkin(), true);
        }
    }

    private void initLocks() {
        lockTexture = game.getUiTexture("lock_closed");
        unlockTexture = game.getUiTexture("lock_open");
        Sprite sp = new Sprite(lockTexture);
        sp.setColor(Color.RED);
        SpriteDrawable drawable = new SpriteDrawable(sp);
        lockImageLeft = new Image(drawable);
        LeftSideBar sidebarLeft = ((ScreenManager) getResourceManager()).getSidebarLeft();
        lockImageLeft.setUserObject(new Pair<UISidebar, Boolean>(sidebarLeft, false));
        lockImageRight = new Image(drawable);
        lockImageRight.setUserObject(new Pair<UISidebar, Boolean>(sidebarRight, false));

        InputListener lockListener = new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                Image img = (Image) event.getListenerActor();
                @SuppressWarnings("unchecked")
                Pair<UISidebar, Boolean> pair = (Pair<UISidebar, Boolean>) img.getUserObject();
                boolean zoomable = pair.getSecond();
                UISidebar sidebar = pair.getFirst();
                zoomable = !zoomable;
                if (sidebar instanceof LeftSideBar)
                    lockOrUnlock(sidebarRight, false, lockImageRight);
                else
                    lockOrUnlock(((ScreenManager) getResourceManager()).getSidebarLeft(), false, lockImageLeft);
                lockOrUnlock(sidebar, zoomable, img);
                return false;
            }
        };
        lockImageLeft.addListener(lockListener);
        lockImageRight.addListener(lockListener);
        Rectangle rect = GameConstants.coordsToRelative(-10, 50, 50, 50);
        lockImageLeft.setBounds(rect.x, rect.y, rect.width, rect.height);
        sidebarLeft.getStage().addActor(lockImageLeft);
        rect = GameConstants.coordsToRelative(10, 50, 50, 50);
        rect.x += sidebarRight.getPosition().x + sidebarRight.getPosition().width - rect.width;
        lockImageRight.setBounds(rect.x, rect.y, rect.width, rect.height);
        sidebarRight.getStage().addActor(lockImageRight);
    }

    private void lockOrUnlock(UISidebar sidebar, boolean zoomable, Image img) {
        Color color = Color.RED;
        TextureRegion tex = lockTexture;
        if (zoomable) {
            tex = unlockTexture;
            color = Color.GREEN;
        }
        uiZoomCamera.zoom = 1;
        cameraControllerGesture.setEnabled(!zoomable);
        cameraControllerOrtho.setEnabled(!zoomable);
        sidebar.getStage().getViewport().setCamera(zoomable ? uiZoomCamera : uicamera);
        cameraControllerGestureUI.setEnabled(zoomable);
        cameraControllerOrthoUI.setEnabled(zoomable);
        uiZoomCamera.update();
        sidebar.getStage().getViewport().update(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), true);
        Sprite sp = new Sprite(tex);
        sp.setColor(color);
        SpriteDrawable drawable = new SpriteDrawable(sp);
        img.setDrawable(drawable);
        img.setUserObject(new Pair<UISidebar, Boolean>(sidebar, zoomable));
    }

    public static boolean shouldShowWinningChance(Major player, Ships attacking, Sector sector) {
        if (!attacking.getOwnerId().equals(player.getRaceId()) || attacking.isStation())
            return false;
        ArrayMap<String, ShipMap> defend = sector.getShipsInSector();
        for (int i = 0; i < defend.size; i++) {
            String otherID = defend.getKeyAt(i);
            if (player.isEnemyOf(otherID))
                if (sector.shouldDrawOutpost(player, otherID) || sector.shouldDrawShip(player, otherID))
                    return true;
        }
        return false;
    }

    public void setRouteType(RouteType type) {
        setRouteType(type, ResourceTypes.TITAN);
    }

    public void setRouteType(RouteType type, ResourceTypes rt) {
        cancelShipMove();
        showPlanets(true);
        resourceType = rt;
        drawRoute = type;
        target = new IntPoint();
        oldPos = new IntPoint();
        setState(GalaxyState.SET_ROUTE);
        showResourceRoute();
    }

    public void showResourceRoute() {
        if (drawRoute != RouteType.NONE) {
            if (currentRoute != null) {
                currentRoute.setVisible(false);
                currentRoute.remove();
            }
            if (!target.equals(new IntPoint())) {
                if (drawRoute == RouteType.RESOURCE)
                    currentRoute = drawBezierCurve(universeMap.getSelectedCoordValue(), target, 25, 2, Color.ORANGE);
                else
                    currentRoute = drawDashedLine(universeMap.getSelectedCoordValue(), target, 2, Color.ORANGE, 8, 2);

                universeStage.addActor(currentRoute);
                if (oldPos.equals(target) && !oldPos.equals(new IntPoint()) && !target.equals(new IntPoint())
                        && !target.equals(universeMap.getSelectedCoordValue())) {
                    StarSystem starSystem = universeMap.getCurrentStarSystem();
                    ResearchInfo researchInfo = game.getRaceController().getPlayerRace().getEmpire().getResearch()
                            .getResearchInfo();
                    if (drawRoute == RouteType.RESOURCE) {
                        if (!starSystem.addResourceRoute(target, resourceType.getType(), universeMap.getStarSystems(),
                                researchInfo)) {
                            game.getSoundManager().playSound(SndMgrValue.SNDMGR_SOUND_ERROR);
                        }
                    } else {
                        StarSystem ts = universeMap.getStarSystemAt(target);
                        Major playerRace = game.getRaceController().getPlayerRace();
                        if (ts.isSunSystem() && !ts.isFree() && ts.getInhabitants() > 0
                                && (playerRace.getAgreement(ts.getOwnerId()).getType() >= DiplomaticAgreement.TRADE.getType())
                                && !starSystem.addTradeRoute(target, universeMap.getStarSystems(), researchInfo)) {
                            game.getSoundManager().playSound(SndMgrValue.SNDMGR_SOUND_ERROR);
                        }
                    }
                    universeMap.setSelectedCoordValue(target);
                    hideResourceRoute();
                }
            }
        }
    }

    private void hideResourceRoute() {
        drawRoute = RouteType.NONE;
        if (state == GalaxyState.SET_ROUTE)
            setState(GalaxyState.NORMAL);
        if (currentRoute != null) {
            currentRoute.setVisible(false);
            currentRoute.remove();
        }
        drawResourceRoutes();
    }

    public void showShips() {
        showShips(false);
    }

    public void showShips(boolean setSelection) {
        if (setSelection) {
            setState(GalaxyState.MOVE);
            sidebarRight.setStarSystemInfo(universeMap.getCurrentStarSystem());
        }
        hideResourceRoute();
        needstomove = true;
        sidebarRight.hideStarSystemInfo();
        sidebarRight.showShips();
    }

    public void showPlanets() {
        showPlanets(false);
    }

    public void clearSelection() {
        setState(GalaxyState.NORMAL);
        selectDistLabel.setVisible(false);
        winningChanceLabel.setVisible(false);
    }

    public void showPlanets(boolean clearSelection) {
        if (clearSelection) {
            clearSelection();
            sidebarRight.setStarSystemInfo(universeMap.getCurrentStarSystem());
            ((ScreenManager) game).getSidebarLeft().showInfo(true);
        }
        hideResourceRoute();
        needstomove = false;
        sidebarRight.getShipRenderer().hideShips(clearSelection);
        sidebarRight.showStarSystemInfo();
    }

    public TiledMapTileLayer generateGridLayer() {
        TiledMapTileLayer layer = new TiledMapTileLayer(game.getGridSizeX(), game.getGridSizeY(), GamePreferences.spriteSize,
                GamePreferences.spriteSize);
        Pixmap gridPixmap = new Pixmap(GamePreferences.spriteSize, GamePreferences.spriteSize, Format.RGBA8888);
        gridPixmap.setColor(Color.BLACK);
        for (int i = 0; i < 2; i++)
            gridPixmap.drawRectangle(i, i, GamePreferences.spriteSize - i * 2, GamePreferences.spriteSize - i * 2);
        Texture tex = new Texture(new PixmapTextureData(gridPixmap, Format.RGBA8888, game.useMipMaps(), false, true));
        tex.setFilter(game.useMipMaps() ? TextureFilter.MipMapLinearLinear : TextureFilter.Linear, TextureFilter.Linear);
        gridTex = new TextureRegion(tex);
        game.getDynamicTextureManager().register(gridTex, gridPixmap);

        for (int j = 0; j < game.getGridSizeY(); j++)
            for (int i = 0; i < game.getGridSizeX(); i++) {
                Cell cell = new Cell();
                cell.setTile(new StaticTiledMapTile(gridTex));
                layer.setCell(i, j, cell);
            }
        layer.setName("GRID");
        return layer;
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batchfixed.begin();
        universeBackground.draw(batchfixed);
        batchfixed.end();

        camera.update();
        renderer.setView(camera);
        renderer.render();
        if (game.getGameSettings().showStarSystemNames) {
            universeStage.getBatch().begin();
            cache.draw(universeStage.getBatch());
            universeStage.getBatch().end();
        }

        universeStage.act(1 / 30.0f);
        universeStage.draw();

        ((ScreenManager) game).getSidebarLeft().getStage().act(1 / 30.0f);
        ((ScreenManager) game).getSidebarLeft().getStage().draw();

        sidebarRight.getStage().act(1 / 30.0f);
        sidebarRight.getStage().draw();

        if (Gdx.graphics.isContinuousRendering()) { //draw a couple of frames when rendering is requested
            if (renderCnt > 2)
                Gdx.graphics.setContinuousRendering(false);
            else
                renderCnt++;
        }

        game.getAchievementManager().render();
        //batchfixed.begin();
        //CharSequence str ="FPS: " + Gdx.graphics.getFramesPerSecond();
        //((ScreenManager)game).getScaledFont().draw(batchfixed, str, 0, font.getBounds(str).height+2);
        //batchfixed.end();
    }

    @Override
    public void dispose() {
        game.getDynamicTextureManager().clear();
        batchfixed.dispose();
        sidebarRight.dispose();
        universeStage.dispose();
        map.dispose();
        universeMap.dispose();
        super.dispose();
    }

    @Override
    public void resize(int width, int height) {
        Vector3 oldposition = camera.position.cpy();
        universeStage.getViewport().update(width, height, true);
        ((ScreenManager) game).getSidebarLeft().getStage().getViewport().update(width, height, true);
        sidebarRight.getStage().getViewport().update(width, height, true);
        camera.translate(oldposition.x - camera.position.x, oldposition.y - camera.position.y, 0);
        uiZoomCamera.update();
        game.getAchievementManager().updateSize(width, height, true);
    }

    private void requestRendering() {
        Gdx.graphics.setContinuousRendering(true);
        renderCnt = 0;
    }

    @Override
    public void show() {
        game.getAchievementManager().popupAchievements();
        setInputProcessor(inputs);
        lockImageLeft.setVisible(true);
        lockImageRight.setVisible(true);
        selectStack.setPosition(universeMap.getSelectedCoordValue().x * GamePreferences.spriteSize,
                universeMap.getSelectedCoordValue().y * GamePreferences.spriteSize);
        IntPoint coord = universeMap.getSelectedCoordValue();
        DefaultScreen previous = ((ScreenManager) game).getPreviousScreen();
        if (!(previous instanceof TutorialScreen) && !(previous instanceof OptionsScreen) && !(previous instanceof FleetScreen)
                && game.getRaceController().getPlayerRace().getEmpire().getSystemList().contains(coord, false))
            sidebarRight.setStarSystemInfo(universeMap.getStarSystemAt(coord));
        camera.translate(-camera.position.x + coord.x * GamePreferences.spriteSize, -camera.position.y + coord.y
                * GamePreferences.spriteSize, 0);
        updateScreen();
        //update ships
        StarSystem ss = universeMap.getCurrentStarSystem();
        getRightSideBar().setStarSystemInfo(ss);
        getRightSideBar().showStarSystemInfo();
        ShipRenderer shipRenderer = getRightSideBar().getShipRenderer();
        if (shipRenderer.isShipMove()) {
            shipRenderer.hideShips();
            target = new IntPoint(coord);
            drawShipMove();
            needstomove = false;
        }
        if (needstomove) {
            showPlanets(true);
            needstomove = false;
        }
        showPathImages();
        oldPosition = new IntPoint(coord);
        requestRendering();
    }

    public void saveScreenShot(String name) {
        Pixmap galaxyPixmap = new Pixmap(game.getGridSizeX() * GameConstants.screenshotMulti, game.getGridSizeY()
                * GameConstants.screenshotMulti, Format.RGBA8888);
        galaxyPixmap.setColor(new Color(0.0f, 0.0f, 0.0f, 1.0f));
        galaxyPixmap.fill();

        Major playerRace = getResourceManager().getRaceController().getPlayerRace();
        for (int y = 0; y < game.getGridSizeY(); y++)
            for (int x = 0; x < game.getGridSizeX(); x++) {
                StarSystem starSystem = universeMap.getStarSystemAt(x, y);
                if (!starSystem.isFree() && !starSystem.independentMinor() && starSystem.getScanned(playerRace.getRaceId())
                        && playerRace.isRaceContacted(starSystem.getOwnerId())
                        || starSystem.getOwnerId().equals(playerRace.getRaceId())) {
                    galaxyPixmap
                            .drawPixmap(game.getDynamicTextureManager().getPixmap(ownerMark.get(starSystem.getOwnerId())), 0, 0,
                                    GamePreferences.spriteSize, GamePreferences.spriteSize, x * GameConstants.screenshotMulti,
                                    game.getGridSizeY() * GameConstants.screenshotMulti - y * GameConstants.screenshotMulti
                                            - GameConstants.screenshotMulti, GameConstants.screenshotMulti,
                                    GameConstants.screenshotMulti);
                } else if (!starSystem.getScanned(playerRace.getRaceId()) && !starSystem.getKnown(playerRace.getRaceId())) {
                    galaxyPixmap
                            .drawPixmap(game.getDynamicTextureManager().getPixmap(ownerMark.get("FOG_OF_WAR")), 0, 0,
                                    GamePreferences.spriteSize, GamePreferences.spriteSize, x * GameConstants.screenshotMulti,
                                    game.getGridSizeY() * GameConstants.screenshotMulti - y * GameConstants.screenshotMulti
                                            - GameConstants.screenshotMulti, GameConstants.screenshotMulti,
                                    GameConstants.screenshotMulti);
                } else if (starSystem.getKnown(playerRace.getRaceId())
                        && (starSystem.getMinorRace() != null && !starSystem.isMajorized() || starSystem.hasRebelled())) {
                    galaxyPixmap
                            .drawPixmap(game.getDynamicTextureManager().getPixmap(ownerMark.get("_MINOR_")), 0, 0,
                                    GamePreferences.spriteSize, GamePreferences.spriteSize, x * GameConstants.screenshotMulti,
                                    game.getGridSizeY() * GameConstants.screenshotMulti - y * GameConstants.screenshotMulti
                                            - GameConstants.screenshotMulti, GameConstants.screenshotMulti,
                                    GameConstants.screenshotMulti);
                } else {
                    galaxyPixmap
                            .drawPixmap(game.getDynamicTextureManager().getPixmap(ownerMark.get("BLACK_TEX")), 0, 0,
                                    GamePreferences.spriteSize, GamePreferences.spriteSize, x * GameConstants.screenshotMulti,
                                    game.getGridSizeY() * GameConstants.screenshotMulti - y * GameConstants.screenshotMulti
                                            - GameConstants.screenshotMulti, GameConstants.screenshotMulti,
                                    GameConstants.screenshotMulti);
                }

                if (starSystem.getScanned(playerRace.getRaceId())) {
                    if (starSystem.isSunSystem() || starSystem.getAnomaly() != null) {
                        Pixmap sunpixmap;
                        if (starSystem.isSunSystem())
                            sunpixmap = game.getSunPixmap(starSystem.getStarType().getName());
                        else {
                            sunpixmap = game.getSunPixmap(starSystem.getAnomaly().getType().getName());
                        }
                        galaxyPixmap.drawPixmap(sunpixmap, x * GameConstants.screenshotMulti, game.getGridSizeY()
                                * GameConstants.screenshotMulti - y * GameConstants.screenshotMulti
                                - GameConstants.screenshotMulti);
                    }
                }
            }
        try {
            FileHandle fh = new FileHandle(GameConstants.getSaveLocation() + name);
            PixmapIO.writePNG(fh, galaxyPixmap);
        } catch (Exception e) {
        }
        galaxyPixmap.dispose();
    }

    @Override
    public void hide() {
        lockOrUnlock(((ScreenManager) getResourceManager()).getSidebarLeft(), false, lockImageLeft);
        lockOrUnlock(sidebarRight, false, lockImageRight);
        lockImageLeft.setVisible(false);
        lockImageRight.setVisible(false);
        if (Gdx.graphics.isContinuousRendering())
            Gdx.graphics.setContinuousRendering(false);
    }

    public Image getNewTargetImage(IntPoint p) {
        StarSystem ss = universeMap.getStarSystemAt(p);
        Vector2 vector = new Vector2(0, 0);
        if (ss.getPathImage(vector) != null) {
            ss.getPathImage(vector).remove();
            return ss.getPathImage(vector);
        }

        TextureRegion tex = game.getUiTexture("shippath");
        Image shipPathImg = new Image(new TextureRegionDrawable(tex));
        shipPathImg.setSize(GamePreferences.spriteSize / 5.12f, GamePreferences.spriteSize / 5.12f);
        shipPathImg.setPosition(p.x * GamePreferences.spriteSize
                + (GamePreferences.spriteSize / 2 - shipPathImg.getWidth() / 2), p.y * GamePreferences.spriteSize
                + (GamePreferences.spriteSize / 2 - shipPathImg.getHeight() / 2));
        ss.putPathImage(vector, shipPathImg);
        return shipPathImg;
    }

    public Image getNewShipDirImage(IntPoint current, IntPoint previous) {
        StarSystem ss = universeMap.getStarSystemAt(previous);
        Vector2 vector = new Vector2(current.x - previous.x, current.y - previous.y);
        if (ss.getPathImage(vector) != null) {
            ss.getPathImage(vector).remove();
            return ss.getPathImage(vector);
        }

        TextureRegion tex = game.getUiTexture("shipdir");
        Image shipPathImg = new Image(new TextureRegionDrawable(tex));
        shipPathImg.setSize(GamePreferences.spriteSize / 6.4f, GamePreferences.spriteSize / 6.4f);
        shipPathImg.setOrigin(shipPathImg.getWidth() / 2,  shipPathImg.getHeight() / 2);
        shipPathImg.setPosition((current.x - vector.x / 2) * GamePreferences.spriteSize + GamePreferences.spriteSize / 2 - shipPathImg.getWidth() * vector.x / 2 - shipPathImg.getWidth() / 2,
                (current.y - vector.y / 2) * GamePreferences.spriteSize + GamePreferences.spriteSize / 2 - shipPathImg.getHeight() * vector.y / 2 - shipPathImg.getHeight() / 2);
        shipPathImg.rotateBy(vector.angle() - 90);
        ss.putPathImage(vector, shipPathImg);
        return shipPathImg;
    }

    private Array<Image> clearPath(Ships ship) {
        Array<Image> pathImgs = ship.getPathImgs();
        for (int i = 0; i < pathImgs.size; i++) {
            pathImgs.get(i).remove();
            pathImgs.removeIndex(i--);
        }
        return pathImgs;
    }

    public void drawPath(Ships ship, Array<IntPoint> path) {
        Color color = Color.CYAN;
        if (state == GalaxyState.MOVE
                && ship.equals(getRightSideBar().getShipRenderer().getSelectedShip()))
            color = Color.GREEN;
        Array<Image> pathImgs = clearPath(ship);
        Image img;
        if (path.size == 0 || !(path.get(path.size - 1).equals(ship.getTargetCoord()) || path.get(path.size - 1).equals(target)))
            return;

        img = getNewTargetImage(path.get(path.size - 1));
        img.setColor(color);
        universeStage.addActor(img);
        pathImgs.add(img);
        img = getNewShipDirImage(path.get(0), ship.getCoordinates());
        img.setColor(color);
        universeStage.addActor(img);
        pathImgs.add(img);
        for (int i = 0; i < path.size - 1; i++) {
            img = getNewShipDirImage(path.get(i + 1), path.get(i));
            img.setColor(color);
            universeStage.addActor(img);
            pathImgs.add(img);
        }
    }

    public void showPathImages() {
        for (int i = 0; i < universeMap.getShipMap().getSize(); i++) {
            Ships s = universeMap.getShipMap().getAt(i);
            if (s.getOwnerId().equals(game.getRaceController().getPlayerRaceString()))
                if (s.hasTarget())
                    drawPath(s, s.getPath());
                else
                    clearPath(s);
        }
        if (state == GalaxyState.MOVE && sidebarRight.getShipRenderer().isShipMove()
                &&  sidebarRight.getShipRenderer().getSelectedShip().getOwnerId().equals(game.getRaceController().getPlayerRaceString())) { //put this above all
            drawPath(universeMap.getCurrentShip(), universeMap.getCurrentShip().getPath());
        }
    }

    private int getRangeBorder(int range1, int range2, int range) {
        if (range1 != range2) {
            int maximum = Math.max(range1, range2);
            if (range > 0)
                return Math.max(Math.min(maximum, range), Math.min(range1, range2) + 1);
            else
                return maximum;
        } else
            return StarMap.RangeTypes.SM_RANGE_SPACE.getType();
    }

    public void drawRangeLines(int shipRange) {
        oldShipRange = shipRange;
        for (int i = 0; i < rangeLines.size; i++)
            rangeLines.get(i).remove();
        rangeLines.clear();
        float lineWidth = camera.zoom < 4 ? 4 : camera.zoom < 6 ? 6 : 8;

        int range = StarMap.RangeTypes.SM_RANGE_SPACE.getType();
        Major major = game.getRaceController().getPlayerRace();
        StarMap starMap = major.getStarMap();
        Color lineColor = new Color(0, 0, 0, 0);
        Texture lineTexture = game.getAssetManager().get(GameConstants.UI_BG_SIMPLE, Texture.class);

        for (int i = 0, y = 0; i < getResourceManager().getGridSizeY(); i++, y += GamePreferences.spriteSize)
            for (int j = 0, x = 0; j < getResourceManager().getGridSizeX(); j++, x += GamePreferences.spriteSize) {
                boolean doDraw = false;
                if (j < getResourceManager().getGridSizeX() - 1) {
                    int border = getRangeBorder(starMap.getRangeValue(new IntPoint(j, i)),
                            starMap.getRangeValue(new IntPoint(j + 1, i)), range);
                    if (range == 0 || border == range) {
                        if (shipRange == -1 || border == 3 - shipRange)
                            doDraw = true;
                        if (border == StarMap.RangeTypes.SM_RANGE_FAR.getType()) {
                            lineColor = new Color(Color.RED);
                        } else if (border == StarMap.RangeTypes.SM_RANGE_MIDDLE.getType()) {
                            lineColor = new Color(Color.YELLOW);
                        } else if (border == StarMap.RangeTypes.SM_RANGE_NEAR.getType()) {
                            lineColor = new Color(Color.GREEN);
                        } else {
                            doDraw = false;
                        }
                    }
                }

                if (doDraw)
                    if (x < GamePreferences.spriteSize * (getResourceManager().getGridSizeX() - 1)) {
                        Image rangeImage = new Image();
                        rangeImage.setTouchable(Touchable.disabled);
                        Sprite sp = new Sprite(lineTexture);
                        sp.setColor(lineColor);
                        rangeImage.setDrawable(new SpriteDrawable(sp));
                        rangeImage.setBounds(x + GamePreferences.spriteSize - lineWidth / 2, y, lineWidth,
                                GamePreferences.spriteSize);
                        universeStage.addActor(rangeImage);
                        rangeLines.add(rangeImage);
                    }
                doDraw = false;
                if (i < getResourceManager().getGridSizeY() - 1) {
                    int border = getRangeBorder(starMap.getRangeValue(new IntPoint(j, i)),
                            starMap.getRangeValue(new IntPoint(j, i + 1)), range);
                    if (range == 0 || border == range) {
                        if (shipRange == -1 || border == 3 - shipRange)
                            doDraw = true;
                        if (border == StarMap.RangeTypes.SM_RANGE_FAR.getType()) {
                            lineColor = new Color(Color.RED);
                        } else if (border == StarMap.RangeTypes.SM_RANGE_MIDDLE.getType()) {
                            lineColor = new Color(Color.YELLOW);
                        } else if (border == StarMap.RangeTypes.SM_RANGE_NEAR.getType()) {
                            lineColor = new Color(Color.GREEN);
                        } else {
                            doDraw = false;
                        }
                    }
                }
                if (doDraw)
                    if (y < GamePreferences.spriteSize * (getResourceManager().getGridSizeY() - 1)) {
                        Image rangeImage = new Image();
                        rangeImage.setTouchable(Touchable.disabled);
                        Sprite sp = new Sprite(lineTexture);
                        sp.setColor(lineColor);
                        rangeImage.setOrigin(rangeImage.getImageWidth() / 2, rangeImage.getImageHeight());
                        rangeImage.rotateBy(90);
                        rangeImage.setDrawable(new SpriteDrawable(sp));
                        rangeImage.setBounds(x + GamePreferences.spriteSize, y + GamePreferences.spriteSize - lineWidth / 2,
                                lineWidth, GamePreferences.spriteSize);
                        universeStage.addActor(rangeImage);
                        rangeLines.add(rangeImage);
                    }
            }
    }

    private void drawResourceRoutes() {
        for (Image img : resRoutes) {
            img.setVisible(false);
            img.remove();
        }
        resRoutes.clear();
        if (game.getGameSettings().drawRoutes)
            for (int j = 0; j < getResourceManager().getGridSizeY(); j++)
                for (int i = 0; i < getResourceManager().getGridSizeX(); i++) {
                    StarSystem starSystem = universeMap.getStarSystemAt(i, j);

                    if (starSystem.getOwnerId().equals(game.getRaceController().getPlayerRaceString())) {
                        for (int k = 0; k < starSystem.getResourceRoutes().size; k++) {
                            ResourceRoute rr = starSystem.getResourceRoutes().get(k);
                            //Image img = drawLine(starSystem.getCoordinates(), rr.getCoord(), 2, Color.ORANGE);
                            Image img = drawBezierCurve(starSystem.getCoordinates(), rr.getCoord(), 25, 2, Color.ORANGE);
                            universeStage.addActor(img);
                            resRoutes.add(img);
                        }
                        for (int k = 0; k < starSystem.getTradeRoutes().size; k++) {
                            TradeRoute tr = starSystem.getTradeRoutes().get(k);
                            Image img = drawDashedLine(starSystem.getCoordinates(), tr.getDestCoord(), 2, Color.ORANGE, 8, 2);
                            universeStage.addActor(img);
                            resRoutes.add(img);
                        }
                    }
                }
    }

    private void drawSunLayer(TiledMapTileLayer layer, int i, int j, StarSystem starSystem, boolean init) {
        Image sunImg = null;
        Cell cell = layer.getCell(i, j);
        if (cell == null)
            cell = new Cell();

        if (init) {
            if (starSystem.isSunSystem() || (starSystem.getAnomaly() != null)) {
                TextureRegion sunRgn;
                if (starSystem.isSunSystem())
                    sunRgn = atlas.findRegion(starSystem.getStarType().getName());
                else {
                    sunRgn = atlas.findRegion(starSystem.getAnomaly().getType().getName());
                    sunRgn = new TextureRegion(sunRgn);
                    sunRgn.flip(starSystem.getAnomaly().getFlipHorz(), false);
                }
                sunImg = new Image(sunRgn);
                sunImg.setBounds(i * GamePreferences.spriteSize, j * GamePreferences.spriteSize, GamePreferences.spriteSize,
                        GamePreferences.spriteSize);
                sunImg.setVisible(false);
                sunImgs.add(sunImg);
                universeStage.addActor(sunImg);
                starSystem.setImageIndex(sunImgs.size - 1);
            }
        } else {
            if (starSystem.getImageIndex() != -1) {
                sunImg = sunImgs.get(starSystem.getImageIndex());
                sunImg.setVisible(false);
            }
        }
        layer.setCell(i, j, cell);
    }

    private void drawOwnerLayer(TiledMapTileLayer ownerLayer, int i, int j, StarSystem starSystem, Major playerRace) {
        Cell cell = ownerLayer.getCell(i, j);
        if (cell == null)
            cell = new Cell();

        if (!starSystem.isFree() && !starSystem.independentMinor() && starSystem.getScanned(playerRace.getRaceId())
                && playerRace.isRaceContacted(starSystem.getOwnerId()) || starSystem.getOwnerId().equals(playerRace.getRaceId())) {
            cell.setTile(new StaticTiledMapTile(new TextureRegion(ownerMark.get(starSystem.getOwnerId()))));
        } else if (!starSystem.getScanned(playerRace.getRaceId()) && !starSystem.getKnown(playerRace.getRaceId())) {
            cell.setTile(new StaticTiledMapTile(new TextureRegion(ownerMark.get("FOG_OF_WAR"))));
        } else if (starSystem.getKnown(playerRace.getRaceId())
                && (starSystem.getMinorRace() != null && !starSystem.isMajorized() || starSystem.hasRebelled())) {
            cell.setTile(new StaticTiledMapTile(new TextureRegion(ownerMark.get("_MINOR_"))));
        } else {
            cell.setTile(new StaticTiledMapTile(ownerMark.get("BLACK_TEX")));
        }
        ownerLayer.setCell(i, j, cell);
    }

    private void drawTextAndImageIfScanned(int i, int j, StarSystem starSystem, Major playerRace) {
        if (starSystem.getScanned(playerRace.getRaceId())) {
            if (starSystem.isSunSystem()) {
                if (starSystem.getImageIndex() != -1) {
                    Image sunImg = sunImgs.get(starSystem.getImageIndex());
                    sunImg.setVisible(true);
                }
                if (starSystem.getMinorRace() != null)
                    cache.setColor(Color.WHITE);
                else
                    cache.setColor(Color.WHITE);
                for (Iterator<Entry<String, Race>> iter = getResourceManager().getRaceController().getRaces().entries()
                        .iterator(); iter.hasNext();) {
                    Entry<String, Race> e = iter.next();
                    if (e.value.isMajor()
                            && (starSystem.isHomeOf() == e.value || (starSystem.isMajorized() && starSystem.getOwnerId().equals(
                                    e.key))) && (playerRace.isRaceContacted(e.key) || e.key.equals(playerRace.getRaceId())))
                        cache.setColor(((Major) e.value).getRaceDesign().clrGalaxySectorText);
                }

                if (!starSystem.getFullKnown(playerRace.getRaceId()))
                    cache.setColor(cache.getColor().r, cache.getColor().g, cache.getColor().b, 0.4f);
                GlyphLayout layout = new GlyphLayout(font, starSystem.getName(), cache.getColor(), GamePreferences.spriteSize,
                        Align.center, true);
                cache.addText(layout, i * GamePreferences.spriteSize,
                        (int) (j * GamePreferences.spriteSize + (GamePreferences.spriteSize - layout.height) / 3));
            } else if (starSystem.getAnomaly() != null) {
                Image sunImg = sunImgs.get(starSystem.getImageIndex());
                sunImg.setVisible(true);
            }
        }
    }

    private void drawScanLayer(TiledMapTileLayer layer, int i, int j, StarSystem starSystem, Major playerRace) {
        Cell cell = layer.getCell(i, j);
        if (cell == null)
            cell = new Cell();

        cell.setTile(new StaticTiledMapTile(ownerMark.get(ScanPower.getName(starSystem.getScanPower(playerRace.getRaceId())))));
        layer.setCell(i, j, cell);
    }

    private void drawDiploLayer(TiledMapTileLayer layer, int i, int j, StarSystem starSystem, Major playerRace) {
        Cell cell = layer.getCell(i, j);
        if (cell == null)
            cell = new Cell();
        cell.setTile(new StaticTiledMapTile(ownerMark.get(ScanPower.getName(starSystem.getScanPower(playerRace.getRaceId())))));

        boolean valid = false;
        if (starSystem.isSunSystem() && !starSystem.isFree()) {
            Race race = starSystem.getOwner();
            Minor minor = starSystem.getMinorRace();
            if (race != null && playerRace.isRaceContacted(race.getRaceId())) {
                Pair<String, Color> status = DiplomaticAgreement.getDiplomacyStatus(playerRace.getAgreement(race.getRaceId()));
                cell.setTile(new StaticTiledMapTile(ownerMark.get("DIPLO_" + status.getFirst())));
                valid = true;
            }
            if (minor != null && playerRace.isRaceContacted(minor.getRaceId())) {
                Pair<String, Color> status = DiplomaticAgreement.getDiplomacyStatus(playerRace.getAgreement(minor.getRaceId()));
                if (minor.isSubjugated()) {
                    if (race != null && race.getRaceId().equals(playerRace.getRaceId())) {
                        cell.setTile(new StaticTiledMapTile(ownerMark.get("DIPLO_SUBJUGATED")));
                        valid = true;
                    }
                } else {
                    cell.setTile(new StaticTiledMapTile(ownerMark.get("DIPLO_" + status.getFirst())));
                    valid = true;
                }
            }
        }
        if (!valid) {
            if (!starSystem.getScanned(playerRace.getRaceId()))
                cell.setTile(new StaticTiledMapTile(ownerMark.get("FOG_OF_WAR")));
            else
                cell.setTile(new StaticTiledMapTile(ownerMark.get("BLACK_TEX")));
        }
        layer.setCell(i, j, cell);
    }

    private void drawExpansionLayer(TiledMapTileLayer layer, int i, int j, StarSystem starSystem, Major playerRace) {
        Cell cell = layer.getCell(i, j);
        if (cell == null)
            cell = new Cell();

        int val = (int) (starSystem.getCompareValue(playerRace.getRaceId()) * 2.2f);
        if (val > 0 && starSystem.getFullKnown(playerRace.getRaceId())) {
            val -= 25;
            cell.setTile(new StaticTiledMapTile(ownerMark.get(ScanPower.getName(val)))); //reusing scanpower values
        } else {
            if (!starSystem.getScanned(playerRace.getRaceId()))
                cell.setTile(new StaticTiledMapTile(ownerMark.get("FOG_OF_WAR")));
            else
                cell.setTile(new StaticTiledMapTile(ownerMark.get("BLACK_TEX")));
        }
        layer.setCell(i, j, cell);
    }

    private void updateLayers() {
        MapLayers layers = map.getLayers();
        TiledMapTileLayer ownerLayer = (TiledMapTileLayer) layers.get("OWNERS");
        TiledMapTileLayer scanLayer = (TiledMapTileLayer) layers.get("SCANRANGE");
        TiledMapTileLayer diploLayer = (TiledMapTileLayer) layers.get("DIPLOMACYVIEW");
        TiledMapTileLayer expansionLayer = (TiledMapTileLayer) layers.get("EXPANSIONVIEW");
        TiledMapTileLayer gridLayer = (TiledMapTileLayer) layers.get("GRID");

        if (ownerLayer != null)
            ownerLayer.setVisible(game.getGameSettings().getShowState() == GalaxyShowState.POLITICAL);
        if (scanLayer != null)
            scanLayer.setVisible(game.getGameSettings().getShowState() == GalaxyShowState.SCANRANGE);
        if (diploLayer != null)
            diploLayer.setVisible(game.getGameSettings().getShowState() == GalaxyShowState.DIPLOMACY);
        if (expansionLayer != null)
            expansionLayer.setVisible(game.getGameSettings().getShowState() == GalaxyShowState.EXPANSION);
        if (gridLayer != null)
            gridLayer.setVisible(game.getGameSettings().drawGrid);
    }

    public void updateScreen() {
        cache.clear();
        MapLayers layers = map.getLayers();
        TiledMapTileLayer layer;
        TiledMapTileLayer ownerLayer;
        TiledMapTileLayer scanLayer;
        TiledMapTileLayer diploLayer;
        TiledMapTileLayer gridLayer;
        TiledMapTileLayer expansionLayer;
        boolean init = false;
        if (layers.getCount() == 0) {
            layer = new TiledMapTileLayer(getResourceManager().getGridSizeX(), getResourceManager().getGridSizeY(),
                    GamePreferences.spriteSize, GamePreferences.spriteSize);
            layer.setName("UNIVERSE");
            ownerLayer = new TiledMapTileLayer(getResourceManager().getGridSizeX(), getResourceManager().getGridSizeY(),
                    GamePreferences.spriteSize, GamePreferences.spriteSize);
            ownerLayer.setName("OWNERS");
            scanLayer = new TiledMapTileLayer(getResourceManager().getGridSizeX(), getResourceManager().getGridSizeY(),
                    GamePreferences.spriteSize, GamePreferences.spriteSize);
            scanLayer.setName("SCANRANGE");
            diploLayer = new TiledMapTileLayer(getResourceManager().getGridSizeX(), getResourceManager().getGridSizeY(),
                    GamePreferences.spriteSize, GamePreferences.spriteSize);
            diploLayer.setName("DIPLOMACYVIEW");
            expansionLayer = new TiledMapTileLayer(getResourceManager().getGridSizeX(), getResourceManager().getGridSizeY(),
                    GamePreferences.spriteSize, GamePreferences.spriteSize);
            expansionLayer.setName("EXPANSIONVIEW");
            gridLayer = generateGridLayer();
            layers.add(layer);
            layers.add(ownerLayer);
            layers.add(scanLayer);
            layers.add(diploLayer);
            layers.add(expansionLayer);
            layers.add(gridLayer);
            sunImgs = new Array<Image>();
            init = true;
        } else {
            layer = (TiledMapTileLayer) layers.get("UNIVERSE");
            ownerLayer = (TiledMapTileLayer) layers.get("OWNERS");
            scanLayer = (TiledMapTileLayer) layers.get("SCANRANGE");
            diploLayer = (TiledMapTileLayer) layers.get("DIPLOMACYVIEW");
            expansionLayer = (TiledMapTileLayer) layers.get("EXPANSIONVIEW");
        }
        for (int j = 0; j < getResourceManager().getGridSizeY(); j++)
            for (int i = 0; i < getResourceManager().getGridSizeX(); i++) {
                StarSystem starSystem = universeMap.getStarSystemAt(i, j);
                drawSunLayer(layer, i, j, starSystem, init);

                Major playerRace = getResourceManager().getRaceController().getPlayerRace();
                drawOwnerLayer(ownerLayer, i, j, starSystem, playerRace);
                drawTextAndImageIfScanned(i, j, starSystem, playerRace);
                drawScanLayer(scanLayer, i, j, starSystem, playerRace);
                drawDiploLayer(diploLayer, i, j, starSystem, playerRace);
                drawExpansionLayer(expansionLayer, i, j, starSystem, playerRace);

                starSystem.resetImages();
                universeMap.drawImages(universeStage, starSystem);
            }
        ((ScreenManager) game).getSidebarLeft().showInfo(true);
        if (sidebarRight.isStarSystemShown())
            sidebarRight.showStarSystemInfo();
        else if (sidebarRight.getShipRenderer().isShipMove())
            sidebarRight.refreshShips();
        if (!sidebarRight.getShipRenderer().isShipMove()) //in this case we update the ranges only when the ship is deselected
            drawRangeLines(-1);

        updateView();
    }

    public RightSideBar getRightSideBar() {
        return sidebarRight;
    }

    public boolean isShipMove() {
        return sidebarRight.getShipRenderer().isShipMove();
    }

    @Override
    public void zoomed() {
        drawRangeLines(oldShipRange);
        requestRendering();
    }

    @Override
    public void moved() {
        requestRendering();
    }

    public void setBuildTargetCoord(IntPoint origin) {
        cancelShipMove();
        showPlanets(true);
        this.origin = origin;
        target = new IntPoint();
        oldPos = new IntPoint();
        setState(GalaxyState.SET_BUILD_TARGET);
        drawRangeLines(ShipRange.LONG.getRange());
    }

    public void confirmBuildTargetCoord() {
        universeMap.setSelectedCoordValue(target);
        if (oldPos.equals(target) && !oldPos.equals(new IntPoint()) && !target.equals(new IntPoint())) {
            setState(GalaxyState.NORMAL);
            boolean confirmed = game.getRaceController().getPlayerRace().getStarMap().getRangeValue(target) > 0;
            target = !target.equals(origin) ? target : new IntPoint();
            if (confirmed) {
                universeMap.getStarSystemAt(origin).setBuildTargetCoord(target);
                game.getSoundManager().playSound(SndMgrValue.SNDMGR_SOUND_SHIPTARGET);
            } else {
                universeMap.getStarSystemAt(origin).setBuildTargetCoord(new IntPoint());
                game.getSoundManager().playSound(SndMgrValue.SNDMGR_SOUND_ERROR);
            }
            drawRangeLines(-1);
        }
    }

    public void updateView() {
        updateLayers();
        drawResourceRoutes();
    }
}