/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.loaders.TextureLoader.TextureParameter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectSet;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.ObjectSet.ObjectSetIterator;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;
import com.badlogic.gdx.utils.viewport.ScalingViewport;
import com.blotunga.bote.DefaultScreen;
import com.blotunga.bote.GamePreferences;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.CombatOrder;
import com.blotunga.bote.constants.CombatTactics;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.ShipType;
import com.blotunga.bote.events.EventScreenType;
import com.blotunga.bote.galaxy.Anomaly;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.races.Race;
import com.blotunga.bote.ships.Combat;
import com.blotunga.bote.ships.Ships;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.ui.combatview.CombatDecisionButtonType;
import com.blotunga.bote.ui.combatview.CombatOrderButtonType;
import com.blotunga.bote.ui.universemap.ShipRenderer;
import com.blotunga.bote.utils.IntPoint;
import com.blotunga.bote.utils.ui.BaseTooltip;
import com.blotunga.bote.utils.ui.ScrollEventHandler;
import com.blotunga.bote.utils.ui.VerticalScroller;
import com.blotunga.bote.utils.ui.ZoomWithLock;

public class CombatScreen extends DefaultScreen {
    protected final Stage stage;
    private final OrthographicCamera camera;
    protected Skin skin;
    protected EventScreenType type;
    protected Major playerRace;
    protected Color normalColor;
    protected Color listMarkTextColor;
    private Image background;
    private Array<String> loadedTextures;
    private Array<Ships> ships;
    private ObjectSet<String> friends;
    private ObjectSet<String> enemies;
    private boolean inOrderMenu;
    private CombatOrderButtonType selectedOrderButton;
    private TextureAtlas uiAtlas;
    final private float shipPadding = GamePreferences.sceneHeight / 36;
    private Ships selectedShip;
    private ShipRenderer ownShipRenderer;
    private int selectShipType;
    private boolean canEnd;
    protected final ZoomWithLock inputs;

    public CombatScreen(ScreenManager game) {
        super(game);
        playerRace = game.getRaceController().getPlayerRace();
        normalColor = playerRace.getRaceDesign().clrNormalText;
        listMarkTextColor = playerRace.getRaceDesign().clrListMarkTextColor;

        camera = new OrthographicCamera();
        stage = new Stage(new ScalingViewport(Scaling.fit, GamePreferences.sceneWidth, GamePreferences.sceneHeight, camera));

        Rectangle rect = GameConstants.coordsToRelative(1400, 810, 50, 50);
        inputs = new ZoomWithLock(game, camera, stage, rect);

        TextureParameter param = new TextureParameter();
        param.genMipMaps = game.useMipMaps();
        param.minFilter = game.useMipMaps() ? TextureFilter.MipMapLinearLinear : TextureFilter.Linear;
        param.magFilter = TextureFilter.Linear;

        skin = game.getSkin();
        loadedTextures = new Array<String>();
        ships = new Array<Ships>();
        friends = new ObjectSet<String>();
        enemies = new ObjectSet<String>();
        inOrderMenu = false;
        selectedOrderButton = CombatOrderButtonType.ATTACK_BUTTON;
        uiAtlas = game.getAssetManager().get("graphics/ui/general_ui.pack");
        selectedShip = null;
        ownShipRenderer = null;
        selectShipType = -1;
        canEnd = false;
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(1 / 30.0f);
        stage.draw();
        if (game.isEndRoundPressed() && canEnd) {
            canEnd = false;
            Timer.schedule(new Task() {
                @Override
                public void run() {
                    Gdx.app.debug("CombatScreen", "Running combat thread");
                    if (game.isEndRoundPressed()) {
                        game.setEndRoundPressed(false);
                        game.getUniverseMap().getSemaphore().release();
                    }
                }
            }, 0.2f);
        }
    }

    @Override
    public void show() {
        hide();
        background = new Image();
        background.setBounds(0, 0, GamePreferences.sceneWidth, GamePreferences.sceneHeight);
        stage.addActor(background);
        background.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                System.out.println("x = " + x + " y = " + y);
                return false;
            }
        });

        setInputProcessor(inputs);
        // Wait until they are finished loading
        if (game.getCurrentRound() < game.getGamePreferences().autoTurns) {
            game.setEndRoundPressed(true);
            game.getUniverseMap().setCombatOrder(CombatOrder.AUTOCOMBAT);
        }

        if (game.isEndRoundPressed()) {
            showCombatInfoMenu();
            return;
        }

        ships.clear();
        for (int i = 0; i < game.getUniverseMap().getShipMap().getSize(); i++) {
            Ships s = game.getUniverseMap().getShipMap().getAt(i);
            if (!s.getCoordinates().equals(game.getUniverseMap().getCurrentCombatSector()))
                continue;

            ships.add(s);
            for (int j = 0; j < s.getFleetSize(); j++)
                ships.add(s.getFleet().getAt(j));
        }

        Anomaly anomaly = game.getUniverseMap().getStarSystemAt(game.getUniverseMap().getCurrentCombatSector()).getAnomaly();
        double winningChance = Combat.getWinningChance(playerRace, ships, game.getRaceController(), friends, enemies,
                anomaly, false, true);

        if (friends.contains(playerRace.getRaceId())) {
            if (!inOrderMenu)
                showCombatDecisionMenu(winningChance);
            else
                showCombatOrderMenu();
        } else
            showCombatInfoMenu();
        inputs.getImage().setVisible(true);
        stage.addActor(inputs.getImage());
    }

    /**
     * Function shows the menu where the action is decided
     * @param winningChance
     */
    private void showCombatDecisionMenu(double winningChance) {
        String path = "graphics/events/CombatDec.jpg";
        TextureRegion ptex = new TextureRegion(game.loadTextureImmediate(path));
        background.setDrawable(new TextureRegionDrawable(ptex));
        loadedTextures.add(path);

        IntPoint p = game.getUniverseMap().getCurrentCombatSector();
        StarSystem ss = game.getUniverseMap().getStarSystemAt(p);
        Rectangle rect = GameConstants.coordsToRelative(0, 810, 1440, 60);
        String headline = String.format("%s %s", StringDB.getString("COMBAT_IN_SECTOR"),
                ss.coordsName(ss.getKnown(playerRace.getRaceId())));
        Table nameTable = new Table();
        nameTable.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        nameTable.align(Align.center);
        nameTable.setSkin(skin);
        stage.addActor(nameTable);
        nameTable.add(headline, "hugeFont", normalColor);

        Table friendsTable = new Table();
        rect = GameConstants.coordsToRelative(0, 735, 1440, 180);
        friendsTable.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        friendsTable.setSkin(skin);
        addRacesToTable(friendsTable, friends);
        stage.addActor(friendsTable);

        Table enemiesTable = new Table();
        rect = GameConstants.coordsToRelative(0, 475, 1440, 180);
        enemiesTable.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        enemiesTable.setSkin(skin);
        addRacesToTable(enemiesTable, enemies);
        stage.addActor(enemiesTable);

        Table againstTable = new Table();
        rect = GameConstants.coordsToRelative(0, 535, 1440, 50);
        againstTable.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        againstTable.align(Align.center);
        againstTable.setSkin(skin);
        stage.addActor(againstTable);
        againstTable.add(StringDB.getString("AGAINST"), "hugeFont", normalColor);

        String text = String.format("%s: %.0f%%", StringDB.getString("WINNING_CHANCE"), winningChance * 100);
        boolean flipHand = false;
        Color color = new Color();
        if (winningChance < 0.4) {
            path = "Win0";
            color = new Color(200 / 255.0f, 0, 0, 1.0f);
        } else if (winningChance < 0.6) {
            path = "Win40";
            color = new Color(255 / 255.0f, 100 / 255.0f, 0, 1.0f);
            flipHand = true;
        } else if (winningChance < 0.75) {
            path = "Win60";
            color = new Color(255 / 255.0f, 225 / 255.0f, 0, 1.0f);
            flipHand = true;
        } else if (winningChance < 0.9) {
            path = "Win75";
            color = new Color(200 / 255.0f, 225 / 255.0f, 0, 1.0f);
        } else {
            path = "Win90";
            color = new Color(0, 200 / 255.0f, 0, 1.0f);
        }
        Table winTable = new Table();
        rect = GameConstants.coordsToRelative(0, 275, 1440, 170);
        winTable.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        winTable.align(Align.center);
        stage.addActor(winTable);
        winTable.add(new Image(uiAtlas.findRegion(path))).width(winTable.getHeight()).height(winTable.getHeight());
        Label l = new Label(text, skin, "hugeFont", color);
        l.setAlignment(Align.center);
        winTable.add(l).width(GameConstants.wToRelative(815) - 2 * winTable.getHeight());
        TextureRegion region = new TextureRegion(uiAtlas.findRegion(path));
        region.flip(false, flipHand);
        Image thumb = new Image(region);
        winTable.add(thumb).width(winTable.getHeight()).height(winTable.getHeight());

        TextButton[] buttons = new TextButton[CombatDecisionButtonType.values().length];
        TextButtonStyle style = skin.get("default", TextButtonStyle.class);
        for (int i = 0; i < CombatDecisionButtonType.values().length; i++) {
            buttons[i] = new TextButton(StringDB.getString(CombatDecisionButtonType.values()[i].getLabel()), style);
            rect = GameConstants.coordsToRelative(350 + i * 190, 63, 170, 35);
            buttons[i].setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
            buttons[i].setUserObject(CombatDecisionButtonType.values()[i]);

            buttons[i].addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    TextButton button = (TextButton) event.getListenerActor();
                    if (!button.isDisabled()) {
                        CombatDecisionButtonType selected = (CombatDecisionButtonType) event.getListenerActor().getUserObject();
                        if (selected.getCombatOrder() == CombatOrder.USER) {
                            inOrderMenu = true;
                            show();
                        } else {
                            game.getUniverseMap().setCombatOrder(selected.getCombatOrder());
                            game.setEndRoundPressed(true);
                            hide();
                            show();
                            canEnd = true;
                        }
                    }
                }
            });
            stage.addActor(buttons[i]);
        }
    }

    private void addRacesToTable(Table table, ObjectSet<String> races) {
        String path = "";
        float imgWidth = GameConstants.wToRelative(180);
        float imgPad = GameConstants.wToRelative(20);
        float imgHeight = GameConstants.hToRelative(150);
        int i = 0;
        for (ObjectSetIterator<String> iter = races.iterator(); iter.hasNext();) {
            String entry = iter.next();
            Race race = game.getRaceController().getRace(entry);
            if (race != null) {
                Label l = new Label(race.getName(), skin, "normalFont", listMarkTextColor);
                l.setAlignment(Align.center);
                table.add(l).width(imgWidth).height(table.getHeight() - imgHeight).spaceLeft(i == 0 ? 0 : imgPad);
            }
            i++;
        }
        table.row();
        Texture tooltipTexture = game.getAssetManager().get(GameConstants.UI_BG_ROUNDED);
        i = 0;
        for (ObjectSetIterator<String> iter = races.iterator(); iter.hasNext();) {
            String entry = iter.next();
            Race race = game.getRaceController().getRace(entry);
            if (race != null) {
                path = "graphics/races/" + race.getGraphicFileName() + ".jpg";
                loadedTextures.add(path);
                Image raceImage = new Image(new TextureRegionDrawable(
                        new TextureRegion(game.loadTextureImmediate(path))));
                generateRaceTooltip(BaseTooltip.createTableTooltip(raceImage, tooltipTexture).getActor(), race);
                table.add(raceImage).width(imgWidth).height(imgHeight).spaceLeft(i == 0 ? 0 : imgPad);
            }
            i++;
        }
    }

    private void generateRaceTooltip(Table table, Race race) {
        race.getTooltip(table, skin, "xlFont", listMarkTextColor, "largeFont", normalColor, true);
    }
    /**
     * Function shows the menu where orders can be assigned to ships individually
     */
    private void showCombatOrderMenu() {
        String path = "graphics/events/CombatOrder.jpg";
        TextureRegion ptex = new TextureRegion(game.loadTextureImmediate(path));
        background.setDrawable(new TextureRegionDrawable(ptex));
        loadedTextures.add(path);

        IntPoint p = game.getUniverseMap().getCurrentCombatSector();
        StarSystem ss = game.getUniverseMap().getStarSystemAt(p);
        Rectangle rect = GameConstants.coordsToRelative(0, 810, 1440, 60);
        String headline = String.format("%s %s", StringDB.getString("COMBAT_IN_SECTOR"),
                ss.coordsName(ss.getKnown(playerRace.getRaceId())));
        Table nameTable = new Table();
        nameTable.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        nameTable.align(Align.center);
        nameTable.setSkin(skin);
        stage.addActor(nameTable);
        nameTable.add(headline, "hugeFont", normalColor);

        //---- top buttons
        final TextButton[] orderButtons = new TextButton[CombatOrderButtonType.values().length];
        Table[] buttonTable = new Table[orderButtons.length];
        TextButtonStyle style = skin.get("default", TextButtonStyle.class);
        for (int i = 0; i < CombatOrderButtonType.values().length; i++) {
            buttonTable[i] = new Table();
            rect = GameConstants.coordsToRelative(435 + i * 200, 754, 180, 75);
            float buttonHeight = GameConstants.hToRelative(35);
            float pad = GameConstants.hToRelative(2);
            buttonTable[i].setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
            Image img = new Image(new TextureRegion(uiAtlas.findRegion(CombatOrderButtonType.values()[i]
                    .getCombatTactics().getImgName())));
            buttonTable[i].add(img).height(rect.height - buttonHeight).width(rect.height - buttonHeight);
            buttonTable[i].row();
            orderButtons[i] = new TextButton(StringDB.getString(CombatOrderButtonType.values()[i].getLabel()), style);
            orderButtons[i].setUserObject(CombatOrderButtonType.values()[i]);
            if (CombatOrderButtonType.values()[i] == selectedOrderButton) {
                orderButtons[i].setDisabled(true);
                skin.setEnabled(orderButtons[i], false);
            }

            orderButtons[i].addListener(new ActorGestureListener() {
                @Override
                public void tap(InputEvent event, float x, float y, int count, int button) {
                    TextButton b = (TextButton) event.getListenerActor();
                    if (!b.isDisabled()) {
                        setToSelection((CombatOrderButtonType) event.getListenerActor().getUserObject(), orderButtons);
                    }
                    if (count == 2) {
                        if (ownShipRenderer != null)
                            ownShipRenderer.setAllTactics();
                    }
                }
            });
            buttonTable[i].add(orderButtons[i]).height((int) buttonHeight).spaceTop((int) pad);
            stage.addActor(buttonTable[i]);
        }

        Table tacticDesc = new Table();
        rect = GameConstants.coordsToRelative(350, 675, 735, 55);
        tacticDesc.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        tacticDesc.align(Align.center);
        tacticDesc.setSkin(skin);
        stage.addActor(tacticDesc);
        for (int i = 1; i <= 3; i++) {
            if (i != 1)
                tacticDesc.row();
            Label l = new Label(StringDB.getString(String.format("CHOOSE_TACTIC_DESC%d", i)), skin, "mediumFont",
                    normalColor);
            l.setAlignment(Align.center);
            tacticDesc.add(l).height((int) (tacticDesc.getHeight() / 3));
        }

        Table shipInfo = new Table();
        rect = GameConstants.coordsToRelative(0, 613, 1440, 25);
        shipInfo.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        shipInfo.align(Align.center);
        shipInfo.setSkin(skin);
        stage.addActor(shipInfo);
        Label l = new Label(StringDB.getString("FRIENDLY_SHIPS"), skin, "largeFont", listMarkTextColor);
        l.setAlignment(Align.center);
        shipInfo.add(l).width((int) shipInfo.getWidth() / 3);
        l = new Label(StringDB.getString("SHOWN_SHIPTYPES"), skin, "largeFont", normalColor);
        l.setAlignment(Align.center);
        shipInfo.add(l).width((int) shipInfo.getWidth() / 3);
        l = new Label(StringDB.getString("ENEMY_SHIPS"), skin, "largeFont", listMarkTextColor);
        l.setAlignment(Align.center);
        shipInfo.add(l).width((int) shipInfo.getWidth() / 3);

        String text = selectShipType == -1 ? StringDB.getString("ALL_SHIPS") : StringDB.getString(ShipType.fromInt(
                selectShipType).getName());
        TextButton selectShipTypeButton = new TextButton(text, style);
        rect = GameConstants.coordsToRelative(630, 580, 180, 35);
        selectShipTypeButton.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        selectShipTypeButton.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                if (button == 0) {
                    selectShipType++;
                    if (selectShipType == ShipType.ALIEN.getType())
                        selectShipType = -1;
                } else if (button == 1) {
                    selectShipType--;
                    if (selectShipType < -1)
                        selectShipType = ShipType.ALIEN.getType() - 1;
                }
                hide();
                show();
            }
        });
        stage.addActor(selectShipTypeButton);

        Table vsTable = new Table();
        rect = GameConstants.coordsToRelative(0, 395, 1440, 75);
        vsTable.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        vsTable.align(Align.center);
        vsTable.setSkin(skin);
        vsTable.add(StringDB.getString("VS"), "vsFont", normalColor);
        stage.addActor(vsTable);

        //----shiprenderer
        rect = GameConstants.coordsToRelative(10, 575, 280, 455);
        drawShipRenderer(rect, true);
        rect = GameConstants.coordsToRelative(845, 575, 280, 455);
        drawShipRenderer(rect, false);

        text = StringDB.getString("BTN_BACK");
        TextButton backButton = new TextButton(text, style);
        rect = GameConstants.coordsToRelative(540, 50, 180, 35);
        backButton.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        backButton.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                inOrderMenu = false;
                hide();
                show();
            }
        });
        stage.addActor(backButton);

        text = StringDB.getString("BTN_READY");
        TextButton fightButton = new TextButton(text, style);
        rect = GameConstants.coordsToRelative(720, 50, 180, 35);
        fightButton.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        fightButton.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                game.getUniverseMap().setCombatOrder(CombatOrder.USER);
                game.setEndRoundPressed(true);
                canEnd = true;
                inOrderMenu = false;
                hide();
                show();
            }
        });
        stage.addActor(fightButton);
    }

    private void drawShipRenderer(Rectangle rect, boolean forFriends) {
        float shipWidth = rect.width / 2 - shipPadding * 2;
        float shipHeight = shipWidth * 0.75f;
        shipHeight = Math.round(shipHeight / 20) * 20.0f;
        int shipsInPage = (int) (rect.height / shipHeight);
        shipWidth = shipHeight / 0.75f;
        float tableHeight = shipHeight * shipsInPage;
        rect = new Rectangle(rect.x, rect.y + (rect.height - tableHeight) / 2, rect.width * 2.1f, tableHeight);
        ShipRenderer shipRenderer = new ShipRenderer((ScreenManager) game, rect, shipWidth, shipHeight,
                shipPadding);
        if (forFriends)
            ownShipRenderer = shipRenderer;
        Table table = new Table();
        table.align(Align.top);
        VerticalScroller pane = new VerticalScroller(table);
        pane.setScrollingDisabled(true, false);

        int cnt = 0;
        int pageCnt = 0;
        for (int i = 0; i < ships.size; i++) {
            if (forFriends && friends.contains(ships.get(i).getOwnerId())
                    && (selectShipType == -1 || selectShipType == ships.get(i).getShipType().getType())) {
                shipRenderer.drawShip(table, ships.get(i), skin, false, true, 2);
                cnt++;
            } else if (!forFriends && enemies.contains(ships.get(i).getOwnerId())
                    && (selectShipType == -1 || selectShipType == ships.get(i).getShipType().getType())) {
                shipRenderer.drawShip(table, ships.get(i), skin, false, true, 2);
                cnt++;
            }
            if (cnt != 1 && (cnt % (shipsInPage * 2) == 1)) {
                pageCnt++;
                cnt = 1;
            }
        }
        pane.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        stage.addActor(pane);
        float oldX = rect.x;
        rect = GameConstants.coordsToRelative(190, 115, 100, 35);
        rect.x += oldX;
        TextButtonStyle style = skin.get("default", TextButtonStyle.class);
        ScrollEventHandler evhandler = new ScrollEventHandler(rect, stage, style, pane, (int) shipHeight, shipsInPage,
                pageCnt);
        pane.setEventHandler(evhandler);
        stage.draw();
        if (selectedShip != null)
            shipRenderer.selectShipInternal(selectedShip);
    }

    private boolean needEnabled(CombatOrderButtonType type) {
        if (type == selectedOrderButton)
            return false;
        else
            return true;
    }

    public void setToSelection(CombatOrderButtonType selected, TextButton[] buttons) {
        selectedOrderButton = selected;
        for (int i = 0; i < buttons.length; i++)
            if (!needEnabled(CombatOrderButtonType.values()[i])) {
                buttons[i].setDisabled(true);
                skin.setEnabled(buttons[i], false);
            } else {
                buttons[i].setDisabled(false);
                skin.setEnabled(buttons[i], true);
            }
    }

    private void showCombatInfoMenu() {
        String path = "graphics/events/CombatInfo.jpg";
        TextureRegion ptex = new TextureRegion(game.loadTextureImmediate(path));
        background.setDrawable(new TextureRegionDrawable(ptex));
        loadedTextures.add(path);

        String text;
        if (game.isEndRoundPressed())
            text = StringDB.getString("COMBATCALCULATION_IS_RUNNING");
        else
            text = StringDB.getString("OTHER_PLAYERS_IN_COMBAT");
        Table textTable = new Table();
        textTable.align(Align.top);
        Rectangle rect = GameConstants.coordsToRelative(0, 425, 1440, 405);
        textTable.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        Label l = new Label(text, skin, "xxlFont", normalColor);
        textTable.add(l);
        text = String.format("%s ...", StringDB.getString("PLEASE_WAIT"));
        l = new Label(text, skin, "xxlFont", normalColor);
        textTable.row();
        textTable.add(l).spaceTop(GameConstants.hToRelative(158));
        stage.addActor(textTable);
        if (!game.isEndRoundPressed()) {
            game.getUniverseMap().setCombatOrder(CombatOrder.NONE);
            game.setEndRoundPressed(true);
        }
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void hide() {
        for (int i = 0; i < stage.getActors().size; i++) {
            stage.getActors().get(i).setVisible(false);
            stage.getActors().get(i).remove();
        }
    }

    @Override
    public void dispose() {
        stage.dispose();
        for (String path : loadedTextures)
            if (game.getAssetManager().isLoaded(path))
                game.getAssetManager().unload(path);
        loadedTextures.clear();
        inputs.dispose();
    }

    public CombatTactics getSelectedTactics() {
        return selectedOrderButton.getCombatTactics();
    }

    public void setSelectedShip(Ships s) {
        selectedShip = s;
    }
}