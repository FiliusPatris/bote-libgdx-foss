/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.screens;

import java.nio.IntBuffer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane.ScrollPaneStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.BufferUtils;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ScalingViewport;
import com.blotunga.bote.DefaultScreen;
import com.blotunga.bote.GamePreferences;
import com.blotunga.bote.PlatformCallback;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.ui.mainmenu.AchievementMenu;
import com.blotunga.bote.ui.mainmenu.CreditsMenu;
import com.blotunga.bote.ui.mainmenu.InitialSettingsMenu;
import com.blotunga.bote.ui.mainmenu.InitialSettingsTutorialMenu;
import com.blotunga.bote.ui.mainmenu.LoadMenu;
import com.blotunga.bote.ui.mainmenu.RaceSelectionMenu;
import com.blotunga.bote.ui.mainmenu.StartMenu;
import com.blotunga.bote.utils.ui.ZoomWithLock;

public class MainMenu extends DefaultScreen implements PlatformCallback {
    enum MainMenuState {
        START_MENU,
        RACE_SELECTION,
        INITIAL_SETTINGS,
        LOADING_SCREEN,
        ACHIEVEMENTS,
        CREDITS,
        TUTORIAL
    }
    private final OrthographicCamera camera;
    private final ZoomWithLock inputs;
    private final Texture texture;
    private final Stage stage;
    private final Skin skin;
    private final Image backImg;
    private RaceSelectionMenu raceSelection;
    private StartMenu startMenu;
    private InitialSettingsMenu initialSettingsMenu;
    private InitialSettingsTutorialMenu tutorial;
    private AchievementMenu achivementMenu;
    private LoadMenu loadMenu;
    private CreditsMenu creditsMenu;
    private String extraTexture;
    private MainMenuState state;

    public MainMenu(ScreenManager screenManager) {
        super(screenManager);

        camera = new OrthographicCamera();
        ScalingViewport viewPort = new ScalingViewport(Scaling.fit, GamePreferences.sceneWidth, GamePreferences.sceneHeight, camera);
        stage = new Stage(viewPort);

        Rectangle rect = GameConstants.coordsToRelative(1390, 810, 50, 50);
        inputs = new ZoomWithLock(game, camera, stage, rect);
        Gdx.input.setInputProcessor(inputs);

        texture = game.getAssetManager().get("graphics/events/Startmenu.jpg");

        skin = screenManager.getAssetManager().get(GameConstants.UISKIN_PATH);
        TextButtonStyle style = skin.get("default", TextButtonStyle.class);

        style.font = screenManager.getScaledFont();
        TextButtonStyle disabledStyle = new TextButtonStyle(style);
        disabledStyle.fontColor = Color.BLACK;
        disabledStyle.downFontColor = Color.BLACK;
        disabledStyle.down = style.up;
        skin.add("default-disabled", disabledStyle);
        ScrollPaneStyle st = new ScrollPaneStyle();
        Sprite sp = new Sprite(screenManager.getUiTexture("sidescroll"));
        sp.setBounds(0, 0, GameConstants.wToRelative(16), GameConstants.hToRelative(16));
        st.vScroll = new SpriteDrawable(sp);
        sp = new Sprite(screenManager.getUiTexture("shippath"));
        sp.setBounds(0, 0, GameConstants.wToRelative(16), GameConstants.hToRelative(16));
        st.vScrollKnob = new SpriteDrawable(sp);
        skin.add("default", st);

        backImg = new Image(new TextureRegionDrawable(new TextureRegion(texture)));
        backImg.setFillParent(true);
        stage.addActor(backImg);

        startMenu = new StartMenu(screenManager, stage, skin);
        loadMenu = new LoadMenu((ScreenManager) game, stage, skin);
        creditsMenu = new CreditsMenu(screenManager, stage);

        stage.addActor(inputs.getImage()); //this has to be added after all the background images
        extraTexture = "";
        game.getPlatformApiIntegration().setCallback(this);
        state = MainMenuState.START_MENU;
    }

    public void showInitialSettingsMenu() {
        hideAll();
        backImg.setColor(new Color(0.5f, 0.5f, 0.5f, 1.0f));
        backImg.setDrawable(new TextureRegionDrawable(new TextureRegion((Texture) game.getAssetManager().get("graphics/events/Startmenu.jpg"))));
        initialSettingsMenu = new InitialSettingsMenu((ScreenManager) game, stage);
        initialSettingsMenu.show();
        state = MainMenuState.INITIAL_SETTINGS;
    }

    public void showRaceSelection() {
        hideAll();
        if (raceSelection != null) {
            raceSelection.dispose();
            raceSelection = null;
        }
        raceSelection = new RaceSelectionMenu((ScreenManager) game, stage, skin);
        do {
            //wait around for thread to finish
        } while (!game.isInitialized());
        raceSelection.show();
        state = MainMenuState.RACE_SELECTION;
    }

    public void showTutorial() {
        String prefix = game.getRaceController().getPlayerRace().getPrefix(); //this should be initialized as only after the race is selected is this active
        String path = "graphics/backgrounds/" + prefix + "top5menu" + ".jpg";
        if(!extraTexture.isEmpty() && !extraTexture.equals(path) && game.getAssetManager().isLoaded(extraTexture))
            game.getAssetManager().unload(extraTexture);
        if (initialSettingsMenu != null) {
            initialSettingsMenu.hide();
        }
        backImg.setDrawable(new TextureRegionDrawable(new TextureRegion(game.loadTextureImmediate(path))));
        tutorial = new InitialSettingsTutorialMenu(game, stage);
        tutorial.show();
        state = MainMenuState.TUTORIAL;
    }

    public void showLoadMenu() {
        hideAll();
        backImg.setColor(new Color(0.5f, 0.5f, 0.5f, 1.0f));
        loadMenu.show();
        state = MainMenuState.LOADING_SCREEN;
    }

    public void showMainMenu() {
        hideAll();
        startMenu.show();
        state = MainMenuState.START_MENU;
    }

    public void showCreditsMenu() {
        hideAll();
        creditsMenu.show();
        state = MainMenuState.CREDITS;
    }

    public void showAchievementMenu() {
        hideAll();
        achivementMenu = new AchievementMenu((ScreenManager) game, skin, stage);
        backImg.setColor(new Color(0.3f, 0.3f, 0.3f, 1.0f));
        achivementMenu.show();
        state = MainMenuState.ACHIEVEMENTS;
    }

    public void hideAll() {
        backImg.setColor(Color.WHITE);
        startMenu.hide();
        loadMenu.hide();
        creditsMenu.hide();
        if (raceSelection != null) {
            raceSelection.hide();
        }
        if (initialSettingsMenu != null) {
            initialSettingsMenu.hide();
            initialSettingsMenu.dispose();
            initialSettingsMenu = null;
        }
        if (tutorial != null) {
            tutorial.hide();
            tutorial = null;
        }
        if(achivementMenu != null) {
           achivementMenu.hide();
           achivementMenu.dispose();
           achivementMenu = null;
        }
        inputs.resetPositionAndZoom();
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
        stage.draw();
        if (creditsMenu.canScroll())
            creditsMenu.scroll();
    }

    @Override
    public void dispose() {
        Gdx.app.postRunnable(new Runnable() {
            private final Runnable _self = this;
            private int counter = 10;

            @Override
            public void run() {
                if (counter-- > 0) {
                    Gdx.app.postRunnable(_self);
                    return;
                }
                System.out.println("Dispose mainmenu");
                if(game.getAssetManager().isLoaded("graphics/events/Startmenu.jpg"))
                    game.getAssetManager().unload("graphics/events/Startmenu.jpg");
                if(!extraTexture.isEmpty() && game.getAssetManager().isLoaded(extraTexture))
                    game.getAssetManager().unload(extraTexture);
                stage.dispose();
                inputs.dispose();
                game.getPlatformApiIntegration().setCallback(null);
            }
        });
    }

    private void restoreState() {
        switch (state) {
            case START_MENU:
                showMainMenu();
                break;
            case ACHIEVEMENTS:
                showAchievementMenu();
                break;
            case CREDITS:
                showCreditsMenu();
                break;
            case INITIAL_SETTINGS:
                showInitialSettingsMenu();
                break;
            case RACE_SELECTION:
                showRaceSelection();
                break;
            case LOADING_SCREEN:
                showLoadMenu();
                break;
            case TUTORIAL:
                showTutorial();
                break;
        }
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void show() {
        showMainMenu();
        IntBuffer buf = BufferUtils.newIntBuffer(16);
        Gdx.gl.glGetIntegerv(GL20.GL_MAX_TEXTURE_SIZE, buf);
        final int maxSize = buf.get(0);
        Thread th = new Thread(new Runnable() {
            @Override
            public void run() {
                game.Init(maxSize);
            }
        });
        th.start();
        game.setGameLoaded("");
    }

    @Override
    public void hide() {
        hideAll();
    }

    @Override
    public void call() {
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                restoreState();
            }
        });
    }
}
