/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.screens;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.ArrayMap;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.ui.tradeview.TradeBottomView;
import com.blotunga.bote.ui.tradeview.TradeEmptyMonopolView;
import com.blotunga.bote.ui.tradeview.TradeExchangeView;
import com.blotunga.bote.ui.tradeview.TradeMonopolView;
import com.blotunga.bote.ui.tradeview.TradeTransferView;
import com.blotunga.bote.ui.tradeview.TradeViewMainButtonType;
import com.blotunga.bote.utils.IntPoint;

public class TradeScreen extends ZoomableScreen {
    private TradeViewMainButtonType selectedButton;
    private TextButton[] buttons;
    private int numButtons = TradeViewMainButtonType.values().length;
    private Major playerRace;
    private IntPoint lastSystem;
    private TradeExchangeView tradeExchangeView;
    private TradeEmptyMonopolView tradeEmptyMonopolView;
    private TradeMonopolView tradeMonopolView;
    private TradeTransferView tradeTransferView;
    private TradeBottomView tradeBottomView;

    public TradeScreen(final ScreenManager screenManager) {
        super(screenManager, "trademenu", "tradeV3");
        lastSystem = new IntPoint();
        playerRace = screenManager.getRaceController().getPlayerRace();
        float xOffset = sidebarLeft.getPosition().getWidth();
        float yOffset = backgroundBottom.getHeight();
        buttons = new TextButton[numButtons];

        tradeExchangeView = new TradeExchangeView(screenManager, mainStage, skin, xOffset, yOffset);
        tradeEmptyMonopolView = new TradeEmptyMonopolView(screenManager, mainStage, skin, xOffset, yOffset);
        tradeMonopolView = new TradeMonopolView(screenManager, mainStage, skin, xOffset, yOffset);
        tradeTransferView = new TradeTransferView(screenManager, mainStage, skin, xOffset, yOffset);
        tradeBottomView = new TradeBottomView(screenManager, mainStage, skin, xOffset);

        TextButtonStyle style = skin.get("default", TextButtonStyle.class);
        selectedButton = TradeViewMainButtonType.EXCHANGE_BUTTON;
        for (int i = 0; i < numButtons; i++) {
            buttons[i] = new TextButton(StringDB.getString(TradeViewMainButtonType.values()[i].getLabel()), style);
            Rectangle rect = GameConstants.coordsToRelative(310 + i * 200, 50, 180, 35);
            buttons[i].setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width,
                    (int) rect.height);
            buttons[i].setUserObject(TradeViewMainButtonType.values()[i]);

            if (!needEnabled(TradeViewMainButtonType.values()[i])) {
                buttons[i].setDisabled(true);
                skin.setEnabled(buttons[i], false);
            }

            buttons[i].addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    TextButton button = (TextButton) event.getListenerActor();
                    if (!button.isDisabled()) {
                        TradeViewMainButtonType selected = (TradeViewMainButtonType) event.getListenerActor()
                                .getUserObject();
                        setSubMenu(selected.getId());
                    }
                }
            });
            mainStage.addActor(buttons[i]);
        }
    }

    @Override
    public void show() {
        super.show();
        ScreenManager screenManager = (ScreenManager) game;
        IntPoint coord = screenManager.getUniverseMap().getSelectedCoordValue();
        if (!coord.equals(new IntPoint())) {
            StarSystem system = screenManager.getUniverseMap().getStarSystemAt(coord);
            Major major = screenManager.getRaceController().getPlayerRace();
            if (!system.getOwnerId().equals(major.getRaceId()) || !system.isSunSystem() || !system.isMajorized()) {
                if (!lastSystem.equals(new IntPoint()) && !lastSystem.equals(coord))
                    screenManager.getUniverseMap().setSelectedCoordValue(lastSystem);
                else
                    screenManager.getUniverseMap().setSelectedCoordValue(major.getEmpire().getSystemList().get(0));
            }
            coord = screenManager.getUniverseMap().getSelectedCoordValue();
            system = screenManager.getUniverseMap().getStarSystemAt(coord);

            if (system.getOwnerId().equals(major.getRaceId()) && system.isSunSystem() && system.isMajorized()) {
                lastSystem = new IntPoint(system.getCoordinates());
            }

            sidebarLeft.setStarSystemInfo(system);
            tradeExchangeView.setStarSystem(system);
            tradeTransferView.setStarSystem(system);

            if (selectedButton == TradeViewMainButtonType.EXCHANGE_BUTTON) {
                setBackground(selectedButton.getBgImage());
                tradeExchangeView.show();
            } else if (selectedButton == TradeViewMainButtonType.MONOPOLY_BUTTON) {
                int otherEmpiresInGame = 0;
                int otherKnownEmpires = 0;
                ArrayMap<String, Major> majors = game.getRaceController().getMajors();
                for (int i = 0; i < majors.size; i++) {
                    Major other = majors.getValueAt(i);
                    if (other.getEmpire().countSystems() > 0 && !other.getRaceId().equals(playerRace.getRaceId())) {
                        otherEmpiresInGame++;
                        if (playerRace.isRaceContacted(other.getRaceId()))
                            otherKnownEmpires++;
                    }
                }
                if (otherKnownEmpires > ((float) otherEmpiresInGame / 2)) {
                    setBackground(selectedButton.getBgImage());
                    tradeMonopolView.show();
                } else {
                    setBackground("emptyscreen");
                    tradeEmptyMonopolView.show();
                }
            } else if (selectedButton == TradeViewMainButtonType.TRANSFERS_BUTTON) {
                setBackground(selectedButton.getBgImage());
                tradeTransferView.show();
            }
            tradeBottomView.setStarSystem(system);
            tradeBottomView.show();
        }
    }

    @Override
    public void hide() {
        super.hide();
        tradeExchangeView.hide();
        tradeEmptyMonopolView.hide();
        tradeMonopolView.hide();
        tradeTransferView.hide();
        tradeBottomView.hide();
    }

    private boolean needEnabled(TradeViewMainButtonType type) {
        if (type == selectedButton)
            return false;
        return true;
    }

    private void setToSelection(TradeViewMainButtonType selected) {
        selectedButton = selected;
        for (int i = 0; i < numButtons; i++)
            if (!needEnabled(TradeViewMainButtonType.values()[i])) {
                buttons[i].setDisabled(true);
                skin.setEnabled(buttons[i], false);
            } else {
                buttons[i].setDisabled(false);
                skin.setEnabled(buttons[i], true);
            }
        hide(); //redraw...
        show();
    }

    @Override
    public void setSubMenu(int id) {
        super.setSubMenu(id);
        setToSelection(TradeViewMainButtonType.fromInt(id));
    }
}
