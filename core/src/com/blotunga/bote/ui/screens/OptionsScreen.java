/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.Align;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.SndMgrValue;
import com.blotunga.bote.constants.ViewTypes;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.ui.optionsview.LoadSaveWidget;
import com.blotunga.bote.ui.optionsview.OptionsButtonType;
import com.blotunga.bote.utils.ui.PercentageWidget;
import com.blotunga.bote.utils.ui.ValueChangedEvent;

public class OptionsScreen extends ZoomableScreen implements ValueChangedEvent {
    private Table nameTable;
    private LoadSaveWidget saveLoadWidget;
    private Table mainTable;
    private Table buttonTable;
    private TextureRegion lineDrawable;
    private Color normalColor;

    public OptionsScreen(final ScreenManager screenManager) {
        super(screenManager, "top5menu", "");
        final float xOffset = sidebarLeft.getPosition().width;
        Major playerRace = screenManager.getRaceController().getPlayerRace();
        normalColor = playerRace.getRaceDesign().clrNormalText;
        lineDrawable = new TextureRegion(game.getAssetManager().get(GameConstants.UI_BG_SIMPLE, Texture.class));

        nameTable = new Table();
        Rectangle rect = GameConstants.coordsToRelative(200, 790, 800, 40);
        nameTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        nameTable.align(Align.center);
        nameTable.setSkin(skin);
        mainStage.addActor(nameTable);
        String s = StringDB.getString("BTN_OPTIONS");
        nameTable.add(s, "hugeFont", normalColor);
        nameTable.setVisible(false);

        saveLoadWidget = new LoadSaveWidget(screenManager, mainStage, skin, xOffset, 0,
                playerRace.getRaceDesign().clrNormalText, playerRace.getRaceDesign().clrListMarkTextColor);

        rect = GameConstants.coordsToRelative(230, 650, 870, 500);
        mainTable = new Table();
        mainStage.addActor(mainTable);
        mainTable.setBounds((int) (rect.x + xOffset), rect.y, rect.width, rect.height);
        mainTable.align(Align.top);
        mainTable.setVisible(false);

        rect = GameConstants.coordsToRelative(55, 650, 160, OptionsButtonType.values().length * 45);
        buttonTable = new Table();
        mainStage.addActor(buttonTable);
        buttonTable.setBounds((int) (rect.x + xOffset), rect.y, rect.width, rect.height);
        buttonTable.align(Align.top);
        buttonTable.setVisible(true);

        for (int i = 0; i < OptionsButtonType.values().length; i++) {
            OptionsButtonType type = OptionsButtonType.values()[i];
            String text = "";
            if (type == OptionsButtonType.AUTOSAVE)
                text = screenManager.getGameSettings().autoSave ? StringDB.getString("DISABLE") : StringDB
                        .getString("ENABLE");
            if (type == OptionsButtonType.STICKY_SCREEN)
                text = game.getGameSettings().stickyScreen ? StringDB.getString("OFF") : StringDB
                        .getString("ON");
            if (type == OptionsButtonType.PRELOAD)
                text = screenManager.getGameSettings().isPreload() ? StringDB.getString("DISABLE") : StringDB
                        .getString("ENABLE");
            text = StringDB.getString(type.getLabel(), true, text);
            TextButton button = new TextButton(text, skin, "default");
            button.setName(type.getLabel());
            button.setUserObject(type);
            button.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    TextButton b = (TextButton) event.getListenerActor();
                    OptionsButtonType bt = (OptionsButtonType) event.getListenerActor().getUserObject();
                    String text = "";
                    switch (bt) {
                        case PRELOAD:
                            if (game.getGameSettings().isPreload())
                                game.getGameSettings().setDisablePreload();
                            else
                                game.getGameSettings().setEnablePreload();
                            text = game.getGameSettings().isPreload() ? StringDB.getString("DISABLE") : StringDB.getString("ENABLE");
                            break;
                        case AUTOSAVE:
                            game.getGameSettings().autoSave = !game.getGameSettings().autoSave;
                            text = game.getGameSettings().autoSave ? StringDB.getString("DISABLE") : StringDB
                                    .getString("ENABLE");
                            break;
                        case STICKY_SCREEN:
                            if (!game.getGameSettings().stickyScreen) {
                                Rectangle rect = new Rectangle(b.getX() + xOffset, GameConstants.hToRelative(810) - b.getY() - b.getHeight(), GameConstants.wToRelative(450), GameConstants.hToRelative(250));
                                showDialog(StringDB.getString("STICKY_QUERY"), rect, bt);
                            } else {
                                game.getGameSettings().stickyScreen = !game.getGameSettings().stickyScreen;
                                text = game.getGameSettings().stickyScreen ? StringDB.getString("OFF") : StringDB
                                    .getString("ON");
                            }
                        default:
                            hide();
                            show(bt);
                            break;
                    }
                    text = StringDB.getString(bt.getLabel(), true, text);
                    b.setText(text);
                }
            });

            if(type != OptionsButtonType.PRELOAD || (type == OptionsButtonType.PRELOAD) && !game.getGameSettings().isLowMemProfile()) {
                buttonTable.add(button).width(GameConstants.wToRelative(160)).height(GameConstants.hToRelative(40)).spaceBottom(GameConstants.hToRelative(15));
                buttonTable.row();
            }
        }
    }

    private void showDialog(String text, final Rectangle rect, final OptionsButtonType type) {
        Label label1 = new Label(text, skin, "largeFont", normalColor);
        label1.setAlignment(Align.center);
        label1.setWrap(true);

        TextButton btnYes = new TextButton(StringDB.getString("YES", true), skin, "default");
        TextButton btnNo = new TextButton(StringDB.getString("NO", true), skin, "default");

        final Dialog dialog = new Dialog("", skin) {
            @Override
            public float getPrefWidth() {
                return rect.width;
            }

            @Override
            public float getPrefHeight() {
                return rect.height;
            }
        };
        dialog.setModal(true);
        dialog.setMovable(false);
        dialog.setResizable(false);

        btnYes.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y,
                    int pointer, int button) {

                String text = "";
                switch (type) {
                    case STICKY_SCREEN:
                        game.getGameSettings().stickyScreen = !game.getGameSettings().stickyScreen;
                        text = game.getGameSettings().stickyScreen ? StringDB.getString("OFF") : StringDB
                                .getString("ON");
                        break;
                    default:
                        break;
                }
                text = StringDB.getString(type.getLabel(), true, text);
                ((TextButton) buttonTable.findActor(type.getLabel())).setText(text);

                dialog.hide();
                dialog.cancel();
                dialog.remove();

                return true;
            }

        });

        btnNo.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y,
                    int pointer, int button) {

                String text = "";
                switch (type) {
                    case STICKY_SCREEN:
                        text = game.getGameSettings().stickyScreen ? StringDB.getString("OFF") : StringDB
                                .getString("ON");
                        break;
                    default:
                        break;
                }
                text = StringDB.getString(type.getLabel(), true, text);
                ((TextButton) buttonTable.findActor(type.getLabel())).setText(text);

                dialog.cancel();
                dialog.hide();

                return true;
            }

        });

        TextureRegion myTex = new TextureRegion(game.getAssetManager().get(GameConstants.UI_BG_ROUNDED, Texture.class));
        Sprite sp = new Sprite(myTex);
        sp.setColor(new Color(0, 0, 0, 0.85f));
        dialog.setBackground(new SpriteDrawable(sp));

        float btnWidth = GameConstants.wToRelative(110);
        float btnHeight = GameConstants.hToRelative(35);
        Table t = new Table();

        dialog.getContentTable().add(label1).padTop(GameConstants.hToRelative(40)).width(rect.width - GameConstants.wToRelative(25));

        t.add(btnNo).width(btnWidth).height(btnHeight).spaceRight(GameConstants.wToRelative(90));
        t.add(btnYes).width(btnWidth).height(btnHeight);

        dialog.getButtonTable().add(t).center().padBottom(GameConstants.hToRelative(20));
        dialog.show(mainStage).setPosition(rect.x, rect.y);
        dialog.setName(text);
        mainStage.addActor(dialog);
    }

    @Override
    public void show() {
        show(null);
    }

    public void show(OptionsButtonType type) {
        super.show();
        nameTable.setVisible(true);
        if (type != null) {
            switch (type) {
                case SOUND:
                    showSoundSettings();
                    break;
                case SAVE:
                    saveLoadWidget.show(true);
                    break;
                case LOAD:
                    saveLoadWidget.show(false);
                    break;
                case EXIT:
                    showExitDialog();
                    break;
                default:
                    break;
            }
        }
    }

    private void showSoundSettings() {
        mainTable.clear();
        float widgetHeight = GameConstants.hToRelative(25);
        float widgetPad = GameConstants.wToRelative(3);
        float widgetWidth = GameConstants.wToRelative(12);
        Label label = new Label(StringDB.getString("MUSIC_VOLUME") + "("
                + (int) (game.getGameSettings().musicVolume * 100) + "%)", skin, "normalFont", normalColor);
        mainTable.add(label);
        mainTable.row();
        PercentageWidget musicWidget = new PercentageWidget(0, skin, widgetWidth, widgetHeight, widgetPad, 50, 0, 100,
                (int) (game.getGameSettings().musicVolume * 100), lineDrawable, this);
        mainTable.add(musicWidget.getWidget());
        mainTable.row();
        label = new Label(StringDB.getString("SOUND_VOLUME") + "(" + (int) (game.getGameSettings().effectsVolume * 100)
                + "%)", skin, "normalFont", normalColor);
        mainTable.add(label);
        mainTable.row();
        PercentageWidget soundWidget = new PercentageWidget(1, skin, widgetWidth, widgetHeight, widgetPad, 50, 0, 100,
                (int) (game.getGameSettings().effectsVolume * 100), lineDrawable, this);
        mainTable.add(soundWidget.getWidget());
        mainTable.setVisible(true);
    }

    private void showExitDialog() {
        float buttonWidth = GameConstants.wToRelative(160);
        float buttonHeight = GameConstants.hToRelative(35);
        mainTable.clear();
        Label label = new Label(StringDB.getString("EXIT_QUERY"), skin, "xlFont", normalColor);
        label.setWrap(true);
        label.setAlignment(Align.center);
        mainTable.add(label).width(mainTable.getWidth()).spaceBottom(GameConstants.hToRelative(20));
        mainTable.row();
        Table table = new Table();
        TextButton button = new TextButton(StringDB.getString("BTN_EXIT_MAIN"), skin, "default");
        button.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                ((ScreenManager) game).setView(ViewTypes.MAIN_MENU, true);
            }
        });
        table.add(button).width(buttonWidth).height(buttonHeight).expand();
        button = new TextButton(StringDB.getString("LEAVE"), skin, "default");
        button.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.exit();
            }
        });
        table.add(button).width(buttonWidth).height(buttonHeight).expand();
        button = new TextButton(StringDB.getString("BTN_CANCEL"), skin, "default");
        button.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                hide();
                show();
            }
        });
        table.add(button).width(buttonWidth).height(buttonHeight).expand();
        mainTable.add(table).width(mainTable.getWidth());
        mainTable.setVisible(true);
    }

    @Override
    public void hide() {
        super.hide();
        saveLoadWidget.hide();
        nameTable.setVisible(false);
        mainTable.setVisible(false);
        game.saveGameSettings();
    }

    @Override
    public void valueChanged(int typeID, int value) {
        if (typeID == 0) {
            game.getGameSettings().musicVolume = value / 100.0f;
            game.getMusic().setVolume(game.getGameSettings().musicVolume);
        } else if (typeID == 1) {
            game.getGameSettings().effectsVolume = value / 100.0f;
            game.getSoundManager().playSound(SndMgrValue.SNDMGR_SOUND_SHIPTARGET);
        }
        show(OptionsButtonType.SOUND);
    }
}
