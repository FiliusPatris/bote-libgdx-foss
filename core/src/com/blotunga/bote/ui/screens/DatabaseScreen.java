/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.InputEvent.Type;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.PlayerRaces;
import com.blotunga.bote.constants.ResearchType;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.races.Minor;
import com.blotunga.bote.races.Research;
import com.blotunga.bote.races.ResearchInfo;
import com.blotunga.bote.ships.Ship;
import com.blotunga.bote.ships.ShipInfo;
import com.blotunga.bote.starsystem.BuildingInfo;
import com.blotunga.bote.troops.TroopInfo;
import com.blotunga.bote.ui.databaseview.DatabaseMainButtonType;
import com.blotunga.bote.ui.databaseview.DatabaseObjectButtonType;
import com.blotunga.bote.utils.ui.BaseTooltip;

public class DatabaseScreen extends ZoomableScreen {
    private ScreenManager manager;
    private Major playerRace;
    private Table nameTable;
    private Table mainButtonTable;
    private Table buttonTable; // the buttons with the different fields/object types
    private Image objectImage; // image of the selected object
    private Table objectTable;
    private ScrollPane objectList; //holds the currently affected objects
    private Table statsTable;
    private ScrollPane stats; //holds the stats of the object
    private Table descriptionTable;
    private ScrollPane description; //holds the description of the object
    private Table statusTable;
    private Table requiredTextTable;
    private ScrollPane requirements;
    private Table requirementsTable;
    private int buttonHeight;
    private int buttonWidth;
    private int vpad;
    private int hpad;
    private TextButtonStyle buttonStyle;
    private Button objectSelection = null;
    private Color normalColor;
    private Color markColor;
    private Color oldColor;
    private TextureRegion selectTexture;
    private DatabaseMainButtonType objectsOrTech;
    private DatabaseObjectButtonType selectedObjectType;
    private ResearchType selectedResearchType;
    private Array<String> loadedTextures;

    //tooltip
    private Color tooltipHeaderColor;
    private Color tooltipTextColor;
    private String tooltipHeaderFont = "xlFont";
    private String tooltipTextFont = "largeFont";
    private Texture tooltipTexture;
    private int selectedIdOrLevel;

    private boolean visible;

    public DatabaseScreen(ScreenManager screenManager) {
        super(screenManager, "top5menu", "researchV3");
        this.manager = screenManager;
        playerRace = screenManager.getRaceController().getPlayerRace();
        selectTexture = manager.getUiTexture("listselect");
        normalColor = playerRace.getRaceDesign().clrNormalText;
        markColor = playerRace.getRaceDesign().clrListMarkTextColor;
        tooltipHeaderColor = playerRace.getRaceDesign().clrListMarkTextColor;
        tooltipTextColor = playerRace.getRaceDesign().clrNormalText;

        float xOffset = sidebarLeft.getPosition().getWidth();
        float yOffset = (int) bottomHeight;
        vpad = (int) GameConstants.hToRelative(15);
        hpad = (int) GameConstants.wToRelative(160);
        buttonHeight = (int) GameConstants.hToRelative(35);
        buttonWidth = (int) GameConstants.wToRelative(160);
        buttonStyle = skin.get("default", TextButtonStyle.class);

        nameTable = new Table();
        Rectangle rect = GameConstants.coordsToRelative(400, 800, 400, 40);
        nameTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        nameTable.align(Align.center);
        nameTable.setSkin(skin);
        mainStage.addActor(nameTable);
        nameTable.add(StringDB.getString("BTN_DATABASE"), "hugeFont", normalColor);
        nameTable.setVisible(false);

        mainButtonTable = new Table();
        rect = GameConstants.coordsToRelative(362, 565, 480, 35);
        mainButtonTable.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        mainButtonTable.align(Align.center);
        mainStage.addActor(mainButtonTable);
        mainButtonTable.setVisible(false);
        for (int i = 0; i < DatabaseMainButtonType.values().length; i++) {
            TextButton button = new TextButton(StringDB.getString(DatabaseMainButtonType.values()[i].getLabel()), buttonStyle);
            button.setUserObject(DatabaseMainButtonType.values()[i]);
            mainButtonTable.add(button).spaceLeft(hpad).height(buttonHeight).width(buttonWidth);
            button.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    setToSelection((DatabaseMainButtonType) event.getListenerActor().getUserObject());
                    selectedIdOrLevel = 0;
                    show();
                }
            });
        }

        buttonTable = new Table();
        rect = GameConstants.coordsToRelative(55, 500, 160, 420);
        buttonTable.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        buttonTable.align(Align.center);
        mainStage.addActor(buttonTable);
        buttonTable.setVisible(false);

        objectImage = new Image();
        rect = GameConstants.coordsToRelative(225, 500, 300, 200);
        objectImage.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        mainStage.addActor(objectImage);
        objectImage.setVisible(false);
        tooltipTexture = manager.getAssetManager().get(GameConstants.UI_BG_ROUNDED);
        objectImage.setUserObject(BaseTooltip.createTableTooltip(objectImage, tooltipTexture).getActor());

        descriptionTable = new Table();
        descriptionTable.align(Align.top);
        description = new ScrollPane(descriptionTable, skin);
        description.setVariableSizeKnobs(false);
        description.setFadeScrollBars(false);
        description.setScrollingDisabled(true, false);
        rect = GameConstants.coordsToRelative(225, 300, 300, 220);
        description.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        mainStage.addActor(description);
        description.setVisible(false);

        statsTable = new Table();
        statsTable.align(Align.top);
        stats = new ScrollPane(statsTable, skin);
        stats.setVariableSizeKnobs(false);
        stats.setFadeScrollBars(false);
        stats.setScrollingDisabled(true, false);
        rect = GameConstants.coordsToRelative(535, 500, 300, 420);
        stats.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        mainStage.addActor(stats);
        stats.setVisible(false);

        objectTable = new Table();
        objectTable.align(Align.top);
        objectList = new ScrollPane(objectTable, skin);
        objectList.setVariableSizeKnobs(false);
        objectList.setFadeScrollBars(false);
        objectList.setScrollingDisabled(true, false);
        rect = GameConstants.coordsToRelative(845, 500, 300, 420);
        objectList.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        mainStage.addActor(objectList);
        objectList.setVisible(false);

        statusTable = new Table();
        rect = GameConstants.coordsToRelative(230, 53, 610, 40);
        statusTable.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        statusTable.align(Align.center);
        mainStage.addActor(statusTable);
        statusTable.setVisible(false);

        requiredTextTable = new Table();
        rect = GameConstants.coordsToRelative(55, 160, 200, 25);
        requiredTextTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height); //this is on the bottom image - no yOffset
        requiredTextTable.align(Align.left);
        mainStage.addActor(requiredTextTable);
        requiredTextTable.setVisible(false);

        requirementsTable = new Table();
        requirementsTable.align(Align.left);
        requirements = new ScrollPane(requirementsTable, skin);
        requirements.setVariableSizeKnobs(false);
        requirements.setFadeScrollBars(false);
        requirements.setScrollingDisabled(false, true);
        rect = GameConstants.coordsToRelative(55, 135, 1080, 121);
        requirements.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height); //this is on the bottom image - no yOffset
        mainStage.addActor(requirements);
        requirements.setVisible(false);

        objectsOrTech = DatabaseMainButtonType.TECH_FIELDS;
        selectedObjectType = DatabaseObjectButtonType.RESEARCH;
        selectedResearchType = ResearchType.BIO;
        selectedIdOrLevel = 0;
        loadedTextures = new Array<String>();
        visible = false;
    }

    @Override
    public void show() {
        super.show();
        visible = true;
        nameTable.setVisible(true);
        mainButtonTable.setVisible(true);
        buttonTable.clear();
        objectTable.clear();
        descriptionTable.clear();
        statsTable.clear();

        if (objectsOrTech == DatabaseMainButtonType.OBJECT_DATABASE) {
            for (int i = 0; i < DatabaseObjectButtonType.values().length; i++) {
                TextButton button = new TextButton(StringDB.getString(DatabaseObjectButtonType.values()[i].getLabel(), true),
                        buttonStyle);
                button.setUserObject(DatabaseObjectButtonType.values()[i]);
                buttonTable.add(button).spaceBottom(vpad).width(buttonTable.getWidth()).height(buttonHeight);
                buttonTable.row();
                button.addListener(new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        setToSelection((DatabaseObjectButtonType) event.getListenerActor().getUserObject());
                        selectedIdOrLevel = 0;
                        show();
                    }
                });
            }

            populateObjectList();
        } else if (objectsOrTech == DatabaseMainButtonType.TECH_FIELDS) {
            for (int i = 0; i < ResearchType.UNIQUE.getType(); i++) {
                TextButton button = new TextButton(StringDB.getString(ResearchType.fromIdx(i).getKey()), buttonStyle);
                button.setUserObject(ResearchType.fromIdx(i));
                buttonTable.add(button).spaceBottom(vpad).width(buttonTable.getWidth()).height(buttonHeight);
                buttonTable.row();
                button.addListener(new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        selectedIdOrLevel = 0;
                        setToSelection((ResearchType) event.getListenerActor().getUserObject());
                        show();
                    }
                });
            }

            populateResearchList();
        }

        buttonTable.setVisible(true);
        objectImage.setVisible(true);
        description.setVisible(true);
        stats.setVisible(true);
        objectList.setVisible(true);
        statusTable.setVisible(true);
        requiredTextTable.setVisible(true);
        requirements.setVisible(true);

        //activate things
        setToSelection(objectsOrTech);
        if (objectsOrTech == DatabaseMainButtonType.OBJECT_DATABASE)
            setToSelection(selectedObjectType);
        else
            setToSelection(selectedResearchType);

        mainStage.setKeyboardFocus(objectTable);
        objectTable.addListener(new InputListener() {
            @Override
            public boolean keyDown (InputEvent event, final int keycode) {
                int limit = objectTable.getCells().size;
                int selectedItem = Integer.parseInt(objectSelection.getName());
                if (limit < selectedItem)
                    selectedItem = limit - 1;
                Button b = objectTable.findActor("" + selectedItem);
                switch (keycode) {
                    case Keys.DOWN:
                        selectedItem++;
                        break;
                    case Keys.UP:
                        selectedItem--;
                        break;
                    case Keys.HOME:
                        selectedItem = 0;
                        break;
                    case Keys.END:
                        selectedItem = limit - 1;
                        break;
                    case Keys.PAGE_DOWN:
                        selectedItem += objectList.getScrollHeight() / b.getHeight();
                        break;
                    case Keys.PAGE_UP:
                        selectedItem -= objectList.getScrollHeight() / b.getHeight();
                        break;
                }

                if (DatabaseScreen.this.visible) {
                    if (selectedItem >= limit)
                        selectedItem = limit - 1;
                    if (selectedItem < 0)
                        selectedItem = 0;

                    b = objectTable.findActor("" + selectedItem);
                    showInfo(b);

                    Thread th = new Thread() {
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(150);
                            } catch (InterruptedException e) {
                            }
                            if (Gdx.input.isKeyPressed(keycode)) {
                                Gdx.app.postRunnable(new Runnable() {
                                    @Override
                                    public void run() {
                                        InputEvent event = new InputEvent();
                                        event.setType(Type.keyDown);
                                        event.setKeyCode(keycode);
                                        objectTable.fire(event);
                                    }
                                });
                            }
                        }
                    };
                    th.start();
                }

                return false;
            }
        });

        mainStage.draw();
        Button button = null;
        for (int i = 0; i < objectTable.getCells().size; i++) {
            button = (Button) objectTable.getCells().get(i).getActor();
            int idOrLevel = getObjectId(button);
            if (selectedIdOrLevel == idOrLevel) {
                showInfo(button);
                objectList.setScrollY(i * button.getHeight());
                break;
            }
        }

        if (objectTable.getCells().size > 0 && selectedIdOrLevel == 0 || button == null) {
            showInfo((Button) objectTable.getCells().get(0).getActor());
            objectList.setScrollY(0);
        }
        description.setScrollY(0);
        stats.setScrollY(0);
    }

    @Override
    public void hide() {
        super.hide();
        visible = false;
        nameTable.setVisible(false);
        mainButtonTable.setVisible(false);
        buttonTable.setVisible(false);
        description.setVisible(false);
        objectImage.setVisible(false);
        stats.setVisible(false);
        objectList.setVisible(false);
        statusTable.setVisible(false);
        requiredTextTable.setVisible(false);
        requirements.setVisible(false);
        unloadTextures();
    }

    private void unloadTextures() {
        for (String path : loadedTextures)
            if (manager.getAssetManager().isLoaded(path))
                manager.getAssetManager().unload(path);
        loadedTextures.clear();
    }

    private boolean skipObject(int[] researchLevels) {
        boolean skip = false;
        for (int i = 0; i < researchLevels.length; i++) {
            if (researchLevels[i] > GameConstants.MAX_RESEARCH_LEVEL) { // ignore buildings with research level > 14
                skip = true;
                break;
            }
        }
        return skip;
    }

    private boolean isOwnerMinorBuilding(BuildingInfo bi) {
        if (bi.getOwnerOfBuilding() == PlayerRaces.NOBODY.getType()) {
            for (int i = 0; i < manager.getRaceController().getRaces().size; i++) {
                Minor minor = Minor.toMinor(manager.getRaceController().getRaces().getValueAt(i));
                if (minor != null
                        && (minor.isMemberTo(playerRace.getRaceId()) || (minor.isSubjugated() && minor.getOwnerId().equals(
                                playerRace.getRaceId()))) && minor.getHomeSystemName().equals(bi.getOnlyInSystemWithName())) {
                    return true;
                }
            }
        }
        return false;
    }

    private void populateObjectList() {
        if (selectedObjectType == DatabaseObjectButtonType.SHIPS) {
            Array<ShipInfo> ships = manager.getShipInfos();
            int idx = 0;
            for (int i = 0; i < ships.size; i++) {
                ShipInfo si = ships.get(i);
                if (si.getRace() == playerRace.getRaceShipNumber()) {
                    int[] researchLevels = si.getNeededResearchLevels();
                    if (skipObject(researchLevels))
                        continue;
                    addObjectItem(si.getShipClass(), si.getID(), idx++);
                }
            }
        } else if (selectedObjectType == DatabaseObjectButtonType.TROOPS) {
            Array<TroopInfo> troops = manager.getTroopInfos();
            int idx = 0;
            for (int i = 0; i < troops.size; i++) {
                TroopInfo ti = troops.get(i);
                if (ti.getOwner().equals(playerRace.getRaceId())) {
                    addObjectItem(ti.getName(), ti.getID() + 20000, idx++);
                }
            }
        } else {
            Array<BuildingInfo> buildingsInfos = manager.getBuildingInfos();
            int idx = 0;
            for (BuildingInfo bi : buildingsInfos) {
                int[] researchLevels = bi.getNeededResearchLevels();
                if (skipObject(researchLevels))
                    continue;

                if (bi.getOwnerOfBuilding() == playerRace.getRaceBuildingNumber() || isOwnerMinorBuilding(bi)) {

                    if ((selectedObjectType == DatabaseObjectButtonType.RESEARCH && bi.isResearchBuilding())
                            || (selectedObjectType == DatabaseObjectButtonType.DEVELOPMENT && bi.isDevelopmentBuilding())
                            || (selectedObjectType == DatabaseObjectButtonType.FOOD && bi.isFoodBuilding())
                            || (selectedObjectType == DatabaseObjectButtonType.ENERGY && bi.isEnergyBuilding())
                            || (selectedObjectType == DatabaseObjectButtonType.DEFENSE && bi.isStrategicBuilding())
                            || (selectedObjectType == DatabaseObjectButtonType.RESOURCE && bi.isResourceBuilding()))
                        addObjectItem(bi.getBuildingName(), bi.getRunningNumber(), idx++);
                }
            }
        }
    }

    private void addObjectItem(String name, int id, int index) {
        Button.ButtonStyle bs = new Button.ButtonStyle();
        Button button = new Button(bs);
        button.align(Align.center);
        button.setSkin(skin);
        Label objectLabel = new Label(name, skin, "largeFont", Color.WHITE);
        objectLabel.setAlignment(Align.center);
        objectLabel.setColor(normalColor);
        objectLabel.setUserObject(id);
        button.add(objectLabel).width(objectList.getWidth() - objectList.getStyle().vScrollKnob.getMinWidth());
        button.setUserObject(objectLabel);
        button.setName("" + index);
        ActorGestureListener gestureListener = new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                Button b = (Button) event.getListenerActor();
                showInfo(b);
            }
        };
        button.addListener(gestureListener);
        objectTable.add(button);
        objectTable.row();
    }

    private void showInfo(Button b) {
        statsTable.clear();
        markObjectListSelected(b);
        int id = getObjectId(b);
        if (objectsOrTech == DatabaseMainButtonType.OBJECT_DATABASE) {
            if (id < 10000)
                showBuildingInfo(id);
            else if (id < 20000)
                showShipInfo(id - 10000);
            else
                showTroopInfo(id - 20000);
        } else {
            showResearchInfo(id % 1000);
        }
    }

    private void showBuildingInfo(int index) {
        BuildingInfo bi = manager.getBuildingInfo(index);
        String imageName = "graphics/buildings/" + bi.getGraphicFileName() + ".png";
        objectImage.setDrawable(new TextureRegionDrawable(new TextureRegion(manager.loadTextureImmediate(imageName))));
        loadedTextures.add(imageName);
        bi.drawBuildingInfo(statsTable, skin, normalColor, markColor, stats.getWidth(), vpad);
        showDescription(descriptionTable, bi.getBuildingDescription());
        bi.getTooltip((Table) objectImage.getUserObject(), skin, tooltipHeaderFont, tooltipHeaderColor, tooltipTextFont,
                tooltipTextColor);
        Research research = playerRace.getEmpire().getResearch();
        updateStatusTable(bi.isBuildingBuildableNow(research.getResearchLevels()));
        drawRequirements(bi.getNeededResearchLevels());
    }

    private void showShipInfo(int index) {
        ShipInfo si = manager.getShipInfos().get(index);
        float width = stats.getWidth() - stats.getStyle().vScrollKnob.getMinWidth();
        Ship ship = new Ship(si);
        ship.addSpecialResearchBoni(playerRace);
        si = new ShipInfo(ship, si);
        String imageName = "graphics/ships/" + si.getShipImageName() + ".png";
        objectImage.setDrawable(new TextureRegionDrawable(new TextureRegion(manager.loadTextureImmediate(imageName))));
        loadedTextures.add(imageName);
        Label nameLabel = new Label(si.getShipClass(), skin, "normalFont", markColor);
        nameLabel.setWrap(true);
        nameLabel.setAlignment(Align.center);
        statsTable.add(nameLabel).width(width);
        statsTable.row();
        si.drawShipInfo(statsTable, skin, markColor, normalColor, width, 0, 2);
        showDescription(descriptionTable, si.getDescription());
        si.getTooltip(null, (Table) objectImage.getUserObject(), skin, tooltipHeaderFont, tooltipHeaderColor, tooltipTextFont,
                tooltipTextColor);
        Research research = playerRace.getEmpire().getResearch();
        updateStatusTable(si.isThisShipBuildableNow(research.getResearchLevels()));
        drawRequirements(si.getNeededResearchLevels());
    }

    private void showTroopInfo(int index) {
        TroopInfo ti = manager.getTroopInfos().get(index);
        String imageName = "graphics/troops/" + ti.getGraphicFile() + ".png";
        objectImage.setDrawable(new TextureRegionDrawable(new TextureRegion(manager.loadTextureImmediate(imageName))));
        loadedTextures.add(imageName);
        ti.drawTroopInfo(playerRace.getEmpire().getResearch().getResearchInfo(), statsTable, skin, normalColor, markColor,
                stats.getWidth(), vpad);
        showDescription(descriptionTable, ti.getDescription());
        ti.getTooltip(playerRace.getEmpire().getResearch().getResearchInfo(), (Table) objectImage.getUserObject(), skin, tooltipHeaderFont, tooltipHeaderColor, tooltipTextFont,
                tooltipTextColor);
        Research research = playerRace.getEmpire().getResearch();
        updateStatusTable(ti.isThisTroopBuildableNow(research.getResearchLevels()));
        drawRequirements(ti.getNeededTechs());
    }

    private void showResearchInfo(int level) {
        String imageName = "graphics/research/" + selectedResearchType.getImgName() + ".png";
        objectImage.setDrawable(new TextureRegionDrawable(new TextureRegion(manager.loadTextureImmediate(imageName))));
        loadedTextures.add(imageName);
        String descText = ResearchInfo.getTechInfos(selectedResearchType.getType(), level).getSecond();
        Label descLabel = new Label(descText, skin, "normalFont", normalColor);
        descLabel.setWrap(true);
        descLabel.setAlignment(Align.center);
        statsTable.add(descLabel).width(description.getWidth());

        String resName = ResearchInfo.getTechInfos(selectedResearchType.getType(), level).getFirst();
        Table table = (Table) objectImage.getUserObject();
        table.clearChildren();
        Label l = new Label(resName, skin, tooltipHeaderFont, tooltipHeaderColor);
        table.add(l);
        table.row();
        l = new Label(descText, skin, tooltipTextFont, tooltipTextColor);
        l.setWrap(true);
        table.add(l).width(objectImage.getWidth());

        Research research = playerRace.getEmpire().getResearch();
        updateStatusTable(research.getResearchLevels()[selectedResearchType.getType()] >= level);

        descriptionTable.clear();
        String levelText = String.format("%s - %s %d", StringDB.getString(selectedResearchType.getKey()),
                StringDB.getString("LEVEL"), level);
        l = new Label(levelText, skin, tooltipHeaderFont, tooltipHeaderColor);
        l.setAlignment(Align.center);
        l.setWrap(true);
        descriptionTable.add(l).width(description.getWidth() - description.getStyle().vScrollKnob.getMinWidth());

        drawRequiredBy(level);
    }

    private void drawRequiredBy(int level) {
        requiredTextTable.clear();
        String text = StringDB.getString("REQUIRED_BY");
        text += ":";
        Label label = new Label(text, skin, "largeFont", normalColor);
        label.setAlignment(Align.left);
        requiredTextTable.add(label);

        requirements.setScrollX(0);
        requirementsTable.clear();
        Array<BuildingInfo> buildingsInfos = manager.getBuildingInfos();
        String imagePath = "graphics/buildings/";
        for (BuildingInfo bi : buildingsInfos) {
            int[] researchLevels = bi.getNeededResearchLevels();
            if(skipObject(researchLevels))
                continue;

            if (bi.getOwnerOfBuilding() == playerRace.getRaceBuildingNumber() || isOwnerMinorBuilding(bi)) {
                if (bi.getNeededTechLevel(selectedResearchType) == level) {
                    String path = imagePath + bi.getGraphicFileName() + ".png";
                    text = bi.getBuildingName();
                    addRequirementsItem(path, text, bi.getRunningNumber());
                }
            }
        }

        Array<ShipInfo> shipInfos = manager.getShipInfos();
        imagePath = "graphics/ships/";
        for (ShipInfo si : shipInfos)
            if (si.getRace() == playerRace.getRaceShipNumber()) {
                int[] researchLevels = si.getNeededResearchLevels();
                if(skipObject(researchLevels))
                    continue;

                if (si.getNeededTechLevel(selectedResearchType) == level) {
                    String path = imagePath + si.getShipImageName() + ".png";
                    text = si.getShipClass();
                    addRequirementsItem(path, text, si.getID());
                }
            }

        Array<TroopInfo> troopInfos = manager.getTroopInfos();
        imagePath = "graphics/troops/";
        for (TroopInfo ti : troopInfos)
            if (ti.getOwner().equals(playerRace.getRaceId())) {
                if (ti.getNeededTechlevel(selectedResearchType.getType()) == level) {
                    String path = imagePath + ti.getGraphicFile() + ".png";
                    text = ti.getName();
                    addRequirementsItem(path, text, ti.getID() + 20000);
                }
            }
    }

    private void drawRequirements(int[] researchLevels) {
        requiredTextTable.clear();
        String text = StringDB.getString("REQUIRES");
        text += ":";
        Label label = new Label(text, skin, "largeFont", normalColor);
        label.setAlignment(Align.left);
        requiredTextTable.add(label);

        requirementsTable.clear();
        String imagePath = "graphics/research/";
        for (int i = 0; i < ResearchType.UNIQUE.getType(); i++) {
            ResearchType rt = ResearchType.fromIdx(i);
            int level = researchLevels[rt.getType()];
            String path = imagePath + rt.getImgName() + ".png";
            text = StringDB.getString(rt.getShortName()) + " " + level;
            addRequirementsItem(path, text, i * 1000 + level);
        }
    }

    private void addRequirementsItem(String imagePath, String text, int idOrLevel) {
        float buttonWidth = GameConstants.wToRelative(140);
        float imageWidth = GameConstants.wToRelative(100);
        float imgHeight = GameConstants.hToRelative(75);
        Button.ButtonStyle bs = new Button.ButtonStyle();
        Button button = new Button(bs);
        button.align(Align.center);
        button.setSkin(skin);

        Image image = new Image(new TextureRegionDrawable(new TextureRegion(manager.loadTextureImmediate(imagePath))));
        loadedTextures.add(imagePath);
        button.add(image).width(imageWidth).height(imgHeight);
        button.row();
        Label requiredLabel = new Label(text, skin, "normalFont", normalColor);
        requiredLabel.setAlignment(Align.center);
        requiredLabel.setEllipsis(true);
        requiredLabel.setUserObject(idOrLevel);
        button.add(requiredLabel).width(buttonWidth);
        button.setUserObject(requiredLabel);

        ActorGestureListener gestureListener = new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                Button b = (Button) event.getListenerActor();
                int idOrLevel = getObjectId(b);
                if (objectsOrTech == DatabaseMainButtonType.TECH_FIELDS) {
                    objectsOrTech = DatabaseMainButtonType.OBJECT_DATABASE;
                    if (idOrLevel < 10000) {
                        BuildingInfo bi = manager.getBuildingInfo(idOrLevel);
                        if (bi.isResearchBuilding())
                            selectedObjectType = DatabaseObjectButtonType.RESEARCH;
                        else if (bi.isDevelopmentBuilding())
                            selectedObjectType = DatabaseObjectButtonType.DEVELOPMENT;
                        else if (bi.isFoodBuilding())
                            selectedObjectType = DatabaseObjectButtonType.FOOD;
                        else if (bi.isEnergyBuilding())
                            selectedObjectType = DatabaseObjectButtonType.ENERGY;
                        else if (bi.isStrategicBuilding())
                            selectedObjectType = DatabaseObjectButtonType.DEFENSE;
                        else if (bi.isResourceBuilding())
                            selectedObjectType = DatabaseObjectButtonType.RESOURCE;
                    } else if (idOrLevel < 20000)
                        selectedObjectType = DatabaseObjectButtonType.SHIPS;
                    else
                        selectedObjectType = DatabaseObjectButtonType.TROOPS;
                } else {
                    objectsOrTech = DatabaseMainButtonType.TECH_FIELDS;
                    selectedResearchType = ResearchType.fromIdx(idOrLevel / 1000);
                }
                selectedIdOrLevel = idOrLevel;
                show();
            }
        };
        button.addListener(gestureListener);
        requirementsTable.add(button).width(buttonWidth);
    }

    private void updateStatusTable(boolean available) {
        statusTable.clear();
        Label label = new Label(StringDB.getString("STATUS") + ":", skin, "hugeFont", markColor);
        label.setAlignment(Align.left);
        statusTable.add(label).width(statusTable.getWidth() / 3).spaceRight(statusTable.getWidth() / 3);
        String text;
        Color color;
        if (available) {
            text = StringDB.getString("RESEARCHED");
            color = Color.GREEN;
        } else {
            text = StringDB.getString("NOT_RESEARCHED");
            color = Color.RED;
        }
        label = new Label(text, skin, "hugeFont", color);
        label.setAlignment(Align.right);
        statusTable.add(label).width(statusTable.getWidth() / 3);
    }

    private void populateResearchList() {
        for (int i = 0; i <= GameConstants.MAX_RESEARCH_LEVEL; i++) {
            String text = ResearchInfo.getTechInfos(selectedResearchType.getType(), i).getFirst();
            text = text.toLowerCase();
            text = Character.toString(text.charAt(0)).toUpperCase() + text.substring(1);
            addObjectItem(text, selectedResearchType.getIndex() * 1000 + i, i);
        }
    }

    private void showDescription(Table table, String text) {
        table.clear();
        Label descLabel = new Label(text, skin, "normalFont", normalColor);
        descLabel.setWrap(true);
        descLabel.setAlignment(Align.center);
        table.add(descLabel).width(description.getWidth());
    }

    private boolean needEnabled(DatabaseMainButtonType type) {
        if (type == objectsOrTech)
            return false;
        return true;
    }

    private void setToSelection(DatabaseMainButtonType selected) {
        objectsOrTech = selected;
        for (int i = 0; i < mainButtonTable.getCells().size; i++) {
            TextButton button = (TextButton) mainButtonTable.getCells().get(i).getActor();
            if (!needEnabled(DatabaseMainButtonType.values()[i])) {
                button.setDisabled(true);
                skin.setEnabled(button, false);
            } else {
                button.setDisabled(false);
                skin.setEnabled(button, true);
            }
        }
    }

    private boolean needEnabled(DatabaseObjectButtonType type) {
        if (type == selectedObjectType)
            return false;
        return true;
    }

    private void setToSelection(DatabaseObjectButtonType selected) {
        selectedObjectType = selected;
        for (int i = 0; i < buttonTable.getCells().size; i++) {
            TextButton button = (TextButton) buttonTable.getCells().get(i).getActor();
            if (!needEnabled(DatabaseObjectButtonType.values()[i])) {
                button.setDisabled(true);
                skin.setEnabled(button, false);
            } else {
                button.setDisabled(false);
                skin.setEnabled(button, true);
            }
        }
    }

    private boolean needEnabled(ResearchType type) {
        if (type == selectedResearchType)
            return false;
        return true;
    }

    private void setToSelection(ResearchType selected) {
        selectedResearchType = selected;
        for (int i = 0; i < buttonTable.getCells().size; i++) {
            TextButton button = (TextButton) buttonTable.getCells().get(i).getActor();
            if (!needEnabled(ResearchType.fromIdx(i))) {
                button.setDisabled(true);
                skin.setEnabled(button, false);
            } else {
                button.setDisabled(false);
                skin.setEnabled(button, true);
            }
        }
    }

    private void markObjectListSelected(Button b) {
        if (objectSelection != null) {
            objectSelection.getStyle().up = null;
            objectSelection.getStyle().down = null;
            ((Label) objectSelection.getUserObject()).setColor(oldColor);
        }
        if (b == null)
            b = objectSelection;
        Image itemSelection = new Image(selectTexture);
        b.getStyle().up = itemSelection.getDrawable();
        b.getStyle().down = itemSelection.getDrawable();
        oldColor = new Color(((Label) b.getUserObject()).getColor());
        ((Label) b.getUserObject()).setColor(markColor);
        objectSelection = b;

        float scrollerHeight = objectList.getScrollHeight();
        float scrollerPos = objectList.getScrollY();
        int selectedItem = Integer.parseInt(objectSelection.getName());
        float buttonPos = b.getHeight() * selectedItem;
        if (buttonPos + b.getHeight() / 2 - scrollerPos < 0)
            objectList.setScrollY(b.getHeight() * selectedItem - objectList.getScrollHeight() * 2);
        else if (buttonPos + b.getHeight() / 2 - scrollerPos > scrollerHeight)
            objectList.setScrollY(b.getHeight() * (selectedItem - objectList.getScrollHeight() / b.getHeight() + 1));
    }

    private int getObjectId(Button b) {
        return (Integer) (((Label) b.getUserObject()).getUserObject());
    }
}
