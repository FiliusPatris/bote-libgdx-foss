/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.screens;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.ui.empireview.EmpireDemographicsView;
import com.blotunga.bote.ui.empireview.EmpireNewsFilterButtonType;
import com.blotunga.bote.ui.empireview.EmpireNewsList;
import com.blotunga.bote.ui.empireview.EmpireMilitaryOverview;
import com.blotunga.bote.ui.empireview.EmpireSystemOverview;
import com.blotunga.bote.ui.empireview.EmpireTopView;
import com.blotunga.bote.ui.empireview.EmpireVictoryView;
import com.blotunga.bote.ui.empireview.EmpireViewMainButtonType;

public class EmpireScreen extends ZoomableScreen {
    private EmpireViewMainButtonType selectedButton;
    private TextButton[] buttons;
    private int numButtons = EmpireViewMainButtonType.values().length;
    private EmpireNewsList evList;
    private EmpireSystemOverview evSystems;
    private EmpireMilitaryOverview evShips;
    private EmpireDemographicsView evDemographics;
    private EmpireTopView evTop5;
    private EmpireVictoryView evVictory;

    //double click go to event, long press delete event

    public EmpireScreen(final ScreenManager manager) {
        super(manager, "newsovmenu", "");

        evList = new EmpireNewsList(manager, mainStage, skin, sidebarLeft.getPosition());
        evSystems = new EmpireSystemOverview(manager, mainStage, skin, sidebarLeft.getPosition());
        evShips = new EmpireMilitaryOverview(manager, mainStage, skin, sidebarLeft.getPosition());
        evDemographics = new EmpireDemographicsView(manager, mainStage, skin, sidebarLeft.getPosition());
        evTop5 = new EmpireTopView(manager, mainStage, skin, sidebarLeft.getPosition());
        evVictory = new EmpireVictoryView(manager, mainStage, skin, sidebarLeft.getPosition());

        buttons = new TextButton[numButtons];
        TextButtonStyle style = skin.get("default", TextButtonStyle.class);
        float xOffset = sidebarLeft.getPosition().getWidth();
        selectedButton = EmpireViewMainButtonType.EVENTS_BUTTON;
        for (int i = 0; i < numButtons; i++) {
            buttons[i] = new TextButton(StringDB.getString(EmpireViewMainButtonType.values()[i].getLabel()), style);
            Rectangle rect = GameConstants.coordsToRelative(25 + i * 190, 60, 180, 35);
            buttons[i].setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
            buttons[i].setUserObject(EmpireViewMainButtonType.values()[i]);

            if (!needEnabled(EmpireViewMainButtonType.values()[i])) {
                buttons[i].setDisabled(true);
                skin.setEnabled(buttons[i], false);
            }

            buttons[i].addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    TextButton button = (TextButton) event.getListenerActor();
                    if (!button.isDisabled()) {
                        EmpireViewMainButtonType selected = (EmpireViewMainButtonType) event.getListenerActor().getUserObject();
                        setSubMenu(selected.getId());
                    }
                }
            });
            mainStage.addActor(buttons[i]);
        }
    }

    @Override
    public void show() {
        super.show();
        setBackground(selectedButton.getBgImage());
        if (selectedButton.equals(EmpireViewMainButtonType.EVENTS_BUTTON)) {
            evList.setToSelection(EmpireNewsFilterButtonType.BUTTON_ALL);
            evList.show();
        } else if (selectedButton.equals(EmpireViewMainButtonType.SYSTEMS_BUTTON)) {
            evSystems.show();
        } else if (selectedButton.equals(EmpireViewMainButtonType.SHIPS_BUTTON)) {
            evShips.show();
        } else if (selectedButton.equals(EmpireViewMainButtonType.STATUS_BUTTON)) {
            evDemographics.show();
        } else if (selectedButton.equals(EmpireViewMainButtonType.TOP_BUTTON)) {
            evTop5.show();
        } else if (selectedButton.equals(EmpireViewMainButtonType.VICTORIES_BUTTON)) {
            evVictory.show();
        }
    }

    @Override
    public void hide() {
        super.hide();
        evList.hide();
        evSystems.hide();
        evShips.hide();
        evDemographics.hide();
        evTop5.hide();
        evVictory.hide();
    }

    private boolean needEnabled(EmpireViewMainButtonType type) {
        if (type == selectedButton)
            return false;
        return true;
    }

    private void setToSelection(EmpireViewMainButtonType selected) {
        selectedButton = selected;
        for (int i = 0; i < numButtons; i++)
            if (!needEnabled(EmpireViewMainButtonType.values()[i])) {
                buttons[i].setDisabled(true);
                skin.setEnabled(buttons[i], false);
            } else {
                buttons[i].setDisabled(false);
                skin.setEnabled(buttons[i], true);
            }
        hide(); //redraw...
        show();
    }

    @Override
    public void setSubMenu(int id) {
        super.setSubMenu(id);
        setToSelection(EmpireViewMainButtonType.fromInt(id));
    }
}
