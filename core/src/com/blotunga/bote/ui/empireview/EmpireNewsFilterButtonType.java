/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.empireview;

import com.blotunga.bote.general.EmpireNews.EmpireNewsType;

public enum EmpireNewsFilterButtonType {
    BUTTON_ALL("BTN_ALL", EmpireNewsType.NO_TYPE),
    BUTTON_ECONOMY("BTN_ECONOMY", EmpireNewsType.ECONOMY),
    BUTTON_RESEARCH("BTN_RESEARCH", EmpireNewsType.RESEARCH),
    BUTTON_SECURITY("BTN_SECURITY", EmpireNewsType.SECURITY),
    BUTTON_DIPLOMACY("BTN_DIPLOMACY", EmpireNewsType.DIPLOMACY),
    BUTTON_MILITARY("BTN_MILITARY", EmpireNewsType.MILITARY);

    private String label;
    private EmpireNewsType type;

    EmpireNewsFilterButtonType(String label, EmpireNewsType type) {
        this.label = label;
        this.type = type;
    }

    public String getLabel() {
        return label;
    }

    public EmpireNewsType getType() {
        return type;
    }
}
