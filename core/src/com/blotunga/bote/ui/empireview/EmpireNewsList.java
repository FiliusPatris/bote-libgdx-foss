/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.empireview;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.InputEvent.Type;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import com.blotunga.bote.DefaultScreen;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.ViewTypes;
import com.blotunga.bote.general.EmpireNews;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.general.EmpireNews.EmpireNewsType;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.utils.IntPoint;

public class EmpireNewsList {
    final private ScreenManager manager;
    private Stage stage;
    private Skin skin;
    private float xOffset;
    private Major playerRace;
    private TextButton[] buttons;
    private EmpireNewsFilterButtonType selectedButton;
    private int numButtons = EmpireNewsFilterButtonType.values().length;
    private Table nameTable;
    private Color normalColor;
    private Color markColor;
    private Color oldColor;
    private TextureRegion selectTexture;
    private Table newsScrollTable;
    private ScrollPane newsScroller;
    private EmpireNews clickedNews;
    private Button newsSelection = null;
    private Array<Button> newsItems;
    private boolean visible;

    public EmpireNewsList(final ScreenManager manager, Stage stage, Skin skin, Rectangle position) {
        this.manager = manager;
        this.skin = skin;
        this.stage = stage;
        xOffset = position.getWidth();
        playerRace = manager.getRaceController().getPlayerRace();

        buttons = new TextButton[numButtons];
        TextButtonStyle style = skin.get("default", TextButtonStyle.class);
        selectedButton = EmpireNewsFilterButtonType.BUTTON_ALL;
        for (int i = 0; i < numButtons; i++) {
            buttons[i] = new TextButton(StringDB.getString(EmpireNewsFilterButtonType.values()[i].getLabel()), style);
            Rectangle rect = GameConstants.coordsToRelative(60 + i * 180, 730, 180, 35);
            buttons[i].setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
            buttons[i].setUserObject(EmpireNewsFilterButtonType.values()[i]);

            if (!needEnabled(EmpireNewsFilterButtonType.values()[i])) {
                buttons[i].setDisabled(true);
                skin.setEnabled(buttons[i], false);
            }

            buttons[i].addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    TextButton button = (TextButton) event.getListenerActor();
                    if (!button.isDisabled()) {
                        EmpireNewsFilterButtonType selected = (EmpireNewsFilterButtonType) event.getListenerActor()
                                .getUserObject();
                        setToSelection(selected);
                    }
                }
            });
            stage.addActor(buttons[i]);
        }
        for (TextButton button : buttons)
            button.setVisible(false);

        nameTable = new Table();
        Rectangle rect = GameConstants.coordsToRelative(400, 800, 400, 60);
        nameTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        nameTable.align(Align.center);
        nameTable.setSkin(skin);
        stage.addActor(nameTable);
        nameTable.add(StringDB.getString("NEWS_MENUE"), "hugeFont", playerRace.getRaceDesign().clrNormalText);
        nameTable.setVisible(false);

        selectTexture = manager.getUiTexture("listselect");
        normalColor = playerRace.getRaceDesign().clrNormalText;
        markColor = playerRace.getRaceDesign().clrListMarkTextColor;

        newsScrollTable = new Table();
        newsScrollTable.align(Align.top);
        newsScroller = new ScrollPane(newsScrollTable, skin);
        newsScroller.setVariableSizeKnobs(false);
        newsScroller.setFadeScrollBars(false);
        newsScroller.setScrollbarsOnTop(true);
        newsScroller.setScrollingDisabled(true, false);
        newsScrollTable.setTouchable(Touchable.childrenOnly);
        stage.addActor(newsScroller);
        rect = GameConstants.coordsToRelative(110, 660, 975, 560);
        newsScroller.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);

        clickedNews = null;
        newsItems = new Array<Button>();
        visible = false;
    }

    public void show() {
        visible = true;
        nameTable.setVisible(true);

        for (TextButton button : buttons) {
            button.setVisible(true);
        }
        newsScrollTable.clear();
        newsSelection = null;
        newsItems.clear();
        newsScroller.setVisible(true);
        int idx = 0;
        for (int i = 0; i < playerRace.getEmpire().getMsgs().size; i++) {
            EmpireNews news = playerRace.getEmpire().getMsgs().get(i);
            if ((news.getType() == selectedButton.getType()) || (selectedButton.getType() == EmpireNewsType.NO_TYPE)) {
                Button.ButtonStyle bs = new Button.ButtonStyle();
                Button button = new Button(bs);
                button.align(Align.center);
                button.setSkin(skin);
                Color color;
                switch (news.getType()) {
                    case ECONOMY:
                        color = new Color(0, 150 / 255.0f, 0, 1.0f);
                        break;
                    case RESEARCH:
                        color = new Color(50 / 255.0f, 75 / 255.0f, 1.0f, 1.0f);
                        break;
                    case SECURITY:
                        color = new Color(155 / 255.0f, 25 / 255.0f, 1.0f, 1.0f);
                        break;
                    case DIPLOMACY:
                        color = new Color(1.0f, 220 / 255.0f, 0, 1.0f);
                        break;
                    case MILITARY:
                        color = new Color(1.0f, 0, 0, 1.0f);
                        break;
                    default:
                        color = new Color(normalColor);
                        break;
                }

                Label msgLabel = new Label(news.getText(), skin, "normalFont", Color.WHITE);
                msgLabel.setAlignment(Align.center);
                msgLabel.setColor(color);
                msgLabel.setUserObject(news);
                button.add(msgLabel).width(newsScroller.getWidth());
                button.setUserObject(msgLabel);
                button.setName("" + idx++);
                ActorGestureListener gestureListener = new ActorGestureListener() {
                    @Override
                    public void tap(InputEvent event, float x, float y, int count, int button) {
                        Button b = (Button) event.getListenerActor();
                        if (button == 0) {
                            markNewsListSelected(b);
                            clickedNews = getNews(b);

                            if (count >= 2) {
                                jumpToClickedNews();
                            }
                        } else
                            removeItem(b);
                    }

                    @Override
                    public boolean longPress(Actor actor, float x, float y) {
                        Button b = (Button) actor;
                        removeItem(b);
                        return true;
                    }
                };
                gestureListener.getGestureDetector().setLongPressSeconds(1.0f);
                gestureListener.getGestureDetector().setTapCountInterval(0.6f);
                button.addListener(gestureListener);
                newsScrollTable.add(button);
                newsScrollTable.row();
                newsItems.add(button);
            }
        }

        stage.setKeyboardFocus(newsScrollTable);
        newsScrollTable.addListener(new InputListener() {
            @Override
            public boolean keyDown (InputEvent event, final int keycode) {
                int limit = newsScrollTable.getCells().size;
                if (limit == 0)
                    return false;
                int selectedItem = Integer.parseInt(newsSelection.getName());
                if (limit < selectedItem)
                    selectedItem = limit - 1;
                Button b = newsScrollTable.findActor("" + selectedItem);
                switch (keycode) {
                    case Keys.DOWN:
                        selectedItem++;
                        break;
                    case Keys.UP:
                        selectedItem--;
                        break;
                    case Keys.HOME:
                        selectedItem = 0;
                        break;
                    case Keys.END:
                        selectedItem = limit - 1;
                        break;
                    case Keys.PAGE_DOWN:
                        selectedItem += newsScroller.getScrollHeight() / b.getHeight();
                        break;
                    case Keys.PAGE_UP:
                        selectedItem -= newsScroller.getScrollHeight() / b.getHeight();
                        break;
                    case Keys.ENTER:
                        jumpToClickedNews();
                        break;
                    case Keys.DEL:
                    case Keys.FORWARD_DEL: //Delete key
                        removeItem(b);
                        break;
                }

                if (EmpireNewsList.this.visible) {
                    if (selectedItem >= limit)
                        selectedItem = limit - 1;
                    if (selectedItem < 0)
                        selectedItem = 0;

                    b = newsScrollTable.findActor("" + selectedItem);
                    markNewsListSelected(b);
                    clickedNews = getNews(b);

                    Thread th = new Thread() {
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(150);
                            } catch (InterruptedException e) {
                            }
                            if (Gdx.input.isKeyPressed(keycode)) {
                                Gdx.app.postRunnable(new Runnable() {
                                    @Override
                                    public void run() {
                                        InputEvent event = new InputEvent();
                                        event.setType(Type.keyDown);
                                        event.setKeyCode(keycode);
                                        newsScrollTable.fire(event);
                                    }
                                });
                            }
                        }
                    };
                    th.start();
                }

                return false;
            }
        });

        stage.setScrollFocus(newsScroller);
        stage.draw();
        Button btn = null;
        if (clickedNews != null) {
            for (Button b : newsItems) {
                if (clickedNews == getNews(b)) {
                    btn = b;
                }
            }
        }
        if (btn == null) {
            if (newsItems.size > 0) {
                btn = newsItems.get(0);
                clickedNews = getNews(btn);
            }
        }
        if (btn != null)
            markNewsListSelected(btn);
    }

    private void jumpToClickedNews() {
        if (clickedNews.getType() == EmpireNewsType.ECONOMY
                || clickedNews.getType() == EmpireNewsType.SOMETHING) {
            IntPoint p = clickedNews.getCoord();
            if (!p.equals(new IntPoint())) {
                manager.getUniverseMap().setSelectedCoordValue(p);
                StarSystem ss = manager.getUniverseMap().getStarSystemAt(p);
                if (ss.getOwnerId().equals(playerRace.getRaceId()) && ss.isMajorized()) {
                    DefaultScreen screen = manager.setView(ViewTypes.SYSTEM_VIEW);
                    manager.setSubMenu(screen, clickedNews.getFlag());
                } else {
                    manager.setView(ViewTypes.GALAXY_VIEW);
                    manager.getUniverseMap().getRenderer().showPlanets(true);
                }
            }
        } else if (clickedNews.getType() == EmpireNewsType.MILITARY) {
            IntPoint p = clickedNews.getCoord();
            if (!p.equals(new IntPoint())) {
                manager.getUniverseMap().setSelectedCoordValue(p);
                StarSystem ss = manager.getUniverseMap().getStarSystemAt(p);
                if (clickedNews.getFlag() == 1 && ss.getOwnerId().equals(playerRace.getRaceId())) {
                    DefaultScreen screen = manager.setView(ViewTypes.SYSTEM_VIEW);
                    manager.setSubMenu(screen, 0);
                } else {
                    manager.setView(ViewTypes.GALAXY_VIEW);
                }
                if (ss.getIsShipInSector()) {
                    manager.getUniverseMap().getRenderer().showShips(true);
                } else {
                    manager.getUniverseMap().getRenderer().showPlanets(true);
                }
            }
        } else if (clickedNews.getType() == EmpireNewsType.RESEARCH) {
            DefaultScreen screen = manager.setView(ViewTypes.RESEARCH_VIEW);
            manager.setSubMenu(screen, clickedNews.getFlag());
        } else if (clickedNews.getType() == EmpireNewsType.SECURITY) {
            DefaultScreen screen = manager.setView(ViewTypes.INTEL_VIEW);
            manager.setSubMenu(screen, clickedNews.getFlag());
        } else if (clickedNews.getType() == EmpireNewsType.DIPLOMACY) {
            DefaultScreen screen = manager.setView(ViewTypes.DIPLOMACY_VIEW);
            manager.setSubMenu(screen, clickedNews.getFlag());
        }
    }

    private void removeItem(Button b) {
        markNewsListSelected(b);
        clickedNews = getNews(b);
        final int selectedIndex =  Integer.parseInt(newsSelection.getName());
        playerRace.getEmpire().getMsgs().removeValue(clickedNews, true);

        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                manager.getSidebarLeft().showInfo();
                show();
                int limit = newsScrollTable.getCells().size;
                if (limit > 0) {
                    int newIndex = selectedIndex;
                    if (newIndex >= limit)
                        newIndex = limit - 1;
                    Button btn = newsScrollTable.findActor("" + newIndex);
                    markNewsListSelected(btn);
                    clickedNews = getNews(btn);
                }
            }
        });
    }

    public void hide() {
        visible = false;
        nameTable.setVisible(false);
        for (TextButton button : buttons)
            button.setVisible(false);
        newsScroller.setVisible(false);
    }

    private boolean needEnabled(EmpireNewsFilterButtonType type) {
        if (type == selectedButton)
            return false;
        return true;
    }

    public void setToSelection(EmpireNewsFilterButtonType selected) {
        selectedButton = selected;
        for (int i = 0; i < numButtons; i++)
            if (!needEnabled(EmpireNewsFilterButtonType.values()[i])) {
                buttons[i].setDisabled(true);
                skin.setEnabled(buttons[i], false);
            } else {
                buttons[i].setDisabled(false);
                skin.setEnabled(buttons[i], true);
            }
        hide(); //redraw...
        show();
    }

    private void markNewsListSelected(Button b) {
        if (newsSelection != null) {
            newsSelection.getStyle().up = null;
            newsSelection.getStyle().down = null;
            ((Label) newsSelection.getUserObject()).setColor(oldColor);
        }
        if (b == null)
            b = newsSelection;
        Image itemSelection = new Image(selectTexture);
        b.getStyle().up = itemSelection.getDrawable();
        b.getStyle().down = itemSelection.getDrawable();
        oldColor = new Color(((Label) b.getUserObject()).getColor());
        ((Label) b.getUserObject()).setColor(markColor);
        newsSelection = b;

        float scrollerHeight = newsScroller.getScrollHeight();
        float scrollerPos = newsScroller.getScrollY();
        int selectedItem = Integer.parseInt(newsSelection.getName());
        float buttonPos = b.getHeight() * selectedItem;
        if (buttonPos + b.getHeight() / 2 - scrollerPos < 0)
            newsScroller.setScrollY(b.getHeight() * selectedItem - newsScroller.getScrollHeight() * 2);
        else if (buttonPos + b.getHeight() / 2 - scrollerPos > scrollerHeight)
            newsScroller.setScrollY(b.getHeight() * (selectedItem - newsScroller.getScrollHeight() / b.getHeight() + 1));
    }

    private EmpireNews getNews(Button b) {
        return (EmpireNews) (((Label) b.getUserObject()).getUserObject());
    }
}
