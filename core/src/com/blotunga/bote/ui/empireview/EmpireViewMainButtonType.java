/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.empireview;

import com.badlogic.gdx.utils.IntMap;

public enum EmpireViewMainButtonType {
    EVENTS_BUTTON("BTN_EVENTS", "newsovmenu", 0),
    SYSTEMS_BUTTON("BTN_SYSTEMS", "systemovmenu", 1),
    SHIPS_BUTTON("BTN_MILITARY", "shipovmenu", 2),
    STATUS_BUTTON("BTN_DEMOGRAPHY", "demomenu", 3),
    TOP_BUTTON("BTN_TOP5SYSTEMS", "top5menu", 4),
    VICTORIES_BUTTON("BTN_VICTORIES", "victorymenu", 5);

    private String label;
    private String bgImage;
    private int ord;
    private static final IntMap<EmpireViewMainButtonType> intToTypeMap = new IntMap<EmpireViewMainButtonType>();

    EmpireViewMainButtonType(String label, String bgImage, int ord) {
        this.label = label;
        this.bgImage = bgImage;
        this.ord = ord;
    }

    static {
        for (EmpireViewMainButtonType et : EmpireViewMainButtonType.values()) {
            intToTypeMap.put(et.ord, et);
        }
    }

    public String getLabel() {
        return label;
    }

    public String getBgImage() {
        return bgImage;
    }

    public int getId() {
        return ord;
    }

    public static EmpireViewMainButtonType fromInt(int i) {
        EmpireViewMainButtonType et = intToTypeMap.get(i);
        return et;
    }
}
