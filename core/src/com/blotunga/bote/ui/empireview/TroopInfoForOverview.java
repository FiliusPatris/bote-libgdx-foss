/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.empireview;

import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.troops.TroopInfo;
import com.blotunga.bote.utils.IntPoint;

public class TroopInfoForOverview extends TroopInfo implements Comparable<TroopInfoForOverview> {
    public enum TroopSortType {
        BY_NAME,
        BY_CURRENTSECTOR,
        BY_SPACE,
        BY_ATTACK,
        BY_DEFENSE,
        BY_MORALE,
        BY_EXP,
        BY_INSHIP,
        BY_MAINTAINANCE,
    }

    private static TroopSortType sortType = TroopSortType.BY_NAME;
    private String currentSystem;
    private String currentShip;
    private boolean inShip;
    private IntPoint coord;

    public TroopInfoForOverview(TroopInfo ti, IntPoint coord, String currentPlace, String currentShip, boolean inShip) {
        super(ti);
        this.coord = new IntPoint(coord);
        this.currentSystem = currentPlace;
        this.currentShip = currentShip;
        this.inShip = inShip;
    }

    public String getCurrentPlace() {
        return currentSystem;
    }

    public String getCurrentShip() {
        return currentShip;
    }

    public boolean isInShip() {
        return inShip;
    }

    public IntPoint getCoord() {
        return coord;
    }

    public static void setSortType(TroopSortType tst) {
        sortType = tst;
    }

    @Override
    public int compareTo(TroopInfoForOverview other) {
        switch (sortType) {
            case BY_NAME:
                return getName().compareTo(other.getName());
            case BY_CURRENTSECTOR:
                return currentSystem.compareTo(other.currentSystem);
            case BY_SPACE:
                return GameConstants.compare(getSize(), other.getSize());
            case BY_ATTACK:
                return GameConstants.compare(getOffense(), other.getOffense());
            case BY_DEFENSE:
                return GameConstants.compare(getDefense(), other.getDefense());
            case BY_MORALE:
                return GameConstants.compare(getMoralValue(), other.getMoralValue());
            case BY_EXP:
                return GameConstants.compare(experience, other.experience);
            case BY_INSHIP:
                return currentShip.compareTo(other.currentShip);
            case BY_MAINTAINANCE:
                return GameConstants.compare(getMaintenanceCosts(), other.getMaintenanceCosts());
        }
        return 0;
    }
}
