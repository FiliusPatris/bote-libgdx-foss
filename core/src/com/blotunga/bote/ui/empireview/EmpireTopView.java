/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.empireview;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.galaxy.Planet;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.utils.IntPoint;

public class EmpireTopView {
    private ScreenManager manager;
    private Skin skin;
    private Major playerRace;
    private Color normalColor;
    private Table nameTable;
    private Table topTable;
    private Array<String> loadedTextures;

    public EmpireTopView(ScreenManager manager, Stage stage, Skin skin, Rectangle position) {
        this.manager = manager;
        this.skin = skin;
        float xOffset = position.getWidth();
        playerRace = manager.getRaceController().getPlayerRace();
        normalColor = playerRace.getRaceDesign().clrNormalText;

        nameTable = new Table();
        Rectangle rect = GameConstants.coordsToRelative(400, 800, 400, 60);
        nameTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        nameTable.align(Align.center);
        nameTable.setSkin(skin);
        stage.addActor(nameTable);
        nameTable.add(StringDB.getString("TOP5SYSTEMS_MENUE"), "hugeFont", playerRace.getRaceDesign().clrNormalText);
        nameTable.setVisible(false);

        topTable = new Table();
        rect = GameConstants.coordsToRelative(135, 655, 930, 500);
        topTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        topTable.align(Align.left);
        topTable.setSkin(skin);
        stage.addActor(topTable);
        topTable.setVisible(false);

        loadedTextures = new Array<String>();
    }

    public void show() {
        nameTable.setVisible(true);

        topTable.clear();
        Array<IntPoint> systemslist = manager.getStatistics().getTopSystems(manager, 5);
        for (int i = 0; i < systemslist.size; i++) {
            if (i != 0)
                topTable.row();
            addEntry(manager.getUniverseMap().getStarSystemAt(systemslist.get(i)), i);
        }
        topTable.setVisible(true);
    }

    private void addEntry(StarSystem starSystem, int cnt) {
        ButtonStyle bs = new ButtonStyle();
        Button b = new Button(bs);
        b.setSkin(skin);
        boolean showPlanets = false;
        float textWidth = GameConstants.hToRelative(180);
        if (starSystem.getFullKnown(playerRace.getRaceId())
                || (playerRace.isRaceContacted(starSystem.getOwnerId()) && starSystem
                        .getScanned(playerRace.getRaceId()))) {
            String text = String.format("%s %d: %s", StringDB.getString("PLACE"), cnt + 1, starSystem.getName());
            b.add(text, "largeFont", normalColor).align(Align.left).width((int) textWidth);
            Major owner = Major.toMajor(starSystem.getOwner());
            if (owner != null) {
                b.row();
                text = owner.getEmpireName();
                b.add(text, "largeFont", owner.getRaceDesign().clrSector).align(Align.left).width((int) textWidth);
            }
            showPlanets = true;
        } else {
            String text = String.format("%s %d: %s", StringDB.getString("PLACE"), cnt + 1,
                    StringDB.getString("UNKNOWN"));
            b.add(text, "largeFont", normalColor).align(Align.left).width((int) textWidth);
        }
        topTable.add(b).height((int) (topTable.getHeight() / 5)).width((int) textWidth).align(Align.left);
        float planetsWidth = GameConstants.wToRelative(600);
        float pad = GameConstants.wToRelative(5);
        if (showPlanets) {
            b = new Button(bs);
            for (int i = starSystem.getPlanets().size - 1; i >= 0; i--) {
                Image img = new Image();
                Planet p = starSystem.getPlanets().get(i);
                String path = "graphics/planets/" + p.getPlanetGraphicFile() + ".png";
                loadedTextures.add(path);
                TextureRegion ptex = new TextureRegion(manager.loadTextureImmediate(path));
                img.setDrawable(new TextureRegionDrawable(ptex));
                float size = planetsWidth * 1.2f / p.getSize().getScale();
                b.add(img).width(size).height(size).spaceLeft(pad);
            }
            topTable.add(b).width(planetsWidth);
            String path = "graphics/sun/" + starSystem.getStarType().getSunName() + ".png";
            loadedTextures.add(path);
            TextureRegion tex = new TextureRegion(manager.loadTextureImmediate(path));
            Image img = new Image();
            img.setDrawable(new TextureRegionDrawable(tex));
            float size = topTable.getHeight() / 5;
            topTable.add(img).width(size).height(size);
        }
    }

    public void hide() {
        nameTable.setVisible(false);
        topTable.setVisible(false);
        for (String path : loadedTextures)
            if (manager.getAssetManager().isLoaded(path))
                manager.getAssetManager().unload(path);
        loadedTextures.clear();
    }
}
