/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.empireview;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.general.GameStatistics.DemographicsInfo;
import com.blotunga.bote.races.Major;

public class EmpireDemographicsView {
    private ScreenManager manager;
    private Skin skin;
    private Major playerRace;
    private Table nameTable;
    private Table ratingsTable;
    private Color normalColor;
    private Color markColor;
    private Table statisticsTable;
    private TextureRegionDrawable trophy1;
    private TextureRegionDrawable trophy2;
    private TextureRegionDrawable trophy3;

    public EmpireDemographicsView(ScreenManager manager, Stage stage, Skin skin, Rectangle position) {
        this.manager = manager;
        this.skin = skin;
        float xOffset = position.getWidth();
        playerRace = manager.getRaceController().getPlayerRace();
        normalColor = playerRace.getRaceDesign().clrNormalText;
        markColor = playerRace.getRaceDesign().clrListMarkTextColor;

        nameTable = new Table();
        Rectangle rect = GameConstants.coordsToRelative(400, 800, 400, 60);
        nameTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        nameTable.align(Align.center);
        nameTable.setSkin(skin);
        stage.addActor(nameTable);
        nameTable.add(StringDB.getString("DEMOGRAPHY_MENUE"), "hugeFont", playerRace.getRaceDesign().clrNormalText);
        nameTable.setVisible(false);

        TextureAtlas uiAtlas = manager.getAssetManager().get("graphics/ui/general_ui.pack");
        trophy1 = new TextureRegionDrawable(uiAtlas.findRegion("trophy1"));
        trophy2 = new TextureRegionDrawable(uiAtlas.findRegion("trophy2"));
        trophy3 = new TextureRegionDrawable(uiAtlas.findRegion("trophy3"));

        ratingsTable = new Table();
        rect = GameConstants.coordsToRelative(450, 725, 300, 30);
        ratingsTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        ratingsTable.align(Align.center);
        ratingsTable.setSkin(skin);
        stage.addActor(ratingsTable);
        ratingsTable.setVisible(false);

        statisticsTable = new Table();
        rect = GameConstants.coordsToRelative(135, 655, 930, 500);
        statisticsTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        statisticsTable.align(Align.center);
        statisticsTable.setSkin(skin);
        stage.addActor(statisticsTable);
        statisticsTable.setVisible(false);
    }

    public void show() {
        nameTable.setVisible(true);
        ratingsTable.clear();
        float mark = manager.getStatistics().getMark(playerRace.getRaceId());
        String text = String.format("%s: %.1f", StringDB.getString("RATING"), mark);
        Image img = new Image();
        Image img2 = new Image();
        float imgWidth = GameConstants.wToRelative(45);
        float imgHeight = GameConstants.hToRelative(40);
        if (mark < 1.5f) {
            img.setDrawable(trophy1);
            img2.setDrawable(trophy1);
        } else if (mark < 2.0f) {
            img.setDrawable(trophy2);
            img2.setDrawable(trophy2);
        } else if (mark < 2.5f) {
            img.setDrawable(trophy3);
            img2.setDrawable(trophy3);
        }

        ratingsTable.add(img).height((int) imgHeight).width((int) imgWidth);
        ratingsTable.add(text, "xxlFont", markColor);
        ratingsTable.add(img2).height((int) imgHeight).width((int) imgWidth);

        ratingsTable.setVisible(true);
        statisticsTable.clear();

        String[] headers = { "", "RANK", "VALUE", "FIRST_RANK", "AVERAGE", "LAST_RANK" };
        float columnWidth = statisticsTable.getWidth() / 6;
        float columnHeight = statisticsTable.getHeight() / 6;
        for (int i = 0; i < headers.length; i++) {
            if (!headers[i].isEmpty())
                text = StringDB.getString(headers[i]);
            else
                text = "";

            Label label = new Label(text, skin, "xlFont", markColor);
            label.setAlignment(Align.center);
            statisticsTable.add(label).width((int) columnWidth).height((int) columnHeight);
        }
        statisticsTable.row();
        statisticsTable.add(StringDB.getString("DEMO_BSP"), "largeFont", markColor).width((int) columnWidth)
                .height((int) columnHeight);
        DemographicsInfo info = manager.getStatistics().getDemographicsBSP(playerRace.getRaceId());
        addInfo(info, 5.0f);
        statisticsTable.row();
        statisticsTable.add(StringDB.getString("DEMO_PRODUCTIVITY"), "largeFont", markColor).width((int) columnWidth)
                .height((int) columnHeight);
        info = manager.getStatistics().getDemographicsProductivity(playerRace.getRaceId());
        addInfo(info, 1.0f);
        statisticsTable.row();
        statisticsTable.add(StringDB.getString("DEMO_MILITARY"), "largeFont", markColor).width((int) columnWidth)
                .height((int) columnHeight);
        info = manager.getStatistics().getDemographicsMilitary(playerRace.getRaceId());
        addInfo(info, 0.1f);
        statisticsTable.row();
        statisticsTable.add(StringDB.getString("DEMO_SCIENCE"), "largeFont", markColor).width((int) columnWidth)
                .height((int) columnHeight);
        info = manager.getStatistics().getDemographicsResearch(playerRace.getRaceId());
        addInfo(info, 2.0f);
        statisticsTable.row();
        statisticsTable.add(StringDB.getString("DEMO_HAPPINESS"), "largeFont", markColor).width((int) columnWidth)
                .height((int) columnHeight);
        info = manager.getStatistics().getDemographicsMorale(playerRace.getRaceId());
        addInfo(info, 1.0f);

        statisticsTable.setVisible(true);
    }

    private void addInfo(DemographicsInfo info, float mul) {
        float imgWidth = GameConstants.wToRelative(45);
        float imgHeight = GameConstants.hToRelative(40);
        ButtonStyle bs = new ButtonStyle();
        Button b = new Button(bs);
        Image img = new Image();
        b.add(img).width((int) (imgWidth * 0.8)).height((int) (imgHeight * 0.8));
        if (info.place == 1)
            img.setDrawable(trophy1);
        else if (info.place == 2)
            img.setDrawable(trophy2);
        else if (info.place == 3)
            img.setDrawable(trophy3);

        String text = "" + info.place;
        Label label = new Label(text, skin, "normalFont", normalColor);
        label.setAlignment(Align.center);
        b.add(label).width((int) (imgWidth * 0.8));
        statisticsTable.add(b).align(Align.center);
        text = String.format("%.0f", info.value * mul);
        label = new Label(text, skin, "normalFont", normalColor);
        label.setAlignment(Align.center);
        statisticsTable.add(label);
        text = String.format("%.0f", info.first * mul);
        label = new Label(text, skin, "normalFont", normalColor);
        label.setAlignment(Align.center);
        statisticsTable.add(label);
        text = String.format("%.0f", info.average * mul);
        label = new Label(text, skin, "normalFont", normalColor);
        label.setAlignment(Align.center);
        statisticsTable.add(label);
        text = String.format("%.0f", info.last * mul);
        label = new Label(text, skin, "normalFont", normalColor);
        label.setAlignment(Align.center);
        statisticsTable.add(label);
    }

    public void hide() {
        nameTable.setVisible(false);
        ratingsTable.setVisible(false);
        statisticsTable.setVisible(false);
    }
}
