/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui;

import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputEvent.Type;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.ObjectMap.Entry;
import com.blotunga.bote.GamePreferences;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.ShipOrder;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.races.Race;
import com.blotunga.bote.ui.screens.DiplomacyScreen;
import com.blotunga.bote.ui.screens.EmpireScreen;
import com.blotunga.bote.ui.screens.IntelScreen;
import com.blotunga.bote.ui.screens.OptionsScreen;
import com.blotunga.bote.ui.screens.ResearchScreen;
import com.blotunga.bote.ui.screens.SystemBuildScreen;
import com.blotunga.bote.ui.screens.TradeScreen;
import com.blotunga.bote.ui.screens.UniverseRenderer;

public class LeftSideBar extends UISidebar {
    final private int numButtons = UIButtonType.values().length;
    private final int paddingY = (int) GameConstants.hToRelative(18);
    private final int paddingX = (int) (Gdx.graphics.getWidth() / 5.81f / 8.5);
    private final TextButton[] buttons;
    private final Label turnCounter;
    private final Label creditText; // says 'credits'
    private final Label creditValue; // displays the value
    private final Label creditChangeText; //says "change" ... etc
    private final Label creditChangeVal;
    private final Label upKeepText;
    private final Label upKeepValue;
    private final Label popSupportText;
    private final Label popSupportVal;
    private final Label newsCntText;
    private final Label newsCntVal;
    private final Label ratingText;
    private final Label ratingVal;
    private final Label starDateText;
    private ScrollPane scrollPane;
    private Image infoImage;
    private Table infoTable;
    private boolean knowOtherMajors;
    private boolean knowOtherRaces;
    private Skin skin;

    public LeftSideBar(final ScreenManager screenManager, Camera camera) {
        super(new Rectangle(0, 0, GameConstants.wToRelative(247), GameConstants.hToRelative(810)), "", screenManager, false, camera);
        skin = screenManager.getSkin();
        Major r = screenManager.getRaceController().getPlayerRace();
        setBackground(r.getPrefix() + "menuV2");

        TextButtonStyle buttonStyle = skin.get("default", TextButtonStyle.class);
        TextButtonStyle endTurnStyle = new TextButtonStyle(buttonStyle);
        endTurnStyle.up = skin.getDrawable("ebackground");
        endTurnStyle.down = skin.getDrawable("ebackgroundi");
        endTurnStyle.over = skin.getDrawable("ebackgrounda");
        endTurnStyle.disabled = skin.getDrawable("ebackgroundi");

        buttons = new TextButton[numButtons];
        for (int i = 0; i < numButtons; i++) {
            if (UIButtonType.values()[i] != UIButtonType.END_TURN)
                buttons[i] = new TextButton(StringDB.getString(UIButtonType.values()[i].getLabel()), buttonStyle);
            else
                buttons[i] = new TextButton(StringDB.getString(UIButtonType.values()[i].getLabel()), endTurnStyle);
            buttons[i].setWidth(position.width - paddingX * 2);
            buttons[i].setHeight((int) (position.height / 20));
            if (UIButtonType.values()[i] != UIButtonType.END_TURN)
                buttons[i].setPosition((int) (position.x + paddingX),
                        (int) (GamePreferences.sceneHeight - i * buttons[i].getHeight() - i * GamePreferences.sceneHeight
                                / 240 - GamePreferences.sceneHeight / 2 - paddingY * 2));
            else
                buttons[i].setPosition((int) (position.x + paddingX),
                        (int) (GamePreferences.sceneHeight - buttons[i].getHeight()));

            buttons[i].addListener(UIButtonType.values()[i].getListener());
            if (!needEnabled(UIButtonType.values()[i], screenManager.getScreen())) {
                skin.setEnabled(buttons[i], false);
                buttons[i].setDisabled(true);
            }
            buttons[i].setUserObject(screenManager);
            stage.addActor(buttons[i]);
        }

        infoTable = new Table();
        scrollPane = new ScrollPane(infoTable, skin);
        scrollPane.setVariableSizeKnobs(false);
        scrollPane.setFadeScrollBars(false);
        infoTable.setSkin(skin);
        infoTable.align(Align.top);

        infoTable.setWidth((int) (position.getWidth() - paddingX * 2));
        float tableHeight = GameConstants.hToRelative(212);
        infoTable.setHeight((int) tableHeight);
        infoTable.setPosition((int) (position.x + paddingX),
                (int) (buttons[1].getY() + buttons[1].getHeight() + paddingY / 8));

        buttons[0].setPosition((int) (position.x + paddingX),
                (int) (infoTable.getY() + infoTable.getHeight() + paddingY / 8)); //move the options button above the image
        scrollPane.setBounds(infoTable.getX(), infoTable.getY(), infoTable.getWidth(), infoTable.getHeight());
        scrollPane.setScrollingDisabled(true, false);

        infoImage = new Image();
        infoImage.setBounds(infoTable.getX(), infoTable.getY() + (infoTable.getHeight() - infoTable.getWidth()) / 2,
                infoTable.getWidth(), infoTable.getWidth());
        stage.addActor(infoImage);
        stage.addActor(scrollPane);

        Major playerRace = screenManager.getRaceController().getPlayerRace();

        LabelStyle turnStyle = new LabelStyle();
        Color secondColor = r.getRaceDesign().clrGalaxySectorText;
        Color textColor = r.getRaceDesign().clrNormalText;
        turnStyle.font = screenManager.getRacialFont(r.getRaceDesign().fontName, (int) (GamePreferences.sceneHeight / 42));
        turnStyle.font.setFixedWidthGlyphs("1234567890");
        turnStyle.fontColor = Color.WHITE;
        turnCounter = new Label(StringDB.getString("ROUND"), turnStyle);
        turnCounter.setColor(secondColor);
        turnCounter.setHeight((int) turnStyle.font.getLineHeight());
        turnCounter.setPosition(position.x, (int) (buttons[numButtons - 1].getY() - turnCounter.getHeight()));
        turnCounter.setAlignment(Align.center);
        turnCounter.setWidth(position.width);
        turnCounter.setTouchable(Touchable.disabled);

        LabelStyle textStyle = new LabelStyle();
        textStyle.fontColor = Color.WHITE;
        textStyle.font = screenManager.getRacialFont(r.getRaceDesign().fontName, (int) (GamePreferences.sceneHeight / 55));
        textStyle.font.setFixedWidthGlyphs("1234567890");
        creditText = new Label(StringDB.getString("CREDITS"), textStyle);
        creditText.setHeight((int) textStyle.font.getLineHeight());
        creditText.setColor(textColor);
        creditText.setPosition(position.x + paddingX, (int) (turnCounter.getY() - creditText.getHeight()));
        creditText.setTouchable(Touchable.disabled);
        String text = "" + playerRace.getEmpire().getCredits();
        creditValue = new Label(text, textStyle);
        creditValue.setColor(textColor);
        creditValue.setHeight((int) textStyle.font.getLineHeight());
        GlyphLayout ly = new GlyphLayout(textStyle.font, text);
        float lwidth = ly.width;
        creditValue
                .setPosition(position.width - paddingX - lwidth, (int) (turnCounter.getY() - creditText.getHeight()));
        creditValue.setAlignment(Align.left);
        creditValue.setTouchable(Touchable.disabled);

        creditChangeText = new Label(StringDB.getString("CHANGE"), textStyle);
        creditChangeText.setHeight((int) textStyle.font.getLineHeight());
        creditChangeText.setColor(textColor);
        creditChangeText.setPosition(position.x + paddingX, (int) (creditText.getY() - creditText.getHeight()));
        creditChangeText.setTouchable(Touchable.disabled);
        text = "" + playerRace.getEmpire().getCreditsChange();
        creditChangeVal = new Label(text, textStyle);
        creditChangeVal.setHeight((int) textStyle.font.getLineHeight());
        ly.setText(textStyle.font, text);
        lwidth = ly.width;
        creditChangeVal.setPosition(position.width - paddingX - lwidth, creditChangeText.getY());
        creditChangeVal.setAlignment(Align.left);
        creditChangeVal.setTouchable(Touchable.disabled);

        LabelStyle shipStyle = new LabelStyle();
        shipStyle.font = textStyle.font;
        shipStyle.fontColor = Color.WHITE;

        upKeepText = new Label(StringDB.getString("SHIPCOSTS"), textStyle);
        upKeepText.setColor(textColor);
        upKeepText.setHeight((int) textStyle.font.getLineHeight());
        upKeepText.setPosition(position.x + paddingX, (int) (creditChangeVal.getY() - creditChangeVal.getHeight()));
        upKeepText.setTouchable(Touchable.disabled);

        text = "" + playerRace.getEmpire().getShipCosts();
        upKeepValue = new Label(text, textStyle);
        upKeepValue.setColor(textColor);
        upKeepValue.setHeight((int) textStyle.font.getLineHeight());
        ly.setText(textStyle.font, text);
        lwidth = ly.width;
        upKeepValue.setPosition(position.width - paddingX - lwidth, upKeepText.getY());
        upKeepValue.setAlignment(Align.left);
        upKeepValue.setTouchable(Touchable.disabled);

        popSupportText = new Label(StringDB.getString("POPSUPPORT"), textStyle);
        popSupportText.setColor(textColor);
        popSupportText.setHeight((int) textStyle.font.getLineHeight());
        popSupportText.setPosition(position.x + paddingX, (int) (upKeepText.getY() - upKeepText.getHeight()));
        popSupportText.setTouchable(Touchable.disabled);

        text = "" + playerRace.getEmpire().getPopSupportCosts();
        popSupportVal = new Label(text, textStyle);
        popSupportVal.setColor(textColor);
        popSupportVal.setHeight((int) textStyle.font.getLineHeight());
        ly.setText(textStyle.font, text);
        lwidth = ly.width;
        popSupportVal.setPosition(position.width - paddingX - lwidth, popSupportText.getY());
        popSupportVal.setAlignment(Align.left);
        popSupportVal.setTouchable(Touchable.disabled);

        newsCntText = new Label(StringDB.getString("NEWS"), textStyle);
        newsCntText.setColor(textColor);
        newsCntText.setHeight((int) textStyle.font.getLineHeight());
        newsCntText.setPosition(position.x + paddingX,
                (int) (popSupportText.getY() - popSupportText.getHeight() - paddingY / 8));
        newsCntText.setTouchable(Touchable.disabled);

        text = "" + playerRace.getEmpire().getMsgs().size;
        newsCntVal = new Label(text, textStyle);
        newsCntVal.setColor(textColor);
        newsCntVal.setHeight((int) textStyle.font.getLineHeight());
        ly.setText(textStyle.font, text);
        lwidth = ly.width;
        newsCntVal.setPosition(position.width - paddingX - lwidth, newsCntText.getY());
        newsCntVal.setAlignment(Align.left);
        newsCntVal.setTouchable(Touchable.disabled);

        ratingText = new Label(StringDB.getString("RATING"), textStyle);
        ratingText.setColor(textColor);
        ratingText.setHeight((int) textStyle.font.getLineHeight());
        ratingText.setPosition(position.x + paddingX, (int) (newsCntText.getY() - newsCntText.getHeight()));
        ratingText.setTouchable(Touchable.disabled);

        text = String.format("%.1f", screenManager.getStatistics().getMark(playerRace.getRaceId()));
        ratingVal = new Label(text, textStyle);
        ratingVal.setColor(textColor);
        ratingVal.setHeight((int) textStyle.font.getLineHeight());
        ly.setText(textStyle.font, text);
        lwidth = ly.width;
        ratingVal.setPosition(position.width - paddingX - lwidth, ratingText.getY());
        ratingVal.setAlignment(Align.left);
        ratingVal.setTouchable(Touchable.disabled);

        starDateText = new Label(StringDB.getString("STARDATE") + ": "
                + String.format("%.1f", screenManager.getStarDate()), textStyle);
        starDateText.setColor(textColor);
        starDateText.setPosition(position.x, (int) (buttons[numButtons - 2].getY() - paddingY * 1.2f));
        starDateText.setWidth(position.width);
        starDateText.setAlignment(Align.center);

        stage.addActor(turnCounter);
        stage.addActor(creditText);
        stage.addActor(creditValue);
        stage.addActor(creditChangeText);
        stage.addActor(creditChangeVal);
        stage.addActor(upKeepText);
        stage.addActor(upKeepValue);
        stage.addActor(popSupportText);
        stage.addActor(popSupportVal);
        stage.addActor(newsCntText);
        stage.addActor(newsCntVal);
        stage.addActor(ratingText);
        stage.addActor(ratingVal);
        stage.addActor(starDateText);

        knowOtherMajors = false;
        knowOtherRaces = false;

        ActorGestureListener gestureListener = new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                if (count > 1 && screenManager.getScreen() instanceof UniverseRenderer) {
                    screenManager.getUniverseMap().searchNextIdleShipAndJumpToIt(ShipOrder.NONE);
                }
            }
        };
        gestureListener.getGestureDetector().setTapCountInterval(0.6f);
        gestureListener.getGestureDetector().setTapSquareSize(50f);
        bgImage.addListener(gestureListener);
    }

    private void updateLabels() {
        Major race = screenManager.getRaceController().getPlayerRace();
        turnCounter.setText(StringDB.getString("ROUND") + " " + screenManager.getCurrentRound());
        String text = "" + race.getEmpire().getCredits();

        creditValue.setText(text);
        GlyphLayout ly = new GlyphLayout(creditValue.getStyle().font, text);
        float lwidth = ly.width;
        creditValue.setX(position.width - lwidth - paddingX);

        text = "" + race.getEmpire().getCreditsChange();
        creditChangeVal.setText(text);
        ly.setText(creditChangeVal.getStyle().font, text);
        lwidth = ly.width;
        if (race.getEmpire().getCreditsChange() >= 0)
            creditChangeVal.setColor(new Color(0, 200 / 255.0f, 0, 1.0f));
        else
            creditChangeVal.setColor(new Color(200 / 255.0f, 0, 0, 1.0f));
        creditChangeVal.setPosition(position.width - paddingX - lwidth, creditChangeText.getY());

        text = "" + race.getEmpire().getShipCosts();
        upKeepValue.setText(text);
        if (race.getEmpire().getShipCosts() > race.getEmpire().getPopSupportCosts())
            upKeepValue.setColor(new Color(200 / 255.0f, 0, 0, 1.0f));
        else
            upKeepValue.setColor(race.getRaceDesign().clrNormalText);
        ly.setText(upKeepValue.getStyle().font, text);
        lwidth = ly.width;
        upKeepValue.setX(position.width - lwidth - paddingX);

        text = "" + race.getEmpire().getPopSupportCosts();
        popSupportVal.setText(text);
        ly.setText(popSupportVal.getStyle().font, text);
        lwidth = ly.width;
        popSupportVal.setX(position.width - lwidth - paddingX);

        text = "" + race.getEmpire().getMsgs().size;
        newsCntVal.setText(text);
        ly.setText(newsCntVal.getStyle().font, text);
        lwidth = ly.width;
        newsCntVal.setX(position.width - lwidth - paddingX);

        text = String.format("%.1f", screenManager.getStatistics().getMark(race.getRaceId()));
        ratingVal.setText(text);
        ly.setText(ratingVal.getStyle().font, text);
        lwidth = ly.width;
        ratingVal.setX(position.width - lwidth - paddingX);

        text = StringDB.getString("STARDATE") + ": " + String.format("%.1f", screenManager.getStarDate());
        starDateText.setText(text);
    }

    public void showInfo() {
        showInfo(true);
    }

    public Table getInfoTable() {
        return infoTable;
    }

    public int getInfoTableWidth() {
        return (int) (scrollPane.getWidth() - scrollPane.getStyle().vScrollKnob.getMinWidth());
    }

    public Image getInfoImage() {
        return infoImage;
    }

    public void showInfo(boolean reset) {
        Major r = screenManager.getRaceController().getPlayerRace();
        if (reset) {
            infoTable.clear();
            TextureRegion tex = new TextureRegion(screenManager.getSymbolTextures(r.getRaceId()));
            infoImage.setDrawable(new TextureRegionDrawable(new TextureRegion(tex)));
        }
        updateLabels();

        ArrayMap<String, Race> races = screenManager.getRaceController().getRaces();
        if (!knowOtherMajors || !knowOtherRaces)
            for (Iterator<Entry<String, Race>> iter = races.entries().iterator(); iter.hasNext();) {
                Entry<String, Race> e = iter.next();
                if (e.value.isMajor() && !e.key.equals(r.getRaceId()) && r.isRaceContacted(e.key)) {
                    knowOtherMajors = true;
                    knowOtherRaces = true;
                    break;
                } else if (!knowOtherRaces && !e.key.equals(r.getRaceId()) && r.isRaceContacted(e.key)) {
                    knowOtherRaces = true;
                }
            }

        for (int i = 0; i < buttons.length; i++) {
            if (!needEnabled(UIButtonType.values()[i], screenManager.getScreen())) {
                buttons[i].setDisabled(true);
                skin.setEnabled(buttons[i], false);
            } else {
                buttons[i].setDisabled(false);
                skin.setEnabled(buttons[i], true);
            }
            if (buttons[i].isOver()) {
                InputEvent event = new InputEvent();
                event.setType(Type.exit);
                event.setPointer(-1);
                buttons[i].fire(event);
            }
            if(UIButtonType.values()[i] == UIButtonType.END_TURN) {
                InputEvent event = new InputEvent();
                event.setType(Type.touchUp);
                event.setPointer(0);
                buttons[i].fire(event);
            }
        }
    }

    public boolean needEnabled(UIButtonType type, Screen parent) {
        switch (type) {
            case GALAXY_MAP_BUTTON:
                if (parent instanceof UniverseRenderer || screenManager.processingTurn())
                    return false;
                break;
            case OPTIONS:
                if (parent instanceof OptionsScreen || screenManager.processingTurn())
                    return false;
                break;
            case DIPLOMACY_BUTTON:
                if (!knowOtherRaces || parent instanceof DiplomacyScreen || screenManager.processingTurn())
                    return false;
                break;
            case EMPIRE_OVERVIEW_BUTTON:
                if (parent instanceof EmpireScreen || screenManager.processingTurn())
                    return false;
                break;
            case END_TURN:
                if (screenManager.processingTurn() || screenManager.isEndRoundPressed())
                    return false;
                break;
            case INTELLIGENCE_BUTTON:
                if (!knowOtherMajors || parent instanceof IntelScreen || screenManager.processingTurn())
                    return false;
                break;
            case RESEARCH_BUTTON:
                if (parent instanceof ResearchScreen || screenManager.processingTurn())
                    return false;
                break;
            case SYSTEM_VIEW_BUTTON:
                if (parent instanceof SystemBuildScreen || screenManager.processingTurn())
                    return false;
                break;
            case TRADE_BUTTON:
                if (parent instanceof TradeScreen || screenManager.processingTurn())
                    return false;
                break;
        }
        return true;
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
