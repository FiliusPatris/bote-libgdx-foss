/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.intelview;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.InputEvent.Type;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.intel.IntelObject;
import com.blotunga.bote.intel.Intelligence;
import com.blotunga.bote.intel.IntelObject.IntelObjectSortType;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.ui.screens.IntelScreen;
import com.blotunga.bote.utils.Pair;

public class IntelReportView {
    private ResourceManager manager;
    private Stage stage;
    private Skin skin;
    private float xOffset;
    private Major playerRace;
    private Color normalColor;
    private Color markColor;
    private Color oldColor;
    private Table nameTable;
    private Table headerTable;
    private TextureRegion selectTexture;
    private Table reportScrollTable;
    private ScrollPane reportScroller;
    private IntelObjectSortType oldSortType = null;
    private Button reportSelection = null;
    private Array<Button> reportItems;
    private int clickedReport;

    public IntelReportView(ScreenManager manager, Stage stage, Skin skin, float xOffset, float yOffset) {
        this.manager = manager;
        this.stage = stage;
        this.skin = skin;
        this.xOffset = xOffset;
        playerRace = manager.getRaceController().getPlayerRace();
        normalColor = playerRace.getRaceDesign().clrNormalText;
        markColor = playerRace.getRaceDesign().clrListMarkTextColor;

        nameTable = new Table();
        Rectangle rect = GameConstants.coordsToRelative(200, 805, 800, 40);
        nameTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        nameTable.align(Align.center);
        nameTable.setSkin(skin);
        stage.addActor(nameTable);
        nameTable.setVisible(false);

        headerTable = new Table();
        rect = GameConstants.coordsToRelative(110, 725, 975, 25);
        headerTable.align(Align.left);
        headerTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        stage.addActor(headerTable);
        headerTable.setVisible(false);

        reportScrollTable = new Table();
        reportScrollTable.align(Align.topLeft);
        reportScroller = new ScrollPane(reportScrollTable, skin);
        reportScroller.setScrollingDisabled(true, false);
        reportScroller.setVariableSizeKnobs(false);
        reportScroller.setFadeScrollBars(false);
        reportScroller.setScrollbarsOnTop(true);
        stage.addActor(reportScroller);
        reportScroller.setVisible(false);

        reportItems = new Array<Button>();
        selectTexture = manager.getUiTexture("listselect");
        clickedReport = -1;
    }

    public void show(final boolean all) {
        Intelligence intel = playerRace.getEmpire().getIntelligence();
        headerTable.clear();
        reportItems.clear();

        ActorGestureListener gestureListener = new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                IntelObjectSortType st = IntelObjectSortType.BY_ROUND;
                Label l = (Label) event.getListenerActor();
                st = ((IntelReportHeader) l.getUserObject()).sortType();
                IntelObject.setSortType(st);
                if (oldSortType != null && oldSortType == st)
                    playerRace.getEmpire().getIntelligence().getIntelReports().getAllReports().reverse();
                else
                    playerRace.getEmpire().getIntelligence().getIntelReports().getAllReports().sort();
                oldSortType = st;
                manager.getScreen().show();
            }
        };

        for (IntelReportHeader ih : IntelReportHeader.values()) {
            Label headerLabel = new Label(ih.getCaption(), skin, "largeFont", markColor);
            headerLabel.setUserObject(ih);
            headerLabel.addListener(gestureListener);
            headerTable.add(headerLabel).width(ih.getWidth());
        }
        headerTable.setVisible(true);

        reportScrollTable.clear();
        stage.setKeyboardFocus(reportScrollTable);
        reportScrollTable.addListener(new InputListener() {
            @Override
            public boolean keyDown(InputEvent event, final int keycode) {
                if (clickedReport < 0)
                    return false;
                if (reportItems.size <= clickedReport)
                    return false;

                Button b = reportItems.get(clickedReport);
                switch (keycode) {
                    case Keys.DOWN:
                        clickedReport++;
                        break;
                    case Keys.UP:
                        clickedReport--;
                        break;
                    case Keys.HOME:
                        clickedReport = 0;
                        break;
                    case Keys.END:
                        clickedReport = reportItems.size - 1;
                        break;
                    case Keys.PAGE_DOWN:
                        clickedReport += reportScroller.getScrollHeight() / b.getHeight();
                        break;
                    case Keys.PAGE_UP:
                        clickedReport -= reportScroller.getScrollHeight() / b.getHeight();
                        break;
                }
                clickedReport = MathUtils.clamp(clickedReport, 0, reportItems.size - 1);

                Thread th = new Thread() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(150);
                        } catch (InterruptedException e) {
                        }
                        if (Gdx.input.isKeyPressed(keycode)) {
                            Gdx.app.postRunnable(new Runnable() {
                                @Override
                                public void run() {
                                    InputEvent event = new InputEvent();
                                    event.setType(Type.keyDown);
                                    event.setKeyCode(keycode);
                                    reportScrollTable.fire(event);
                                }
                            });
                        }
                    }
                };
                th.start();
                drawChanges();
                return false;
            }
        });

        int numberOfReports = 0;
        for (int i = 0; i < intel.getIntelReports().getNumberOfReports(); i++) {
            IntelObject intelObj = intel.getIntelReports().getReport(i);
            if (all
                    || (intelObj.isSpy() && !intelObj.getEnemy().equals(playerRace.getRaceId()) && intelObj.getRound() > manager
                            .getCurrentRound() - 10)) {
                numberOfReports++;
                drawIntelObject(i);
            }
        }
        if (numberOfReports < clickedReport)
            clickedReport = -1;

        reportScroller.setVisible(true);
        nameTable.clear();
        String text = String.format(" (%d)", numberOfReports);
        if (all)
            text = StringDB.getString("SECURITY") + " - " + StringDB.getString("REPORTS") + text;
        else
            text = StringDB.getString("SECURITY") + " - " + StringDB.getString("POSSIBLE_ATTEMPTS") + text;
        nameTable.add(text, "hugeFont", normalColor);
        nameTable.setVisible(true);

        drawChanges();
    }

    private void drawChanges() {
        stage.draw();

        Button btn = null;
        if (clickedReport != -1) {
            for (Button b : reportItems) {
                if (clickedReport == getReport(b)) {
                    btn = b;
                    break;
                }
            }
        }
        if (btn == null) {
            if (reportItems.size > 0) {
                btn = reportItems.get(0);
                clickedReport = getReport(btn);
            }
        }
        if (btn != null)
            markReportListSelected(btn);
    }

    private void drawIntelObject(int index) {
        Intelligence intel = playerRace.getEmpire().getIntelligence();
        IntelObject intelObj = intel.getIntelReports().getReport(index);
        Color labelColor = new Color(normalColor);
        if (intelObj.getEnemy().equals(playerRace.getRaceId()))
            labelColor = new Color(150 / 255.0f, 150 / 255.0f, 150 / 255.0f, 1.0f);

        Button.ButtonStyle bs = new Button.ButtonStyle();
        Button button = new Button(bs);
        button.align(Align.left);
        button.setSkin(skin);
        button.add().width(GameConstants.wToRelative(10));
        Array<Label> labels = new Array<Label>();
        for (IntelReportHeader ih : IntelReportHeader.values()) {
            String text = "";
            if (ih == IntelReportHeader.ROUND)
                text = "" + intelObj.getRound();
            else if (ih == IntelReportHeader.KIND)
                text = intelObj.isSpy() ? StringDB.getString("SPY") : StringDB.getString("SABOTAGE");
            else if (ih == IntelReportHeader.ENEMY) {
                Major enemy = Major.toMajor(manager.getRaceController().getRace(intelObj.getEnemy()));
                if (enemy != null)
                    text = enemy.getEmpireName();
                else
                    text = StringDB.getString("UNKNOWN");
            } else if (ih == IntelReportHeader.TYPE) {
                switch (intelObj.getType()) {
                    case 0:
                        text = StringDB.getString("ECONOMY");
                        break;
                    case 1:
                        text = StringDB.getString("SCIENCE");
                        break;
                    case 2:
                        text = StringDB.getString("MILITARY");
                        break;
                    case 3:
                        text = StringDB.getString("DIPLOMACY");
                        break;
                    default:
                        text = StringDB.getString("UNKNOWN");
                        break;
                }
            }
            Label label = new Label(text, skin, "normalFont", Color.WHITE);
            label.setColor(labelColor);
            button.add(label).width(ih.getWidth());
            labels.add(label);
        }

        ActorGestureListener gestureListener = new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                Button b = (Button) event.getListenerActor();
                markReportListSelected(b);
                manager.getScreen().show();
            }
        };

        Pair<Integer, Array<Label>> pair = new Pair<Integer, Array<Label>>(index, labels);
        button.addListener(gestureListener);
        button.setUserObject(pair);
        button.add().width(GameConstants.wToRelative(10));
        reportScrollTable.add(button);
        reportScrollTable.row();
        reportItems.add(button);
    }

    public void hide() {
        nameTable.setVisible(false);
        headerTable.setVisible(false);
        reportScroller.setVisible(false);
    }

    public void setScrollerHeight(float height) {
        Rectangle rect = GameConstants.coordsToRelative(100, 695, 985, height);
        reportScroller.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
    }

    @SuppressWarnings("unchecked")
    private void markReportListSelected(Button b) {
        if (reportSelection != null) {
            Pair<Integer, Array<Label>> p = (Pair<Integer, Array<Label>>) reportSelection.getUserObject();
            reportSelection.getStyle().up = null;
            reportSelection.getStyle().down = null;
            for (Label l : p.getSecond())
                l.setColor(oldColor);
        }
        if (b == null)
            b = reportSelection;
        Image itemSelection = new Image(selectTexture);
        b.getStyle().up = itemSelection.getDrawable();
        b.getStyle().down = itemSelection.getDrawable();
        Pair<Integer, Array<Label>> p = (Pair<Integer, Array<Label>>) b.getUserObject();
        if (p.getSecond().size > 0) {
            oldColor = new Color(p.getSecond().get(0).getColor());
            for (Label l : p.getSecond())
                l.setColor(markColor);
        }
        reportSelection = b;
        clickedReport = p.getFirst();
        playerRace.getEmpire().getIntelligence().getIntelReports().setActiveReport(clickedReport);
        float buttonY = b.getHeight() * clickedReport;
        if (buttonY <= reportScroller.getScrollY() || buttonY > reportScroller.getScrollY() + reportScroller.getHeight())
            reportScroller.setScrollY(b.getHeight() * (clickedReport - 1));
        ((IntelScreen) manager.getScreen()).updateBottomInfo();
    }

    @SuppressWarnings("unchecked")
    private int getReport(Button b) {
        Pair<Integer, Array<Label>> p = (Pair<Integer, Array<Label>>) b.getUserObject();
        return p.getFirst();
    }
}
