/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.intelview;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.ArrayMap;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.intel.Intelligence;
import com.blotunga.bote.races.Major;

public class IntelInformation {
    private ResourceManager manager;
    private Skin skin;
    private Major playerRace;
    private Color normalColor;
    private TextureAtlas symbolAtlas;
    private String selectedRace;
    private Table nameTable;
    private Table infoTable;
    private ScrollPane blameDescPane;
    private Table blameDescTable;
    private Table blameTable;

    public IntelInformation(ScreenManager manager, Stage stage, Skin skin, float xOffset, float yOffset) {
        this.manager = manager;
        this.skin = skin;
        playerRace = manager.getRaceController().getPlayerRace();
        normalColor = playerRace.getRaceDesign().clrNormalText;
        symbolAtlas = manager.getAssetManager().get("graphics/symbols/symbols.pack");
        selectedRace = "";

        nameTable = new Table();
        Rectangle rect = GameConstants.coordsToRelative(200, 805, 800, 40);
        nameTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        nameTable.align(Align.center);
        nameTable.setSkin(skin);
        nameTable.add(StringDB.getString("SECURITY") + " - " + StringDB.getString("INFORMATION"), "hugeFont",
                normalColor);
        stage.addActor(nameTable);
        nameTable.setVisible(false);

        infoTable = new Table();
        rect = GameConstants.coordsToRelative(225, 420, 340, 275);
        infoTable.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        infoTable.align(Align.center);
        infoTable.setSkin(skin);
        stage.addActor(infoTable);
        infoTable.setVisible(false);

        rect = GameConstants.coordsToRelative(680, 380, 350, 80);
        blameDescTable = new Table();
        blameDescTable.align(Align.top);
        blameDescPane = new ScrollPane(blameDescTable, skin);
        blameDescPane.setVariableSizeKnobs(false);
        blameDescPane.setFadeScrollBars(false);
        blameDescPane.setScrollingDisabled(true, false);
        blameDescPane
                .setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        Label label = new Label(StringDB.getString("CHOOSE_RESPONSIBLE_RACE"), skin, "normalFont", normalColor);
        label.setAlignment(Align.top, Align.center);
        label.setWrap(true);
        label.setTouchable(Touchable.disabled);
        blameDescTable.add(label).width((int) (blameDescPane.getWidth() - blameDescPane.getStyle().vScrollKnob.getMinWidth()));
        stage.addActor(blameDescPane);
        blameDescPane.setVisible(false);

        blameTable = new Table();
        rect = GameConstants.coordsToRelative(785, 292, 150, 92);
        blameTable.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        blameTable.align(Align.center);
        blameTable.setSkin(skin);
        stage.addActor(blameTable);
        blameTable.setVisible(false);
    }

    public void show(String race) {
        this.selectedRace = race;
        ArrayMap<String, Major> majors = manager.getRaceController().getMajors();
        if (selectedRace.isEmpty()) {
            for (int i = 0; i < majors.size; i++) {
                String majorID = majors.getKeyAt(i);
                if (!majorID.equals(playerRace.getRaceId()) && playerRace.isRaceContacted(majorID)) {
                    selectedRace = majorID;
                    break;
                }
            }
        }
        Intelligence intel = playerRace.getEmpire().getIntelligence();
        intel.getIntelInfo().calcIntelInfo(manager, playerRace);

        nameTable.setVisible(true);
        infoTable.clear();
        blameDescPane.setVisible(true);
        blameTable.clear();
        Major responsibleRace = Major.toMajor(manager.getRaceController().getRace(intel.getResponsibleRace()));
        if (responsibleRace == null)
            responsibleRace = playerRace;

        TextButtonStyle smallButtonStyle = new TextButtonStyle(skin.get("small-buttons", TextButtonStyle.class));
        TextButton blameButton = new TextButton(responsibleRace.getName(), smallButtonStyle);
        blameButton.setUserObject(responsibleRace);
        blameButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                ArrayMap<String, Major> majors = manager.getRaceController().getMajors();
                Major responsibleRace = (Major) (event.getListenerActor().getUserObject());
                Intelligence intel = playerRace.getEmpire().getIntelligence();
                int index = majors.indexOfKey(responsibleRace.getRaceId());
                while (true) {
                    index++;
                    if (index == majors.size)
                        index = 0;
                    String raceID = majors.getKeyAt(index);
                    if (playerRace.isRaceContacted(raceID) || raceID.equals(playerRace.getRaceId())) {
                        intel.setResponsibleRace(raceID);
                        break;
                    }
                }
                show(selectedRace);
            }
        });
        blameTable.add(blameButton).align(Align.center).height((int) GameConstants.hToRelative(32));

        blameTable.row();
        blameTable.add(new Image(new TextureRegionDrawable(symbolAtlas.findRegion(responsibleRace.getRaceId()))))
                .height(GameConstants.hToRelative(60)).width((int) GameConstants.wToRelative(90));
        blameTable.setVisible(true);

        if (!selectedRace.isEmpty()) {
            TextureRegion region = symbolAtlas.findRegion(selectedRace);
            if (region == null)
                region = new TextureRegion(manager.loadTextureImmediate("graphics/buildings/ImageMissing.png"));

            Sprite sp = new Sprite(region);
            sp.setColor(0.5f, 0.5f, 0.5f, 1.0f);
            infoTable.setBackground(new SpriteDrawable(sp));
            addInfo(StringDB.getString("CONTROLLED_SECTORS") + ":",
                    intel.getIntelInfo().getControlledSectors(selectedRace));
            infoTable.row();
            addInfo(StringDB.getString("CONTROLLED_SYSTEMS") + ":", intel.getIntelInfo().getOwnedSystems(selectedRace));
            infoTable.row();
            addInfo(StringDB.getString("INHABITED_SYSTEMS") + ":",
                    intel.getIntelInfo().getInhabitedSystems(selectedRace));
            infoTable.row();
            addInfo(StringDB.getString("KNOWN_MINORRACES") + ":", intel.getIntelInfo().getKnownMinors(selectedRace));
            infoTable.row();
            addInfo(StringDB.getString("NUMBER_OF_MINORMEMBERS") + ":",
                    intel.getIntelInfo().getMinorMembers(selectedRace));

            infoTable.setVisible(true);
        }
    }

    private void addInfo(String text, int value) {
        float pad = GameConstants.wToRelative(5);
        Label label = new Label(text, skin, "largeFont", normalColor);
        label.setAlignment(Align.right);
        infoTable.add(label).height((int) infoTable.getHeight() / 5).spaceRight((int) pad).align(Align.right);
        text = "" + value;
        label = new Label(text, skin, "largeFont", normalColor);
        label.setAlignment(Align.left);
        infoTable.add(label).height((int) infoTable.getHeight() / 5).spaceLeft((int) pad).align(Align.left);
    }

    public void hide() {
        nameTable.setVisible(false);
        infoTable.setVisible(false);
        blameDescPane.setVisible(false);
        blameTable.setVisible(false);
    }
}
