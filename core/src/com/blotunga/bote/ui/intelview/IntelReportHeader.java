/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.intelview;

import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.intel.IntelObject.IntelObjectSortType;

public enum IntelReportHeader {
    ROUND("ROUND", IntelObjectSortType.BY_ROUND, 130),
    ENEMY("ENEMY", IntelObjectSortType.BY_ENEMY, 475),
    KIND("KIND", IntelObjectSortType.BY_ISSPY, 190),
    TYPE("TYPE", IntelObjectSortType.BY_TYPE, 190);

    private String caption;
    private IntelObjectSortType sortType;
    private float width;

    IntelReportHeader(String caption, IntelObjectSortType sortType, float width) {
        this.caption = caption;
        this.width = width;
        this.sortType = sortType;
    }

    public String getCaption() {
        if (sortType != IntelObjectSortType.BY_ENEMY)
            return StringDB.getString(caption);
        else
            return StringDB.getString(caption) + " (" + StringDB.getString("TARGET") + ")";
    }

    public float getWidth() {
        return GameConstants.wToRelative(width);
    }

    public IntelObjectSortType sortType() {
        return sortType;
    }
}
