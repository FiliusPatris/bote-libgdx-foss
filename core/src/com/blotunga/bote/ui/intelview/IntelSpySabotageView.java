/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.intelview;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.ArrayMap;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.intel.IntelAssignment;
import com.blotunga.bote.intel.Intelligence;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.utils.ui.PercentageWidget;
import com.blotunga.bote.utils.ui.ValueChangedEvent;

public class IntelSpySabotageView implements ValueChangedEvent {
    private ResourceManager manager;
    private Skin skin;
    private Major playerRace;
    private Table assignTable;
    private Color normalColor;
    private TextureAtlas symbolAtlas;
    private Table globalAssign;
    private Table nameTable;
    private String selectedRace;
    private Table assignHeader;
    private Table assignInfo;
    private Table aggressivityTable;
    private Table descriptionTable;

    public IntelSpySabotageView(ScreenManager manager, Stage stage, Skin skin, float xOffset, float yOffset) {
        this.manager = manager;
        this.skin = skin;
        playerRace = manager.getRaceController().getPlayerRace();
        normalColor = playerRace.getRaceDesign().clrNormalText;
        symbolAtlas = manager.getAssetManager().get("graphics/symbols/symbols.pack");

        nameTable = new Table();
        Rectangle rect = GameConstants.coordsToRelative(200, 805, 800, 40);
        nameTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        nameTable.align(Align.center);
        nameTable.setSkin(skin);
        stage.addActor(nameTable);
        nameTable.setVisible(false);

        globalAssign = new Table();
        rect = GameConstants.coordsToRelative(70, 562, 995, 40);
        globalAssign.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        globalAssign.align(Align.left);
        globalAssign.setSkin(skin);
        stage.addActor(globalAssign);
        globalAssign.setVisible(false);

        assignTable = new Table();
        rect = GameConstants.coordsToRelative(330, 455, 350, 300);
        assignTable.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        assignTable.align(Align.center);
        assignTable.setSkin(skin);
        stage.addActor(assignTable);
        assignTable.setVisible(false);
        selectedRace = "";

        assignHeader = new Table();
        rect = GameConstants.coordsToRelative(100, 455, 200, 300);
        assignHeader.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        assignHeader.align(Align.right);
        assignHeader.setSkin(skin);
        stage.addActor(assignHeader);
        assignHeader.setVisible(false);

        assignInfo = new Table();
        rect = GameConstants.coordsToRelative(700, 455, 200, 300);
        assignInfo.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        assignInfo.align(Align.left);
        assignInfo.setSkin(skin);
        stage.addActor(assignInfo);
        assignInfo.setVisible(false);

        aggressivityTable = new Table();
        rect = GameConstants.coordsToRelative(125, 505, 775, 60);
        aggressivityTable.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width,
                (int) rect.height);
        aggressivityTable.align(Align.center);
        aggressivityTable.setSkin(skin);
        stage.addActor(aggressivityTable);
        aggressivityTable.setVisible(false);

        descriptionTable = new Table();
        rect = GameConstants.coordsToRelative(150, 160, 725, 95);
        descriptionTable.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width,
                (int) rect.height);
        descriptionTable.align(Align.center);
        descriptionTable.setSkin(skin);
        stage.addActor(descriptionTable);
        descriptionTable.setVisible(false);
    }

    public String show(final int type, String race) {
        this.selectedRace = race;
        if (selectedRace.equals(playerRace.getRaceId()))
            selectedRace = "";
        ArrayMap<String, Major> majors = manager.getRaceController().getMajors();
        if (selectedRace.isEmpty()) {
            for (int i = 0; i < majors.size; i++) {
                String majorID = majors.getKeyAt(i);
                if (!majorID.equals(playerRace.getRaceId()) && playerRace.isRaceContacted(majorID)) {
                    selectedRace = majorID;
                    break;
                }
            }
        }
        nameTable.clear();
        String text = StringDB.getString("SECURITY") + " - ";
        text += type == 0 ? StringDB.getString("SPY") : StringDB.getString("SABOTAGE");
        nameTable.add(text, "hugeFont", normalColor);
        nameTable.setVisible(true);

        globalAssign.clear();
        assignTable.clear();
        assignHeader.clear();
        assignInfo.clear();
        aggressivityTable.clear();
        descriptionTable.clear();

        float lheight = GameConstants.hToRelative(30);
        float lpad = GameConstants.wToRelative(4);
        float lwidth = Math.round(GameConstants.wToRelative(14)) - (int) lpad;
        if (!selectedRace.isEmpty()) {
            Texture texture = manager.getAssetManager().get(GameConstants.UI_BG_SIMPLE);
            TextureRegion dr = new TextureRegion(texture);

            text = StringDB.getString("INTEL_RESERVE") + ":";
            globalAssign.add(text, "largeFont", normalColor).width(GameConstants.wToRelative(140));
            Intelligence intel = playerRace.getEmpire().getIntelligence();

            int store = 100;
            for (int i = 0; i < 4; i++)
                store -= (type == 0) ? intel.getAssignment().getSpyPercentages(selectedRace, i) : intel.getAssignment()
                        .getSabotagePercentages(selectedRace, i);

            PercentageWidget internalSec = new PercentageWidget(8 + type, skin, lwidth, lheight, lpad, 50, 0, 100,
                    store, dr, this);
            globalAssign.add(internalSec.getWidget()).width(GameConstants.hToRelative(803));
            text = "" + store + "%";
            Label label = new Label(text, skin, "largeFont", normalColor);
            label.setAlignment(Align.right);
            globalAssign.add(label).width(GameConstants.hToRelative(52));
            globalAssign.setVisible(true);

            TextureRegion region = symbolAtlas.findRegion(selectedRace);
            if (region == null)
                region = new TextureRegion(manager.loadTextureImmediate("graphics/buildings/ImageMissing.png"));

            Sprite sp = new Sprite(region);
            sp.setColor(0.5f, 0.5f, 0.5f, 1.0f);
            assignTable.setBackground(new SpriteDrawable(sp));
            lheight = GameConstants.hToRelative(30);
            lpad = GameConstants.wToRelative(4);
            lwidth = Math.round(GameConstants.wToRelative(12)) - (int) lpad;
            for (int i = 0; i < 4; i++) {
                if (i != 0)
                    assignTable.row();
                int perc = (type == 0) ? intel.getAssignment().getSpyPercentages(selectedRace, i) : intel
                        .getAssignment().getSabotagePercentages(selectedRace, i);
                PercentageWidget widget = new PercentageWidget(i * 2 + type, skin, lwidth, lheight, lpad, 25, 0, 100,
                        perc, dr, this);
                assignTable.add(widget.getWidget()).height((int) assignTable.getHeight() / 4);
                String s = "";
                switch (i) {
                    case 0:
                        s = StringDB.getString("ECONOMY");
                        break;
                    case 1:
                        s = StringDB.getString("SCIENCE");
                        break;
                    case 2:
                        s = StringDB.getString("MILITARY");
                        break;
                    case 3:
                        s = StringDB.getString("DIPLOMACY");
                        break;
                }
                if (!s.isEmpty()) {
                    if (i != 0)
                        assignHeader.row();
                    assignHeader.add(s + ":", "normalFont", normalColor).height((int) assignTable.getHeight() / 4);
                }
                long gp = 0;
                if (type == 0)
                    gp = (long) intel.getSecurityPoints()
                            * intel.getAssignment().getGlobalSpyPercentage(selectedRace)
                            * intel.getAssignment().getSpyPercentages(selectedRace, i)
                            / 10000
                            + (intel.getSPStorage(type, selectedRace)
                                    * intel.getAssignment().getSpyPercentages(selectedRace, i) / 100);
                else
                    gp = (long) intel.getSecurityPoints()
                            * intel.getAssignment().getGlobalSabotagePercentage(selectedRace)
                            * intel.getAssignment().getSabotagePercentages(selectedRace, i)
                            / 10000
                            + (intel.getSPStorage(type, selectedRace)
                                    * intel.getAssignment().getSabotagePercentages(selectedRace, i) / 100);
                gp += gp * intel.getBonus(i, type) / 100;
                s = String.format("%d%% (%d %s)", perc, gp, StringDB.getString("SP"));
                if (i != 0)
                    assignInfo.row();
                assignInfo.add(s, "normalFont", normalColor).height((int) assignTable.getHeight() / 4);
            }

            ButtonStyle bs = new ButtonStyle();
            Button button = new Button(bs);
            button.setSkin(skin);
            button.add(StringDB.getString("AGGRESSIVENESS") + ":", "normalFont", normalColor).align(Align.right)
                    .spaceRight((int) lpad).height((int) aggressivityTable.getHeight() / 2);
            TextButtonStyle smallButtonStyle = new TextButtonStyle(skin.get("small-buttons", TextButtonStyle.class));
            switch (intel.getAggressiveness(type, selectedRace)) {
                case 0:
                    text = StringDB.getString("CAREFUL");
                    break;
                case 1:
                    text = StringDB.getString("NORMAL");
                    break;
                case 2:
                    text = StringDB.getString("AGGRESSIVE");
                    break;
            }
            TextButton aggrButton = new TextButton(text, smallButtonStyle);
            button.add(aggrButton).align(Align.left).spaceLeft((int) lpad)
                    .height((int) aggressivityTable.getHeight() / 2);
            button.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    Intelligence intel = playerRace.getEmpire().getIntelligence();
                    int aggr = intel.getAggressiveness(type, selectedRace);
                    aggr++;
                    if (aggr == 3)
                        aggr = 0;
                    intel.setAgressiveness(type, selectedRace, aggr);
                    show(type, selectedRace);
                }
            });
            aggressivityTable.add(button);
            aggressivityTable.row();
            text = type == 0 ? StringDB.getString("SPY_OF_ALL", false, ""
                    + intel.getAssignment().getGlobalSpyPercentage(selectedRace)) : StringDB.getString(
                    "SABOTAGE_OF_ALL", false, "" + intel.getAssignment().getGlobalSabotagePercentage(selectedRace));
            aggressivityTable.add(text, "normalFont", normalColor).height((int) aggressivityTable.getHeight() / 2);

            Major major = Major.toMajor(manager.getRaceController().getRace(selectedRace));
            text = "";
            if (major != null) {
                text = StringDB.getString("USE_SP_FROM_DEPOT", false, "" + intel.getSPStorage(type, selectedRace),
                        major.getEmpireNameWithArticle());
                label = new Label(text, skin, "normalFont", normalColor);
                label.setWrap(true);
                label.setAlignment(Align.center);
                descriptionTable.add(label).width((int) descriptionTable.getWidth())
                        .height((int) descriptionTable.getHeight());
            }
            descriptionTable.setVisible(true);
            aggressivityTable.setVisible(true);
            assignTable.setVisible(true);
            assignHeader.setVisible(true);
            assignInfo.setVisible(true);
        }
        return selectedRace;
    }

    public void hide() {
        descriptionTable.setVisible(false);
        aggressivityTable.setVisible(false);
        assignTable.setVisible(false);
        nameTable.setVisible(false);
        globalAssign.setVisible(false);
        assignHeader.setVisible(false);
        assignInfo.setVisible(false);
    }

    @Override
    public void valueChanged(int typeID, int value) {
        IntelAssignment assignment = playerRace.getEmpire().getIntelligence().getAssignment();
        if (typeID % 2 == 0) { //SPY
            assignment.setSpyPercentage(typeID / 2, value, selectedRace);
        } else { //SABOTAGE
            assignment.setSabotagePercentage(typeID / 2, value, selectedRace);
        }
        show(typeID % 2, selectedRace);
    }
}
