/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.intelview;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.ArrayMap;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.ui.screens.IntelScreen;

public class IntelRaceLogos {
    private ResourceManager manager;
    private Major playerRace;
    private Table logoTable;
    private TextureAtlas symbolAtlas;
    private String selectedRace;

    public IntelRaceLogos(ScreenManager manager, Stage stage, Skin skin, float xOffset, float yOffset) {
        this.manager = manager;
        playerRace = manager.getRaceController().getPlayerRace();
        symbolAtlas = manager.getAssetManager().get("graphics/symbols/symbols.pack");
        logoTable = new Table();
        Rectangle rect = GameConstants.coordsToRelative(20, 500, 85, 440);
        logoTable.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        logoTable.align(Align.topLeft);
        logoTable.setSkin(skin);
        stage.addActor(logoTable);
        logoTable.setVisible(false);
        selectedRace = "";
    }

    public void show() {
        show(false);
    }

    public void show(boolean highLightPlayerRace) {
        logoTable.clear();
        float imgHeight = logoTable.getHeight() / 6;
        ArrayMap<String, Major> majors = manager.getRaceController().getMajors();
        for (int i = 0; i < majors.size; i++) {
            String majorID = majors.getKeyAt(i);
            if (i != 0)
                logoTable.row();
            TextureRegion region = symbolAtlas.findRegion(majorID);
            if (region == null)
                region = new TextureRegion(manager.loadTextureImmediate("graphics/buildings/ImageMissing.png"));
            Sprite sprite = new Sprite(region);
            Color color = new Color(.5f, .5f, .5f, 160 / 255.0f);
            if ((!majorID.equals(playerRace.getRaceId()) && playerRace.isRaceContacted(majorID))
                    || (majorID.equals(playerRace.getRaceId()) && highLightPlayerRace))
                color = Color.WHITE;
            sprite.setColor(color);
            Image logo = new Image(new SpriteDrawable(sprite));
            logo.setUserObject(majorID);
            if ((!majorID.equals(playerRace.getRaceId()) && playerRace.isRaceContacted(majorID))
                    || (majorID.equals(playerRace.getRaceId()) && highLightPlayerRace))
                logo.addListener(new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        selectedRace = (String) event.getListenerActor().getUserObject();
                        ((IntelScreen) manager.getScreen()).show();
                    }
                });
            logoTable.add(logo).height((int) imgHeight);
        }
        logoTable.setVisible(true);
    }

    public void hide() {
        logoTable.setVisible(false);
    }

    public String getSelectedRace() {
        return selectedRace;
    }

    public void setSelectedRace(String race) {
        selectedRace = race;
    }
}
