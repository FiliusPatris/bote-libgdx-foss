/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.intelview;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.intel.Intelligence;
import com.blotunga.bote.races.Major;

public class IntelInfos {
    private Skin skin;
    private Major playerRace;
    private Table infoTable;
    private Color normalColor;
    private Color markColor;

    public IntelInfos(ScreenManager manager, Stage stage, Skin skin, float xOffset, float yOffset) {
        this.skin = skin;
        playerRace = manager.getRaceController().getPlayerRace();
        normalColor = playerRace.getRaceDesign().clrNormalText;
        markColor = playerRace.getRaceDesign().clrListMarkTextColor;
        infoTable = new Table();
        Rectangle rect = GameConstants.coordsToRelative(945, 515, 205, 455);
        infoTable.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        infoTable.align(Align.top);
        infoTable.setSkin(skin);
        stage.addActor(infoTable);
        infoTable.setVisible(false);
    }

    public void show() {
        show("");
    }

    public void show(String activeRace) {
        infoTable.clear();
        infoTable.add(StringDB.getString("SECURITYPOINTS"), "mediumFont", normalColor);
        infoTable.row();

        String text = StringDB.getString("TOTAL").toUpperCase() + ":";
        String text2 = String.format("%d %s", playerRace.getEmpire().getSecurityPoints(), StringDB.getString("SP"));
        addLine(text, text2, (int) GameConstants.hToRelative(32));

        infoTable.row();
        infoTable.add(StringDB.getString("SECURITYBONI"), "mediumFont", normalColor).height(
                GameConstants.hToRelative(29));

        Intelligence intel = playerRace.getEmpire().getIntelligence();
        text = StringDB.getString("INNER_SECURITY_SHORT").toUpperCase() + ":";
        text2 = String.format("%d%%", intel.getInnerSecurityBonus());
        addLine(text, text2, (int) GameConstants.hToRelative(30));

        infoTable.row();
        infoTable.add(StringDB.getString("SPY"), "mediumFont", normalColor).height(GameConstants.hToRelative(29));

        text = StringDB.getString("ECONOMY").toUpperCase() + ":";
        text2 = String.format("%d%%", intel.getEconomyBonus(0));
        addLine(text, text2, (int) GameConstants.hToRelative(29));

        text = StringDB.getString("SCIENCE").toUpperCase() + ":";
        text2 = String.format("%d%%", intel.getScienceBonus(0));
        addLine(text, text2, (int) GameConstants.hToRelative(29));

        text = StringDB.getString("MILITARY").toUpperCase() + ":";
        text2 = String.format("%d%%", intel.getMilitaryBonus(0));
        addLine(text, text2, (int) GameConstants.hToRelative(29));

        infoTable.row();
        infoTable.add(StringDB.getString("SABOTAGE"), "mediumFont", normalColor).height(GameConstants.hToRelative(29));

        text = StringDB.getString("ECONOMY").toUpperCase() + ":";
        text2 = String.format("%d%%", intel.getEconomyBonus(1));
        addLine(text, text2, (int) GameConstants.hToRelative(27));

        text = StringDB.getString("SCIENCE").toUpperCase() + ":";
        text2 = String.format("%d%%", intel.getScienceBonus(1));
        addLine(text, text2, (int) GameConstants.hToRelative(27));

        text = StringDB.getString("MILITARY").toUpperCase() + ":";
        text2 = String.format("%d%%", intel.getMilitaryBonus(1));
        addLine(text, text2, (int) GameConstants.hToRelative(27));

        infoTable.row();
        infoTable.add(StringDB.getString("DEPOTS"), "mediumFont", normalColor).height(GameConstants.hToRelative(38));

        text = StringDB.getString("INNER_SECURITY_SHORT").toUpperCase() + ":";
        text2 = String.format("%d", intel.getInnerSecurityStorage());
        addLine(text, text2, (int) GameConstants.hToRelative(26));

        if (!activeRace.isEmpty() && !activeRace.equals(playerRace.getRaceId())) {
            text = StringDB.getString("SPY").toUpperCase() + ":";
            text2 = String.format("%d", intel.getSPStorage(0, activeRace));
            addLine(text, text2, (int) GameConstants.hToRelative(26));

            text = StringDB.getString("SABOTAGE").toUpperCase() + ":";
            text2 = String.format("%d", intel.getSPStorage(1, activeRace));
            addLine(text, text2, (int) GameConstants.hToRelative(26));
        }

        infoTable.setVisible(true);
    }

    public void hide() {
        infoTable.setVisible(false);
    }

    private void addLine(String text, String text2, int height) {
        infoTable.row();
        Button.ButtonStyle bs = new Button.ButtonStyle();
        Button button = new Button(bs);
        button.align(Align.center);
        button.setSkin(skin);
        BitmapFont font = skin.getFont("mediumFont");
        button.add(text, "mediumFont", markColor);
        GlyphLayout layout = new GlyphLayout(font, text);
        float width = layout.width;
        layout.setText(font, text2);
        float sp = infoTable.getWidth() - width - layout.width;
        sp = Math.max(sp, 0);
        button.add(text2, "mediumFont", markColor).spaceLeft(sp).height(height);
        infoTable.add(button);
    }
}
