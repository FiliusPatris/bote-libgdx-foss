/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.intelview;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.ArrayMap;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.intel.IntelAssignment;
import com.blotunga.bote.intel.Intelligence;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.utils.ui.PercentageWidget;
import com.blotunga.bote.utils.ui.ValueChangedEvent;

public class IntelAssignmentView implements ValueChangedEvent {
    private Skin skin;
    private ScreenManager manager;
    private Major playerRace;
    private Color normalColor;
    private Table nameTable;
    private Table globalAssign;
    private Table assignHeader;
    private Table assignTable;

    public IntelAssignmentView(final ScreenManager manager, Stage stage, Skin skin, float xOffset, float yOffset) {
        this.manager = manager;
        this.skin = skin;
        playerRace = manager.getRaceController().getPlayerRace();
        normalColor = playerRace.getRaceDesign().clrNormalText;

        nameTable = new Table();
        Rectangle rect = GameConstants.coordsToRelative(200, 805, 800, 40);
        nameTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        nameTable.align(Align.center);
        nameTable.setSkin(skin);
        stage.addActor(nameTable);
        String s = StringDB.getString("SECURITY") + " - " + StringDB.getString("SECURITY_HEADQUARTERS");
        nameTable.add(s, "hugeFont", normalColor);
        nameTable.setVisible(false);

        globalAssign = new Table();
        rect = GameConstants.coordsToRelative(70, 562, 995, 40);
        globalAssign.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        globalAssign.align(Align.left);
        globalAssign.setSkin(skin);
        stage.addActor(globalAssign);
        globalAssign.setVisible(false);

        assignHeader = new Table();
        rect = GameConstants.coordsToRelative(90, 515, 800, 25);
        assignHeader.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        assignHeader.align(Align.topLeft);
        assignHeader.setSkin(skin);
        stage.addActor(assignHeader);
        assignHeader.setVisible(false);

        assignTable = new Table();
        rect = GameConstants.coordsToRelative(127, 500, 769, 450);
        assignTable.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        assignTable.align(Align.topLeft);
        assignTable.setSkin(skin);
        stage.addActor(assignTable);
        assignTable.setVisible(false);
    }

    public void show() {
        nameTable.setVisible(true);

        globalAssign.clear();
        Texture texture = manager.getAssetManager().get(GameConstants.UI_BG_SIMPLE);
        TextureRegion dr = new TextureRegion(texture);

        globalAssign.add(StringDB.getString("INNER_SECURITY") + ":", "largeFont", normalColor).width(
                GameConstants.wToRelative(140));
        Intelligence intel = playerRace.getEmpire().getIntelligence();
        int innerSecurityPerc = intel.getAssignment().getInnerSecurityPercentage();
        float lheight = GameConstants.hToRelative(30);
        float lpad = GameConstants.wToRelative(4);
        float lwidth = Math.round(GameConstants.wToRelative(14)) - (int) lpad;

        PercentageWidget internalSec = new PercentageWidget(12, skin, lwidth, lheight, lpad, 50, 0, 100,
                innerSecurityPerc, dr, this);
        globalAssign.add(internalSec.getWidget()).width(GameConstants.hToRelative(803));
        String text = "" + innerSecurityPerc + "%";
        Label label = new Label(text, skin, "largeFont", normalColor);
        label.setAlignment(Align.right);
        globalAssign.add(label).width(GameConstants.hToRelative(52));
        globalAssign.setVisible(true);

        float width = assignHeader.getWidth() / 2;
        assignHeader.clear();
        text = StringDB.getString("SPY");
        label = new Label(text, skin, "largeFont", normalColor);
        label.setAlignment(Align.center);
        assignHeader.add(label).width((int) width);
        text = StringDB.getString("SABOTAGE");
        label = new Label(text, skin, "largeFont", normalColor);
        label.setAlignment(Align.center);
        assignHeader.add(label).width((int) width);
        assignHeader.setVisible(true);

        width = assignTable.getWidth() / 2;
        assignTable.clear();
        ArrayMap<String, Major> majors = manager.getRaceController().getMajors();
        for (int i = 0; i < majors.size; i++) {
            String majorID = majors.getKeyAt(i);
            if (i != 0)
                assignTable.row();
            for (int j = 0; j < 2; j++) {
                lheight = GameConstants.hToRelative(49);
                lpad = GameConstants.wToRelative(4);
                lwidth = Math.round(GameConstants.wToRelative(14)) - (int) lpad;
                IntelAssignment assignment = intel.getAssignment();
                int value = j == 0 ? assignment.getGlobalSpyPercentage(majorID) : assignment
                        .getGlobalSabotagePercentage(majorID);
                PercentageWidget widget = new PercentageWidget(j + i * 2, skin, lwidth, lheight / 2, lpad, 20, 0, 100,
                        value, dr, this, majorID.equals(playerRace.getRaceId()) || !playerRace.isRaceContacted(majorID));
                Table row = new Table();
                row.add(widget.getWidget()).width((int) GameConstants.wToRelative(360)).height((int) lheight * 1.5f);
                text = "" + value + "%";
                label = new Label(text, skin, "normalFont", normalColor);
                label.setAlignment(Align.right);
                row.add(label).width((int) GameConstants.wToRelative(25));
                assignTable.add(row).width((int) width);
            }
        }

        assignTable.setVisible(true);
    }

    public void hide() {
        nameTable.setVisible(false);
        globalAssign.setVisible(false);
        assignTable.setVisible(false);
        assignHeader.setVisible(false);
    }

    @Override
    public void valueChanged(int typeID, int value) {
        IntelAssignment assignment = playerRace.getEmpire().getIntelligence().getAssignment();
        ArrayMap<String, Major> majors = manager.getRaceController().getMajors();
        if (typeID < 12) {
            String raceID = majors.getKeyAt(typeID / 2);
            assignment.setGlobalPercentage(typeID % 2, value, playerRace, raceID, majors);
        } else {
            assignment.setGlobalPercentage(2, value, playerRace, "", majors);
        }
        show();
    }
}
