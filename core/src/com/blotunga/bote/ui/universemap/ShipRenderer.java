/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.universemap;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.Disposable;
import com.blotunga.bote.GamePreferences;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.AnomalyType;
import com.blotunga.bote.constants.CombatTactics;
import com.blotunga.bote.constants.DiplomaticAgreement;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.ShipOrder;
import com.blotunga.bote.constants.ShipType;
import com.blotunga.bote.constants.SndMgrValue;
import com.blotunga.bote.constants.ViewTypes;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.races.Race;
import com.blotunga.bote.races.Research;
import com.blotunga.bote.races.Race.RaceSpecialAbilities;
import com.blotunga.bote.ships.ShipMap;
import com.blotunga.bote.ships.Ships;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.ui.screens.CombatScreen;
import com.blotunga.bote.ui.screens.FleetScreen;
import com.blotunga.bote.ui.screens.UniverseRenderer;
import com.blotunga.bote.ui.screens.UniverseRenderer.GalaxyState;
import com.blotunga.bote.utils.IntPoint;
import com.blotunga.bote.utils.Pair;
import com.blotunga.bote.utils.ui.BaseTooltip;
import com.blotunga.bote.utils.ui.ScrollEvent;
import com.blotunga.bote.utils.ui.VerticalScroller;

public class ShipRenderer implements Disposable, ScrollEvent {
    private enum MainButton {
        MAIN_BUTTON_NONE(""),
        MAIN_BUTTON_COMBAT_BEHAVIOR("COMBAT_BEHAVIOR"),
        MAIN_BUTTON_IMMEDIATE_ORDER("BTN_IMMEDIATE_ORDER"),
        MAIN_BUTTON_SINGLE_TURN_ORDER("BTN_SINGLE_TURN_ORDER"),
        MAIN_BUTTON_MULTI_TURN_ORDER("BTN_MULTI_TURN_ORDER"),
        MAIN_BUTTON_CANCEL("BTN_CANCEL");

        private String dbText;

        MainButton(String dbText) {
            this.dbText = dbText;
        }

        public String getDbText() {
            return dbText;
        }
    }

    private final TextureAtlas uiAtlas;
    private final TextureAtlas buttonAtlas;
    private final ScreenManager screenManager;
    private Major playerRace;
    private Ships selectedShip = null;
    private TextureRegion shipSelectTexture;
    private Table scrollTable;
    private VerticalScroller shipScroller;
    private Button oldSelection = null;
    final private float padding;
    private float shipWidth;
    private float shipHeight;
    private int currentPage;
    private int pageCnt;
    private Array<Pair<Button, Ships>> shipImgs;
    private TextButton prevButton;
    private TextButton nextButton;
    private Array<TextButton> mainShipOrders;
    private Array<TextButton> secondaryShipOrders;
    private Label selectLabel = null;
    private int shipsInPage = 0;
    private Array<String> loadedTextures;
    private int colCnt;

    //tooltip constants
    private Color headerColor;
    private Color textColor;
    private String headerFont = "xlFont";
    private String textFont = "largeFont";

    public ShipRenderer(ScreenManager screenManager, Rectangle position, float shipWidth,
            float shipHeight, float padding) {
        this.screenManager = screenManager;
        this.padding = padding;
        this.shipWidth = shipWidth;
        this.shipHeight = shipHeight;
        playerRace = screenManager.getRaceController().getPlayerRace();
        uiAtlas = screenManager.getAssetManager().get("graphics/ui/general_ui.pack");
        buttonAtlas = screenManager.getAssetManager().get("graphics/ui/" + playerRace.getPrefix() + "ui.pack");
        headerColor = playerRace.getRaceDesign().clrListMarkTextColor;
        textColor = playerRace.getRaceDesign().clrNormalText;

        shipImgs = new Array<Pair<Button, Ships>>();
        mainShipOrders = new Array<TextButton>();
        secondaryShipOrders = new Array<TextButton>();

        shipSelectTexture = screenManager.getUiTexture("shipselect");
        loadedTextures = new Array<String>();
        scrollTable = new Table();
        shipScroller = new VerticalScroller(scrollTable);

        scrollTable.setBounds((int) position.x, (int) position.y, (int) position.width, (int) position.height);
        shipScroller.setBounds(scrollTable.getX(), scrollTable.getY(), scrollTable.getWidth(), scrollTable.getHeight());
        shipScroller.setScrollingDisabled(true, false);
        shipScroller.setEventHandler(this);
        colCnt = 0;
    }

    public void drawShips(StarSystem starsystemInfo, Ships ships, Stage stage, Skin skin) {
        for (TextButton button : mainShipOrders) {
            button.setVisible(false);
            button.remove();
        }
        for (TextButton button : secondaryShipOrders) {
            button.setVisible(false);
            button.remove();
        }
        mainShipOrders.clear();
        secondaryShipOrders.clear();
        scrollTable.clear();
        shipImgs.clear();
        colCnt = 0;

        //Buttons
        if (!skin.has("shiporderbutton", Sprite.class)) {
            TextureRegion buttonTex = new TextureRegion(buttonAtlas.findRegion(playerRace.getPrefix()
                    + "button_shiporder"));
            skin.add("shiporderbutton", new Sprite(buttonTex));
        }
        TextButtonStyle style = new TextButtonStyle(skin.get("default", TextButtonStyle.class));
        style.font = screenManager.getRacialFont(playerRace.getRaceDesign().fontName, (int) (GamePreferences.sceneHeight / 55));
        style.fontColor = playerRace.getRaceDesign().clrSmallBtn;
        style.up = skin.getDrawable("shiporderbutton");
        style.down = skin.newDrawable("shiporderbutton", Color.DARK_GRAY);
        style.over = skin.newDrawable("shiporderbutton", Color.LIGHT_GRAY);

        if (nextButton == null) {
            //next
            nextButton = new TextButton(StringDB.getString("BTN_NEXT"), style);
            nextButton.setWidth(shipScroller.getWidth() / 2);
            nextButton.setHeight(nextButton.getWidth() / 4);
            nextButton.setPosition((int) (shipScroller.getX() + shipScroller.getWidth() / 2),
                    (int) (shipScroller.getY() - nextButton.getHeight() - padding / 2));
            nextButton.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    currentPage++;
                    if (currentPage <= pageCnt) {
                        shipScroller.setScrollY(currentPage * shipHeight * shipsInPage);
                        prevButton.setVisible(true);
                    }
                    if (currentPage == pageCnt) {
                        nextButton.setVisible(false);
                    }
                }
            });
        }

        if (prevButton == null) {
            //prevbutton
            prevButton = new TextButton(StringDB.getString("BTN_BACK"), style);
            prevButton.setWidth(shipScroller.getWidth() / 2);
            prevButton.setHeight(prevButton.getWidth() / 4);
            prevButton.setPosition((int) (shipScroller.getX()),
                    (int) (shipScroller.getY() - prevButton.getHeight() - padding / 2));
            prevButton.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    currentPage--;
                    if (currentPage >= 0) {
                        shipScroller.setScrollY(currentPage * shipHeight * shipsInPage);
                        nextButton.setVisible(true);
                    }
                    if (currentPage == 0) {
                        prevButton.setVisible(false);
                    }
                }
            });
        }

        int cnt = 0;
        pageCnt = 0;
        currentPage = 0;
        if (ships == null) {
            //main command buttons
            int i = 0;
            for (MainButton b : MainButton.values()) {
                if (b != MainButton.MAIN_BUTTON_NONE) {
                    TextButton button = new TextButton(StringDB.getString(b.getDbText()), style);
                    button.setWidth(shipScroller.getWidth() / 2);
                    button.setHeight(button.getWidth() / 4);
                    button.setPosition(nextButton.getX(), prevButton.getY() - (button.getHeight() + padding / 2)
                            * (i + 1));
                    button.setUserObject(b);
                    button.setVisible(false);
                    for (Actor a : button.getChildren())
                        a.setTouchable(Touchable.disabled);
                    stage.addActor(button);
                    button.addListener(new ClickListener() {
                        @Override
                        public void clicked(InputEvent event, float x, float y) {
                            if (selectedShip != null) {
                                TextButton b = (TextButton) event.getListenerActor();
                                if (b.getUserObject().equals(MainButton.MAIN_BUTTON_CANCEL)) {
                                    selectedShip.setCurrentOrderAccordingToType();
                                    selectedShip.setTargetCoord(new IntPoint());
                                    if (screenManager.getScreen() instanceof UniverseRenderer) {
                                        UniverseRenderer ur = (UniverseRenderer) screenManager.getScreen();
                                        ur.getRightSideBar().refreshShips();
                                    }
                                    clearSelectedShip();
                                } else {
                                    showCorrespondingSecondaryButtons(b);
                                }
                            }
                        }
                    });
                    mainShipOrders.add(button);
                    i++;
                }
            }

            //secondary command buttons
            for (i = 0; i < mainShipOrders.size; i++) {
                TextButton button = new TextButton("", style);
                button.setWidth(shipScroller.getWidth() / 2);
                button.setHeight(button.getWidth() / 4);
                button.setPosition(prevButton.getX(), prevButton.getY() - (button.getHeight() + padding / 2) * (i + 1));
                button.setVisible(false);
                button.setUserObject(ShipOrder.NONE);
                for (Actor a : button.getChildren())
                    a.setTouchable(Touchable.disabled);
                stage.addActor(button);
                secondaryShipOrders.add(button);
                button.addListener(new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        TextButton b = (TextButton) event.getListenerActor();
                        ShipOrder order = (ShipOrder) b.getUserObject();
                        if (selectedShip != null) {
                            if (order != ShipOrder.NONE && order != ShipOrder.AVOID && order != ShipOrder.ATTACK
                                    && order != ShipOrder.ENCLOAK && order != ShipOrder.DECLOAK
                                    && order != ShipOrder.ASSIGN_FLAGSHIP && order != ShipOrder.CREATE_FLEET
                                    && order != ShipOrder.TRANSPORT) {
                                selectedShip.setTargetCoord(new IntPoint());
                                selectedShip.setPath(new Array<IntPoint>());
                            }
                            if (order == ShipOrder.CREATE_FLEET) {
                                ShipMap map = screenManager.getUniverseMap().getShipMap();
                                map.setFleetShip(map.currentShip());
                                screenManager.setView(ViewTypes.FLEET_VIEW, true);
                            } else if (order == ShipOrder.ASSIGN_FLAGSHIP) {
                                ShipMap map = screenManager.getUniverseMap().getShipMap();
                                for (int i = 0; i < map.getSize(); i++) {
                                    Ships s = map.getAt(i);
                                    if (!s.getOwnerId().equals(selectedShip.getOwnerId()))
                                        continue;
                                    s.unassignFlagship();
                                }
                                selectedShip.setFlagShip(true);
                            } else if (order == ShipOrder.TRANSPORT) {
                                screenManager.setView(ViewTypes.TRANSPORT_VIEW);
                            } else if (order == ShipOrder.TERRAFORM) {
                                screenManager.getSoundManager().playSound(SndMgrValue.SNDMGR_MSG_TERRAFORM_SELECT,
                                        playerRace.getPrefix());
                                if (screenManager.getScreen() instanceof UniverseRenderer) {
                                    UniverseRenderer ur = (UniverseRenderer) screenManager.getScreen();
                                    ur.showPlanets();
                                    ur.setState(GalaxyState.TERRAFORM);
                                }
                            } else if (order == ShipOrder.ATTACK) {
                                selectedShip.setCombatTactics(CombatTactics.CT_ATTACK);
                            } else if (order == ShipOrder.AVOID) {
                                selectedShip.setCombatTactics(CombatTactics.CT_AVOID);
                            } else
                                selectedShip.setCurrentOrder(order);

                            if (order == ShipOrder.COLONIZE)
                                screenManager.getSoundManager().playSound(SndMgrValue.SNDMGR_MSG_COLONIZING,
                                        playerRace.getPrefix());
                            else if (order == ShipOrder.BUILD_OUTPOST || order == ShipOrder.UPGRADE_OUTPOST)
                                screenManager.getSoundManager().playSound(SndMgrValue.SNDMGR_MSG_OUTPOST_CONSTRUCT,
                                        playerRace.getPrefix());
                            else if (order == ShipOrder.BUILD_STARBASE || order == ShipOrder.UPGRADE_STARBASE)
                                screenManager.getSoundManager().playSound(SndMgrValue.SNDMGR_MSG_STARBASE_CONSTRUCT,
                                        playerRace.getPrefix());

                            if (order != ShipOrder.TERRAFORM && order != ShipOrder.CREATE_FLEET && order != ShipOrder.TRANSPORT) {
                                if (screenManager.getScreen() instanceof UniverseRenderer) {
                                    UniverseRenderer ur = (UniverseRenderer) screenManager.getScreen();
                                    ur.getRightSideBar().refreshShips();
                                    clearSelectedShip();
                                }
                            }

                        }
                    }
                });
            }

            //ships themselves
            ArrayMap<String, ShipMap> sh = starsystemInfo.getShipsInSector();
            for (int j = 0; j < sh.size; j++) {
                ShipMap map = sh.getValueAt(j);
                for (int k = 0; k < map.getSize(); k++) {
                    Ships s = map.getAt(k);
                    boolean shouldDrawShip = starsystemInfo.shouldDrawShip(playerRace, s.getOwnerId());
                    boolean shouldDrawOutpost = starsystemInfo.shouldDrawOutpost(playerRace, s.getOwnerId());
                    if (shouldDrawShip || shouldDrawOutpost) {
                        if ((!s.isStation() && shouldDrawShip) || (s.isStation() && shouldDrawOutpost)) {
                            drawShip(scrollTable, s, skin, true);
                            if(cnt == 0 && shipsInPage == 0) {
                                stage.draw();
                                shipHeight = scrollTable.getCells().get(0).getPrefHeight();
                                shipsInPage = Math.max(1, (int) (shipScroller.getHeight() / shipHeight));
                                shipScroller.setHeight(shipsInPage * shipHeight);
                            }
                            cnt++;
                        }
                    }
                }
            }
        } else {
            drawShip(scrollTable, ships, skin, false);
            if(shipsInPage == 0) {
                stage.draw();
                shipHeight = scrollTable.getCells().get(0).getPrefHeight();
                shipsInPage = Math.max(1, (int) (shipScroller.getHeight() / shipHeight));
                shipScroller.setHeight(shipsInPage * shipHeight);
            }
            cnt++;
            for (int k = 0; k < ships.getFleet().getSize(); k++) {
                Ships s = ships.getFleet().getAt(k);
                cnt++;
                drawShip(scrollTable, s, skin, false);
            }
        }
        stage.addActor(shipScroller);
        if(shipsInPage > 0 && cnt > 0)
            pageCnt = (cnt - 1) / shipsInPage;

        if (pageCnt > 0) {
            stage.addActor(nextButton);
            stage.addActor(prevButton);
            prevButton.setVisible(false);
        }
        currentPage = (int) Math.ceil(shipScroller.getScrollY() / (shipHeight * shipsInPage));
        showNextPrevButtonsIfNeeded();
        shipScroller.setVisible(true);

        if (ships == null) {
            if (selectedShip != null) {
                stage.draw(); //this is needed to validate layout before marking selection
                selectShip(selectedShip);
                drawSidebarInfo(selectedShip, skin);
            } else {
                if (cnt == 1) {//Only one ship found -> activate it
                    stage.draw(); //this is needed to validate layout before marking selection
                    Pair<Button, Ships> p = shipImgs.get(0);
                    Ships s = p.getSecond();
                    selectShip(s);
                    drawSidebarInfo(s, skin);
                }
            }
        }
        showNextPrevButtonsIfNeeded();
    }

    public void drawShip(final Table scrollTable, Ships s, final Skin skin, final boolean isFleet) {
        drawShip(scrollTable, s, skin, isFleet, false, 1);
    }

    public void drawShip(final Table scrollTable, final Ships ship, final Skin skin, final boolean isFleet,
            final boolean showTactics, int numColumns) {
        boolean bUnknown = (!playerRace.getRaceId().equals(ship.getOwnerId()) && (playerRace.isRaceContacted(ship
                .getOwnerId()) == false))
                && !ship.getOwner().hasSpecialAbility(RaceSpecialAbilities.SPECIAL_NO_DIPLOMACY.getAbility());
        Button.ButtonStyle bs = new Button.ButtonStyle();
        Button button = new Button(bs);
        final Image tacticsImg = new Image();
        if (showTactics) {
            if (ship.getOwnerId().equals(playerRace.getRaceId()))
                tacticsImg.setDrawable(new TextureRegionDrawable(new TextureRegion(uiAtlas.findRegion(ship
                        .getCombatTactics().getImgName()))));
            button.add(tacticsImg).width((int) (shipHeight / 2)).height((int) (shipHeight / 2));
            tacticsImg.setName("tacticsImg");
        }
        String name = "Unknown";
        if (!bUnknown)
            name = ship.getShipImageName();
        String path = "graphics/ships/" + name + ".png";
        loadedTextures.add(path);
        TextureRegion tex = new TextureRegion(screenManager.loadTextureImmediate(path));
        Image shipImg = new Image(tex);

        Group container = new Group();
        shipImg.setSize(shipWidth, shipHeight);
        shipImg.setName(ship.getName());
        container.addActor(shipImg);
        if (ship.isFlagShip()) {
            tex = screenManager.getSymbolTextures(ship.getOwnerId());
            Image img = new Image(tex);
            container.addActor(img);
            float imgSize = GameConstants.hToRelative(25);
            img.setBounds(0, shipHeight - imgSize, imgSize, imgSize);
        }

        if (isFleet && ship.getFleetSize() > 0) {
            Label shipNum = new Label((ship.getFleetSize() + 1) + "", skin, "normalFont", Color.WHITE);
            container.addActor(shipNum);
            shipNum.setName(ship.getName());
            shipNum.setPosition(0, shipHeight - shipNum.getHeight());
        }
        String text = "";
        if (!bUnknown) {
            tex = null;
            if (ship.getTerraformingPlanet() != -1 && ship.getCurrentOrder() == ShipOrder.TERRAFORM) {
                StarSystem system = screenManager.getUniverseMap().getStarSystemAt(ship.getCoordinates());
                path = "graphics/planets/" + system.getPlanet(ship.getTerraformingPlanet()).getPlanetGraphicFile()
                        + ".png";
                loadedTextures.add(path);
                tex = new TextureRegion(screenManager.loadTextureImmediate(path));
            } else if (ship.getCurrentOrder() == ShipOrder.COLONIZE) {
                tex = uiAtlas.findRegion("populationSmall");
            } else if (ship.getCurrentOrder() == ShipOrder.BUILD_OUTPOST || ship.getCurrentOrder() == ShipOrder.UPGRADE_OUTPOST
                    || ship.getCurrentOrder() == ShipOrder.BUILD_STARBASE || ship.getCurrentOrder() == ShipOrder.UPGRADE_STARBASE) {
                Major owner = Major.toMajor(ship.getOwner());
                if(owner != null)
                    tex = buttonAtlas.findRegion(owner.getPrefix() + "Starbase");
            } else if (ship.getCurrentOrder() == ShipOrder.EXTRACT_DEUTERIUM) {
                tex = uiAtlas.findRegion("deuteriumSmall");
            }
            if (tex != null) {
                Image planetImg = new Image(tex);
                container.addActor(planetImg);
                float planetSize = GameConstants.hToRelative(20);
                planetImg.setBounds(shipWidth - planetSize, 0, planetSize, planetSize);
            }

            if (ship.hasTroops()) {
                tex = uiAtlas.findRegion("troopSmall");
                Image img = new Image(tex);
                container.addActor(img);
                float imgSize = GameConstants.hToRelative(20);
                img.setBounds(0, 0, imgSize, imgSize);
            }
            if (isFleet && ship.getFleetSize() > 0) {
                text = ship.getFleetName();
                if (ship.getFleetShipType() == -1)
                    text += "\n" + StringDB.getString("MIXED_FLEET");
                else
                    text += "\n" + ship.getShipTypeAsString();
            } else {
                if (!ship.isAlien()) {
                    text = ship.getName() + "\n" + ship.getShipClass();
                } else {
                    text = ship.getShipClass();
                }
            }

            if (ship.isCloakOn()) {
                shipImg.setColor(1, 1, 1, 120 / 255.0f);
                tex = uiAtlas.findRegion("cloakedSmall");
                Image img = new Image(tex);
                container.addActor(img);
                float imgSize = GameConstants.hToRelative(20);
                img.setBounds(shipWidth - imgSize, shipHeight - imgSize, imgSize, imgSize);
            }
        }
        Label classLabel = new Label(text, skin, "smallFont", Color.WHITE);
        classLabel.setName(ship.getName());

        Image experienceImage = new Image();
        switch (ship.getExpLevel()) {
            case 1:
                experienceImage.setDrawable(new TextureRegionDrawable(uiAtlas.findRegion("xp_beginner")));
                break;
            case 2:
                experienceImage.setDrawable(new TextureRegionDrawable(uiAtlas.findRegion("xp_normal")));
                break;
            case 3:
                experienceImage.setDrawable(new TextureRegionDrawable(uiAtlas.findRegion("xp_profi")));
                break;
            case 4:
                experienceImage.setDrawable(new TextureRegionDrawable(uiAtlas.findRegion("xp_veteran")));
                break;
            case 5:
                experienceImage.setDrawable(new TextureRegionDrawable(uiAtlas.findRegion("xp_elite")));
                break;
            case 6:
                experienceImage.setDrawable(new TextureRegionDrawable(uiAtlas.findRegion("xp_legend")));
                break;
        }
        float expImageHeight = experienceImage.getDrawable() == null ? shipHeight : shipHeight / (7 - ship.getExpLevel());

        button.add(experienceImage).size(8 * shipHeight / 48, expImageHeight).pad(0).space(0).center();
        experienceImage.setName(ship.getName());
        button.add(container).size(shipWidth, shipHeight).pad(0).space(0);
        container.setName(ship.getName());
        Table hullTable = ship.getHullRegion((int) (8 * shipHeight / 48), (int) (shipHeight));
        hullTable.setName(ship.getName());
        button.add(hullTable).size(8 * shipHeight / 48, shipHeight).pad(0).space(0);
        Table shieldTable = ship.getShieldRegion((int) (8 * shipHeight / 48), (int) (shipHeight));
        shieldTable.setName(ship.getName());
        button.add(shieldTable).size(8 * shipHeight / 48, shipHeight).pad(0).space(0);
        button.add(classLabel).width(shipWidth).pad(0).space(0);
        button.setUserObject(classLabel); //save label to change color when selected
        classLabel.setUserObject(ship);
        button.align(Align.top);
        button.setName(ship.getName());
        scrollTable.align(Align.top);
        button.setSize(shipWidth, shipHeight);
        scrollTable.add(button);
        colCnt++;
        if (numColumns == colCnt) {
            colCnt = 0;
            scrollTable.row();
        }
        shipImgs.add(new Pair<Button, Ships>(button, ship));
        ActorGestureListener gestureListener = new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                for (int i = 0; i < shipImgs.size; i++) {
                    Pair<Button, Ships> p = shipImgs.get(i);
                    Ships s = p.getSecond();
                    if (isFleet) {
                        if (event.getListenerActor().getName().equals(s.getName())) {
                            if (screenManager.getScreen() instanceof UniverseRenderer) {
                                if (selectedShip != s) {
                                    selectShip(s);
                                    drawSidebarInfo(s, skin);
                                } else {
                                    clearSelectedShip();
                                }
                                if (count >= 2) {
                                    if (selectedShip != s) {
                                        selectShip(s);
                                        drawSidebarInfo(s, skin);
                                    }
                                    if (s.getOwnerId().equals(screenManager.getRaceController().getPlayerRaceString())
                                            && !s.isStation()) {
                                        ShipMap map = screenManager.getUniverseMap().getShipMap();
                                        map.setFleetShip(map.currentShip());
                                        screenManager.setView(ViewTypes.FLEET_VIEW, true);
                                    }
                                }
                            } else if (screenManager.getScreen() instanceof FleetScreen) {
                                if (!selectedShip.isStation() && !selectedShip.getName().equals(s.getName())
                                        && s.getOwnerId().equals(playerRace.getRaceId()) && !s.isStation()) {
                                    selectShip(s);
                                    drawSidebarInfo(s, skin);
                                    ShipMap map = screenManager.getUniverseMap().getShipMap();
                                    Ships fleetShip = map.getAt(map.fleetShip());
                                    fleetShip.addShipToFleet(s);
                                    map.eraseAt(map.currentShip());
                                    selectShip(fleetShip);
                                    map.setFleetShip(map.currentShip());
                                    screenManager.getScreen().show();
                                }
                            }
                        }
                    } else if (screenManager.getScreen() instanceof CombatScreen) {
                        if (s.getOwnerId().equals(playerRace.getRaceId())
                                && event.getListenerActor().getName().equals(s.getName())) {
                            selectShipInternal(s);
                            CombatScreen scr = (CombatScreen) screenManager.getScreen();
                            scr.setSelectedShip(s);
                            s.setCombatTactics(scr.getSelectedTactics(), false);
                            tacticsImg.setDrawable(new TextureRegionDrawable(new TextureRegion(uiAtlas.findRegion(s
                                    .getCombatTactics().getImgName()))));
                        }
                    }
                }
                if (!isFleet && shipImgs.size > 1 && screenManager.getScreen() instanceof FleetScreen) {
                    Ships ships = (Ships) ((Label) event.getListenerActor().getUserObject()).getUserObject();
                    ShipMap map = screenManager.getUniverseMap().getShipMap();
                    Ships fleetShip = map.getAt(map.fleetShip());
                    if (ships != fleetShip) {
                        int idx = fleetShip.getFleet().getShipMap().indexOfValue(ships, true);
                        fleetShip.removeShipFromFleet(idx);
                        map.add(ships);
                        selectShip(fleetShip);
                    } else {
                        Ships newFleetShip = fleetShip.giveFleetToFleetsFirstShip();
                        map.add(newFleetShip);
                        selectShip(newFleetShip);
                    }
                    map.setFleetShip(map.currentShip());
                    screenManager.getScreen().show();
                }
                if (count == 2) {
                    if (screenManager.getScreen() instanceof CombatScreen)
                        for (int i = 0; i < shipImgs.size; i++) {
                            Pair<Button, Ships> p = shipImgs.get(i);
                            Ships s = p.getSecond();
                            Button b = p.getFirst();
                            if (s.getOwnerId().equals(playerRace.getRaceId())
                                    && s.getShipClass().equals(ship.getShipClass())) {
                                CombatScreen scr = (CombatScreen) screenManager.getScreen();
                                s.setCombatTactics(scr.getSelectedTactics(), false);
                                for (int j = 0; j < b.getChildren().size; j++)
                                    if (b.getChildren().get(j).getName().equals("tacticsImg"))
                                        ((Image) b.getChildren().get(j))
                                                .setDrawable(new TextureRegionDrawable(new TextureRegion(uiAtlas
                                                        .findRegion(s.getCombatTactics().getImgName()))));
                            }
                        }
                }
            }
        };
        button.addListener(gestureListener);
        Texture tooltipTexture = screenManager.getAssetManager().get(GameConstants.UI_BG_ROUNDED);
        generateShipTooltip(ship, isFleet, BaseTooltip.createTableTooltip(button, tooltipTexture).getActor(), skin);
    }

    private void generateShipTooltip(Ships ship, boolean isFleet, Table table, Skin skin) {
        table.clearChildren();
        String text;
        if(!playerRace.getRaceId().equals(ship.getOwnerId())) {
            boolean noDiplomacy = false;
            Race shipOwner = ship.getOwner();
            if(shipOwner != null)
                noDiplomacy = shipOwner.hasSpecialAbility(RaceSpecialAbilities.SPECIAL_NO_DIPLOMACY.getAbility());

            if(!noDiplomacy && !playerRace.isRaceContacted(ship.getOwnerId())) {
                text = StringDB.getString("UNKNOWN");
                Label l = new Label(text, skin, headerFont, headerColor);
                table.add(l);
                return;
            }
        }

        if(isFleet)
            ship.getTooltip(table, skin, headerFont, headerColor, textFont, textColor);
        else
            ship.getTooltip(null, table, skin, headerFont, headerColor, textFont, textColor);
    }

    public void drawSidebarInfo(Ships s, Skin skin) {
        drawSidebarInfo(s, skin, false);
    }

    public void drawSidebarInfo(Ships s, Skin skin, boolean shipMove) {
        boolean unknown = (!playerRace.getRaceId().equals(s.getOwnerId()) && (playerRace
                .isRaceContacted(s.getOwnerId()) == false))
                && !s.getOwner().hasSpecialAbility(RaceSpecialAbilities.SPECIAL_NO_DIPLOMACY.getAbility());
        String name = "Unknown";
        if (!unknown)
            name = s.getShipImageName();
        String path = "graphics/ships/" + name + ".png";
        Texture tex = screenManager.loadTextureImmediate(path);
        Sprite sp = new Sprite(tex);
        sp.setColor(new Color(0.4f, 0.4f, 0.4f, 0.9f));
        Table table = screenManager.getSidebarLeft().getInfoTable();
        table.clear();
        screenManager.getSidebarLeft().getInfoImage().setDrawable(new SpriteDrawable(sp));
        if (!unknown) {
            String text = String.format("%s (%s: %d)", s.getName(), StringDB.getString("EXPERIANCE_SHORT"),
                    s.getCrewExperience());
            Label l = new Label(text, skin, "mediumFont", playerRace.getRaceDesign().clrListMarkTextColor);
            l.setAlignment(Align.top);
            table.add(l);
            table.row();
            if ((s.hasFleet() && s.getFleetShipType() != -1) || !s.hasFleet())
                text = String.format("%s: %s", StringDB.getString("TYPE"), s.getShipTypeAsString());
            else
                text = StringDB.getString("MIXED_FLEET");
            l = new Label(text, skin, "mediumFont", playerRace.getRaceDesign().clrNormalText);
            l.setAlignment(Align.top);
            table.add(l);
            if (!s.hasFleet()) {
                table.row();
                text = String.format("%s: %d / %d", StringDB.getString("HULL"), s.getHull().getCurrentHull(), s
                        .getHull().getMaxHull());
                l = new Label(text, skin, "mediumFont", playerRace.getRaceDesign().clrNormalText);
                l.setAlignment(Align.top);
                table.add(l);
                table.row();
                text = String.format("%s: %d / %d", StringDB.getString("SHIELDS"), s.getShield().getCurrentShield(), s
                        .getShield().getMaxShield());
                l = new Label(text, skin, "mediumFont", playerRace.getRaceDesign().clrNormalText);
                l.setAlignment(Align.top);
                table.add(l);
            }
            table.row();
            text = String.format("%s: %s", StringDB.getString("RANGE"), StringDB.getString(s.getRange(true).getName()));
            l = new Label(text, skin, "mediumFont", playerRace.getRaceDesign().clrNormalText);
            l.setAlignment(Align.top);
            table.add(l);
            table.row();
            text = String.format("%s: %d", StringDB.getString("SPEED"), s.getSpeed(true));
            l = new Label(text, skin, "mediumFont", playerRace.getRaceDesign().clrNormalText);
            l.setAlignment(Align.top);
            table.add(l);
            table.row();
            text = StringDB.getString("TARGET") + ": ";
            if (playerRace.getRaceId().equals(s.getOwnerId())
                    || playerRace.getAgreement(s.getOwnerId()).getType() >= DiplomaticAgreement.ALLIANCE.getType()) {
                if (s.hasTarget()) {
                    int range = 3 - s.getRange(true).getRange();
                    int speed = s.getSpeed(true);
                    IntPoint position = new IntPoint(s.getCoordinates());
                    IntPoint target = new IntPoint(s.getTargetCoord());
                    Pair<IntPoint, Array<IntPoint>> pair = playerRace.getStarMap().calcPath(position, target, range,
                            speed);
                    int rounds = 0;
                    if (speed > 0)
                        rounds = (int) Math.round(Math.ceil((float) pair.getSecond().size / (float) speed));
                    text += String.format("%s (%d %s)", s.getCurrentTargetAsString(playerRace), rounds,
                            rounds > 1 ? StringDB.getString("ROUNDS") : StringDB.getString("ROUND"));
                } else
                    text += s.getCurrentTargetAsString(playerRace);
            } else
                text += s.getCurrentTargetAsString(playerRace);
            Color color = playerRace.getRaceDesign().clrNormalText;
            if (shipMove) {
                color = Color.GREEN;
                text = String.format("--- %s ---", StringDB.getString("SET_MOVEMENT_TARGET").toUpperCase());
            }
            l = new Label(text, skin, "mediumFont", color);
            l.setAlignment(Align.top);
            table.add(l);
            if (s.getOwnerId().equals(playerRace.getRaceId())) {
                table.row();
                text = String.format("%s: %s", StringDB.getString("COMBAT_BEHAVIOR"), s.getCurrentTacticsAsString());
                l = new Label(text, skin, "mediumFont", playerRace.getRaceDesign().clrSecondText);
                l.setAlignment(Align.top);
                table.add(l);
                table.row();
                if (s.getCurrentOrder() == ShipOrder.TERRAFORM)
                    text = String.format(
                            "%s: %s\n%s",
                            StringDB.getString("ORDER"),
                            s.getCurrentOrderAsString(),
                            screenManager.getUniverseMap().getStarSystemAt(s.getCoordinates())
                                    .getPlanet(s.getTerraformingPlanet()).getPlanetName());
                else
                    text = String.format("%s: %s", StringDB.getString("ORDER"), s.getCurrentOrderAsString());
                l = new Label(text, skin, "mediumFont", playerRace.getRaceDesign().clrSecondText);
                l.setAlignment(Align.top);
                table.add(l);
            }
        }
        screenManager.getSidebarLeft().showInfo(false);
    }

    public void hideShips() {
        hideShips(true);
    }

    /**
     * @param clearTextures - when true, the textures loaded by the renderer will be unloaded.
     * This should be false in case of the FleetScreen's renderer as that would break textures for the other parts of the game
     * Also it should be false when ships are moved 
     */
    public void hideShips(boolean clearTextures) {
        scrollTable.clear();
        shipScroller.remove();

        shipScroller.setVisible(false);
        for (Pair<Button, Ships> p : shipImgs)
            p.getFirst().remove();
        shipImgs.clear();
        if (nextButton != null) {
            nextButton.remove();
            nextButton.setVisible(false);
        }
        if (prevButton != null) {
            prevButton.setVisible(false);
            prevButton.remove();
        }
        hideCommandButtons();
        if (clearTextures) {
            for (String path : loadedTextures)
                if (screenManager.getAssetManager().isLoaded(path))
                    screenManager.getAssetManager().unload(path);
            loadedTextures.clear();
        }
    }

    private boolean canHaveOrder(Ships ships, ShipOrder order) {
        return canHaveOrder(ships, order, null);
    }

    private boolean canHaveOrder(Ships ships, ShipOrder order, StarSystem system) {
        switch (order) {
            case REPAIR:
                return ships.canHaveOrder(ShipOrder.REPAIR, true) && system.getShipPort(ships.getOwnerId());
            case TRANSPORT:
                return ships.canHaveOrder(order, true, false);
            case TERRAFORM:
                return ships.canHaveOrder(order, false);
            case IMPROVE_SHIELDS:
                return ships.canHaveOrder(order, true) && (system.getAnomaly() != null)
                        && (system.getAnomaly().getType() == AnomalyType.IONSTORM);
            case EXTRACT_DEUTERIUM:
                return ships.canHaveOrder(order, true) && (system.getAnomaly() != null)
                        && (system.getAnomaly().getType() == AnomalyType.DEUTNEBULA);
            case EXPLORE_WORMHOLE: //TODO: check also if techlevel is high enough
                return ships.canHaveOrder(order, true) && (system.getAnomaly() != null)
                        && (system.getAnomaly().getType() == AnomalyType.WORMHOLE);
            default:
                return ships.canHaveOrder(order, true);
        }
    }

    private void enableSecondaryOrderButton(TextButton button, ShipOrder order, String text) {
        button.setVisible(true);
        button.setUserObject(order);
        button.setText(StringDB.getString(text));
    }

    private void drawCombatMenu() {
        int counter = 0;
        if (canHaveOrder(selectedShip, ShipOrder.ATTACK)) {
            enableSecondaryOrderButton(secondaryShipOrders.get(nextNearestButton(counter)), ShipOrder.ATTACK,
                    "BTN_ATTACK");
            counter++;
        } else if (canHaveOrder(selectedShip, ShipOrder.AVOID)) {
            enableSecondaryOrderButton(secondaryShipOrders.get(nextNearestButton(counter)), ShipOrder.AVOID,
                    "BTN_AVOID");
            counter++;
        }
    }

    private void drawImmediateOrderMenu() {
        int counter = 0;
        if (canHaveOrder(selectedShip, ShipOrder.CREATE_FLEET)) {
            enableSecondaryOrderButton(secondaryShipOrders.get(nextNearestButton(counter)), ShipOrder.CREATE_FLEET,
                    "BTN_CREATE_FLEET");
            counter++;
        }
        if (canHaveOrder(selectedShip, ShipOrder.TRANSPORT)) {
            enableSecondaryOrderButton(secondaryShipOrders.get(nextNearestButton(counter)), ShipOrder.TRANSPORT,
                    "BTN_TRANSPORT");
            counter++;
        }
        if (canHaveOrder(selectedShip, ShipOrder.ASSIGN_FLAGSHIP)) {
            enableSecondaryOrderButton(secondaryShipOrders.get(nextNearestButton(counter)), ShipOrder.ASSIGN_FLAGSHIP,
                    "BTN_ASSIGN_FLAGSHIP");
            counter++;
        }
    }

    private void drawSingleTurnOrderMenu() {
        int counter = 0;
        if (canHaveOrder(selectedShip, ShipOrder.WAIT_SHIP_ORDER)) {
            enableSecondaryOrderButton(secondaryShipOrders.get(nextNearestButton(counter)), ShipOrder.WAIT_SHIP_ORDER,
                    "BTN_WAIT_SHIP_ORDER");
            counter++;
        }
        if (canHaveOrder(selectedShip, ShipOrder.DESTROY_SHIP)) {
            enableSecondaryOrderButton(secondaryShipOrders.get(nextNearestButton(counter)), ShipOrder.DESTROY_SHIP,
                    "BTN_DESTROY_SHIP");
            counter++;
        }
        if (canHaveOrder(selectedShip, ShipOrder.ENCLOAK)) {
            enableSecondaryOrderButton(secondaryShipOrders.get(nextNearestButton(counter)), ShipOrder.ENCLOAK,
                    "BTN_CLOAK");
            counter++;
        }
        if (canHaveOrder(selectedShip, ShipOrder.DECLOAK)) {
            enableSecondaryOrderButton(secondaryShipOrders.get(nextNearestButton(counter)), ShipOrder.DECLOAK,
                    "BTN_DECLOAK");
            counter++;
        }
        StarSystem system = screenManager.getUniverseMap().getStarSystemAt(selectedShip.getCoordinates());
        if (canHaveOrder(selectedShip, ShipOrder.COLONIZE)) {
            if (system.isColonizable(selectedShip.getOwnerId())) {
                enableSecondaryOrderButton(secondaryShipOrders.get(nextNearestButton(counter)), ShipOrder.COLONIZE,
                        "BTN_COLONIZE");
                counter++;
            }
        }
        if (canHaveOrder(selectedShip, ShipOrder.EXPLORE_WORMHOLE, system)) {
            enableSecondaryOrderButton(secondaryShipOrders.get(nextNearestButton(counter)), ShipOrder.EXPLORE_WORMHOLE,
                    "BTN_EXPLORE_WORMHOLE");
            counter++;
        }
    }

    private void drawMultiTurnOrderMenu() {
        StarSystem system = screenManager.getUniverseMap().getStarSystemAt(selectedShip.getCoordinates());
        Research research = playerRace.getEmpire().getResearch();
        int[] researchLevels = research.getResearchLevels();
        //maximum orders in this category:
        //5; in an own system with training, a terraformable planet, a damaged ship
        int counter = 0;
        boolean topDown = false;
        if (canHaveOrder(selectedShip, ShipOrder.SENTRY_SHIP_ORDER)) {
            enableSecondaryOrderButton(secondaryShipOrders.get(nextNearestButton(counter, topDown)),
                    ShipOrder.SENTRY_SHIP_ORDER, "BTN_SENTRY_SHIP_ORDER");
            counter++;
        }
        if (canHaveOrder(selectedShip, ShipOrder.ATTACK_SYSTEM)) {
            if (system.isSunSystem() && system.getCurrentInhabitants() > 0.0f
                    && !system.getOwnerId().equals(selectedShip.getOwnerId())) {
                if (system.getOwner() != null
                        && (playerRace.getAgreement(system.getOwnerId()) == DiplomaticAgreement.WAR) //if we are at war
                        || (!system.isMajorized() && system.getMinorRace() == null)) { //in case of a rebellinon
                    enableSecondaryOrderButton(secondaryShipOrders.get(nextNearestButton(counter, topDown)),
                            ShipOrder.ATTACK_SYSTEM, "BTN_ATTACK_SYSTEM");
                    counter++;
                }
            }
        }
        if (canHaveOrder(selectedShip, ShipOrder.BLOCKADE_SYSTEM)) {
            if (system.getOwner() != null
                    && !system.getOwnerId().equals(selectedShip.getOwnerId())
                    && playerRace.getAgreement(system.getOwnerId()).getType() < DiplomaticAgreement.FRIENDSHIP
                            .getType()) {
                enableSecondaryOrderButton(secondaryShipOrders.get(nextNearestButton(counter, topDown)),
                        ShipOrder.BLOCKADE_SYSTEM, "BTN_BLOCKADE_SYSTEM");
                counter++;
            }
        }
        if (canHaveOrder(selectedShip, ShipOrder.TERRAFORM)) {
            boolean doEnable = false;
            for (int l = 0; l < system.getNumberOfPlanets(); l++) {
                if (system.getPlanet(l).getIsHabitable() && !system.getPlanet(l).getIsTerraformed()) {
                    doEnable = true;
                    break;
                }
            }
            if (doEnable) {
                enableSecondaryOrderButton(secondaryShipOrders.get(nextNearestButton(counter, topDown)),
                        ShipOrder.TERRAFORM, "BTN_TERRAFORM");
                counter++;
            }
        }
        if (canHaveOrder(selectedShip, ShipOrder.BUILD_OUTPOST)) {
            if (system.isStationBuildable(ShipOrder.BUILD_OUTPOST, selectedShip.getOwnerId())) {
                boolean doEnable = false;
                for (int l = 0; l < screenManager.getShipInfos().size; l++)
                    if (playerRace.canBuildShip(ShipType.OUTPOST, researchLevels, screenManager.getShipInfos().get(l))) {
                        doEnable = true;
                        break;
                    }
                if (doEnable) {
                    enableSecondaryOrderButton(secondaryShipOrders.get(nextNearestButton(counter, topDown)),
                            ShipOrder.BUILD_OUTPOST, "BTN_BUILD_OUTPOST");
                    counter++;
                }
            }
            if (system.isStationBuildable(ShipOrder.BUILD_STARBASE, selectedShip.getOwnerId())) {
                boolean doEnable = false;
                for (int l = 0; l < screenManager.getShipInfos().size; l++)
                    if (playerRace.canBuildShip(ShipType.STARBASE, researchLevels, screenManager.getShipInfos().get(l))) {
                        doEnable = true;
                        break;
                    }
                if (doEnable) {
                    enableSecondaryOrderButton(secondaryShipOrders.get(nextNearestButton(counter, topDown)),
                            ShipOrder.BUILD_STARBASE, "BTN_BUILD_STARBASE");
                    counter++;
                }
            }
            // If a better outpost is available and buildable in this sector draw the upgrade button
            if (system.isStationBuildable(ShipOrder.UPGRADE_OUTPOST, selectedShip.getOwnerId())) {
                enableSecondaryOrderButton(secondaryShipOrders.get(nextNearestButton(counter, topDown)),
                        ShipOrder.UPGRADE_OUTPOST, "BTN_UPGRADE_OUTPOST");
                counter++;
            }
            // If a better starbase is available and buildable in this sector draw the upgrade button
            if (system.isStationBuildable(ShipOrder.UPGRADE_STARBASE, selectedShip.getOwnerId())) {
                enableSecondaryOrderButton(secondaryShipOrders.get(nextNearestButton(counter, topDown)),
                        ShipOrder.UPGRADE_STARBASE, "BTN_UPGRADE_STARBASE");
                counter++;
            }
        }
        // shield improvement
        // Only possible if we are at an ionstorm and we haven't yet reached the max max shields
        if (canHaveOrder(selectedShip, ShipOrder.IMPROVE_SHIELDS, system)) {
            enableSecondaryOrderButton(secondaryShipOrders.get(nextNearestButton(counter, topDown)),
                    ShipOrder.IMPROVE_SHIELDS, "IMPROVE_SHIELDS_SHIP_ORDER");
            counter++;
        }
        //deuterium extraction
        if (canHaveOrder(selectedShip, ShipOrder.EXTRACT_DEUTERIUM, system)) {
            enableSecondaryOrderButton(secondaryShipOrders.get(nextNearestButton(counter, topDown)),
                    ShipOrder.EXTRACT_DEUTERIUM, "EXTRACT_DEUTERIUM_SHIP_ORDER");
            counter++;
        }
        // Repairing
        // Only possible if
        // 1) the ship or any of the ships in its fleet are actually damaged.
        // 2) we (or an allied race) have a ship port in this sector.
        if (canHaveOrder(selectedShip, ShipOrder.REPAIR, system)) {
            enableSecondaryOrderButton(secondaryShipOrders.get(nextNearestButton(counter, topDown)), ShipOrder.REPAIR,
                    "BTN_REPAIR_SHIP");
            counter++;
        }
        if (canHaveOrder(selectedShip, ShipOrder.AUTO_EXPLORE, system)) {
            enableSecondaryOrderButton(secondaryShipOrders.get(nextNearestButton(counter, topDown)), ShipOrder.AUTO_EXPLORE,
                    ShipOrder.AUTO_EXPLORE.getName());
            counter++;
        }
    }

    private int nextNearestButton(int counter) {
        return nextNearestButton(counter, true);
    }

    private int nextNearestButton(int counter, boolean fromTop) {
        return fromTop ? counter : secondaryShipOrders.size - 1 - counter;
    }

    private void showCorrespondingSecondaryButtons(TextButton button) {
        MainButton buttonType = (MainButton) button.getUserObject();
        for (TextButton b : secondaryShipOrders)
            b.setVisible(false);
        switch (buttonType) {
            case MAIN_BUTTON_COMBAT_BEHAVIOR:
                drawCombatMenu();
                break;
            case MAIN_BUTTON_IMMEDIATE_ORDER:
                drawImmediateOrderMenu();
                break;
            case MAIN_BUTTON_SINGLE_TURN_ORDER:
                drawSingleTurnOrderMenu();
                break;
            case MAIN_BUTTON_MULTI_TURN_ORDER:
                drawMultiTurnOrderMenu();
                break;
            default:
                break;
        }
    }

    private void drawCommandButtons() {
        for (TextButton button : mainShipOrders)
            button.setVisible(true);
    }

    private void hideCommandButtons() {
        for (TextButton button : mainShipOrders)
            button.setVisible(false);
        for (TextButton button : secondaryShipOrders)
            button.setVisible(false);
    }

    public void selectShip(String name) {
        for (int i = 0; i < screenManager.getUniverseMap().getShipMap().getSize(); i++) {
            Ships ship = screenManager.getUniverseMap().getShipMap().getAt(i);
            if (ship.getName().equals(name)) {
                selectShip(ship);
                return;
            }
            for (int j = 0; j < ship.getFleetSize(); j++) {
                Ships fleetship = ship.getFleet().getAt(j);
                if (fleetship.getName().equals(name)) {
                    selectShip(ship);
                    return;
                }
            }
        }
    }

    public void selectShip(Ships s) {
        if (screenManager.getScreen() instanceof UniverseRenderer && selectedShip != null
                && !s.getName().equals(selectedShip.getName())) {
            UniverseRenderer ur = (UniverseRenderer) screenManager.getScreen();
            ur.cancelShipMove();
            if (s.getTargetCoord().equals(new IntPoint()))
                s.getPath().clear();
        }

        selectedShip = s;
        screenManager.getUniverseMap().setCurrentShip(selectedShip);
        if (oldSelection != null) {
            oldSelection.getStyle().up = null;
            oldSelection.getStyle().down = null;
        }
        Image shipSelection = new Image(shipSelectTexture);
        Button b = null;
        int idx = 0;
        for (Pair<Button, Ships> entry : shipImgs) {
            if (entry.getSecond().getName().equals(s.getName())) {
                b = entry.getFirst();
                idx = shipImgs.indexOf(entry, true);
                break;
            }
        }
        if (b != null) {
            b.getStyle().up = shipSelection.getDrawable();
            b.getStyle().down = shipSelection.getDrawable();
            if (screenManager.getScreen() instanceof UniverseRenderer && s.getOwnerId().equals(playerRace.getRaceId())) {
                UniverseRenderer ur = (UniverseRenderer) screenManager.getScreen();
                if (s.hasTarget())
                    ur.drawPath(s, s.getPath());
                ur.drawRangeLines(s.getRange().getRange());
                hideCommandButtons();
                drawCommandButtons();
            }
            if (selectLabel != null)
                selectLabel.setColor(Color.WHITE);
            selectLabel = ((Label) b.getUserObject());
            selectLabel.setColor(playerRace.getRaceDesign().clrListMarkTextColor);
            oldSelection = b;
            if (idx != -1) {
                currentPage = idx / shipsInPage;
                shipScroller.setScrollY(shipHeight * shipsInPage * currentPage);
                showNextPrevButtonsIfNeeded();
            }
        }
    }

    public void selectShipInternal(Ships s) {
        selectedShip = s;
        if (oldSelection != null) {
            oldSelection.getStyle().up = null;
            oldSelection.getStyle().down = null;
        }

        Image shipSelection = new Image(shipSelectTexture);
        Button b = null;
        for (Pair<Button, Ships> entry : shipImgs) {
            if (entry.getSecond().getName().equals(s.getName())) {
                b = entry.getFirst();
                break;
            }
        }
        if (b != null) {
            b.getStyle().up = shipSelection.getDrawable();
            b.getStyle().down = shipSelection.getDrawable();
            if (selectLabel != null)
                selectLabel.setColor(Color.WHITE);
            selectLabel = ((Label) b.getUserObject());
            selectLabel.setColor(playerRace.getRaceDesign().clrListMarkTextColor);
            oldSelection = b;
        }
    }

    public void clearSelectedShip() {
        if (oldSelection != null) {
            oldSelection.getStyle().up = null;
            oldSelection.getStyle().down = null;
            selectLabel.setColor(Color.WHITE);
            oldSelection = null;
            if(selectedShip.getTargetCoord().equals(new IntPoint()))
                selectedShip.getPath().clear();
            selectedShip = null;
            hideCommandButtons();
            if (screenManager.getScreen() instanceof UniverseRenderer) {
                UniverseRenderer ur = (UniverseRenderer) screenManager.getScreen();
                ur.showPathImages();
                ur.drawRangeLines(-1);
                hideCommandButtons();
            }
        }
    }

    public boolean isShipMove() {
        return (selectedShip != null);
    }

    public Ships getSelectedShip() {
        return selectedShip;
    }

    @Override
    public void scrollEventY(float pixelsY) {
        currentPage = (int) Math.ceil(pixelsY / (shipHeight * shipsInPage));
        showNextPrevButtonsIfNeeded();
    }

    private void showNextPrevButtonsIfNeeded() {
        if (currentPage == pageCnt) {
            nextButton.setVisible(false);
        } else {
            nextButton.setVisible(true);
        }
        if (currentPage == 0) {
            prevButton.setVisible(false);
        } else {
            prevButton.setVisible(true);
        }
    }

    /**
     * Function to be called only from the Combat screen!!!
     */
    public void setAllTactics() {
        if (screenManager.getScreen() instanceof CombatScreen)
            for (int i = 0; i < shipImgs.size; i++) {
                Pair<Button, Ships> p = shipImgs.get(i);
                Ships s = p.getSecond();
                Button b = p.getFirst();
                if (s.getOwnerId().equals(playerRace.getRaceId())) {
                    CombatScreen scr = (CombatScreen) screenManager.getScreen();
                    s.setCombatTactics(scr.getSelectedTactics(), false);
                    for (int j = 0; j < b.getChildren().size; j++)
                        if (b.getChildren().get(j).getName().equals("tacticsImg"))
                            ((Image) b.getChildren().get(j)).setDrawable(new TextureRegionDrawable(new TextureRegion(
                                    uiAtlas.findRegion(s.getCombatTactics().getImgName()))));
                }
            }
    }

    @Override
    public void dispose() {
        loadedTextures.clear();
    }
}
