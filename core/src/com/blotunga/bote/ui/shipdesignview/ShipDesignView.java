/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ui.shipdesignview;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.InputEvent.Type;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectSet;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.ResourceTypes;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.races.Research;
import com.blotunga.bote.races.WeaponObserver.TubeWeaponsObserverStruct;
import com.blotunga.bote.ships.BeamWeapons;
import com.blotunga.bote.ships.Hull;
import com.blotunga.bote.ships.Shield;
import com.blotunga.bote.ships.Ship;
import com.blotunga.bote.ships.ShipInfo;
import com.blotunga.bote.ships.TorpedoInfo;
import com.blotunga.bote.ships.TorpedoWeapons;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.ui.OverlayBanner;
import com.blotunga.bote.utils.ui.BaseTooltip;

public class ShipDesignView {
    private ScreenManager manager;
    private Stage stage;
    private Skin skin;
    private Major playerRace;
    private Color normalColor;
    private Color markColor;
    private Color oldColor;
    private int selectedShipInfo;
    private Table nameTable;
    private TextureRegion selectTexture;
    private Table shipListScrollTable;
    private ScrollPane shipListScroller;
    private Table infoTable;
    private ScrollPane infoScroller;
    private Button shipSelection = null;
    private Array<Button> shipItems;
    private Table infoHeader;
    private ObjectSet<String> loadedTextures;
    private Table beamWeaponsTable;
    private Table torpedoWeaponsTable;
    private int beamWeaponNumber;
    private int torpedoWeaponNumber;
    private boolean isShipBuilding;
    private Table hullTable;
    private Table shieldTable;
    private Array<OverlayBanner> banners;
    private float xOffset;
    private float yOffset;
    //tooltip constants
    private Color headerColor;
    private Color textColor;
    private String headerFont = "xlFont";
    private String textFont = "largeFont";
    private Texture tooltipTexture;
    private boolean visible;

    public ShipDesignView(ScreenManager manager, Stage stage, Skin skin, float xOffset, float yOffset) {
        this.manager = manager;
        this.stage = stage;
        this.skin = skin;
        this.xOffset = xOffset;
        this.yOffset = yOffset;
        playerRace = manager.getRaceController().getPlayerRace();
        normalColor = playerRace.getRaceDesign().clrNormalText;
        markColor = playerRace.getRaceDesign().clrListMarkTextColor;
        headerColor = markColor;
        textColor = normalColor;

        nameTable = new Table();
        Rectangle rect = GameConstants.coordsToRelative(340, 810, 400, 60);
        nameTable.setBounds((int) (rect.x + xOffset), (int) rect.y, (int) rect.width, (int) rect.height);
        nameTable.align(Align.center);
        nameTable.setSkin(skin);
        stage.addActor(nameTable);
        nameTable.add(StringDB.getString("SHIPDESIGN"), "hugeFont", playerRace.getRaceDesign().clrNormalText);
        nameTable.setVisible(false);

        selectTexture = manager.getUiTexture("listselect");

        shipListScrollTable = new Table();
        shipListScrollTable.align(Align.top);
        shipListScroller = new ScrollPane(shipListScrollTable, skin);
        shipListScroller.setVariableSizeKnobs(false);
        shipListScroller.setFadeScrollBars(false);
        shipListScroller.setScrollingDisabled(true, false);
        stage.addActor(shipListScroller);
        rect = GameConstants.coordsToRelative(25, 530, 194, 490);
        shipListScroller.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width,
                (int) rect.height);

        selectedShipInfo = -1;
        shipItems = new Array<Button>();

        rect = GameConstants.coordsToRelative(328, 390, 430, 360);
        infoTable = new Table();
        infoTable.align(Align.top);
        infoScroller = new ScrollPane(infoTable, skin);
        infoScroller.setVariableSizeKnobs(false);
        infoScroller.setFadeScrollBars(false);
        infoScroller.setScrollingDisabled(true, false);
        infoScroller.setTouchable(Touchable.childrenOnly);
        infoScroller.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        stage.addActor(infoScroller);

        infoHeader = new Table();
        rect = GameConstants.coordsToRelative(420, 545, 245, 155);
        infoHeader.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        infoHeader.align(Align.top);
        infoHeader.setSkin(skin);
        stage.addActor(infoHeader);
        infoHeader.setVisible(false);
        loadedTextures = new ObjectSet<String>();

        beamWeaponsTable = new Table();
        rect = GameConstants.coordsToRelative(889, 560, 275, 75);
        beamWeaponsTable.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width,
                (int) rect.height);
        beamWeaponsTable.align(Align.top);
        beamWeaponsTable.setSkin(skin);
        stage.addActor(beamWeaponsTable);
        beamWeaponsTable.setVisible(false);

        torpedoWeaponsTable = new Table();
        rect = GameConstants.coordsToRelative(889, 483, 275, 90);
        torpedoWeaponsTable.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width,
                (int) rect.height);
        torpedoWeaponsTable.align(Align.top);
        torpedoWeaponsTable.setSkin(skin);
        stage.addActor(torpedoWeaponsTable);
        torpedoWeaponsTable.setVisible(false);

        hullTable = new Table();
        rect = GameConstants.coordsToRelative(889, 302, 275, 60);
        hullTable.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        hullTable.align(Align.top);
        hullTable.setSkin(skin);
        stage.addActor(hullTable);
        hullTable.setVisible(false);

        shieldTable = new Table();
        rect = GameConstants.coordsToRelative(889, 214, 275, 60);
        shieldTable.setBounds((int) (rect.x + xOffset), (int) (rect.y + yOffset), (int) rect.width, (int) rect.height);
        shieldTable.align(Align.top);
        shieldTable.setSkin(skin);
        stage.addActor(shieldTable);
        shieldTable.setVisible(false);

        beamWeaponNumber = 0;
        torpedoWeaponNumber = 0;
        isShipBuilding = false;
        banners = new Array<OverlayBanner>();
        tooltipTexture = manager.getAssetManager().get(GameConstants.UI_BG_ROUNDED);
        visible = false;
    }

    public int show() {
        visible = true;
        for (OverlayBanner banner : banners)
            banner.remove();
        banners.clear();

        nameTable.setVisible(true);
        drawShipsList();
        float vpadding = GameConstants.hToRelative(6);
        if (selectedShipInfo != -1) {
            ShipInfo si = manager.getShipInfos().get(selectedShipInfo);
            Ship ship = new Ship(si);
            ship.addSpecialResearchBoni(playerRace);
            si = new ShipInfo(ship, si);
            infoScroller.setVisible(false);
            infoTable.clear();
            int width = (int) (infoScroller.getWidth() - infoScroller.getStyle().vScrollKnob.getMinWidth());
            si.drawShipInfo(infoTable, skin, markColor, normalColor, width, 0, 1);
            if (infoScroller.getScrollY() > 0) {
                infoScroller.setScrollY(0);
            }
            infoScroller.setVisible(true);
            infoHeader.clear();
            String path = "graphics/ships/" + si.getShipImageName() + ".png";
            loadedTextures.add(path);
            TextureRegion tex = new TextureRegion(manager.loadTextureImmediate(path));
            Image infoImage = new Image(new TextureRegionDrawable(tex));
            ship.getTooltip(null, BaseTooltip.createTableTooltip(infoImage, tooltipTexture).getActor(), skin, headerFont, headerColor, textFont, textColor);
            infoHeader.add(infoImage).height(GameConstants.hToRelative(123));
            Label label = new Label(si.getShipTypeAsString(), skin, "normalFont", markColor);
            label.setAlignment(Align.center);
            infoHeader.row();
            infoHeader.add(label).spaceTop(vpadding);
            infoHeader.setVisible(true);
            drawWeaponsTable(si);
            drawHullAndShieldTables(si);
        }
        return selectedShipInfo;
    }

    private void drawHullAndShieldTables(ShipInfo si) {
        hullTable.clear();
        float labelHeight = GameConstants.hToRelative(20);
        float buttonHPad = GameConstants.wToRelative(10);
        float buttonHeight = GameConstants.hToRelative(30);
        float buttonWidth = hullTable.getWidth() / 2 - buttonHPad;
        float buttonVPad = GameConstants.hToRelative(15);
        TextButtonStyle smallButtonStyle = new TextButtonStyle(skin.get("small-buttons", TextButtonStyle.class));
        String material = StringDB.getString(ResourceTypes.fromResourceTypes(si.getHull().getHullMaterial()).getName());
        String text;
        boolean doubleHull = si.getHull().isDoubleHull();
        text = String.format("%s %s", material,
                doubleHull ? StringDB.getString("DOUBLE_HULL_ARMOUR") : StringDB.getString("HULL_ARMOR"));
        Label label = new Label(text, skin, "normalFont", normalColor);
        label.setAlignment(Align.center, Align.top);
        hullTable.add(label).height((int) labelHeight);
        hullTable.row();
        ButtonStyle bs = new ButtonStyle();
        Button button = new Button(bs);
        TextButton materialBtn = new TextButton(StringDB.getString("BTN_MATERIAL"), smallButtonStyle);
        materialBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (!isShipBuilding) {
                    ShipInfo si = manager.getShipInfos().get(selectedShipInfo);
                    Hull hull = si.getHull();

                    boolean oldDoubleHull = hull.isDoubleHull();
                    int oldBaseHull = hull.getBaseHull();
                    boolean ablative = hull.isAblative();
                    boolean polarisation = hull.isPolarisation();
                    int newHullMaterial = ResourceTypes.TITAN.getType();
                    if (hull.getHullMaterial() == ResourceTypes.TITAN.getType())
                        newHullMaterial = ResourceTypes.DURANIUM.getType();
                    else if (hull.getHullMaterial() == ResourceTypes.DURANIUM.getType())
                        newHullMaterial = ResourceTypes.IRIDIUM.getType();
                    else if (hull.getHullMaterial() == ResourceTypes.IRIDIUM.getType())
                        newHullMaterial = ResourceTypes.TITAN.getType();
                    hull.modifyHull(oldDoubleHull, oldBaseHull, newHullMaterial, ablative, polarisation);
                    si.calculateFinalCosts();
                    show();
                }
            }
        });
        TextButton hullTypeBtn = new TextButton(StringDB.getString("BTN_HULLTYPE"), smallButtonStyle);
        hullTypeBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (!isShipBuilding) {
                    ShipInfo si = manager.getShipInfos().get(selectedShipInfo);
                    Hull hull = si.getHull();
                    boolean oldDoubleHull = hull.isDoubleHull();
                    int oldBaseHull = hull.getBaseHull();
                    boolean ablative = hull.isAblative();
                    boolean polarisation = hull.isPolarisation();
                    int oldHullMaterial = hull.getHullMaterial();
                    if (!oldDoubleHull)
                        si.setManeuverabilty(si.getManeuverabilty() - 1);
                    else
                        si.setManeuverabilty(si.getManeuverabilty() + 1);
                    hull.modifyHull(!oldDoubleHull, oldBaseHull, oldHullMaterial, ablative, polarisation);
                    si.calculateFinalCosts();
                    show();
                }
            }
        });
        hullTypeBtn.setVisible(false);
        if ((!doubleHull && si.getManeuverabilty() > 1) || (doubleHull && si.getManeuverabilty() < 9))
            hullTypeBtn.setVisible(true);
        button.add(materialBtn).height(buttonHeight).width(buttonWidth);
        button.add(hullTypeBtn).height(buttonHeight).width(buttonWidth).spaceLeft(buttonHPad);
        hullTable.add(button).spaceTop(buttonVPad);
        hullTable.setVisible(true);
        shieldTable.clear();
        text = String.format("%s %d %s", StringDB.getString("TYPE"), si.getShield().getShieldType(),
                StringDB.getString("SHIELDS"));
        label = new Label(text, skin, "normalFont", normalColor);
        label.setAlignment(Align.center, Align.top);
        shieldTable.add(label).height((int) labelHeight);
        shieldTable.row();
        button = new Button(bs);
        TextButton weakerBtn = new TextButton(StringDB.getString("BTN_WEAKER"), smallButtonStyle);
        weakerBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (!isShipBuilding) {
                    modifyShieldToType(-1);
                    show();
                }
            }
        });
        weakerBtn.setVisible(false);
        TextButton strongerBtn = new TextButton(StringDB.getString("BTN_STRONGER"), smallButtonStyle);
        strongerBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (!isShipBuilding) {
                    modifyShieldToType(1);
                    show();
                }
            }
        });
        strongerBtn.setVisible(false);
        if (si.getShield().getShieldType() > 0)
            weakerBtn.setVisible(true);
        if (si.getShield().getShieldType() < playerRace.getWeaponObserver().getMaxShieldType())
            strongerBtn.setVisible(true);
        button.add(weakerBtn).height(buttonHeight).width(buttonWidth);
        button.add(strongerBtn).height(buttonHeight).width(buttonWidth).spaceLeft(buttonHPad);
        shieldTable.add(button).spaceTop(buttonVPad);
        shieldTable.setVisible(true);
        String systemName = checkIfShipIsBuilding(si);
        if (!systemName.isEmpty()) {
            isShipBuilding = true;
            Rectangle rect = GameConstants.coordsToRelative(328, 390, 430, 360);
            rect.x += xOffset;
            rect.y += yOffset;
            OverlayBanner banner = new OverlayBanner(rect, StringDB.getString("NO_CHANGE_POSSIBLE", false, systemName),
                    new Color(200 / 255.0f, 0, 0, 1.0f), (Texture) manager.getAssetManager()
                            .get(GameConstants.UI_BG_SIMPLE), skin, "largeFont");
            stage.addActor(banner);
            banners.add(banner);
        } else
            isShipBuilding = false;
    }

    private String checkIfShipIsBuilding(ShipInfo shipInfo) {
        int id = shipInfo.getID();
        for (int i = 0; i < manager.getUniverseMap().getStarSystems().size; i++) {
            StarSystem ss = manager.getUniverseMap().getStarSystems().get(i);
            if (ss.getOwnerId().equals(playerRace.getRaceId()))
                for (int j = 0; j < GameConstants.ALE; j++)
                    if (ss.getAssemblyList().getAssemblyListEntry(j).id == id)
                        return ss.getName();
        }
        return "";
    }

    private void drawWeaponsTable(ShipInfo si) {
        beamWeaponsTable.clear();
        float labelHeight = GameConstants.hToRelative(30);
        float buttonVPad = GameConstants.hToRelative(11);
        float buttonHPad = GameConstants.wToRelative(10);
        float buttonHeight = GameConstants.hToRelative(30);
        float buttonWidth = beamWeaponsTable.getWidth() / 2 - buttonHPad;
        float largeButtonWidth = GameConstants.wToRelative(250);
        TextButtonStyle smallButtonStyle = new TextButtonStyle(skin.get("small-buttons", TextButtonStyle.class));
        if (si.getBeamWeapons().size > beamWeaponNumber) {
            BeamWeapons bw = si.getBeamWeapons().get(beamWeaponNumber);
            String text = String.format("%s %d %s", StringDB.getString("TYPE"), bw.getBeamType(), bw.getBeamName());
            TextButton bwButton = new TextButton(text, smallButtonStyle);
            Table ttable = BaseTooltip.createTableTooltip(bwButton, tooltipTexture).getActor();
            bw.getTooltip(ttable, skin, headerFont, headerColor, textFont, textColor);
            bwButton.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    if (!isShipBuilding) {
                        ShipInfo si = manager.getShipInfos().get(selectedShipInfo);
                        if (si.getBeamWeapons().size - 1 > beamWeaponNumber) {
                            beamWeaponNumber++;
                            show();
                        } else if (si.getBeamWeapons().size - 1 == beamWeaponNumber) {
                            beamWeaponNumber = 0;
                            show();
                        }
                    }
                }
            });
            beamWeaponsTable.add(bwButton).height((int) labelHeight).width((int) largeButtonWidth);
            beamWeaponsTable.row();
            ButtonStyle bs = new ButtonStyle();
            Button button = new Button(bs);
            TextButton weakerBtn = new TextButton(StringDB.getString("BTN_WEAKER"), smallButtonStyle);
            weakerBtn.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    if (!isShipBuilding) {
                        modifyBeamToType(-1);
                        show();
                    }
                }
            });
            weakerBtn.setVisible(false);
            //then we can put a better beam
            TextButton strongerBtn = new TextButton(StringDB.getString("BTN_STRONGER"), smallButtonStyle);
            strongerBtn.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    if (!isShipBuilding) {
                        modifyBeamToType(1);
                        show();
                    }
                }
            });
            strongerBtn.setVisible(false);
            button.add(weakerBtn).height(buttonHeight).width(buttonWidth);
            button.add(strongerBtn).height(buttonHeight).width(buttonWidth).spaceLeft(buttonHPad);
            beamWeaponsTable.add(button).spaceTop(buttonVPad);
            //if our type is higherthan 1 then we can make it weaker
            if (bw.getBeamType() > 1) {
                weakerBtn.setVisible(true);
            }
            //is there a higher type on this ship already?
            int maxType = playerRace.getWeaponObserver().getMaxBeamType(bw.getBeamName());
            if (maxType > bw.getBeamType()) {
                strongerBtn.setVisible(true);
            }
            beamWeaponsTable.setVisible(true);
        }
        torpedoWeaponsTable.clear();
        if (si.getTorpedoWeapons().size > torpedoWeaponNumber) {
            String text;
            ButtonStyle bs = new ButtonStyle();
            Button button = new Button(bs);
            TorpedoWeapons tw = si.getTorpedoWeapons().get(torpedoWeaponNumber);
            int currentTorpType = tw.getTorpedoType();
            text = String.format("%s (%d\u00b0)", tw.getTubeName(), tw.getFireArc().getAngle());
            TextButton twButton = new TextButton(text, smallButtonStyle);
            Table ttable = BaseTooltip.createTableTooltip(twButton, tooltipTexture).getActor();
            tw.getTooltip(ttable, skin, headerFont, headerColor, textFont, textColor);
            twButton.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    if (!isShipBuilding) {
                        ShipInfo si = manager.getShipInfos().get(selectedShipInfo);
                        if (si.getTorpedoWeapons().size - 1 > torpedoWeaponNumber) {
                            torpedoWeaponNumber++;
                            show();
                        } else if (si.getTorpedoWeapons().size - 1 == torpedoWeaponNumber) {
                            torpedoWeaponNumber = 0;
                            show();
                        }
                    }
                }
            });
            torpedoWeaponsTable.add(twButton).height((int) (labelHeight)).width((int) largeButtonWidth).spaceBottom(GameConstants.hToRelative(3));
            torpedoWeaponsTable.row();

            text = String.format("%s (%d)", TorpedoInfo.getName(currentTorpType), TorpedoInfo.getPower(currentTorpType));
            Label label = new Label(text, skin, "normalFont", normalColor);
            ttable = BaseTooltip.createTableTooltip(label, tooltipTexture).getActor();
            tw.getTooltip(ttable, skin, headerFont, headerColor, textFont, textColor);
            label.setAlignment(Align.center, Align.top);
            torpedoWeaponsTable.add(label).height((int) labelHeight);
            torpedoWeaponsTable.row();

            TextButton launcherBtn = new TextButton(StringDB.getString("BTN_LAUNCHER"), smallButtonStyle);
            launcherBtn.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    if (!isShipBuilding) {
                        ShipInfo si = manager.getShipInfos().get(selectedShipInfo);
                        TorpedoWeapons tw = si.getTorpedoWeapons().get(torpedoWeaponNumber);
                        int oldTorpType = tw.getTorpedoType();
                        int oldTubeNumber = tw.getNumberOfTubes();
                        int oldAcc = tw.getAccuracy();
                        String oldTubeName = tw.getTubeName();

                        TubeWeaponsObserverStruct twos = playerRace.getWeaponObserver().getNextTube(oldTubeName,
                                oldTorpType);
                        tw.modifyTorpedoWeapon(oldTorpType, twos.number, twos.fireRate, oldTubeNumber, twos.tubeName,
                                twos.onlyMicro, oldAcc);
                        int mountPos = tw.getFireArc().getPosition();
                        int angle = twos.fireAngle;
                        tw.getFireArc().setValues(mountPos, angle);

                        si.calculateFinalCosts();
                        show();
                    }
                }
            });
            //then we can put a better beam
            TextButton torpedoBtn = new TextButton(StringDB.getString("BTN_TORPEDO"), smallButtonStyle);
            torpedoBtn.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    if (!isShipBuilding) {
                        ShipInfo si = manager.getShipInfos().get(selectedShipInfo);
                        TorpedoWeapons tw = si.getTorpedoWeapons().get(torpedoWeaponNumber);
                        int oldNumber = tw.getNumber();
                        int oldFireRate = tw.getTubeFirerate();
                        int oldTubeNumber = tw.getNumberOfTubes();
                        boolean oldOnlyMicro = tw.isOnlyMicroPhoton();
                        int oldAcc = tw.getAccuracy();
                        int oldTorpType = tw.getTorpedoType();
                        String oldTubeName = tw.getTubeName();

                        int newTorpType = playerRace.getWeaponObserver().getNextTorpedo(oldTorpType, oldOnlyMicro);
                        tw.modifyTorpedoWeapon(newTorpType, oldNumber, oldFireRate, oldTubeNumber, oldTubeName,
                                oldOnlyMicro, oldAcc);
                        si.calculateFinalCosts();
                        show();
                    }
                }
            });
            button.add(launcherBtn).height(buttonHeight).width(buttonWidth);
            button.add(torpedoBtn).height(buttonHeight).width(buttonWidth).spaceLeft(buttonHPad);
            torpedoWeaponsTable.add(button).spaceTop(buttonVPad);
            torpedoWeaponsTable.setVisible(true);
        }
    }

    private void modifyShieldToType(int add) {
        ShipInfo si = manager.getShipInfos().get(selectedShipInfo);
        Shield shield = si.getShield();
        int oldMaxShield = shield.getMaxShield();
        int oldShieldType = shield.getShieldType();
        boolean regenerative = shield.isRegenerative();
        shield.modifyShield(oldMaxShield, (oldShieldType + add), regenerative);
        si.calculateFinalCosts();
    }

    private void modifyBeamToType(int add) {
        ShipInfo si = manager.getShipInfos().get(selectedShipInfo);
        BeamWeapons bw = si.getBeamWeapons().get(beamWeaponNumber);
        int oldType = bw.getBeamType();
        int oldPower = bw.getBeamPower();
        int oldNumber = bw.getBeamNumber();
        int oldShootNumber = bw.getShootNumber();
        String oldName = bw.getBeamName();
        int oldBonus = bw.getBonus();
        int oldLength = bw.getBeamLength();
        int oldRechargeTime = bw.getRechargeTime();
        boolean piercing = bw.isPiercing();
        boolean modulating = bw.isModulating();

        bw.modifyBeamWeapon(oldType + add, oldPower, oldNumber, oldName, modulating, piercing, oldBonus, oldLength,
                oldRechargeTime, oldShootNumber);
        si.calculateFinalCosts();
    }

    private void drawShipsList() {
        shipListScrollTable.clear();
        shipItems.clear();
        Research research = playerRace.getEmpire().getResearch();
        int researchLevels[] = research.getResearchLevels();
        int pad = (int) GameConstants.wToRelative(7);
        int idx = 0;
        for (int i = 0; i < manager.getShipInfos().size; i++) {
            ShipInfo si = manager.getShipInfos().get(i);
            if (si.getRace() == playerRace.getRaceShipNumber())
                if (!si.isStation())
                    if (si.isThisShipBuildableNow(researchLevels)) {
                        boolean foundObsolete = false;
                        for (int m = 0; m < manager.getShipInfos().size; m++) {
                            ShipInfo info = manager.getShipInfos().get(m);
                            if (info.getRace() == playerRace.getRaceShipNumber())
                                if (info.getObsoletesClassId() == si.getID())
                                    if (info.isThisShipBuildableNow(researchLevels)) {
                                        foundObsolete = true;
                                        break;
                                    }
                        }
                        if (foundObsolete)
                            continue;

                        Button.ButtonStyle bs = new Button.ButtonStyle();
                        Button button = new Button(bs);
                        button.align(Align.left);
                        button.setSkin(skin);

                        button.add().width(pad);
                        Label slabel = new Label(si.getShipClass(), skin, "normalFont", Color.WHITE);
                        slabel.setUserObject(i);
                        slabel.setColor(normalColor);
                        int width = (int) ((int) (shipListScroller.getWidth() - pad * 2 - shipListScroller.getStyle().vScrollKnob.getMinWidth()));
                        button.add(slabel).width(width);
                        button.add().width(pad);
                        button.setUserObject(slabel);
                        button.setName("" + idx++);
                        ActorGestureListener gestureListener = new ActorGestureListener() {
                            @Override
                            public void tap(InputEvent event, float x, float y, int count, int button) {
                                Button b = (Button) event.getListenerActor();
                                showInfo(b);
                            }
                        };
                        button.addListener(gestureListener);

                        shipListScrollTable.add(button);
                        shipListScrollTable.row();
                        shipItems.add(button);
                    }
        }
        shipListScroller.setVisible(true);

        stage.setKeyboardFocus(shipListScrollTable);
        shipListScrollTable.addListener(new InputListener() {
            @Override
            public boolean keyDown (InputEvent event, final int keycode) {
                int limit = shipItems.size;
                int selectedItem = Integer.parseInt(shipSelection.getName());
                if (limit < selectedItem)
                    selectedItem = limit - 1;
                Button b = shipListScrollTable.findActor("" + selectedItem);
                switch (keycode) {
                    case Keys.DOWN:
                        selectedItem++;
                        break;
                    case Keys.UP:
                        selectedItem--;
                        break;
                    case Keys.HOME:
                        selectedItem = 0;
                        break;
                    case Keys.END:
                        selectedItem = limit - 1;
                        break;
                    case Keys.PAGE_DOWN:
                        selectedItem += shipListScroller.getScrollHeight() / b.getHeight();
                        break;
                    case Keys.PAGE_UP:
                        selectedItem -= shipListScroller.getScrollHeight() / b.getHeight();
                        break;
                }

                if (ShipDesignView.this.visible) {
                    if (selectedItem >= limit)
                        selectedItem = limit - 1;
                    if (selectedItem < 0)
                        selectedItem = 0;

                    b = shipListScrollTable.findActor("" + selectedItem);
                    showInfo(b);

                    Thread th = new Thread() {
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(150);
                            } catch (InterruptedException e) {
                            }
                            if (Gdx.input.isKeyPressed(keycode)) {
                                Gdx.app.postRunnable(new Runnable() {
                                    @Override
                                    public void run() {
                                        InputEvent event = new InputEvent();
                                        event.setType(Type.keyDown);
                                        event.setKeyCode(keycode);
                                        shipListScrollTable.fire(event);
                                    }
                                });
                            }
                        }
                    };
                    th.start();
                }

                return false;
            }
        });

        stage.draw();
        Button btn = null;
        if (selectedShipInfo != -1) {
            for (Button b : shipItems) {
                if (selectedShipInfo == getShips(b)) {
                    btn = b;
                    break;
                }
            }
        }
        if (btn == null) {
            if (shipItems.size > 0) {
                btn = shipItems.get(0);
                selectedShipInfo = getShips(btn);
            }
        }
        if (btn != null)
            markShipListSelected(btn);
    }

    public void hide() {
        visible = false;
        nameTable.setVisible(false);
        shipListScroller.setVisible(false);
        infoScroller.setVisible(false);
        infoHeader.setVisible(false);
        beamWeaponsTable.setVisible(false);
        torpedoWeaponsTable.setVisible(false);
        hullTable.setVisible(false);
        shieldTable.setVisible(false);
        for (String path : loadedTextures)
            if (manager.getAssetManager().isLoaded(path))
                manager.getAssetManager().unload(path);
        loadedTextures.clear();
    }

    private void showInfo(Button b) {
        markShipListSelected(b);
        selectedShipInfo = getShips(b);
        beamWeaponNumber = 0;
        torpedoWeaponNumber = 0;
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                manager.getScreen().show();
            }
        });
    }

    private void markShipListSelected(Button b) {
        if (shipSelection != null) {
            shipSelection.getStyle().up = null;
            shipSelection.getStyle().down = null;
            ((Label) shipSelection.getUserObject()).setColor(oldColor);
        }
        if (b == null)
            b = shipSelection;
        Image itemSelection = new Image(selectTexture);
        b.getStyle().up = itemSelection.getDrawable();
        b.getStyle().down = itemSelection.getDrawable();
        oldColor = new Color(((Label) b.getUserObject()).getColor());
        ((Label) b.getUserObject()).setColor(markColor);
        shipSelection = b;

        float scrollerHeight = shipListScroller.getScrollHeight();
        float scrollerPos = shipListScroller.getScrollY();
        int selectedItem = Integer.parseInt(shipSelection.getName());
        float buttonPos = b.getHeight() * selectedItem;
        if (buttonPos + b.getHeight() / 2 - scrollerPos < 0)
            shipListScroller.setScrollY(b.getHeight() * selectedItem - shipListScroller.getScrollHeight() * 2);
        else if (buttonPos + b.getHeight() / 2 - scrollerPos > scrollerHeight)
            shipListScroller.setScrollY(b.getHeight() * (selectedItem - shipListScroller.getScrollHeight() / b.getHeight() + 1));
    }

    private int getShips(Button b) {
        return (Integer) ((Label) b.getUserObject()).getUserObject();
    }
}
