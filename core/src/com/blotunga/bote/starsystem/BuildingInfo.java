/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.starsystem;

import java.util.Arrays;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.PlanetType;
import com.blotunga.bote.constants.ResearchType;
import com.blotunga.bote.constants.ResourceTypes;
import com.blotunga.bote.constants.ShipSize;
import com.blotunga.bote.constants.WorkerType;
import com.blotunga.bote.general.StringDB;

public class BuildingInfo {
    public class NumberOfId {
        public int runningNumber;
        public int number;

        NumberOfId() {
            runningNumber = 0;
            number = 0;
        }

        NumberOfId(NumberOfId other) {
            runningNumber = other.runningNumber;
            number = other.number;
        }
    }

    private int runningNumber;	//ID of the building
    private int ownerOfBuilding;
    private String buildingName;
    private String buildingDescription;
    private boolean upgradeable;
    private String graphicFileName;
    private NumberOfId maxInSystem; // max number which may be built in the system
    private NumberOfId maxInEmpire;	// max number of buildings of this type which have to be built in the empire
    private boolean onlyHomePlanet; // can be built only in home system
    private boolean onlyOwnColony;	// can be built only in colonies founded by the empire or home system
    private boolean onlyMinorRace;	// can be built only in the system of a minor race
    private boolean onlyTakenSystem; // can be built only in a taken system
    private String onlyInSystemWithName;	// can be built only in system with the specified name (for example minor race home system)
    private int minInhabitants;		// minimum population of the system (in billions)
    private NumberOfId minInSystem;	// minimum number which has to be built in the system
    private NumberOfId minInEmpire;	// minimum number which have to be built in the empire
    private boolean onlyRace;		// only the race whom this belongs to can build it (if conquered the conqueror can't build new ones)
    private int planetTypes;		// planent types on which we can build the building
    // research needed for the building:
    private int bioTech;
    private int energyTech;
    private int computerTech;
    private int propulsionTech;
    private int constructionTech;
    private int weaponTech;
    // resources needed to build building
    private int neededIndustry;
    private int neededTitan;
    private int neededDeuterium;
    private int neededDuranium;
    private int neededCrystal;
    private int neededIridium;
    private int neededDeritium;
    private int predecessorId;		// runningNumber (ID) of the predecessor, needed for upgrades
    private int buildingEquivalent[];	// equivalent to other races, stores the equivalent's runningNumber
    private int neededEnergy;		// energy needed to run the building
    private boolean alwaysOnline;	// is the building always on?
    // production
    private int food;
    private int industryPoints;
    private int energy;
    private int securityPoints;
    private int researchPoints;
    private int titan;
    private int deuterium;
    private int duranium;
    private int crystal;
    private int iridium;
    private int deritium;
    private int credits;
    private int morale;
    private int moraleEmpire;
    //production bonus/malus
    private int foodBonus;
    private int industryBonus;
    private int energyBonus;
    private int securityBonus;
    private int researchBonus;
    private int titanBonus;
    private int deuteriumBonus;
    private int duraniumBonus;
    private int crystalBonus;
    private int iridiumBonus;
    private int deritiumBonus;
    private int allResourceBonus;
    private int creditsBonus;
    //research bonus
    private int bioTechBonus;
    private int energyTechBonus;
    private int computerTechBonus;
    private int propulsionTechBonus;
    private int constructionTechBonus;
    private int weaponTechBonus;
    //security bonus
    private int innerSecurityBonus;
    private int economySpyBonus;
    private int economySabotageBonus;
    private int researchSpyBonus;
    private int researchSabotageBonus;
    private int militarySpyBonus;
    private int militarySabogateBonus;
    //other info
    private boolean shipyard;
    private ShipSize buildableShipSizes;
    private int shipyardSpeed;
    private boolean barrack;
    private int barrackSpeed;
    private int hitPoints;
    private int shieldPower;
    private int shieldPowerBonus;
    private int shipDefend;
    private int shipDefendBonus;
    private int groundDefend;
    private int groundDefendBonus;
    private int scanPower;
    private int scanPowerBonus;
    private int scanRange;
    private int scanRangeBonus;
    private int shipTraining;
    private int troopTraining;
    private int resistance;
    private int addedTradeRoutes;
    private int incomeOnTradeRoutes;
    private int shipRecycling;
    private int buildingBuildSpeed;
    private int updateBuildSpeed;
    private int shipBuildSpeed;
    private int troopBuildSpeed;
    private boolean worker; //does the building need workers?
    private boolean neverReady; // is the building never ready (ie tribunal)
    private boolean[] resourceDistributor;
    private int neededSystems;	//number of systems needed to build the building

    public BuildingInfo() {
        runningNumber = 0;
        ownerOfBuilding = 0;
        buildingName = "";
        buildingDescription = "";
        upgradeable = false;
        graphicFileName = "";
        maxInSystem = new NumberOfId();
        maxInEmpire = new NumberOfId();
        onlyHomePlanet = false;
        onlyOwnColony = false;
        onlyMinorRace = false;
        onlyInSystemWithName = "";
        minInhabitants = 0;
        minInSystem = new NumberOfId();
        minInEmpire = new NumberOfId();
        onlyRace = false;
        planetTypes = 0;
        bioTech = 0;
        energyTech = 0;
        computerTech = 0;
        propulsionTech = 0;
        constructionTech = 0;
        weaponTech = 0;
        neededIndustry = 0;
        neededTitan = 0;
        neededDeuterium = 0;
        neededDuranium = 0;
        neededCrystal = 0;
        neededIridium = 0;
        neededDeritium = 0;
        predecessorId = 0;
        buildingEquivalent = new int[7];
        Arrays.fill(buildingEquivalent, 0);
        neededEnergy = 0;
        alwaysOnline = false;
        food = 0;
        industryPoints = 0;
        energy = 0;
        securityPoints = 0;
        researchPoints = 0;
        titan = 0;
        deuterium = 0;
        duranium = 0;
        crystal = 0;
        iridium = 0;
        deritium = 0;
        credits = 0;
        morale = 0;
        moraleEmpire = 0;
        foodBonus = 0;
        industryBonus = 0;
        energyBonus = 0;
        securityBonus = 0;
        researchBonus = 0;
        titanBonus = 0;
        deuteriumBonus = 0;
        duraniumBonus = 0;
        crystalBonus = 0;
        iridiumBonus = 0;
        deritiumBonus = 0;
        allResourceBonus = 0;
        creditsBonus = 0;
        bioTechBonus = 0;
        energyTechBonus = 0;
        computerTechBonus = 0;
        propulsionTechBonus = 0;
        constructionTechBonus = 0;
        weaponTechBonus = 0;
        innerSecurityBonus = 0;
        economySpyBonus = 0;
        economySabotageBonus = 0;
        researchSpyBonus = 0;
        researchSabotageBonus = 0;
        militarySpyBonus = 0;
        militarySabogateBonus = 0;
        shipyard = false;
        buildableShipSizes = ShipSize.HUGE;
        shipyardSpeed = 0;
        barrack = false;
        barrackSpeed = 0;
        hitPoints = 0;
        shieldPower = 0;
        shieldPowerBonus = 0;
        shipDefend = 0;
        shipDefendBonus = 0;
        groundDefend = 0;
        groundDefendBonus = 0;
        scanPower = 0;
        scanPowerBonus = 0;
        scanRange = 0;
        scanRangeBonus = 0;
        shipTraining = 0;
        troopTraining = 0;
        resistance = 0;
        addedTradeRoutes = 0;
        incomeOnTradeRoutes = 0;
        shipRecycling = 0;
        buildingBuildSpeed = 0;
        updateBuildSpeed = 0;
        shipBuildSpeed = 0;
        troopBuildSpeed = 0;
        worker = false;
        neverReady = false;
        resourceDistributor = new boolean[ResourceTypes.DERITIUM.getType() + 1];
        Arrays.fill(resourceDistributor, false);
        neededSystems = 0;
    }

    public BuildingInfo(BuildingInfo other) {
        runningNumber = other.runningNumber;
        ownerOfBuilding = other.ownerOfBuilding;
        buildingName = other.buildingName;
        buildingDescription = other.buildingDescription;
        upgradeable = other.upgradeable;
        graphicFileName = other.graphicFileName;
        maxInSystem = new NumberOfId(other.maxInSystem);
        maxInEmpire = new NumberOfId(other.maxInEmpire);
        onlyHomePlanet = other.onlyHomePlanet;
        onlyOwnColony = other.onlyOwnColony;
        onlyMinorRace = other.onlyMinorRace;
        onlyInSystemWithName = other.onlyInSystemWithName;
        minInhabitants = other.minInhabitants;
        minInSystem = new NumberOfId(other.minInSystem);
        minInEmpire = new NumberOfId(other.minInEmpire);
        onlyRace = other.onlyRace;
        planetTypes = other.planetTypes;
        bioTech = other.bioTech;
        energyTech = other.energyTech;
        computerTech = other.computerTech;
        propulsionTech = other.propulsionTech;
        constructionTech = other.constructionTech;
        weaponTech = other.weaponTech;
        neededIndustry = other.neededIndustry;
        neededTitan = other.neededTitan;
        neededDeuterium = other.neededDeuterium;
        neededDuranium = other.neededDuranium;
        neededCrystal = other.neededCrystal;
        neededIridium = other.neededIridium;
        neededDeritium = other.neededDeritium;
        predecessorId = other.predecessorId;
        buildingEquivalent = other.buildingEquivalent.clone();
        neededEnergy = other.neededEnergy;
        alwaysOnline = other.alwaysOnline;
        food = other.food;
        industryPoints = other.industryPoints;
        energy = other.energy;
        securityPoints = other.securityPoints;
        researchPoints = other.researchPoints;
        titan = other.titan;
        deuterium = other.deuterium;
        duranium = other.duranium;
        crystal = other.crystal;
        iridium = other.iridium;
        deritium = other.deritium;
        credits = other.credits;
        morale = other.morale;
        moraleEmpire = other.moraleEmpire;
        foodBonus = other.foodBonus;
        industryBonus = other.industryBonus;
        energyBonus = other.energyBonus;
        securityBonus = other.securityBonus;
        researchBonus = other.researchBonus;
        titanBonus = other.titanBonus;
        deuteriumBonus = other.deuteriumBonus;
        duraniumBonus = other.duraniumBonus;
        crystalBonus = other.crystalBonus;
        iridiumBonus = other.iridiumBonus;
        deritiumBonus = other.deritiumBonus;
        allResourceBonus = other.allResourceBonus;
        creditsBonus = other.creditsBonus;
        bioTechBonus = other.bioTechBonus;
        energyTechBonus = other.energyTechBonus;
        computerTechBonus = other.computerTechBonus;
        propulsionTechBonus = other.propulsionTechBonus;
        constructionTechBonus = other.constructionTechBonus;
        weaponTechBonus = other.weaponTechBonus;
        innerSecurityBonus = other.innerSecurityBonus;
        economySpyBonus = other.economySpyBonus;
        economySabotageBonus = other.economySabotageBonus;
        researchSpyBonus = other.researchSpyBonus;
        researchSabotageBonus = other.researchSabotageBonus;
        militarySpyBonus = other.militarySpyBonus;
        militarySabogateBonus = other.militarySabogateBonus;
        shipyard = other.shipyard;
        buildableShipSizes = other.buildableShipSizes;
        shipyardSpeed = other.shipyardSpeed;
        barrack = other.barrack;
        barrackSpeed = other.barrackSpeed;
        hitPoints = other.hitPoints;
        shieldPower = other.shieldPower;
        shieldPowerBonus = other.shieldPowerBonus;
        shipDefend = other.shipDefend;
        shipDefendBonus = other.shipDefendBonus;
        groundDefend = other.groundDefend;
        groundDefendBonus = other.groundDefendBonus;
        scanPower = other.scanPower;
        scanPowerBonus = other.scanPowerBonus;
        scanRange = other.scanRange;
        scanRangeBonus = other.scanRangeBonus;
        shipTraining = other.shipTraining;
        troopTraining = other.troopTraining;
        resistance = other.resistance;
        addedTradeRoutes = other.addedTradeRoutes;
        incomeOnTradeRoutes = other.incomeOnTradeRoutes;
        shipRecycling = other.shipRecycling;
        buildingBuildSpeed = other.buildingBuildSpeed;
        updateBuildSpeed = other.updateBuildSpeed;
        shipBuildSpeed = other.shipBuildSpeed;
        troopBuildSpeed = other.troopBuildSpeed;
        worker = other.worker;
        neverReady = other.neverReady;
        resourceDistributor = other.resourceDistributor.clone();
        neededSystems = other.neededSystems;
    }

    public int getRunningNumber() {
        return runningNumber;
    }

    public int getOwnerOfBuilding() {
        return ownerOfBuilding;
    }

    public String getBuildingName() {
        return buildingName;
    }

    public String getBuildingDescription() {
        return buildingDescription;
    }

    public boolean isUpgradeable() {
        return upgradeable;
    }

    public String getGraphicFileName() {
        return graphicFileName;
    }

    public NumberOfId getMaxInSystem() {
        return maxInSystem;
    }

    public int getMaxInEmpire() {
        return maxInEmpire.number;
    }

    public int getMaxInEmpireID() {
        return maxInEmpire.runningNumber;
    }

    public boolean isOnlyHomePlanet() {
        return onlyHomePlanet;
    }

    public boolean isOnlyOwnColony() {
        return onlyOwnColony;
    }

    public boolean isOnlyMinorRace() {
        return onlyMinorRace;
    }

    public boolean isOnlyTakenSystem() {
        return onlyTakenSystem;
    }

    public String getOnlyInSystemWithName() {
        return onlyInSystemWithName;
    }

    public int getMinInhabitants() {
        return minInhabitants;
    }

    public NumberOfId getMinInSystem() {
        return minInSystem;
    }

    public NumberOfId getMinInEmpire() {
        return minInEmpire;
    }

    public boolean isOnlyRace() {
        return onlyRace;
    }

    public boolean getPlanetTypes(int planetType) {
        PlanetType pt = PlanetType.fromPlanetType(planetType);
        return ((planetTypes & pt.getType()) == pt.getType());
    }

    public int getBioTech() {
        return bioTech;
    }

    public int getEnergyTech() {
        return energyTech;
    }

    public int getComputerTech() {
        return computerTech;
    }

    public int getPropulsionTech() {
        return propulsionTech;
    }

    public int getConstructionTech() {
        return constructionTech;
    }

    public int getWeaponTech() {
        return weaponTech;
    }

    public int[] getNeededResearchLevels() {
        int researchLevels[] = { bioTech, energyTech, computerTech, propulsionTech, constructionTech, weaponTech };
        return researchLevels;
    }

    public int getNeededIndustry() {
        return neededIndustry;
    }

    public int getNeededTitan() {
        return neededTitan;
    }

    public int getNeededDeuterium() {
        return neededDeuterium;
    }

    public int getNeededDuranium() {
        return neededDuranium;
    }

    public int getNeededCrystal() {
        return neededCrystal;
    }

    public int getNeededIridium() {
        return neededIridium;
    }

    public int getNeededDeritium() {
        return neededDeritium;
    }

    public int getNeededResource(int type) {
        ResourceTypes res = ResourceTypes.fromResourceTypes(type);
        switch (res) {
            case TITAN:
                return getNeededTitan();
            case DEUTERIUM:
                return getNeededDeuterium();
            case DURANIUM:
                return getNeededDuranium();
            case CRYSTAL:
                return getNeededCrystal();
            case IRIDIUM:
                return getNeededIridium();
            case DERITIUM:
                return getNeededDeritium();
            default:
                return 0;
        }
    }

    public int getPredecessorId() {
        return predecessorId;
    }

    public int getBuildingEquivalent(int race) {
        return buildingEquivalent[race];
    }

    public int getNeededEnergy() {
        return neededEnergy;
    }

    public int getFoodProd() {
        return food;
    }

    public int getIndustryPointsProd() {
        return industryPoints;
    }

    public int getEnergyProd() {
        return energy;
    }

    public int getSecurityPointsProd() {
        return securityPoints;
    }

    public int getResearchPointsProd() {
        return researchPoints;
    }

    public int getTitanProd() {
        return titan;
    }

    public int getDeuteriumProd() {
        return deuterium;
    }

    public int getDuraniumProd() {
        return duranium;
    }

    public int getCrystalProd() {
        return crystal;
    }

    public int getIridiumProd() {
        return iridium;
    }

    public int getDeritiumProd() {
        return deritium;
    }

    public int getResourceProd(int type) {
        ResourceTypes res = ResourceTypes.fromResourceTypes(type);
        switch (res) {
            case TITAN:
                return getTitanProd();
            case DEUTERIUM:
                return getDeuteriumProd();
            case DURANIUM:
                return getDuraniumProd();
            case CRYSTAL:
                return getCrystalProd();
            case IRIDIUM:
                return getIridiumProd();
            case DERITIUM:
                return getDeritiumProd();
            default:
                return 0;
        }
    }

    public int getCredits() {
        return credits;
    }

    public int getMoraleProd() {
        return morale;
    }

    public int getMoraleProdEmpire() {
        return moraleEmpire;
    }

    public int getFoodBonus() {
        return foodBonus;
    }

    public int getIndustryBonus() {
        return industryBonus;
    }

    public int getEnergyBonus() {
        return energyBonus;
    }

    public int getSecurityBonus() {
        return securityBonus;
    }

    public int getResearchBonus() {
        return researchBonus;
    }

    public int getTitanBonus() {
        return titanBonus;
    }

    public int getDeuteriumBonus() {
        return deuteriumBonus;
    }

    public int getDuraniumBonus() {
        return duraniumBonus;
    }

    public int getCrystalBonus() {
        return crystalBonus;
    }

    public int getIridiumBonus() {
        return iridiumBonus;
    }

    public int getDeritiumBonus() {
        return deritiumBonus;
    }

    public int getAllResourceBonus() {
        return allResourceBonus;
    }

    public int getCreditsBonus() {
        return creditsBonus;
    }

    public int getBioTechBonus() {
        return bioTechBonus;
    }

    public int getEnergyTechBonus() {
        return energyTechBonus;
    }

    public int getComputerTechBonus() {
        return computerTechBonus;
    }

    public int getPropulsionTechBonus() {
        return propulsionTechBonus;
    }

    public int getConstructionTechBonus() {
        return constructionTechBonus;
    }

    public int getWeaponTechBonus() {
        return weaponTechBonus;
    }

    public int getInnerSecurityBonus() {
        return innerSecurityBonus;
    }

    public int getEconomySpyBonus() {
        return economySpyBonus;
    }

    public int getEconomySabotageBonus() {
        return economySabotageBonus;
    }

    public int getResearchSpyBonus() {
        return researchSpyBonus;
    }

    public int getResearchSabotageBonus() {
        return researchSabotageBonus;
    }

    public int getMilitarySpyBonus() {
        return militarySpyBonus;
    }

    public int getMilitarySabogateBonus() {
        return militarySabogateBonus;
    }

    public boolean isShipYard() {
        return shipyard;
    }

    public ShipSize getMaxBuildableShipSize() {
        return buildableShipSizes;
    }

    public int getShipYardSpeed() {
        return shipyardSpeed;
    }

    public boolean isBarrack() {
        return barrack;
    }

    public int getBarrackSpeed() {
        return barrackSpeed;
    }

    public int getHitPoints() {
        return hitPoints;
    }

    public int getShieldPower() {
        return shieldPower;
    }

    public int getShieldPowerBonus() {
        return shieldPowerBonus;
    }

    public int getShipDefend() {
        return shipDefend;
    }

    public int getShipDefendBonus() {
        return shipDefendBonus;
    }

    public int getGroundDefend() {
        return groundDefend;
    }

    public int getGroundDefendBonus() {
        return groundDefendBonus;
    }

    public int getScanPower() {
        return scanPower;
    }

    public int getScanPowerBonus() {
        return scanPowerBonus;
    }

    public int getScanRange() {
        return scanRange;
    }

    public int getScanRangeBonus() {
        return scanRangeBonus;
    }

    public int getShipTraining() {
        return shipTraining;
    }

    public int getTroopTraining() {
        return troopTraining;
    }

    public int getResistance() {
        return resistance;
    }

    public int getAddedTradeRoutes() {
        return addedTradeRoutes;
    }

    public int getIncomeOnTradeRoutes() {
        return incomeOnTradeRoutes;
    }

    public int getShipRecycling() {
        return shipRecycling;
    }

    public int getBuildingBuildSpeed() {
        return buildingBuildSpeed;
    }

    public int getUpdateBuildSpeed() {
        return updateBuildSpeed;
    }

    public int getShipBuildSpeed() {
        return shipBuildSpeed;
    }

    public int getTroopBuildSpeed() {
        return troopBuildSpeed;
    }

    public boolean getWorker() {
        return worker;
    }

    public boolean getNeverReady() {
        return neverReady;
    }

    public boolean getAlwaysOnline() {
        return alwaysOnline;
    }

    public boolean getResourceDistributor(int res) {
        return resourceDistributor[res];
    }

    public int getNeededSystems() {
        return neededSystems;
    }

    //setters
    public void setRunningNumber(int runningNumber) {
        this.runningNumber = runningNumber;
    }

    public void setOwnerOfBuilding(int ownerOfBuilding) {
        this.ownerOfBuilding = ownerOfBuilding;
    }

    public void setBuildingName(String buildingName) {
        this.buildingName = buildingName;
    }

    public void setBuildingDescription(String buildingDescription) {
        this.buildingDescription = buildingDescription;
    }

    public void setUpgradeable(boolean upgradeable) {
        this.upgradeable = upgradeable;
    }

    public void setGraphicFileName(String graphicFileName) {
        this.graphicFileName = graphicFileName;
    }

    public void setMaxInSystem(int number, int runningNumber) {
        maxInSystem.number = number;
        maxInSystem.runningNumber = runningNumber;
    }

    public void setMaxInEmpire(int number, int runningNumber) {
        maxInEmpire.number = number;
        maxInEmpire.runningNumber = runningNumber;
    }

    public void setOnlyHomePlanet(boolean onlyHomePlanet) {
        this.onlyHomePlanet = onlyHomePlanet;
    }

    public void setOnlyOwnColony(boolean onlyOwnColony) {
        this.onlyOwnColony = onlyOwnColony;
    }

    public void setOnlyMinorRace(boolean onlyMinorRace) {
        this.onlyMinorRace = onlyMinorRace;
    }

    public void setOnlyTakenSystem(boolean onlyTakenSystem) {
        this.onlyTakenSystem = onlyTakenSystem;
    }

    public void setOnlyInSystemWithName(String onlyInSystemWithName) {
        this.onlyInSystemWithName = onlyInSystemWithName;
    }

    public void setMinInhabitants(int minInhabitants) {
        this.minInhabitants = minInhabitants;
    }

    public void setMinInSystem(int number, int runningNumber) {
        minInSystem.number = number;
        minInSystem.runningNumber = runningNumber;
    }

    public void setMinInEmpire(int number, int runningNumber) {
        minInEmpire.number = number;
        minInEmpire.runningNumber = runningNumber;
    }

    public void setOnlyRace(boolean onlyRace) {
        this.onlyRace = onlyRace;
    }

    public void setPlanetTypes(int planetClass, boolean allow) {
        PlanetType pt = PlanetType.fromPlanetClass(planetClass);
        planetTypes = GameConstants.setAttributes(allow, pt.getType(), planetTypes);
    }

    public void setBioTech(int bioTech) {
        this.bioTech = bioTech;
    }

    public void setEnergyTech(int energyTech) {
        this.energyTech = energyTech;
    }

    public void setComputerTech(int computerTech) {
        this.computerTech = computerTech;
    }

    public void setPropulsionTech(int propulsionTech) {
        this.propulsionTech = propulsionTech;
    }

    public void setConstructionTech(int constructionTech) {
        this.constructionTech = constructionTech;
    }

    public void setWeaponTech(int weaponTech) {
        this.weaponTech = weaponTech;
    }

    public void setNeededIndustry(int neededIndustry) {
        this.neededIndustry = neededIndustry;
    }

    public void setNeededTitan(int neededTitan) {
        this.neededTitan = neededTitan;
    }

    public void setNeededDeuterium(int neededDeuterium) {
        this.neededDeuterium = neededDeuterium;
    }

    public void setNeededDuranium(int neededDuranium) {
        this.neededDuranium = neededDuranium;
    }

    public void setNeededCrystal(int neededCrystal) {
        this.neededCrystal = neededCrystal;
    }

    public void setNeededIridium(int neededIridium) {
        this.neededIridium = neededIridium;
    }

    public void setNeededDeritium(int neededDeritium) {
        this.neededDeritium = neededDeritium;
    }

    public void setPredecessorId(int predecessorId) {
        this.predecessorId = predecessorId;
    }

    public void setBuildingEquivalent(int race, int equivalentID) {
        buildingEquivalent[race] = equivalentID;
    }

    public void setNeededEnergy(int neededEnergy) {
        this.neededEnergy = neededEnergy;
    }

    public void setFoodProd(int food) {
        this.food = food;
    }

    public void setIndustryPointsProd(int industryPoints) {
        this.industryPoints = industryPoints;
    }

    public void setEnergyProd(int energy) {
        this.energy = energy;
    }

    public void setSecurityPointsProd(int securityPoints) {
        this.securityPoints = securityPoints;
    }

    public void setResearchPointsProd(int researchPoints) {
        this.researchPoints = researchPoints;
    }

    public void setTitanProd(int titan) {
        this.titan = titan;
    }

    public void setDeuteriumProd(int deuterium) {
        this.deuterium = deuterium;
    }

    public void setDuraniumProd(int duranium) {
        this.duranium = duranium;
    }

    public void setCrystalProd(int crystal) {
        this.crystal = crystal;
    }

    public void setIridiumProd(int iridium) {
        this.iridium = iridium;
    }

    public void setDeritiumProd(int deritium) {
        this.deritium = deritium;
    }

    public void setCreditsProd(int credits) {
        this.credits = credits;
    }

    public void setMoraleProd(int morale) {
        this.morale = morale;
    }

    public void setMoraleProdEmpire(int moraleEmpire) {
        this.moraleEmpire = moraleEmpire;
    }

    public void setFoodBonus(int foodBonus) {
        this.foodBonus = foodBonus;
    }

    public void setIndustryBonus(int industryBonus) {
        this.industryBonus = industryBonus;
    }

    public void setEnergyBonus(int energyBonus) {
        this.energyBonus = energyBonus;
    }

    public void setSecurityBonus(int securityBonus) {
        this.securityBonus = securityBonus;
    }

    public void setResearchBonus(int researchBonus) {
        this.researchBonus = researchBonus;
    }

    public void setTitanBonus(int titanBonus) {
        this.titanBonus = titanBonus;
    }

    public void setDeuteriumBonus(int deuteriumBonus) {
        this.deuteriumBonus = deuteriumBonus;
    }

    public void setDuraniumBonus(int duraniumBonus) {
        this.duraniumBonus = duraniumBonus;
    }

    public void setCrystalBonus(int crystalBonus) {
        this.crystalBonus = crystalBonus;
    }

    public void setIridiumBonus(int iridiumBonus) {
        this.iridiumBonus = iridiumBonus;
    }

    public void setDeritiumBonus(int deritiumBonus) {
        this.deritiumBonus = deritiumBonus;
    }

    public void setAllResourceBonus(int allResourceBonus) {
        this.allResourceBonus = allResourceBonus;
    }

    public void setCreditsBonus(int creditsBonus) {
        this.creditsBonus = creditsBonus;
    }

    public void setBioTechBonus(int bioTechBonus) {
        this.bioTechBonus = bioTechBonus;
    }

    public void setEnergyTechBonus(int energyTechBonus) {
        this.energyTechBonus = energyTechBonus;
    }

    public void setComputerTechBonus(int computerTechBonus) {
        this.computerTechBonus = computerTechBonus;
    }

    public void setPropulsionTechBonus(int propulsionTechBonus) {
        this.propulsionTechBonus = propulsionTechBonus;
    }

    public void setConstructionTechBonus(int constructionTechBonus) {
        this.constructionTechBonus = constructionTechBonus;
    }

    public void setWeaponTechBonus(int weaponTechBonus) {
        this.weaponTechBonus = weaponTechBonus;
    }

    public void setInnerSecurityBonus(int innerSecurityBonus) {
        this.innerSecurityBonus = innerSecurityBonus;
    }

    public void setEconomySpyBonus(int economySpyBonus) {
        this.economySpyBonus = economySpyBonus;
    }

    public void setEconomySabotageBonus(int economySabotageBonus) {
        this.economySabotageBonus = economySabotageBonus;
    }

    public void setResearchSpyBonus(int researchSpyBonus) {
        this.researchSpyBonus = researchSpyBonus;
    }

    public void setResearchSabotageBonus(int researchSabotageBonus) {
        this.researchSabotageBonus = researchSabotageBonus;
    }

    public void setMilitarySpyBonus(int militarySpyBonus) {
        this.militarySpyBonus = militarySpyBonus;
    }

    public void setMilitarySabogateBonus(int militarySabogateBonus) {
        this.militarySabogateBonus = militarySabogateBonus;
    }

    public void setShipYard(boolean shipYard) {
        this.shipyard = shipYard;
    }

    public void setBuildableShipSizes(ShipSize buildableShipSizes) {
        this.buildableShipSizes = buildableShipSizes;
    }

    public void setShipYardSpeed(int shipYardSpeed) {
        this.shipyardSpeed = shipYardSpeed;
    }

    public void setBarrack(boolean barrack) {
        this.barrack = barrack;
    }

    public void setBarrackSpeed(int barrackSpeed) {
        this.barrackSpeed = barrackSpeed;
    }

    public void setHitPoints(int hitPoints) {
        this.hitPoints = hitPoints;
    }

    public void setShieldPower(int shieldPower) {
        this.shieldPower = shieldPower;
    }

    public void setShieldPowerBonus(int shieldPowerBonus) {
        this.shieldPowerBonus = shieldPowerBonus;
    }

    public void setShipDefend(int shipDefend) {
        this.shipDefend = shipDefend;
    }

    public void setShipDefendBonus(int shipDefendBonus) {
        this.shipDefendBonus = shipDefendBonus;
    }

    public void setGroundDefend(int groundDefend) {
        this.groundDefend = groundDefend;
    }

    public void setGroundDefendBonus(int groundDefendBonus) {
        this.groundDefendBonus = groundDefendBonus;
    }

    public void setScanPower(int scanPower) {
        this.scanPower = scanPower;
    }

    public void setScanPowerBonus(int scanPowerBonus) {
        this.scanPowerBonus = scanPowerBonus;
    }

    public void setScanRange(int scanRange) {
        this.scanRange = scanRange;
    }

    public void setScanRangeBonus(int scanRangeBonus) {
        this.scanRangeBonus = scanRangeBonus;
    }

    public void setShipTraining(int shipTraining) {
        this.shipTraining = shipTraining;
    }

    public void setTroopTraining(int troopTraining) {
        this.troopTraining = troopTraining;
    }

    public void setResistance(int resistance) {
        this.resistance = resistance;
    }

    public void setAddedTradeRoutes(int addedTradeRoutes) {
        this.addedTradeRoutes = addedTradeRoutes;
    }

    public void setIncomeOnTradeRoutes(int incomeOnTradeRoutes) {
        this.incomeOnTradeRoutes = incomeOnTradeRoutes;
    }

    public void setShipRecycling(int shipRecycling) {
        this.shipRecycling = shipRecycling;
    }

    public void setBuildingBuildSpeed(int buildingBuildSpeed) {
        this.buildingBuildSpeed = buildingBuildSpeed;
    }

    public void setUpdateBuildSpeed(int updateBuildSpeed) {
        this.updateBuildSpeed = updateBuildSpeed;
    }

    public void setShipBuildSpeed(int shipBuildSpeed) {
        this.shipBuildSpeed = shipBuildSpeed;
    }

    public void setTroopBuildSpeed(int troopBuildSpeed) {
        this.troopBuildSpeed = troopBuildSpeed;
    }

    public void setWorker(boolean worker) {
        this.worker = worker;
    }

    public void setNeverReady(boolean neverReady) {
        this.neverReady = neverReady;
    }

    public void setAlwaysOnline(boolean alwaysOnline) {
        this.alwaysOnline = alwaysOnline;
    }

    public void setResourceDistributor(int res, boolean is) {
        resourceDistributor[res] = is;
    }

    public void setNeededSystems(int neededSystems) {
        this.neededSystems = neededSystems;
    }

    public String getProductionAsString() {
        return getProductionAsString(1);
    }

    public String getProductionAsString(int number) {
        String s = "";
        if (getFoodProd() > 0)
            s += StringDB.getString("FOOD") + ": " + getFoodProd() * number + "\n";
        if (getIndustryPointsProd() > 0)
            s += StringDB.getString("INDUSTRY") + ": " + getIndustryPointsProd() * number + "\n";
        if (getEnergyProd() > 0)
            s += StringDB.getString("ENERGY") + ": " + getEnergyProd() * number + "\n";
        if (getSecurityPointsProd() > 0)
            s += StringDB.getString("SECURITY") + ": " + getSecurityPointsProd() * number + "\n";
        if (getResearchPointsProd() > 0)
            s += StringDB.getString("RESEARCH") + ": " + getResearchPointsProd() * number + "\n";
        if (getTitanProd() > 0)
            s += StringDB.getString("TITAN") + ": " + getTitanProd() * number + "\n";
        if (getDeuteriumProd() > 0)
            s += StringDB.getString("DEUTERIUM") + ": " + getDeuteriumProd() * number + "\n";
        if (getDuraniumProd() > 0)
            s += StringDB.getString("DURANIUM") + ": " + getDuraniumProd() * number + "\n";
        if (getCrystalProd() > 0)
            s += StringDB.getString("CRYSTAL") + ": " + getCrystalProd() * number + "\n";
        if (getIridiumProd() > 0)
            s += StringDB.getString("IRIDIUM") + ": " + getIridiumProd() * number + "\n";
        if (getDeritiumProd() > 0)
            s += StringDB.getString("DERITIUM") + ": " + getDeritiumProd() * number + "\n";
        if (getCredits() != 0)
            s += StringDB.getString("CREDITS") + ": " + getCredits() * number + "\n";
        if (getMoraleProd() != 0)
            s += StringDB.getString("MORAL") + ": " + getMoraleProd() * number + "\n";
        if (getMoraleProdEmpire() != 0)
            s += StringDB.getString("MORAL_EMPIREWIDE") + ": " + getMoraleProdEmpire() * number + "\n";

        if (getFoodBonus() != 0)
            s += StringDB.getString("FOOD_BONUS") + ": " + getFoodBonus() * number + "%\n";
        if (getIndustryBonus() != 0)
            s += StringDB.getString("INDUSTRY_BONUS") + ": " + getIndustryBonus() * number + "%\n";
        if (getEnergyBonus() != 0)
            s += StringDB.getString("ENERGY_BONUS") + ": " + getEnergyBonus() * number + "%\n";
        if (getSecurityBonus() != 0)
            s += StringDB.getString("SECURITY_BONUS") + ": " + getSecurityBonus() * number + "%\n";
        if (getResearchBonus() != 0)
            s += StringDB.getString("RESEARCH_BONUS") + ": " + getResearchBonus() * number + "%\n";
        if (getTitanBonus() != 0)
            s += StringDB.getString("TITAN_BONUS") + ": " + getTitanBonus() * number + "%\n";
        if (getDeuteriumBonus() != 0)
            s += StringDB.getString("DEUTERIUM_BONUS") + ": " + getDeuteriumBonus() * number + "%\n";
        if (getDuraniumBonus() != 0)
            s += StringDB.getString("DURANIUM_BONUS") + ": " + getDuraniumBonus() * number + "%\n";
        if (getCrystalBonus() != 0)
            s += StringDB.getString("CRYSTAL_BONUS") + ": " + getCrystalBonus() * number + "%\n";
        if (getIridiumBonus() != 0)
            s += StringDB.getString("IRIDIUM_BONUS") + ": " + getIridiumBonus() * number + "%\n";
        if (getDeritiumBonus() != 0)
            s += StringDB.getString("DERITIUM_BONUS") + ": " + getDeritiumBonus() * number + "%\n";
        if (getAllResourceBonus() != 0)
            s += StringDB.getString("BONUS_TO_ALL_RES") + ": " + getAllResourceBonus() * number + "%\n";
        if (getCreditsBonus() != 0)
            s += StringDB.getString("CREDITS_BONUS") + ": " + getCreditsBonus() * number + "%\n";

        if (getBioTechBonus() != 0 && getBioTechBonus() == getEnergyTechBonus()
                && getBioTechBonus() == getComputerTechBonus() && getBioTechBonus() == getPropulsionTechBonus()
                && getBioTechBonus() == getConstructionTechBonus() && getBioTechBonus() == getWeaponTechBonus())
            s += StringDB.getString("TECHNIC_BONUS") + ": " + getBioTechBonus() * number + "%\n";
        else {
            if (getBioTechBonus() != 0)
                s += StringDB.getString("BIOTECH_BONUS") + ": " + getBioTechBonus() * number + "%\n";
            if (getEnergyTechBonus() != 0)
                s += StringDB.getString("ENERGYTECH_BONUS") + ": " + getEnergyTechBonus() * number + "%\n";
            if (getComputerTechBonus() != 0)
                s += StringDB.getString("COMPUTERTECH_BONUS") + ": " + getComputerTechBonus() * number + "%\n";
            if (getPropulsionTechBonus() != 0)
                s += StringDB.getString("PROPULSIONTECH_BONUS") + ": " + getPropulsionTechBonus() * number + "%\n";
            if (getConstructionTechBonus() != 0)
                s += StringDB.getString("CONSTRUCTIONTECH_BONUS") + ": " + getConstructionTechBonus() * number + "%\n";
            if (getWeaponTechBonus() != 0)
                s += StringDB.getString("WEAPONTECH_BONUS") + ": " + getWeaponTechBonus() * number + "%\n";
        }
        if (getInnerSecurityBonus() != 0 && getInnerSecurityBonus() == getEconomySpyBonus()
                && getInnerSecurityBonus() == getEconomySabotageBonus()
                && getInnerSecurityBonus() == getResearchSpyBonus()
                && getInnerSecurityBonus() == getResearchSabotageBonus()
                && getInnerSecurityBonus() == getMilitarySpyBonus()
                && getInnerSecurityBonus() == getMilitarySabogateBonus())
            s += StringDB.getString("COMPLETE_SECURITY_BONUS") + ": " + getInnerSecurityBonus() * number + "%\n";
        else {
            if (getEconomySpyBonus() != 0 && getEconomySpyBonus() == getResearchSpyBonus()
                    && getEconomySpyBonus() == getMilitarySpyBonus())
                s += StringDB.getString("SPY_BONUS") + ": " + getEconomySpyBonus() * number + "%\n";
            else {
                if (getEconomySpyBonus() != 0)
                    s += StringDB.getString("ECONOMY_SPY_BONUS") + ": " + getEconomySpyBonus() * number + "%\n";
                if (getResearchSpyBonus() != 0)
                    s += StringDB.getString("RESEARCH_SPY_BONUS") + ": " + getResearchSpyBonus() * number + "%\n";
                if (getMilitarySpyBonus() != 0)
                    s += StringDB.getString("MILITARY_SPY_BONUS") + ": " + getMilitarySpyBonus() * number + "%\n";
            }
            if (getEconomySabotageBonus() != 0 && getEconomySabotageBonus() == getResearchSabotageBonus()
                    && getEconomySabotageBonus() == getMilitarySabogateBonus())
                s += StringDB.getString("SABOTAGE_BONUS") + ": " + getEconomySabotageBonus() * number + "%\n";
            else {
                if (getEconomySabotageBonus() != 0)
                    s += StringDB.getString("ECONOMY_SABOTAGE_BONUS") + ": " + getEconomySabotageBonus() * number
                            + "%\n";
                if (getResearchSabotageBonus() != 0)
                    s += StringDB.getString("RESEARCH_SABOTAGE_BONUS") + ": " + getResearchSabotageBonus() * number
                            + "%\n";
                if (getMilitarySabogateBonus() != 0)
                    s += StringDB.getString("MILITARY_SABOTAGE_BONUS") + ": " + getMilitarySabogateBonus() * number
                            + "%\n";
            }
            if (getInnerSecurityBonus() != 0)
                s += StringDB.getString("INNER_SECURITY_BONUS") + ": " + getInnerSecurityBonus() * number + "%\n";
        }
        if (isShipYard())
            s += StringDB.getString("SHIPYARD") + "\n" + StringDB.getString("MAX_SHIPSIZE") + ": "
                    + StringDB.getString(getMaxBuildableShipSize().getName()) + "\n";
        if (getShipYardSpeed() != 0)
            s += StringDB.getString("SHIPYARD_EFFICIENCY") + ": " + getShipYardSpeed() + "%\n";
        if (isBarrack())
            s += StringDB.getString("BARRACK") + "\n";
        if (getBarrackSpeed() != 0)
            s += StringDB.getString("BARRACK_EFFICIENCY") + ": " + getBarrackSpeed() + "%\n";
        if (getShieldPower() != 0)
            s += StringDB.getString("SHIELDPOWER") + ": " + getShieldPower() * number + "\n";
        if (getShieldPowerBonus() != 0)
            s += StringDB.getString("SHIELDPOWER_BONUS") + ": " + getShieldPowerBonus() * number + "%\n";
        if (getShipDefend() != 0)
            s += StringDB.getString("SHIPDEFEND") + ": " + getShipDefend() * number + "\n";
        if (getShipDefendBonus() != 0)
            s += StringDB.getString("SHIPDEFEND_BONUS") + ": " + getShipDefendBonus() * number + "%\n";
        if (getGroundDefend() != 0)
            s += StringDB.getString("GROUNDDEFEND") + ": " + getGroundDefend() * number + "\n";
        if (getGroundDefendBonus() != 0)
            s += StringDB.getString("GROUNDDEFEND_BONUS") + ": " + getGroundDefendBonus() * number + "%\n";
        if (getScanPower() != 0)
            s += StringDB.getString("SCANPOWER") + ": " + getScanPower() * number + "\n";
        if (getScanPowerBonus() != 0)
            s += StringDB.getString("SCANPOWER_BONUS") + ": " + getScanPowerBonus() * number + "%\n";
        if (getScanRange() != 0)
            s += StringDB.getString("SCANRANGE") + ": " + getScanRange() * number + "\n";
        if (getScanRangeBonus() != 0)
            s += StringDB.getString("SCANRANGE_BONUS") + ": " + getScanRangeBonus() * number + "%\n";
        if (getShipTraining() != 0)
            s += StringDB.getString("SHIPTRAINING") + ": " + getShipTraining() * number + "\n";
        if (getTroopTraining() != 0)
            s += StringDB.getString("TROOPTRAINING") + ": " + getTroopTraining() * number + "\n";
        if (getResistance() != 0)
            s += StringDB.getString("RESISTANCE") + ": " + getResistance() * number + "\n";
        if (getAddedTradeRoutes() != 0)
            s += StringDB.getString("ADDED_TRADEROUTES") + ": " + getAddedTradeRoutes() * number + "\n";
        if (getIncomeOnTradeRoutes() != 0)
            s += StringDB.getString("INCOME_ON_TRADEROUTES") + ": " + getIncomeOnTradeRoutes() * number + "%\n";
        if (getShipRecycling() != 0)
            s += StringDB.getString("SHIPRECYCLING") + ": " + getShipRecycling() * number + "%\n";
        if (getBuildingBuildSpeed() != 0)
            s += StringDB.getString("BUILDING_BUILDSPEED") + ": " + getBuildingBuildSpeed() * number + "%\n";
        if (getUpdateBuildSpeed() != 0)
            s += StringDB.getString("UPGRADE_BUILDSPEED") + ": " + getUpdateBuildSpeed() * number + "%\n";
        if (getShipBuildSpeed() != 0)
            s += StringDB.getString("SHIP_BUILDSPEED") + ": " + getShipBuildSpeed() * number + "%\n";
        if (getTroopBuildSpeed() != 0)
            s += StringDB.getString("TROOP_BUILDSPEED") + ": " + getTroopBuildSpeed() * number + "%\n";
        for (int i = 0; i < ResourceTypes.DERITIUM.getType() + 1; i++) {
            if (getResourceDistributor(i))
                s += StringDB.getString("RESOURCE_DISTRIBUTOR") + " - "
                        + StringDB.getString(ResourceTypes.fromResourceTypes(i).getName()) + "\n";
        }
        return s.trim();
    }

    public String getPrerequisitesAsString() {
        String s = "";
        if (getNeededSystems() != 0)
            s += StringDB.getString("NEEDED_SYSTEMS") + ": " + getNeededSystems() + "\n";
        if (getMaxInEmpire() > 0) {
            if (getMaxInEmpire() == 1)
                s += StringDB.getString("ONCE_PER_EMPIRE") + "\n";
            else
                s += StringDB.getString("MAX_PER_EMPIRE", false, "" + getMaxInEmpire()) + "\n";
        }

        if (getMaxInSystem().number > 0 && getMaxInSystem().runningNumber == getRunningNumber()) {
            if (getMaxInSystem().number == 1)
                s += StringDB.getString("ONCE_PER_SYSTEM") + "\n";
            else
                s += StringDB.getString("MAX_PER_SYSTEM", false, "" + getMaxInSystem().number) + "\n";
        } else {
            if (getMaxInSystem().number > 0 && getMaxInSystem().runningNumber != getRunningNumber())
                s += StringDB.getString("MAX_ID_PER_SYSTEM", false, "" + getMaxInSystem().number) + "\n";
        }
        if (getMinInSystem().number > 0)
            s += StringDB.getString("MIN_PER_SYSTEM", false, "" + getMinInSystem().number) + "\n";
        if (isOnlyHomePlanet())
            s += StringDB.getString("ONLY_HOMEPLANET") + "\n";
        if (isOnlyOwnColony())
            s += StringDB.getString("ONLY_OWN_COLONY") + "\n";
        if (isOnlyMinorRace())
            s += StringDB.getString("ONLY_MINORRACE_SYSTEM") + "\n";
        if (!onlyInSystemWithName.isEmpty() && !onlyInSystemWithName.equals("0"))
            s += onlyInSystemWithName + "\n";
        if (isOnlyTakenSystem())
            s += StringDB.getString("ONLY_TAKEN_SYSTEM") + "\n";
        if (getMinInhabitants() != 0)
            s += StringDB.getString("NEED_MIN_HABITANTS", false, "" + getMinInhabitants()) + "\n";
        if (getNeededEnergy() != 0)
            s += StringDB.getString("ENERGY") + ": " + getNeededEnergy() + "\n";
        if (getWorker())
            s += StringDB.getString("NEED_WORKER") + "\n";

        if(!getPlanetTypes(PlanetType.PLANETCLASS_M.getType())) //if it can't be built on M-class then it has to be some special building
            for(int i = 1; i < PlanetType.values().length - 1; i++) {
                PlanetType pt = PlanetType.values()[i];
                if(getPlanetTypes(pt.getType())) {
                    s += pt.getTypeName() + " - " + StringDB.getString(pt.getDBName()) + "\n";
                }
            }
        return s.trim();
    }

    public void drawBuildingInfo(Table table, Skin skin, Color normalColor, Color markColor, float width, float vpad) {
        Label nameLabel = new Label(getBuildingName(), skin, "normalFont", markColor);
        nameLabel.setWrap(true);
        nameLabel.setAlignment(Align.center);
        table.add(nameLabel).width(width).spaceBottom(vpad);
        table.row();
        Label prodLabel = new Label(StringDB.getString("PRODUCTION") + ":", skin, "normalFont", markColor);
        prodLabel.setWrap(true);
        prodLabel.setAlignment(Align.center);
        table.add(prodLabel).width(width);
        table.row();
        String text = getProductionAsString();
        Label prodInfoLabel = new Label(text, skin, "normalFont", normalColor);
        prodInfoLabel.setWrap(true);
        prodInfoLabel.setAlignment(Align.center);
        table.add(prodInfoLabel).width(width);
        table.row();
        Label prereqLabel = new Label(StringDB.getString("PREREQUISITES") + ":", skin, "normalFont", markColor);
        prereqLabel.setWrap(true);
        prereqLabel.setAlignment(Align.center);
        table.add(prereqLabel).width(width);
        table.row();
        text = getPrerequisitesAsString();
        Label prereqTextLabel = new Label(text, skin, "normalFont", normalColor);
        prereqTextLabel.setWrap(true);
        prereqTextLabel.setAlignment(Align.center);
        table.add(prereqTextLabel).width(width);
    }

    public boolean isBuildingBuildableNow(int[] researchLevels) {
        if (researchLevels[ResearchType.BIO.getType()] < getBioTech())
            return false;
        if (researchLevels[ResearchType.ENERGY.getType()] < getEnergyTech())
            return false;
        if (researchLevels[ResearchType.COMPUTER.getType()] < getComputerTech())
            return false;
        if (researchLevels[ResearchType.PROPULSION.getType()] < getPropulsionTech())
            return false;
        if (researchLevels[ResearchType.CONSTRUCTION.getType()] < getConstructionTech())
            return false;
        if (researchLevels[ResearchType.WEAPON.getType()] < getWeaponTech())
            return false;
        return true;
    }

    public boolean isDefenseBuilding() {
        return shieldPower > 0 || shieldPowerBonus > 0 || groundDefend > 0 || groundDefendBonus > 0 || shipDefend > 0
                || shipDefendBonus > 0;
    }

    public boolean isResearchBuilding() {
        return researchPoints > 0 || researchBonus > 0 || bioTechBonus > 0 || energyTechBonus > 0
                || computerTechBonus > 0 || propulsionTechBonus > 0 || constructionTechBonus > 0 || weaponTechBonus > 0;
    }

    public boolean isDevelopmentBuilding() {
        return isUsefulMorale() || credits > 0 || creditsBonus > 0 || industryPoints > 0 || industryBonus > 0
                || resistance > 0 || buildingBuildSpeed > 0 || updateBuildSpeed > 0 || shipRecycling > 0
                || incomeOnTradeRoutes > 0|| addedTradeRoutes > 0;
    }

    public boolean isIntelBuilding() {
        return securityPoints > 0 || securityBonus > 0 || innerSecurityBonus > 0
                || researchSpyBonus > 0 || researchSabotageBonus > 0 || militarySpyBonus > 0 || militarySabogateBonus > 0
                || economySpyBonus > 0 || economySabotageBonus > 0;
    }

    public boolean isStrategicBuilding() {
        return isDefenseBuilding() || isBarrack() || isShipYard() || isDeritiumRefinery() || isIntelBuilding()
                || shipTraining > 0 || troopTraining > 0 || shipBuildSpeed > 0 || deritiumBonus > 0 || resourceDistributor[ResourceTypes.DERITIUM.getType()]
                || scanPower > 0 || scanPowerBonus > 0 || scanRangeBonus > 0;
    }

    public boolean isEnergyBuilding() {
        return energy > 0 || energyBonus > 0;
    }

    public boolean isFoodBuilding() {
        return food > 0 || foodBonus > 0;
    }

    public boolean isResourceBuilding() {
        boolean ok = false;
        for (int i = 0; i < resourceDistributor.length; i++)
            if(i != ResourceTypes.DERITIUM.getType()) //deritium distribution facility is a strategic building
                ok |= resourceDistributor[i];
        return ok || titan > 0 || titanBonus > 0 || deuterium > 0 || deuteriumBonus > 0 || duranium > 0 || duraniumBonus > 0
                || crystal > 0 || crystalBonus > 0 || iridium > 0 || iridiumBonus > 0;
    }

    public boolean onlyImprovesProduction(boolean minusMoral) {
        boolean ok = minusMoral || morale >= 0;
        return ok && allResourceBonus >= 0 && moraleEmpire >= 0 && foodBonus >= 0 && industryBonus >= 0
                && energyBonus >= 0 && securityBonus >= 0 && researchBonus >= 0 && titanBonus >= 0
                && deuteriumBonus >= 0 && duraniumBonus >= 0 && crystalBonus >= 0 && iridiumBonus >= 0
                && deritiumBonus >= 0 && creditsBonus >= 0 && innerSecurityBonus >= 0 && economySpyBonus >= 0
                && economySabotageBonus >= 0 && researchSpyBonus >= 0 && researchSabotageBonus >= 0
                && militarySpyBonus >= 0 && militarySabogateBonus >= 0 && bioTechBonus >= 0 && energyTechBonus >= 0
                && computerTechBonus >= 0 && propulsionTechBonus >= 0 && constructionTechBonus >= 0
                && weaponTechBonus >= 0 && scanPowerBonus >= 0 && scanRangeBonus >= 0 && incomeOnTradeRoutes >= 0
                && shipRecycling >= 0 && buildingBuildSpeed >= 0 && updateBuildSpeed >= 0 && shipBuildSpeed >= 0
                && troopBuildSpeed >= 0;
    }

    public WorkerType producesWorkerFull() {
        if (food > 0)
            return WorkerType.FOOD_WORKER;
        if (industryPoints > 0)
            return WorkerType.INDUSTRY_WORKER;
        if (energy > 0)
            return WorkerType.ENERGY_WORKER;
        if (securityPoints > 0)
            return WorkerType.SECURITY_WORKER;
        if (researchPoints > 0)
            return WorkerType.RESEARCH_WORKER;
        if (titan > 0)
            return WorkerType.TITAN_WORKER;
        if (deuterium > 0)
            return WorkerType.DEUTERIUM_WORKER;
        if (duranium > 0)
            return WorkerType.DURANIUM_WORKER;
        if (crystal > 0)
            return WorkerType.CRYSTAL_WORKER;
        if (iridium > 0)
            return WorkerType.IRIDIUM_WORKER;
        return WorkerType.NONE;
    }

    public boolean isDeritiumRefinery() {
        return deritium > 0;
    }

    private boolean producesWorkerLess() {
        boolean does = credits > 0 || scanPower > 0 || scanPowerBonus > 0 || scanRangeBonus > 0 || addedTradeRoutes > 0
                || incomeOnTradeRoutes > 0 || shipRecycling > 0 || buildingBuildSpeed > 0 || updateBuildSpeed > 0
                || shipBuildSpeed > 0 || troopBuildSpeed > 0;
        if (does)
            return true;
        for (int i = 0; i < resourceDistributor.length; i++)
            if (resourceDistributor[i])
                return true;
        return false;
    }

    private boolean improvesProduction() {
        return allResourceBonus > 0 || foodBonus > 0 || industryBonus > 0 || energyBonus > 0 || securityBonus > 0
                || researchBonus > 0 || titanBonus > 0 || deuteriumBonus > 0 || duraniumBonus > 0 || crystalBonus > 0
                || iridiumBonus > 0 || deritiumBonus > 0 || creditsBonus > 0 || innerSecurityBonus > 0
                || economySpyBonus > 0 || economySabotageBonus > 0 || researchSpyBonus > 0 || researchSabotageBonus > 0
                || militarySpyBonus > 0 || militarySabogateBonus > 0 || bioTechBonus > 0 || energyTechBonus > 0
                || computerTechBonus > 0 || propulsionTechBonus > 0 || constructionTechBonus > 0 || weaponTechBonus > 0;
    }

    public boolean isUsefulForProduction() {
        return improvesProduction() || producesWorkerLess();
    }

    public boolean isUsefulMorale() {
        if (moraleEmpire < 0)
            return false;
        return moraleEmpire > 0 || morale > 0;
    }

    public int getXProd(WorkerType x) {
        switch (x) {
            case FOOD_WORKER:
                return food;
            case INDUSTRY_WORKER:
                return industryPoints;
            case ENERGY_WORKER:
                return energy;
            case SECURITY_WORKER:
                return securityPoints;
            case RESEARCH_WORKER:
                return researchPoints;
            case TITAN_WORKER:
                return titan;
            case DEUTERIUM_WORKER:
                return deuterium;
            case DURANIUM_WORKER:
                return duranium;
            case CRYSTAL_WORKER:
                return crystal;
            case IRIDIUM_WORKER:
                return iridium;
            default:
                return 0;
        }
    }

    public int getXProd(ResourceTypes x) {
        switch (x) {
            case FOOD:
                return food;
            case TITAN:
                return titan;
            case DEUTERIUM:
                return deuterium;
            case DURANIUM:
                return duranium;
            case IRIDIUM:
                return iridium;
            case DERITIUM:
                return deritium;
            default:
                return 0;
        }
    }

    public int getNeededTechLevel(ResearchType type) {
        switch(type) {
            case BIO:
                return bioTech;
            case ENERGY:
                return energyTech;
            case COMPUTER:
                return computerTech;
            case PROPULSION:
                return propulsionTech;
            case CONSTRUCTION:
                return constructionTech;
            case WEAPON:
                return weaponTech;
            default:
                return -1;
        }
    }

    public void getTooltip(Table table, Skin skin, String headerFont, Color headerColor, String textFont, Color textColor) {
        table.clearChildren();
        String text = getBuildingName();
        Label l = new Label(text, skin, headerFont, headerColor);
        table.add(l);
        table.row();

        text = getProductionAsString();
        l = new Label(text, skin, textFont, headerColor);
        l.setWrap(true);
        l.setAlignment(Align.center);
        table.add(l).width(GameConstants.wToRelative(400));
        table.row();

        text = getBuildingDescription();
        l = new Label(text, skin, textFont, textColor);
        l.setWrap(true);
        l.setAlignment(Align.center);
        table.add(l).width(GameConstants.wToRelative(400));
        table.row();
    }

    public void setNeededResource(ResourceTypes rt, int value) {
        switch (rt) {
            case TITAN:
                setNeededTitan(value);
                break;
            case DEUTERIUM:
                setNeededDeuterium(value);
                break;
            case DURANIUM:
                setNeededDuranium(value);
                break;
            case CRYSTAL:
                setNeededCrystal(value);
                break;
            case IRIDIUM:
                setNeededIridium(value);
                break;
            case DERITIUM:
                setNeededDeritium(value);
                break;
            default:
                break;
        }
    }

    public void setNeededResearch(ResearchType rt, int value) {
        switch (rt) {
            case BIO:
                this.bioTech = value;
                break;
            case ENERGY:
                this.energyTech = value;
                break;
            case COMPUTER:
                this.computerTech = value;
                break;
            case PROPULSION:
                this.propulsionTech = value;
                break;
            case CONSTRUCTION:
                this.constructionTech = value;
                break;
            case WEAPON:
                this.weaponTech = value;
                break;
            default:
                break;
        }
    }

    public int getProductionByWorkers(WorkerType wt) {
        switch (wt) {
            case FOOD_WORKER:
                return food;
            case INDUSTRY_WORKER:
                return industryPoints;
            case ENERGY_WORKER:
                return energy;
            case SECURITY_WORKER:
                return securityPoints;
            case RESEARCH_WORKER:
                return researchPoints;
            case TITAN_WORKER:
                return titan;
            case DEUTERIUM_WORKER:
                return deuterium;
            case DURANIUM_WORKER:
                return duranium;
            case CRYSTAL_WORKER:
                return crystal;
            case IRIDIUM_WORKER:
                return iridium;
            default:
                return 0;
        }
    }

    public void setProductionByWorkers(WorkerType wt, int value) {
        switch (wt) {
            case FOOD_WORKER:
                food = value;
                break;
            case INDUSTRY_WORKER:
                industryPoints = value;
                break;
            case ENERGY_WORKER:
                energy = value;
                break;
            case SECURITY_WORKER:
                securityPoints = value;
                break;
            case RESEARCH_WORKER:
                researchPoints = value;
                break;
            case TITAN_WORKER:
                titan = value;
                break;
            case DEUTERIUM_WORKER:
                deuterium = value;
                break;
            case DURANIUM_WORKER:
                duranium = value;
                break;
            case CRYSTAL_WORKER:
                crystal = value;
                break;
            case IRIDIUM_WORKER:
                iridium = value;
                break;
            default:
                break;
        }
    }

    public int getProductionBonusByWorkers(WorkerType wt) {
        switch (wt) {
            case FOOD_WORKER:
                return foodBonus;
            case INDUSTRY_WORKER:
                return industryBonus;
            case ENERGY_WORKER:
                return energyBonus;
            case SECURITY_WORKER:
                return securityBonus;
            case RESEARCH_WORKER:
                return researchBonus;
            case TITAN_WORKER:
                return titanBonus;
            case DEUTERIUM_WORKER:
                return deuteriumBonus;
            case DURANIUM_WORKER:
                return duraniumBonus;
            case CRYSTAL_WORKER:
                return crystalBonus;
            case IRIDIUM_WORKER:
                return iridiumBonus;
            default:
                return 0;
        }
    }

    public void setProductionBonusByWorkers(WorkerType wt, int value) {
        switch (wt) {
            case FOOD_WORKER:
                foodBonus = value;
                break;
            case INDUSTRY_WORKER:
                industryBonus = value;
                break;
            case ENERGY_WORKER:
                energyBonus = value;
                break;
            case SECURITY_WORKER:
                securityBonus = value;
                break;
            case RESEARCH_WORKER:
                researchBonus = value;
                break;
            case TITAN_WORKER:
                titanBonus = value;
                break;
            case DEUTERIUM_WORKER:
                deuteriumBonus = value;
                break;
            case DURANIUM_WORKER:
                duraniumBonus = value;
                break;
            case CRYSTAL_WORKER:
                crystalBonus = value;
                break;
            case IRIDIUM_WORKER:
                iridiumBonus = value;
                break;
            default:
                break;
        }
    }

    public int getTechBonus(ResearchType rt) {
        switch (rt) {
            case BIO:
                return bioTechBonus;
            case ENERGY:
                return energyTechBonus;
            case COMPUTER:
                return computerTechBonus;
            case PROPULSION:
                return propulsionTechBonus;
            case CONSTRUCTION:
                return constructionTechBonus;
            case WEAPON:
                return weaponTechBonus;
            default:
                return 0;
        }
    }

    public void setTechBonus(ResearchType rt, int value) {
        switch (rt) {
            case BIO:
                bioTechBonus = value;
                break;
            case ENERGY:
                energyTechBonus = value;
                break;
            case COMPUTER:
                computerTechBonus = value;
                break;
            case PROPULSION:
                propulsionTechBonus = value;
                break;
            case CONSTRUCTION:
                constructionTechBonus = value;
                break;
            case WEAPON:
                weaponTechBonus = value;
                break;
            default:
                break;
        }
    }
}