/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.starsystem;

import java.util.Arrays;

import com.blotunga.bote.constants.ResourceTypes;
import com.blotunga.bote.constants.ShipSize;
import com.blotunga.bote.constants.WorkerType;

public class SystemProd {
    public class ResearchBonus {
        public int[] bonuses;

        public ResearchBonus() {
            this(0, 0, 0, 0, 0, 0);
        }

        public ResearchBonus(int bio, int energy, int computer, int propulsion, int construction, int weapon) {
            bonuses = new int[6];
            bonuses[0] = bio;
            bonuses[1] = energy;
            bonuses[2] = computer;
            bonuses[3] = propulsion;
            bonuses[4] = construction;
            bonuses[5] = weapon;
        }

        public void add(ResearchBonus other) {
            for (int i = 0; i < bonuses.length; i++)
                bonuses[i] += other.bonuses[i];
        }
    }

    int foodProd;				///< food production
    int maxFoodProd;			///< food production without considering population
    int industryProd;			///< industrial production
    int potentialIndustryProd;	///< industrial production without considering population
    int energyProd;				///< energy production of the system
    int maxEnergyProd;			///< energy production without the energy requirements of some buildings
    int potentialEnergyProd;	///< all energy buildings filled as far as workers available;
    int securityProd;			///< security production of the system
    int researchProd;			///< research production of the system
    int titanProd;
    int deuteriumProd;
    int duraniumProd;
    int crystalProd;
    int iridiumProd;
    int deritiumProd;
    int creditsProd;
    int moraleProd;
    // Technology bonuses
    int bioTechBonus;
    int energyTechBonus;
    int computerTechBonus;
    int propulsionTechBonus;
    int constructionTechBonus;
    int weaponTechBonus;
    // security bonuses
    int innerSecurityBonus;
    int economySpyBonus;
    int economySabotageBonus;
    int researchSpyBonus;
    int researchSabotageBonus;
    int militarySpyBonus;
    int militarySabotageBonus;
    // misc info
    boolean shipyard;
    ShipSize buildableShipSizes;
    int shipYardEfficiency;
    boolean barrack;
    int barrackEfficiency;
    int shieldPower;
    int shieldPowerBonus;
    int shipDefend;
    int shipDefendBonus;
    int groundDefend;
    int groundDefendBonus;
    int scanPower;
    int scanPowerBonus;
    int scanRange;
    int scanRangeBonus;
    int shipTraining;
    int troopTraining;
    int resistance;
    int addedTradeRoutes;
    int incomeOnTradeRoutes;
    int shipRecycling;
    int buildingBuildSpeed;
    int updateBuildSpeed;
    int shipBuildSpeed;
    int troopBuildSpeed;
    boolean[] resourceDistributor;

    public SystemProd() {
        potentialIndustryProd = 0;
        potentialEnergyProd = 0;
        reset();
    }

    public void reset() {
        foodProd = 10;
        maxFoodProd = 0;
        industryProd = 5;
        potentialIndustryProd = industryProd;
        energyProd = 0;
        maxEnergyProd = 0;
        potentialEnergyProd = energyProd;
        securityProd = 0;
        researchProd = 0;
        titanProd = 0;
        deuteriumProd = 0;
        duraniumProd = 0;
        crystalProd = 0;
        iridiumProd = 0;
        deritiumProd = 0;
        creditsProd = 0;
        moraleProd = 0;
        //research bonuses
        bioTechBonus = 0;
        energyTechBonus = 0;
        computerTechBonus = 0;
        propulsionTechBonus = 0;
        constructionTechBonus = 0;
        weaponTechBonus = 0;
        //security bonuses
        innerSecurityBonus = 0;
        economySpyBonus = 0;
        economySabotageBonus = 0;
        researchSpyBonus = 0;
        researchSabotageBonus = 0;
        militarySpyBonus = 0;
        militarySabotageBonus = 0;
        //other
        shipyard = false;
        buildableShipSizes = ShipSize.HUGE;
        shipYardEfficiency = 0;
        barrack = false;
        barrackEfficiency = 0;
        shieldPower = 0;
        shieldPowerBonus = 0;
        shipDefend = 0;
        shipDefendBonus = 0;
        groundDefend = 0;
        groundDefendBonus = 0;
        scanPower = 0;
        scanPowerBonus = 0;
        scanRange = 0;
        scanRangeBonus = 0;
        shipTraining = 0;
        troopTraining = 0;
        resistance = 0;
        addedTradeRoutes = 0;
        incomeOnTradeRoutes = 0;
        shipRecycling = 0;
        buildingBuildSpeed = 0;
        updateBuildSpeed = 0;
        shipBuildSpeed = 0;
        troopBuildSpeed = 0;
        resourceDistributor = new boolean[ResourceTypes.DERITIUM.getType() + 1];
        Arrays.fill(resourceDistributor, false);
    }

    public int getFoodProd() {
        return foodProd;
    }

    public int getMaxFoodProd() {
        return maxFoodProd;
    }

    public int getIndustryProd() {
        return industryProd;
    }

    public int getPotentialIndustryProd() {
        return potentialIndustryProd;
    }

    public int getEnergyProd() {
        return energyProd;
    }

    public int getMaxEnergyProd() {
        return maxEnergyProd;
    }

    public int getPotentialEnergyProd() {
        return potentialEnergyProd;
    }

    public int getEnergyConsumption() {
        return maxEnergyProd - energyProd;
    }

    public int getAvailableEnergy() {
        return potentialEnergyProd - getEnergyConsumption();
    }

    public int getSecurityProd() {
        return securityProd;
    }

    public int getResearchProd() {
        return researchProd;
    }

    public int getTitanProd() {
        return titanProd;
    }

    public int getDeuteriumProd() {
        return deuteriumProd;
    }

    public int getDuraniumProd() {
        return duraniumProd;
    }

    public int getCrystalProd() {
        return crystalProd;
    }

    public int getIridiumProd() {
        return iridiumProd;
    }

    public int getDeritiumProd() {
        return deritiumProd;
    }

    public int getCreditsProd() {
        return creditsProd;
    }

    public int getMoraleProd() {
        return moraleProd;
    }

    public int getBioTechBonus() {
        return bioTechBonus;
    }

    public int getEnergyTechBonus() {
        return energyTechBonus;
    }

    public int getComputerTechBonus() {
        return computerTechBonus;
    }

    public int getPropulsionTechBonus() {
        return propulsionTechBonus;
    }

    public int getConstructionTechBonus() {
        return constructionTechBonus;
    }

    public int getWeaponTechBonus() {
        return weaponTechBonus;
    }

    public int getInnerSecurityBonus() {
        return innerSecurityBonus;
    }

    public int getEconomySpyBonus() {
        return economySpyBonus;
    }

    public int getEconomySabotageBonus() {
        return economySabotageBonus;
    }

    public int getResearchSpyBonus() {
        return researchSpyBonus;
    }

    public int getResearchSabotageBonus() {
        return researchSabotageBonus;
    }

    public int getMilitarySpyBonus() {
        return militarySpyBonus;
    }

    public int getMilitarySabotageBonus() {
        return militarySabotageBonus;
    }

    public boolean isShipyard() {
        return shipyard;
    }

    public ShipSize getMaxBuildableShipSize() {
        return buildableShipSizes;
    }

    public int getShipYardEfficiency() {
        return shipYardEfficiency;
    }

    public boolean isBarrack() {
        return barrack;
    }

    public int getBarrackEfficiency() {
        return barrackEfficiency;
    }

    public int getShieldPower() {
        return shieldPower;
    }

    public int getShieldPowerBonus() {
        return shieldPowerBonus;
    }

    public int getShipDefend() {
        return shipDefend;
    }

    public int getShipDefendBonus() {
        return shipDefendBonus;
    }

    public int getGroundDefend() {
        return groundDefend;
    }

    public int getGroundDefendBonus() {
        return groundDefendBonus;
    }

    public int getScanPower() {
        return scanPower;
    }

    public int getScanPowerBonus() {
        return scanPowerBonus;
    }

    public int getScanRange() {
        return scanRange;
    }

    public int getScanRangeBonus() {
        return scanRangeBonus;
    }

    public int getShipTraining() {
        return shipTraining;
    }

    public int getTroopTraining() {
        return troopTraining;
    }

    public int getResistance() {
        return resistance;
    }

    public int getAddedTradeRoutes() {
        return addedTradeRoutes;
    }

    public int getIncomeOnTradeRoutes() {
        return incomeOnTradeRoutes;
    }

    public int getShipRecycling() {
        return shipRecycling;
    }

    public int getBuildingBuildSpeed() {
        return buildingBuildSpeed;
    }

    public int getUpdateBuildSpeed() {
        return updateBuildSpeed;
    }

    public int getShipBuildSpeed() {
        return shipBuildSpeed;
    }

    public int getTroopBuildSpeed() {
        return troopBuildSpeed;
    }

    public boolean getResourceDistributor(int res) {
        return resourceDistributor[res];
    }

    public int getXProd(WorkerType x) {
        switch (x) {
            case FOOD_WORKER:
                return foodProd;
            case INDUSTRY_WORKER:
                return industryProd;
            case ENERGY_WORKER:
                return energyProd;
            case SECURITY_WORKER:
                return securityProd;
            case RESEARCH_WORKER:
                return researchProd;
            case TITAN_WORKER:
                return titanProd;
            case DEUTERIUM_WORKER:
                return deuteriumProd;
            case DURANIUM_WORKER:
                return duraniumProd;
            case CRYSTAL_WORKER:
                return crystalProd;
            case IRIDIUM_WORKER:
                return iridiumProd;
            default:
                return 0;
        }
    }

    public int getXProdMax(WorkerType x) {
        switch (x) {
            case FOOD_WORKER:
                return maxFoodProd;
            case ENERGY_WORKER:
                return maxEnergyProd;
            default:
                return getXProd(x);
        }
    }

    public ResearchBonus getResearchBonus() {
        return new ResearchBonus(bioTechBonus, energyTechBonus, computerTechBonus, propulsionTechBonus,
                constructionTechBonus, weaponTechBonus);
    }

    public int getResourceProd(ResourceTypes rt) {
        switch (rt) {
            case FOOD:
                return foodProd;
            case TITAN:
                return titanProd;
            case DEUTERIUM:
                return deuteriumProd;
            case DURANIUM:
                return duraniumProd;
            case CRYSTAL:
                return crystalProd;
            case IRIDIUM:
                return iridiumProd;
            case DERITIUM:
                return deritiumProd;
        }
        return 0;
    }

    /**
     * Function returns the industrial production based on efficiency
     * @param type - 0 - building, 1 - ships, 2 - troops
     * @return actual production
     */
    public int getIndustrialProductionByType(int type) {
        if (type == 0) {
            if (updateBuildSpeed == buildingBuildSpeed)
                return industryProd * (100 + buildingBuildSpeed) / 100;
            else
                return industryProd;
        } else if (type == 1) {
            return industryProd * shipYardEfficiency / 100 * (100 + shipBuildSpeed) / 100;
        } else {
            return industryProd * barrackEfficiency / 100 * (100 + troopBuildSpeed) / 100;
        }
    }

    public void addMoraleProd(int moraleAdd) {
        moraleProd += moraleAdd;
    }

    public void calculateProduction(BuildingInfo buildingInfo, boolean isOnline) {
        calculateProduction(buildingInfo, isOnline, false, false);
    }

    public void calculateProduction(BuildingInfo building, boolean isOnline, boolean indPot, boolean enPot) {
        if (indPot)
            potentialIndustryProd += building.getIndustryPointsProd();
        if (enPot)
            potentialEnergyProd += building.getEnergyProd();
        if (!isOnline)
            return;
        foodProd += building.getFoodProd();
        industryProd += building.getIndustryPointsProd();
        energyProd += building.getEnergyProd();
        securityProd += building.getSecurityPointsProd();
        researchProd += building.getResearchPointsProd();
        titanProd += building.getTitanProd();
        deuteriumProd += building.getDeuteriumProd();
        duraniumProd += building.getDuraniumProd();
        crystalProd += building.getCrystalProd();
        iridiumProd += building.getIridiumProd();
        deritiumProd += building.getDeritiumProd();
        creditsProd += building.getCredits();
        moraleProd += building.getMoraleProd();
        //tech bonuses
        bioTechBonus += building.getBioTechBonus();
        energyTechBonus += building.getEnergyTechBonus();
        computerTechBonus += building.getComputerTechBonus();
        propulsionTechBonus += building.getPropulsionTechBonus();
        constructionTechBonus += building.getConstructionTechBonus();
        weaponTechBonus += building.getWeaponTechBonus();
        //security bonuses
        innerSecurityBonus += building.getInnerSecurityBonus();
        economySpyBonus += building.getEconomySpyBonus();
        economySabotageBonus += building.getEconomySabotageBonus();
        researchSpyBonus += building.getResearchSpyBonus();
        researchSabotageBonus += building.getResearchSabotageBonus();
        militarySpyBonus += building.getMilitarySpyBonus();
        militarySabotageBonus += building.getMilitarySabogateBonus();
        shipyard |= building.isShipYard();
        if (building.getMaxBuildableShipSize().getSize() > buildableShipSizes.getSize())
            buildableShipSizes = building.getMaxBuildableShipSize();
        if (building.getShipYardSpeed() > shipYardEfficiency)
            shipYardEfficiency = building.getShipYardSpeed();
        barrack |= building.isBarrack();
        if (building.getBarrackSpeed() > barrackEfficiency)
            barrackEfficiency = building.getBarrackSpeed();
        shieldPower += building.getShieldPower();
        shieldPowerBonus += building.getShieldPowerBonus();
        shipDefend += building.getShipDefend();
        shipDefendBonus += building.getShipDefendBonus();
        groundDefend += building.getGroundDefend();
        groundDefendBonus += building.getGroundDefendBonus();
        if (building.getScanPower() > scanPower)
            scanPower = building.getScanPower();
        scanPowerBonus += building.getScanPowerBonus();
        if (building.getScanRange() > scanRange)
            scanRange = building.getScanRange();
        scanRangeBonus += building.getScanRangeBonus();
        shipTraining += building.getShipTraining();
        troopTraining += building.getTroopTraining();
        resistance += building.getResistance();
        addedTradeRoutes += building.getAddedTradeRoutes();
        incomeOnTradeRoutes += building.getIncomeOnTradeRoutes();
        shipRecycling += building.getShipRecycling();
        buildingBuildSpeed += building.getBuildingBuildSpeed();
        updateBuildSpeed += building.getUpdateBuildSpeed();
        shipBuildSpeed += building.getShipBuildSpeed();
        troopBuildSpeed += building.getTroopBuildSpeed();
        for (int res = ResourceTypes.TITAN.getType(); res <= ResourceTypes.DERITIUM.getType(); res++)
            resourceDistributor[res] |= building.getResourceDistributor(res);
    }

    public void disableProduction(boolean[] disabledProduction) {
        if (disabledProduction[WorkerType.FOOD_WORKER.getType()])
            foodProd = maxFoodProd = 0;
        if (disabledProduction[WorkerType.INDUSTRY_WORKER.getType()])
            industryProd = potentialIndustryProd = 0;
        if (disabledProduction[WorkerType.ENERGY_WORKER.getType()])
            energyProd = maxEnergyProd = potentialEnergyProd = 0;
        if (disabledProduction[WorkerType.SECURITY_WORKER.getType()])
            securityProd = 0;
        if (disabledProduction[WorkerType.RESEARCH_WORKER.getType()])
            researchProd = 0;
        if (disabledProduction[WorkerType.TITAN_WORKER.getType()])
            titanProd = 0;
        if (disabledProduction[WorkerType.DEUTERIUM_WORKER.getType()])
            deuteriumProd = 0;
        if (disabledProduction[WorkerType.DURANIUM_WORKER.getType()])
            duraniumProd = 0;
        if (disabledProduction[WorkerType.CRYSTAL_WORKER.getType()])
            crystalProd = 0;
        if (disabledProduction[WorkerType.IRIDIUM_WORKER.getType()])
            iridiumProd = 0;
    }

    public void disableCreditsProduction() {
        creditsProd = Math.min(creditsProd, 0);
    }

    public void includeSystemMorale(int morale) {
        foodProd = (int) (foodProd * morale / 100.0);
        maxFoodProd = foodProd;
        industryProd = (int) (industryProd * morale / 100.0);
        potentialIndustryProd = (int) (potentialIndustryProd * morale / 100.0);
        securityProd = (int) (securityProd * morale / 100.0);
        researchProd = (int) (researchProd * morale / 100.0);
        titanProd = (int) (titanProd * morale / 100.0);
        deuteriumProd = (int) (deuteriumProd * morale / 100.0);
        duraniumProd = (int) (duraniumProd * morale / 100.0);
        crystalProd = (int) (crystalProd * morale / 100.0);
        iridiumProd = (int) (iridiumProd * morale / 100.0);
        creditsProd = (int) (creditsProd * morale / 100.0);
    }

    public void setCreditsProd(int creditsProd) {
        this.creditsProd = creditsProd;
    }

    public void addCreditsProd(int add) {
        this.creditsProd += add;
    }

    public void addFoodProd(int add) {
        this.foodProd += add;
    }

    public void addIndustryProd(int add) {
        this.industryProd += add;
    }

    public void addEnergyProd(int add) {
        this.energyProd += add;
    }

    public void setMaxEnergyProd(int maxEnergyProd) {
        this.maxEnergyProd = maxEnergyProd;
    }

    public void setFoodProd(int foodProd) {
        this.foodProd = foodProd;
    }
}
