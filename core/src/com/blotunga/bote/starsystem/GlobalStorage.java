/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.starsystem;

import java.util.Arrays;

import com.badlogic.gdx.utils.Array;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.constants.ResourceTypes;
import com.blotunga.bote.utils.IntPoint;

public class GlobalStorage {
    public static class StorageStruct {
        int resTransfer;
        int res;
        IntPoint coord;

        public StorageStruct() {
            resTransfer = 0;
            res = 0;
            coord = new IntPoint();
        }
    }

    private int resourceStorages[]; /// array to store the resources
    private int percentLosing;		/// percent lost per round from global storage
    private int takeFromStorage;	/// how many resources were taken this round from storage
    private int maxTakeFromStorage;	/// maximum resources that can be taken, 20k is hard limit
    private Array<StorageStruct> resOut; //here we store all systems where resources are transfered to in the next round
    private Array<StorageStruct> resIn; //here we store all systems from where resources are transfered in the next round

    public GlobalStorage() {
        resourceStorages = new int[5];
        resOut = new Array<StorageStruct>();
        resIn = new Array<StorageStruct>();
        reset();
    }

    public void reset() {
        Arrays.fill(resourceStorages, 0);
        percentLosing = 15;
        takeFromStorage = 0;
        maxTakeFromStorage = 1000;
        resOut.clear();
        resIn.clear();
    }

    /**
     * Returns the contents of the Global Storage.
     * @param res - resource which we want to check
     * @return - the number of resources in the Global Storage
     */
    public int getResourceStorage(int res) {
        return resourceStorages[res];
    }

    /**
     * Returns the contents of the Global Storage.
     * @return the entire array
     */
    public int[] getResourceStorages() {
        return resourceStorages;
    }

    /**
     * This function returns the number of resources which have to come into the global storage from the system
     * @param res - the resource type
     * @param coord - the coordinates of the system
     * @return - number of resources
     */
    public int getAddedResource(int res, IntPoint coord) {
        int rIn = 0;
        for (int i = 0; i < resIn.size; i++) {
            StorageStruct r = resIn.get(i);
            if (r.coord.equals(coord) && r.res == res) {
                rIn += r.resTransfer;
                break;
            }
        }
        return rIn;
    }

    /**
     * This function returns the number of resources which are transferred from the Global Storage into the starsystem
     * @param res - the resource type
     * @param coord - the coordinates of the system
     * @return - number of resources
     */
    public int getSubResource(int res, IntPoint coord) {
        int rOut = 0;
        for (int i = 0; i < resOut.size; i++) {
            StorageStruct r = resOut.get(i);
            if (r.coord.equals(coord) && r.res == res) {
                rOut += r.resTransfer;
                break;
            }
        }
        return rOut;
    }

    /**
     * @param res - the resource type 
     * @return the entire quantity of resource which should go to Global Storage
     */
    public int getAllAddedResource(int res) {
        int rIn = 0;
        for (int i = 0; i < resIn.size; i++)
            if (resIn.get(i).res == res)
                rIn += resIn.get(i).resTransfer;
        return rIn;
    }

    /**
     * @param res - the resource type 
     * @return the entire quantity of resource which should be removed from the Global Storage
     */
    public int getAllSubResource(int res) {
        int rOut = 0;
        for (int i = 0; i < resOut.size; i++)
            if (resOut.get(i).res == res)
                rOut += resOut.get(i).resTransfer;
        return rOut;
    }

    /**
     * @return the percent which is lost from each resource type every round
     */
    public int getLosing() {
        return percentLosing;
    }

    /**
     * @return the number of resources already taken from storage this round
     */
    public int getTakenResources() {
        return takeFromStorage;
    }

    /**
     * @return the maximum number of resources that can be taken out of the global storage in one round
     */
    public int getMaxTakenResources() {
        return maxTakeFromStorage;
    }

    /**
     * @return true if there is any resource in the GS
     */
    public boolean isFilled() {
        for (int i = ResourceTypes.TITAN.getType(); i <= ResourceTypes.IRIDIUM.getType(); i++)
            if (resourceStorages[i] > 0)
                return true;
        return false;
    }

    /**
     * Sets the percentage which is lost from Global Storage each round
     * @param percent - should be positive and maximum 100%
     */
    public void setLosing(int percent) {
        percentLosing = percent;
    }

    /**
     * Sets the maximum value that can be removed each round from the global storage
     * @param max - hard cap is 20k
     */
    public void setMaxTakenResources(int max) {
        maxTakeFromStorage = Math.min(20000, max);
    }

    /**
     * Function adds resources to Global Storage, checks that the storage doesn't becomes negative
     * @param add - number of resources to add
     * @param res - resource type
     * @param coord - system from which to take the resources
     * @return - the number of resources that have to be transfered in the same round to the same system
     */
    public int addResource(int add, int res, IntPoint coord) {
        int getBack = 0;
        //check if there is already is an entry for the system, and remove it preliminarily
        for (int i = 0; i < resOut.size; i++) {
            StorageStruct ro = resOut.get(i);
            if (ro.coord.equals(coord) && ro.res == res) {
                //if value is smaller than the resources that go out of the GS
                if (add < ro.resTransfer) {
                    ro.resTransfer -= add;
                    takeFromStorage -= add;
                    return add;
                } else if (add == ro.resTransfer) {
                    resOut.removeIndex(i--);
                    takeFromStorage -= add;
                    return add;
                } else {
                    takeFromStorage -= ro.resTransfer;
                    resOut.removeIndex(i--);
                    return add;
                }
            }
        }

        //check if the system already has some transfers, then add to them
        for (int i = 0; i < resIn.size; i++) {
            StorageStruct ri = resIn.get(i);
            if (ri.coord.equals(coord) && ri.res == res) {
                ri.resTransfer += add;
                return getBack;
            }
        }
        StorageStruct ss = new StorageStruct();
        ss.coord = new IntPoint(coord);
        ss.res = res;
        ss.resTransfer = add;
        resIn.add(ss);
        return getBack;
    }

    /**
     * Function substracts resources from the Global Storage. It also checks that no too much was removed from storage
     * and that it doesn't becomes negative
     * @param sub - number of resources to remove
     * @param res - resource type
     * @param coord - system from where to transfer the resources
     * @return - the number of resources that have to be transfered in the same round from the same system
     */
    public int subResource(int sub, int res, IntPoint coord) {
        int getBack = 0;
        //if there were resources moved from this system in this round to GS, then give those back first
        for (int i = 0; i < resIn.size; i++) {
            StorageStruct ri = resIn.get(i);
            if (ri.coord.equals(coord) && ri.res == res) {
                if (sub < ri.resTransfer) {
                    ri.resTransfer -= sub;
                    return sub;
                } else if (sub == ri.resTransfer) {
                    resIn.removeIndex(i--);
                    return sub;
                } else {
                    sub -= ri.resTransfer;
                    getBack = ri.resTransfer;
                    resIn.removeIndex(i--);
                    break;
                }
            }
        }
        //check that there won't be too many resource removed from storage
        if ((takeFromStorage + sub) > maxTakeFromStorage)
            sub = maxTakeFromStorage - takeFromStorage;
        //make sure that we can't get more resources from GS than there are
        if ((sub + getAllSubResource(res)) > resourceStorages[res])
            sub = resourceStorages[res] - getAllSubResource(res);
        if (sub == 0)
            return getBack;
        takeFromStorage += sub;

        //check if the system already has some transfers, then only add
        for (int i = 0; i < resOut.size; i++) {
            StorageStruct ro = resOut.get(i);
            if (ro.coord.equals(coord) && ro.res == res) {
                ro.resTransfer += sub;
                return getBack;
            }
        }
        //otherwise make a new entry
        StorageStruct ss = new StorageStruct();
        ss.coord = new IntPoint(coord);
        ss.res = res;
        ss.resTransfer = sub;
        resOut.add(ss);
        return getBack;
    }

    /**
     * The function does all the modifications in the GS for each round. It also fills the storages of the corresponding systems
     * @param starSystems - the array with the starsystems in the universe
     */
    public void calculate(ResourceManager manager) {
        Array<StarSystem> starSystems = manager.getUniverseMap().getStarSystems();
        takeFromStorage = 0;
        //first move the resources from the Global Storage to the systems
        for (int i = 0; i < resOut.size;) {
            StorageStruct ro = resOut.get(i);
            //just to make sure that by chance there is less than we need
            if (resourceStorages[ro.res] >= ro.resTransfer) {
                starSystems.get(manager.coordsToIndex(ro.coord)).addResourceStore(ro.res, ro.resTransfer);
                resourceStorages[ro.res] -= ro.resTransfer;
            } else {
                starSystems.get(manager.coordsToIndex(ro.coord)).addResourceStore(ro.res, resourceStorages[ro.res]);
                resourceStorages[ro.res] = 0;
            }
            resOut.removeIndex(i);
        }
        //then we fill the global storage with the transfers from the starsystems
        for (int i = 0; i < resIn.size;) {
            StorageStruct ri = resIn.get(i);
            resourceStorages[ri.res] += ri.resTransfer;
            resIn.removeIndex(i);
        }
        //then calculate the lost resources
        for (int i = ResourceTypes.TITAN.getType(); i <= ResourceTypes.IRIDIUM.getType(); i++)
            resourceStorages[i] -= resourceStorages[i] * percentLosing / 100;
    }
}