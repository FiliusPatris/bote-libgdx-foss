/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.starsystem;

import java.util.Arrays;

import com.badlogic.gdx.utils.Array;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.ResearchComplexType;
import com.blotunga.bote.constants.ResearchStatus;
import com.blotunga.bote.constants.ResourceTypes;
import com.blotunga.bote.constants.ShipType;
import com.blotunga.bote.galaxy.ResourceRoute;
import com.blotunga.bote.races.ResearchInfo;
import com.blotunga.bote.ships.ShipInfo;
import com.blotunga.bote.troops.TroopInfo;
import com.blotunga.bote.utils.IntPoint;
import com.blotunga.bote.utils.KryoV;
import com.blotunga.bote.utils.RandUtil;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.KryoSerializable;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

public class AssemblyList implements KryoSerializable {
    public static class AssemblyListEntry {
        public int id;
        public int count;

        public AssemblyListEntry() {
            id = 0;
            count = 0;
        }
    }

    private AssemblyListEntry[] listEntry;

    //variables which give the remaining costs of the elements in the buildlist
    private int[] neededIndustryInAssemblyList;
    private int[] neededTitanInAssemblyList;
    private int[] neededDeuteriumInAssemblyList;
    private int[] neededDuraniumInAssemblyList;
    private int[] neededCrystalInAssemblyList;
    private int[] neededIridiumInAssemblyList;
    private int[] neededDeritiumInAssemblyList;
    //variables which show how many resources are needed to build
    private int neededIndustryForBuild;
    private int neededTitanForBuild;
    private int neededDeuteriumForBuild;
    private int neededDuraniumForBuild;
    private int neededCrystalForBuild;
    private int neededIridiumForBuild;
    private int neededDeritiumForBuild;
    //was the building bought in this turn?
    private boolean wasBuildingBought;
    //costs of an order
    private int buildCosts;

    public AssemblyList() {
        listEntry = new AssemblyListEntry[GameConstants.ALE];
        for (int i = 0; i < listEntry.length; i++)
            listEntry[i] = new AssemblyListEntry();
        neededIndustryInAssemblyList = new int[GameConstants.ALE];
        Arrays.fill(neededIndustryInAssemblyList, 0);
        neededTitanInAssemblyList = new int[GameConstants.ALE];
        Arrays.fill(neededTitanInAssemblyList, 0);
        neededDeuteriumInAssemblyList = new int[GameConstants.ALE];
        Arrays.fill(neededDeuteriumInAssemblyList, 0);
        neededDuraniumInAssemblyList = new int[GameConstants.ALE];
        Arrays.fill(neededDuraniumInAssemblyList, 0);
        neededCrystalInAssemblyList = new int[GameConstants.ALE];
        Arrays.fill(neededCrystalInAssemblyList, 0);
        neededIridiumInAssemblyList = new int[GameConstants.ALE];
        Arrays.fill(neededIridiumInAssemblyList, 0);
        neededDeritiumInAssemblyList = new int[GameConstants.ALE];
        Arrays.fill(neededDeritiumInAssemblyList, 0);
        neededIndustryForBuild = 0;
        neededTitanForBuild = 0;
        neededDeuteriumForBuild = 0;
        neededDuraniumForBuild = 0;
        neededCrystalForBuild = 0;
        neededIridiumForBuild = 0;
        neededDeritiumForBuild = 0;
        wasBuildingBought = false;
        buildCosts = 0;
    }

    public void reset() {
        for (int i = 0; i < listEntry.length; i++) {
            listEntry[i].id = 0;
            listEntry[i].count = 0;
        }
        Arrays.fill(neededIndustryInAssemblyList, 0);
        Arrays.fill(neededTitanInAssemblyList, 0);
        Arrays.fill(neededDeuteriumInAssemblyList, 0);
        Arrays.fill(neededDuraniumInAssemblyList, 0);
        Arrays.fill(neededCrystalInAssemblyList, 0);
        Arrays.fill(neededIridiumInAssemblyList, 0);
        Arrays.fill(neededDeritiumInAssemblyList, 0);
        neededIndustryForBuild = 0;
        neededTitanForBuild = 0;
        neededDeuteriumForBuild = 0;
        neededDuraniumForBuild = 0;
        neededCrystalForBuild = 0;
        neededIridiumForBuild = 0;
        neededDeritiumForBuild = 0;
        wasBuildingBought = false;
        buildCosts = 0;
    }

    public boolean isEmpty() {
        return listEntry[0].id == 0;
    }

    public AssemblyListEntry getAssemblyListEntry(int pos) {
        return listEntry[pos];
    }

    public int getNeededIndustryInAssemblyList(int entry) {
        return neededIndustryInAssemblyList[entry];
    }

    public int getNeededTitanInAssemblyList(int entry) {
        return neededTitanInAssemblyList[entry];
    }

    public int getNeededDeuteriumInAssemblyList(int entry) {
        return neededDeuteriumInAssemblyList[entry];
    }

    public int getNeededDuraniumInAssemblyList(int entry) {
        return neededDuraniumInAssemblyList[entry];
    }

    public int getNeededCrystalInAssemblyList(int entry) {
        return neededCrystalInAssemblyList[entry];
    }

    public int getNeededIridiumInAssemblyList(int entry) {
        return neededIridiumInAssemblyList[entry];
    }

    public int getNeededDeritiumInAssemblyList(int entry) {
        return neededDeritiumInAssemblyList[entry];
    }

    public int getNeededResourceInAssemblyList(int entry, int resType) {
        ResourceTypes res = ResourceTypes.fromResourceTypes(resType);
        switch (res) {
            case TITAN:
                return getNeededTitanInAssemblyList(entry);
            case DEUTERIUM:
                return getNeededDeuteriumInAssemblyList(entry);
            case DURANIUM:
                return getNeededDuraniumInAssemblyList(entry);
            case CRYSTAL:
                return getNeededCrystalInAssemblyList(entry);
            case IRIDIUM:
                return getNeededIridiumInAssemblyList(entry);
            case DERITIUM:
                return getNeededDeritiumInAssemblyList(entry);
            default:
                return 0;
        }
    }

    public int getNeededIndustryForBuild() {
        return neededIndustryForBuild;
    }

    public int getNeededTitanForBuild() {
        return neededTitanForBuild;
    }

    public int getNeededDeuteriumForBuild() {
        return neededDeuteriumForBuild;
    }

    public int getNeededDuraniumForBuild() {
        return neededDuraniumForBuild;
    }

    public int getNeededCrystalForBuild() {
        return neededCrystalForBuild;
    }

    public int getNeededIridiumForBuild() {
        return neededIridiumForBuild;
    }

    public int getNeededDeritiumForBuild() {
        return neededDeritiumForBuild;
    }

    public int getNeededResourceForBuild(int resType) {
        ResourceTypes res = ResourceTypes.fromResourceTypes(resType);
        switch (res) {
            case TITAN:
                return getNeededTitanForBuild();
            case DEUTERIUM:
                return getNeededDeuteriumForBuild();
            case DURANIUM:
                return getNeededDuraniumForBuild();
            case CRYSTAL:
                return getNeededCrystalForBuild();
            case IRIDIUM:
                return getNeededIridiumForBuild();
            case DERITIUM:
                return getNeededDeritiumForBuild();
            default:
                return 0;
        }
    }

    public boolean getWasBuildingBought() {
        return wasBuildingBought;
    }

    public int getBuildCosts() {
        return buildCosts;
    }

    public void setWasBuildingBought(boolean was) {
        wasBuildingBought = was;
    }

    private void resetBuildCosts() {
        neededIndustryForBuild = 0;
        neededTitanForBuild = 0;
        neededDeuteriumForBuild = 0;
        neededDuraniumForBuild = 0;
        neededCrystalForBuild = 0;
        neededIridiumForBuild = 0;
        neededDeritiumForBuild = 0;
    }

    private void applyBonus(int bonus) {
        neededIndustryForBuild -= (int) (bonus * neededIndustryForBuild / 100.0);
        neededTitanForBuild -= (int) (bonus * neededTitanForBuild / 100.0);
        neededDeuteriumForBuild -= (int) (bonus * neededDeuteriumForBuild / 100.0);
        neededDuraniumForBuild -= (int) (bonus * neededDuraniumForBuild / 100.0);
        neededCrystalForBuild -= (int) (bonus * neededCrystalForBuild / 100.0);
        neededIridiumForBuild -= (int) (bonus * neededIridiumForBuild / 100.0);
        neededDeritiumForBuild -= (int) (bonus * neededDeritiumForBuild / 100.0);
    }

    private void applyInfoForUpgrade(BuildingInfo buildingInfo) {
        neededIndustryForBuild += (int) (buildingInfo.getNeededIndustry() * 0.6);
        neededTitanForBuild += (int) (buildingInfo.getNeededTitan() * 0.8);
        neededDeuteriumForBuild += (int) (buildingInfo.getNeededDeuterium() * 0.8);
        neededDuraniumForBuild += (int) (buildingInfo.getNeededDuranium() * 0.8);
        neededCrystalForBuild += (int) (buildingInfo.getNeededCrystal() * 0.8);
        neededIridiumForBuild += (int) (buildingInfo.getNeededIridium() * 0.8);
        neededDeritiumForBuild += (int) (buildingInfo.getNeededDeritium() * 0.8);
    }

    public void calculateNeededResources(BuildingInfo buildingInfo, ShipInfo shipInfo, TroopInfo troopInfo,
            Array<Building> buildings, int runningNumber, ResearchInfo researchInfo) {
        calculateNeededResources(buildingInfo, shipInfo, troopInfo, buildings, runningNumber, researchInfo, 1.0f);
    }

    /**
     * Function calculates the resources needed for building something.
     * @param buildingInfo Info about the building which we want to build (null if not building)
     * @param shipInfo Info about the ship which we want to build (null if not ship)
     * @param troopInfo Info about the troop which we want to build (null if not troop)
     * @param buildings Array with all buildings in the sector
     * @param runningNumber Number of the the build order
     * @param researchInfo Research status
     * @param modifier
     */
    public void calculateNeededResources(BuildingInfo buildingInfo, ShipInfo shipInfo, TroopInfo troopInfo,
            Array<Building> buildings, int runningNumber, ResearchInfo researchInfo, float modifier) {
        if (runningNumber > 0 && runningNumber < 10000) { // not an upgrade, but a real building
            neededIndustryForBuild = buildingInfo.getNeededIndustry();
            neededTitanForBuild = buildingInfo.getNeededTitan();
            neededDeuteriumForBuild = buildingInfo.getNeededDeuterium();
            neededDuraniumForBuild = buildingInfo.getNeededDuranium();
            neededCrystalForBuild = buildingInfo.getNeededCrystal();
            neededIridiumForBuild = buildingInfo.getNeededIridium();
            neededDeritiumForBuild = buildingInfo.getNeededDeritium();
        } else if (runningNumber < 0) {//we want to upgrade
            int numberOfBuildings = buildings.size;
            runningNumber *= (-1);
            resetBuildCosts(); // set build costs to 0
            for (int i = 0; i < numberOfBuildings; i++) {
                if (buildings.get(i).getRunningNumber() == buildingInfo.getPredecessorId()) {
                    // use 80% of the resources and 60% of the industry
                    applyInfoForUpgrade(buildingInfo);
                }
            }
        } else if (runningNumber >= 10000 && runningNumber < 20000) { //it's a ship
            neededIndustryForBuild = shipInfo.getNeededIndustry();
            neededTitanForBuild = shipInfo.getNeededTitan();
            neededDeuteriumForBuild = shipInfo.getNeededDeuterium();
            neededDuraniumForBuild = shipInfo.getNeededDuranium();
            neededCrystalForBuild = shipInfo.getNeededCrystal();
            neededIridiumForBuild = shipInfo.getNeededIridium();
            neededDeritiumForBuild = shipInfo.getNeededDeritium();
            // Apply unique research if necessary
            if (researchInfo.getResearchComplex(ResearchComplexType.GENERAL_SHIP_TECHNOLOGY).getFieldStatus(3) == ResearchStatus.RESEARCHED) {
                int bonus = researchInfo.getResearchComplex(ResearchComplexType.GENERAL_SHIP_TECHNOLOGY).getBonus(3);
                applyBonus(bonus);
            }
            // Also on transport ships the friendly ship research
            if (researchInfo.getResearchComplex(ResearchComplexType.PEACEFUL_SHIP_TECHNOLOGY).getFieldStatus(3) == ResearchStatus.RESEARCHED
                    && shipInfo.getShipType().getType() <= ShipType.COLONYSHIP.getType()) {
                int bonus = researchInfo.getResearchComplex(ResearchComplexType.PEACEFUL_SHIP_TECHNOLOGY).getBonus(3);
                applyBonus(bonus);
            }
        } else if (runningNumber >= 20000) { // it's all about the troops :)
            neededIndustryForBuild = troopInfo.getNeededIndustry();
            neededTitanForBuild = troopInfo.getNeededResources()[ResourceTypes.TITAN.getType()];
            neededDeuteriumForBuild = troopInfo.getNeededResources()[ResourceTypes.DEUTERIUM.getType()];
            neededDuraniumForBuild = troopInfo.getNeededResources()[ResourceTypes.DURANIUM.getType()];
            neededCrystalForBuild = troopInfo.getNeededResources()[ResourceTypes.CRYSTAL.getType()];
            neededIridiumForBuild = troopInfo.getNeededResources()[ResourceTypes.IRIDIUM.getType()];
            neededDeritiumForBuild = 0;
            // check for research
            if (researchInfo.getResearchComplex(ResearchComplexType.TROOPS).getFieldStatus(3) == ResearchStatus.RESEARCHED) {
                int bonus = researchInfo.getResearchComplex(ResearchComplexType.TROOPS).getBonus(3);
                applyBonus(bonus);
            }
        }
        // apply global unique research bonus
        if (researchInfo.getResearchComplex(ResearchComplexType.ECONOMY).getFieldStatus(2) == ResearchStatus.RESEARCHED) {
            int bonus = researchInfo.getResearchComplex(ResearchComplexType.ECONOMY).getBonus(2);
            applyBonus(bonus);
        }
        neededIndustryForBuild = (int) (neededIndustryForBuild * modifier);
        neededTitanForBuild = (int) (neededTitanForBuild * modifier);
        neededDeuteriumForBuild = (int) (neededDeuteriumForBuild * modifier);
        neededDuraniumForBuild = (int) (neededDuraniumForBuild * modifier);
        neededCrystalForBuild = (int) (neededCrystalForBuild * modifier);
        neededIridiumForBuild = (int) (neededIridiumForBuild * modifier);
        neededDeritiumForBuild = (int) (neededDeritiumForBuild * modifier);
    }

    /**
     * Function calculates the resources needed for each update in the building list. Is called in NextRound().
     * Because in the mean time the player could've bought the building, which could modify the costs of some updates
     * @param buildingInfo Global building info
     * @param buildings All buildings in the system
     * @param researchInfo The empire's research info
     */
    public void calculateNeededResourcesForUpdate(Array<BuildingInfo> buildingInfos, Array<Building> buildings,
            ResearchInfo researchInfo) {
        int numberOfBuildings = buildings.size;
        for (int j = 0; j < GameConstants.ALE; j++)
            if (listEntry[j].id < 0) {
                // the runningnumber of the building which has to be updated here
                // this is the number of the previous entry
                int runningNumber = buildingInfos.get(buildingInfos.get(listEntry[j].id * (-1) - 1).getPredecessorId() - 1)
                        .getRunningNumber();
                resetBuildCosts(); // set build costs to 0
                for (int i = 0; i < numberOfBuildings; i++)
                    if (buildings.get(i).getRunningNumber() == runningNumber) {
                        BuildingInfo buildingInfo = buildingInfos.get(listEntry[j].id * (-1) - 1);
                        applyInfoForUpgrade(buildingInfo);
                        // Total Cost == (Cost of Level X+1 Building) - ((Cost of Level X Building) * Recycle factor)
                    }
                // apply global unique research bonus
                if (researchInfo.getResearchComplex(ResearchComplexType.ECONOMY).getFieldStatus(2) == ResearchStatus.RESEARCHED) {
                    int bonus = researchInfo.getResearchComplex(ResearchComplexType.ECONOMY).getBonus(2);
                    applyBonus(bonus);
                }
                //if the order is the first in the assembly list then we can't simply consider it newly inserted, so we have to consider that it has already progressed
                if (j == 0) {
                    if (neededIndustryInAssemblyList[j] > neededIndustryForBuild)
                        neededIndustryInAssemblyList[j] = neededIndustryForBuild;
                    if (neededTitanInAssemblyList[j] > neededTitanForBuild)
                        neededTitanInAssemblyList[j] = neededTitanForBuild;
                    if (neededDeuteriumInAssemblyList[j] > neededDeuteriumForBuild)
                        neededDeuteriumInAssemblyList[j] = neededDeuteriumForBuild;
                    if (neededDuraniumInAssemblyList[j] > neededDuraniumForBuild)
                        neededDuraniumInAssemblyList[j] = neededDuraniumForBuild;
                    if (neededCrystalInAssemblyList[j] > neededCrystalForBuild)
                        neededCrystalInAssemblyList[j] = neededCrystalForBuild;
                    if (neededIridiumInAssemblyList[j] > neededIridiumForBuild)
                        neededIridiumInAssemblyList[j] = neededIridiumForBuild;
                    if (neededDeritiumInAssemblyList[j] > neededDeritiumForBuild)
                        neededDeritiumInAssemblyList[j] = neededDeritiumForBuild;
                } else { // if it's not first, then consider it newly introduced
                    neededIndustryInAssemblyList[j] = neededIndustryForBuild;
                    neededTitanInAssemblyList[j] = neededTitanForBuild;
                    neededDeuteriumInAssemblyList[j] = neededDeuteriumForBuild;
                    neededDuraniumInAssemblyList[j] = neededDuraniumForBuild;
                    neededCrystalInAssemblyList[j] = neededCrystalForBuild;
                    neededIridiumInAssemblyList[j] = neededIridiumForBuild;
                    neededDeritiumInAssemblyList[j] = neededDeritiumForBuild;
                }
            }
    }

    /**
     * This function removes the resources from them local storage of the system and in case
     * there are resource routes assigned, also from the starsystems on the route. But only if necessary
     * @param res
     * @param coord
     * @param systems
     * @param routesFrom
     */
    private void removeResourceFromStorage(int res, IntPoint coord, IntPoint distributorCoord, ResourceManager manager,
            Array<IntPoint> routesFrom, int resourceFromRoutes, int neededRes) {
        if (coord.equals(new IntPoint(-1, -1)))
            return;
        Array<StarSystem> systems = manager.getUniverseMap().getStarSystems();
        StarSystem system = systems.get(manager.coordsToIndex(coord));
        StarSystem distributor = null;
        if (!distributorCoord.equals(new IntPoint()))
            distributor = systems.get(manager.coordsToIndex(distributorCoord));
        int remainingRes = getNeededResourceInAssemblyList(0, res) - system.getResourceStore(res);
        //deritium has no resource route
        if (res != ResourceTypes.DERITIUM.getType() && (system.getResourceStore(res) + resourceFromRoutes >= neededRes)) {
            if (remainingRes > 0) {
                system.setResourceStore(res, 0);
                class RouteList {
                    ResourceRoute route;
                    IntPoint fromSystem;

                    public RouteList(ResourceRoute route, IntPoint fromSystem) {
                        this.route = route;
                        this.fromSystem = fromSystem;
                    }
                }

                Array<RouteList> routes = new Array<RouteList>();
                for (int j = 0; j < routesFrom.size; j++) {
                    IntPoint p = routesFrom.get(j);
                    for (int k = 0; k < systems.get(manager.coordsToIndex(p)).getResourceRoutes().size; k++) {
                        //is it what we're looking for?
                        if (systems.get(manager.coordsToIndex(p)).getResourceRoutes().get(k).getResource().getType() == res) {
                            // is it our target?
                            if (systems.get(manager.coordsToIndex(p)).getResourceRoutes().get(k).getCoord().equals(coord)) {
                                // check if route already used
                                boolean used = false;
                                for (int l = 0; l < routes.size; l++)
                                    if (routes.get(l).fromSystem.equals(p)) {
                                        used = true;
                                        break;
                                    }
                                if (!used)
                                    routes.add(new RouteList(systems.get(manager.coordsToIndex(p)).getResourceRoutes().get(k), p));
                            }
                        }
                    }
                }
                // we have the list of the routes, so deliver
                while (routes.size != 0) {
                    // choose a route randomly, in case there are several routes of the same kind
                    int random = (int) (RandUtil.random() * routes.size);
                    int percent = 0;
                    IntPoint start = routes.get(random).fromSystem;
                    //check if there are enough stores in the storage of the system
                    if (systems.get(manager.coordsToIndex(start)).getResourceStore(res) >= remainingRes) {
                        systems.get(manager.coordsToIndex(start)).addResourceStore(res, -remainingRes);
                        if (getNeededResourceInAssemblyList(0, res) > 0)
                            percent = 100 * remainingRes / getNeededResourceInAssemblyList(0, res);
                        remainingRes = 0;
                    } else {
                        remainingRes -= systems.get(manager.coordsToIndex(start)).getResourceStore(res);
                        if (getNeededResourceInAssemblyList(0, res) > 0)
                            percent = 100 * systems.get(manager.coordsToIndex(start)).getResourceStore(res)
                                    / getNeededResourceInAssemblyList(0, res);
                        systems.get(manager.coordsToIndex(start)).setResourceStore(res, 0);
                    }
                    ResourceRoute resRoute = routes.get(random).route;
                    resRoute.setPercent(percent);
                    routes.removeIndex(random);

                    if (remainingRes == 0) {
                        routes.clear();
                        break;
                    }
                }
            } else
                system.addResourceStore(res, (-1) * getNeededResourceInAssemblyList(0, res));
        } else {
            if (system.getResourceStore(res) + resourceFromRoutes < neededRes && remainingRes > 0 && (distributor != null)) {
                system.setResourceStore(res, 0);
                distributor.addResourceStore(res, -remainingRes);
            } else
                system.addResourceStore(res, (-1) * getNeededResourceInAssemblyList(0, res));
        }
    }

    public boolean makeEntry(int runningNumber, IntPoint coord, ResourceManager manager) {
        return makeEntry(runningNumber, coord, manager, false);
    }

    /**
     * The function creates an entry in the assemblylist. Has to be called after CalculateNeededResources()
     * @param runningNumber - for example 1 - primitive Farm etc...
     * @param coord - coordinates of the system
     * @param systems - array of the systems
     * @param onlyTest - don't make an actual entry just check if it can be added
     * @return true if the entry could be made. In case the return value is True, the function CalculateVariables has to be called!!!
     */
    public boolean makeEntry(int runningNumber, IntPoint coord, ResourceManager manager, boolean onlyTest) {
        //check the list and if we find an entry which is 0, we can save it there, if not then the list is full
        int entry = -1;
        if (!onlyTest) {
            for (int i = 0; i < GameConstants.ALE; i++) {
                if (listEntry[i].id == 0) {
                    if (i != 0 && listEntry[i-1].id == runningNumber) { //if last, increase count
                        entry = i-1;
                        break;
                    }
                    entry = i;
                    break;
                }
            }

            if (listEntry[GameConstants.ALE - 1].id == runningNumber) { //if last, increase count
                entry = GameConstants.ALE - 1;
            }

            //if full return
            if (entry == -1)
                return false;
        }

        Array<StarSystem> systems = manager.getUniverseMap().getStarSystems();
        StarSystem system = systems.get(manager.coordsToIndex(coord));
        // check resource routes and query extra resources
        Array<IntPoint> routesFrom = new Array<IntPoint>();
        int[] resourceFromRoutes = new int[ResourceTypes.DERITIUM.getType() + 1];
        int[] resInDistSys = new int[ResourceTypes.DERITIUM.getType() + 1];
        IntPoint[] resourceDistributorCoords = new IntPoint[ResourceTypes.DERITIUM.getType() + 1];

        for (int i = 0; i <= ResourceTypes.DERITIUM.getType(); i++) {
            resourceFromRoutes[i] = 0;
            resInDistSys[i] = 0;
            resourceDistributorCoords[i] = new IntPoint();
        }

        for (int k = 0; k < systems.size; k++) {
            StarSystem other = systems.get(k);
            if (other.getOwnerId().equals(system.getOwnerId()) && !other.getCoordinates().equals(system.getCoordinates())) {
                for (int i = 0; i < other.getResourceRoutes().size; i++) {
                    if (other.getResourceRoutes().get(i).getCoord().equals(coord)) {
                        if (other.getBlockade() == 0 && system.getBlockade() == 0) {
                            routesFrom.add(other.getCoordinates());
                            int res = other.getResourceRoutes().get(i).getResource().getType();
                            resourceFromRoutes[res] += other.getResourceStore(res);
                        }
                    }
                }
                //if blockaded we can't transfer resources
                if (other.getBlockade() == 0 && system.getBlockade() == 0) {
                    for (int res = ResourceTypes.TITAN.getType(); res <= ResourceTypes.DERITIUM.getType(); res++) {
                        if (other.getProduction().getResourceDistributor(res)) {
                            resourceDistributorCoords[res] = other.getCoordinates();
                            resInDistSys[res] = other.getResourceStore(res);
                        }
                    }
                }
            }
        }
        //check if we have enough resources on store
        for (int res = ResourceTypes.TITAN.getType(); res <= ResourceTypes.DERITIUM.getType(); res++) {
            int neededRes = getNeededResourceForBuild(res);
            if (system.getResourceStore(res) + resourceFromRoutes[res] < neededRes && resInDistSys[res] < neededRes)
                return false;
        }
        if (onlyTest)
            return true;

        //looks like we're good to go
        listEntry[entry].id = runningNumber;
        listEntry[entry].count++;
        //save what's needed
        if (entry != 0 || (entry == 0 && listEntry[0].count == 1)) {
            neededIndustryInAssemblyList[entry] = neededIndustryForBuild;
            neededTitanInAssemblyList[entry] = neededTitanForBuild;
            neededDeuteriumInAssemblyList[entry] = neededDeuteriumForBuild;
            neededDuraniumInAssemblyList[entry] = neededDuraniumForBuild;
            neededCrystalInAssemblyList[entry] = neededCrystalForBuild;
            neededIridiumInAssemblyList[entry] = neededIridiumForBuild;
            neededDeritiumInAssemblyList[entry] = neededDeritiumForBuild;
        }

        //if it's the first entry, then we can also remove them, otherwise only when it becomes the first entry
        if (entry == 0 && listEntry[0].count == 1) {
            for (int res = ResourceTypes.TITAN.getType(); res <= ResourceTypes.DERITIUM.getType(); res++) {
                int neededRes = getNeededResourceForBuild(res);
                if (neededRes > 0) {
                    //resource is removed from own system and the routes
                    removeResourceFromStorage(res, coord, resourceDistributorCoords[res], manager, routesFrom,
                            resourceFromRoutes[res], neededRes);
                }
            }
        }

        return true;
    }

    /**
     * Function calculates the costs of the build if we want to buy it right now
     * @param resPrices - prices on the market of the resources
     */
    public void calculateBuildCosts(int[] resPrices, int bonus) {
        if (neededIndustryInAssemblyList[0] > 1) {
            //prices for the goods are for batches of 1000
            //for buying we take them halved into account, as otherwise it would be too expensive
            buildCosts = neededIndustryInAssemblyList[0];
            buildCosts += (int) (neededTitanInAssemblyList[0] * resPrices[ResourceTypes.TITAN.getType()] / 2000.0f);
            buildCosts += (int) (neededDeuteriumInAssemblyList[0] * resPrices[ResourceTypes.DEUTERIUM.getType()] / 2000.0f);
            buildCosts += (int) (neededDuraniumInAssemblyList[0] * resPrices[ResourceTypes.DURANIUM.getType()] / 2000.0f);
            buildCosts += (int) (neededCrystalInAssemblyList[0] * resPrices[ResourceTypes.CRYSTAL.getType()] / 2000.0f);
            buildCosts += (int) (neededIridiumInAssemblyList[0] * resPrices[ResourceTypes.IRIDIUM.getType()] / 2000.0f);

            ///SPECIAL RESEARCH///
            buildCosts -= buildCosts * bonus / 100;
        }
    }

    /**
     * Function sets the remaining costs to 1 and says that we have bought something. CalculateBuildCosts() has to be called before!!!
     * @param empiresCredits - money of the empire
     * @return - costs of the buy
     */
    public int buyBuilding(int empiresCredits) {
        if (neededIndustryInAssemblyList[0] > 1)
            if (buildCosts <= empiresCredits) {
                neededIndustryInAssemblyList[0] = 1;
                wasBuildingBought = true;
                return buildCosts;
            }
        return 0;
    }

    /**
     * Function calculates what industry output is available after ending the turn
     * @param industryProd - current industry production
     * @return - if the build is complete, it returns TRUE. After receiving TRUE, clearAssemblyList() has to be called!!!
     */
    public boolean calculateBuildInAssemblyList(int industryProd) {
        neededIndustryInAssemblyList[0] -= industryProd;
        //the needed resources are reduced each turn internally
        //thus if we stop a half-finished build, we won't get the full amount
        neededTitanInAssemblyList[0] = (int) (neededTitanInAssemblyList[0] * 0.75);
        neededDeuteriumInAssemblyList[0] = (int) (neededDeuteriumInAssemblyList[0] * 0.75);
        neededDuraniumInAssemblyList[0] = (int) (neededDuraniumInAssemblyList[0] * 0.75);
        neededCrystalInAssemblyList[0] = (int) (neededCrystalInAssemblyList[0] * 0.75);
        neededIridiumInAssemblyList[0] = (int) (neededIridiumInAssemblyList[0] * 0.75);
        neededDeritiumInAssemblyList[0] = (int) (neededDeritiumInAssemblyList[0] * 0.75);
        if (neededIndustryInAssemblyList[0] <= 0) {
            neededIndustryInAssemblyList[0] = 0;
            neededTitanInAssemblyList[0] = 0;
            neededDeuteriumInAssemblyList[0] = 0;
            neededDuraniumInAssemblyList[0] = 0;
            neededCrystalInAssemblyList[0] = 0;
            neededIridiumInAssemblyList[0] = 0;
            neededDeritiumInAssemblyList[0] = 0;
            return true;
            //call clearAssemblyList() after this!!!
        }
        return false;
    }

    public void shiftAssemblyListToLeft() {
        // delete the assemblyList entry of the building/ship/etc. If the list is not empty, shift all others into front
        if (listEntry[0].count == 1) {
            listEntry[0].id = 0;
            listEntry[0].count = 0;
            neededIndustryInAssemblyList[0] = 0;
            neededTitanInAssemblyList[0] = 0;
            neededDeuteriumInAssemblyList[0] = 0;
            neededDuraniumInAssemblyList[0] = 0;
            neededCrystalInAssemblyList[0] = 0;
            neededIridiumInAssemblyList[0] = 0;
            neededDeritiumInAssemblyList[0] = 0;
        } else if (listEntry[0].count > 1){
            listEntry[0].count--;
            neededIndustryInAssemblyList[0] = neededIndustryForBuild;
            neededTitanInAssemblyList[0] = neededTitanForBuild;
            neededDeuteriumInAssemblyList[0] = neededDeuteriumForBuild;
            neededDuraniumInAssemblyList[0] = neededDuraniumForBuild;
            neededCrystalInAssemblyList[0] = neededCrystalForBuild;
            neededIridiumInAssemblyList[0] = neededIridiumForBuild;
            neededDeritiumInAssemblyList[0] = neededDeritiumForBuild;
            return;
        }

        for (int i = 0; i < GameConstants.ALE - 1; i++) {
            if (listEntry[i + 1].id != 0) {
                listEntry[i].id = listEntry[i + 1].id;
                listEntry[i].count = listEntry[i + 1].count;
                neededIndustryInAssemblyList[i] = neededIndustryInAssemblyList[i + 1];
                neededTitanInAssemblyList[i] = neededTitanInAssemblyList[i + 1];
                neededDeuteriumInAssemblyList[i] = neededDeuteriumInAssemblyList[i + 1];
                neededDuraniumInAssemblyList[i] = neededDuraniumInAssemblyList[i + 1];
                neededCrystalInAssemblyList[i] = neededCrystalInAssemblyList[i + 1];
                neededIridiumInAssemblyList[i] = neededIridiumInAssemblyList[i + 1];
                neededDeritiumInAssemblyList[i] = neededDeritiumInAssemblyList[i + 1];

                listEntry[i + 1].id = 0;
                listEntry[i + 1].count = 0;
                neededIndustryInAssemblyList[i + 1] = 0;
                neededTitanInAssemblyList[i + 1] = 0;
                neededDeuteriumInAssemblyList[i + 1] = 0;
                neededDuraniumInAssemblyList[i + 1] = 0;
                neededCrystalInAssemblyList[i + 1] = 0;
                neededIridiumInAssemblyList[i + 1] = 0;
                neededDeritiumInAssemblyList[i + 1] = 0;
            } else
                break;
        }
    }

    /**
     * Function deletes an entry from the assemblylist, which is the case when it was successfully built or we want to manually remove the first entry
     * After calling this, calculateVariables() has to be called!!!
     * @param coord - coordinates of the system
     * @param systems - array containing the systems
     */
    public void clearAssemblyList(IntPoint coord, ResourceManager manager) {
        Array<StarSystem> systems = manager.getUniverseMap().getStarSystems();
        StarSystem system = systems.get(manager.coordsToIndex(coord));
        Array<IntPoint> routesFrom = new Array<IntPoint>();
        int[] resourcesFromRoutes = new int[ResourceTypes.DERITIUM.getType() + 1];
        int[] resInDistSys = new int[ResourceTypes.DERITIUM.getType() + 1];
        IntPoint[] resourceDistributorCoords = new IntPoint[ResourceTypes.DERITIUM.getType() + 1];

        for (int i = 0; i <= ResourceTypes.DERITIUM.getType(); i++) {
            resourcesFromRoutes[i] = 0;
            resInDistSys[i] = 0;
            resourceDistributorCoords[i] = new IntPoint();
        }

        for (int k = 0; k < systems.size; k++) {
            StarSystem other = systems.get(k);
            if (other.getOwnerId().equals(system.getOwnerId()) && !other.getCoordinates().equals(coord)) {
                for (int i = 0; i < other.getResourceRoutes().size; i++) {
                    if (other.getResourceRoutes().get(i).getCoord().equals(coord)) {
                        //Reset percentage
                        other.getResourceRoutes().get(i).setPercent(0);
                        //get resources through route
                        if (other.getBlockade() == 0 && system.getBlockade() == 0) {
                            routesFrom.add(other.getCoordinates());
                            int res = other.getResourceRoutes().get(i).getResource().getType();
                            resourcesFromRoutes[res] += other.getResourceStore(res);
                        }
                    }
                }
                //not for blockades
                if (other.getBlockade() == 0 && system.getBlockade() == 0) {
                    for (int res = ResourceTypes.TITAN.getType(); res <= ResourceTypes.DERITIUM.getType(); res++) {
                        if (other.getProduction().getResourceDistributor(res)) {
                            resourceDistributorCoords[res] = other.getCoordinates();
                            resInDistSys[res] = other.getResourceStore(res);
                        }
                    }
                }
            }
        }

        shiftAssemblyListToLeft();

        //check if the next entry can be built -> there are enough resources on store
        //this can happen for example if the resources were sold on the market earlier
        //if this is the case, the build will be cancelled
        for (int res = ResourceTypes.TITAN.getType(); res <= ResourceTypes.DERITIUM.getType();) {
            if (listEntry[0].id == 0)
                break;

            int neededRes = getNeededResourceInAssemblyList(0, res);
            int currentStore = system.getResourceStore(res);
            if ((currentStore + resourcesFromRoutes[res] < neededRes) && (resInDistSys[res] + currentStore < neededRes)) {
                shiftAssemblyListToLeft();
                res = ResourceTypes.TITAN.getType();
            } else
                res++;
        }

        //if it can be built, remove the resources
        for (int res = ResourceTypes.TITAN.getType(); res <= ResourceTypes.DERITIUM.getType(); res++) {
            int neededRes = getNeededResourceInAssemblyList(0, res);
            if (neededRes > 0) {
                //resource is removed from own system and the routes
                removeResourceFromStorage(res, coord, resourceDistributorCoords[res], manager, routesFrom,
                        resourcesFromRoutes[res], neededRes);
            }
        }
    }

    /**
     * Sorts the assemblyList so that there are no empty entries in the middle. Has to be called when an entry was manually removed.
     * For the first entry though we have to use clearAssemblyList()!!!
     * @param entry
     */
    public void adjustAssemblyList(int entry) {
        if (listEntry[entry].count == 1) {
            listEntry[entry].id = 0;
            listEntry[entry].count = 0;
            int mod = 0;
            for (int i = entry; i < GameConstants.ALE - 1 - mod; i++)
                if (listEntry[i].id == 0) // if empty
                    if (listEntry[i + 1 + mod].id != 0) {
                        if (i > 0 && listEntry[i - 1].id != 0 && listEntry[i - 1].id == listEntry[i + 1].id) {
                            listEntry[i - 1].count += listEntry[i + 1].count;
                            listEntry[i + 1].id = 0;
                            listEntry[i + 1].count = 0;
                            mod = 1; //jump over i + 1
                            i--;
                        } else {
                            listEntry[i].id = listEntry[i + 1 + mod].id;
                            listEntry[i].count = listEntry[i + 1 + mod].count;
                            neededIndustryInAssemblyList[i] = neededIndustryInAssemblyList[i + 1 + mod];
                            neededTitanInAssemblyList[i] = neededTitanInAssemblyList[i + 1 + mod];
                            neededDeuteriumInAssemblyList[i] = neededDeuteriumInAssemblyList[i + 1 + mod];
                            neededDuraniumInAssemblyList[i] = neededDuraniumInAssemblyList[i + 1 + mod];
                            neededCrystalInAssemblyList[i] = neededCrystalInAssemblyList[i + 1 + mod];
                            neededIridiumInAssemblyList[i] = neededIridiumInAssemblyList[i + 1 + mod];
                            neededDeritiumInAssemblyList[i] = neededDeritiumInAssemblyList[i + 1 + mod];
                            listEntry[i + 1 + mod].id = 0;
                            listEntry[i + 1 + mod].count = 0;
                        }
                    }
        } else
            listEntry[entry].count--;
    }

    @Override
    public void write(Kryo kryo, Output output) {
        output.writeInt(buildCosts, false);
        kryo.writeObject(output, listEntry);
        output.writeInt(neededCrystalForBuild, false);
        kryo.writeObject(output, neededCrystalInAssemblyList);
        output.writeInt(neededDeritiumForBuild, false);
        kryo.writeObject(output, neededDeritiumInAssemblyList);
        output.writeInt(neededDeuteriumForBuild, false);
        kryo.writeObject(output, neededDeuteriumInAssemblyList);
        output.writeInt(neededDuraniumForBuild, false);
        kryo.writeObject(output, neededDuraniumInAssemblyList);
        output.writeInt(neededIndustryForBuild, false);
        kryo.writeObject(output, neededIndustryInAssemblyList);
        output.writeInt(neededIridiumForBuild, false);
        kryo.writeObject(output, neededIridiumInAssemblyList);
        output.writeInt(neededTitanForBuild, false);
        kryo.writeObject(output, neededTitanInAssemblyList);
        output.writeBoolean(wasBuildingBought);
    }

    @Override
    public void read(Kryo kryo, Input input) { //to preserve save compatibility we need to use this order
        buildCosts = input.readInt(false);
        if (((KryoV) kryo).getSaveVersion() < 10) {
            int[] le = kryo.readObject(input, int[].class);
            for (int i = 0; i < listEntry.length; i++) {
                listEntry[i].id = le[i];
                if (le[i] != 0)
                    listEntry[i].count = 1;
            }
        } else
            listEntry = kryo.readObject(input, AssemblyListEntry[].class);
        neededCrystalForBuild = input.readInt(false);
        neededCrystalInAssemblyList = kryo.readObject(input, int[].class);
        neededDeritiumForBuild = input.readInt(false);
        neededDeritiumInAssemblyList = kryo.readObject(input, int[].class);
        neededDeuteriumForBuild = input.readInt(false);
        neededDeuteriumInAssemblyList = kryo.readObject(input, int[].class);
        neededDuraniumForBuild = input.readInt(false);
        neededDuraniumInAssemblyList = kryo.readObject(input, int[].class);
        neededIndustryForBuild = input.readInt(false);
        neededIndustryInAssemblyList = kryo.readObject(input, int[].class);
        neededIridiumForBuild = input.readInt(false);
        neededIridiumInAssemblyList = kryo.readObject(input, int[].class);
        neededTitanForBuild = input.readInt(false);
        neededTitanInAssemblyList = kryo.readObject(input, int[].class);
        wasBuildingBought = input.readBoolean();
    }
}
