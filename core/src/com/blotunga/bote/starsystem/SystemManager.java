/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.starsystem;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.IntSet;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.constants.Aliens;
import com.blotunga.bote.constants.DiplomaticAgreement;
import com.blotunga.bote.constants.ResourceTypes;
import com.blotunga.bote.constants.WorkerType;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.races.Race;
import com.blotunga.bote.ships.ShipMap;
import com.blotunga.bote.ships.Ships;
import com.blotunga.bote.starsystem.StarSystem.SetWorkerMode;

public class SystemManager {
    final public static int max_priority = 40;
    final public static int min_priority = 0;

    final public static int max_min_morale = 200;
    final public static int min_min_morale = 0;
    final public static int max_min_morale_prod = 8;
    final public static int min_min_morale_prod = -8;

    private boolean active;
    private boolean safeMorale;
    private boolean maxIndustry;
    private boolean neglectFood;
    private int minMorale;
    private int minMoraleProd;
    private boolean bombWarning;
    private boolean onOffline;
    private ArrayMap<WorkerType, Integer> priorityMap; //min_priority + 1 till max_priority each, borders included
    private IntSet ignoredBuildings;

    //holds the relation data between a producible resource needing workers and the amount of workers it gets
    static class DistributionElem implements Comparable<DistributionElem> {
        WorkerType type;
        double count;
        int counti;
        double fPart;
        static boolean compareCount;

        public DistributionElem(WorkerType type, double count) {
            this.type = type;
            this.count = count;
            this.counti = (int) Math.floor(count);
            this.fPart = count - counti;
        }

        @Override
        public int compareTo(DistributionElem o) {
            if (!compareCount)
                return Double.compare(fPart, o.fPart);
            else
                return Double.compare(count, o.count);
        }
    }

    //Class which calculates a default distribution (that is, without considering number of buildings) onto (currently)
    //categories industry, security, research, titan, deuterium, duranium, crystal, iridium.
    //Energy and food are currently not passed to this class.
    //The only deciding factors are given priorities and number of all_workers (determined by system population).
    //The whole class is basically an implementation of the following mathematical formula:
    //workers_i = (all_workers - sum(workers_j, j = 1..(i-1))) / (1 + (1 / priority_i) * sum(priority_j, j = (i + 1)..n))
    //where i = 1..n with n = 8 in our case (count of priorities).
    //This means that workers_i : workers_j = priority_i : priority_j for i,j = 1..n; i and j symbolizing one of the
    //categories from above.
    //
    //The algorithm first distributes workers according to the integer part of the resulting double worker counts,
    //then sorts it according to the floating point part, and distributes any remaining workers onto those results
    //with the highest floating point parts.
    //
    //The result is sorted according to number of workers the category got (by the floating point result,
    //not the integer one).
    class DefaultDistributionCalculator {
        private int allWorkers;
        private ArrayMap<WorkerType, Integer> priorities;
        private Array<DistributionElem> result;
        private int resultingWorkers;

        public DefaultDistributionCalculator(int allWorkers, ArrayMap<WorkerType, Integer> priorities) {
            this.allWorkers = allWorkers;
            this.priorities = priorities;
            result = new Array<SystemManager.DistributionElem>();
            resultingWorkers = 0;
        }

        public Array<DistributionElem> calc() {
            if (priorities.size == 0 || allWorkers == 0)
                return new Array<SystemManager.DistributionElem>();

            //calculate the distribution with floating point results
            calcDefaultWorkersDistributionDouble();

            //give any workers which remain after considering the integer parts
            //to those categories with the highest floating point parts
            distributeRemaining();

            DistributionElem.compareCount = true;
            result.sort();
            result.reverse();
            return result;
        }

        private double workersOn(WorkerType type, double workers) {
            for (DistributionElem el : result)
                workers -= el.count;
            double divisor = 0;
            double weightOfThis = priorities.get(type);
            int i = priorities.indexOfKey(type) + 1;
            for (; i < priorities.size; i++)
               divisor += priorities.getValueAt(i);
            divisor = 1.0f + divisor / weightOfThis;
            return workers / divisor;
        }

        private void calcDefaultWorkersDistributionDouble() {
            for (int i = 0; i < priorities.size; i++) {
                WorkerType wk = priorities.getKeyAt(i);
                double workers = workersOn(wk, allWorkers);
                resultingWorkers += (int) Math.floor(workers);
                result.add(new DistributionElem(wk, workers));
            }
        }

        private void distributeRemaining() {
            DistributionElem.compareCount = false;
            result.sort();
            for (int i = result.size - 1; i >= 0; i--) {
                if (resultingWorkers == allWorkers)
                    return;
                ++resultingWorkers;
                result.get(i).counti++;
            }
        }
    }

    private class WorkersDistributionCalculator {
        private StarSystem system;
        private SystemProd prod;

        //currently remaining workers
        //At the start of the algorithm all workers are unset, thus this is the same as system population at that point.
        private int workersLeftToSet;

        public WorkersDistributionCalculator(StarSystem system) {
            this.system = system;
            this.prod = system.getProduction();
            workersLeftToSet = system.getWorker(WorkerType.ALL_WORKER);
        }

        public void prepare() {
            system.freeAllWorkers();
            system.calculateVariables();
        }

        public void finish() {
            system.calculateVariables();
        }

        /**
         * Increases workers in categories energy and food until we produce enough to suffice for the consumption we have
         * @param type
         * @param allowInsufficient
         * @return
         */
        public boolean increaseWorkersUntilSufficient(WorkerType type, boolean allowInsufficient) {
            if (system.getDisabledProductions()[type.getType()])
                return true;
            while (true) {
                int value = (type == WorkerType.ENERGY_WORKER) ? prod.getEnergyProd() : prod.getFoodProd();
                if (value >= 0 && workersLeftToSet > 0)
                    return true;
                if (workersLeftToSet <= 0)
                    return allowInsufficient;
                int numberOfBuildings = system.getNumberOfWorkBuildings(type, 0);
                int workersSet = system.getWorker(type);
                if (workersSet == numberOfBuildings)
                    return allowInsufficient;
                setWorker(type, SetWorkerMode.SET_WORKER_MODE_INCREMENT);
                system.calculateVariables();
            }
        }

        public void doPriorities(ArrayMap<WorkerType, Integer> priorities, boolean maxIndustry, boolean safeMorale) {
            if (safeMorale)
                --workersLeftToSet;
            ArrayMap<WorkerType, Integer> prios = new ArrayMap<WorkerType, Integer>(true, 8);
            if (priorities.size != 0)
                prios.putAll(priorities);
            doMaxPriorities(prios, maxIndustry);
            DefaultDistributionCalculator decalc = new DefaultDistributionCalculator(workersLeftToSet, prios);
            Array<DistributionElem> result = decalc.calc();
            int failedToSet = 0;
            for (int i = 0; i < result.size; i++) {
                DistributionElem el = result.get(i);
                int buildings = system.getNumberOfWorkBuildings(el.type, 0);
                if (buildings >= el.counti) {
                    int trySet = el.counti;
                    if (el.type != WorkerType.INDUSTRY_WORKER)
                        trySet += failedToSet;
                    if (buildings >= trySet) {
                        if (el.type != WorkerType.INDUSTRY_WORKER)
                            failedToSet = 0;
                        setWorker(el.type, SetWorkerMode.SET_WORKER_MODE_SET, trySet);
                    } else {
                        if (el.type != WorkerType.INDUSTRY_WORKER)
                            failedToSet -= buildings - el.counti;
                        setWorker(el.type, SetWorkerMode.SET_WORKER_MODE_SET, buildings);
                    }
                } else {
                    failedToSet += el.counti - buildings;
                    setWorker(el.type, SetWorkerMode.SET_WORKER_MODE_SET, buildings);
                }
                failedToSet += decrementDueToFullStore(el.type);
                if (el.type == WorkerType.INDUSTRY_WORKER)
                    failedToSet += decrementDueToWastedIndustry(maxIndustry);
            }
            if (safeMorale)
                ++workersLeftToSet;
        }

        /**
         * fills any workers remaining into remaining worker slots, with decreasing order of sense
         * at first we try to only put them if store isn't already full, then we produce industry
         * (gives a little money), at last we produce superfluous energy
         */
        public void doRemaining() {
            if (fillRemainingSlots(WorkerType.FOOD_WORKER))
                decrementDueToFullStore(WorkerType.FOOD_WORKER);

            fillRemainingSlots(WorkerType.RESEARCH_WORKER);

            if (fillRemainingSlots(WorkerType.TITAN_WORKER))
                decrementDueToFullStore(WorkerType.TITAN_WORKER);
            if (fillRemainingSlots(WorkerType.DEUTERIUM_WORKER))
                decrementDueToFullStore(WorkerType.DEUTERIUM_WORKER);
            if (fillRemainingSlots(WorkerType.DURANIUM_WORKER))
                decrementDueToFullStore(WorkerType.DURANIUM_WORKER);
            if (fillRemainingSlots(WorkerType.CRYSTAL_WORKER))
                decrementDueToFullStore(WorkerType.CRYSTAL_WORKER);
            if (fillRemainingSlots(WorkerType.IRIDIUM_WORKER))
                decrementDueToFullStore(WorkerType.IRIDIUM_WORKER);

            fillRemainingSlots(WorkerType.SECURITY_WORKER);

            fillRemainingSlots(WorkerType.INDUSTRY_WORKER);

            fillRemainingSlots(WorkerType.TITAN_WORKER);
            fillRemainingSlots(WorkerType.DEUTERIUM_WORKER);
            fillRemainingSlots(WorkerType.DURANIUM_WORKER);
            fillRemainingSlots(WorkerType.CRYSTAL_WORKER);
            fillRemainingSlots(WorkerType.IRIDIUM_WORKER);
            fillRemainingSlots(WorkerType.FOOD_WORKER);

            fillRemainingSlots(WorkerType.ENERGY_WORKER);
        }

        /**
         * puts an additional worker into industry, to prevent not finishing the project because
         * industry prod decreased due to loss of moral
         */
        public void safeMorale() {
            AssemblyList assemblyList = system.getAssemblyList();
            if (assemblyList.isEmpty() || workersLeftToSet == 0)
                return;
            int maxBuildings = system.getNumberOfWorkBuildings(WorkerType.INDUSTRY_WORKER, 0);
            if (system.getWorker(WorkerType.INDUSTRY_WORKER) < maxBuildings)
                setWorker(WorkerType.INDUSTRY_WORKER, SetWorkerMode.SET_WORKER_MODE_INCREMENT);
        }

        /**
         * fills all remaining empty buildings of the given category, only considering available workers and buildings
         */
        private boolean fillRemainingSlots(WorkerType type) {
            if (workersLeftToSet == 0)
                return false;
            workersLeftToSet += system.getWorker(type);
            system.setWorker(type, SetWorkerMode.SET_WORKER_MODE_SET, 0);
            int buildings = system.getNumberOfWorkBuildings(type, 0);
            int to_set = Math.min(buildings, workersLeftToSet);
            setWorker(type, SetWorkerMode.SET_WORKER_MODE_SET, to_set);
            return true;
        }

        /**
         * removes workers from a category which has a store until production + store <= store,
         * so that nothing will be wasted on next turn change
         */
        private int decrementDueToFullStore(WorkerType type) {
            int unset = 0;
            if (!system.hasStore(type))
                return unset;
            int store = system.getResourceStore(type);
            while (true) {
                int workers = system.getWorker(type);
                if (workers == 0)
                    break;
                system.calculateVariables();
                int production = system.getProduction().getXProd(type);
                if (store + production <= system.getXStoreMax(type))
                    break;
                setWorker(type, SetWorkerMode.SET_WORKER_MODE_DECREMENT);
                unset++;
            }
            return unset;
        }

        private int onBestIndustryWorkerCountFound(int unset) {
            setWorker(WorkerType.INDUSTRY_WORKER, SetWorkerMode.SET_WORKER_MODE_INCREMENT);
            unset--;
            return unset;
        }

        private int decrementDueToWastedIndustry(boolean maxIndustry) {
            system.calculateVariables();
            AssemblyList assemblyList = system.getAssemblyList();
            int unset = 0;
            if (assemblyList.isEmpty() || assemblyList.getWasBuildingBought())
                return unset;
            int minRounds = system.neededRoundsToBuild(0, true);
            if (maxIndustry && minRounds > 1 || system.getWorker(WorkerType.INDUSTRY_WORKER) == 0)
                return unset;
            while (true) {
                setWorker(WorkerType.INDUSTRY_WORKER, SetWorkerMode.SET_WORKER_MODE_DECREMENT);
                ++unset;
                system.calculateVariables();
                int currentRounds = system.neededRoundsToBuild(0, true, false);
                if (system.getWorker(WorkerType.INDUSTRY_WORKER) == 0) {
                    if (minRounds < currentRounds)
                        unset = onBestIndustryWorkerCountFound(unset);
                    return unset;
                }
                if (minRounds < currentRounds) {
                    unset = onBestIndustryWorkerCountFound(unset);
                    return unset;
                }
            }
        }

        /**
         * worker setting function which makes according changes to m_WorkersLeftToSet, tracking how many
         * we still have to distribute at all
         */
        private void setWorker(WorkerType type, SetWorkerMode mode) {
            setWorker(type, mode, -1);
        }

        private void setWorker(WorkerType type, SetWorkerMode mode, int val) {
            if (mode == SetWorkerMode.SET_WORKER_MODE_INCREMENT) {
                system.setWorker(type, mode);
                --workersLeftToSet;
            } else if (mode == SetWorkerMode.SET_WORKER_MODE_DECREMENT) {
                system.setWorker(type, mode);
                ++workersLeftToSet;
            } else if (mode == SetWorkerMode.SET_WORKER_MODE_SET) {
                system.setWorker(type, mode, val);
                workersLeftToSet -= val;
            }
        }

        private void doMaxPriorities(ArrayMap<WorkerType, Integer> prios, boolean maxIndustry) {
            for (int i = 0; i < prios.size; i++) {
                int value = prios.getValueAt(i);
                WorkerType type = prios.getKeyAt(i);
                if (value != SystemManager.max_priority)
                    continue;
                fillRemainingSlots(type);
                decrementDueToFullStore(type);
                if (type == WorkerType.INDUSTRY_WORKER)
                    decrementDueToWastedIndustry(maxIndustry);
                prios.removeIndex(i--);
            }
        }
    }

    //Energy consumers
    class EnergyConsumersChecker {
        private StarSystem system;

        public EnergyConsumersChecker(StarSystem system) {
            this.system = system;
        }

        public boolean shouldTakeShipyardOnline() {
            AssemblyList assemblyList = system.getAssemblyList();
            if (assemblyList.isEmpty())
                return false;
            int assemblyListEntry = assemblyList.getAssemblyListEntry(0).id;
            if (assemblyListEntry < 10000 || assemblyListEntry >= 20000)
                return false;
            return true;
        }

        public boolean checkMorale(BuildingInfo info, Building building) {
            return checkMorale(info, building, max_min_morale, max_min_morale_prod);
        }

        public boolean checkMorale(BuildingInfo info, Building building, int mMorale, int mMoraleProd) {
            if (system.getMorale() < mMorale)
                return false;
            int mp = system.getProduction().getMoraleProd();
            if (building.getIsBuildingOnline())
                return mp >= mMoraleProd;
            return mp + info.getMoraleProd() >= mMoraleProd;
        }

        public boolean checkStoreFull(ResourceTypes type, BuildingInfo info, Building building) {
            int mstore = system.getXStoreMax(type);
            int store = system.getResourceStore(type.getType());
            int prod = info.getXProd(type);
            if (building.getIsBuildingOnline())
                return store + prod <= mstore;
            return store + system.getProduction().getResourceProd(type) + prod <= mstore;
        }

        public boolean bomberInSector() {
            ArrayMap<String, ShipMap> ships = new ArrayMap<String, ShipMap>();
            ships.putAll(system.getShipsInSector());
            for (int i = 0; i < ships.size; i++) {
                ShipMap sm = ships.getValueAt(i);
                DiplomaticAgreement agreement = sm.getAt(0).getOwner().getAgreement(system.getOwnerId());
                if (agreement != DiplomaticAgreement.WAR) {
                    ships.removeIndex(i--);
                }
            }
            return ships.size != 0 && checkShips(ships);
        }

        private boolean checkShips(ArrayMap<String, ShipMap> ships) {
            int scanPower = system.getScanPower(system.getOwnerId(), true);
            for (int i = 0; i < ships.size; i++)
                for (int j = 0; j < ships.getValueAt(i).getSize(); j++) {
                    Ships s = ships.getValueAt(i).getAt(j);
                    if (s.getTorpedoWeapons().size == 0) {
                        Major owner = Major.toMajor(s.getOwner());
                        if (owner != null && !owner.isHumanPlayer())
                            continue;
                    }
                    if (s.isAlien()) {
                        Race alien = s.getOwner();
                        if (alien.getRaceId().equals(Aliens.KAMPFSTATION.getId()))
                            return false;
                    }
                    if (!s.isCloakOn() && system.getNeededScanPower(s.getOwnerId()) <= scanPower)
                        return true;
                }
            return false;
        }
    }

    public SystemManager() {
        active = false;
        safeMorale = false;
        maxIndustry = false;
        neglectFood = false;
        minMorale = max_min_morale;
        minMoraleProd = max_min_morale_prod;
        bombWarning = true;
        onOffline = true;
        ignoredBuildings = new IntSet();
        priorityMap = new ArrayMap<WorkerType, Integer>(true, 8);
    }

    public void reset() {
        active = false;
        safeMorale = false;
        maxIndustry = false;
        neglectFood = false;
        minMorale = max_min_morale;
        minMoraleProd = max_min_morale_prod;
        bombWarning = true;
        onOffline = true;
        ignoredBuildings.clear();
        clearPriorities();
    }

    public boolean isActive() {
        return active;
    }

    public boolean isSafeMorale() {
        return safeMorale;
    }

    public boolean isMaxIndustry() {
        return maxIndustry;
    }

    public boolean isNeglectFood() {
        return neglectFood;
    }

    public int getMinMorale() {
        return minMorale;
    }

    public int getMinMoraleProd() {
        return minMoraleProd;
    }

    public boolean isBombWarning() {
        return bombWarning;
    }

    public boolean isOnOffline() {
        return onOffline;
    }

    public int getPriority(WorkerType type) {
        return priorityMap.get(type) == null ? min_priority : (int) priorityMap.get(type);
    }

    public IntSet getIgnoredBuildings() {
        return ignoredBuildings;
    }

    public boolean isBuildingIgnored(int id) {
        return ignoredBuildings.contains(id);
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public void setSafeMorale(boolean safeMorale) {
        this.safeMorale = safeMorale;
    }

    public void setMaxIndustry(boolean maxIndustry) {
        this.maxIndustry = maxIndustry;
    }

    public void setNeglectFood(boolean neglectFood) {
        this.neglectFood = neglectFood;
    }

    public void setMinMorale(int minMorale) {
        this.minMorale = minMorale;
    }

    public void setMinMoraleProd(int minMoraleProd) {
        this.minMoraleProd = minMoraleProd;
    }

    public void setBombWarning(boolean bombWarning) {
        this.bombWarning = bombWarning;
    }

    public void setOnOffline(boolean onOffline) {
        this.onOffline = onOffline;
    }

    public void clearPriorities() {
        priorityMap.clear();
    }

    public void setPriority(WorkerType type, int value) {
        if (value > min_priority)
            priorityMap.put(type, value);
        else if (value == min_priority)
            priorityMap.removeKey(type);
    }

    public void setIgnoredBuildings(IntSet ignoredBuildings) {
        this.ignoredBuildings = ignoredBuildings;
    }

    public void toggleBuildingIgnored(int id) {
        if (ignoredBuildings.contains(id))
            ignoredBuildings.remove(id);
        else
            ignoredBuildings.add(id);
    }

    public void upgradeIgnoredBuilding(int old_id, int new_id) {
        if (ignoredBuildings.contains(old_id)) {
            ignoredBuildings.remove(old_id);
            ignoredBuildings.add(new_id);
        }
    }

    /**
     * Distributes workers according to priorities and other manager settings
     * @param system
     * @return false in case of an error
     */
    public boolean distributeWorkers(StarSystem system) {
        WorkersDistributionCalculator calc = new WorkersDistributionCalculator(system);
        calc.prepare();
        //energy
        if (!calc.increaseWorkersUntilSufficient(WorkerType.ENERGY_WORKER, false))
            return false;
        //food
        if (!neglectFood || checkFamine(system))
            if (!calc.increaseWorkersUntilSufficient(WorkerType.FOOD_WORKER, true))
                return false;
        //industry, security, research, titan, deuterium, duranium, crystal, iridium
        //in order according to priorities
        calc.doPriorities(priorityMap, maxIndustry, safeMorale);
        if (safeMorale)
            calc.safeMorale();
        //distribute any remaining workers
        calc.doRemaining();
        calc.finish();

        return true;
    }

    public boolean checkEnergyConsumers(ResourceManager manager, StarSystem system) {
        if (!onOffline)
            return false;
        EnergyConsumersChecker checker = new EnergyConsumersChecker(system);
        Array<Building> buildings = system.getAllBuildings();
        boolean bWarning = false;
        boolean defenseChecked = false;
        SystemProd prod = system.getProduction();
        int additionalAvailableEnergy = prod.getAvailableEnergy();
        for (int i = 0; i < buildings.size; i++) {
            Building building = buildings.get(i);
            BuildingInfo info = manager.getBuildingInfo(building.getRunningNumber());
            int needed = info.getNeededEnergy();
            if (needed == 0)
                continue;
            if (ignoredBuildings.contains(info.getRunningNumber()))
                continue;
            boolean shouldBeOnline = building.getIsBuildingOnline();
            if (info.isShipYard() || info.getShipBuildSpeed() > 0)
                shouldBeOnline = checker.shouldTakeShipyardOnline();

            if (info.isDefenseBuilding()) {
                if (bWarning)
                    shouldBeOnline = true;
                else if (!defenseChecked) {
                    bWarning = checker.bomberInSector();
                    shouldBeOnline = bWarning;
                    defenseChecked = true;
                } else
                    shouldBeOnline = false;
            }
            if (info.isDeritiumRefinery())
                shouldBeOnline = checker.checkStoreFull(ResourceTypes.DERITIUM, info, building);

            if (info.getResistance() > 0)
                shouldBeOnline = !system.isTaken();

            if (info.getShipTraining() > 0)
                shouldBeOnline = system.getOwnerOfShip(system.getOwnerId(), true);

            if (info.getTroopTraining() > 0)
                shouldBeOnline = system.getTroops().size != 0;

            boolean minusMorale = false;
            if (info.getMoraleProd() < 0)
                minusMorale = shouldBeOnline = checker.checkMorale(info, building, minMorale, minMoraleProd);

            WorkerType type = info.producesWorkerFull();
            if (type != WorkerType.NONE)
                shouldBeOnline = !system.hasStore(type) || checker.checkStoreFull(type.getResource(), info, building);

            shouldBeOnline = shouldBeOnline || info.isUsefulForProduction();
            shouldBeOnline = shouldBeOnline
                    && (info.onlyImprovesProduction(minusMorale) || (info.getShipBuildSpeed() > 0 && checker
                            .shouldTakeShipyardOnline()));
            shouldBeOnline = shouldBeOnline || info.isUsefulMorale();

            boolean isOnline = building.getIsBuildingOnline();
            if (shouldBeOnline && !isOnline) {
                if (additionalAvailableEnergy >= needed) {
                    building.setIsBuildingOnline(true);
                    additionalAvailableEnergy -= needed;
                    system.calculateVariables();
                }
            } else if (!shouldBeOnline && isOnline) {
                int n = info.getNeededEnergy();
                building.setIsBuildingOnline(false);
                additionalAvailableEnergy += n;
                system.calculateVariables();
            }
        }
        return bWarning && bombWarning;
    }

    public boolean checkFamine(StarSystem system) {
        int foodProd = system.getProduction().getFoodProd();
        if (foodProd >= 0)
            return false;
        int foodStore = system.getFoodStore();
        return foodStore + foodProd < 0;
    }
}