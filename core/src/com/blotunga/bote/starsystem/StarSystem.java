/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.starsystem;

import java.util.Arrays;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.IntArray;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.achievements.AchievementsList;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.PlanetType;
import com.blotunga.bote.constants.RaceProperty;
import com.blotunga.bote.constants.ResearchComplexType;
import com.blotunga.bote.constants.ResearchStatus;
import com.blotunga.bote.constants.ResourceTypes;
import com.blotunga.bote.constants.ShipOrder;
import com.blotunga.bote.constants.SystemOwningStatus;
import com.blotunga.bote.constants.WorkerType;
import com.blotunga.bote.events.EventColonization;
import com.blotunga.bote.galaxy.Planet;
import com.blotunga.bote.galaxy.ResourceRoute;
import com.blotunga.bote.galaxy.Sector;
import com.blotunga.bote.general.EmpireNews;
import com.blotunga.bote.general.GameResources;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.general.EmpireNews.EmpireNewsType;
import com.blotunga.bote.races.Empire;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.races.Minor;
import com.blotunga.bote.races.Race;
import com.blotunga.bote.races.Research;
import com.blotunga.bote.races.ResearchInfo;
import com.blotunga.bote.ships.ShipInfo;
import com.blotunga.bote.ships.Ships;
import com.blotunga.bote.trade.TradeRoute;
import com.blotunga.bote.troops.Troop;
import com.blotunga.bote.troops.TroopInfo;
import com.blotunga.bote.utils.IntPoint;
import com.blotunga.bote.utils.KryoV;
import com.blotunga.bote.utils.Pair;
import com.blotunga.bote.utils.RandUtil;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

public class StarSystem extends Sector implements Comparable<Sector> {
    public enum SystemSortType {
        BY_COORD,
        BY_NAME,
        BY_MORALE,
        BY_FOODPROD,
        BY_FOODSTORAGE,
        BY_IPPROD,
        BY_CREDITSPROD,
        BY_ORDER,
        BY_TITANSTORE,
        BY_DEUTERIUMSTORE,
        BY_DURANIUMSTORE,
        BY_CRYSTALSTORE,
        BY_IRIDIUMSTORE,
        BY_DERITIUMSTORE,
        BY_TROOPS,
        BY_SHIELD,
        BY_SHIPDEFENSE,
        BY_GROUNDDEFENSE,
        BY_SCANSTRENGTH,
        BY_SECTORVALUE
    }

    private static transient SystemSortType sortType = SystemSortType.BY_COORD;

    private SystemOwningStatus owningStatus;
    private double inhabitants;				//the inhabitants of the system
    private AssemblyList assemblyList;		//the production list of the system
    private SystemProd production;
    private Array<Building> buildings;
    private IntArray buildableBuildings;
    private IntArray buildableUpdates;
    private IntArray buildableShips;
    private IntArray buildableTroops;
    private IntArray alwaysBuildableBuildings;
    private IntArray buildableWithoutAssemblylistCheck;
    private Worker workers;
    private int morale;
    private int blockade;
    private boolean[] disabledProductions;
    private GameResources store;
    private IntArray buildingDestroy;
    private int foodBuildings;
    private int industryBuildings;
    private int energyBuildings;
    private int securityBuildings;
    private int researchBuildings;
    private int titanMines;
    private int deuteriumMines;
    private int duraniumMines;
    private int crystalMines;
    private int iridiumMines;
    private Array<TradeRoute> tradeRoutes;
    private Array<ResourceRoute> resourceRoutes;
    private int maxTradeRoutesFromHab;
    private Array<Troop> troops;
    private boolean autoBuild;
    private SystemManager manager;
    private IntPoint buildTargetCoord; //coordinate of the system where ships will be sent to
    private transient int imageIndex;

    private class TurnsCalc { //helper class for neededRoundsToBuild()
        private float speedFactor(int speed) {
            return 1.0f + speed / 100.0f;
        }

        private float effFactor(int efficiency) {
            if (efficiency == 0)
                return 1;
            return efficiency / 100.0f;
        }

        public int turns(float needed, float prod, int speed) {
            return turns(needed, prod, speed, -1);
        }

        public int turns(float needed, float prod, int speed, int eff) {
            float eff_factor = 1.0f;
            if (eff != -1)
                eff_factor = effFactor(eff);
            float turns = needed / (prod * speedFactor(speed) * eff_factor);
            return (int) (Math.ceil(turns));
        }
    }

    public StarSystem() {
        this("", new IntPoint(-1, -1), null);
    }

    public StarSystem(String name, IntPoint position, ResourceManager res) {
        super(res);
        this.name = name;
        this.coordinates = position;
        owningStatus = SystemOwningStatus.OWNING_STATUS_EMPTY;
        inhabitants = 0.0f;

        assemblyList = new AssemblyList();
        production = new SystemProd();
        buildings = new Array<Building>();
        buildableBuildings = new IntArray();
        buildableUpdates = new IntArray();
        buildableShips = new IntArray();
        buildableTroops = new IntArray();
        alwaysBuildableBuildings = new IntArray();
        buildableWithoutAssemblylistCheck = new IntArray();

        workers = new Worker();
        morale = 100;
        blockade = 0;
        disabledProductions = new boolean[WorkerType.IRIDIUM_WORKER.getType() + 1];
        Arrays.fill(disabledProductions, false);
        store = new GameResources();
        buildingDestroy = new IntArray();

        foodBuildings = 0;
        industryBuildings = 0;
        energyBuildings = 0;
        securityBuildings = 0;
        researchBuildings = 0;
        titanMines = 0;
        deuteriumMines = 0;
        duraniumMines = 0;
        iridiumMines = 0;
        crystalMines = 0;

        tradeRoutes = new Array<TradeRoute>();
        resourceRoutes = new Array<ResourceRoute>();
        maxTradeRoutesFromHab = 0;
        troops = new Array<Troop>();

        autoBuild = false;
        manager = new SystemManager();
        imageIndex = -1;
        buildTargetCoord = new IntPoint();
    }

    @Override
    public void reset(boolean callUp) {
        if (callUp)
            super.reset();
        owningStatus = SystemOwningStatus.OWNING_STATUS_EMPTY;
        inhabitants = 0.0f;
        morale = 100;
        blockade = 0;

        foodBuildings = 0;
        industryBuildings = 0;
        energyBuildings = 0;
        securityBuildings = 0;
        researchBuildings = 0;
        titanMines = 0;
        deuteriumMines = 0;
        duraniumMines = 0;
        iridiumMines = 0;
        crystalMines = 0;

        store.reset();
        buildingDestroy.clear();
        production.reset();
        assemblyList.reset();
        buildings.clear();
        buildableBuildings.clear();
        alwaysBuildableBuildings.clear();
        buildableUpdates.clear();
        buildableWithoutAssemblylistCheck.clear();
        buildableShips.clear();
        buildableTroops.clear();
        tradeRoutes.clear();
        resourceRoutes.clear();
        maxTradeRoutesFromHab = 0;
        troops.clear();
        autoBuild = false;
        Arrays.fill(disabledProductions, false);
        manager.reset();
        imageIndex = -1;
        buildTargetCoord = new IntPoint();
    }

    public boolean isMajorized() {
        return (owningStatus == SystemOwningStatus.OWNING_STATUS_COLONIZED_MEMBERSHIP_OR_HOME || owningStatus == SystemOwningStatus.OWNING_STATUS_TAKEN);
    }

    public boolean isTaken() {
        return (owningStatus == SystemOwningStatus.OWNING_STATUS_TAKEN);
    }

    public boolean hasRebelled() {
        return (owningStatus == SystemOwningStatus.OWNING_STATUS_REBELLED);
    }

    public boolean independentMinor() {
        return (owningStatus == SystemOwningStatus.OWNING_STATUS_INDEPENDENT_MINOR);
    }

    public double getInhabitants() {
        return inhabitants;
    }

    public IntArray getBuildableBuildings() {
        return buildableBuildings;
    }

    public IntArray getBuildableUpdates() {
        return buildableUpdates;
    }

    public IntArray getBuildableShips() {
        return buildableShips;
    }

    public IntArray getBuildableTroops() {
        return buildableTroops;
    }

    /**
     * Returns the either runningnumber of the building or the number of buildings
     * @param worker
     * @param mode if 0 then we return the number of buildings of the type, otherwise the runningnumber
     * @return
     */
    public int getNumberOfWorkBuildings(WorkerType worker, int mode) {
        if (mode == 0) {
            switch (worker) {
                case FOOD_WORKER:
                    return foodBuildings;
                case INDUSTRY_WORKER:
                    return industryBuildings;
                case ENERGY_WORKER:
                    return energyBuildings;
                case SECURITY_WORKER:
                    return securityBuildings;
                case RESEARCH_WORKER:
                    return researchBuildings;
                case TITAN_WORKER:
                    return titanMines;
                case DEUTERIUM_WORKER:
                    return deuteriumMines;
                case DURANIUM_WORKER:
                    return duraniumMines;
                case CRYSTAL_WORKER:
                    return crystalMines;
                case IRIDIUM_WORKER:
                    return iridiumMines;
                case ALL_WORKER:
                    return workers.getWorker(WorkerType.ALL_WORKER);
                default:
                    return 0;
            }
        } else if (mode == 1) {
            for (Building building : buildings) {
                BuildingInfo info = resourceManager.getBuildingInfos().get(building.getRunningNumber() - 1);
                if (info.getWorker()) {
                    int runningNumber = 0;
                    if (worker == WorkerType.FOOD_WORKER) {
                        if (info.getFoodProd() != 0) {
                            runningNumber = building.getRunningNumber();
                            return runningNumber;
                        }
                    } else if (worker == WorkerType.INDUSTRY_WORKER) {
                        if (info.getIndustryPointsProd() != 0) {
                            runningNumber = building.getRunningNumber();
                            return runningNumber;
                        }
                    } else if (worker == WorkerType.ENERGY_WORKER) {
                        if (info.getEnergyProd() != 0) {
                            runningNumber = building.getRunningNumber();
                            return runningNumber;
                        }
                    } else if (worker == WorkerType.SECURITY_WORKER) {
                        if (info.getSecurityPointsProd() != 0) {
                            runningNumber = building.getRunningNumber();
                            return runningNumber;
                        }
                    } else if (worker == WorkerType.RESEARCH_WORKER) {
                        if (info.getResearchPointsProd() != 0) {
                            runningNumber = building.getRunningNumber();
                            return runningNumber;
                        }
                    } else if (worker == WorkerType.TITAN_WORKER) {
                        if (info.getTitanProd() != 0) {
                            runningNumber = building.getRunningNumber();
                            return runningNumber;
                        }
                    } else if (worker == WorkerType.DEUTERIUM_WORKER) {
                        if (info.getDeuteriumProd() != 0) {
                            runningNumber = building.getRunningNumber();
                            return runningNumber;
                        }
                    } else if (worker == WorkerType.DURANIUM_WORKER) {
                        if (info.getDuraniumProd() != 0) {
                            runningNumber = building.getRunningNumber();
                            return runningNumber;
                        }
                    } else if (worker == WorkerType.CRYSTAL_WORKER) {
                        if (info.getCrystalProd() != 0) {
                            runningNumber = building.getRunningNumber();
                            return runningNumber;
                        }
                    } else if (worker == WorkerType.IRIDIUM_WORKER) {
                        if (info.getIridiumProd() != 0) {
                            runningNumber = building.getRunningNumber();
                            return runningNumber;
                        }
                    }
                }
            }
        }
        return 0;
    }

    /**
     * Returns the count of buildings with the given runningnumber
     * @param runningNumber
     * @return
     */
    public int getNumberofBuilding(int runningNumber) {
        int count = 0;
        for (Building building : buildings)
            if (runningNumber == building.getRunningNumber())
                count++;
        return count;
    }

    /**
     * Returns the number of active workers of given type
     * @param worker
     * @return
     */
    public int getWorker(WorkerType worker) {
        return workers.getWorker(worker);
    }

    public int getMorale() {
        return morale;
    }

    /**
     * Returns the blockade value (percentage) in the system, 0 meaning no blockade
     * @return
     */
    public int getBlockade() {
        return blockade;
    }

    public boolean[] getDisabledProductions() {
        return disabledProductions;
    }

    public int getFoodStore() {
        return store.food;
    }

    public int getTitanStore() {
        return store.titan;
    }

    public int getDeuteriumStore() {
        return store.deuterium;
    }

    public int getDuraniumStore() {
        return store.duranium;
    }

    public int getCrystalStore() {
        return store.crystal;
    }

    public int getIridiumStore() {
        return store.iridium;
    }

    public int getDeritiumStore() {
        return store.deritium;
    }

    static public int getFoodStoreMax() {
        return GameConstants.MAX_FOOD_STORE;
    }

    static public int getTitanStoreMax() {
        return GameConstants.MAX_RES_STORE;
    }

    static public int getDeuteriumStoreMax() {
        return GameConstants.MAX_RES_STORE;
    }

    static public int getDuraniumStoreMax() {
        return GameConstants.MAX_RES_STORE;
    }

    static public int getCrystalStoreMax() {
        return GameConstants.MAX_RES_STORE;
    }

    static public int getIridiumStoreMax() {
        return GameConstants.MAX_RES_STORE;
    }

    public int getDeritiumStoreMax() {
        int multi = 1;
        ResearchInfo info = resourceManager.getRaceController().getRace(ownerID).getEmpire().getResearch()
                .getResearchInfo();
        if (info.getResearchComplex(ResearchComplexType.STORAGE_AND_TRANSPORT).getFieldStatus(1) == ResearchStatus.RESEARCHED)
            multi = info.getResearchComplex(ResearchComplexType.STORAGE_AND_TRANSPORT).getBonus(1);
        return GameConstants.MAX_DERITIUM_STORE * multi;
    }

    public int getXStoreMax(ResourceTypes x) {
        switch (x) {
            case TITAN:
                return getTitanStoreMax();
            case DEUTERIUM:
                return getDeuteriumStoreMax();
            case DURANIUM:
                return getDuraniumStoreMax();
            case CRYSTAL:
                return getCrystalStoreMax();
            case IRIDIUM:
                return getIridiumStoreMax();
            case DERITIUM:
                return getDeritiumStoreMax();
            case FOOD:
                return getFoodStoreMax();
        }
        return 0;
    }

    public int getXStoreMax(WorkerType x) {
        if (hasStore(x))
            return getXStoreMax(x.getResource());
        else
            return 0;
    }

    /**
     * Function returns the number of stored resources of a type
     * @param res
     * @return
     */
    public int getResourceStore(int res) {
        return store.getResource(res);
    }

    public void setResourceStore(int res, int val) {
        store.setResource(res, val);
    }

    public int getResourceStore(WorkerType type) {
        if (hasStore(type))
            return getResourceStore(type.getResource().getType());
        else
            return 0;
    }

    /**
     * Function returns the number of the buildings with runningnumber which have to be destroyed in the next turn
     * @param runningNumber
     * @return
     */
    public int getBuildingDestroy(int runningNumber) {
        int count = 0;
        for (int i = 0; i < buildingDestroy.size; i++)
            if (buildingDestroy.get(i) == runningNumber)
                count++;
        return count;
    }

    private int getXBuildings(WorkerType x) {
        switch (x) {
            case FOOD_WORKER:
                return foodBuildings;
            case INDUSTRY_WORKER:
                return industryBuildings;
            case ENERGY_WORKER:
                return energyBuildings;
            case SECURITY_WORKER:
                return securityBuildings;
            case RESEARCH_WORKER:
                return researchBuildings;
            case TITAN_WORKER:
                return titanMines;
            case DEUTERIUM_WORKER:
                return deuteriumMines;
            case DURANIUM_WORKER:
                return duraniumMines;
            case CRYSTAL_WORKER:
                return crystalMines;
            case IRIDIUM_WORKER:
                return iridiumMines;
            default:
                return 0;
        }
    }

    public AssemblyList getAssemblyList() {
        return assemblyList;
    }

    public SystemManager getManager() {
        return manager;
    }

    public SystemProd getProduction() {
        return production;
    }

    public Array<Building> getAllBuildings() {
        return buildings;
    }

    public Array<TradeRoute> getTradeRoutes() {
        return tradeRoutes;
    }

    public Array<ResourceRoute> getResourceRoutes() {
        return resourceRoutes;
    }

    public Array<Troop> getTroops() {
        return troops;
    }

    /**
     * Sets the number of inhabitants
     * @param inhabitants
     * @return true if a new traderoute can be opened, false otherwise
     */
    public boolean setInhabitants(double inhabitants) {
        this.inhabitants = inhabitants;
        if ((int) (inhabitants / GameConstants.TRADEROUTEHAB) > maxTradeRoutesFromHab) {
            maxTradeRoutesFromHab++;
            if (isMajorized())
                return true;
        }
        return false;
    }

    /**
     * Function decides if a building in a sector buildable is or not
     * @param runningNumber
     * @param val
     */
    public void setBuildableBuildings(int runningNumber, boolean val) {
        if (val == false) {
            for (int i = 0; i < buildableBuildings.size; i++)
                if (buildableBuildings.get(i) == runningNumber) {
                    buildableBuildings.removeIndex(i);
                    break;
                }
        } else
            buildableBuildings.add(runningNumber);
    }

    public void setBuildableUpdates(int runningNumber, boolean val) {
        if (val == false) {
            for (int i = 0; i < buildableUpdates.size; i++)
                if (buildableUpdates.get(i) == runningNumber) {
                    buildableUpdates.removeIndex(i);
                    break;
                }
        } else
            buildableUpdates.add(runningNumber);
    }

    public void setIsBuildingOnline(int index, boolean newStatus) {
        buildings.get(index).setIsBuildingOnline(newStatus);
    }

    public void setMorale(int moraleAdd) {
        if (morale + moraleAdd >= 0)
            morale += moraleAdd;
        if (morale > GameConstants.MAX_MORALE)
            morale = GameConstants.MAX_MORALE;
    }

    public void setBlockade(int blockadeValue) {
        blockade = Math.min(blockadeValue, 100);
    }

    public void setDisabledProduction(int type) {
        disabledProductions[type] = true;
    }

    public void setFoodStore(int food) {
        store.food = food;
    }

    public void setTitanStore(int titanAdd) {
        store.titan += titanAdd;
    }

    public void setDeuteriumStore(int deuteriumAdd) {
        store.deuterium += deuteriumAdd;
    }

    public void setDuraniumStore(int duraniumAdd) {
        store.duranium += duraniumAdd;
    }

    public void setCrystalStore(int crystalAdd) {
        store.crystal += crystalAdd;
    }

    public void setIridiumStore(int iridiumAdd) {
        store.iridium += iridiumAdd;
    }

    public void setDeritiumStore(int deritiumAdd) {
        store.deritium += deritiumAdd;
    }

    public void setStores(GameResources add) {
        store.add(add);
    }

    public void addResourceStore(int res, int resAdd) {
        //make sure the resourcestorage won't go into the negative
        if (resAdd < 0)
            if (resAdd + getResourceStore(res) < 0)
                resAdd = getResourceStore(res) * (-1);
        store.addResource(res, resAdd);
    }

    public IntPoint getBuildTargetCoord() {
        return buildTargetCoord;
    }

    public void setBuildTargetCoord(IntPoint p) {
        buildTargetCoord = new IntPoint(p);
    }

    /**
     * @param runningNumber
     * @param add true if set the building to the list of the buildings which have to be deleted
     */
    public void setBuildingDestroy(int runningNumber, boolean add) {
        if (add == true) {
            int maxDestroy = getNumberofBuilding(runningNumber);
            if (blockade > 0)
                maxDestroy -= maxDestroy * blockade / 100;
            if (maxDestroy > getBuildingDestroy(runningNumber))
                buildingDestroy.add(runningNumber);
        } else {
            for (int i = 0; i < buildingDestroy.size; i++)
                if (buildingDestroy.get(i) == runningNumber) {
                    buildingDestroy.removeIndex(i);
                    break;
                }
        }
    }

    public void setAutoBuild(boolean autoBuild) {
        this.autoBuild = autoBuild;
    }

    public void clearDisabledProductions() {
        Arrays.fill(disabledProductions, false);
    }

    //////////////////////////////////////////
    //Worker stuff
    ////////////////////////////////////////
    public enum SetWorkerMode {
        SET_WORKER_MODE_INCREMENT, SET_WORKER_MODE_DECREMENT, SET_WORKER_MODE_SET
    }

    public void setWorker(WorkerType whichWorker, SetWorkerMode mode) {
        setWorker(whichWorker, mode, -1);
    }

    public void setWorker(WorkerType whichWorker, SetWorkerMode mode, int value) {
        if (mode == SetWorkerMode.SET_WORKER_MODE_INCREMENT)
            incrementWorker(whichWorker);
        else if (mode == SetWorkerMode.SET_WORKER_MODE_DECREMENT)
            decrementWorker(whichWorker);
        else if (mode == SetWorkerMode.SET_WORKER_MODE_SET) {
            workers.setWorker(whichWorker, value);
        }
    }

    public void incrementWorker(WorkerType whichWorker) {
        workers.incrementWorker(whichWorker);
    }

    public void decrementWorker(WorkerType whichWorker) {
        workers.decrementWorker(whichWorker);
    }

    public void setWorkersIntoBuildings() {
        setWorker(WorkerType.ALL_WORKER, SetWorkerMode.SET_WORKER_MODE_SET, (int) inhabitants);

        while (workers.getWorker(WorkerType.FREE_WORKER) > 0) {
            boolean allBuildingsFull = true;
            for (int i = WorkerType.FOOD_WORKER.getType(); i <= WorkerType.IRIDIUM_WORKER.getType(); i++) {
                WorkerType worker = WorkerType.fromWorkerType(i);
                if (workers.getWorker(WorkerType.FREE_WORKER) > 0
                        && getNumberOfWorkBuildings(worker, 0) > workers.getWorker(worker)) {
                    allBuildingsFull = false;
                    incrementWorker(worker);
                }
            }
            if (allBuildingsFull)
                break;
        }
    }

    public void freeAllWorkers() {
        workers.freeAll();
    }

    public boolean hasWorkerBuilding(WorkerType type) {
        return getXBuildings(type) > 0;
    }

    public boolean hasStore(WorkerType type) {
        return type.getResource() != null;
    }

    public boolean getAutoBuild() {
        return autoBuild;
    }

    public boolean canTakeOnline(BuildingInfo info) {
        if (manager.isActive())
            return info.getNeededEnergy() <= production.getAvailableEnergy();
        return production.getEnergyProd() >= info.getNeededEnergy();
    }

    public boolean sanityCheckWorkers() {
        boolean inRange = sanityCheckWorkersInRange(WorkerType.FOOD_WORKER)
                && sanityCheckWorkersInRange(WorkerType.INDUSTRY_WORKER)
                && sanityCheckWorkersInRange(WorkerType.ENERGY_WORKER)
                && sanityCheckWorkersInRange(WorkerType.SECURITY_WORKER)
                && sanityCheckWorkersInRange(WorkerType.RESEARCH_WORKER)
                && sanityCheckWorkersInRange(WorkerType.TITAN_WORKER)
                && sanityCheckWorkersInRange(WorkerType.DEUTERIUM_WORKER)
                && sanityCheckWorkersInRange(WorkerType.DURANIUM_WORKER)
                && sanityCheckWorkersInRange(WorkerType.CRYSTAL_WORKER)
                && sanityCheckWorkersInRange(WorkerType.IRIDIUM_WORKER);
        if (!inRange)
            return false;
        return (workers.getWorker(WorkerType.ALL_WORKER) == workers.getWorker(WorkerType.FREE_WORKER)
                + workers.getWorker(WorkerType.FOOD_WORKER) + workers.getWorker(WorkerType.INDUSTRY_WORKER)
                + workers.getWorker(WorkerType.ENERGY_WORKER) + workers.getWorker(WorkerType.SECURITY_WORKER)
                + workers.getWorker(WorkerType.RESEARCH_WORKER) + workers.getWorker(WorkerType.TITAN_WORKER)
                + workers.getWorker(WorkerType.DEUTERIUM_WORKER) + workers.getWorker(WorkerType.DURANIUM_WORKER)
                + workers.getWorker(WorkerType.CRYSTAL_WORKER) + workers.getWorker(WorkerType.IRIDIUM_WORKER));
    }

    private boolean sanityCheckWorkersInRange(WorkerType type) {
        int worker = workers.getWorker(type);
        return 0 <= worker && worker <= getNumberOfWorkBuildings(type, 0);
    }

    private int addBonusToProd(int prod, int bonus) {
        return prod + bonus * prod / 100;
    }

    private int getPlanetBonusFromSize(int size) {
        return size * 25;
    }

    /**
     * Function calculates from the buildings which are built all attributes of the system
     */
    public void calculateVariables() {
        Race owner = resourceManager.getRaceController().getRace(ownerID);
        if (owner == null || !owner.isMajor())
            return;
        ResearchInfo researchInfo = owner.getEmpire().getResearch().getResearchInfo();
        Array<BuildingInfo> buildingInfos = resourceManager.getBuildingInfos();
        int numberOfBuildings = buildings.size;
        production.reset();
        setWorker(WorkerType.ALL_WORKER, SetWorkerMode.SET_WORKER_MODE_SET, (int) inhabitants);

        production.setCreditsProd((int) inhabitants);
        production.setCreditsProd((int) (production.getCreditsProd() * ((Major) owner).creditsMulti()));

        IntArray wk = new IntArray(10);
        for (int i = WorkerType.FOOD_WORKER.getType(); i <= WorkerType.IRIDIUM_WORKER.getType(); i++) {
            WorkerType type = WorkerType.fromWorkerType(i);
            wk.add(workers.capacity(type, getXBuildings(type)));
        }
        // if we have trade in the buildlist, the number of online industries is transformed into credits
        if (assemblyList.isEmpty())
            production.addCreditsProd(workers.getWorker(WorkerType.INDUSTRY_WORKER));

        //calculate each production
        int usableIndustryBuildings = 0; //tracking variables for industry/energy potentials to not use more than ALL_WORKERS
        int usableEnergyBuildings = 0;
        for (int i = 0; i < numberOfBuildings; i++) {
            BuildingInfo buildingInfo = buildingInfos.get(buildings.get(i).getRunningNumber() - 1);
            Building building = buildings.get(i);
            //in case of a blockade the shipyards are set to offline
            if (blockade >= 100 && buildingInfo.isShipYard() && buildingInfo.getNeededEnergy() > 0)
                building.setIsBuildingOnline(false);

            //set buildings offline
            if (buildingInfo.getWorker() == true) {
                building.setIsBuildingOnline(false);

                //now if possible set online
                for (int j = 0; j < wk.size; j++)
                    if (wk.get(j) > 0 && buildingInfo.getXProd(WorkerType.fromWorkerType(j)) > 0) {
                        wk.set(j, wk.get(j) - 1);
                        building.setIsBuildingOnline(true);
                    }
                if (buildingInfo.getEnergyProd() > 0)
                    ++usableEnergyBuildings;
                if (buildingInfo.getIndustryPointsProd() > 0)
                    ++usableIndustryBuildings;
            }
            //calculate the productions (without bonus)
            production.calculateProduction(buildingInfo, building.getIsBuildingOnline(),
                    usableIndustryBuildings <= workers.getWorker(WorkerType.ALL_WORKER) || !buildingInfo.getWorker(),
                    usableEnergyBuildings <= workers.getWorker(WorkerType.ALL_WORKER) || !buildingInfo.getWorker());
        }
        //if there are disabled productions, set to 0
        production.disableProduction(disabledProductions);
        //calculate credits through trade routes
        production.addCreditsProd(creditsFromTradeRoutes());

        String[] monopolOwner = resourceManager.getMonopolOwners();
        if (monopolOwner[ResourceTypes.TITAN.getType()].equals(ownerID))
            production.titanProd *= 2;
        if (monopolOwner[ResourceTypes.DEUTERIUM.getType()].equals(ownerID))
            production.deuteriumProd *= 2;
        if (monopolOwner[ResourceTypes.DURANIUM.getType()].equals(ownerID))
            production.duraniumProd *= 2;
        if (monopolOwner[ResourceTypes.CRYSTAL.getType()].equals(ownerID))
            production.crystalProd *= 2;
        if (monopolOwner[ResourceTypes.IRIDIUM.getType()].equals(ownerID))
            production.iridiumProd *= 2;

        Research research = getOwner().getEmpire().getResearch();
        int tmpFoodBonus = (int) (research.getBioTech() * GameConstants.TECH_PROD_BONUS);
        int tmpIndustryBonus = (int) (research.getConstructionTech() * GameConstants.TECH_PROD_BONUS);
        int tmpEnergyBonus = (int) (research.getEnergyTech() * GameConstants.TECH_PROD_BONUS);
        int tmpSecurityBonus = (int) (research.getWeaponTech() * GameConstants.TECH_PROD_BONUS);
        int tmpResearchBonus = (int) (research.getComputerTech() * GameConstants.TECH_PROD_BONUS);
        int tmpTitanBonus = 0;
        int tmpDeuteriumBonus = 0;
        int tmpDuraniumBonus = 0;
        int tmpCrystalBonus = 0;
        int tmpIridiumBonus = 0;
        int tmpDeritiumBonus = 0;
        int tmpCreditsBonus = 0;
        int tmpAllResourcesBonus = 0;

        int neededEnergy = 0;
        for (int i = 0; i < numberOfBuildings; i++) {
            BuildingInfo buildingInfo = buildingInfos.get(buildings.get(i).getRunningNumber() - 1);
            Building building = buildings.get(i);
            // calculate needed energy by the buildings
            if (building.getIsBuildingOnline() && buildingInfo.getNeededEnergy() > 0)
                neededEnergy += buildingInfo.getNeededEnergy();
            if (building.getIsBuildingOnline()) {
                tmpFoodBonus += buildingInfo.getFoodBonus();
                tmpIndustryBonus += buildingInfo.getIndustryBonus();
                tmpEnergyBonus += buildingInfo.getEnergyBonus();
                tmpSecurityBonus += buildingInfo.getSecurityBonus();
                tmpResearchBonus += buildingInfo.getResearchBonus();
                tmpAllResourcesBonus = buildingInfo.getAllResourceBonus();
                tmpTitanBonus += buildingInfo.getTitanBonus() + tmpAllResourcesBonus;
                tmpDeuteriumBonus += buildingInfo.getDeuteriumBonus() + tmpAllResourcesBonus;
                tmpDuraniumBonus += buildingInfo.getDuraniumBonus() + tmpAllResourcesBonus;
                tmpCrystalBonus += buildingInfo.getCrystalBonus() + tmpAllResourcesBonus;
                tmpIridiumBonus += buildingInfo.getIridiumBonus() + tmpAllResourcesBonus;
                tmpDeritiumBonus += buildingInfo.getDeritiumBonus();
                tmpCreditsBonus += buildingInfo.getCreditsBonus();
            }
        }
        //now add possible bonuses caused by planet types
        int deritiumProdMulti = 0;
        for (Planet p : planets) {
            if (!p.getIsInhabited())
                continue;
            int size_plus_one = p.getSize().getSize() + 1;
            boolean[] bonuses = p.getBonuses();
            //for each planetsize there is a 25% bonus
            if (bonuses[ResourceTypes.TITAN.getType()])
                tmpTitanBonus += getPlanetBonusFromSize(size_plus_one);
            if (bonuses[ResourceTypes.DEUTERIUM.getType()])
                tmpDeuteriumBonus += getPlanetBonusFromSize(size_plus_one);
            if (bonuses[ResourceTypes.DURANIUM.getType()])
                tmpDuraniumBonus += getPlanetBonusFromSize(size_plus_one);
            if (bonuses[ResourceTypes.CRYSTAL.getType()])
                tmpCrystalBonus += getPlanetBonusFromSize(size_plus_one);
            if (bonuses[ResourceTypes.IRIDIUM.getType()])
                tmpIridiumBonus += getPlanetBonusFromSize(size_plus_one);
            if (bonuses[ResourceTypes.FOOD.getType()])
                tmpFoodBonus += getPlanetBonusFromSize(size_plus_one);
            if (bonuses[7])
                tmpEnergyBonus += getPlanetBonusFromSize(size_plus_one);
            //quantity of deritium multiply by the number of colonised planets which have deritium
            if (bonuses[ResourceTypes.DERITIUM.getType()])
                deritiumProdMulti += 1;
        }
        production.deritiumProd *= deritiumProdMulti;

        //modify bonuses according to race properties
        Minor minor = getMinorRace();
        if (minor != null) {
            tmpFoodBonus += minor.getRaceMod(Minor.RaceModType.RACE_MOD_TYPE_FOOD);
            tmpIndustryBonus += minor.getRaceMod(Minor.RaceModType.RACE_MOD_TYPE_INDUSTRY);
            tmpEnergyBonus += minor.getRaceMod(Minor.RaceModType.RACE_MOD_TYPE_ENERGY);
            tmpSecurityBonus += minor.getRaceMod(Minor.RaceModType.RACE_MOD_TYPE_SECURITY);
            tmpResearchBonus += minor.getRaceMod(Minor.RaceModType.RACE_MOD_TYPE_RESEARCH);
            tmpAllResourcesBonus += minor.getRaceMod(Minor.RaceModType.RACE_MOD_TYPE_ALL_RESOURCES);
            tmpTitanBonus += tmpAllResourcesBonus;
            tmpDeuteriumBonus += tmpAllResourcesBonus;
            tmpDuraniumBonus += tmpAllResourcesBonus;
            tmpCrystalBonus += tmpAllResourcesBonus;
            tmpIridiumBonus += tmpAllResourcesBonus;
            tmpCreditsBonus += minor.getRaceMod(Minor.RaceModType.RACE_MOD_TYPE_CREDITS);
        }
        production.foodProd = addBonusToProd(production.foodProd, tmpFoodBonus);
        production.industryProd = addBonusToProd(production.industryProd, tmpIndustryBonus);
        production.potentialIndustryProd = addBonusToProd(production.potentialIndustryProd, tmpIndustryBonus);
        production.energyProd = addBonusToProd(production.energyProd, tmpEnergyBonus);
        production.potentialEnergyProd = addBonusToProd(production.potentialEnergyProd, tmpEnergyBonus);
        production.securityProd = addBonusToProd(production.securityProd, tmpSecurityBonus);
        production.researchProd = addBonusToProd(production.researchProd, tmpResearchBonus);
        production.titanProd = addBonusToProd(production.titanProd, tmpTitanBonus);
        production.deuteriumProd = addBonusToProd(production.deuteriumProd, tmpDeuteriumBonus);
        production.duraniumProd = addBonusToProd(production.duraniumProd, tmpDuraniumBonus);
        production.crystalProd = addBonusToProd(production.crystalProd, tmpCrystalBonus);
        production.iridiumProd = addBonusToProd(production.iridiumProd, tmpIridiumBonus);
        production.deritiumProd = addBonusToProd(production.deritiumProd, tmpDeritiumBonus);
        production.creditsProd = addBonusToProd(production.creditsProd, tmpCreditsBonus);

        //if the system is under a blockade, some production numbers are lowered
        if (blockade > 0) {
            int bonus = (-1) * blockade;
            production.industryProd = addBonusToProd(production.industryProd, bonus);
            production.potentialIndustryProd = addBonusToProd(production.potentialIndustryProd, bonus);
            production.securityProd = addBonusToProd(production.securityProd, bonus);
            production.researchProd = addBonusToProd(production.researchProd, bonus);
            production.titanProd = addBonusToProd(production.titanProd, bonus);
            production.deuteriumProd = addBonusToProd(production.deuteriumProd, bonus);
            production.duraniumProd = addBonusToProd(production.duraniumProd, bonus);
            production.crystalProd = addBonusToProd(production.crystalProd, bonus);
            production.iridiumProd = addBonusToProd(production.iridiumProd, bonus);
            production.deritiumProd = addBonusToProd(production.deritiumProd, bonus);
            production.creditsProd = addBonusToProd(production.creditsProd, bonus);
            if (blockade >= 100)
                production.shipyard = false;
        }
        // add bonuses to the special stuff (scanpower etc)
        production.scanPower = addBonusToProd(production.scanPower, production.scanPowerBonus);
        production.scanRange = addBonusToProd(production.scanRange, production.scanRangeBonus);
        production.groundDefend = addBonusToProd(production.groundDefend, production.groundDefendBonus);
        production.shipDefend = addBonusToProd(production.shipDefend, production.shipDefendBonus);
        production.shieldPower = addBonusToProd(production.shieldPower, production.shieldPowerBonus);

        ///Bonuses by unique research!
        int bonus;
        bonus = researchInfo.isResearchedThenGetBonus(ResearchComplexType.ECONOMY, 1);
        if (bonus != 0) {
            production.industryProd = addBonusToProd(production.industryProd, bonus);
            production.potentialIndustryProd = addBonusToProd(production.potentialIndustryProd, bonus);
        } else {
            bonus = researchInfo.isResearchedThenGetBonus(ResearchComplexType.ECONOMY, 3);
            if (bonus != 0)
                production.creditsProd = addBonusToProd(production.creditsProd, bonus);
        }
        if (isHomeOf() != null && isHomeOf().getRaceId().equals(ownerID)) {
            bonus = researchInfo.isResearchedThenGetBonus(ResearchComplexType.FINANCES, 3);
            if (bonus != 0)
                production.creditsProd = addBonusToProd(production.creditsProd, bonus);
        }

        bonus = researchInfo.isResearchedThenGetBonus(ResearchComplexType.PRODUCTION, 1);
        if (bonus != 0)
            production.foodProd = addBonusToProd(production.foodProd, bonus);
        else {
            bonus = researchInfo.isResearchedThenGetBonus(ResearchComplexType.PRODUCTION, 2);
            if (bonus != 0) {
                production.titanProd = addBonusToProd(production.titanProd, bonus);
                production.deuteriumProd = addBonusToProd(production.deuteriumProd, bonus);
                production.duraniumProd = addBonusToProd(production.duraniumProd, bonus);
                production.crystalProd = addBonusToProd(production.crystalProd, bonus);
                production.iridiumProd = addBonusToProd(production.iridiumProd, bonus);
            } else {
                bonus = researchInfo.isResearchedThenGetBonus(ResearchComplexType.PRODUCTION, 3);
                if (bonus != 0) {
                    production.energyProd = addBonusToProd(production.energyProd, bonus);
                    production.potentialEnergyProd = addBonusToProd(production.potentialEnergyProd, bonus);
                }
            }
        }

        bonus = researchInfo.isResearchedThenGetBonus(ResearchComplexType.DEVELOPMENT_AND_SECURITY, 1);
        if (bonus != 0)
            production.researchProd = addBonusToProd(production.researchProd, bonus);
        else {
            bonus = researchInfo.isResearchedThenGetBonus(ResearchComplexType.DEVELOPMENT_AND_SECURITY, 2);
            if (bonus != 0)
                production.securityProd = addBonusToProd(production.securityProd, bonus);
            else {
                bonus = researchInfo.isResearchedThenGetBonus(ResearchComplexType.DEVELOPMENT_AND_SECURITY, 3);
                if (bonus != 0) {
                    production.researchProd = addBonusToProd(production.researchProd, bonus);
                    production.securityProd = addBonusToProd(production.securityProd, bonus);
                }
            }
        }

        production.maxEnergyProd = production.energyProd;
        production.energyProd -= neededEnergy;

        production.moraleProd += getOwner().getEmpire().getMoraleEmpireWide();
        production.includeSystemMorale(morale);
        if (!owner.hasSpecialAbility(Race.RaceSpecialAbilities.SPECIAL_NEED_NO_FOOD.getAbility()))
            production.foodProd -= (int) Math.ceil(inhabitants * 10);
        else
            production.foodProd = production.maxFoodProd;
    }

    /**
     * Function calculates the storage of system. Has to be called at the end/beginning of a turn.
     * @param researchInfo
     * @param diliAdd
     * @return true if the system has rebelled because of bad morale
     */
    public boolean calculateStorages(ResearchInfo researchInfo, int diliAdd) {
        store.food += production.foodProd;
        store.titan += production.titanProd;
        store.deuterium += production.deuteriumProd;
        store.duranium += production.duraniumProd;
        store.crystal += production.crystalProd;
        store.iridium += production.iridiumProd;
        store.deritium += production.deritiumProd + diliAdd;

        store.cap();

        int multi = 1;
        ///Bonuses by unique research!
        if (researchInfo.getResearchComplex(ResearchComplexType.STORAGE_AND_TRANSPORT).getFieldStatus(1) == ResearchStatus.RESEARCHED)
            multi = researchInfo.getResearchComplex(ResearchComplexType.STORAGE_AND_TRANSPORT).getBonus(1);
        if (store.deritium > GameConstants.MAX_DERITIUM_STORE * multi)
            store.deritium = GameConstants.MAX_DERITIUM_STORE * multi;

        morale += production.moraleProd;

        // if the morale produced is -1, then the morale is reduced at most to 85
        if (morale < 85 && production.moraleProd == -1)
            morale = 85;
        else if (morale < 70 && production.moraleProd == -2)
            morale = 70;

        //calcuate morale
        if (morale <= 30)
            morale -= 4;
        else if (morale <= 40)
            morale -= 3;
        else if (morale <= 50)
            morale -= 2;
        else if (morale <= 60)
            morale -= 1;
        else if (morale >= 190)
            morale -= 5;
        else if (morale >= 173)
            morale -= 4;
        else if (morale >= 152)
            morale -= 3;
        else if (morale >= 121)
            morale -= 2;
        else if (morale >= 110)
            morale -= 1;

        //without more moraleprod, it goes to 100 if it's lower than 110
        if (morale > 100 && morale <= 109 && production.moraleProd == 0)
            morale--;
        if (morale > 200)
            morale -= 6;
        if (morale < 0)
            morale = 0;
        if (morale > 200)
            morale = 200;

        //check if the system rebells if morale is under 30
        if (morale < 30) {
            int rand = (int) (RandUtil.random() * (morale + 1));
            if (rand < 8) {
                production.foodProd = 10;
                production.industryProd = 5;
                morale = 65;
                if (store.food < 0)
                    store.food = 0;
                return true; // system has rebelled
            }
        }
        return false;
    }

    /**
     * Function calculates the ships which are buildable in this system
     */
    public void calculateBuildableShips() {
        if (!isMajorized() || Major.toMajor(getOwner()) == null)
            return;

        buildableShips.clear();
        if (production.isShipyard()) {
            int minorShipNumber = -1;
            if (getMinorRace() != null)
                minorShipNumber = getMinorRace().getRaceShipNumber();

            Research research = getOwner().getEmpire().getResearch();
            IntArray obsoleteClasses = new IntArray();

            for (int i = 0; i < resourceManager.getShipInfos().size; i++) {
                ShipInfo shipInfo = resourceManager.getShipInfos().get(i);
                if ((shipInfo.getRace() == getOwner().getRaceShipNumber() || shipInfo.getRace() == minorShipNumber)
                        && !shipInfo.isStation()) {
                    int researchLevels[] = research.getResearchLevels();
                    if (!shipInfo.isThisShipBuildableNow(researchLevels))
                        continue;
                    //if this ship obsoletes something else, handle that
                    if (shipInfo.getObsoletesClassIdx() != -1) {
                        obsoleteClasses.add(shipInfo.getObsoletesClassId());
                    }
                    //if this ship can be built only in some systems
                    if (!shipInfo.getOnlyInSystem().isEmpty()) {
                        if (!shipInfo.getOnlyInSystem().equals(getName()))
                            continue;
                        else
                            shipInfo.setRace(getOwner().getRaceShipNumber());
                    } else if (shipInfo.getRace() == minorShipNumber)
                        continue;

                    //do we have a shipyard which can build this ship size?
                    if (shipInfo.getSize().getSize() > production.getMaxBuildableShipSize().getSize())
                        continue;

                    //if the empire's credits are under 0, we can't build anymore warships
                    if (!shipInfo.isNonCombat() && getOwner().getEmpire().getCredits() < 0)
                        continue;

                    buildableShips.add(shipInfo.getID());
                }
            }
            for (int i = 0; i < obsoleteClasses.size; i++)
                for (int j = 0; j < buildableShips.size; j++)
                    if (buildableShips.get(j) == obsoleteClasses.get(i)) {
                        buildableShips.removeIndex(j);
                        break;
                    }
        }
    }

    /**
     * Function calculates the number of farms, factories etc which need workers
     * It must be called at the beginning of each turn before!! calculateVariables() and it also sorts
     * the buildings array by runningnumber.
     * @param buildingInfos
     */
    public void calculateNumberOfWorkBuildings(Array<BuildingInfo> buildingInfos) {
        buildings.sort();
        foodBuildings = 0;
        industryBuildings = 0;
        energyBuildings = 0;
        securityBuildings = 0;
        researchBuildings = 0;
        titanMines = 0;
        deuteriumMines = 0;
        duraniumMines = 0;
        iridiumMines = 0;
        crystalMines = 0;

        for (int i = 0; i < buildings.size; i++) {
            BuildingInfo buildingInfo = buildingInfos.get(buildings.get(i).getRunningNumber() - 1);
            if (buildingInfo.getWorker()) {
                if (buildingInfo.getFoodProd() > 0)
                    foodBuildings++;
                if (buildingInfo.getIndustryPointsProd() > 0)
                    industryBuildings++;
                if (buildingInfo.getEnergyProd() > 0)
                    energyBuildings++;
                if (buildingInfo.getSecurityPointsProd() > 0)
                    securityBuildings++;
                if (buildingInfo.getResearchPointsProd() > 0)
                    researchBuildings++;
                if (buildingInfo.getTitanProd() > 0)
                    titanMines++;
                if (buildingInfo.getDeuteriumProd() > 0)
                    deuteriumMines++;
                if (buildingInfo.getDuraniumProd() > 0)
                    duraniumMines++;
                if (buildingInfo.getCrystalProd() > 0)
                    crystalMines++;
                if (buildingInfo.getIridiumProd() > 0)
                    iridiumMines++;
            }
        }
    }

    /**
     * Calculates the morale production resulting from this system to the empire
     * @param buildingInfos
     */
    public void calculateEmpireWideMoraleProd(Array<BuildingInfo> buildingInfos) {
        for (int i = 0; i < buildings.size; i++)
            if (buildings.get(i).getIsBuildingOnline()) {
                BuildingInfo buildingInfo = buildingInfos.get(buildings.get(i).getRunningNumber() - 1);
                Major owner = Major.toMajor(getOwner());
                if (owner != null && owner.getEmpire() != null)
                    owner.getEmpire().setMoraleEmpireWide(
                        owner.getEmpire().getMoraleEmpireWide() + buildingInfo.getMoraleProdEmpire());
            }
    }

    private boolean checkGeneralConditions(BuildingInfo building) {
        // general conditions
        // ------------------
        // number of owned systems
        // can be built only in system with name
        // only the owner can build
        // minimum pop
        // minimum X buildings with ID in system
        // maximum X per system
        // maximum X per empire
        //
        // or something of below applies:
        // homesystem
        // own colony
        // minor race system
        // conquered system

        if (building.getNeededSystems() > getOwner().getEmpire().countSystems())
            return false;
        if (!building.getOnlyInSystemWithName().equals("0") && !building.getOnlyInSystemWithName().isEmpty())
            if (!building.getOnlyInSystemWithName().equals(getName()))
                return false;
        if (building.isOnlyRace())
            if (getOwner().getRaceBuildingNumber() != building.getOwnerOfBuilding())
                return false;
        if (building.getMinInhabitants() > 0)
            if (inhabitants < building.getMinInhabitants())
                return false;
        if (building.getMinInSystem().number > 0) {
            int number = 0;
            for (int i = 0; i < buildings.size; i++) {
                if (buildings.get(i).getRunningNumber() == building.getMinInSystem().runningNumber)
                    number++;
                if (number >= building.getMinInSystem().number)
                    break;
            }
            if (number < building.getMinInSystem().number)
                return false;
        }
        if (building.getMaxInSystem().number > 0) {
            int number = 0;
            for (int i = 0; i < buildings.size; i++) {
                if (buildings.get(i).getRunningNumber() == building.getMaxInSystem().runningNumber)
                    number++;
                if (number >= building.getMaxInSystem().number)
                    return false;
            }
        }
        if (building.getMaxInEmpire() > 0) {
            int count = resourceManager.getGlobalBuildings().getCountGlobalBuilding(ownerID,
                    building.getRunningNumber());
            if (count >= building.getMaxInEmpire())
                return false;
        }
        if (building.isOnlyHomePlanet()) {
            if (getName().equals(getOwner().getHomeSystemName()))
                return true;
            if (building.isOnlyOwnColony())
                if (colonyOwner.equals(ownerID) && !getName().equals(getOwner().getHomeSystemName()))
                    return true;
            if (building.isOnlyMinorRace())
                if (getMinorRace() != null)
                    return true;
            if (building.isOnlyTakenSystem())
                if (owningStatus == SystemOwningStatus.OWNING_STATUS_TAKEN)
                    return true;
            return false;
        }
        if (building.isOnlyOwnColony()) {
            if (colonyOwner.equals(ownerID) && !getName().equals(getOwner().getHomeSystemName()))
                return true;
            if (building.isOnlyHomePlanet())
                if (getName().equals(getOwner().getHomeSystemName()))
                    return true;
            if (building.isOnlyMinorRace())
                if (getMinorRace() != null)
                    return true;
            if (building.isOnlyTakenSystem())
                if (owningStatus == SystemOwningStatus.OWNING_STATUS_TAKEN)
                    return true;
            return false;
        }
        if (building.isOnlyMinorRace()) {
            if (getMinorRace() != null)
                return true;
            if (building.isOnlyHomePlanet())
                if (getName().equals(getOwner().getHomeSystemName()))
                    return true;
            if (building.isOnlyOwnColony())
                if (colonyOwner.equals(ownerID) && !getName().equals(getOwner().getHomeSystemName()))
                    return true;
            if (building.isOnlyTakenSystem())
                if (owningStatus == SystemOwningStatus.OWNING_STATUS_TAKEN)
                    return true;
            return false;
        }
        if (building.isOnlyTakenSystem()) {
            if (owningStatus == SystemOwningStatus.OWNING_STATUS_TAKEN)
                return true;
            if (building.isOnlyHomePlanet())
                if (getName().equals(getOwner().getHomeSystemName()))
                    return true;
            if (building.isOnlyOwnColony())
                if (colonyOwner.equals(ownerID) && !getName().equals(getOwner().getHomeSystemName()))
                    return true;
            if (building.isOnlyMinorRace())
                if (getMinorRace() != null)
                    return true;
            return false;
        }
        return true;
    }

    /**
     * This function checks if there is an follower to our building in the list of buildable buildings (if flag is false), or as existing building (flag is true)
     * @param buildingInfos
     * @param id
     * @param flag
     * @param equivalence
     * @return
     */
    private boolean checkFollower(Array<BuildingInfo> buildingInfos, int id, boolean flag, boolean equivalence) {
        if (!flag) {
            //check the buildable buildings for a potential predecessor, if one is my id then stop
            for (int i = 0; i < buildableWithoutAssemblylistCheck.size; i++) {
                // because there can be also negative IDs (upgrades) we can take only real buildings into account
                if (buildableWithoutAssemblylistCheck.get(i) > 0) {
                    int pre = buildingInfos.get(buildableWithoutAssemblylistCheck.get(i) - 1).getPredecessorId();
                    if (pre == id)
                        return true;
                    do {
                        if (pre > 0) {
                            int oldPre = pre;
                            pre = buildingInfos.get(pre - 1).getPredecessorId();
                            if (oldPre == pre) {
                                //System.err.println("Error with predecessorID");
                                break;
                            }
                        }
                    } while (pre != id && pre != 0);
                    if (pre == id)
                        return true;
                    //if we have buildings from other races in the system, then we have to check if there is an equivalent building
                    if (equivalence) {
                        int race = buildingInfos.get(id - 1).getOwnerOfBuilding();
                        int equi = buildingInfos.get(buildableWithoutAssemblylistCheck.get(i) - 1)
                                .getBuildingEquivalent(race);
                        if (equi == id)
                            return true;
                        pre = buildingInfos.get(buildableWithoutAssemblylistCheck.get(i) - 1).getPredecessorId();
                        do {
                            if (pre > 0) {
                                int oldPre = pre;
                                equi = buildingInfos.get(pre - 1).getBuildingEquivalent(race);
                                pre = buildingInfos.get(pre - 1).getPredecessorId();
                                if (oldPre == pre) {
                                    //System.err.println("Error with predecessorID");
                                    break;
                                }
                            }
                        } while (id != equi && pre != 0);
                        if (equi == id)
                            return true;
                    }
                }
            }
        } else {//check if a potential follow-up is already in the system. Normally this is already in the list of buildable buildings,
                //but fore example buildings which are buildable only once are not in the list. For example if we have an upgraded shipyard of a different race
                //in the system, then I can't build a potentially equivalent predecessor of this shipyard.
            for (int i = 0; i < buildings.size; i++) {
                int pre = buildingInfos.get(buildings.get(i).getRunningNumber() - 1).getPredecessorId();
                if (pre == id)
                    return true;
                do {
                    if (pre > 0) {
                        int oldPre = pre;
                        pre = buildingInfos.get(pre - 1).getPredecessorId();
                        if (oldPre == pre) {
                            //System.err.println("Error with buildings predecessor");
                            break;
                        }
                    }

                } while (pre != id && pre != 0);
                if (pre == id)
                    return true;

                if (equivalence) {
                    int race = buildingInfos.get(id - 1).getOwnerOfBuilding();
                    int equi = buildingInfos.get(buildings.get(i).getRunningNumber() - 1).getBuildingEquivalent(race);

                    if (equi == id)
                        return true;
                    pre = buildingInfos.get(buildings.get(i).getRunningNumber() - 1).getPredecessorId();
                    do {
                        if (pre > 0) {
                            int oldPre = pre;
                            equi = buildingInfos.get(pre - 1).getBuildingEquivalent(race);
                            pre = buildingInfos.get(pre - 1).getPredecessorId();
                            if (oldPre == pre) {
                                //System.err.println("Error with buildings predecessor");
                                break;
                            }
                        }
                    } while (id != equi && pre != 0);

                    if (id == equi)
                        return true;
                }
            }

        }
        return false;
    }

    private boolean checkTech(BuildingInfo building, Research research) {
        if (research.getBioTech() < building.getBioTech())
            return false;
        if (research.getEnergyTech() < building.getEnergyTech())
            return false;
        if (research.getComputerTech() < building.getComputerTech())
            return false;
        if (research.getPropulsionTech() < building.getPropulsionTech())
            return false;
        if (research.getConstructionTech() < building.getConstructionTech())
            return false;
        if (research.getWeaponTech() < building.getWeaponTech())
            return false;
        return true;
    }

    private boolean checkPlanet(BuildingInfo building) {
        boolean ok = false;
        boolean deritium = false;
        for (int i = 0; i < getNumberOfPlanets() && (ok == false); i++) {
            Planet p = getPlanet(i);
            if (p.getIsInhabited()) {
                if (building.getPlanetTypes(PlanetType.PLANETCLASS_A.getType()) && p.getPlanetClass().equals("A")
                        && (ok == false))
                    ok = true;
                if (building.getPlanetTypes(PlanetType.PLANETCLASS_B.getType()) && p.getPlanetClass().equals("B")
                        && (ok == false))
                    ok = true;
                if (building.getPlanetTypes(PlanetType.PLANETCLASS_C.getType()) && p.getPlanetClass().equals("C")
                        && (ok == false))
                    ok = true;
                if (building.getPlanetTypes(PlanetType.PLANETCLASS_E.getType()) && p.getPlanetClass().equals("E")
                        && (ok == false))
                    ok = true;
                if (building.getPlanetTypes(PlanetType.PLANETCLASS_F.getType()) && p.getPlanetClass().equals("F")
                        && (ok == false))
                    ok = true;
                if (building.getPlanetTypes(PlanetType.PLANETCLASS_G.getType()) && p.getPlanetClass().equals("G")
                        && (ok == false))
                    ok = true;
                if (building.getPlanetTypes(PlanetType.PLANETCLASS_H.getType()) && p.getPlanetClass().equals("H")
                        && (ok == false))
                    ok = true;
                if (building.getPlanetTypes(PlanetType.PLANETCLASS_I.getType()) && p.getPlanetClass().equals("I")
                        && (ok == false))
                    ok = true;
                if (building.getPlanetTypes(PlanetType.PLANETCLASS_J.getType()) && p.getPlanetClass().equals("J")
                        && (ok == false))
                    ok = true;
                if (building.getPlanetTypes(PlanetType.PLANETCLASS_K.getType()) && p.getPlanetClass().equals("K")
                        && (ok == false))
                    ok = true;
                if (building.getPlanetTypes(PlanetType.PLANETCLASS_L.getType()) && p.getPlanetClass().equals("L")
                        && (ok == false))
                    ok = true;
                if (building.getPlanetTypes(PlanetType.PLANETCLASS_M.getType()) && p.getPlanetClass().equals("M")
                        && (ok == false))
                    ok = true;
                if (building.getPlanetTypes(PlanetType.PLANETCLASS_N.getType()) && p.getPlanetClass().equals("N")
                        && (ok == false))
                    ok = true;
                if (building.getPlanetTypes(PlanetType.PLANETCLASS_O.getType()) && p.getPlanetClass().equals("O")
                        && (ok == false))
                    ok = true;
                if (building.getPlanetTypes(PlanetType.PLANETCLASS_P.getType()) && p.getPlanetClass().equals("P")
                        && (ok == false))
                    ok = true;
                if (building.getPlanetTypes(PlanetType.PLANETCLASS_Q.getType()) && p.getPlanetClass().equals("Q")
                        && (ok == false))
                    ok = true;
                if (building.getPlanetTypes(PlanetType.PLANETCLASS_R.getType()) && p.getPlanetClass().equals("R")
                        && (ok == false))
                    ok = true;
                if (building.getPlanetTypes(PlanetType.PLANETCLASS_S.getType()) && p.getPlanetClass().equals("S")
                        && (ok == false))
                    ok = true;
                if (building.getPlanetTypes(PlanetType.PLANETCLASS_T.getType()) && p.getPlanetClass().equals("T")
                        && (ok == false))
                    ok = true;
                if (building.getPlanetTypes(PlanetType.PLANETCLASS_Y.getType()) && p.getPlanetClass().equals("Y")
                        && (ok == false))
                    ok = true;
            }
        }
        if (building.getDeritiumProd() > 0) {
            for (int i = 0; i < getNumberOfPlanets(); i++)
                if (getPlanet(i).getIsInhabited())
                    deritium |= getPlanet(i).getBonuses()[ResourceTypes.DERITIUM.getType()];
            if (!deritium || !ok)
                return false;
        }
        return ok;
    }

    /**
     * Function calculates the buildable buildings and upgrades in the system
     */
    public void calculateBuildableBuildings() {
        if (!isMajorized() || Major.toMajor(getOwner()) == null)
            return;

        Array<BuildingInfo> buildingInfos = resourceManager.getBuildingInfos();
        Empire empire = getOwner().getEmpire();

        buildableWithoutAssemblylistCheck.clear();
        buildableBuildings.clear();
        int minID = 0;
        boolean equivalents = false; //is in the system a building from a different race (on which we couldn've have built)

        for (int i = 0; i < buildings.size; i++) {
            int id = buildings.get(i).getRunningNumber();
            boolean found = false;

            if (id > minID) {
                //is there a building of a different race
                if (buildingInfos.get(id - 1).getOwnerOfBuilding() != getOwner().getRaceBuildingNumber())
                    equivalents = true;
                minID = id;
                //check if the building is already on the buildable buildings list
                for (int k = 0; k < buildableWithoutAssemblylistCheck.size; k++)
                    if (buildableWithoutAssemblylistCheck.get(k) == id) {
                        found = true;
                        break;
                    }

                //check if our planets allow those buildings
                if (!found && checkPlanet(buildingInfos.get(id - 1)))
                    if (checkGeneralConditions(buildingInfos.get(id - 1))) {
                        //if we are here, we can build the building, check if it's in the alwaysBuildable list (buildings which require workers)
                        boolean id_in_list = false;
                        BuildingInfo info = buildingInfos.get(buildings.get(i).getRunningNumber() - 1);

                        if (info.getWorker()) {
                            for (int j = 0; j < alwaysBuildableBuildings.size; j++)
                                if (id == alwaysBuildableBuildings.get(j)) {
                                    id_in_list = true;
                                    break;
                                }
                            if (!id_in_list && (buildingInfos.get(id - 1).getPredecessorId() != 0)) {
                                boolean found_predecessor = false;

                                for (int j = 0; j < alwaysBuildableBuildings.size; j++)
                                    if (alwaysBuildableBuildings.get(j) == buildingInfos.get(id - 1).getPredecessorId()) {
                                        alwaysBuildableBuildings.set(j, id);
                                        found_predecessor = true;
                                        break;
                                    }

                                if (!found_predecessor)
                                    alwaysBuildableBuildings.add(id);
                            }
                        }
                        buildableWithoutAssemblylistCheck.add(id);
                    }
            }
        }

        //add the always buildable buildings to the buildable list
        int size = buildableWithoutAssemblylistCheck.size;
        for (int i = 0; i < alwaysBuildableBuildings.size; i++) {
            if (buildingInfos.get(alwaysBuildableBuildings.get(i) - 1).getOwnerOfBuilding() != getOwner()
                    .getRaceBuildingNumber())
                equivalents = true;
            boolean found = false;
            for (int j = 0; j < size; j++)
                if (alwaysBuildableBuildings.get(i) == buildableWithoutAssemblylistCheck.get(j)) {
                    found = true;
                    break;
                }
            if (!found)
                buildableWithoutAssemblylistCheck.add(alwaysBuildableBuildings.get(i));
        }

        //iterate all other building infos
        for (int i = 0; i < buildingInfos.size; i++) {
            boolean found = false;
            for (int j = 0; j < buildableWithoutAssemblylistCheck.size; j++) {
                if (buildableWithoutAssemblylistCheck.get(j) == buildingInfos.get(i).getRunningNumber()) {
                    found = true;
                    break;
                }
            }

            //if the building a never-ready building is and produces morale, then it can be only built if
            //the morale is 2.5 times under the production of the building
            if (buildingInfos.get(i).getNeverReady() && buildingInfos.get(i).getMoraleProd() > 0)
                if (morale > 100 - buildingInfos.get(i).getMoraleProd() * 2.5)
                    continue;

            //if we still haven't found the building, we can check all of the requirements
            if (!found && checkTech(buildingInfos.get(i), empire.getResearch()))
                if (checkPlanet(buildingInfos.get(i)))
                    if (checkGeneralConditions(buildingInfos.get(i))) {
                        //check if it has a predecessor
                        if (buildingInfos.get(i).getPredecessorId() != 0) {
                            boolean found_predecessor = false;
                            for (int k = 0; k < buildings.size; k++)
                                if (buildings.get(k).getRunningNumber() == buildingInfos.get(i).getPredecessorId()) {
                                    found_predecessor = true;
                                    break;
                                }
                            if (found_predecessor)
                                buildableWithoutAssemblylistCheck.add(buildingInfos.get(i).getRunningNumber() * (-1));
                        } else if (buildingInfos.get(i).getOwnerOfBuilding() == 0
                                || buildingInfos.get(i).getOwnerOfBuilding() == getOwner().getRaceBuildingNumber()) {
                            if (equivalents) { // check if we have an equivalent from a different race in the list
                                for (int j = 0; j < buildableWithoutAssemblylistCheck.size; j++) {
                                    int tid = buildableWithoutAssemblylistCheck.get(j);
                                    if (tid > 0) //not an upgrade
                                        if (buildingInfos.get(i).getBuildingEquivalent(
                                                buildingInfos.get(tid - 1).getOwnerOfBuilding()) == tid) {
                                            found = true;
                                            break;
                                        }
                                }
                                if (!found) {
                                    minID = 0;
                                    for (int j = 0; j < buildings.size; j++) {
                                        int tid = buildings.get(j).getRunningNumber();
                                        if (tid > minID) //because buildings is sorted
                                            if (buildingInfos.get(i).getBuildingEquivalent(
                                                    buildingInfos.get(tid - 1).getOwnerOfBuilding()) == tid) {
                                                found = true;
                                                break;
                                            }
                                    }
                                }
                            }

                            if (found)	//if there is a equivalent building, we can't build those
                                continue;
                            if (buildingInfos.get(i).isUpgradeable()) {
                                if (!checkFollower(buildingInfos, buildingInfos.get(i).getRunningNumber(), false,
                                        equivalents))
                                    if (!checkFollower(buildingInfos, buildingInfos.get(i).getRunningNumber(), true,
                                            equivalents))
                                        buildableWithoutAssemblylistCheck.add(buildingInfos.get(i).getRunningNumber());
                            } else
                                buildableWithoutAssemblylistCheck.add(buildingInfos.get(i).getRunningNumber());
                        }
                    }
        }
        assemblyListCheck(buildingInfos);
        buildableBuildings.sort();
    }

    public void addNewBuilding(Building building) {
        buildings.add(building);
    }

    /**
     * Removes the buildings which have the supplied runningNumber and it returns their count and if they were online
     * Addbuilding has to be called afterwards with the upgraded version and same count
     * @param runningNumber
     * @param neededEnergy
     * @return
     */
    public Pair<Integer, Boolean> updateBuildings(int runningNumber, int neededEnergy) {
        boolean wasOnline = false;
        int count = 0;
        for (int i = 0; i < buildings.size; i++)
            if (buildings.get(i).getRunningNumber() == runningNumber) {
                //if it was online, free energy
                if (neededEnergy > 0 && buildings.get(i).getIsBuildingOnline()) {
                    wasOnline = true;
                    production.energyProd += neededEnergy;
                }
                buildings.removeIndex(i--);
                count++;
            }
        return new Pair<Integer, Boolean>(count, wasOnline);
    }

    public void colonize(Ships ship, Major major) {
        String shipOwner = ship.getOwnerId();
        Empire empire = major.getEmpire();
        if (!isMajorized()) {
            changeOwner(shipOwner, SystemOwningStatus.OWNING_STATUS_COLONIZED_MEMBERSHIP_OR_HOME);
            colonyOwner = shipOwner;
            buildBuildingsAfterColonization(resourceManager.getBuildingInfos(), ship.getColonizePoints());
            String s = StringDB.getString("FOUND_COLONY_MESSAGE", false, getName());
            EmpireNews message = new EmpireNews();
            message.CreateNews(s, EmpireNewsType.SOMETHING, getName(), coordinates);
            empire.addMsg(message);

            //extra message for morale
            message = new EmpireNews();
            message.CreateNews(major.getMoraleObserver().addEvent(12, major.getRaceMoralNumber(), getName()),
                    EmpireNewsType.SOMETHING, "", coordinates);
            empire.addMsg(message);
            if (major.isHumanPlayer()) {
                resourceManager.getClientWorker().setEmpireViewFor(major);
                EventColonization eventScreen = new EventColonization(resourceManager, StringDB.getString(
                        "COLOEVENT_HEADLINE", false, getName()), StringDB.getString(
                        "COLOEVENT_TEXT_" + major.getRaceId(), false, getName()));
                empire.pushEvent(eventScreen);
                if (major.getRaceId().equals(resourceManager.getRaceController().getPlayerRaceString())) {
                    resourceManager.getAchievementManager().unlockAchievement(AchievementsList.achievementTerraNova.getAchievement());
                    resourceManager.getAchievementManager().incrementAchievement(AchievementsList.achievementColonizer.getAchievement(), 1);
                }
            }
        } else {
            String s = StringDB.getString("NEW_PLANET_COLONIZED", false, getName());
            EmpireNews message = new EmpireNews();
            message.CreateNews(s, EmpireNewsType.SOMETHING, getName(), coordinates);
            empire.addMsg(message);
            resourceManager.getClientWorker().setEmpireViewFor(major);
            if (major.getRaceId().equals(resourceManager.getRaceController().getPlayerRaceString()))
                resourceManager.getAchievementManager().unlockAchievement(AchievementsList.achievementTerraNova.getAchievement());
        }
        setInhabitants(getCurrentInhabitants());
        calculateNumberOfWorkBuildings(resourceManager.getBuildingInfos());
        setWorkersIntoBuildings();
        calculateVariables();
    }

    /**
     * Function calculates and builds the starting buildings of a system after it was colonized
     * @param buildingInfo
     * @param colonizationPoints
     */
    private void buildBuildingsAfterColonization(Array<BuildingInfo> buildingInfo, int colonizationPoints) {
        removeSpecialRaceBuildings(buildingInfo);
        int raceBuildingId = getOwner().getRaceBuildingNumber();
        Research research = getOwner().getEmpire().getResearch();
        int researchLevels[] = research.getResearchLevels();
        boolean exist[] = getAvailableResources(true);
        int start = 0;
        for (int i = 0; i < buildingInfo.size; i++)
            if (buildingInfo.get(i).getOwnerOfBuilding() == raceBuildingId) {
                start = i;
                break;
            }
        int counter[] = new int[10];
        Arrays.fill(counter, 0);
        int runningNumber[] = new int[10];
        Arrays.fill(runningNumber, 0);
        for (int i = start; i < buildingInfo.size; i++) {
            //stop condition, for the case that the building doesn't belongs to the buildings of the sector owner
            //for this the building list should be sorted (which it is)
            if (buildingInfo.get(i).getOwnerOfBuilding() != raceBuildingId)
                break;
            //if it can't be built, then continue to the next one
            if (!buildingInfo.get(i).isBuildingBuildableNow(researchLevels))
                continue;

            if (buildingInfo.get(i).getFoodProd() > 0 && buildingInfo.get(i).getWorker()
                    && counter[0] < colonizationPoints) {
                counter[0]++;
                runningNumber[0] = buildingInfo.get(i).getRunningNumber();
            }
            if (buildingInfo.get(i).getIndustryPointsProd() > 0 && buildingInfo.get(i).getWorker()
                    && counter[1] < colonizationPoints) {
                counter[1]++;
                runningNumber[1] = buildingInfo.get(i).getRunningNumber();
            }
            if (buildingInfo.get(i).getEnergyProd() > 0 && buildingInfo.get(i).getWorker()
                    && counter[2] < colonizationPoints) {
                counter[2]++;
                runningNumber[2] = buildingInfo.get(i).getRunningNumber();
            }
            if (buildingInfo.get(i).getSecurityPointsProd() > 0 && buildingInfo.get(i).getWorker()
                    && counter[3] < colonizationPoints) {
                counter[3]++;
                runningNumber[3] = buildingInfo.get(i).getRunningNumber();
            }
            if (buildingInfo.get(i).getResearchPointsProd() > 0 && buildingInfo.get(i).getWorker()
                    && counter[4] < colonizationPoints) {
                counter[4]++;
                runningNumber[4] = buildingInfo.get(i).getRunningNumber();
            }
            for (int j = 0; j <= ResourceTypes.IRIDIUM.getType(); j++)
                if (buildingInfo.get(i).getResourceProd(j) > 0 && buildingInfo.get(i).getWorker()
                        && counter[5 + j] < colonizationPoints && exist[j]) {
                    counter[5 + j]++;
                    runningNumber[5 + j] = buildingInfo.get(i).getRunningNumber();
                }
        }
        //if there are some buildings of a type, we can't build a different version, for example if we have type 4 farms, we can't buid primitive ones
        calculateNumberOfWorkBuildings(buildingInfo);
        for (int build = WorkerType.FOOD_WORKER.getType(); build <= WorkerType.IRIDIUM_WORKER.getType(); build++) {
            WorkerType worker = WorkerType.fromWorkerType(build);
            if (getNumberOfWorkBuildings(worker, 0) > 0)
                runningNumber[build] = 0;
            if (runningNumber[build] != 0)
                for (int i = 0; i < colonizationPoints * 2; i++) {
                    Building building = new Building(runningNumber[build]);
                    building.setIsBuildingOnline(buildingInfo.get(runningNumber[build] - 1).getAlwaysOnline());
                    buildings.add(building);
                }
        }
        //if the system was destoyed by a bombardment we should erase the always buildable buildings
        alwaysBuildableBuildings.clear();
    }

    /**
     * Function builds the buildings for a minor race in case we reach membership with them
     * @param buildingInfo
     * @param averageTechLevel
     * @param minor
     */
    public void buildBuildingsForMinorRace(Array<BuildingInfo> buildingInfo, int averageTechLevel, Minor minor) {
        //all buildings which we shouldn't have after getting the system should be removed from the list of the current buildings
        removeSpecialRaceBuildings(buildingInfo);
        if (buildings.size < 5) {
            boolean exist[] = getAvailableResources(true);

            int level = 0;
            switch (minor.getTechnologicalProgress()) {
                case 0: {//very primitive
                    int temp = (int) (RandUtil.random() * 3) + 1;
                    level = averageTechLevel - temp;
                    break;
                }
                case 1: {// primitive
                    int temp = (int) (RandUtil.random() * 2) + 1;
                    level = averageTechLevel - temp;
                    break;
                }
                case 2: {// normal
                    int temp = (int) (RandUtil.random() * 3);
                    level = averageTechLevel + 1 - temp;
                    break;
                }
                case 3: {// advanced
                    int temp = (int) (RandUtil.random() * 2) + 1;
                    level = averageTechLevel + temp;
                    break;
                }
                case 4: {// very advanced
                    int temp = (int) (RandUtil.random() * 3) + 1;
                    level = averageTechLevel + temp;
                    break;
                }
            }
            level = Math.max(0, level);
            int researchLevels[] = new int[6];
            Arrays.fill(researchLevels, level);

            Array<RaceProperty> properties = new Array<RaceProperty>();
            if (minor.isRaceProperty(RaceProperty.FINANCIAL))
                properties.add(RaceProperty.FINANCIAL);
            if (minor.isRaceProperty(RaceProperty.WARLIKE))
                properties.add(RaceProperty.WARLIKE);
            if (minor.isRaceProperty(RaceProperty.AGRARIAN))
                properties.add(RaceProperty.AGRARIAN);
            if (minor.isRaceProperty(RaceProperty.INDUSTRIAL))
                properties.add(RaceProperty.INDUSTRIAL);
            if (minor.isRaceProperty(RaceProperty.SECRET))
                properties.add(RaceProperty.SECRET);
            if (minor.isRaceProperty(RaceProperty.SCIENTIFIC))
                properties.add(RaceProperty.SCIENTIFIC);
            if (minor.isRaceProperty(RaceProperty.PRODUCER))
                properties.add(RaceProperty.PRODUCER);
            if (minor.isRaceProperty(RaceProperty.PACIFIST))
                properties.add(RaceProperty.PACIFIST);
            if (minor.isRaceProperty(RaceProperty.SNEAKY))
                properties.add(RaceProperty.SNEAKY);
            if (minor.isRaceProperty(RaceProperty.SOLOING))
                properties.add(RaceProperty.SOLOING);
            if (minor.isRaceProperty(RaceProperty.HOSTILE))
                properties.add(RaceProperty.HOSTILE);
            if (properties.size == 0)
                properties.add(RaceProperty.NOTHING_SPECIAL);

            RaceProperty property = properties.get((int) (RandUtil.random() * properties.size));
            int fromRace = (int) (RandUtil.random() * 6) + 1;
            int runningNumber[] = new int[10];
            Arrays.fill(runningNumber, 0);
            int shipYard = 0;
            for (int i = 0; i < buildingInfo.size; i++) {
                BuildingInfo info = buildingInfo.get(i);
                if (fromRace == info.getOwnerOfBuilding()) {
                    if (!info.isBuildingBuildableNow(researchLevels))
                        continue;
                    if (shipYard == 0 && buildings.size == 0 && minor.getSpaceFlightNation())
                        if (info.isShipYard() && info.getPredecessorId() == 0 && info.getMaxInEmpire() == 0)
                            shipYard = info.getRunningNumber();
                    if (info.getFoodProd() > 0 && info.getWorker())
                        runningNumber[0] = info.getRunningNumber();
                    if (info.getIndustryPointsProd() > 0 && info.getWorker())
                        runningNumber[1] = info.getRunningNumber();
                    if (info.getEnergyProd() > 0 && info.getWorker())
                        runningNumber[2] = info.getRunningNumber();
                    if (info.getSecurityPointsProd() > 0 && info.getWorker())
                        runningNumber[3] = info.getRunningNumber();
                    if (info.getResearchPointsProd() > 0 && info.getWorker())
                        runningNumber[4] = info.getRunningNumber();
                    for (int j = 0; j <= ResourceTypes.IRIDIUM.getType(); j++)
                        if (info.getResourceProd(j) > 0 && info.getWorker() && exist[j])
                            runningNumber[5 + j] = info.getRunningNumber();
                }
            }

            int numberOfBuildings = 0;
            numberOfBuildings += (int) inhabitants + 1 + (int) (RandUtil.random() * 5);
            int foodBuildings = 0;
            while (numberOfBuildings > 0) {
                int build = (int) (RandUtil.random() * 10);
                int temp;
                switch (property) {
                    case AGRARIAN:
                        if (build >= 5) {
                            temp = (int) (RandUtil.random() * 3);
                            if (temp == 0)
                                build = 0;
                        } else if (build == 4 || build == 2) {
                            temp = (int) (RandUtil.random() * 2);
                            if (temp == 0)
                                build = 0;
                        } else if (build == 3)
                            build = 0;
                        break;
                    case INDUSTRIAL:
                        if (build >= 5) {
                            temp = (int) (RandUtil.random() * 3);
                            if (temp == 0)
                                build = 1;
                        } else if (build == 4 || build == 2) {
                            temp = (int) (RandUtil.random() * 2);
                            if (temp == 0)
                                build = 1;
                        } else if (build == 3) {
                            temp = (int) (RandUtil.random() * 2);
                            if (temp == 0)
                                build = 0;
                            else
                                build = 1;
                        }
                        break;
                    case SECRET:
                        if (build >= 5) {
                            temp = (int) (RandUtil.random() * 3);
                            if (temp == 0)
                                build = 3;
                        } else if (build == 4) {
                            temp = (int) (RandUtil.random() * 4);
                            if (temp == 0)
                                build = 3;
                        } else if (build == 2) {
                            temp = (int) (RandUtil.random() * 4);
                            if (temp == 0)
                                build = 0;
                            else if (temp == 1)
                                build = 3;
                        }
                        break;
                    case SCIENTIFIC:
                        if (build >= 5) {
                            temp = (int) (RandUtil.random() * 3);
                            if (temp == 0)
                                build = 4;
                        } else if (build == 1) {
                            temp = (int) (RandUtil.random() * 6);
                            if (temp == 0)
                                build = 4;
                        } else if (build == 2) {
                            temp = (int) (RandUtil.random() * 4);
                            if (temp == 0)
                                build = 4;
                        } else if (build == 3) {
                            temp = (int) (RandUtil.random() * 2);
                            if (temp == 0)
                                build = 0;
                            else
                                build = 4;
                        }
                        break;
                    case PRODUCER:
                        if (build < 5 && build > 1) {
                            temp = (int) (RandUtil.random() * 5);
                            if (temp == 0)
                                build = 5;
                            else if (temp == 1)
                                build = 6;
                            else if (temp == 2)
                                build = 7;
                            else if (temp == 3)
                                build = 8;
                            else if (temp == 4)
                                build = 9;
                        }
                        break;
                    case SNEAKY:
                    case SOLOING:
                    case NOTHING_SPECIAL:
                        break;
                    default:
                        if (build == 2 || build == 3) {
                            if ((int) (RandUtil.random() * 2) == 0)
                                build = 1;
                        }
                        break;
                }

                if (runningNumber[build] != 0) {
                    if (build == 0)
                        foodBuildings++;
                    Building building = new Building(runningNumber[build]);
                    building.setIsBuildingOnline(buildingInfo.get(runningNumber[build] - 1).getAlwaysOnline());
                    buildings.add(building);
                    numberOfBuildings--;
                }
            }
            //if there are too few food buildings, here we add some
            while (runningNumber[0] != 0 && foodBuildings < inhabitants / 4) {
                Building building = new Building(runningNumber[0]);
                building.setIsBuildingOnline(buildingInfo.get(runningNumber[0] - 1).getAlwaysOnline());
                buildings.add(building);
                foodBuildings++;
            }

            //fill stores, and if needed alter later
            calculateNumberOfWorkBuildings(buildingInfo);
            setFoodStore(getFoodStore()
                    + (int) (RandUtil.random() * (getNumberOfWorkBuildings(WorkerType.FOOD_WORKER, 0)
                            * (minor.getTechnologicalProgress() + 1) * 100 + 1)));
            for (int res = ResourceTypes.TITAN.getType(); res <= ResourceTypes.IRIDIUM.getType(); res++) {
                WorkerType worker = WorkerType.fromWorkerType(res + 5);
                int resAdd = (int) (RandUtil.random() * getNumberOfWorkBuildings(worker, 0)
                        * (minor.getTechnologicalProgress() + 1) * 100 + 1);
                addResourceStore(res, resAdd);
            }

            if (shipYard != 0) {
                Building building = new Building(shipYard);
                building.setIsBuildingOnline(buildingInfo.get(shipYard - 1).getAlwaysOnline());
                buildings.add(building);
                setNewBuildingOnline(buildingInfo);
            }
        }
    }

    /**
     * Function destroys all buildings from the buildingDestroy variable
     * Function should be called in the nextRound() call
     * @return
     */
    public boolean destroyBuildings() {
        boolean destroy = buildingDestroy.size != 0;
        for (int i = 0; i < buildingDestroy.size; i++)
            for (int j = buildings.size - 1; j >= 0; j--)
                //loop backwards because workers are allocated form the front and we don't want to destroy an online building, but a probably unused one
                if (buildings.get(j).getRunningNumber() == buildingDestroy.get(i)) {
                    buildings.removeIndex(j);
                    buildingDestroy.removeIndex(i--);
                    break;
                }
        if (destroy)
            buildingDestroy.clear();
        return destroy;
    }

    /**
     * function sets the last built building online if it's possible
     * @param buildingInfo
     * @return 0 - the building could be switched online, 1 - too few workers, 2 - energy needed
     */
    public int setNewBuildingOnline(Array<BuildingInfo> buildingInfo) {
        int lastBuilding = buildings.size - 1;
        if (buildings.get(lastBuilding).getIsBuildingOnline())
            return 0;
        BuildingInfo info = buildingInfo.get(buildings.get(lastBuilding).getRunningNumber() - 1);

        if (info.getWorker() && workers.getWorker(WorkerType.FREE_WORKER) == 0)
            return 1;

        if (info.getNeededEnergy() > production.getEnergyProd())
            return 2;

        if (info.getWorker()) {
            if (info.getFoodProd() > 0)
                incrementWorker(WorkerType.FOOD_WORKER);
            else if (info.getIndustryPointsProd() > 0)
                incrementWorker(WorkerType.INDUSTRY_WORKER);
            else if (info.getEnergyProd() > 0)
                incrementWorker(WorkerType.ENERGY_WORKER);
            else if (info.getSecurityPointsProd() > 0)
                incrementWorker(WorkerType.SECURITY_WORKER);
            else if (info.getResearchPointsProd() > 0)
                incrementWorker(WorkerType.RESEARCH_WORKER);
            else if (info.getTitanProd() > 0)
                incrementWorker(WorkerType.TITAN_WORKER);
            else if (info.getDeuteriumProd() > 0)
                incrementWorker(WorkerType.DEUTERIUM_WORKER);
            else if (info.getDuraniumBonus() > 0)
                incrementWorker(WorkerType.DURANIUM_WORKER);
            else if (info.getCrystalProd() > 0)
                incrementWorker(WorkerType.CRYSTAL_WORKER);
            else if (info.getIridiumProd() > 0)
                incrementWorker(WorkerType.IRIDIUM_WORKER);
        }

        production.energyProd -= info.getNeededEnergy();
        buildings.get(lastBuilding).setIsBuildingOnline(true);
        return 0;
    }

    /**
     * Function checks the buildings which need energy and stops them if there is too little energy in the system.
     * This function has to be called before calling calculateVariables() etc. because we want to stop the online buildings before that
     * @return false if there is enough energy, true if at least one building was stopped
     */
    public boolean checkEnergyBuildings() {
        Array<BuildingInfo> buildingInfos = resourceManager.getBuildingInfos();
        boolean ret = false;
        for (int i = 0; i < buildings.size; i++) {

            //if there is enough energy, nothing has to be checked
            if (production.getEnergyProd() >= 0)
                return ret;

            BuildingInfo info = buildingInfos.get(buildings.get(i).getRunningNumber() - 1);
            if (production.getEnergyProd() < 0 && info.getNeededEnergy() > 0 && buildings.get(i).getIsBuildingOnline()) {
                buildings.get(i).setIsBuildingOnline(false);
                production.energyProd += info.getNeededEnergy();
                ret = true;
            }
        }
        return ret;
    }

    /**
     * checks the assemblylist for consistency
     * @param buildingInfo
     */
    public void assemblyListCheck(Array<BuildingInfo> buildingInfo) {
        //first fill the list of buildable buildings und upgrades with the list of buildings buildable in this turn
        buildableBuildings.clear();
        buildableUpdates.clear();
        for (int i = 0; i < buildableWithoutAssemblylistCheck.size; i++) {
            //upgrades have a negative id so the sign has to be flipped
            int entry = buildableWithoutAssemblylistCheck.get(i);
            entry = Math.abs(entry);
            //if this building can exist only X times in the empire, then it's not supposed to exist more then X times in the global building list
            boolean found = false;
            if (buildingInfo.get(entry - 1).getMaxInEmpire() > 0) {
                int ID = buildingInfo.get(entry - 1).getRunningNumber();
                int count = resourceManager.getGlobalBuildings().getCountGlobalBuilding(getOwnerId(), ID);
                if (count >= buildingInfo.get(entry - 1).getMaxInEmpire())
                    found = true;
            }
            //we haven't found the building in the global list
            if (!found) {
                if (buildableWithoutAssemblylistCheck.get(i) > 0)
                    buildableBuildings.add(entry);
                else
                    buildableUpdates.add(entry);
            }
        }
        //after upgrades check the assemblylist, and if we find an update in it, then we have to remove it and all predecessors from the list
        for (int i = 0; i < GameConstants.ALE; i++) {
            int id = assemblyList.getAssemblyListEntry(i).id;

            if (id < 0) {
                //remove predecessors of the upgrade from the list
                for (int t = 0; t < buildableBuildings.size; t++) {
                    int pre = buildingInfo.get(Math.abs(id) - 1).getPredecessorId();
                    if (pre == buildableBuildings.get(t))
                        buildableBuildings.removeIndex(t--);
                    for (int s = 0; s < buildableUpdates.size; s++) {
                        int pre2 = buildingInfo.get(buildableUpdates.get(s) - 1).getPredecessorId();
                        if (pre2 == pre)
                            buildableUpdates.removeIndex(s--);
                    }
                }
            } else if (id < 10000 && id != 0) {
                //check max/empire
                if (buildingInfo.get(id - 1).getMaxInEmpire() > 0) {
                    int count = resourceManager.getGlobalBuildings().getCountGlobalBuilding(getOwnerId(), id);
                    if (count >= buildingInfo.get(id - 1).getMaxInEmpire())
                        for (int t = 0; t < buildableBuildings.size; t++)
                            if (id == buildableBuildings.get(t)) {
                                buildableBuildings.removeIndex(t--);
                                break;
                            }
                }
                //check max in system
                if (buildingInfo.get(id - 1).getMaxInSystem().number > 0) {
                    int n = 0;
                    for (int j = 0; j < buildings.size; j++) {
                        if (buildings.get(j).getRunningNumber() == buildingInfo.get(id - 1).getMaxInSystem().runningNumber)
                            n++;
                        if (n >= buildingInfo.get(id - 1).getMaxInSystem().number)
                            break;
                    }
                    //check build list, and if it appears, remove from there too
                    for (int j = 0; j < GameConstants.ALE; j++) {
                        int id2 = assemblyList.getAssemblyListEntry(j).id;
                        int count2 = assemblyList.getAssemblyListEntry(j).count;
                        if (id == id2)
                            if (id2 == buildingInfo.get(id2 - 1).getMaxInSystem().runningNumber)
                                n += count2;
                    }
                    if (n >= buildingInfo.get(id - 1).getMaxInSystem().number)
                        for (int t = 0; t < buildableBuildings.size; t++)
                            if (id == buildableBuildings.get(t)) {
                                buildableBuildings.removeIndex(t--);
                                break;
                            }
                }
            }
        }
    }

    /**
     * Function removes all special buildings from the list. It must be called after conquering a system. After this there will
     * be no more buildings which can be built only X times pro empire or which can be built only by the original race
     * @param buildingInfos
     */
    public void removeSpecialRaceBuildings(Array<BuildingInfo> buildingInfos) {
        for (int i = 0; i < buildings.size; i++) {
            BuildingInfo info = buildingInfos.get(buildings.get(i).getRunningNumber() - 1);
            if (info.getMaxInEmpire() > 0 || info.isOnlyRace() || info.isShipYard() || info.isBarrack())
                buildings.removeIndex(i--);
        }
    }

    private void calculateNeededResources(int index, BuildingInfo buildingInfo, ShipInfo shipInfo, TroopInfo troopInfo) {
        //FIXME: debug code to help catch the crash
        Race owner = getOwner();
        Empire empire = owner.getEmpire();
        Research research = empire.getResearch();
        ResearchInfo ri = research.getResearchInfo();
        // debug code end
        assemblyList.calculateNeededResources(buildingInfo, shipInfo, troopInfo, buildings, index, ri);
    }

    public int neededRoundsToBuild(int indexOrId, boolean alreadyInList) {
        return neededRoundsToBuild(indexOrId, alreadyInList, false);
    }

    /**
     * Function calculates the estimated number of rounds until a project is done in this system
     * @param indexOrId
     * @param alreadyInList
     * @param usePotential
     * @return
     */
    public int neededRoundsToBuild(int indexOrId, boolean alreadyInList, boolean usePotential) {
        TurnsCalc turnsCalc = new TurnsCalc();
        float prod = usePotential ? production.getPotentialIndustryProd() : production.getIndustryProd();
        if (prod <= 0)
            return Integer.MAX_VALUE;
        float needed = 0;
        if (alreadyInList) {
            //need to be an assembly list index (maybe needs assert)
            needed = assemblyList.getNeededIndustryInAssemblyList(indexOrId);
            //till the following call it's perhaps an index, afterwards always an ID
            indexOrId = assemblyList.getAssemblyListEntry(indexOrId).id;
        }
        if (indexOrId < 0) { //upgrade
            if (!alreadyInList) {
                calculateNeededResources(indexOrId, resourceManager.getBuildingInfo(Math.abs(indexOrId)), null, null);
                needed = assemblyList.getNeededIndustryForBuild();
            }
            return turnsCalc.turns(needed, prod, production.getUpdateBuildSpeed());
        } else if (indexOrId < 10000) { //buildings
            if (!alreadyInList) {
                calculateNeededResources(indexOrId, resourceManager.getBuildingInfo(indexOrId), null, null);
                needed = assemblyList.getNeededIndustryForBuild();
            }
            if (resourceManager.getBuildingInfo(indexOrId).getNeverReady())
                return alreadyInList ? (int) needed : 0;
            return turnsCalc.turns(needed, prod, production.getBuildingBuildSpeed());
        } else if (indexOrId < 20000) { // ships, take into account shipyard efficiency
            if (!alreadyInList) {
                calculateNeededResources(indexOrId, null, resourceManager.getShipInfos().get(indexOrId - 10000), null);
                needed = assemblyList.getNeededIndustryForBuild();
            }
            int eff = production.getShipYardEfficiency();
            return turnsCalc.turns(needed, prod, production.getShipBuildSpeed(), eff);
        } else { // troops
            if (!alreadyInList) {
                calculateNeededResources(indexOrId, null, null, resourceManager.getTroopInfos().get(indexOrId - 20000));
                needed = assemblyList.getNeededIndustryForBuild();
            }
            int eff = production.getBarrackEfficiency();
            return turnsCalc.turns(needed, prod, production.getTroopBuildSpeed(), eff);
        }
    }

    /**
     * Function calculates, starting from the "base IPs", the actual industry points a system
     * generates, considering "never ready" buildings, UpdateBuildSpeed-Bonuses, BuildingBuildSpeed-Bonuses,
     * ShipYardEfficiency, BarrackEfficiency
     * @param buildingInfo
     * @param list
     * @return
     */
    public int calcIndustryPointsProd(Array<BuildingInfo> buildingInfo, int list) {
        int ipprod = production.getIndustryProd();
        //if the something order on the assemblylist is neverready (martial law), then take into account
        if (list > 0 && list < 10000 && buildingInfo.get(list - 1).getNeverReady()) {
            //one ip is subtracted except if the morale higher than 85, which cancels the order
            if (morale >= 85)
                ipprod = assemblyList.getNeededIndustryForBuild();
            else
                ipprod = 1;
        } else if (list < 0) //upgrade on list, apply possible bonus
            ipprod = (int) Math.floor((float) ipprod * (100 + production.getUpdateBuildSpeed()) / 100.0f);
        else if (list < 10000) //building in list, check possible bonus
            ipprod = (int) Math.floor((float) ipprod * (100 + production.getBuildingBuildSpeed()) / 100.0f);
        else if (list < 20000) //ship - take into account both shipyard and bonus
            ipprod = (int) Math.floor((float) ipprod * production.getShipYardEfficiency() / 100.0f
                    * (100 + production.getShipBuildSpeed()) / 100.0f);
        else
            //troop - take into account both shipyard and bonus
            ipprod = (int) Math.floor((float) ipprod * production.getBarrackEfficiency() / 100.0f
                    * (100 + production.getTroopBuildSpeed()) / 100.0f);
        return ipprod;
    }

    /**
     * Function checks based on the population in the system whether a new traderoute can be added
     * @param researchInfo
     * @return
     */
    public boolean canAddTradeRoute(ResearchInfo researchInfo) {
        //each 20Bil population adds a traderoute
        int currentTradeRoutes = tradeRoutes.size;
        int addResRoute = 1;
        int maxTradeRoutes = (int) (inhabitants / GameConstants.TRADEROUTEHAB) + production.getAddedTradeRoutes();
        int bonus = researchInfo.isResearchedThenGetBonus(ResearchComplexType.STORAGE_AND_TRANSPORT, 3);
        addResRoute += bonus;

        bonus = researchInfo.isResearchedThenGetBonus(ResearchComplexType.TRADE, 3);
        if (bonus != 0)
            if (maxTradeRoutes == 0) {
                addResRoute += bonus;
                maxTradeRoutes += bonus;
            }

        if (resourceRoutes.size > addResRoute)
            currentTradeRoutes += resourceRoutes.size - addResRoute;
        return (currentTradeRoutes < maxTradeRoutes);
    }

    /**
     * Function generates a new traderoute, if it returns true, then it could create the route.
     * @param dest
     * @param systems
     * @param researchInfo
     * @return
     */
    public boolean addTradeRoute(IntPoint dest, Array<StarSystem> systems, ResearchInfo researchInfo) {
        boolean canAddTradeRoute = canAddTradeRoute(researchInfo);
        //first check that there is no traderoute from a system which has the same owner as this system,
        //that leads to that target. You can only have one traderoute to a system.
        if (canAddTradeRoute)
            for (int j = 0; j < systems.size; j++) {
                StarSystem system = systems.get(j);
                if (system.getOwnerId().equals(ownerID))
                    if (system != this)
                        for (int i = 0; i < system.getTradeRoutes().size; i++)
                            if (system.getTradeRoutes().get(i).getDestCoord().equals(dest))
                                return false;
            }
        // check that there is no traderoute with the target, if there is then we should remove it
        for (int i = 0; i < tradeRoutes.size; i++) {
            TradeRoute tr = tradeRoutes.get(i);
            if (tr.getDestCoord().equals(dest)) {
                if (tr.getDuration() > 0) {
                    //if we want to cancel a traderoute, the duration has to be negative.
                    //if the route has a duration under 5, set it to the smaller value, else to -1
                    if (tr.getDuration() < 5)
                        tr.setDuration(tr.getDuration() - 6);
                    else
                        tr.setDuration(-1);
                    return true;
                } else
                    return false;
            }
        }
        if (canAddTradeRoute) {
            TradeRoute route = new TradeRoute();
            route.generateTradeRoute(dest);
            tradeRoutes.add(route);
            return true;
        }

        return false;
    }

    /**
     * @return the credits generated by all traderoutes
     */
    public int creditsFromTradeRoutes() {
        int credits = 0;
        for (int i = 0; i < tradeRoutes.size; i++)
            credits += tradeRoutes.get(i).getCredits(production.getIncomeOnTradeRoutes());
        return credits;
    }

    /**
     * Checks if the traderoutes are supposed to exist. If some were deleted, the return value equals their number
     * @param researchInfo
     * @return
     */
    public int checkTradeRoutes(ResearchInfo researchInfo) {
        int number = 0;
        //check the duration of the traderoutes. If it reaches -5, delete the traderoute. If 1 was reached, extend to 20 rounds again.
        for (int i = 0; i < tradeRoutes.size; i++) {
            if (getBlockade() > 0)
                tradeRoutes.get(i).setCredits(0);
            if (tradeRoutes.get(i).getDuration() <= -5) {
                tradeRoutes.removeIndex(i--);
                number++;
            } else if (tradeRoutes.get(i).getDuration() == 1)
                tradeRoutes.get(i).setDuration(20);
            else
                tradeRoutes.get(i).setDuration(tradeRoutes.get(i).getDuration() - 1);
        }
        //check population and buildings
        int currentTradeRoutes = tradeRoutes.size;
        int addResRoute = 1;
        int maxTradeRoutes = (int) (inhabitants / GameConstants.TRADEROUTEHAB) + production.getAddedTradeRoutes();
        //bonus from special research
        if (researchInfo.getResearchComplex(ResearchComplexType.STORAGE_AND_TRANSPORT).getFieldStatus(3) == ResearchStatus.RESEARCHED)
            addResRoute += researchInfo.getResearchComplex(ResearchComplexType.STORAGE_AND_TRANSPORT).getBonus(3);
        if (researchInfo.getResearchComplex(ResearchComplexType.TRADE).getFieldStatus(3) == ResearchStatus.RESEARCHED) {
            if (maxTradeRoutes == 0) {
                addResRoute += researchInfo.getResearchComplex(ResearchComplexType.TRADE).getBonus(3);
                maxTradeRoutes += researchInfo.getResearchComplex(ResearchComplexType.TRADE).getBonus(3);
            }
        }

        if (resourceRoutes.size > addResRoute)
            currentTradeRoutes += resourceRoutes.size - addResRoute;
        while (currentTradeRoutes > maxTradeRoutes) {
            if (tradeRoutes.size > 0) {
                tradeRoutes.removeIndex(tradeRoutes.size - 1);
                number++;
                currentTradeRoutes--;
            } else
                break;
        }
        return number;
    }

    /**
     * Calculates diplomacy-related effects of and to this system's trade routes.
     * @return number of deleted trade routes
     */
    public int checkTradeRoutesDiplomacy() {
        int deletedTradeRoutes = 0;
        for (int i = 0; i < tradeRoutes.size; i++) {
            TradeRoute route = tradeRoutes.get(i);
            IntPoint dest = route.getDestCoord();
            if (!route.checkTradeRoute(getCoordinates(), dest, resourceManager)) {
                tradeRoutes.removeIndex(i--);
                deletedTradeRoutes++;
            } else
                route.perhapsChangeRelationship(getCoordinates(), dest, resourceManager);
        }
        return deletedTradeRoutes;
    }

    /**
     * Function generates a new ResourceRoute
     * @param dest
     * @param res
     * @param starsystems
     * @param researchInfo
     * @return true if it could be created
     */
    public boolean addResourceRoute(IntPoint dest, int res, Array<StarSystem> starsystems, ResearchInfo researchInfo) {
        // there can be at least one resource route and there can be number of traderoutes + 1 resourceroutes
        int addResRoute = 1;
        //bonus from special research
        if (researchInfo.getResearchComplex(ResearchComplexType.STORAGE_AND_TRANSPORT).getFieldStatus(3) == ResearchStatus.RESEARCHED)
            addResRoute += researchInfo.getResearchComplex(ResearchComplexType.STORAGE_AND_TRANSPORT).getBonus(3);
        int maxTradeRoutes = (int) (inhabitants / GameConstants.TRADEROUTEHAB) + production.getAddedTradeRoutes();
        if (researchInfo.getResearchComplex(ResearchComplexType.TRADE).getFieldStatus(3) == ResearchStatus.RESEARCHED) {
            if (maxTradeRoutes == 0)
                addResRoute += researchInfo.getResearchComplex(ResearchComplexType.TRADE).getBonus(3);
        }
        int maxResourceRoutes = maxTradeRoutes + addResRoute;
        StarSystem destSystem = starsystems.get(resourceManager.coordsToIndex(dest));
        if (!destSystem.getOwnerId().equals(ownerID))
            return false;
        if (destSystem.getInhabitants() == 0.0f || getInhabitants() == 0.0f)
            return false;
        if (maxResourceRoutes <= resourceRoutes.size + tradeRoutes.size)
            return false;
        //is there a route with that resource in the destination system
        for (int i = 0; i < resourceRoutes.size; i++)
            if (resourceRoutes.get(i).getResource().getType() == res && resourceRoutes.get(i).getCoord().equals(dest))
                return false;

        ResourceRoute route = new ResourceRoute();
        route.generateResourceRoute(dest, res);
        resourceRoutes.add(route);
        return true;
    }

    /**
     * Checks if the resourceroutes are supposed to exist. If some were deleted, the return value equals their number
     * @param researchInfo
     * @return
     */
    public int checkResourceRoutes(ResearchInfo researchInfo) {
        // there can be at least one resource route and there can be number of traderoutes + 1 resourceroutes
        int addResRoute = 1;
        //bonus from special research
        if (researchInfo.getResearchComplex(ResearchComplexType.STORAGE_AND_TRANSPORT).getFieldStatus(3) == ResearchStatus.RESEARCHED)
            addResRoute += researchInfo.getResearchComplex(ResearchComplexType.STORAGE_AND_TRANSPORT).getBonus(3);
        int maxTradeRoutes = (int) (inhabitants / GameConstants.TRADEROUTEHAB) + production.getAddedTradeRoutes();
        if (researchInfo.getResearchComplex(ResearchComplexType.TRADE).getFieldStatus(3) == ResearchStatus.RESEARCHED) {
            if (maxTradeRoutes == 0)
                addResRoute += researchInfo.getResearchComplex(ResearchComplexType.TRADE).getBonus(3);
        }
        int maxResourceRoutes = maxTradeRoutes + addResRoute;
        int currentResourceRoutes = resourceRoutes.size + tradeRoutes.size;
        int number = 0;
        while (currentResourceRoutes > maxResourceRoutes) {
            if (resourceRoutes.size > 0) {
                resourceRoutes.removeIndex(resourceRoutes.size - 1);
                number++;
                currentResourceRoutes--;
            } else
                break;
        }
        return number;
    }

    /**Checks this system's resource routes for whether the target system is still part of the empire this system belongs to.
     * @return number of deleted resource routes
     */
    public int checkResourceRoutesExistence() {
        int deletedResourceRoutes = 0;
        for (int i = 0; i < resourceRoutes.size; i++) {
            ResourceRoute route = resourceRoutes.get(i);
            IntPoint dest = route.getCoord();
            if (!route.checkResourceRoute(getOwnerId(), resourceManager.getUniverseMap().getStarSystemAt(dest))) {
                resourceRoutes.removeIndex(i--);
                deletedResourceRoutes++;
            }
        }
        return deletedResourceRoutes;
    }

    //////////////////////////////////////////////////////////////////////
    //troops
    //////////////////////////////////////////////////////////////////////
    /**
     * Function calculates the buildable troops of this system
     * @param troopInfos
     * @param research
     */
    public void calculateBuildableTroops(Array<TroopInfo> troopInfos, Research research) {
        buildableTroops.clear();
        if (production.isBarrack())
            for (int i = 0; i < troopInfos.size; i++) {
                TroopInfo info = troopInfos.get(i);
                if (info.getOwner().equals(getOwnerId())) {
                    boolean buildable = true;
                    if (research.getBioTech() < info.getNeededTechlevel(0))
                        buildable = false;
                    if (research.getEnergyTech() < info.getNeededTechlevel(1))
                        buildable = false;
                    if (research.getComputerTech() < info.getNeededTechlevel(2))
                        buildable = false;
                    if (research.getPropulsionTech() < info.getNeededTechlevel(3))
                        buildable = false;
                    if (research.getConstructionTech() < info.getNeededTechlevel(4))
                        buildable = false;
                    if (research.getWeaponTech() < info.getNeededTechlevel(5))
                        buildable = false;
                    if (buildable)
                        buildableTroops.add(info.getID());
                }
            }
    }

    /**
     * If there are troops stationed in this system, their morale value is also taken into account.
     * If the system morale is under 100, then the morale of the unit is added, if it's above 100 then it's substracted
     * @param troopInfo
     */
    public void includeTroopMoraleValue(Array<TroopInfo> troopInfo) {
        if (troops.size > 0 && morale != 100)
            for (int i = 0; i < troops.size; i++) {
                int id = troops.get(i).getID();
                TroopInfo info = troopInfo.get(id);
                if (morale < 100) {
                    morale += info.getMoralValue();
                    if (morale > 100)
                        morale = 100;
                } else if (morale > 100) {
                    morale -= info.getMoralValue();
                    if (morale < 100)
                        morale = 100;
                }
            }
    }

    public void addTroop(Troop troop) {
        troops.add(troop);
    }

    public void trainTroops() {
        int xp = production.getTroopTraining();
        for (int i = 0; i < troops.size; i++)
            troops.get(i).addExperiencePoints(xp);
    }

    /**
     * Function sets the new owner of a system
     * @param newOne
     * @param status
     * @param resetTroops
     */
    public void changeOwner(String newOne, SystemOwningStatus status, boolean resetTroops) {
        owningStatus = status;
        if (getOwnerId().equals(newOne))
            return;

        setOwner(newOne);

        tradeRoutes.clear();
        resourceRoutes.clear();

        if (resetTroops)
            troops.clear();

        assemblyList.reset();
    }

    public void changeOwner(String newOne, SystemOwningStatus status) {
        changeOwner(newOne, status, true);
    }

    @Override
    public void calculateOwner() {
        if (owningStatus != SystemOwningStatus.OWNING_STATUS_EMPTY) {
            return;
        }
        super.calculateOwner();
    }

    public void executeManager(Major owner, boolean turnChange) {
        executeManager(owner, turnChange, true);
    }

    private static void managerMessage(String text, Major owner, IntPoint p) {
        EmpireNews message = new EmpireNews();
        message.CreateNews(text, EmpireNewsType.ECONOMY, "", p);
        owner.getEmpire().addMsg(message);
    }

    public void executeManager(Major owner, boolean turnChange, boolean energy) {
        if (!manager.isActive() || !owner.isHumanPlayer())
            return;
        String name = getName();
        if (energy && manager.checkEnergyConsumers(resourceManager, this) && turnChange)
            managerMessage(StringDB.getString("MANAGER_BOMB_WARNING", false, name), owner, coordinates);
        if (!manager.distributeWorkers(this))
            managerMessage(StringDB.getString("MANAGER_MALFUNCTION", false, name), owner, coordinates);
        if (turnChange && manager.checkFamine(this))
            managerMessage(StringDB.getString("MANAGER_FAMINE_WARNING", false, name), owner, coordinates);
    }

    public void setImageIndex(int imageIndex) {
        this.imageIndex = imageIndex;
    }

    public int getImageIndex() {
        return imageIndex;
    }

    public String getProdText() {
        String text = "";
        int id = getAssemblyList().getAssemblyListEntry(0).id;
        int roundToBuild = 0;
        int ip = getProduction().getIndustryProd();
        if (id > 0 && id < 1000) {
            if (ip > 0 && !resourceManager.getBuildingInfo(id).getNeverReady()) {
                roundToBuild = Math.round(getAssemblyList().getNeededIndustryInAssemblyList(0)
                        / ((float) ip * (100 + getProduction().getBuildingBuildSpeed()) / 100.0f));
                if (roundToBuild > 1)
                    text = String.format("%s (%d %s)", resourceManager.getBuildingInfo(id).getBuildingName(), roundToBuild,
                            StringDB.getString("ROUNDS"));
                else
                    text = String.format("%s (%d %s)", resourceManager.getBuildingInfo(id).getBuildingName(), roundToBuild,
                            StringDB.getString("ROUND"));
            } else
                text = String.format("%s (? %s)", resourceManager.getBuildingInfo(id).getBuildingName(),
                        StringDB.getString("ROUNDS"));
        } else if (id >= 10000 && id < 20000) {
            if (getProduction().getShipYardEfficiency() > 0 && ip > 0) {
                roundToBuild = Math.round(getAssemblyList().getNeededIndustryInAssemblyList(0)
                        / ((float) ip * getProduction().getShipYardEfficiency() / 100.0f
                                * (100 + getProduction().getShipBuildSpeed()) / 100.0f));
                if (roundToBuild > 1)
                    text = String.format("%s-%s (%d %s)", resourceManager.getShipInfos().get(id - 10000).getShipClass(),
                            StringDB.getString("CLASS"), roundToBuild, StringDB.getString("ROUNDS"));
                else
                    text = String.format("%s-%s (%d %s)", resourceManager.getShipInfos().get(id - 10000).getShipClass(),
                            StringDB.getString("CLASS"), roundToBuild, StringDB.getString("ROUND"));
            } else
                text = String.format("%s-%s (? %s)", resourceManager.getShipInfos().get(id - 10000).getShipClass(),
                        StringDB.getString("CLASS"), StringDB.getString("ROUNDS"));
        } else if (id >= 20000) {
            if (getProduction().getBarrackEfficiency() > 0 && ip > 0) {
                roundToBuild = Math.round(getAssemblyList().getNeededIndustryInAssemblyList(0)
                        / ((float) ip * getProduction().getBarrackEfficiency() / 100.0f
                                * (100 + getProduction().getTroopBuildSpeed()) / 100.0f));
                if (roundToBuild > 1)
                    text = String.format("%s (%d %s)", resourceManager.getTroopInfos().get(id - 20000).getName(), roundToBuild,
                            StringDB.getString("ROUNDS"));
                else
                    text = String.format("%s (%d %s)", resourceManager.getTroopInfos().get(id - 20000).getName(), roundToBuild,
                            StringDB.getString("ROUND"));
            } else
                text = String.format("%s (? %s)", resourceManager.getTroopInfos().get(id - 20000).getName(),
                        StringDB.getString("ROUNDS"));
        } else if (id < 0) {
            int tmp = Math.abs(id);
            if (ip > 0 && !resourceManager.getBuildingInfo(tmp).getNeverReady()) {
                roundToBuild = Math.round(getAssemblyList().getNeededIndustryInAssemblyList(0)
                        / ((float) ip * (100 + getProduction().getUpdateBuildSpeed()) / 100.0f));
                if (roundToBuild > 1)
                    text = String.format("Upg. %s (%d %s)", resourceManager.getBuildingInfo(tmp).getBuildingName(),
                            roundToBuild, StringDB.getString("ROUNDS"));
                else
                    text = String.format("Upg. %s (%d %s)", resourceManager.getBuildingInfo(tmp).getBuildingName(),
                            roundToBuild, StringDB.getString("ROUND"));
            } else
                text = String.format("Upg. %s (? %s)", resourceManager.getBuildingInfo(tmp).getBuildingName(),
                        StringDB.getString("ROUNDS"));
        }
        if (id > 0 && id < 10000 && resourceManager.getBuildingInfo(id).getNeverReady())
            text = String.format("%s (%s)", resourceManager.getBuildingInfo(id).getBuildingName(), StringDB.getString("RUNS"));
        if (getAutoBuild())
            text = StringDB.getString("AUTOBUILD") + ": " + text;
        return text;
    }

    public void drawStationData(Table table, Skin skin, String fontName, Color fontColor) {
        Major playerRace = resourceManager.getRaceController().getPlayerRace();
        //if an outpost is under construction
        ArrayMap<String, Major> majors = resourceManager.getRaceController().getMajors();
        for (int i = 0; i < majors.size; i++) {
            String raceID = majors.getKeyAt(i);
            if (getIsStationBuilding(raceID)) {
                Major race = majors.getValueAt(i);
                int percent = (getStartStationPoints(raceID) - getNeededStationPoints(raceID)) * 100 / getStartStationPoints(raceID);

                String raceName;
                if (playerRace.getRaceId().equals(raceID) || playerRace.isRaceContacted(raceID))
                    raceName = race.getName();
                else
                    raceName = StringDB.getString("UNKNOWN");

                ShipOrder type = stationWork(raceID);
                String station = "";
                if (type == ShipOrder.BUILD_OUTPOST)
                    station = StringDB.getString("OUTPOST")
                            + StringDB.getString("STATION_BUILDING", false, raceName, "" + percent);
                else if (type == ShipOrder.BUILD_STARBASE)
                    station = StringDB.getString("STARBASE")
                            + StringDB.getString("STATION_BUILDING", false, raceName, "" + percent);
                else if (type == ShipOrder.UPGRADE_OUTPOST)
                    station = StringDB.getString("OUTPOST")
                            + StringDB.getString("STATION_UPGRADING", false, raceName, "" + percent);
                else if (type == ShipOrder.UPGRADE_STARBASE)
                    station = StringDB.getString("STARBASE")
                            + StringDB.getString("STATION_UPGRADING", false, raceName, "" + percent);

                Label l = new Label(station, skin, fontName, fontColor);
                l.setAlignment(Align.center);
                l.setWrap(true);
                table.add(l).width(GameConstants.wToRelative(200));
                table.row();
            }
        }
    }

    public boolean isExpansionSector(String raceId) {
        if (!getFullKnown(raceId))
            return false;

        if (!isFree() && !getOwnerId().equals(raceId))
            return false;

        int value = getCompareValue(raceId);
        if (value > 0)
            return true;
        return false;
    }

    public static void setSortType(SystemSortType st) {
        sortType = st;
    }

    public static SystemSortType getSortType() {
        return sortType;
    }

    private static int compare(int x, int y) { //we can't use Integer.compare() because java 6 doesn't has it
        return (x < y) ? -1 : ((x == y) ? 0 : 1);
    }

    @Override
    public int compareTo(Sector o) {
        StarSystem other = (StarSystem) o;
        switch (sortType) {
            case BY_COORD:
                return coordinates.compareTo(other.coordinates);
            case BY_NAME:
                return name.compareTo(other.name);
            case BY_MORALE:
                return compare(morale, other.morale);
            case BY_FOODPROD:
                return compare(production.getFoodProd(), other.production.getFoodProd());
            case BY_FOODSTORAGE:
                return compare(getFoodStore(), other.getFoodStore());
            case BY_IPPROD:
                return compare(production.getIndustryProd(), other.production.getIndustryProd());
            case BY_CREDITSPROD:
                return compare(production.getCreditsProd(), other.production.getCreditsProd());
            case BY_ORDER:
                return getProdText().compareTo(other.getProdText());
            case BY_TITANSTORE:
                return compare(getTitanStore(), other.getTitanStore());
            case BY_DEUTERIUMSTORE:
                return compare(getDeuteriumStore(), other.getDeuteriumStore());
            case BY_DURANIUMSTORE:
                return compare(getDuraniumStore(), other.getDuraniumStore());
            case BY_CRYSTALSTORE:
                return compare(getCrystalStore(), other.getCrystalStore());
            case BY_IRIDIUMSTORE:
                return compare(getIridiumStore(), other.getIridiumStore());
            case BY_DERITIUMSTORE:
                return compare(getDeritiumStore(), other.getDeritiumStore());
            case BY_TROOPS:
                return compare(getTroops().size, other.getTroops().size);
            case BY_SHIELD:
                return compare(production.getShieldPower(), other.production.getShieldPower());
            case BY_SHIPDEFENSE:
                return compare(production.getShipDefend(), other.production.getShipDefend());
            case BY_GROUNDDEFENSE:
                return compare(production.getGroundDefend(), other.production.getGroundDefend());
            case BY_SCANSTRENGTH:
                return compare(production.getScanPower(), other.production.getScanPower());
            case BY_SECTORVALUE:
                String raceID = resourceManager.getRaceController().getPlayerRaceString();
                return compare(getCompareValue(raceID), other.getCompareValue(raceID));
        }
        return 0;
    }

    @Override
    public void write(Kryo kryo, Output output) {
        write(kryo, output, false);
    }

    public void write(Kryo kryo, Output output, boolean endOfRound) {
        if (!endOfRound) {
            super.write(kryo, output);
            if (!isSunSystem() || ownerID.isEmpty() && colonyOwner.isEmpty() && getMinorRace() == null) {
                output.writeBoolean(false);
                return;
            }
            output.writeBoolean(true);
        }

        kryo.writeObject(output, owningStatus);
        output.writeDouble(inhabitants);
        kryo.writeObject(output, assemblyList);
        kryo.writeObject(output, production);
        kryo.writeObject(output, buildings);
        kryo.writeObject(output, buildableBuildings);
        kryo.writeObject(output, buildableUpdates);
        kryo.writeObject(output, buildableShips);
        kryo.writeObject(output, buildableTroops);
        kryo.writeObject(output, alwaysBuildableBuildings);
        kryo.writeObject(output, buildableWithoutAssemblylistCheck);
        kryo.writeObject(output, workers);
        output.writeInt(morale);
        output.writeInt(blockade);
        kryo.writeObject(output, disabledProductions);
        kryo.writeObject(output, store);
        kryo.writeObject(output, buildingDestroy);
        output.writeInt(foodBuildings);
        output.writeInt(industryBuildings);
        output.writeInt(energyBuildings);
        output.writeInt(securityBuildings);
        output.writeInt(researchBuildings);
        output.writeInt(titanMines);
        output.writeInt(deuteriumMines);
        output.writeInt(duraniumMines);
        output.writeInt(crystalMines);
        output.writeInt(iridiumMines);
        kryo.writeObject(output, tradeRoutes);
        kryo.writeObject(output, resourceRoutes);
        output.writeInt(maxTradeRoutesFromHab);
        kryo.writeObject(output, troops);
        output.writeBoolean(autoBuild);
        kryo.writeObject(output, manager);
        kryo.writeObject(output, buildTargetCoord);
    }

    @Override
    public void read(Kryo kryo, Input input) {
        read(kryo, input, false);
    }

    @SuppressWarnings("unchecked")
    public void read(Kryo kryo, Input input, boolean endOfRound) {
        if (!endOfRound) {
            super.read(kryo, input);
            if (!input.readBoolean())
                return;
        }
        owningStatus = kryo.readObject(input, SystemOwningStatus.class);
        inhabitants = input.readDouble();
        assemblyList = kryo.readObject(input, AssemblyList.class);
        production = kryo.readObject(input, SystemProd.class);
        buildings = kryo.readObject(input, Array.class);
        buildableBuildings = kryo.readObject(input, IntArray.class);
        buildableUpdates = kryo.readObject(input, IntArray.class);
        buildableShips = kryo.readObject(input, IntArray.class);
        buildableTroops = kryo.readObject(input, IntArray.class);
        alwaysBuildableBuildings = kryo.readObject(input, IntArray.class);
        buildableWithoutAssemblylistCheck = kryo.readObject(input, IntArray.class);
        workers = kryo.readObject(input, Worker.class);
        morale = input.readInt();
        blockade = input.readInt();
        disabledProductions = kryo.readObject(input, boolean[].class);
        store = kryo.readObject(input, GameResources.class);
        buildingDestroy = kryo.readObject(input, IntArray.class);
        foodBuildings = input.readInt();
        industryBuildings = input.readInt();
        energyBuildings = input.readInt();
        securityBuildings = input.readInt();
        researchBuildings = input.readInt();
        titanMines = input.readInt();
        deuteriumMines = input.readInt();
        duraniumMines = input.readInt();
        crystalMines = input.readInt();
        iridiumMines = input.readInt();
        tradeRoutes = kryo.readObject(input, Array.class);
        resourceRoutes = kryo.readObject(input, Array.class);
        maxTradeRoutesFromHab = input.readInt();
        troops = kryo.readObject(input, Array.class);
        autoBuild = input.readBoolean();
        manager = kryo.readObject(input, SystemManager.class);
        if (((KryoV) kryo).getSaveVersion() >= 6) //added in save version 6
            buildTargetCoord = kryo.readObject(input, IntPoint.class);
    }
}