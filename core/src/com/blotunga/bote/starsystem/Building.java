/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.starsystem;

public class Building implements Comparable<Building> {
    private int runningNumber; // ID of the building;
    private boolean isOnline; // is the building online?

    public Building() {
        runningNumber = 0;
        isOnline = false;
    }

    public Building(int id) {
        runningNumber = id;
        isOnline = false;
    }

    public int getRunningNumber() {
        return runningNumber;
    }

    public boolean getIsBuildingOnline() {
        return isOnline;
    }

    public void setIsBuildingOnline(boolean isOnline) {
        this.isOnline = isOnline;
    }

    @Override
    public int compareTo(Building arg0) {
        if (runningNumber < arg0.runningNumber)
            return -1;
        else if (runningNumber == arg0.runningNumber)
            return 0;
        else
            return 1;
    }
}
