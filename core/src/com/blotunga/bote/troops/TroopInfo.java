/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.troops;

import java.util.Arrays;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.ResearchComplexType;
import com.blotunga.bote.constants.ResearchStatus;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.ResearchInfo;

/**
 * @author dragon
 * @version 1.1
 */
public class TroopInfo extends Troop {
    private String name;			// name of the unit
    private String description;		// description of the unit
    private String graphicFile;		// graphic file of the unit
    private int maintenanceCosts;	// maintenance costs of the unit
    private int[] neededTechs;		// this keeps the necessary research level to build this unit
    private int[] neededResources;	// this keeps the resources needed to build this unit
    private int neededIndustry;		// the industry needed to build this unit
    private int size;				// the number of soldiers/the place needed in a transport
    private int moralValue;			// moral value of the troop, this is used to influence system morale

    public TroopInfo() {
        this("", "", "", 0, 0, 0, null, null, 0, 0, "", 0, 0);
    }

    public TroopInfo(String name, String description, String graphicFile, int offense, int defense,
            int maintenanceCosts, int[] neededTechs, int[] neededResources, int neededIndustry, int ID, String owner,
            int size, int moralValue) {
        this.neededTechs = new int[6];
        Arrays.fill(this.neededTechs, 0);
        this.neededResources = new int[5];
        Arrays.fill(this.neededResources, 0);

        this.name = name;
        this.description = description;
        this.graphicFile = graphicFile;
        this.offense = offense;
        this.defense = defense;
        this.maintenanceCosts = maintenanceCosts;
        if (neededTechs != null)
            this.neededTechs = neededTechs.clone();
        if (neededResources != null)
            this.neededResources = neededResources.clone();
        this.neededIndustry = neededIndustry;
        this.ID = ID;
        this.owner = owner;
        this.size = size;
        this.moralValue = moralValue;
        this.experience = 0;
    }

    public TroopInfo(TroopInfo info) {
        ID = info.ID;
        owner = info.owner;
        offense = info.offense;
        defense = info.defense;
        experience = info.experience;
        name = info.name;
        description = info.description;
        graphicFile = info.graphicFile;
        maintenanceCosts = info.maintenanceCosts;
        neededTechs = info.neededTechs.clone();
        neededResources = info.neededResources.clone();
        neededIndustry = info.neededIndustry;
        size = info.size;
        moralValue = info.moralValue;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getGraphicFile() {
        return graphicFile;
    }

    public int getMaintenanceCosts() {
        return maintenanceCosts;
    }

    public int[] getNeededTechs() {
        return neededTechs;
    }

    public int getNeededTechlevel(int tech) {
        return neededTechs[tech];
    }

    public int[] getNeededResources() {
        return neededResources;
    }

    public int getNeededIndustry() {
        return neededIndustry;
    }

    public int getSize() {
        return size;
    }

    public int getMoralValue() {
        return moralValue;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setGraphicFile(String file) {
        graphicFile = file;
    }

    public void setNeededIndustry(int ip) {
        neededIndustry = ip;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public void setMaintainance(int cost) {
        maintenanceCosts = cost;
    }

    public void setMorale(int morale) {
        this.moralValue = morale;
    }

    public int getNeededResource(int type) {
        return neededResources[type];
    }

    public void setNeededResource(int type, int value) {
        neededResources[type] = value;
    }

    public void setNeededTechLevel(int type, int value) {
        neededTechs[type] = value;
    }
/**
     * Function returns whether a troop type can be built at current research level
     * @param researchLevels
     * @return
     */
    public boolean isThisTroopBuildableNow(int[] researchLevels) {
        for (int i = 0; i < researchLevels.length; i++)
            if (researchLevels[i] < this.neededTechs[i])
                return false;
        return true;
    }

    public void drawTroopInfo(ResearchInfo researchInfo, Table table, Skin skin, Color normalColor, Color markColor, float width, float vpadding) {
        Label nameLabel = new Label(name, skin, "normalFont", markColor);
        nameLabel.setWrap(true);
        nameLabel.setAlignment(Align.center);
        table.add(nameLabel).width(width).spaceBottom(vpadding);
        table.row();
        String text = "";
        int offPower = getOffense();
        if (researchInfo.getResearchComplex(ResearchComplexType.TROOPS).getFieldStatus(1) == ResearchStatus.RESEARCHED)
            offPower += (offPower * researchInfo.getResearchComplex(ResearchComplexType.TROOPS).getBonus(1) / 100.0f);
        text += StringDB.getString("OPOWER") + ": " + offPower + "\n";
        text += StringDB.getString("DPOWER") + ": " + getDefense() + "\n";
        text += StringDB.getString("MORALVALUE") + ": " + getMoralValue() + "\n";
        text += StringDB.getString("PLACE") + ": " + getSize() + "\n";
        text += StringDB.getString("MAINTENANCE_COSTS") + ": " + getMaintenanceCosts() + "\n";
        Label infoLabel = new Label(text, skin, "normalFont", normalColor);
        infoLabel.setWrap(true);
        infoLabel.setAlignment(Align.center);
        table.add(infoLabel).width(width);
    }

    public void getTooltip(ResearchInfo researchInfo, Table table, Skin skin, String headerFont, Color headerColor, String textFont, Color textColor) {
        table.clearChildren();
        String text = getName();
        Label l = new Label(text, skin, headerFont, headerColor);
        table.add(l);
        table.row();

        int offPower = getOffense();
        if (researchInfo != null && researchInfo.getResearchComplex(ResearchComplexType.TROOPS).getFieldStatus(1) == ResearchStatus.RESEARCHED)
            offPower += (offPower * researchInfo.getResearchComplex(ResearchComplexType.TROOPS).getBonus(1) / 100.0f);
        text = StringDB.getString("OPOWER") + ": " + offPower + "\n";
        text += StringDB.getString("DPOWER") + ": " + getDefense() + "\n";
        text += StringDB.getString("MORALVALUE") + ": " + getMoralValue() + "\n";
        text += StringDB.getString("PLACE") + ": " + getSize();
        l = new Label(text, skin, textFont, headerColor);
        l.setWrap(true);
        l.setAlignment(Align.center);
        table.add(l).width(GameConstants.wToRelative(400));
        table.row();

        text = getDescription();
        l = new Label(text, skin, textFont, textColor);
        l.setWrap(true);
        l.setAlignment(Align.center);
        table.add(l).width(GameConstants.wToRelative(400));
        table.row();
    }
}
