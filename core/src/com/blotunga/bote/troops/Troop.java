/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.troops;

import com.blotunga.bote.utils.RandUtil;

/**
 * @author dragon
 * @version 1
 */
public class Troop {
    protected int ID;			/// helper ID
    protected String owner;		/// race which owns the troops
    protected int offense;		/// offensive power of these troops
    protected int defense;  	/// defensive power of troops
    protected int experience; 	/// experience of the troops

    public Troop() {
        ID = 0;
        owner = "";
        offense = 0;
        defense = 0;
        experience = 0;
    }

    public Troop(Troop oldTroop) {
        ID = oldTroop.ID;
        owner = oldTroop.owner;
        offense = oldTroop.offense;
        defense = oldTroop.defense;
        experience = oldTroop.experience;
    }

    public int getID() {
        return ID;
    }

    public String getOwner() {
        return owner;
    }

    public int getOffense() {
        return offense;
    }

    public int getDefense() {
        return defense;
    }

    public int getExperience() {
        return experience;
    }

    public void setOffense(int power) {
        offense = power;
    }

    public void setDefense(int power) {
        defense = power;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public void addExperiencePoints(int add) {
        experience += add;
    }

    private int getDamageBonusForExp() {
        int damageBoni = 0;
        if (experience > 64000)
            damageBoni = 80;
        else if (experience >= 32000)
            damageBoni = 70;
        else if (experience >= 16000)
            damageBoni = 60;
        else if (experience >= 8000)
            damageBoni = 50;
        else if (experience >= 4000)
            damageBoni = 40;
        else if (experience >= 2000)
            damageBoni = 30;
        else if (experience >= 1000)
            damageBoni = 20;
        else if (experience >= 500)
            damageBoni = 10;
        return damageBoni;
    }

    /**
     * This function is called when attacking an enemy troop.
     * There can be an offensive bonus, or a defensive bonus of the system for example
     * @param enemy
     * @param offenceBoni
     * @param defenceBoni
     * @return 0 if this troop wins, 1 if the enemy wins, 2 if they are both annihilated
     */
    public int attack(Troop enemy, int offenceBoni, int defenceBoni) {
        // An attack is pretty simple, the strength of the attacking troop is pitched against the defending one
        // taking chance into account. Those who have the higher die roll wins
        // If the roll is equal then they both lost.
        // Experience is also counted into the damage

        // the attacker (this object)
        int damageBoni = getDamageBonusForExp();

        damageBoni += offenceBoni;
        int attackPower = offense + offense * damageBoni / 100;
        int expBonusForDef = attackPower * 100;

        // the defender
        damageBoni = enemy.getDamageBonusForExp();
        int defencePower = enemy.defense + enemy.defense * damageBoni / 100;
        defencePower += defencePower * defenceBoni / 100;
        int expBonusForAtt = defencePower * 100;

        // a very strong unit should almost never loose against a much weaker one
        int attackValue = 0;
        if (attackPower > 0) {
            attackValue = attackPower;
            for (int i = 0; i < 2; i++)
                attackValue += (int) (RandUtil.random() * attackPower);
        }
        int defenceValue = 0;
        if (defencePower > 0) {
            defenceValue = defencePower;
            for (int i = 0; i < 2; i++)
                defenceValue += (int) (RandUtil.random() * defencePower);
        }

        if (attackValue > defenceValue) { // attacker wins
            experience += expBonusForAtt;
            return 0;
        } else if (attackValue < defenceValue) { // defender wins
            enemy.experience += expBonusForDef;
            return 1;
        } else
            return 2;
    }
}
