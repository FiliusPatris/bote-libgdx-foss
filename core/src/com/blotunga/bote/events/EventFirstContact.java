/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.events;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.PlayerRaces;
import com.blotunga.bote.constants.SndMgrValue;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.races.Minor;
import com.blotunga.bote.races.Race;
import com.blotunga.bote.races.Research;
import com.blotunga.bote.ships.ShipInfo;
import com.blotunga.bote.starsystem.BuildingInfo;
import com.blotunga.bote.utils.ui.BaseTooltip;

public class EventFirstContact extends EventScreen {
    private String raceId;
    private Array<String> imgPaths;

    public EventFirstContact(ResourceManager game, String raceId) {
        super(game, "FirstContact", EventScreenType.EVENT_SCREEN_TYPE_FIRST_CONTACT);
        this.raceId = raceId;
        imgPaths = new Array<String>();
    }

    @Override
    public void show() {
        super.show();
        Race contactedRace = game.getRaceController().getRace(raceId);
        if (contactedRace.isMajor())
            game.getSoundManager().playSound(SndMgrValue.SNDMGR_MSG_FIRSTCONTACT,
                    Major.toMajor(contactedRace).getPrefix());
        else
            game.getSoundManager().playSound(SndMgrValue.SNDMGR_MSG_ALIENCONTACT, playerRace.getPrefix());
        Rectangle rect = GameConstants.coordsToRelative(821, 420, 607, 84);
        headLineTable.clear();
        headLineTable.add(StringDB.getString("FIRSTCONTACTEVENT_HEADLINE"), "hugeFont", normalColor);
        headLineTable.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        textTable.clear();
        rect = GameConstants.coordsToRelative(821, 370, 607, 84);
        textTable.add(StringDB.getString("FIRSTCONTACTEVENT_TEXT", false, contactedRace.getName()), "hugeFont",
                normalColor);
        textTable.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);

        Image raceImg = new Image();
        String path = "graphics/races/" + contactedRace.getGraphicFileName() + ".jpg";
        imgPaths.add(path);
        raceImg.setDrawable(new TextureRegionDrawable(new TextureRegion(game.loadTextureImmediate(path))));
        rect = GameConstants.coordsToRelative(8, 495, 338, 237);
        raceImg.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        Table ttable = BaseTooltip.createTableTooltip(raceImg, tooltipTexture).getActor();
        contactedRace.getTooltip(ttable, skin, tooltipHeaderFont, tooltipHeaderColor, tooltipTextFont, tooltipTextColor, true);
        stage.addActor(raceImg);

        Table raceNameTable = new Table();
        raceNameTable.align(Align.center);
        raceNameTable.setSkin(skin);
        raceNameTable.setTouchable(Touchable.disabled);
        rect = GameConstants.coordsToRelative(8, 245, 338, 40);
        raceNameTable.add(contactedRace.getName(), "xxlFont", normalColor);
        raceNameTable.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        stage.addActor(raceNameTable);

        Table propertiesTable = new Table();
        propertiesTable.align(Align.top);
        propertiesTable.setSkin(skin);
        propertiesTable.setTouchable(Touchable.disabled);
        rect = GameConstants.coordsToRelative(920, 720, 400, 300);
        propertiesTable.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        stage.addActor(propertiesTable);

        String progress = "";
        if (contactedRace.isMinor()) {
            Minor minor = (Minor) contactedRace;
            if (!minor.isAlien())
                progress = minor.getTechnologicalProgressAsString();
        } else if (contactedRace.isMajor()) {
            Research research = playerRace.getEmpire().getResearch();
            double ourLevel = research.getBioTech() + research.getEnergyTech() + research.getComputerTech()
                    + research.getPropulsionTech() + research.getConstructionTech() + research.getWeaponTech();
            ourLevel /= 6.0;
            research = ((Major) contactedRace).getEmpire().getResearch();
            double theirLevel = research.getBioTech() + research.getEnergyTech() + research.getComputerTech()
                    + research.getPropulsionTech() + research.getConstructionTech() + research.getWeaponTech();
            theirLevel /= 6.0;

            //if the difference is under a half techlevel then it will be displayed as Normal Developed
            double diff = ourLevel - theirLevel;
            if (Math.abs(diff) <= 0.5)
                progress = StringDB.getString("NORMAL_DEVELOPED");
            else if (diff > 0.0) {
                if (diff >= 2.0)
                    progress = StringDB.getString("VERY_UNDERDEVELOPED");
                else
                    progress = StringDB.getString("UNDERDEVELOPED");
            } else {
                if (diff <= -2.0)
                    progress = StringDB.getString("VERY_DEVELOPED");
                else
                    progress = StringDB.getString("DEVELOPED");
            }
        }

        propertiesTable.add(progress, "largeFont", normalColor);
        float padding = GameConstants.wToRelative(10);
        propertiesTable.row().space(padding);
        propertiesTable.add(StringDB.getString("PROPERTIES"), "xxlFont", listMarkTextColor);
        Array<String> properties = contactedRace.getPropertiesAsStrings();
        for (String property : properties) {
            propertiesTable.row();
            propertiesTable.add(property, "largeFont", normalColor);
        }

        //draw relationship
        if (contactedRace.isMinor() || (contactedRace.isMajor() && !((Major) contactedRace).isHumanPlayer())) {
            Texture texture = game.getAssetManager().get(GameConstants.UI_BG_SIMPLE);
            TextureRegion dr = new TextureRegion(texture);
            for (int i = 0; i < 20; i++) {
                Sprite sprite = new Sprite(dr);
                Color color;
                if (contactedRace.getRelation(playerRace.getRaceId()) * 2 / 10 > i)
                    color = new Color((250 - i * 12) / 255.0f, (i * 12) / 255.0f, 0.0f, 200 / 255.0f);
                else
                    color = new Color(100 / 255.0f, 100 / 255.0f, 100 / 255.0f, 100 / 255.0f);
                sprite.setColor(color);
                Drawable newDrawable = new SpriteDrawable(sprite);
                Image relImage = new Image(newDrawable);
                relImage.setTouchable(Touchable.disabled);
                rect = GameConstants.coordsToRelative(8 + (i * 17), 530, 13, 30);
                relImage.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
                stage.addActor(relImage);
            }
        }

        //draw special buildings or ships in case it's a minor
        int count = 0;
        if (contactedRace.isMinor()) {
            for (int i = 0; i < game.getBuildingInfos().size; i++) {
                BuildingInfo bi = game.getBuildingInfos().get(i);
                if (bi.getOwnerOfBuilding() == PlayerRaces.NOBODY.getType())
                    if (bi.isOnlyMinorRace())
                        if (bi.getOnlyInSystemWithName().equals(contactedRace.getHomeSystemName())) {
                            path = "graphics/buildings/" + bi.getGraphicFileName() + ".png";
                            imgPaths.add(path);
                            drawImage(new TextureRegion(game.loadTextureImmediate(path)), bi.getBuildingName(), count, bi, null);
                            count++;
                        }
            }
            for (int i = 0; i < game.getShipInfos().size; i++) {
                ShipInfo si = game.getShipInfos().get(i);
                if (si.getRace() == PlayerRaces.MINORNUMBER.getType())
                    if (si.getOnlyInSystem().equals(contactedRace.getHomeSystemName())) {
                        path = "graphics/ships/" + si.getShipImageName() + ".png";
                        imgPaths.add(path);
                        drawImage(new TextureRegion(game.loadTextureImmediate(path)), si.getShipClass() + "-"
                                + StringDB.getString("CLASS") + " (" + si.getShipTypeAsString() + ")", count, null, si);
                        count++;
                    }
            }
        }
    }

    private void drawImage(TextureRegion region, String name, int count, BuildingInfo bi, ShipInfo si) {
        Table buildingTable = new Table(skin);
        buildingTable.setTouchable(Touchable.disabled);
        Image buildingImage = new Image();
        if (region != null)
            buildingImage.setDrawable(new TextureRegionDrawable(region));
        Rectangle rect = GameConstants.coordsToRelative(28 + count * 220, 185, 150, 113);
        buildingImage.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        if(bi != null)
            bi.getTooltip(BaseTooltip.createTableTooltip(buildingImage, tooltipTexture).getActor(), skin, tooltipHeaderFont, tooltipHeaderColor, tooltipTextFont, tooltipTextColor);
        else if(si != null)
            si.getTooltip(null, BaseTooltip.createTableTooltip(buildingImage, tooltipTexture).getActor(), skin, tooltipHeaderFont, tooltipHeaderColor, tooltipTextFont, tooltipTextColor);
        stage.addActor(buildingImage);
        buildingTable.add(name, "mediumFont", playerRace.getRaceDesign().clrNormalText);
        rect = GameConstants.coordsToRelative(28 + count * 220, 70, 150, 25);
        buildingTable.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        stage.addActor(buildingTable);
    }

    @Override
    public void hide() {
        for (String path : imgPaths)
            if (game.getAssetManager().isLoaded(path))
                game.getAssetManager().unload(path);
        imgPaths.clear();
    }
}
