/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.events;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ScalingViewport;
import com.blotunga.bote.DefaultScreen;
import com.blotunga.bote.GamePreferences;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.ViewTypes;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;

public abstract class EventScreen extends DefaultScreen {
    protected Stage stage;
    private OrthographicCamera camera;
    private final String imagePath;
    protected Skin skin;
    private EventScreenType type;
    protected String headLine;
    protected String text;
    protected Table headLineTable;
    protected Table textTable;
    protected Major playerRace;
    protected Color normalColor;
    protected Color listMarkTextColor;
    private TextButton okButton;
    //tooltip
    protected Color tooltipHeaderColor;
    protected Color tooltipTextColor;
    protected String tooltipHeaderFont = "xlFont";
    protected String tooltipTextFont = "largeFont";
    protected Texture tooltipTexture;
    protected Table buttonTable;

    public EventScreen(ResourceManager game, String imageName, EventScreenType type) {
        this(game, imageName, "", "", type);
    }

    public EventScreen(final ResourceManager game, String imageName, String headLine, String text, EventScreenType type) {
        super(game);
        this.imagePath = "graphics/events/" + imageName + ".jpg";
        this.headLine = headLine;
        this.text = text;
        this.type = type;
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                playerRace = game.getRaceController().getPlayerRace();
                normalColor = playerRace.getRaceDesign().clrNormalText;
                listMarkTextColor = playerRace.getRaceDesign().clrListMarkTextColor;

                tooltipTexture = game.getAssetManager().get(GameConstants.UI_BG_ROUNDED);
                tooltipHeaderColor = game.getRaceController().getPlayerRace().getRaceDesign().clrListMarkTextColor;
                tooltipTextColor = game.getRaceController().getPlayerRace().getRaceDesign().clrNormalText;

                camera = new OrthographicCamera();
                stage = new Stage(new ScalingViewport(Scaling.fit, GamePreferences.sceneWidth, GamePreferences.sceneHeight, camera));

                skin = game.getSkin();

                headLineTable = new Table();
                headLineTable.align(Align.center);
                headLineTable.setSkin(skin);
                headLineTable.setTouchable(Touchable.disabled);

                textTable = new Table();
                textTable.align(Align.center);
                textTable.setSkin(skin);
                textTable.setTouchable(Touchable.disabled);

                buttonTable = new Table();
                buttonTable.align(Align.center);
            }
        });
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(1 / 30.0f);
        stage.draw();
        game.getAchievementManager().render();
    }

    @Override
    public void show() {
        setInputProcessor(stage);
        // Wait until they are finished loading
        game.getAssetManager().finishLoading();
        Image background = new Image(game.loadTextureImmediate(imagePath));
        background.setBounds(0, 0, GamePreferences.sceneWidth, GamePreferences.sceneHeight);
        stage.addActor(background);
        stage.addActor(headLineTable);
        stage.addActor(textTable);
        stage.addActor(buttonTable);

        TextButtonStyle style = skin.get("default", TextButtonStyle.class);
        okButton = new TextButton(StringDB.getString("BTN_OKAY"), style);
        Rectangle rect = GameConstants.coordsToRelative(type == EventScreenType.EVENT_SCREEN_TYPE_VICTORY ? 540 : 630, 40, type == EventScreenType.EVENT_SCREEN_TYPE_VICTORY ? 350 : 170, 35);
        buttonTable.clear();
        buttonTable.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        buttonTable.add(okButton).width(GameConstants.wToRelative(170)).height(GameConstants.hToRelative(35));
        okButton.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                System.out.println("x = " + x + " ,y = " + y);
                processNextScreen();
                return false;
            }

            @Override
            public boolean keyDown(InputEvent event, int keycode) {
                if (keycode == Keys.ENTER || keycode == Keys.SPACE)
                    processNextScreen();
                return false;
            }
        });
        stage.setKeyboardFocus(okButton);
        game.getAchievementManager().popupAchievements();
    }

    protected void processNextScreen() {
        ScreenManager screenManager = (ScreenManager) game;
        EventScreen screen = screenManager.getRaceController().getPlayerRace().getEmpire().firstEvent();
        if (type != EventScreenType.EVENT_SCREEN_TYPE_GAME_OVER
                && (screenManager.isContinueAfterVictory() || type != EventScreenType.EVENT_SCREEN_TYPE_VICTORY)) {
            if (screen != null)
                screenManager.setScreen(screen);
            else {
                ViewTypes nextView = game.getClientWorker().getNextActiveView(playerRace.getRaceId());
                if (nextView != ViewTypes.NULL_VIEW) {
                    DefaultScreen scr = ((ScreenManager) game).setView(nextView);
                    if (nextView == ViewTypes.EMPIRE_VIEW)
                        ((ScreenManager) game).setSubMenu(scr, 0);
                }
                screenManager.getClientWorker().commitSoundMessages(screenManager.getSoundManager(), playerRace);
                screenManager.getSoundManager().playMessages();
            }
        } else {
            ((ScreenManager) game).setView(ViewTypes.MAIN_MENU, true);
        }
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
        game.getAchievementManager().updateSize(width, height, true);
    }

    @Override
    public void hide() {
        dispose();
    }

    @Override
    public void dispose() {
        if (game.getAssetManager().isLoaded(imagePath))
            game.getAssetManager().unload(imagePath);
        stage.dispose();
    }
}
