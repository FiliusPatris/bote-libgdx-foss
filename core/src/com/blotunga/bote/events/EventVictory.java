/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.events;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.utils.Align;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.achievements.AchievementsList;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.VictoryType;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;

public class EventVictory extends EventScreen {
    private String winnerRace;
    private VictoryType victoryType;

    public EventVictory(ResourceManager game, String winnerRace, VictoryType victoryType, String imgName) {
        super(game, imgName, EventScreenType.EVENT_SCREEN_TYPE_VICTORY);
        this.winnerRace = winnerRace;
        this.victoryType = victoryType;
    }

    @Override
    public void show() {
        super.show();
        String type = StringDB.getString(victoryType.getDBName());
        if (game.getMusic() != null) {
            game.getMusic().stop();
            game.getMusic().dispose();
        }
        int gamePoints = game.getStatistics().getGamePoints(playerRace.getRaceId(), game.getCurrentRound(),
                game.getGamePreferences().difficulty.getLevel());
        if (winnerRace.equals(playerRace.getRaceId())) {
            game.setMusic("sounds/VictoryTheme.ogg");
            game.getMusic().setVolume(game.getGameSettings().musicVolume);
            game.getMusic().play();
            headLine = StringDB.getString("VICTORY");
            Rectangle rect = GameConstants.coordsToRelative(0, 740, 1440, 40);
            Table table = new Table();
            table.setSkin(skin);
            table.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
            table.add(StringDB.getString("CONGRATULATIONS"), "xxlFont", normalColor);
            stage.addActor(table);

            gamePoints *= 2; // the winner gets double points
            String starDate = String.format("%s: %.1f", StringDB.getString("STARDATE"), game.getStarDate());
            text = String.format("%s:\n\n%s\n\n%s\n%s\n\n%s %d",
                            StringDB.getString("LOGBOOK_ENTRY"),
                            starDate,
                            StringDB.getString("WE_REACHED_VICTORY_IN_TYPE", false, type),
                            StringDB.getString("BIRTH_OF_THE_EMPIRE_END"),
                            StringDB.getString("REACHED_GAMEPOINTS"),
                            gamePoints);
            updateAchievements();

            TextButtonStyle style = skin.get("default", TextButtonStyle.class);
            TextButton continueButton = new TextButton(StringDB.getString("BTN_CONTINUE"), style);
            buttonTable.add(continueButton).width(GameConstants.wToRelative(170)).height(GameConstants.hToRelative(35)).spaceLeft(GameConstants.wToRelative(10));
            continueButton.addListener(new InputListener() {
                @Override
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    System.out.println("x = " + x + " ,y = " + y);
                    game.setContinueAfterVictory(true);
                    processNextScreen();
                    return false;
                }

                @Override
                public boolean keyDown(InputEvent event, int keycode) {
                    if (keycode == Keys.ENTER || keycode == Keys.SPACE) {
                        game.setContinueAfterVictory(true);
                        processNextScreen();
                    }
                    return false;
                }
            });
        } else {
            game.setMusic("sounds/LosingTheme.ogg");
            game.getMusic().setVolume(game.getGameSettings().musicVolume);
            game.getMusic().play();
            headLine = StringDB.getString("DEFEAT");
            Major winner = Major.toMajor(game.getRaceController().getRace(winnerRace));
            String raceName = "?";
            if (winner != null) {
                raceName = winner.getEmpireNameWithArticle();
                raceName = Character.toString(raceName.charAt(0)).toUpperCase() + raceName.substring(1);
            }
            text = String.format("%s\n\n\n%s %d",
                    StringDB.getString("OTHER_REACHED_VICTORY_IN_TYPE", false, raceName, type),
                    StringDB.getString("REACHED_GAMEPOINTS"),
                    gamePoints);
        }

        Rectangle rect = GameConstants.coordsToRelative(0, 810, 1440, 70);
        headLineTable.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        headLineTable.clear();
        headLineTable.add(headLine, "hugeFont", normalColor);

        rect = GameConstants.coordsToRelative(300, 450, 840, 240);
        Label desc = new Label(text, skin, "xlFont", normalColor);
        desc.setWrap(true);
        desc.setAlignment(Align.center);
        desc.setTouchable(Touchable.disabled);
        desc.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        stage.addActor(desc);
    }

    private void updateAchievements() {
        if (winnerRace.equals("MAJOR1"))
            game.getAchievementManager().unlockAchievement(AchievementsList.achievementPeaceToTheGalaxy.getAchievement());
        else if(winnerRace.equals("MAJOR2"))
            game.getAchievementManager().unlockAchievement(AchievementsList.achievementSupremeInc.getAchievement());
        else if(winnerRace.equals("MAJOR3"))
            game.getAchievementManager().unlockAchievement(AchievementsList.achievementTrueWarriors.getAchievement());
        else if(winnerRace.equals("MAJOR4"))
            game.getAchievementManager().unlockAchievement(AchievementsList.achievementWeWatchEverything.getAchievement());
        else if(winnerRace.equals("MAJOR5"))
            game.getAchievementManager().unlockAchievement(AchievementsList.achievementKneelBeforeUs.getAchievement());
        else if(winnerRace.equals("MAJOR6"))
            game.getAchievementManager().unlockAchievement(AchievementsList.achievementAllHaveBeenEliminated.getAchievement());

        switch (game.getGamePreferences().difficulty) {
            case EASY:
                game.getAchievementManager().unlockAchievement(AchievementsList.achievementYouHaveToStartSomewhere.getAchievement());
                break;
            case NORMAL:
                game.getAchievementManager().unlockAchievement(AchievementsList.achievementICanDoThis.getAchievement());
                break;
            case HARD:
                game.getAchievementManager().unlockAchievement(AchievementsList.achievementItsGettingHarder.getAchievement());
                break;
            case VERY_HARD:
                game.getAchievementManager().unlockAchievement(AchievementsList.achievementDoesThisGetAnyHarder.getAchievement());
                break;
            case IMPOSSIBLE:
                game.getAchievementManager().unlockAchievement(AchievementsList.achievementGodlike.getAchievement());
                break;
        }

        switch (victoryType) {
            case VICTORYTYPE_ELIMINATION:
                game.getAchievementManager().unlockAchievement(AchievementsList.achievementVanquisher.getAchievement());
                break;
            case VICTORYTYPE_CONQUEST:
                game.getAchievementManager().unlockAchievement(AchievementsList.achievementConqueror.getAchievement());
                break;
            case VICTORYTYPE_DIPLOMACY:
                game.getAchievementManager().unlockAchievement(AchievementsList.achievementPeacemaker.getAchievement());
                break;
            case VICTORYTYPE_COMBATWINS:
                game.getAchievementManager().unlockAchievement(AchievementsList.achievementGrandAdmiral.getAchievement());
                break;
            case VICTORYTYPE_RESEARCH:
                game.getAchievementManager().unlockAchievement(AchievementsList.achievementTechnomage.getAchievement());
                break;
            case VICTORYTYPE_SABOTAGE:
                game.getAchievementManager().unlockAchievement(AchievementsList.achievementShadow.getAchievement());
                break;            
        }
    }
}
