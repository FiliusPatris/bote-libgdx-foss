/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.events;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.SndMgrValue;

public class EventColonization extends EventScreen {

    public EventColonization(ResourceManager game, String headline, String text) {
        super(game, "Colonization", headline, text, EventScreenType.EVENT_SCREEN_TYPE_COLONIZATION);
    }

    @Override
    public void show() {
        super.show();
        game.getSoundManager().playSound(SndMgrValue.SNDMGR_MSG_CLAIMSYSTEM, playerRace.getPrefix());
        Rectangle rect = GameConstants.coordsToRelative(0, 810, 1440, 70);
        headLineTable.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        headLineTable.clear();
        headLineTable.add(headLine, "hugeFont", normalColor);

        rect = GameConstants.coordsToRelative(45, 722, 1350, 80);
        Label desc = new Label(text, skin, "largeFont", normalColor);
        desc.setWrap(true);
        desc.setAlignment(Align.center);
        desc.setTouchable(Touchable.disabled);
        desc.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        stage.addActor(desc);
    }

}
