/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.events;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.achievements.AchievementsList;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.ResearchType;
import com.blotunga.bote.constants.SndMgrValue;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Research;
import com.blotunga.bote.races.ResearchInfo;
import com.blotunga.bote.ships.ShipInfo;
import com.blotunga.bote.starsystem.BuildingInfo;
import com.blotunga.bote.troops.TroopInfo;
import com.blotunga.bote.utils.Pair;
import com.blotunga.bote.utils.ui.BaseTooltip;

public class EventResearch extends EventScreen {
    private int tech;
    private int techLevel;
    private Array<BuildingInfo> newBuildings;
    private Array<BuildingInfo> newUpgrades;
    private Array<ShipInfo> newShips;
    private Array<TroopInfo> newTroops;
    private Image img;
    private Array<String> loadedTextures;

    public EventResearch(ResourceManager game, String headLine, int tech) {
        super(game, "Research", headLine, "", EventScreenType.EVENT_SCREEN_TYPE_RESEARCH);
        this.tech = tech;
        this.techLevel = 0;
        newBuildings = new Array<BuildingInfo>();
        newUpgrades = new Array<BuildingInfo>();
        newShips = new Array<ShipInfo>();
        newTroops = new Array<TroopInfo>();
        img = new Image();
        img.setTouchable(Touchable.disabled);
        loadedTextures = new Array<String>();
    }

    @Override
    public void show() {
        super.show();
        initResearch();
        game.getSoundManager().playSound(SndMgrValue.SNDMGR_MSG_NEWTECHNOLOGY, playerRace.getPrefix());
        Rectangle rect = GameConstants.coordsToRelative(0, 810, 1440, 40);
        headLineTable.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        headLineTable.clear();
        headLineTable.add(headLine, "hugeFont", normalColor);

        textTable.clear();
        rect = GameConstants.coordsToRelative(0, 765, 1440, 30);
        textTable.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        Pair<String, String> techInfos = ResearchInfo.getTechInfos(tech, techLevel);
        textTable.add(techInfos.getFirst(), "xlFont", listMarkTextColor);

        rect = GameConstants.coordsToRelative(610, 727, 224, 131);
        img.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        stage.addActor(img);

        Label desc = new Label(techInfos.getSecond(), skin, "largeFont", normalColor);
        desc.setWrap(true);
        desc.setAlignment(Align.center);
        desc.setTouchable(Touchable.disabled);
        rect = GameConstants.coordsToRelative(210, 580, 1025, 80);
        desc.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        stage.addActor(desc);

        Label buildingText = new Label(StringDB.getString("RESEARCHEVENT_NEWBUILDINGS"), skin, "mediumFont",
                listMarkTextColor);
        buildingText.setWrap(true);
        buildingText.setAlignment(Align.center);
        rect = GameConstants.coordsToRelative(0, 490, 1440, 25);
        buildingText.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        stage.addActor(buildingText);

        Label upgText = new Label(StringDB.getString("RESEARCHEVENT_NEWUPGRADES"), skin, "mediumFont",
                listMarkTextColor);
        upgText.setWrap(true);
        upgText.setAlignment(Align.center);
        rect = GameConstants.coordsToRelative(0, 340, 1440, 25);
        upgText.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        stage.addActor(upgText);

        Label shipText = new Label(StringDB.getString("RESEARCHEVENT_NEWSHIPS_AND_TROOPS"), skin, "mediumFont",
                listMarkTextColor);
        shipText.setWrap(true);
        shipText.setAlignment(Align.center);
        rect = GameConstants.coordsToRelative(0, 185, 1440, 25);
        shipText.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        stage.addActor(shipText);

        for (int i = 0; i < newBuildings.size; i++) {
            String path = "graphics/buildings/" + newBuildings.get(i).getGraphicFileName() + ".png";
            drawImage(new TextureRegion(game.loadTextureImmediate(path)), newBuildings.get(i).getBuildingName(), i, 467, newBuildings.get(i), null, null);
            loadedTextures.add(path);
        }
        for (int i = 0; i < newUpgrades.size; i++) {
            String path = "graphics/buildings/" + newUpgrades.get(i).getGraphicFileName() + ".png";
            drawImage(new TextureRegion(game.loadTextureImmediate(path)), newUpgrades.get(i).getBuildingName(), i, 317, newUpgrades.get(i), null, null);
            loadedTextures.add(path);
        }
        int count = 0;
        for (int i = 0; i < newShips.size; i++) {
            ShipInfo si = newShips.get(i);
            String path = "graphics/ships/" + si.getShipImageName() + ".png";
            drawImage(new TextureRegion(game.loadTextureImmediate(path)),
                    si.getShipClass() + "-" + StringDB.getString("CLASS") + " (" + si.getShipTypeAsString() + ")", i, 163, null, si, null);
            loadedTextures.add(path);
            count++;
        }
        for (int i = 0; i < newTroops.size; i++) {
            String path = "graphics/troops/" + newTroops.get(i).getGraphicFile() + ".png";
            drawImage(new TextureRegion(game.loadTextureImmediate(path)), newTroops.get(i).getName(), count + i, 163, null, null, newTroops.get(i));
            loadedTextures.add(path);
        }
    }

    private void drawImage(TextureRegion region, String name, int count, int y, BuildingInfo bi, ShipInfo si, TroopInfo ti) {
        Table buildingTable = new Table(skin);
        buildingTable.setTouchable(Touchable.disabled);
        Image buildingImage = new Image();
        if(bi != null)
            bi.getTooltip(BaseTooltip.createTableTooltip(buildingImage, tooltipTexture).getActor(), skin, tooltipHeaderFont, tooltipHeaderColor, tooltipTextFont, tooltipTextColor);
        else if(si != null)
            si.getTooltip(null, BaseTooltip.createTableTooltip(buildingImage, tooltipTexture).getActor(), skin, tooltipHeaderFont, tooltipHeaderColor, tooltipTextFont, tooltipTextColor);
        else if(ti != null)
            ti.getTooltip(playerRace.getEmpire().getResearch().getResearchInfo(), BaseTooltip.createTableTooltip(buildingImage, tooltipTexture).getActor(), skin, tooltipHeaderFont, tooltipHeaderColor, tooltipTextFont, tooltipTextColor);
        if (region != null)
            buildingImage.setDrawable(new TextureRegionDrawable(region));
        Rectangle rect = GameConstants.coordsToRelative(10 + count * 180, y, 160, 95);
        buildingImage.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        stage.addActor(buildingImage);
        buildingTable.add(name, "mediumFont", playerRace.getRaceDesign().clrNormalText);
        rect = GameConstants.coordsToRelative(10 + count * 180, y - 95, 160, 25);
        buildingTable.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        stage.addActor(buildingTable);
    }

    private void initResearch() {
        Research research = playerRace.getEmpire().getResearch();
        ResearchType type = ResearchType.fromType(tech);
        String path = "graphics/research/" + type.getImgName() + ".png";
        img.setDrawable(new TextureRegionDrawable(new TextureRegion(game.loadTextureImmediate(path))));
        loadedTextures.add(path);
        int[] researchLevels = research.getResearchLevels();
        techLevel = researchLevels[tech];
        for (int i = 0; i < game.getBuildingInfos().size; i++) {
            BuildingInfo bi = game.getBuildingInfos().get(i);
            if (bi.getOwnerOfBuilding() == playerRace.getRaceBuildingNumber())
                if (bi.isBuildingBuildableNow(researchLevels)) {
                    int tl = bi.getNeededTechLevel(type);
                    if (tl != -1 && tl == techLevel) {
                        if (bi.getPredecessorId() == 0)
                            newBuildings.add(bi);
                        else
                            newUpgrades.add(bi);
                    }
                }
        }
        for (int i = 0; i < game.getShipInfos().size; i++) {
            ShipInfo si = game.getShipInfos().get(i);
            if (si.getRace() == playerRace.getRaceShipNumber())
                if (si.isThisShipBuildableNow(researchLevels)) {
                    int tl = si.getNeededTechLevel(type);
                    if (tl != -1 && tl == techLevel)
                        newShips.add(si);
                }
        }
        for (int i = 0; i < game.getTroopInfos().size; i++) {
            TroopInfo ti = game.getTroopInfos().get(i);
            if (ti.getOwner().equals(playerRace.getRaceId()))
                if (ti.isThisTroopBuildableNow(researchLevels)) {
                    int tl = ti.getNeededTechlevel(tech);
                    if (tl != -1 && tl == techLevel)
                        newTroops.add(ti);
                }
        }
        switch (type) {
            case BIO:
                if (research.getBioTech() >= 14)
                    game.getAchievementManager().unlockAchievement(AchievementsList.achievementPasteur.getAchievement());
                break;
            case ENERGY:
                if (research.getEnergyTech() >= 14)
                    game.getAchievementManager().unlockAchievement(AchievementsList.achievementTesla.getAchievement());
                break;
            case COMPUTER:
                if (research.getComputerTech() >= 14)
                    game.getAchievementManager().unlockAchievement(AchievementsList.achievementVonNeumann.getAchievement());
                break;
            case CONSTRUCTION:
                if (research.getConstructionTech() >= 14)
                    game.getAchievementManager().unlockAchievement(AchievementsList.achievementBessemer.getAchievement());
                break;
            case PROPULSION:
                if (research.getPropulsionTech() >= 14)
                    game.getAchievementManager().unlockAchievement(AchievementsList.achievementVonBraun.getAchievement());
                break;
            case WEAPON:
                if (research.getWeaponTech() >= 14)
                    game.getAchievementManager().unlockAchievement(AchievementsList.achievementOppenheimer.getAchievement());
                break;
            default:
                break;
        }
    }

    @Override
    public void hide() {
        for (String path : loadedTextures)
            if (game.getAssetManager().isLoaded(path))
                game.getAssetManager().unload(path);
    }
}
