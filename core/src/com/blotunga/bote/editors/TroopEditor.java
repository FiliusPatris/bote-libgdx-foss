/*
 * Copyright (C) 2014-2017 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.editors;

import java.util.Arrays;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.InputEvent.Type;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextArea;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldFilter;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldListener;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.ResearchType;
import com.blotunga.bote.constants.ResourceTypes;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.troops.TroopInfo;

public class TroopEditor {
    /**
     * Helper class for RaceInfo
     */
    class RaceInfo {
        String name;
        String id;

        public RaceInfo(String id, String name) {
            this.id = id;
            this.name = name;
        }

        public String getId() {
            return id;
        }

        @Override
        public String toString() {
            return name;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof RaceInfo)
                return ((RaceInfo) obj).id.equals(id);
            return false;
        }
    }

    private Skin skin;
    private ScreenManager game;
    private Stage stage;
    private ScrollPane troopScroller;
    private Table troopTable;
    private TextButtonStyle styleSmall;
    private TextureRegion selectTexture;
    private Color normalColor = Color.WHITE;
    private Color markColor = Color.CYAN;
    private Color oldColor;
    private Button troopSelection = null;
    private int selectedItem = -1;
    private Table troopInfoTable;
    private Array<Button> troopItems;
    private TroopInfo selectedTroopInfo;
    private String loadedTexture;
    private TextField troopFilter;
    private Table mainButtonTable;
    private boolean visible;
    private String selectedLanguage;
    private ArrayMap<String, Array<TroopInfo>> localeTroopInfos;
    private TroopInfo localeTroopInfo;

    public TroopEditor(ScreenManager manager, Stage stage, Skin skin) {
        this.game = manager;
        this.skin = skin;
        this.stage = stage;

        selectedLanguage = GameConstants.getLocale().getLanguage();
        localeTroopInfos = new ArrayMap<String, Array<TroopInfo>>();
        for (String lang : GameConstants.getSupportedLanguages())
            localeTroopInfos.put(lang, manager.readTroopInfosFromFile("/" + lang));

        troopTable = new Table();
        troopTable.align(Align.top);
        troopScroller = new ScrollPane(troopTable, skin);
        troopScroller.setVariableSizeKnobs(false);
        troopScroller.setScrollingDisabled(true, false);
        stage.addActor(troopScroller);
        Rectangle rect = GameConstants.coordsToRelative(5, 770, 260, 725);
        troopScroller.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        troopTable.align(Align.topLeft);
        troopScroller.setVisible(false);

        troopFilter = new TextField("", skin);
        rect = GameConstants.coordsToRelative(5, 805, 260, 30);
        troopFilter.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        troopFilter.setTextFieldListener(new TextFieldListener() {
            @Override
            public void keyTyped(TextField textField, char c) {
                if (textField.isVisible()) {
                    for (int i = 0; i < game.getTroopInfos().size; i++) {
                        TroopInfo ti = game.getTroopInfos().get(i);
                        if (ti.getName().toLowerCase().contains(textField.getText().toLowerCase())) {
                            selectedItem = i;
                            show();
                            break;
                        }
                    }
                }
            }
        });
        stage.addActor(troopFilter);

        mainButtonTable = new Table();
        mainButtonTable.align(Align.topLeft);
        rect = GameConstants.coordsToRelative(5, 40, 1430, 35);
        mainButtonTable.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        stage.addActor(mainButtonTable);

        styleSmall = skin.get("small-buttons", TextButtonStyle.class);
        loadedTexture = "";
        selectTexture = manager.getUiTexture("listselect");

        troopInfoTable = new Table();
        rect = GameConstants.coordsToRelative(270, 805, 1130, 710);
        stage.addActor(troopInfoTable);
        troopInfoTable.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        troopInfoTable.align(Align.topLeft);
        troopInfoTable.setVisible(false);

        troopItems = new Array<Button>();
        stage.setKeyboardFocus(troopTable);

        visible = false;
    }

    protected void savetroopInfosToFile() {
        String pathPrefix = Gdx.files.getLocalStoragePath();
        FileHandle statHandle = Gdx.files.absolute(pathPrefix + "/data/troops/troopstatlist.txt");
        FileHandle defaultInfoHandle = Gdx.files.absolute(pathPrefix + "/data/troops/troopinfolist.txt");
        int numLangs = GameConstants.getSupportedLanguages().length;
        FileHandle[] langSpecificHandle = new FileHandle[numLangs];
        for (int i = 0; i < numLangs; i++) {
            String lng = GameConstants.getSupportedLanguages()[i];
            if (lng.equals("en"))
                langSpecificHandle[i] = defaultInfoHandle;
            else
                langSpecificHandle[i] = Gdx.files.absolute(pathPrefix + "/data/" + lng + "/troops/troopinfolist.txt");
        }
        String statOut = "";
        String[] infoOut = new String[numLangs];
        Arrays.fill(infoOut, "");
        for (int i = 0; i < game.getTroopInfos().size; i++) {
            TroopInfo ti = game.getTroopInfos().get(i);
            statOut += ti.getOwner() + "\n";
            for (int j = 0; j < numLangs; j++) {
                infoOut[j] += ti.getID() + "\n";
                infoOut[j] += localeTroopInfos.get(GameConstants.getSupportedLanguages()[j]).get(i).getName() + "\n";
                infoOut[j] += localeTroopInfos.get(GameConstants.getSupportedLanguages()[j]).get(i).getDescription() + "\n";
            }
            statOut += ti.getGraphicFile() + ".png" + "\n";
            statOut += ti.getOffense() + "\n";
            statOut += ti.getDefense() + "\n";
            statOut += ti.getMaintenanceCosts() + "\n";
            for (int j = 0; j < ResearchType.UNIQUE.getType();j++) {
                ResearchType rt = ResearchType.fromType(j);
                statOut += ti.getNeededTechlevel(rt.getType()) + "\n";
            }
            for (int j = 0; j < ResourceTypes.DERITIUM.getType(); j++) {
                ResourceTypes rt = ResourceTypes.fromResourceTypes(j);
                statOut += ti.getNeededResource(rt.getType()) + "\n";
            }
            statOut += ti.getNeededIndustry() + "\n";
            statOut += ti.getID() + "\n";
            statOut += ti.getSize() + "\n";
            statOut += ti.getMoralValue() + "\n";
        }
        statHandle.writeString(statOut, false, "ISO-8859-1");
        for (int j = 0; j < numLangs; j++)
            langSpecificHandle[j].writeString(infoOut[j], false,
                    GameConstants.getCharset(GameConstants.getSupportedLanguages()[j]));
    }

    public void show() {
        visible = true;
        show(true, true);
    }

    public void show(boolean resetSelectedtroop, boolean resettroopTable) {
        if (resettroopTable) {
            troopItems.clear();
            troopTable.clear();
            troopTable.addListener(new InputListener() {
                @Override
                public boolean keyDown(InputEvent event, final int keycode) {
                    Button b = troopItems.get(selectedItem);
                    switch (keycode) {
                        case Keys.DOWN:
                            selectedItem++;
                            break;
                        case Keys.UP:
                            selectedItem--;
                            break;
                        case Keys.HOME:
                            selectedItem = 0;
                            break;
                        case Keys.END:
                            selectedItem = troopItems.size - 1;
                            break;
                        case Keys.PAGE_DOWN:
                            selectedItem += troopScroller.getScrollHeight() / b.getHeight();
                            break;
                        case Keys.PAGE_UP:
                            selectedItem -= troopScroller.getScrollHeight() / b.getHeight();
                            break;
                    }

                    if (TroopEditor.this.isVisible()) {
                        if (selectedItem >= troopItems.size)
                            selectedItem = troopItems.size - 1;
                        if (selectedItem < 0)
                            selectedItem = 0;

                        b = troopItems.get(selectedItem);
                        marktroopListSelected(b);

                        Gdx.app.postRunnable(new Runnable() {
                            @Override
                            public void run() {
                                show();
                            }
                        });
                        Thread th = new Thread() {
                            @Override
                            public void run() {
                                try {
                                    Thread.sleep(150);
                                } catch (InterruptedException e) {
                                }
                                if (Gdx.input.isKeyPressed(keycode)) {
                                    Gdx.app.postRunnable(new Runnable() {
                                        @Override
                                        public void run() {
                                            InputEvent event = new InputEvent();
                                            event.setType(Type.keyDown);
                                            event.setKeyCode(keycode);
                                            troopTable.fire(event);
                                        }
                                    });
                                }
                            }
                        };
                        th.start();
                    }

                    return false;
                }
            });

            for (int i = 0; i < localeTroopInfos.get(selectedLanguage).size; i++) {
                TroopInfo lti = localeTroopInfos.get(selectedLanguage).get(i);
                TroopInfo ti = game.getTroopInfos().get(i);
                Button.ButtonStyle bs = new Button.ButtonStyle();
                Button button = new Button(bs);
                Color color = normalColor;
                if (!ti.getOwner().isEmpty()) {
                    Major major = game.getRaceController().getMajors().get(ti.getOwner());
                    color = major.getRaceDesign().clrSector;
                }
                Label l = new Label(String.format("%03d", lti.getID()) + ": " + lti.getName(), skin,
                        "mediumFont", Color.WHITE);
                l.setColor(color);
                l.setUserObject(new Integer(i));
                l.setEllipsis(true);
                button.add(l).width(troopScroller.getWidth());
                button.setUserObject(l);
                ActorGestureListener gestureListener = new ActorGestureListener() {
                    @Override
                    public void tap(InputEvent event, float x, float y, int count, int button) {
                        Button b = (Button) event.getListenerActor();
                        marktroopListSelected(b);
                        selectedItem = getIndex(b);
                        show(true, false);
                        stage.setKeyboardFocus(troopTable);
                    }
                };
                button.addListener(gestureListener);

                if (lti.getName().toLowerCase().contains(troopFilter.getText().toLowerCase())) {
                    troopTable.add(button).align(Align.left);
                    troopItems.add(button);
                    troopTable.row();
                }
            }
        }
        troopScroller.setVisible(true);
        troopFilter.setVisible(true);
        drawMainButtonTable();

        Button btn = null;
        if (selectedItem != -1) {
            for (Button b : troopItems) {
                if (selectedItem == getIndex(b)) {
                    btn = b;
                }
            }
        }
        if (btn == null) {
            if (troopItems.size > 0) {
                btn = troopItems.get(0);
                selectedItem = getIndex(btn);
            }
        }

        showtroopInfo(resetSelectedtroop);
        stage.draw();

        if (btn != null)
            marktroopListSelected(btn);
    }

    private void showtroopInfo(boolean resetSelectedtroop) {
        if (selectedItem != -1) {
            if (resetSelectedtroop) {
                selectedTroopInfo = new TroopInfo(game.getTroopInfos().get(selectedItem));
                localeTroopInfo = new TroopInfo(localeTroopInfos.get(selectedLanguage).get(selectedItem));
            }
            TroopInfo ti = selectedTroopInfo;
            drawTroopInfo(ti);
            troopInfoTable.setVisible(true);
        }
    }

    private void drawTroopInfo(TroopInfo ti) {
        TextFieldFilter numberFilter = new TextFieldFilter() {
            @Override
            public boolean acceptChar(TextField textField, char c) {
                return Character.isDigit(c) || c == '-';
            }
        };
        troopInfoTable.clear();
        Table table = new Table();
        Table container = new Table();
        TextField tf = new TextField(localeTroopInfo.getName(), skin);
        tf.setName("TROOP_NAME");
        container.add(tf).width(GameConstants.wToRelative(200));
        container.row();
        loadedTexture = "graphics/troops/" + ti.getGraphicFile() + ".png";
        Image image = new Image(game.loadTextureImmediate(loadedTexture));
        image.addListener(new ActorGestureListener() {
            public void tap(InputEvent event, float x, float y, int count, int button) {
                if (count > 1) {
                    final TextField tf = new TextField(selectedTroopInfo.getGraphicFile(), skin);
                    Dialog dialog = new Dialog("Change troop image:", skin) {
                        protected void result(Object object) {
                            if ((Boolean) object) {
                                selectedTroopInfo.setGraphicFile(tf.getText());
                                TroopEditor.this.show(false, false);
                            }
                        }
                    }.text("New image name: ").button("Ok", true).button("Cancel", false).key(Keys.ENTER, true)
                            .key(Keys.ESCAPE, false).show(stage);
                    dialog.getContentTable().padTop(GameConstants.hToRelative(10));
                    dialog.getContentTable().add(tf).spaceTop(GameConstants.hToRelative(10))
                            .width(GameConstants.wToRelative(220));
                    dialog.getButtonTable().padTop(GameConstants.hToRelative(10));
                    dialog.getButtonTable().padBottom(GameConstants.hToRelative(10));
                    dialog.key(Keys.ENTER, true).key(Keys.ESCAPE, false);
                    dialog.invalidateHierarchy();
                    dialog.invalidate();
                    dialog.layout();
                    dialog.show(stage);
                }
            }
        });
        container.add(image).width(GameConstants.wToRelative(140)).height(GameConstants.hToRelative(80))
                .spaceTop(GameConstants.hToRelative(5));
        table.add(container).width(GameConstants.wToRelative(200)).height(GameConstants.hToRelative(110));

        tf = new TextArea(localeTroopInfo.getDescription(), skin);
        tf.setName("DESCRIPTION");
        table.add(tf).width(GameConstants.wToRelative(920)).height(GameConstants.hToRelative(110))
                .spaceLeft(GameConstants.wToRelative(10));
        troopInfoTable.add(table).spaceBottom(GameConstants.hToRelative(5));
        troopInfoTable.row();

        //second row
        table = new Table();
        Label l = new Label("ID: ", skin, "mediumFont", normalColor);
        table.add(l).width(GameConstants.wToRelative(35));

        tf = new TextField(ti.getID() + "", skin);
        tf.setName("TROOP_ID");
        tf.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
        tf.setMaxLength(4);
        tf.setTextFieldListener(new TextFieldListener() {
            @Override
            public void keyTyped(TextField textField, char c) {
                if (!textField.getText().isEmpty()) {
                    int id = Integer.parseInt(textField.getText());
                    if (id > game.getTroopInfos().size)
                        textField.setText((game.getTroopInfos().size) + "");
                    if (id < 0)
                        textField.setText(0 + "");
                }
            }
        });
        table.add(tf).width(GameConstants.wToRelative(100)).spaceRight(GameConstants.wToRelative(30));

        l = new Label("Race: ", skin, "mediumFont", normalColor);
        table.add(l).width(GameConstants.wToRelative(60));
        Array<RaceInfo> races = new Array<TroopEditor.RaceInfo>();
        ArrayMap<String, Major> majors = game.getRaceController().getMajors();
        for (int i = 0; i < majors.size; i++) {
            Major major = majors.getValueAt(i);
            races.add(new RaceInfo(major.getRaceId(), major.getName()));
        }
        SelectBox<RaceInfo> troopRaceSelect = new SelectBox<RaceInfo>(skin);
        troopRaceSelect.setItems(races);
        troopRaceSelect.setSelected(races.get(races.indexOf(
                new RaceInfo(ti.getOwner(), majors.get(ti.getOwner()).getName()), false)));
        troopRaceSelect.setName("TROOP_RACE");

        table.add(troopRaceSelect).height(GameConstants.hToRelative(30)).width(GameConstants.wToRelative(180))
                .spaceRight(GameConstants.wToRelative(30));
        troopInfoTable.add(table).spaceBottom(GameConstants.hToRelative(5));
        troopInfoTable.row();

        table = new Table();
        l = new Label("Properties", skin, "normalFont", normalColor);
        l.setAlignment(Align.center);
        troopInfoTable.add(l).width(troopInfoTable.getWidth());
        troopInfoTable.row().spaceBottom(GameConstants.hToRelative(15)).spaceTop(GameConstants.hToRelative(5));

        table = new Table();
        l = new Label("Size: ", skin, "mediumFont", normalColor);
        l.setEllipsis(true);
        table.add(l).width(GameConstants.wToRelative(50));
        tf = new TextField(ti.getSize() + "", skin);
        tf.setName("TROOP_SIZE");
        tf.setTextFieldFilter(numberFilter);
        tf.setMaxLength(5);
        table.add(tf).width(GameConstants.wToRelative(60)).spaceRight(GameConstants.wToRelative(20));

        l = new Label("Offense: ", skin, "mediumFont", normalColor);
        l.setEllipsis(true);
        table.add(l).width(GameConstants.wToRelative(80));
        tf = new TextField(ti.getOffense() + "", skin);
        tf.setName("TROOP_OFFENSE");
        tf.setTextFieldFilter(numberFilter);
        tf.setMaxLength(5);
        table.add(tf).width(GameConstants.wToRelative(60)).spaceRight(GameConstants.wToRelative(20));

        l = new Label("Defense: ", skin, "mediumFont", normalColor);
        l.setEllipsis(true);
        table.add(l).width(GameConstants.wToRelative(85));
        tf = new TextField(ti.getDefense() + "", skin);
        tf.setName("TROOP_DEFENSE");
        tf.setTextFieldFilter(numberFilter);
        tf.setMaxLength(5);
        table.add(tf).width(GameConstants.wToRelative(60)).spaceRight(GameConstants.wToRelative(20));

        l = new Label("Maintainance: ", skin, "mediumFont", normalColor);
        l.setEllipsis(true);
        table.add(l).width(GameConstants.wToRelative(130));
        tf = new TextField(ti.getMaintenanceCosts() + "", skin);
        tf.setName("TROOP_MAINTAINANCE");
        tf.setTextFieldFilter(numberFilter);
        tf.setMaxLength(5);
        table.add(tf).width(GameConstants.wToRelative(60)).spaceRight(GameConstants.wToRelative(20));

        l = new Label("Morale effect: ", skin, "mediumFont", normalColor);
        l.setEllipsis(true);
        table.add(l).width(GameConstants.wToRelative(130));
        tf = new TextField(ti.getMoralValue() + "", skin);
        tf.setName("TROOP_MORALE");
        tf.setTextFieldFilter(numberFilter);
        tf.setMaxLength(5);
        table.add(tf).width(GameConstants.wToRelative(60)).spaceRight(GameConstants.wToRelative(20));

        troopInfoTable.add(table).spaceBottom(GameConstants.hToRelative(5));
        troopInfoTable.row();

        table = new Table();
        l = new Label("Needed resources", skin, "normalFont", normalColor);
        l.setAlignment(Align.center);
        troopInfoTable.add(l).width(troopInfoTable.getWidth());
        troopInfoTable.row().spaceBottom(GameConstants.hToRelative(15)).spaceTop(GameConstants.hToRelative(5));

        l = new Label("Industry: ", skin, "mediumFont", normalColor);
        l.setEllipsis(true);
        table.add(l).width(GameConstants.wToRelative(120));
        tf = new TextField(ti.getNeededIndustry() + "", skin);
        tf.setName("NEEDED_INDUSTRY");
        tf.setTextFieldFilter(numberFilter);
        tf.setMaxLength(5);
        table.add(tf).width(GameConstants.wToRelative(60)).spaceRight(GameConstants.wToRelative(10));

        for (int i = 0; i < ResourceTypes.DERITIUM.getType(); i++) {
            ResourceTypes rt = ResourceTypes.fromResourceTypes(i);
            l = new Label(StringDB.getString(rt.getName()) + ": ", skin, "mediumFont", normalColor);
            l.setEllipsis(true);
            table.add(l).width(GameConstants.wToRelative(120));
            tf = new TextField(ti.getNeededResource(rt.getType()) + "", skin);
            tf.setName("NEEDED_" + rt.getName());
            tf.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
            tf.setMaxLength(5);
            table.add(tf).width(GameConstants.wToRelative(60)).spaceRight(GameConstants.wToRelative(10));
        }

        troopInfoTable.add(table).spaceBottom(GameConstants.hToRelative(5));
        troopInfoTable.row();

        table = new Table();
        l = new Label("Needed research", skin, "normalFont", normalColor);
        l.setAlignment(Align.center);
        troopInfoTable.add(l).width(troopInfoTable.getWidth());
        troopInfoTable.row().spaceBottom(GameConstants.hToRelative(15)).spaceTop(GameConstants.hToRelative(5));

        table = new Table();
        for (int i = 0; i < ResearchType.UNIQUE.getType(); i++) {
            ResearchType rt = ResearchType.fromIdx(i);
            l = new Label(StringDB.getString(rt.getShortName()) + ": ", skin, "mediumFont", normalColor);
            table.add(l).width(GameConstants.wToRelative(140));
            tf = new TextField(ti.getNeededTechlevel(rt.getType()) + "", skin);
            tf.setName("NEEDED_" + rt.getKey());
            tf.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
            tf.setMaxLength(3);
            table.add(tf).width(GameConstants.wToRelative(40)).spaceRight(GameConstants.wToRelative(10));
        }
        troopInfoTable.add(table).spaceBottom(GameConstants.hToRelative(5));
        troopInfoTable.row();
    }

    private void savetroopInfo() {
        TroopInfo ti = selectedTroopInfo;
        ti.setID(parseIntDefault(getTextFieldValue("TROOP_ID")));
        localeTroopInfo.setName(getTextFieldValue("TROOP_NAME"));
        localeTroopInfo.setDescription(getTextFieldValue("DESCRIPTION"));
        ti.setOwner(((RaceInfo) getSelectBoxValue("TROOP_RACE")).getId());
        ti.setSize(parseIntDefault(getTextFieldValue("TROOP_SIZE")));
        ti.setOffense(parseIntDefault(getTextFieldValue("TROOP_OFFENSE")));
        ti.setDefense(parseIntDefault(getTextFieldValue("TROOP_DEFENSE")));
        ti.setMaintainance(parseIntDefault(getTextFieldValue("TROOP_MAINTAINANCE")));
        ti.setMorale(parseIntDefault(getTextFieldValue("TROOP_MORALE")));
        ti.setNeededIndustry(parseIntDefault(getTextFieldValue("NEEDED_INDUSTRY")));
        for (int i = 0; i < ResourceTypes.DERITIUM.getType(); i++) {
            ResourceTypes rt = ResourceTypes.fromResourceTypes(i);
            ti.setNeededResource(rt.getType(), parseIntDefault(getTextFieldValue("NEEDED_" + rt.getName())));
        }
        for (int i = 0; i < ResearchType.UNIQUE.getType(); i++) {
            ResearchType rt = ResearchType.fromIdx(i);
            ti.setNeededTechLevel(rt.getType(), parseIntDefault(getTextFieldValue("NEEDED_" + rt.getKey())));
        }
    }

    private String getTextFieldValue(String name) {
        return ((TextField) troopInfoTable.findActor(name)).getText();
    }

    @SuppressWarnings("rawtypes")
    private Object getSelectBoxValue(String name) {
        return ((SelectBox) troopInfoTable.findActor(name)).getSelected();
    }

    @SuppressWarnings("unused")
    private boolean getCheckBoxValue(String name) {
        return ((CheckBox) troopInfoTable.findActor(name)).isChecked();
    }

    private int parseIntDefault(String text) {
        return parseIntDefault(text, 0);
    }

    private int parseIntDefault(String text, int def) {
        try {
            return Integer.parseInt(text);
        } catch (NumberFormatException e) {
            return def;
        }
    }

    @SuppressWarnings("unused")
    private int getIdFromStringWithName(String val) {
        int res = 0;
        if (!val.isEmpty())
            res = Integer.parseInt(val.split(": ")[0]);
        return res;
    }

    public void hide() {
        visible = false;
        troopFilter.setVisible(false);
        troopScroller.setVisible(false);
        mainButtonTable.setVisible(false);
        troopInfoTable.setVisible(false);
    }

    private void marktroopListSelected(Button b) {
        if (troopSelection != null) {
            troopSelection.getStyle().up = null;
            troopSelection.getStyle().down = null;
            ((Label) troopSelection.getUserObject()).setColor(oldColor);
            if (!loadedTexture.isEmpty() && game.getAssetManager().isLoaded(loadedTexture) && selectedItem != getIndex(b)) {
                game.getAssetManager().unload(loadedTexture);
                loadedTexture = "";
            }
        }
        if (b == null)
            b = troopSelection;
        Image itemSelection = new Image(selectTexture);
        b.getStyle().up = itemSelection.getDrawable();
        b.getStyle().down = itemSelection.getDrawable();
        oldColor = new Color(((Label) b.getUserObject()).getColor());
        ((Label) b.getUserObject()).setColor(markColor);
        troopSelection = b;
        selectedItem = getIndex(b);
        float buttonPos = b.getHeight() * selectedItem;
        float scrollerHeight = troopScroller.getScrollHeight();
        float scrollerPos = troopScroller.getScrollY();
        if (buttonPos + b.getHeight() / 2 - scrollerPos < 0) {
            troopScroller.setScrollY(b.getHeight() * selectedItem - troopScroller.getScrollHeight() * 2);
        } else if (buttonPos + b.getHeight() / 2 - scrollerPos > scrollerHeight) {
            troopScroller.setScrollY(b.getHeight() * (selectedItem - troopScroller.getScrollHeight() / b.getHeight() + 1));
        }
    }

    private int getIndex(Button b) {
        return (Integer) (((Label) b.getUserObject()).getUserObject());
    }

    private void updateTroopInfos(Array<TroopInfo> infos) {
        for (int i = 0; i < infos.size; i++) {
            TroopInfo ti = infos.get(i);
            ti.setID(i);
        }
    }

    private void drawMainButtonTable() {
        mainButtonTable.clear();
        TextButton addBtn = new TextButton("Add", styleSmall);
        addBtn.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                TroopInfo ti = new TroopInfo();
                ti.setID(game.getTroopInfos().size);
                ti.setOwner("MAJOR1");
                game.getTroopInfos().add(ti);
                for (String lang : GameConstants.getSupportedLanguages())
                    localeTroopInfos.get(lang).add(ti);
                selectedItem = game.getTroopInfos().size - 1;
                show();
            }
        });
        mainButtonTable.add(addBtn).width(GameConstants.wToRelative(150)).height(GameConstants.hToRelative(35))
                .spaceRight(GameConstants.wToRelative(5));

        TextButton copyBtn = new TextButton("Copy", styleSmall);
        copyBtn.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                TroopInfo ti = new TroopInfo(game.getTroopInfos().get(selectedItem));
                ti.setID(game.getTroopInfos().size);
                game.getTroopInfos().add(ti);
                for (String lang : GameConstants.getSupportedLanguages()) {
                    TroopInfo localTi = new TroopInfo(localeTroopInfos.get(lang).get(selectedItem));
                    localTi.setID(ti.getID());
                    localeTroopInfos.get(lang).add(localTi);
                }
                selectedItem = game.getTroopInfos().size - 1;
                show();
            }
        });
        mainButtonTable.add(copyBtn).width(GameConstants.wToRelative(150)).height(GameConstants.hToRelative(35))
                .spaceRight(GameConstants.wToRelative(5));

        TextButton removeBtn = new TextButton("Remove", styleSmall);
        removeBtn.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                game.getTroopInfos().removeIndex(selectedItem);
                for (String lang : GameConstants.getSupportedLanguages()) {
                    localeTroopInfos.get(lang).removeIndex(selectedItem);
                }
                for (String lang : GameConstants.getSupportedLanguages()) {
                    updateTroopInfos(localeTroopInfos.get(lang));
                }
                updateTroopInfos(game.getTroopInfos());
                show();
            }
        });
        mainButtonTable.add(removeBtn).width(GameConstants.wToRelative(150)).height(GameConstants.hToRelative(35))
                .spaceRight(GameConstants.wToRelative(5));

        TextButton buttonUpdate = new TextButton("Update", styleSmall);
        buttonUpdate.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                savetroopInfo();
                int newIdx = selectedTroopInfo.getID();
                game.getTroopInfos().removeIndex(selectedItem);
                TroopInfo ti = new TroopInfo(selectedTroopInfo);
                ti.setID(selectedItem);
                game.getTroopInfos().insert(newIdx != selectedItem ? newIdx : selectedItem, ti);
                localeTroopInfos.get(selectedLanguage).removeIndex(selectedItem);
                ti = new TroopInfo(localeTroopInfo);
                ti.setID(selectedItem);
                localeTroopInfos.get(selectedLanguage).insert(newIdx != selectedItem ? newIdx : selectedItem, ti);
                if (newIdx != selectedItem) {
                    for (String lang : GameConstants.getSupportedLanguages()) {
                        if(!lang.equals(selectedLanguage)) {
                            ti = localeTroopInfos.get(lang).removeIndex(selectedItem);
                            ti.setID(selectedItem );
                            localeTroopInfos.get(lang).insert(newIdx != selectedItem ? newIdx : selectedItem, ti);
                        }
                    }

                    for (String lang : GameConstants.getSupportedLanguages())
                            updateTroopInfos(localeTroopInfos.get(lang));
                    updateTroopInfos(game.getTroopInfos());
                }
                show();
            }
        });
        mainButtonTable.add(buttonUpdate).width(GameConstants.wToRelative(150)).height(GameConstants.hToRelative(35))
                .spaceRight(GameConstants.wToRelative(5));

        TextButton buttonSave = new TextButton("Save", styleSmall);
        buttonSave.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                savetroopInfosToFile();
            }
        });
        mainButtonTable.add(buttonSave).width(GameConstants.wToRelative(150)).height(GameConstants.hToRelative(35))
                .spaceRight(GameConstants.wToRelative(5));

        SelectBox<String> languageSelect = new SelectBox<String>(skin);
        languageSelect.setItems(GameConstants.getSupportedLanguages());
        languageSelect.setSelected(selectedLanguage);
        languageSelect.addListener(new ChangeListener() {
            @SuppressWarnings("unchecked")
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                selectedLanguage = ((SelectBox<String>) actor).getSelected();
                show();
            }
        });
        mainButtonTable.add(languageSelect).width(GameConstants.wToRelative(100)).height(GameConstants.hToRelative(35))
                .spaceRight(GameConstants.wToRelative(5));

        TextButton buttonBack = new TextButton(StringDB.getString("BTN_BACK", true), styleSmall);
        buttonBack.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                ((ResourceEditor) game.getScreen()).showMainMenu();
            }
        });
        mainButtonTable.add(buttonBack).width(GameConstants.wToRelative(150)).height(GameConstants.hToRelative(35))
                .spaceLeft(GameConstants.wToRelative(5)).align(Align.right).expand();
        mainButtonTable.setVisible(true);
    }

    public boolean isVisible() {
        return visible;
    }
}