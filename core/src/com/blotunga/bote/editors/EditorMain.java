/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.editors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.blotunga.bote.GamePreferences;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.general.StringDB;

public class EditorMain {
    private final Array<TextButton> buttonArray;
    private Skin skin;
    private ScreenManager game;
    private Table table;

    public EditorMain(ScreenManager manager, Stage stage, Skin skin) {
        this.game = manager;
        this.skin = skin;
        buttonArray = new Array<TextButton>();
        table = new Table();
        table.align(Align.top);
        Rectangle rect = GameConstants.coordsToRelative(0, 665, 1440, 665);
        table.setBounds(rect.x, rect.y, rect.width, rect.height);
        stage.addActor(table);
    }

    public void show() {
        table.clear();
        int cnt = EditorMainButtonType.values().length;
        for (int i = 0; i < cnt; i++) {
            EditorMainButtonType bt = EditorMainButtonType.values()[i];
            TextButton tb = new TextButton(StringDB.getString(bt.getName(), true), skin);

            tb.setUserObject(EditorMainButtonType.values()[i]);
            tb.addListener(new ActorGestureListener() {
                @Override
                public void tap(InputEvent event, float x, float y, int count, int button) {
                    EditorMainButtonType type = (EditorMainButtonType) event.getListenerActor().getUserObject();
                    switch (type) {
                        case BTN_EDIT_SHIPS:
                            ((ResourceEditor) game.getScreen()).showShipEditor();
                            break;
                        case BTN_EDIT_BUILDINGS:
                            ((ResourceEditor) game.getScreen()).showBuildingEditor();
                            break;
                        case BTN_EDIT_TROOPS:
                            ((ResourceEditor) game.getScreen()).showTroopEditor();
                            break;
                        case BTN_EDIT_MINOR_RACES:
                            ((ResourceEditor) game.getScreen()).showMinorRaceEditor();
                            break;
                        case BTN_EXIT:
                            Gdx.app.exit();
                            break;
                        default:
                            break;
                    }
                    //TODO:
                }
            });

            buttonArray.add(tb);
            table.add(tb).width(GamePreferences.sceneWidth / 4.3f).height(GamePreferences.sceneHeight / (cnt * 2 * 1.5f))
                    .pad(GamePreferences.sceneHeight / (cnt * 4.5f));
            table.row();
        }
        table.setVisible(true);
    }

    public void hide() {
        table.setVisible(false);
    }
}