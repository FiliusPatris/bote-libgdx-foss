/*
 * Copyright (C) 2014-2017 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.editors;

import java.util.Arrays;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.InputEvent.Type;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextArea;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldFilter;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldListener;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.PlanetType;
import com.blotunga.bote.constants.PlayerRaces;
import com.blotunga.bote.constants.ResearchType;
import com.blotunga.bote.constants.ResourceTypes;
import com.blotunga.bote.constants.ShipSize;
import com.blotunga.bote.constants.WorkerType;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.starsystem.BuildingInfo;

public class BuildingEditor {
    /**
     * Helper class for RaceInfo
     */
    class RaceInfo {
        String name;
        int id;

        public RaceInfo(int id, String name) {
            this.id = id;
            this.name = name;
        }

        public int getId() {
            return id;
        }

        @Override
        public String toString() {
            return name;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof RaceInfo)
                return ((RaceInfo) obj).id == id;
            return false;
        }
    }

    enum BuildingEditorState {
        StateGeneral,
        StateProduction
    }

    private Skin skin;
    private ScreenManager game;
    private Stage stage;
    private ScrollPane buildingScroller;
    private Table buildingTable;
    private TextButtonStyle styleSmall;
    private TextureRegion selectTexture;
    private Color normalColor = Color.WHITE;
    private Color markColor = Color.CYAN;
    private Color oldColor;
    private Button buildingSelection = null;
    private int selectedItem = -1;
    private Table buildingInfoTable;
    private Table buildingProductionTable;
    private Array<Button> buildingItems;
    private BuildingInfo selectedBuildingInfo;
    private String loadedTexture;
    private TextField buildingFilter;
    private Table mainButtonTable;
    private Table selectButtonTable;
    private boolean visible;
    private BuildingEditorState state;
    private String selectedLanguage;
    private ArrayMap<String, Array<BuildingInfo>> localeBuildingInfos;
    private BuildingInfo localeBuildingInfo;

    public BuildingEditor(ScreenManager manager, Stage stage, Skin skin) {
        this.game = manager;
        this.skin = skin;
        this.stage = stage;

        selectedLanguage = GameConstants.getLocale().getLanguage();
        localeBuildingInfos = new ArrayMap<String, Array<BuildingInfo>>();
        for (String lang : GameConstants.getSupportedLanguages())
            localeBuildingInfos.put(lang, manager.readBuildingInfosFromFile("/" + lang));

        buildingTable = new Table();
        buildingTable.align(Align.top);
        buildingScroller = new ScrollPane(buildingTable, skin);
        buildingScroller.setVariableSizeKnobs(false);
        buildingScroller.setScrollingDisabled(true, false);
        stage.addActor(buildingScroller);
        Rectangle rect = GameConstants.coordsToRelative(5, 770, 260, 725);
        buildingScroller.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        buildingTable.align(Align.topLeft);
        buildingScroller.setVisible(false);

        buildingFilter = new TextField("", skin);
        rect = GameConstants.coordsToRelative(5, 805, 260, 30);
        buildingFilter.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        buildingFilter.setTextFieldListener(new TextFieldListener() {
            @Override
            public void keyTyped(TextField textField, char c) {
                if (textField.isVisible()) {
                    for (int i = 0; i < game.getBuildingInfos().size; i++) {
                        BuildingInfo bi = game.getBuildingInfos().get(i);
                        if (bi.getBuildingName().toLowerCase().contains(textField.getText().toLowerCase())) {
                            selectedItem = i;
                            show();
                            break;
                        }
                    }
                }
            }
        });
        stage.addActor(buildingFilter);

        mainButtonTable = new Table();
        mainButtonTable.align(Align.topLeft);
        rect = GameConstants.coordsToRelative(5, 40, 1430, 35);
        mainButtonTable.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        stage.addActor(mainButtonTable);

        styleSmall = skin.get("small-buttons", TextButtonStyle.class);
        loadedTexture = "";
        selectTexture = manager.getUiTexture("listselect");

        buildingInfoTable = new Table();
        rect = GameConstants.coordsToRelative(270, 805, 1130, 710);
        stage.addActor(buildingInfoTable);
        buildingInfoTable.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        buildingInfoTable.align(Align.topLeft);
        buildingInfoTable.setVisible(false);

        buildingProductionTable = new Table();
        rect = GameConstants.coordsToRelative(270, 805, 1130, 710);
        stage.addActor(buildingProductionTable);
        buildingProductionTable.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        buildingProductionTable.align(Align.topLeft);
        buildingProductionTable.setVisible(false);

        selectButtonTable = new Table();
        rect = GameConstants.coordsToRelative(270, 90, 1130, 35);
        stage.addActor(selectButtonTable);
        selectButtonTable.setBounds((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
        selectButtonTable.align(Align.topLeft);

        buildingItems = new Array<Button>();
        stage.setKeyboardFocus(buildingTable);


        visible = false;
        state = BuildingEditorState.StateGeneral;
    }

    protected void saveBuildingInfosToFile() {
        String pathPrefix = Gdx.files.getLocalStoragePath();
        FileHandle statHandle = Gdx.files.absolute(pathPrefix + "/data/buildings/buildingstatlist.txt");
        FileHandle defaultInfoHandle = Gdx.files.absolute(pathPrefix + "/data/buildings/buildinginfolist.txt");
        int numLangs = GameConstants.getSupportedLanguages().length;
        FileHandle[] langSpecificHandle = new FileHandle[numLangs];
        for (int i = 0; i < numLangs; i++) {
            String lng = GameConstants.getSupportedLanguages()[i];
            if (lng.equals("en"))
                langSpecificHandle[i] = defaultInfoHandle;
            else
                langSpecificHandle[i] = Gdx.files.absolute(pathPrefix + "/data/" + lng + "/buildings/buildinginfolist.txt");
        }
        String statOut = "";
        String[] infoOut = new String[numLangs];
        Arrays.fill(infoOut, "");
        for (int i = 0; i < game.getBuildingInfos().size; i++) {
            BuildingInfo bi = game.getBuildingInfos().get(i);
            for (int j = 0; j < numLangs; j++)
                infoOut[j] += bi.getRunningNumber() + "\n";
            statOut += bi.getRunningNumber() + "\n";
            statOut += bi.getOwnerOfBuilding() + "\n";
            for (int j = 0; j < numLangs; j++) {
                infoOut[j] += localeBuildingInfos.get(GameConstants.getSupportedLanguages()[j]).get(i).getBuildingName() + "\n";
                infoOut[j] += localeBuildingInfos.get(GameConstants.getSupportedLanguages()[j]).get(i).getBuildingDescription() + "\n";
            }
            statOut += (bi.isUpgradeable() ? 1 : 0) + "\n";
            statOut += bi.getGraphicFileName() + ".png" + "\n";
            statOut += bi.getMaxInSystem().number + "\n";
            statOut += bi.getMaxInSystem().runningNumber + "\n";
            statOut += bi.getMaxInEmpire() + "\n";
            statOut += bi.getMaxInEmpireID() + "\n";
            statOut += (bi.isOnlyHomePlanet() ? 1 : 0) + "\n";
            statOut += (bi.isOnlyOwnColony() ? 1 : 0) + "\n";
            statOut += (bi.isOnlyMinorRace() ? 1 : 0) + "\n";
            statOut += (bi.isOnlyTakenSystem() ? 1 : 0) + "\n";
            statOut += bi.getOnlyInSystemWithName() + "\n";
            statOut += bi.getMinInhabitants() + "\n";
            statOut += bi.getMinInSystem().number + "\n";
            statOut += bi.getMinInSystem().runningNumber + "\n";
            statOut += bi.getMinInEmpire().number + "\n";
            statOut += bi.getMinInEmpire().runningNumber + "\n";
            statOut += (bi.isOnlyRace() ? 1 : 0) + "\n";
            for (int j = 1; j <= PlanetType.PLANETCLASS_Y.getType(); j <<= 1) {
                PlanetType pt = PlanetType.fromPlanetType(j);
                statOut += (bi.getPlanetTypes(pt.getType()) ? 1 : 0) + "\n";
            }
            for (int j = 0; j < ResearchType.UNIQUE.getType();j++) {
                ResearchType rt = ResearchType.fromType(j);
                statOut += bi.getNeededTechLevel(rt) + "\n";
            }
            statOut += bi.getNeededIndustry() + "\n";
            statOut += bi.getNeededEnergy() + "\n";
            for (int j = 0; j < ResourceTypes.FOOD.getType(); j++) {
                ResourceTypes rt = ResourceTypes.fromResourceTypes(j);
                statOut += bi.getNeededResource(rt.getType()) + "\n";
            }
            for (int j = 0; j < WorkerType.ALL_WORKER.getType(); j++) {
                WorkerType wk = WorkerType.fromWorkerType(j);
                statOut += bi.getProductionByWorkers(wk) + "\n";
            }
            statOut += bi.getDeritiumProd() + "\n";
            statOut += bi.getCredits() + "\n";
            statOut += bi.getMoraleProd() + "\n";
            statOut += bi.getMoraleProdEmpire() + "\n";
            for (int j = 0; j < WorkerType.ALL_WORKER.getType(); j++) {
                WorkerType wk = WorkerType.fromWorkerType(j);
                statOut += bi.getProductionBonusByWorkers(wk) + "\n";
            }
            statOut += bi.getDeritiumBonus() + "\n";
            statOut += bi.getAllResourceBonus() + "\n";
            statOut += bi.getCreditsBonus() + "\n";
            for (int j = 0; j < ResearchType.UNIQUE.getType(); j++) {
                ResearchType rt = ResearchType.fromType(j);
                statOut += bi.getTechBonus(rt) + "\n";
            }
            statOut += bi.getInnerSecurityBonus() + "\n";
            statOut += bi.getEconomySpyBonus() + "\n";
            statOut += bi.getEconomySabotageBonus() + "\n";
            statOut += bi.getResearchSpyBonus() + "\n";
            statOut += bi.getResearchSabotageBonus() + "\n";
            statOut += bi.getMilitarySpyBonus() + "\n";
            statOut += bi.getMilitarySabogateBonus() + "\n";
            statOut += (bi.isShipYard() ? 1 : 0) + "\n";
            statOut += bi.getMaxBuildableShipSize().getSize() + "\n";
            statOut += bi.getShipYardSpeed() + "\n";
            statOut += (bi.isBarrack() ? 1 : 0) + "\n";
            statOut += bi.getBarrackSpeed() + "\n";
            statOut += bi.getHitPoints() + "\n";
            statOut += bi.getShieldPower() + "\n";
            statOut += bi.getShieldPowerBonus() + "\n";
            statOut += bi.getShipDefend() + "\n";
            statOut += bi.getShipDefendBonus() + "\n";
            statOut += bi.getGroundDefend() + "\n";
            statOut += bi.getGroundDefendBonus() + "\n";
            statOut += bi.getScanPower() + "\n";
            statOut += bi.getScanPowerBonus() + "\n";
            statOut += bi.getScanRange() + "\n";
            statOut += bi.getScanRangeBonus() + "\n";
            statOut += bi.getShipTraining() + "\n";
            statOut += bi.getTroopTraining() + "\n";
            statOut += bi.getResistance() + "\n";
            statOut += bi.getAddedTradeRoutes() + "\n";
            statOut += bi.getIncomeOnTradeRoutes() + "\n";
            statOut += bi.getShipRecycling() + "\n";
            statOut += bi.getBuildingBuildSpeed() + "\n";
            statOut += bi.getUpdateBuildSpeed() + "\n";
            statOut += bi.getShipBuildSpeed() + "\n";
            statOut += bi.getTroopBuildSpeed() + "\n";
            statOut += bi.getPredecessorId() + "\n";
            statOut += (bi.getAlwaysOnline() ? 1 : 0) + "\n";
            statOut += (bi.getWorker() ? 1 : 0) + "\n";
            statOut += (bi.getNeverReady() ? 1 : 0) + "\n";
            for (int p = PlayerRaces.MAJOR1.getType(); p <= PlayerRaces.MAJOR6.getType(); p++)
                statOut += bi.getBuildingEquivalent(p) + "\n";
            for (int res = ResourceTypes.TITAN.getType(); res <= ResourceTypes.DERITIUM.getType(); res++)
                statOut += (bi.getResourceDistributor(res) ? 1 : 0) + "\n";
            statOut += bi.getNeededSystems() + "\n";
        }
        statHandle.writeString(statOut, false, "ISO-8859-1");
        for (int j = 0; j < numLangs; j++)
            langSpecificHandle[j].writeString(infoOut[j], false,
                    GameConstants.getCharset(GameConstants.getSupportedLanguages()[j]));
    }

    public void show() {
        visible = true;
        show(true, true);
    }

    public void show(boolean resetSelectedBuilding, boolean resetBuildingTable) {
        if (resetBuildingTable) {
            buildingItems.clear();
            buildingTable.clear();
            buildingTable.addListener(new InputListener() {
                @Override
                public boolean keyDown(InputEvent event, final int keycode) {
                    Button b = buildingItems.get(selectedItem);
                    switch (keycode) {
                        case Keys.DOWN:
                            selectedItem++;
                            break;
                        case Keys.UP:
                            selectedItem--;
                            break;
                        case Keys.HOME:
                            selectedItem = 0;
                            break;
                        case Keys.END:
                            selectedItem = buildingItems.size - 1;
                            break;
                        case Keys.PAGE_DOWN:
                            selectedItem += buildingScroller.getScrollHeight() / b.getHeight();
                            break;
                        case Keys.PAGE_UP:
                            selectedItem -= buildingScroller.getScrollHeight() / b.getHeight();
                            break;
                    }

                    if (BuildingEditor.this.isVisible()) {
                        if (selectedItem >= buildingItems.size)
                            selectedItem = buildingItems.size - 1;
                        if (selectedItem < 0)
                            selectedItem = 0;

                        b = buildingItems.get(selectedItem);
                        markBuildingListSelected(b);

                        Gdx.app.postRunnable(new Runnable() {
                            @Override
                            public void run() {
                                show();
                            }
                        });
                        Thread th = new Thread() {
                            @Override
                            public void run() {
                                try {
                                    Thread.sleep(150);
                                } catch (InterruptedException e) {
                                }
                                if (Gdx.input.isKeyPressed(keycode)) {
                                    Gdx.app.postRunnable(new Runnable() {
                                        @Override
                                        public void run() {
                                            InputEvent event = new InputEvent();
                                            event.setType(Type.keyDown);
                                            event.setKeyCode(keycode);
                                            buildingTable.fire(event);
                                        }
                                    });
                                }
                            }
                        };
                        th.start();
                    }

                    return false;
                }
            });

            for (int i = 0; i < localeBuildingInfos.get(selectedLanguage).size; i++) {
                BuildingInfo lbi = localeBuildingInfos.get(selectedLanguage).get(i);
                BuildingInfo bi = game.getBuildingInfos().get(i);
                Button.ButtonStyle bs = new Button.ButtonStyle();
                Button button = new Button(bs);
                Color color = normalColor;
                if (bi.getOwnerOfBuilding() != 0) {
                    Major major = game.getRaceController().getMajors().getValueAt(bi.getOwnerOfBuilding() - 1);
                    color = major.getRaceDesign().clrSector;
                }
                Label l = new Label(String.format("%04d", lbi.getRunningNumber()) + ": " + lbi.getBuildingName(), skin,
                        "mediumFont", Color.WHITE);
                l.setColor(color);
                l.setUserObject(new Integer(i));
                l.setEllipsis(true);
                button.add(l).width(buildingScroller.getWidth());
                button.setUserObject(l);
                ActorGestureListener gestureListener = new ActorGestureListener() {
                    @Override
                    public void tap(InputEvent event, float x, float y, int count, int button) {
                        Button b = (Button) event.getListenerActor();
                        markBuildingListSelected(b);
                        selectedItem = getIndex(b);
                        show(true, false);
                        stage.setKeyboardFocus(buildingTable);
                    }
                };
                button.addListener(gestureListener);

                if (lbi.getBuildingName().toLowerCase().contains(buildingFilter.getText().toLowerCase())) {
                    buildingTable.add(button).align(Align.left);
                    buildingItems.add(button);
                    buildingTable.row();
                }
            }
        }
        buildingScroller.setVisible(true);
        buildingFilter.setVisible(true);
        drawMainButtonTable();
        drawSelectButtonTable();

        Button btn = null;
        if (selectedItem != -1) {
            for (Button b : buildingItems) {
                if (selectedItem == getIndex(b)) {
                    btn = b;
                }
            }
        }
        if (btn == null) {
            if (buildingItems.size > 0) {
                btn = buildingItems.get(0);
                selectedItem = getIndex(btn);
            }
        }

        showBuildingInfo(resetSelectedBuilding);
        stage.draw();

        if (btn != null)
            markBuildingListSelected(btn);
    }

    private void showBuildingInfo(boolean resetSelectedBuilding) {
        if (selectedItem != -1) {
            //first row
            if (resetSelectedBuilding) {
                selectedBuildingInfo = new BuildingInfo(game.getBuildingInfos().get(selectedItem));
                localeBuildingInfo = new BuildingInfo(localeBuildingInfos.get(selectedLanguage).get(selectedItem));
            }
            BuildingInfo bi = selectedBuildingInfo;

            drawGenericInfoAndPrerequisites(bi, localeBuildingInfo);
            drawProductionInfo(bi);

            if (state == BuildingEditorState.StateGeneral) {
                buildingInfoTable.setVisible(true);
                buildingProductionTable.setVisible(false);
            } else {
                buildingInfoTable.setVisible(false);
                buildingProductionTable.setVisible(true);
            }
        }
    }

    private void drawGenericInfoAndPrerequisites(BuildingInfo bi, BuildingInfo localeBi) {
        buildingInfoTable.clear();
        Table table = new Table();
        Table container = new Table();
        TextField tf = new TextField(localeBi.getBuildingName(), skin);
        tf.setName("BUILDINGNAME");
        container.add(tf).width(GameConstants.wToRelative(200));
        container.row();
        loadedTexture = "graphics/buildings/" + bi.getGraphicFileName() + ".png";
        Image image = new Image(game.loadTextureImmediate(loadedTexture));
        image.addListener(new ActorGestureListener() {
            public void tap(InputEvent event, float x, float y, int count, int button) {
                if (count > 1) {
                    final TextField tf = new TextField(selectedBuildingInfo.getGraphicFileName(), skin);
                    Dialog dialog = new Dialog("Change building image:", skin) {
                        protected void result(Object object) {
                            if ((Boolean) object) {
                                selectedBuildingInfo.setGraphicFileName(tf.getText());
                                BuildingEditor.this.show(false, false);
                            }
                        }
                    }.text("New image name: ").button("Ok", true).button("Cancel", false).key(Keys.ENTER, true)
                            .key(Keys.ESCAPE, false).show(stage);
                    dialog.getContentTable().padTop(GameConstants.hToRelative(10));
                    dialog.getContentTable().add(tf).spaceTop(GameConstants.hToRelative(10))
                            .width(GameConstants.wToRelative(220));
                    dialog.getButtonTable().padTop(GameConstants.hToRelative(10));
                    dialog.getButtonTable().padBottom(GameConstants.hToRelative(10));
                    dialog.key(Keys.ENTER, true).key(Keys.ESCAPE, false);
                    dialog.invalidateHierarchy();
                    dialog.invalidate();
                    dialog.layout();
                    dialog.show(stage);
                }
            }
        });
        container.add(image).width(GameConstants.wToRelative(140)).height(GameConstants.hToRelative(80))
                .spaceTop(GameConstants.hToRelative(5));
        table.add(container).width(GameConstants.wToRelative(200)).height(GameConstants.hToRelative(110));

        tf = new TextArea(localeBi.getBuildingDescription(), skin);
        tf.setName("DESCRIPTION");
        table.add(tf).width(GameConstants.wToRelative(920)).height(GameConstants.hToRelative(110))
                .spaceLeft(GameConstants.wToRelative(10));
        buildingInfoTable.add(table).spaceBottom(GameConstants.hToRelative(5));
        buildingInfoTable.row();

        //second row
        table = new Table();
        Label l = new Label("ID: ", skin, "mediumFont", normalColor);
        table.add(l).width(GameConstants.wToRelative(35));

        tf = new TextField(bi.getRunningNumber() + "", skin);
        tf.setName("RUNNINGNUMBER");
        tf.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
        tf.setMaxLength(4);
        tf.setTextFieldListener(new TextFieldListener() {
            @Override
            public void keyTyped(TextField textField, char c) {
                if (!textField.getText().isEmpty()) {
                    int id = Integer.parseInt(textField.getText());
                    if (id > game.getBuildingInfos().size)
                        textField.setText((game.getBuildingInfos().size) + "");
                    if (id < 1)
                        textField.setText(1 + "");
                }
            }
        });
        table.add(tf).width(GameConstants.wToRelative(100)).spaceRight(GameConstants.wToRelative(10));

        l = new Label("Race: ", skin, "mediumFont", normalColor);
        table.add(l).width(GameConstants.wToRelative(60));
        Array<RaceInfo> races = new Array<BuildingEditor.RaceInfo>();
        for (int i = 1; i <= 6; i++)
            races.add(new RaceInfo(i, getRaceName(i)));
        races.add(new RaceInfo(0, getRaceName(0)));
        SelectBox<RaceInfo> buildingRaceSelect = new SelectBox<RaceInfo>(skin);
        buildingRaceSelect.setItems(races);
        buildingRaceSelect.setSelected(races.get(races.indexOf(
                new RaceInfo(bi.getOwnerOfBuilding(), getRaceName(bi.getOwnerOfBuilding())), false)));
        buildingRaceSelect.setName("RACE");

        table.add(buildingRaceSelect).height(GameConstants.hToRelative(30)).width(GameConstants.wToRelative(180))
                .spaceRight(GameConstants.wToRelative(30));

        l = new Label("Upgradeable: ", skin, "mediumFont", normalColor);
        table.add(l).width(GameConstants.wToRelative(120));
        CheckBox upgradeable = new CheckBox("", skin);
        upgradeable.setChecked(bi.isUpgradeable());
        upgradeable.setName("UPGRADEABLE");
        table.add(upgradeable).height(GameConstants.hToRelative(30)).width(GameConstants.wToRelative(15))
                .spaceRight(GameConstants.wToRelative(50));

        Array<String> sameClass = getBuildingsFromTheSameProdTypeAndRace(bi);
        l = new Label("Predecessor: ", skin, "mediumFont", normalColor);
        table.add(l).width(GameConstants.wToRelative(120));
        SelectBox<String> buildingPredecessorSelect = new SelectBox<String>(skin);
        buildingPredecessorSelect.setItems(sameClass);
        if (bi.getPredecessorId() != 0)
            buildingPredecessorSelect.setSelected(bi.getPredecessorId() + ": "
                    + localeBuildingInfos.get(selectedLanguage).get(bi.getPredecessorId() - 1).getBuildingName());
        buildingPredecessorSelect.setName("PREDECESSOR");
        table.add(buildingPredecessorSelect).height(GameConstants.hToRelative(30)).width(GameConstants.wToRelative(280))
                .spaceRight(GameConstants.wToRelative(10));

        buildingInfoTable.add(table).spaceBottom(GameConstants.hToRelative(5));
        buildingInfoTable.row();

        //third row
        table = new Table();
        l = new Label("Always online: ", skin, "mediumFont", normalColor);
        table.add(l).width(GameConstants.wToRelative(130));
        CheckBox alwaysOn = new CheckBox("", skin);
        alwaysOn.setChecked(bi.getAlwaysOnline());
        alwaysOn.setName("ALWAYS_ON");
        table.add(alwaysOn).height(GameConstants.hToRelative(30)).width(GameConstants.wToRelative(15))
                .spaceRight(GameConstants.wToRelative(50));

        l = new Label("Needs workers: ", skin, "mediumFont", normalColor);
        table.add(l).width(GameConstants.wToRelative(150));
        CheckBox needsWorker = new CheckBox("", skin);
        needsWorker.setChecked(bi.getWorker());
        needsWorker.setName("NEEDS_WORKER");
        table.add(needsWorker).height(GameConstants.hToRelative(30)).width(GameConstants.wToRelative(15))
                .spaceRight(GameConstants.wToRelative(50));

        l = new Label("Never ready: ", skin, "mediumFont", normalColor);
        table.add(l).width(GameConstants.wToRelative(125));
        CheckBox neverReady = new CheckBox("", skin);
        neverReady.setChecked(bi.getNeverReady());
        neverReady.setName("NEVER_READY");
        table.add(neverReady).height(GameConstants.hToRelative(30)).width(GameConstants.wToRelative(15))
                .spaceRight(GameConstants.wToRelative(50));

        buildingInfoTable.add(table).spaceBottom(GameConstants.hToRelative(15));
        buildingInfoTable.row();

        //fourth row
        table = new Table();
        l = new Label("Building equivalents from other races", skin, "normalFont", normalColor);
        l.setAlignment(Align.center);
        table.add(l).width(buildingInfoTable.getWidth());
        table.row().spaceBottom(GameConstants.hToRelative(10));

        container = new Table();
        ArrayMap<String, Major> majors = game.getRaceController().getMajors();
        for (int i = 0; i < majors.size; i++) {
            if (i % 3 == 0)
                container.row();
            Table tb = new Table();
            Major major = majors.getValueAt(i);
            l = new Label(major.getName(), skin, "mediumFont", normalColor);
            tb.add(l);
            tb.row();

            Array<String> sameType = getBuildingsFromDifferentRaceWithSameProd(bi, major.getRaceBuildingNumber());
            SelectBox<String> buildingEquivSelect = new SelectBox<String>(skin);
            buildingEquivSelect.setItems(sameType);
            int equivalent = bi.getBuildingEquivalent(major.getRaceBuildingNumber());
            if (equivalent != 0)
                buildingEquivSelect.setSelected(equivalent + ": "
                        + localeBuildingInfos.get(selectedLanguage).get(equivalent - 1).getBuildingName());
            buildingEquivSelect.setName("EQUIVALENT_" + major.getRaceId());
            tb.add(buildingEquivSelect).height(GameConstants.hToRelative(30)).width(GameConstants.wToRelative(280));
            container.add(tb).spaceRight(GameConstants.wToRelative(10));
        }
        table.add(container);

        buildingInfoTable.add(table).spaceBottom(GameConstants.hToRelative(15));
        buildingInfoTable.row();

        table = new Table();
        l = new Label("Buildable on planet types", skin, "normalFont", normalColor);
        l.setAlignment(Align.center);
        table.add(l).width(buildingInfoTable.getWidth());
        table.row().spaceBottom(GameConstants.hToRelative(15)).spaceTop(GameConstants.hToRelative(5));

        container = new Table();
        for (int i = 1; i <= PlanetType.PLANETCLASS_Y.getType(); i <<= 1) {
            PlanetType pt = PlanetType.fromPlanetType(i);
            l = new Label("Type " + pt.getTypeName() + ":", skin, "mediumFont", normalColor);
            container.add(l).width(GameConstants.wToRelative(70)).spaceBottom(GameConstants.hToRelative(5));
            CheckBox pbox = new CheckBox("", skin);
            pbox.setChecked(bi.getPlanetTypes(pt.getType()));
            pbox.setName("TYPE_" + pt.getTypeName());
            container.add(pbox).width(GameConstants.wToRelative(20)).spaceRight(GameConstants.wToRelative(10));
            if (i == PlanetType.PLANETCLASS_K.getType())
                container.row();
        }
        table.add(container);

        buildingInfoTable.add(table).spaceBottom(GameConstants.hToRelative(15));
        buildingInfoTable.row();

        table = new Table();
        l = new Label("Other prerequisites", skin, "normalFont", normalColor);
        l.setAlignment(Align.center);
        buildingInfoTable.add(l).width(buildingInfoTable.getWidth());
        buildingInfoTable.row().spaceBottom(GameConstants.hToRelative(15)).spaceTop(GameConstants.hToRelative(5));

        //fifth row
        table = new Table();
        l = new Label("Only homesystem: ", skin, "mediumFont", normalColor);
        table.add(l).width(GameConstants.wToRelative(180));
        CheckBox cb = new CheckBox("", skin);
        cb.setChecked(bi.isOnlyHomePlanet());
        cb.setName("ONLY_HOMEPLANET");
        table.add(cb).height(GameConstants.hToRelative(30)).width(GameConstants.wToRelative(15))
                .spaceRight(GameConstants.wToRelative(50));

        l = new Label("Only own colony: ", skin, "mediumFont", normalColor);
        table.add(l).width(GameConstants.wToRelative(160));
        cb = new CheckBox("", skin);
        cb.setChecked(bi.isOnlyOwnColony());
        cb.setName("ONLY_OWNCOLONY");
        table.add(cb).height(GameConstants.hToRelative(30)).width(GameConstants.wToRelative(15))
                .spaceRight(GameConstants.wToRelative(50));

        l = new Label("Only minorrace: ", skin, "mediumFont", normalColor);
        table.add(l).width(GameConstants.wToRelative(160));
        cb = new CheckBox("", skin);
        cb.setChecked(bi.isOnlyMinorRace());
        cb.setName("ONLY_MINORRACE");
        table.add(cb).height(GameConstants.hToRelative(30)).width(GameConstants.wToRelative(15))
                .spaceRight(GameConstants.wToRelative(50));

        l = new Label("Only conquered: ", skin, "mediumFont", normalColor);
        table.add(l).width(GameConstants.wToRelative(160));
        cb = new CheckBox("", skin);
        cb.setChecked(bi.isOnlyTakenSystem());
        cb.setName("ONLY_TAKENSYSTEM");
        table.add(cb).height(GameConstants.hToRelative(30)).width(GameConstants.wToRelative(15))
                .spaceRight(GameConstants.wToRelative(50));

        l = new Label("Unique to race: ", skin, "mediumFont", normalColor);
        table.add(l).width(GameConstants.wToRelative(160));
        cb = new CheckBox("", skin);
        cb.setChecked(bi.isOnlyRace());
        cb.setName("ONLY_RACE");
        table.add(cb).height(GameConstants.hToRelative(30)).width(GameConstants.wToRelative(15))
                .spaceRight(GameConstants.wToRelative(50));

        buildingInfoTable.add(table).spaceBottom(GameConstants.hToRelative(5));
        buildingInfoTable.row();

        //sixth row
        table = new Table();
        l = new Label("Only buildable in: ", skin, "mediumFont", normalColor);
        table.add(l).width(GameConstants.wToRelative(170));
        tf = new TextField(bi.getOnlyInSystemWithName(), skin);
        tf.setName("ONLY_BUILDABLE_IN");
        table.add(tf).width(GameConstants.wToRelative(140)).spaceRight(GameConstants.wToRelative(10));

        l = new Label("Min inhabitants (bn): ", skin, "mediumFont", normalColor);
        table.add(l).width(GameConstants.wToRelative(200));
        tf = new TextField(bi.getMinInhabitants() + "", skin);
        tf.setName("MIN_INHABITANTS");
        tf.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
        tf.setMaxLength(3);
        table.add(tf).width(GameConstants.wToRelative(40)).spaceRight(GameConstants.wToRelative(10));

        l = new Label("Min systems: ", skin, "mediumFont", normalColor);
        table.add(l).width(GameConstants.wToRelative(140));
        tf = new TextField(bi.getNeededSystems() + "", skin);
        tf.setName("MIN_SYSTEMS");
        tf.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
        tf.setMaxLength(2);
        table.add(tf).width(GameConstants.wToRelative(40)).spaceRight(GameConstants.wToRelative(10));

        l = new Label("Industry: ", skin, "mediumFont", normalColor);
        table.add(l).width(GameConstants.wToRelative(120));
        tf = new TextField(bi.getNeededIndustry() + "", skin);
        tf.setName("NEEDED_INDUSTRY");
        tf.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
        tf.setMaxLength(5);
        table.add(tf).width(GameConstants.wToRelative(60)).spaceRight(GameConstants.wToRelative(10));

        l = new Label("Energy: ", skin, "mediumFont", normalColor);
        table.add(l).width(GameConstants.wToRelative(130));
        tf = new TextField(bi.getNeededEnergy() + "", skin);
        tf.setName("NEEDED_ENERGY");
        tf.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
        tf.setMaxLength(4);
        table.add(tf).width(GameConstants.wToRelative(50)).spaceRight(GameConstants.wToRelative(10));

        buildingInfoTable.add(table).spaceBottom(GameConstants.hToRelative(5));
        buildingInfoTable.row();

        //seventh row
        table = new Table();
        for (int i = 0; i < ResourceTypes.FOOD.getType(); i++) {
            ResourceTypes rt = ResourceTypes.fromResourceTypes(i);
            l = new Label(StringDB.getString(rt.getName()) + ": ", skin, "mediumFont", normalColor);
            l.setEllipsis(true);
            table.add(l).width(GameConstants.wToRelative(120));
            tf = new TextField(bi.getNeededResource(rt.getType()) + "", skin);
            tf.setName("NEEDED_" + rt.getName());
            tf.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
            tf.setMaxLength(5);
            if (i == ResourceTypes.DERITIUM.getType())
                tf.setMaxLength(2);
            table.add(tf).width(GameConstants.wToRelative(60)).spaceRight(GameConstants.wToRelative(10));
        }
        buildingInfoTable.add(table).spaceBottom(GameConstants.hToRelative(5));
        buildingInfoTable.row();

        //eight row
        table = new Table();
        for (int i = 0; i < ResearchType.UNIQUE.getType(); i++) {
            ResearchType rt = ResearchType.fromIdx(i);
            l = new Label(StringDB.getString(rt.getShortName()) + ": ", skin, "mediumFont", normalColor);
            table.add(l).width(GameConstants.wToRelative(140));
            tf = new TextField(bi.getNeededTechLevel(rt) + "", skin);
            tf.setName("NEEDED_" + rt.getKey());
            tf.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
            tf.setMaxLength(3);
            table.add(tf).width(GameConstants.wToRelative(40)).spaceRight(GameConstants.wToRelative(10));
        }
        buildingInfoTable.add(table).spaceBottom(GameConstants.hToRelative(5));
        buildingInfoTable.row();

        //ninth row
        table = new Table();
        l = new Label("Max number per system: ", skin, "mediumFont", normalColor);
        table.add(l).width(GameConstants.wToRelative(220));
        tf = new TextField(bi.getMaxInSystem().number + "", skin);
        tf.setName("MAX_IN_SYSTEM_NR");
        tf.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
        tf.setMaxLength(2);
        table.add(tf).width(GameConstants.wToRelative(30)).spaceRight(GameConstants.wToRelative(10));
        l = new Label("of ID: ", skin, "mediumFont", normalColor);
        table.add(l).width(GameConstants.wToRelative(60));
        tf = new TextField(bi.getMaxInSystem().runningNumber + "", skin);
        tf.setName("MAX_IN_SYSTEM_ID");
        tf.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
        tf.setMaxLength(5);
        table.add(tf).width(GameConstants.wToRelative(60)).spaceRight(GameConstants.wToRelative(30));

        l = new Label("Max number per empire: ", skin, "mediumFont", normalColor);
        table.add(l).width(GameConstants.wToRelative(220));
        tf = new TextField(bi.getMaxInEmpire() + "", skin);
        tf.setName("MAX_IN_EMPIRE_NR");
        tf.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
        tf.setMaxLength(2);
        table.add(tf).width(GameConstants.wToRelative(30)).spaceRight(GameConstants.wToRelative(10));
        l = new Label("of ID: ", skin, "mediumFont", normalColor);
        table.add(l).width(GameConstants.wToRelative(60));
        tf = new TextField(bi.getMaxInEmpireID() + "", skin);
        tf.setName("MAX_IN_EMPIRE_ID");
        tf.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
        tf.setMaxLength(5);
        table.add(tf).width(GameConstants.wToRelative(60)).spaceRight(GameConstants.wToRelative(10));

        buildingInfoTable.add(table).spaceBottom(GameConstants.hToRelative(5));
        buildingInfoTable.row();

        //tenth row
        table = new Table();
        l = new Label("Min number per system: ", skin, "mediumFont", normalColor);
        table.add(l).width(GameConstants.wToRelative(220));
        tf = new TextField(bi.getMinInSystem().number + "", skin);
        tf.setName("MIN_IN_SYSTEM_NR");
        tf.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
        tf.setMaxLength(2);
        table.add(tf).width(GameConstants.wToRelative(30)).spaceRight(GameConstants.wToRelative(10));
        l = new Label("of ID: ", skin, "mediumFont", normalColor);
        table.add(l).width(GameConstants.wToRelative(60));
        tf = new TextField(bi.getMinInSystem().runningNumber + "", skin);
        tf.setName("MIN_IN_SYSTEM_ID");
        tf.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
        tf.setMaxLength(5);
        table.add(tf).width(GameConstants.wToRelative(60)).spaceRight(GameConstants.wToRelative(30));

        l = new Label("Min number per empire: ", skin, "mediumFont", normalColor);
        table.add(l).width(GameConstants.wToRelative(220));
        tf = new TextField(bi.getMinInEmpire().number + "", skin);
        tf.setName("MIN_IN_EMPIRE_NR");
        tf.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
        tf.setMaxLength(2);
        table.add(tf).width(GameConstants.wToRelative(30)).spaceRight(GameConstants.wToRelative(10));
        l = new Label("of ID: ", skin, "mediumFont", normalColor);
        table.add(l).width(GameConstants.wToRelative(60));
        tf = new TextField(bi.getMinInEmpire().runningNumber + "", skin);
        tf.setName("MIN_IN_EMPIRE_ID");
        tf.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
        tf.setMaxLength(5);
        table.add(tf).width(GameConstants.wToRelative(60)).spaceRight(GameConstants.wToRelative(10));

        buildingInfoTable.add(table).spaceBottom(GameConstants.hToRelative(5));
        buildingInfoTable.row();
    }

    private void drawProductionInfo(BuildingInfo bi) {
        TextFieldFilter numberFilter = new TextFieldFilter() {
            @Override
            public boolean acceptChar(TextField textField, char c) {
                return Character.isDigit(c) || c == '-';
            }
        };
        buildingProductionTable.clear();

        Table table = new Table();
        Label l = new Label("General production", skin, "normalFont", normalColor);
        l.setAlignment(Align.center);
        buildingProductionTable.add(l).width(buildingInfoTable.getWidth());
        buildingProductionTable.row().spaceBottom(GameConstants.hToRelative(15)).spaceTop(GameConstants.hToRelative(5));

        //first 4 rows
        for (int i = 0; i < WorkerType.ALL_WORKER.getType(); i++) {
            if (i % 6 == 0)
                table.row();
            Table container = new Table();
            WorkerType wt = WorkerType.fromWorkerType(i);
            l = new Label(StringDB.getString(wt.getDBString()) + ": ", skin, "mediumFont", normalColor);
            l.setEllipsis(true);
            container.add(l).width(GameConstants.wToRelative(120));
            TextField tf = new TextField(bi.getProductionByWorkers(wt) + "", skin);
            tf.setName("PRODUCED_" + wt.getDBString());
            tf.setTextFieldFilter(numberFilter);
            tf.setMaxLength(5);
            container.add(tf).width(GameConstants.wToRelative(60));
            container.row();
            l = new Label("Bonus (%): ", skin, "mediumFont", normalColor);
            l.setEllipsis(true);
            container.add(l).width(GameConstants.wToRelative(120));
            tf = new TextField(bi.getProductionBonusByWorkers(wt) + "", skin);
            tf.setName("PRODUCED_BONUS_" + wt.getDBString());
            tf.setTextFieldFilter(numberFilter);
            tf.setMaxLength(5);
            container.add(tf).width(GameConstants.wToRelative(60));
            table.add(container).spaceRight(GameConstants.wToRelative(10)).spaceBottom(GameConstants.hToRelative(5));
        }

        Table container = new Table();
        l = new Label("Deritium: ", skin, "mediumFont", normalColor);
        l.setEllipsis(true);
        container.add(l).width(GameConstants.wToRelative(120));
        TextField tf = new TextField(bi.getDeritiumProd() + "", skin);
        tf.setName("PRODUCED_DERITIUM");
        tf.setTextFieldFilter(numberFilter);
        tf.setMaxLength(5);
        container.add(tf).width(GameConstants.wToRelative(60));
        container.row();
        l = new Label("Bonus (%): ", skin, "mediumFont", normalColor);
        l.setEllipsis(true);
        container.add(l).width(GameConstants.wToRelative(120));
        tf = new TextField(bi.getDeritiumBonus() + "", skin);
        tf.setName("PRODUCED_BONUS_DERITIUM");
        tf.setTextFieldFilter(numberFilter);
        tf.setMaxLength(5);
        container.add(tf).width(GameConstants.wToRelative(60));
        table.add(container).spaceRight(GameConstants.wToRelative(10)).spaceBottom(GameConstants.hToRelative(5));

        container = new Table();
        l = new Label("Credits: ", skin, "mediumFont", normalColor);
        l.setEllipsis(true);
        container.add(l).width(GameConstants.wToRelative(120));
        tf = new TextField(bi.getCredits() + "", skin);
        tf.setName("PRODUCED_CREDITS");
        tf.setTextFieldFilter(numberFilter);
        tf.setMaxLength(5);
        container.add(tf).width(GameConstants.wToRelative(60));
        container.row();
        l = new Label("Bonus (%): ", skin, "mediumFont", normalColor);
        l.setEllipsis(true);
        container.add(l).width(GameConstants.wToRelative(120));
        tf = new TextField(bi.getCreditsBonus() + "", skin);
        tf.setName("PRODUCED_BONUS_CREDITS");
        tf.setTextFieldFilter(numberFilter);
        tf.setMaxLength(5);
        container.add(tf).width(GameConstants.wToRelative(60));
        table.add(container).spaceRight(GameConstants.wToRelative(10)).spaceBottom(GameConstants.hToRelative(5));

        buildingProductionTable.add(table).spaceBottom(GameConstants.hToRelative(5));
        buildingProductionTable.row();

        //fifth row
        table = new Table();
        l = new Label("System Morale: ", skin, "mediumFont", normalColor);
        l.setEllipsis(true);
        table.add(l).width(GameConstants.wToRelative(150));
        tf = new TextField(bi.getMoraleProd() + "", skin);
        tf.setName("PRODUCED_SYSTEM_MORALE");
        tf.setTextFieldFilter(numberFilter);
        tf.setMaxLength(5);
        table.add(tf).width(GameConstants.wToRelative(60)).spaceRight(GameConstants.wToRelative(50));

        l = new Label("Empire Morale: ", skin, "mediumFont", normalColor);
        l.setEllipsis(true);
        table.add(l).width(GameConstants.wToRelative(150));
        tf = new TextField(bi.getMoraleProdEmpire() + "", skin);
        tf.setName("PRODUCED_EMPIRE_MORALE");
        tf.setTextFieldFilter(numberFilter);
        tf.setMaxLength(5);
        table.add(tf).width(GameConstants.wToRelative(60)).spaceRight(GameConstants.wToRelative(50));

        l = new Label("All resources bonus (%): ", skin, "mediumFont", normalColor);
        l.setEllipsis(true);
        table.add(l).width(GameConstants.wToRelative(220));
        tf = new TextField(bi.getAllResourceBonus() + "", skin);
        tf.setName("PRODUCED_ALL_RESOURCE_BONUS");
        tf.setTextFieldFilter(numberFilter);
        tf.setMaxLength(5);
        table.add(tf).width(GameConstants.wToRelative(60)).spaceRight(GameConstants.wToRelative(50));

        l = new Label("Inner security bonus (%): ", skin, "mediumFont", normalColor);
        l.setEllipsis(true);
        table.add(l).width(GameConstants.wToRelative(220));
        tf = new TextField(bi.getInnerSecurityBonus() + "", skin);
        tf.setName("PRODUCED_INNER_SECURITY_BONUS");
        tf.setTextFieldFilter(numberFilter);
        tf.setMaxLength(5);
        table.add(tf).width(GameConstants.wToRelative(60));

        buildingProductionTable.add(table).spaceBottom(GameConstants.hToRelative(5));
        buildingProductionTable.row();

        l = new Label("Resource distributor", skin, "normalFont", normalColor);
        l.setAlignment(Align.center);
        buildingProductionTable.add(l).width(buildingInfoTable.getWidth());
        buildingProductionTable.row().spaceBottom(GameConstants.hToRelative(15)).spaceTop(GameConstants.hToRelative(5));

        //sixth row
        table = new Table();
        for (int i = 0; i < ResourceTypes.FOOD.getType(); i++) {
            ResourceTypes rt = ResourceTypes.fromResourceTypes(i);
            l = new Label(StringDB.getString(rt.getName()) + ": ", skin, "mediumFont", normalColor);
            table.add(l).width(GameConstants.wToRelative(120));
            CheckBox cb = new CheckBox("", skin);
            cb.setChecked(bi.getResourceDistributor(rt.getType()));
            cb.setName("DISTRIBUTOR_" + rt.getName());
            table.add(cb).height(GameConstants.hToRelative(30)).width(GameConstants.wToRelative(15))
                    .spaceRight(GameConstants.wToRelative(65));
        }
        buildingProductionTable.add(table).spaceBottom(GameConstants.hToRelative(5));
        buildingProductionTable.row();

        l = new Label("Other Spy/Sabotage Bonuses (%)", skin, "normalFont", normalColor);
        l.setAlignment(Align.center);
        buildingProductionTable.add(l).width(buildingInfoTable.getWidth());
        buildingProductionTable.row().spaceBottom(GameConstants.hToRelative(15)).spaceTop(GameConstants.hToRelative(5));

        //seventh row
        table = new Table();
        l = new Label("Economy spy: ", skin, "mediumFont", normalColor);
        l.setEllipsis(true);
        table.add(l).width(GameConstants.wToRelative(120));
        tf = new TextField(bi.getEconomySpyBonus() + "", skin);
        tf.setName("PRODUCED_ECONOMY_SPY_BONUS");
        tf.setTextFieldFilter(numberFilter);
        tf.setMaxLength(5);
        table.add(tf).width(GameConstants.wToRelative(60)).spaceRight(GameConstants.wToRelative(10));

        l = new Label("Economy sab: ", skin, "mediumFont", normalColor);
        l.setEllipsis(true);
        table.add(l).width(GameConstants.wToRelative(120));
        tf = new TextField(bi.getEconomySabotageBonus() + "", skin);
        tf.setName("PRODUCED_ECONOMY_SABOTAGE_BONUS");
        tf.setTextFieldFilter(numberFilter);
        tf.setMaxLength(5);
        table.add(tf).width(GameConstants.wToRelative(60)).spaceRight(GameConstants.wToRelative(10));

        l = new Label("Research spy: ", skin, "mediumFont", normalColor);
        l.setEllipsis(true);
        table.add(l).width(GameConstants.wToRelative(120));
        tf = new TextField(bi.getResearchSpyBonus() + "", skin);
        tf.setName("PRODUCED_RESEARCH_SPY_BONUS");
        tf.setTextFieldFilter(numberFilter);
        tf.setMaxLength(5);
        table.add(tf).width(GameConstants.wToRelative(60)).spaceRight(GameConstants.wToRelative(10));

        l = new Label("Research sab: ", skin, "mediumFont", normalColor);
        l.setEllipsis(true);
        table.add(l).width(GameConstants.wToRelative(120));
        tf = new TextField(bi.getResearchSabotageBonus() + "", skin);
        tf.setName("PRODUCED_RESEARCH_SABOTAGE_BONUS");
        tf.setTextFieldFilter(numberFilter);
        tf.setMaxLength(5);
        table.add(tf).width(GameConstants.wToRelative(60)).spaceRight(GameConstants.wToRelative(10));

        l = new Label("Military spy: ", skin, "mediumFont", normalColor);
        l.setEllipsis(true);
        table.add(l).width(GameConstants.wToRelative(120));
        tf = new TextField(bi.getMilitarySpyBonus() + "", skin);
        tf.setName("PRODUCED_MILITARY_SPY_BONUS");
        tf.setTextFieldFilter(numberFilter);
        tf.setMaxLength(5);
        table.add(tf).width(GameConstants.wToRelative(60)).spaceRight(GameConstants.wToRelative(10));

        l = new Label("Military sab: ", skin, "mediumFont", normalColor);
        l.setEllipsis(true);
        table.add(l).width(GameConstants.wToRelative(120));
        tf = new TextField(bi.getMilitarySabogateBonus() + "", skin);
        tf.setName("PRODUCED_MILITARY_SABOTAGE_BONUS");
        tf.setTextFieldFilter(numberFilter);
        tf.setMaxLength(5);
        table.add(tf).width(GameConstants.wToRelative(60)).spaceRight(GameConstants.wToRelative(10));

        buildingProductionTable.add(table).spaceBottom(GameConstants.hToRelative(5));
        buildingProductionTable.row();

        l = new Label("Research Bonuses (%)", skin, "normalFont", normalColor);
        l.setAlignment(Align.center);
        buildingProductionTable.add(l).width(buildingInfoTable.getWidth());
        buildingProductionTable.row().spaceBottom(GameConstants.hToRelative(15)).spaceTop(GameConstants.hToRelative(5));

        //eight row
        table = new Table();
        for (int i = 0; i < ResearchType.UNIQUE.getType(); i++) {
            ResearchType rt = ResearchType.fromIdx(i);
            l = new Label(StringDB.getString(rt.getShortName()) + ": ", skin, "mediumFont", normalColor);
            table.add(l).width(GameConstants.wToRelative(120));
            tf = new TextField(bi.getTechBonus(rt) + "", skin);
            tf.setName("PRODUCED_RESEARCH_BONUS_" + rt.getKey());
            tf.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
            tf.setMaxLength(4);
            table.add(tf).width(GameConstants.wToRelative(60)).spaceRight(GameConstants.wToRelative(10));
        }
        buildingProductionTable.add(table).spaceBottom(GameConstants.hToRelative(5));
        buildingProductionTable.row();

        l = new Label("Units", skin, "normalFont", normalColor);
        l.setAlignment(Align.center);
        buildingProductionTable.add(l).width(buildingInfoTable.getWidth());
        buildingProductionTable.row().spaceBottom(GameConstants.hToRelative(15)).spaceTop(GameConstants.hToRelative(5));

        //ninth row
        table = new Table();
        l = new Label("Shipyard: ", skin, "mediumFont", normalColor);
        table.add(l).width(GameConstants.wToRelative(90));
        CheckBox cb = new CheckBox("", skin);
        cb.setChecked(bi.isShipYard());
        cb.setName("IS_SHIPYARD");
        table.add(cb).height(GameConstants.hToRelative(30)).width(GameConstants.wToRelative(15))
                .spaceRight(GameConstants.wToRelative(10));

        l = new Label("Shipyard buildspeed (%): ", skin, "mediumFont", normalColor);
        l.setEllipsis(true);
        table.add(l).width(GameConstants.wToRelative(220));
        tf = new TextField(bi.getShipYardSpeed() + "", skin);
        tf.setName("SHIPYARDSPEED");
        tf.setTextFieldFilter(numberFilter);
        tf.setMaxLength(5);
        table.add(tf).width(GameConstants.wToRelative(60)).spaceRight(GameConstants.wToRelative(10));

        l = new Label("Max shipsize: ", skin, "mediumFont", normalColor);
        table.add(l).width(GameConstants.wToRelative(120));
        SelectBox<ShipSize> shipSizeSelect = new SelectBox<ShipSize>(skin);
        shipSizeSelect.setItems(ShipSize.values());
        shipSizeSelect.setSelected(bi.getMaxBuildableShipSize());
        shipSizeSelect.setName("MAX_SHIP_SIZE");
        table.add(shipSizeSelect).height(GameConstants.hToRelative(30)).width(GameConstants.wToRelative(130))
                .spaceRight(GameConstants.wToRelative(10));

        l = new Label("Ship training: ", skin, "mediumFont", normalColor);
        l.setEllipsis(true);
        table.add(l).width(GameConstants.wToRelative(120));
        tf = new TextField(bi.getShipTraining() + "", skin);
        tf.setName("SHIP_TRAINING");
        tf.setTextFieldFilter(numberFilter);
        tf.setMaxLength(5);
        table.add(tf).width(GameConstants.wToRelative(60)).spaceRight(GameConstants.wToRelative(10));

        buildingProductionTable.add(table).spaceBottom(GameConstants.hToRelative(5));
        buildingProductionTable.row();

        //tenth row
        table = new Table();
        l = new Label("Barracks: ", skin, "mediumFont", normalColor);
        table.add(l).width(GameConstants.wToRelative(90));
        cb = new CheckBox("", skin);
        cb.setChecked(bi.isBarrack());
        cb.setName("IS_BARRACK");
        table.add(cb).height(GameConstants.hToRelative(30)).width(GameConstants.wToRelative(15))
                .spaceRight(GameConstants.wToRelative(10));

        l = new Label("Barrack buildspeed (%): ", skin, "mediumFont", normalColor);
        l.setEllipsis(true);
        table.add(l).width(GameConstants.wToRelative(210));
        tf = new TextField(bi.getBarrackSpeed() + "", skin);
        tf.setName("BARRACKSPEED");
        tf.setTextFieldFilter(numberFilter);
        tf.setMaxLength(5);
        table.add(tf).width(GameConstants.wToRelative(60)).spaceRight(GameConstants.wToRelative(10));

        l = new Label("Troop training: ", skin, "mediumFont", normalColor);
        l.setEllipsis(true);
        table.add(l).width(GameConstants.wToRelative(140));
        tf = new TextField(bi.getTroopTraining() + "", skin);
        tf.setName("TROOP_TRAINING");
        tf.setTextFieldFilter(numberFilter);
        tf.setMaxLength(5);
        table.add(tf).width(GameConstants.wToRelative(60)).spaceRight(GameConstants.wToRelative(10));

        buildingProductionTable.add(table).spaceBottom(GameConstants.hToRelative(5));
        buildingProductionTable.row();

        table = new Table();
        l = new Label("Defence", skin, "normalFont", normalColor);
        l.setAlignment(Align.center);
        buildingProductionTable.add(l).width(buildingInfoTable.getWidth());
        buildingProductionTable.row().spaceBottom(GameConstants.hToRelative(15)).spaceTop(GameConstants.hToRelative(5));

        //eleven-twelweth row
        container = new Table();
        l = new Label("Shieldpower: ", skin, "mediumFont", normalColor);
        l.setEllipsis(true);
        container.add(l).width(GameConstants.wToRelative(120));
        tf = new TextField(bi.getShieldPower() + "", skin);
        tf.setName("SHIELDPOWER");
        tf.setTextFieldFilter(numberFilter);
        tf.setMaxLength(5);
        container.add(tf).width(GameConstants.wToRelative(60));
        container.row();
        l = new Label("Bonus (%): ", skin, "mediumFont", normalColor);
        l.setEllipsis(true);
        container.add(l).width(GameConstants.wToRelative(120));
        tf = new TextField(bi.getShieldPowerBonus() + "", skin);
        tf.setName("SHIELDPOWER_BONUS");
        tf.setTextFieldFilter(numberFilter);
        tf.setMaxLength(5);
        container.add(tf).width(GameConstants.wToRelative(60));
        table.add(container).spaceRight(GameConstants.wToRelative(10)).spaceBottom(GameConstants.hToRelative(5));

        container = new Table();
        l = new Label("Gnd. defense: ", skin, "mediumFont", normalColor);
        l.setEllipsis(true);
        container.add(l).width(GameConstants.wToRelative(120));
        tf = new TextField(bi.getGroundDefend() + "", skin);
        tf.setName("GROUNDDEFENSE");
        tf.setTextFieldFilter(numberFilter);
        tf.setMaxLength(5);
        container.add(tf).width(GameConstants.wToRelative(60));
        container.row();
        l = new Label("Bonus (%): ", skin, "mediumFont", normalColor);
        l.setEllipsis(true);
        container.add(l).width(GameConstants.wToRelative(120));
        tf = new TextField(bi.getGroundDefendBonus() + "", skin);
        tf.setName("GROUNDDEFENSE_BONUS");
        tf.setTextFieldFilter(numberFilter);
        tf.setMaxLength(5);
        container.add(tf).width(GameConstants.wToRelative(60));
        table.add(container).spaceRight(GameConstants.wToRelative(10)).spaceBottom(GameConstants.hToRelative(5));

        container = new Table();
        l = new Label("Ship defense: ", skin, "mediumFont", normalColor);
        l.setEllipsis(true);
        container.add(l).width(GameConstants.wToRelative(120));
        tf = new TextField(bi.getShipDefend() + "", skin);
        tf.setName("SHIPDEFENSE");
        tf.setTextFieldFilter(numberFilter);
        tf.setMaxLength(5);
        container.add(tf).width(GameConstants.wToRelative(60));
        container.row();
        l = new Label("Bonus (%): ", skin, "mediumFont", normalColor);
        l.setEllipsis(true);
        container.add(l).width(GameConstants.wToRelative(120));
        tf = new TextField(bi.getShipDefendBonus() + "", skin);
        tf.setName("SHIPDEFENSE_BONUS");
        tf.setTextFieldFilter(numberFilter);
        tf.setMaxLength(5);
        container.add(tf).width(GameConstants.wToRelative(60));
        table.add(container).spaceRight(GameConstants.wToRelative(10)).spaceBottom(GameConstants.hToRelative(5));

        container = new Table();
        l = new Label("Hitpoints: ", skin, "mediumFont", normalColor);
        l.setEllipsis(true);
        container.add(l).width(GameConstants.wToRelative(120));
        tf = new TextField(bi.getHitPoints() + "", skin);
        tf.setName("HITPOINTS");
        tf.setTextFieldFilter(numberFilter);
        tf.setMaxLength(5);
        container.add(tf).width(GameConstants.wToRelative(60));
        container.row();
        table.add(container).spaceRight(GameConstants.wToRelative(10)).spaceBottom(GameConstants.hToRelative(5)).align(Align.top);

        container = new Table();
        l = new Label("Scanpower: ", skin, "mediumFont", normalColor);
        l.setEllipsis(true);
        container.add(l).width(GameConstants.wToRelative(120));
        tf = new TextField(bi.getScanPower() + "", skin);
        tf.setName("SCANPOWER");
        tf.setTextFieldFilter(numberFilter);
        tf.setMaxLength(5);
        container.add(tf).width(GameConstants.wToRelative(60));
        container.row();
        l = new Label("Bonus (%): ", skin, "mediumFont", normalColor);
        l.setEllipsis(true);
        container.add(l).width(GameConstants.wToRelative(120));
        tf = new TextField(bi.getScanPowerBonus() + "", skin);
        tf.setName("SCANPOWER_BONUS");
        tf.setTextFieldFilter(numberFilter);
        tf.setMaxLength(5);
        container.add(tf).width(GameConstants.wToRelative(60));
        table.add(container).spaceRight(GameConstants.wToRelative(10)).spaceBottom(GameConstants.hToRelative(5));

        container = new Table();
        l = new Label("Scanrange: ", skin, "mediumFont", normalColor);
        l.setEllipsis(true);
        container.add(l).width(GameConstants.wToRelative(120));
        tf = new TextField(bi.getScanRange() + "", skin);
        tf.setName("SCANRANGE");
        tf.setTextFieldFilter(numberFilter);
        tf.setMaxLength(5);
        container.add(tf).width(GameConstants.wToRelative(60));
        container.row();
        l = new Label("Bonus (%): ", skin, "mediumFont", normalColor);
        l.setEllipsis(true);
        container.add(l).width(GameConstants.wToRelative(120));
        tf = new TextField(bi.getScanRangeBonus() + "", skin);
        tf.setName("SCANRANGE_BONUS");
        tf.setTextFieldFilter(numberFilter);
        tf.setMaxLength(5);
        container.add(tf).width(GameConstants.wToRelative(60));
        table.add(container).spaceRight(GameConstants.wToRelative(10)).spaceBottom(GameConstants.hToRelative(5));

        buildingProductionTable.add(table).spaceBottom(GameConstants.hToRelative(5));
        buildingProductionTable.row();

        table = new Table();
        l = new Label("Special Bonuses", skin, "normalFont", normalColor);
        l.setAlignment(Align.center);
        buildingProductionTable.add(l).width(buildingInfoTable.getWidth());
        buildingProductionTable.row().spaceBottom(GameConstants.hToRelative(15)).spaceTop(GameConstants.hToRelative(5));

        table = new Table();
        l = new Label("Buildingbuildspeed (%): ", skin, "mediumFont", normalColor);
        l.setEllipsis(true);
        table.add(l).width(GameConstants.wToRelative(210));
        tf = new TextField(bi.getBuildingBuildSpeed() + "", skin);
        tf.setName("BONUS_BUILDINGBUILDSPEED");
        tf.setTextFieldFilter(numberFilter);
        tf.setMaxLength(5);
        table.add(tf).width(GameConstants.wToRelative(60)).spaceRight(GameConstants.wToRelative(10));

        l = new Label("Upgradebuildspeed (%): ", skin, "mediumFont", normalColor);
        l.setEllipsis(true);
        table.add(l).width(GameConstants.wToRelative(215));
        tf = new TextField(bi.getUpdateBuildSpeed() + "", skin);
        tf.setName("BONUS_UPGRADEBUILDSPEED");
        tf.setTextFieldFilter(numberFilter);
        tf.setMaxLength(5);
        table.add(tf).width(GameConstants.wToRelative(60)).spaceRight(GameConstants.wToRelative(10));

        l = new Label("Shipbuildspeed (%): ", skin, "mediumFont", normalColor);
        l.setEllipsis(true);
        table.add(l).width(GameConstants.wToRelative(180));
        tf = new TextField(bi.getShipBuildSpeed() + "", skin);
        tf.setName("BONUS_SHIPBUILDSPEED");
        tf.setTextFieldFilter(numberFilter);
        tf.setMaxLength(5);
        table.add(tf).width(GameConstants.wToRelative(60)).spaceRight(GameConstants.wToRelative(10));

        l = new Label("Troopbuildspeed (%): ", skin, "mediumFont", normalColor);
        l.setEllipsis(true);
        table.add(l).width(GameConstants.wToRelative(200));
        tf = new TextField(bi.getTroopBuildSpeed() + "", skin);
        tf.setName("BONUS_TROOPBUILDSPEED");
        tf.setTextFieldFilter(numberFilter);
        tf.setMaxLength(5);
        table.add(tf).width(GameConstants.wToRelative(60)).spaceRight(GameConstants.wToRelative(10));

        buildingProductionTable.add(table).spaceBottom(GameConstants.hToRelative(5));
        buildingProductionTable.row();

        table = new Table();
        l = new Label("Other specials", skin, "normalFont", normalColor);
        l.setAlignment(Align.center);
        buildingProductionTable.add(l).width(buildingInfoTable.getWidth());
        buildingProductionTable.row().spaceBottom(GameConstants.hToRelative(15)).spaceTop(GameConstants.hToRelative(5));

        table = new Table();
        l = new Label("Bribery resistance: ", skin, "mediumFont", normalColor);
        l.setEllipsis(true);
        table.add(l).width(GameConstants.wToRelative(175));
        tf = new TextField(bi.getResistance() + "", skin);
        tf.setName("BRIBERY_RESISTANCE");
        tf.setTextFieldFilter(numberFilter);
        tf.setMaxLength(5);
        table.add(tf).width(GameConstants.wToRelative(60)).spaceRight(GameConstants.wToRelative(10));

        l = new Label("Added traderoutes: ", skin, "mediumFont", normalColor);
        l.setEllipsis(true);
        table.add(l).width(GameConstants.wToRelative(175));
        tf = new TextField(bi.getAddedTradeRoutes() + "", skin);
        tf.setName("TRADEROUTES");
        tf.setTextFieldFilter(numberFilter);
        tf.setMaxLength(5);
        table.add(tf).width(GameConstants.wToRelative(60)).spaceRight(GameConstants.wToRelative(10));

        l = new Label("Income on traderoutes (%): ", skin, "mediumFont", normalColor);
        l.setEllipsis(true);
        table.add(l).width(GameConstants.wToRelative(245));
        tf = new TextField(bi.getIncomeOnTradeRoutes() + "", skin);
        tf.setName("TRADEROUTES_BONUS");
        tf.setTextFieldFilter(numberFilter);
        tf.setMaxLength(5);
        table.add(tf).width(GameConstants.wToRelative(60)).spaceRight(GameConstants.wToRelative(10));

        l = new Label("Shiprecycling (%): ", skin, "mediumFont", normalColor);
        l.setEllipsis(true);
        table.add(l).width(GameConstants.wToRelative(160));
        tf = new TextField(bi.getShipRecycling() + "", skin);
        tf.setName("SHIPRECYCLING");
        tf.setTextFieldFilter(numberFilter);
        tf.setMaxLength(5);
        table.add(tf).width(GameConstants.wToRelative(60)).spaceRight(GameConstants.wToRelative(10));

        buildingProductionTable.add(table).spaceBottom(GameConstants.hToRelative(5));
        buildingProductionTable.row();
    }

    private void saveBuildingInfo() {
        BuildingInfo bi = selectedBuildingInfo;
        bi.setRunningNumber(parseIntDefault(getTextFieldValue(buildingInfoTable, "RUNNINGNUMBER")));
        localeBuildingInfo.setBuildingName(getTextFieldValue(buildingInfoTable, "BUILDINGNAME"));
        localeBuildingInfo.setBuildingDescription(getTextFieldValue(buildingInfoTable, "DESCRIPTION"));
        bi.setOwnerOfBuilding(((RaceInfo) getSelectBoxValue(buildingInfoTable, "RACE")).getId());
        bi.setUpgradeable(getCheckBoxValue(buildingInfoTable, "UPGRADEABLE"));
        bi.setPredecessorId(getIdFromStringWithName((String) getSelectBoxValue(buildingInfoTable, "PREDECESSOR")));
        bi.setAlwaysOnline(getCheckBoxValue(buildingInfoTable, "ALWAYS_ON"));
        bi.setWorker(getCheckBoxValue(buildingInfoTable, "NEEDS_WORKER"));
        bi.setNeverReady(getCheckBoxValue(buildingInfoTable, "NEVER_READY"));
        if (bi.getRunningNumber() == selectedItem + 1) { //if the running number changed, equivalents are calculated in the update
            ArrayMap<String, Major> majors = game.getRaceController().getMajors();
            for (int i = 0; i < majors.size; i++) {
                Major major = majors.getValueAt(i);
                if (bi.getOwnerOfBuilding() == major.getRaceBuildingNumber())
                    continue;
                int equivalent = getIdFromStringWithName((String) getSelectBoxValue(buildingInfoTable, "EQUIVALENT_" + major.getRaceId()));
                bi.setBuildingEquivalent(major.getRaceBuildingNumber(), equivalent);

                for (BuildingInfo other : game.getBuildingInfos())
                    if (other.getOwnerOfBuilding() == major.getRaceBuildingNumber())
                        if (bi.getRunningNumber() == other.getBuildingEquivalent(bi.getOwnerOfBuilding())) {
                            other.setBuildingEquivalent(bi.getOwnerOfBuilding(), 0);
                        }
                for (BuildingInfo other : game.getBuildingInfos())
                    if (other.getOwnerOfBuilding() == major.getRaceBuildingNumber())
                        if (equivalent == other.getRunningNumber()) {
                            other.setBuildingEquivalent(bi.getOwnerOfBuilding(), bi.getRunningNumber());
                        }
            }
        }
        for (int i = 1; i <= PlanetType.PLANETCLASS_Y.getType(); i <<= 1) {
            PlanetType pt = PlanetType.fromPlanetType(i);
            bi.setPlanetTypes(pt.getPlanetClass(), getCheckBoxValue(buildingInfoTable, "TYPE_" + pt.getTypeName()));
        }
        bi.setOnlyHomePlanet(getCheckBoxValue(buildingInfoTable, "ONLY_HOMEPLANET"));
        bi.setOnlyOwnColony(getCheckBoxValue(buildingInfoTable, "ONLY_OWNCOLONY"));
        bi.setOnlyMinorRace(getCheckBoxValue(buildingInfoTable, "ONLY_MINORRACE"));
        bi.setOnlyTakenSystem(getCheckBoxValue(buildingInfoTable, "ONLY_TAKENSYSTEM"));
        bi.setOnlyRace(getCheckBoxValue(buildingInfoTable, "ONLY_RACE"));
        bi.setOnlyInSystemWithName(getTextFieldValue(buildingInfoTable, "ONLY_BUILDABLE_IN"));
        bi.setMinInhabitants(parseIntDefault(getTextFieldValue(buildingInfoTable, "MIN_INHABITANTS")));
        bi.setNeededSystems(parseIntDefault(getTextFieldValue(buildingInfoTable, "MIN_SYSTEMS")));
        bi.setNeededIndustry(parseIntDefault(getTextFieldValue(buildingInfoTable, "NEEDED_INDUSTRY")));
        bi.setNeededEnergy(parseIntDefault(getTextFieldValue(buildingInfoTable, "NEEDED_ENERGY")));
        for (int i = 0; i < ResourceTypes.FOOD.getType(); i++) {
            ResourceTypes rt = ResourceTypes.fromResourceTypes(i);
            bi.setNeededResource(rt, parseIntDefault(getTextFieldValue(buildingInfoTable, "NEEDED_" + rt.getName())));
        }
        for (int i = 0; i < ResearchType.UNIQUE.getType(); i++) {
            ResearchType rt = ResearchType.fromIdx(i);
            bi.setNeededResearch(rt, parseIntDefault(getTextFieldValue(buildingInfoTable, "NEEDED_" + rt.getKey())));
        }
        bi.setMaxInSystem(parseIntDefault(getTextFieldValue(buildingInfoTable, "MAX_IN_SYSTEM_NR")),
                parseIntDefault(getTextFieldValue(buildingInfoTable, "MAX_IN_SYSTEM_ID")));
        bi.setMaxInEmpire(parseIntDefault(getTextFieldValue(buildingInfoTable, "MAX_IN_EMPIRE_NR")),
                parseIntDefault(getTextFieldValue(buildingInfoTable, "MAX_IN_EMPIRE_ID")));
        bi.setMinInSystem(parseIntDefault(getTextFieldValue(buildingInfoTable, "MIN_IN_SYSTEM_NR")),
                parseIntDefault(getTextFieldValue(buildingInfoTable, "MIN_IN_SYSTEM_ID")));
        bi.setMinInEmpire(parseIntDefault(getTextFieldValue(buildingInfoTable, "MIN_IN_EMPIRE_NR")),
                parseIntDefault(getTextFieldValue(buildingInfoTable, "MIN_IN_EMPIRE_ID")));

        for (int i = 0; i < WorkerType.ALL_WORKER.getType(); i++) {
            WorkerType wt = WorkerType.fromWorkerType(i);
            bi.setProductionByWorkers(wt, parseIntDefault(getTextFieldValue(buildingProductionTable, "PRODUCED_" + wt.getDBString())));
            bi.setProductionBonusByWorkers(wt, parseIntDefault(getTextFieldValue(buildingProductionTable, "PRODUCED_BONUS_" + wt.getDBString())));
        }
        bi.setDeritiumProd(parseIntDefault(getTextFieldValue(buildingProductionTable, "PRODUCED_DERITIUM")));
        bi.setDeritiumBonus(parseIntDefault(getTextFieldValue(buildingProductionTable, "PRODUCED_BONUS_DERITIUM")));
        bi.setCreditsProd(parseIntDefault(getTextFieldValue(buildingProductionTable, "PRODUCED_CREDITS")));
        bi.setCreditsBonus(parseIntDefault(getTextFieldValue(buildingProductionTable, "PRODUCED_BONUS_CREDITS")));
        bi.setMoraleProd(parseIntDefault(getTextFieldValue(buildingProductionTable, "PRODUCED_SYSTEM_MORALE")));
        bi.setMoraleProdEmpire(parseIntDefault(getTextFieldValue(buildingProductionTable, "PRODUCED_EMPIRE_MORALE")));
        bi.setAllResourceBonus(parseIntDefault(getTextFieldValue(buildingProductionTable, "PRODUCED_ALL_RESOURCE_BONUS")));
        bi.setInnerSecurityBonus(parseIntDefault(getTextFieldValue(buildingProductionTable, "PRODUCED_INNER_SECURITY_BONUS")));
        bi.setEconomySpyBonus(parseIntDefault(getTextFieldValue(buildingProductionTable, "PRODUCED_ECONOMY_SPY_BONUS")));
        bi.setEconomySabotageBonus(parseIntDefault(getTextFieldValue(buildingProductionTable, "PRODUCED_ECONOMY_SABOTAGE_BONUS")));
        bi.setResearchSpyBonus(parseIntDefault(getTextFieldValue(buildingProductionTable, "PRODUCED_RESEARCH_SPY_BONUS")));
        bi.setResearchSabotageBonus(parseIntDefault(getTextFieldValue(buildingProductionTable, "PRODUCED_RESEARCH_SABOTAGE_BONUS")));
        bi.setMilitarySpyBonus(parseIntDefault(getTextFieldValue(buildingProductionTable, "PRODUCED_MILITARY_SPY_BONUS")));
        bi.setMilitarySabogateBonus(parseIntDefault(getTextFieldValue(buildingProductionTable, "PRODUCED_MILITARY_SABOTAGE_BONUS")));
        for (int i = 0; i < ResearchType.UNIQUE.getType(); i++) {
            ResearchType rt = ResearchType.fromIdx(i);
            bi.setTechBonus(rt, parseIntDefault(getTextFieldValue(buildingProductionTable, "PRODUCED_RESEARCH_BONUS_" + rt.getKey())));
        }
        for (int i = 0; i < ResourceTypes.FOOD.getType(); i++) {
            ResourceTypes rt = ResourceTypes.fromResourceTypes(i);
            bi.setResourceDistributor(i, getCheckBoxValue(buildingProductionTable, "DISTRIBUTOR_" + rt.getName()));
        }

        bi.setShipYard(getCheckBoxValue(buildingProductionTable, "IS_SHIPYARD"));
        bi.setShipYardSpeed(parseIntDefault(getTextFieldValue(buildingProductionTable, "SHIPYARDSPEED")));
        bi.setBuildableShipSizes((ShipSize) getSelectBoxValue(buildingProductionTable, "MAX_SHIP_SIZE"));
        bi.setShipTraining(parseIntDefault(getTextFieldValue(buildingProductionTable, "SHIP_TRAINING")));
        bi.setBarrack(getCheckBoxValue(buildingProductionTable, "IS_BARRACK"));
        bi.setBarrackSpeed(parseIntDefault(getTextFieldValue(buildingProductionTable, "BARRACKSPEED")));
        bi.setTroopTraining(parseIntDefault(getTextFieldValue(buildingProductionTable, "TROOP_TRAINING")));

        bi.setShieldPower(parseIntDefault(getTextFieldValue(buildingProductionTable, "SHIELDPOWER")));
        bi.setShieldPowerBonus(parseIntDefault(getTextFieldValue(buildingProductionTable, "SHIELDPOWER_BONUS")));
        bi.setGroundDefend(parseIntDefault(getTextFieldValue(buildingProductionTable, "GROUNDDEFENSE")));
        bi.setGroundDefendBonus(parseIntDefault(getTextFieldValue(buildingProductionTable, "GROUNDDEFENSE_BONUS")));
        bi.setShipDefend(parseIntDefault(getTextFieldValue(buildingProductionTable, "SHIPDEFENSE")));
        bi.setShipDefendBonus(parseIntDefault(getTextFieldValue(buildingProductionTable, "SHIPDEFENSE_BONUS")));
        bi.setHitPoints(parseIntDefault(getTextFieldValue(buildingProductionTable, "HITPOINTS")));
        bi.setScanPower(parseIntDefault(getTextFieldValue(buildingProductionTable, "SCANPOWER")));
        bi.setScanPowerBonus(parseIntDefault(getTextFieldValue(buildingProductionTable, "SCANPOWER_BONUS")));
        bi.setScanRange(parseIntDefault(getTextFieldValue(buildingProductionTable, "SCANRANGE")));
        bi.setScanRangeBonus(parseIntDefault(getTextFieldValue(buildingProductionTable, "SCANRANGE_BONUS")));

        bi.setBuildingBuildSpeed(parseIntDefault(getTextFieldValue(buildingProductionTable, "BONUS_BUILDINGBUILDSPEED")));
        bi.setUpdateBuildSpeed(parseIntDefault(getTextFieldValue(buildingProductionTable, "BONUS_UPGRADEBUILDSPEED")));
        bi.setShipBuildSpeed(parseIntDefault(getTextFieldValue(buildingProductionTable, "BONUS_SHIPBUILDSPEED")));
        bi.setTroopBuildSpeed(parseIntDefault(getTextFieldValue(buildingProductionTable, "BONUS_TROOPBUILDSPEED")));

        bi.setResistance(parseIntDefault(getTextFieldValue(buildingProductionTable, "BRIBERY_RESISTANCE")));
        bi.setAddedTradeRoutes(parseIntDefault(getTextFieldValue(buildingProductionTable, "TRADEROUTES")));
        bi.setIncomeOnTradeRoutes(parseIntDefault(getTextFieldValue(buildingProductionTable, "TRADEROUTES_BONUS")));
        bi.setShipRecycling(parseIntDefault(getTextFieldValue(buildingProductionTable, "SHIPRECYCLING")));
    }

    private String getRaceName(int buildingNumber) {
        ArrayMap<String, Major> majors = game.getRaceController().getMajors();
        for (int i = 0; i < majors.size; i++) {
            Major major = majors.getValueAt(i);
            if (buildingNumber == major.getRaceBuildingNumber())
                return major.getName();
        }
        return "Minor Race";
    }

    private String getTextFieldValue(Table table, String name) {
        return ((TextField) table.findActor(name)).getText();
    }

    @SuppressWarnings("rawtypes")
    private Object getSelectBoxValue(Table table, String name) {
        return ((SelectBox) table.findActor(name)).getSelected();
    }

    private boolean getCheckBoxValue(Table table, String name) {
        return ((CheckBox) table.findActor(name)).isChecked();
    }

    private int parseIntDefault(String text) {
        return parseIntDefault(text, 0);
    }

    private int parseIntDefault(String text, int def) {
        try {
            return Integer.parseInt(text);
        } catch (NumberFormatException e) {
            return def;
        }
    }

    private int getIdFromStringWithName(String val) {
        int res = 0;
        if (!val.isEmpty())
            res = Integer.parseInt(val.split(": ")[0]);
        return res;
    }

    private boolean notZeroAndLarger(int first, int second) {
        return first != 0 && second != 0 && second < first;
    }

    private boolean notZero(int first, int second) {
        return first != 0 && second != 0;
    }

    private Array<String> getBuildingsFromDifferentRaceWithSameProd(BuildingInfo bi, int race) {
        Array<String> candidates = new Array<String>();
        candidates.add("");
        for (int i = 0; i < game.getBuildingInfos().size; i++) {
            BuildingInfo other = game.getBuildingInfos().get(i);
            if (bi.getRunningNumber() != other.getRunningNumber()
                    && other.getOwnerOfBuilding() == race
                    && bi.getOwnerOfBuilding() != other.getOwnerOfBuilding()
                    && (notZero(bi.getFoodProd(), other.getFoodProd())
                            || notZero(bi.getIndustryPointsProd(), other.getIndustryPointsProd())
                            || notZero(bi.getEnergyProd(), other.getEnergyProd())
                            || notZero(bi.getSecurityPointsProd(), other.getSecurityPointsProd())
                            || notZero(bi.getResearchPointsProd(), other.getResearchPointsProd())
                            || notZero(bi.getTitanProd(), other.getTitanProd())
                            || notZero(bi.getDeuteriumProd(), other.getDeuteriumProd())
                            || notZero(bi.getDuraniumProd(), other.getDuraniumProd())
                            || notZero(bi.getCrystalProd(), other.getCrystalProd())
                            || notZero(bi.getIridiumProd(), other.getIridiumProd())
                            || notZero(bi.getDeritiumProd(), other.getDeritiumProd())
                            || notZero(bi.getFoodBonus(), other.getFoodBonus())
                            || notZero(bi.getIndustryBonus(), other.getIndustryBonus())
                            || notZero(bi.getEnergyBonus(), other.getEnergyBonus())
                            || notZero(bi.getSecurityBonus(), other.getSecurityBonus())
                            || notZero(bi.getResearchBonus(), other.getResearchBonus())
                            || notZero(bi.getTitanBonus(), other.getTitanBonus())
                            || notZero(bi.getDeuteriumBonus(), other.getDeuteriumBonus())
                            || notZero(bi.getDuraniumBonus(), other.getDuraniumBonus())
                            || notZero(bi.getCrystalBonus(), other.getCrystalBonus())
                            || notZero(bi.getIridiumBonus(), other.getIridiumBonus())
                            || notZero(bi.getDeritiumBonus(), other.getDeritiumBonus())
                            || notZero(bi.getCredits(), other.getCredits())
                            || notZero(bi.getCreditsBonus(), other.getCreditsBonus())
                            || notZero(bi.getMoraleProdEmpire(), other.getMoraleProdEmpire())
                            || notZero(bi.getMoraleProd(), other.getMoraleProd())
                            || notZero(bi.getAddedTradeRoutes(), other.getAddedTradeRoutes())
                            || notZero(bi.getAllResourceBonus(), other.getAllResourceBonus())
                            || notZero(bi.getBarrackSpeed(), other.getBarrackSpeed())
                            || notZero(bi.getBioTechBonus(), other.getBioTechBonus())
                            || notZero(bi.getBuildingBuildSpeed(), other.getBuildingBuildSpeed())
                            || notZero(bi.getComputerTechBonus(), other.getComputerTechBonus())
                            || notZero(bi.getConstructionTechBonus(), other.getConstructionTechBonus())
                            || notZero(bi.getEconomySabotageBonus(), other.getEconomySabotageBonus())
                            || notZero(bi.getEconomySpyBonus(), other.getEconomySpyBonus())
                            || notZero(bi.getEnergyTechBonus(), other.getEnergyTechBonus())
                            || notZero(bi.getGroundDefend(), other.getGroundDefend())
                            || notZero(bi.getGroundDefendBonus(), other.getGroundDefendBonus())
                            || notZero(bi.getHitPoints(), other.getHitPoints())
                            || notZero(bi.getIncomeOnTradeRoutes(), other.getIncomeOnTradeRoutes())
                            || notZero(bi.getInnerSecurityBonus(), other.getInnerSecurityBonus())
                            || notZero(bi.getMilitarySabogateBonus(), other.getMilitarySabogateBonus())
                            || notZero(bi.getMilitarySpyBonus(), other.getMilitarySpyBonus())
                            || notZero(bi.getPropulsionTechBonus(), other.getPropulsionTechBonus())
                            || notZero(bi.getResearchSabotageBonus(), other.getResearchSabotageBonus())
                            || notZero(bi.getResearchSpyBonus(), other.getResearchSpyBonus())
                            || notZero(bi.getResistance(), other.getResistance())
                            || notZero(bi.getScanPower(), other.getScanPower())
                            || notZero(bi.getScanPowerBonus(), other.getScanPowerBonus())
                            || notZero(bi.getScanRange(), other.getScanRange())
                            || notZero(bi.getScanRangeBonus(), other.getScanRangeBonus())
                            || notZero(bi.getShieldPower(), other.getShieldPower())
                            || notZero(bi.getShieldPowerBonus(), other.getShieldPowerBonus())
                            || notZero(bi.getShipBuildSpeed(), other.getShipBuildSpeed())
                            || notZero(bi.getShipDefend(), other.getShipDefend())
                            || notZero(bi.getShipDefendBonus(), other.getShipDefendBonus())
                            || notZero(bi.getShipRecycling(), other.getShipRecycling())
                            || notZero(bi.getShipTraining(), other.getShipTraining())
                            || notZero(bi.getShipYardSpeed(), other.getShipYardSpeed())
                            || notZero(bi.getTroopBuildSpeed(), other.getTroopBuildSpeed())
                            || notZero(bi.getTroopTraining(), other.getTroopTraining())
                            || notZero(bi.getUpdateBuildSpeed(), other.getUpdateBuildSpeed()) || notZero(bi.getWeaponTechBonus(),
                                other.getWeaponTechBonus()))) {
                candidates.add(other.getRunningNumber() + ": " + localeBuildingInfos.get(selectedLanguage).get(i).getBuildingName());
            }
        }
        return candidates;
    }

    private Array<String> getBuildingsFromTheSameProdTypeAndRace(BuildingInfo bi) {
        Array<String> candidates = new Array<String>();
        candidates.add("");
        for (int i = 0; i < game.getBuildingInfos().size; i++) {
            BuildingInfo other = game.getBuildingInfos().get(i);
            if (bi.getRunningNumber() != other.getRunningNumber()
                    && bi.getOwnerOfBuilding() == other.getOwnerOfBuilding()
                    && other.isUpgradeable()
                    && (notZeroAndLarger(bi.getFoodProd(), other.getFoodProd())
                            || notZeroAndLarger(bi.getIndustryPointsProd(), other.getIndustryPointsProd())
                            || notZeroAndLarger(bi.getEnergyProd(), other.getEnergyProd())
                            || notZeroAndLarger(bi.getSecurityPointsProd(), other.getSecurityPointsProd())
                            || notZeroAndLarger(bi.getResearchPointsProd(), other.getResearchPointsProd())
                            || notZeroAndLarger(bi.getTitanProd(), other.getTitanProd())
                            || notZeroAndLarger(bi.getDeuteriumProd(), other.getDeuteriumProd())
                            || notZeroAndLarger(bi.getDuraniumProd(), other.getDuraniumProd())
                            || notZeroAndLarger(bi.getCrystalProd(), other.getCrystalProd())
                            || notZeroAndLarger(bi.getIridiumProd(), other.getIridiumProd())
                            || notZeroAndLarger(bi.getDeritiumProd(), other.getDeritiumProd())
                            || notZeroAndLarger(bi.getFoodBonus(), other.getFoodBonus())
                            || notZeroAndLarger(bi.getIndustryBonus(), other.getIndustryBonus())
                            || notZeroAndLarger(bi.getEnergyBonus(), other.getEnergyBonus())
                            || notZeroAndLarger(bi.getSecurityBonus(), other.getSecurityBonus())
                            || notZeroAndLarger(bi.getResearchBonus(), other.getResearchBonus())
                            || notZeroAndLarger(bi.getTitanBonus(), other.getTitanBonus())
                            || notZeroAndLarger(bi.getDeuteriumBonus(), other.getDeuteriumBonus())
                            || notZeroAndLarger(bi.getDuraniumBonus(), other.getDuraniumBonus())
                            || notZeroAndLarger(bi.getCrystalBonus(), other.getCrystalBonus())
                            || notZeroAndLarger(bi.getIridiumBonus(), other.getIridiumBonus())
                            || notZeroAndLarger(bi.getDeritiumBonus(), other.getDeritiumBonus())
                            || notZeroAndLarger(bi.getCredits(), other.getCredits())
                            || notZeroAndLarger(bi.getCreditsBonus(), other.getCreditsBonus())
                            || notZeroAndLarger(bi.getMoraleProdEmpire(), other.getMoraleProdEmpire())
                            || notZeroAndLarger(bi.getMoraleProd(), other.getMoraleProd())
                            || notZeroAndLarger(bi.getAddedTradeRoutes(), other.getAddedTradeRoutes())
                            || notZeroAndLarger(bi.getAllResourceBonus(), other.getAllResourceBonus())
                            || notZeroAndLarger(bi.getBarrackSpeed(), other.getBarrackSpeed())
                            || notZeroAndLarger(bi.getBioTechBonus(), other.getBioTechBonus())
                            || notZeroAndLarger(bi.getBuildingBuildSpeed(), other.getBuildingBuildSpeed())
                            || notZeroAndLarger(bi.getComputerTechBonus(), other.getComputerTechBonus())
                            || notZeroAndLarger(bi.getConstructionTechBonus(), other.getConstructionTechBonus())
                            || notZeroAndLarger(bi.getEconomySabotageBonus(), other.getEconomySabotageBonus())
                            || notZeroAndLarger(bi.getEconomySpyBonus(), other.getEconomySpyBonus())
                            || notZeroAndLarger(bi.getEnergyTechBonus(), other.getEnergyTechBonus())
                            || notZeroAndLarger(bi.getGroundDefend(), other.getGroundDefend())
                            || notZeroAndLarger(bi.getGroundDefendBonus(), other.getGroundDefendBonus())
                            || notZeroAndLarger(bi.getHitPoints(), other.getHitPoints())
                            || notZeroAndLarger(bi.getIncomeOnTradeRoutes(), other.getIncomeOnTradeRoutes())
                            || notZeroAndLarger(bi.getInnerSecurityBonus(), other.getInnerSecurityBonus())
                            || notZeroAndLarger(bi.getMilitarySabogateBonus(), other.getMilitarySabogateBonus())
                            || notZeroAndLarger(bi.getMilitarySpyBonus(), other.getMilitarySpyBonus())
                            || notZeroAndLarger(bi.getPropulsionTechBonus(), other.getPropulsionTechBonus())
                            || notZeroAndLarger(bi.getResearchSabotageBonus(), other.getResearchSabotageBonus())
                            || notZeroAndLarger(bi.getResearchSpyBonus(), other.getResearchSpyBonus())
                            || notZeroAndLarger(bi.getResistance(), other.getResistance())
                            || notZeroAndLarger(bi.getScanPower(), other.getScanPower())
                            || notZeroAndLarger(bi.getScanPowerBonus(), other.getScanPowerBonus())
                            || notZeroAndLarger(bi.getScanRange(), other.getScanRange())
                            || notZeroAndLarger(bi.getScanRangeBonus(), other.getScanRangeBonus())
                            || notZeroAndLarger(bi.getShieldPower(), other.getShieldPower())
                            || notZeroAndLarger(bi.getShieldPowerBonus(), other.getShieldPowerBonus())
                            || notZeroAndLarger(bi.getShipBuildSpeed(), other.getShipBuildSpeed())
                            || notZeroAndLarger(bi.getShipDefend(), other.getShipDefend())
                            || notZeroAndLarger(bi.getShipDefendBonus(), other.getShipDefendBonus())
                            || notZeroAndLarger(bi.getShipRecycling(), other.getShipRecycling())
                            || notZeroAndLarger(bi.getShipTraining(), other.getShipTraining())
                            || notZeroAndLarger(bi.getShipYardSpeed(), other.getShipYardSpeed())
                            || notZeroAndLarger(bi.getTroopBuildSpeed(), other.getTroopBuildSpeed())
                            || notZeroAndLarger(bi.getTroopTraining(), other.getTroopTraining())
                            || notZeroAndLarger(bi.getUpdateBuildSpeed(), other.getUpdateBuildSpeed()) || notZeroAndLarger(
                                bi.getWeaponTechBonus(), other.getWeaponTechBonus()))) {
                candidates.add(other.getRunningNumber() + ": " + localeBuildingInfos.get(selectedLanguage).get(i).getBuildingName());
            }
        }
        return candidates;
    }

    public void hide() {
        visible = false;
        buildingFilter.setVisible(false);
        buildingScroller.setVisible(false);
        mainButtonTable.setVisible(false);
        buildingInfoTable.setVisible(false);
        buildingProductionTable.setVisible(false);
        selectButtonTable.setVisible(false);
    }

    private void markBuildingListSelected(Button b) {
        if (buildingSelection != null) {
            buildingSelection.getStyle().up = null;
            buildingSelection.getStyle().down = null;
            ((Label) buildingSelection.getUserObject()).setColor(oldColor);
            if (!loadedTexture.isEmpty() && game.getAssetManager().isLoaded(loadedTexture) && selectedItem != getIndex(b)) {
                game.getAssetManager().unload(loadedTexture);
                loadedTexture = "";
            }
        }
        if (b == null)
            b = buildingSelection;
        Image itemSelection = new Image(selectTexture);
        b.getStyle().up = itemSelection.getDrawable();
        b.getStyle().down = itemSelection.getDrawable();
        oldColor = new Color(((Label) b.getUserObject()).getColor());
        ((Label) b.getUserObject()).setColor(markColor);
        buildingSelection = b;
        selectedItem = getIndex(b);
        float buttonPos = b.getHeight() * selectedItem;
        float scrollerHeight = buildingScroller.getScrollHeight();
        float scrollerPos = buildingScroller.getScrollY();
        if (buttonPos + b.getHeight() / 2 - scrollerPos < 0) {
            buildingScroller.setScrollY(b.getHeight() * selectedItem - buildingScroller.getScrollHeight() * 2);
        } else if (buttonPos + b.getHeight() / 2 - scrollerPos > scrollerHeight) {
            buildingScroller.setScrollY(b.getHeight() * (selectedItem - buildingScroller.getScrollHeight() / b.getHeight() + 1));
        }
    }

    private int getIndex(Button b) {
        return (Integer) (((Label) b.getUserObject()).getUserObject());
    }

    private void updateBuildingList(Array<BuildingInfo> infos) {
        for (int i = 0; i < infos.size; i++) {
            BuildingInfo bi = infos.get(i);
            if (bi.getPredecessorId() != 0)
                for (int j = 0; j < infos.size; j++)
                    if (bi.getPredecessorId() == infos.get(j).getRunningNumber()) {
                        bi.setPredecessorId(j + 1);
                        break;
                    }
            for (int k = 0; k < game.getRaceController().getMajors().size; k++) {
                if (bi.getBuildingEquivalent(k) != 0) {
                    for (int j = 0; j < infos.size; j++) {
                        if (bi.getBuildingEquivalent(k) == infos.get(j).getRunningNumber()) {
                            bi.setBuildingEquivalent(k, j + 1);
                            break;
                        }
                    }
                }
            }
            if (bi.getMaxInSystem().number != 0)
                for (int j = 0; j < infos.size; j++)
                    if (bi.getMaxInSystem().runningNumber == infos.get(j).getRunningNumber()) {
                        bi.setMaxInSystem(bi.getMaxInSystem().number, j + 1);
                        break;
                    }
            if (bi.getMaxInEmpire() != 0)
                for (int j = 0; j < infos.size; j++)
                    if (bi.getMaxInEmpireID() == infos.get(j).getRunningNumber()) {
                        bi.setMaxInEmpire(bi.getMaxInEmpire(), j + 1);
                        break;
                    }
            if (bi.getMinInSystem().number != 0)
                for (int j = 0; j < infos.size; j++)
                    if (bi.getMinInSystem().runningNumber == infos.get(j).getRunningNumber()) {
                        bi.setMinInSystem(bi.getMinInSystem().number, j + 1);
                        break;
                    }
            if (bi.getMinInEmpire().number != 0)
                for (int j = 0; j < infos.size; j++)
                    if (bi.getMinInEmpire().runningNumber == infos.get(j).getRunningNumber()) {
                        bi.setMinInEmpire(bi.getMinInEmpire().number, j + 1);
                        break;
                    }
        }
        for (int i = 0; i < infos.size; i++) {
            BuildingInfo bi = infos.get(i);
            bi.setRunningNumber(i + 1);
        }
    }

    private void drawSelectButtonTable() {
        selectButtonTable.clear();
        TextButton btn = new TextButton("Prerequisites", styleSmall);
        btn.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                state = BuildingEditorState.StateGeneral;
                show();
            }
        });
        selectButtonTable.add(btn).width(GameConstants.wToRelative(150)).height(GameConstants.hToRelative(35))
                .spaceRight(GameConstants.wToRelative(10));

        btn = new TextButton("Production", styleSmall);
        btn.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                state = BuildingEditorState.StateProduction;
                show();
            }
        });
        selectButtonTable.add(btn).width(GameConstants.wToRelative(150)).height(GameConstants.hToRelative(35))
                .spaceRight(GameConstants.wToRelative(10));

        selectButtonTable.setVisible(true);
    }
    private void drawMainButtonTable() {
        mainButtonTable.clear();
        TextButton addBtn = new TextButton("Add", styleSmall);
        addBtn.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                BuildingInfo bi = new BuildingInfo();
                bi.setOwnerOfBuilding(1);
                bi.setRunningNumber(game.getBuildingInfos().size + 1);
                game.getBuildingInfos().add(bi);
                for (String lang : GameConstants.getSupportedLanguages())
                    localeBuildingInfos.get(lang).add(bi);
                selectedItem = game.getBuildingInfos().size - 1;
                show();
            }
        });
        mainButtonTable.add(addBtn).width(GameConstants.wToRelative(150)).height(GameConstants.hToRelative(35))
                .spaceRight(GameConstants.wToRelative(5));

        TextButton copyBtn = new TextButton("Copy", styleSmall);
        copyBtn.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                BuildingInfo bi = new BuildingInfo(game.getBuildingInfos().get(selectedItem));
                bi.setRunningNumber(game.getBuildingInfos().size + 1);
                game.getBuildingInfos().add(bi);
                for (String lang : GameConstants.getSupportedLanguages()) {
                    BuildingInfo localBi = new BuildingInfo(localeBuildingInfos.get(lang).get(selectedItem));
                    localBi.setRunningNumber(bi.getRunningNumber());
                    localeBuildingInfos.get(lang).add(localBi);
                }
                selectedItem = game.getBuildingInfos().size - 1;
                show();
            }
        });
        mainButtonTable.add(copyBtn).width(GameConstants.wToRelative(150)).height(GameConstants.hToRelative(35))
                .spaceRight(GameConstants.wToRelative(5));

        TextButton removeBtn = new TextButton("Remove", styleSmall);
        removeBtn.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                game.getBuildingInfos().removeIndex(selectedItem);
                for (String lang : GameConstants.getSupportedLanguages()) {
                    localeBuildingInfos.get(lang).removeIndex(selectedItem);
                }
                for (String lang : GameConstants.getSupportedLanguages()) {
                    updateBuildingList(localeBuildingInfos.get(lang));
                }
                updateBuildingList(game.getBuildingInfos());
                show();
            }
        });
        mainButtonTable.add(removeBtn).width(GameConstants.wToRelative(150)).height(GameConstants.hToRelative(35))
                .spaceRight(GameConstants.wToRelative(5));

        TextButton buttonUpdate = new TextButton("Update", styleSmall);
        buttonUpdate.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                saveBuildingInfo();
                int newIdx = selectedBuildingInfo.getRunningNumber() - 1;
                game.getBuildingInfos().removeIndex(selectedItem);
                BuildingInfo bi = new BuildingInfo(selectedBuildingInfo);
                bi.setRunningNumber(selectedItem + 1);
                game.getBuildingInfos().insert(newIdx != selectedItem ? newIdx : selectedItem, bi);

                localeBuildingInfos.get(selectedLanguage).removeIndex(selectedItem);
                bi = new BuildingInfo(localeBuildingInfo);
                bi.setRunningNumber(selectedItem + 1);
                localeBuildingInfos.get(selectedLanguage).insert(newIdx != selectedItem ? newIdx : selectedItem, bi);
                if (newIdx != selectedItem) {
                    for (String lang : GameConstants.getSupportedLanguages()) {
                        if (!lang.equals(selectedLanguage)) {
                            bi = localeBuildingInfos.get(lang).removeIndex(selectedItem);
                            bi.setRunningNumber(selectedItem + 1);
                            localeBuildingInfos.get(lang).insert(newIdx != selectedItem ? newIdx : selectedItem, bi);
                        }
                    }
                    for (String lang : GameConstants.getSupportedLanguages()) {
                        updateBuildingList(localeBuildingInfos.get(lang));
                    }
                    updateBuildingList(game.getBuildingInfos());
                }
                show();
            }
        });
        mainButtonTable.add(buttonUpdate).width(GameConstants.wToRelative(150)).height(GameConstants.hToRelative(35))
                .spaceRight(GameConstants.wToRelative(5));

        TextButton buttonSave = new TextButton("Save", styleSmall);
        buttonSave.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                saveBuildingInfosToFile();
            }
        });
        mainButtonTable.add(buttonSave).width(GameConstants.wToRelative(150)).height(GameConstants.hToRelative(35))
                .spaceRight(GameConstants.wToRelative(5));

        SelectBox<String> languageSelect = new SelectBox<String>(skin);
        languageSelect.setItems(GameConstants.getSupportedLanguages());
        languageSelect.setSelected(selectedLanguage);
        languageSelect.addListener(new ChangeListener() {
            @SuppressWarnings("unchecked")
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                selectedLanguage = ((SelectBox<String>) actor).getSelected();
                show();
            }
        });
        mainButtonTable.add(languageSelect).width(GameConstants.wToRelative(100)).height(GameConstants.hToRelative(35))
                .spaceRight(GameConstants.wToRelative(5));

        TextButton buttonBack = new TextButton(StringDB.getString("BTN_BACK", true), styleSmall);
        buttonBack.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                ((ResourceEditor) game.getScreen()).showMainMenu();
            }
        });
        mainButtonTable.add(buttonBack).width(GameConstants.wToRelative(150)).height(GameConstants.hToRelative(35))
                .spaceLeft(GameConstants.wToRelative(5)).align(Align.right).expand();
        mainButtonTable.setVisible(true);
    }

    public boolean isVisible() {
        return visible;
    }
}