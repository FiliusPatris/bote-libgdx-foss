/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.general;

import com.blotunga.bote.constants.SndMgrValue;

class SndMgrMessageEntry implements Comparable<SndMgrMessageEntry> {
    public SndMgrValue message;
    public String race;
    public int priority;
    public float volume;

    public SndMgrMessageEntry(SndMgrValue message, String race, int priority, float volume) {
        this.message = message;
        this.race = race;
        this.priority = priority;
        this.volume = volume;
    }

    @Override
    public int compareTo(SndMgrMessageEntry o) {
        return (priority < o.priority) ? -1 : ((priority == o.priority) ? 0 : 1);
    }
}