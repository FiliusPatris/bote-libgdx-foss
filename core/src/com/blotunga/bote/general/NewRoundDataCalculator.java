/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.general;

import java.util.Iterator;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.ObjectMap.Entry;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.DiplomaticAgreement;
import com.blotunga.bote.constants.ResearchComplexType;
import com.blotunga.bote.constants.ResearchStatus;
import com.blotunga.bote.galaxy.Sector;
import com.blotunga.bote.galaxy.MapTile.DiscoverStatus;
import com.blotunga.bote.general.EmpireNews.EmpireNewsType;
import com.blotunga.bote.intel.Intelligence;
import com.blotunga.bote.races.Empire;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.races.Minor;
import com.blotunga.bote.races.Race;
import com.blotunga.bote.races.ResearchComplex;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.starsystem.SystemProd;
import com.blotunga.bote.troops.TroopInfo;
import com.blotunga.bote.utils.IntPoint;
import com.blotunga.bote.utils.RandUtil;

public class NewRoundDataCalculator {
    private ResourceManager res;

    public NewRoundDataCalculator(ScreenManager res) {
        this.res = res;
    }

    public static void getIntelligenceBoniFromSpecialTechsAndSetThem(ArrayMap<String, Major> majors) {
        for (Iterator<Entry<String, Major>> iter = majors.entries().iterator(); iter.hasNext();) {
            Entry<String, Major> e = iter.next();
            Intelligence intel = e.value.getEmpire().getIntelligence();
            ResearchComplex researchComplex = e.value.getEmpire().getResearch().getResearchInfo()
                    .getResearchComplex(ResearchComplexType.SECURITY);
            ///SPECIAL RESEARCH///
            if (researchComplex.getFieldStatus(1) == ResearchStatus.RESEARCHED)
                intel.addInnerSecurityBonus(researchComplex.getBonus(1));
            else if (researchComplex.getFieldStatus(2) == ResearchStatus.RESEARCHED) {
                intel.addEconomyBonus(researchComplex.getBonus(2), 1);
                intel.addMilitaryBonus(researchComplex.getBonus(2), 1);
                intel.addScienceBonus(researchComplex.getBonus(2), 1);
            } else if (researchComplex.getFieldStatus(3) == ResearchStatus.RESEARCHED) {
                intel.addEconomyBonus(researchComplex.getBonus(3), 0);
                intel.addMilitaryBonus(researchComplex.getBonus(3), 0);
                intel.addScienceBonus(researchComplex.getBonus(3), 0);
            }
        }
    }

    private void emitLostRouteMEssage(int deletedRoutes, String singleKey, String multiKey, String sectorname,
            IntPoint coord, Empire empire) {
        String news;
        if (deletedRoutes == 1)
            news = StringDB.getString(singleKey, false, sectorname);
        else {
            String lost = String.format("%d", deletedRoutes);
            news = StringDB.getString(multiKey, false, lost, sectorname);
        }
        EmpireNews message = new EmpireNews();
        message.CreateNews(news, EmpireNewsType.ECONOMY, "", coord, false, 4);
        empire.addMsg(message);
    }

    //Function checks, whether trade and resource routes starting in this system are still valid.
    //If not, removes them and queues messages about that.
    public void checkRoutes(StarSystem system, Major major) {
        Empire empire = major.getEmpire();
        IntPoint coord = system.getCoordinates();
        String sectorname = system.getName();
        boolean selectEmpireScreen = false;
        int deletedTradeRoutes = 0;
        deletedTradeRoutes += system.checkTradeRoutesDiplomacy();
        deletedTradeRoutes += system.checkTradeRoutes(empire.getResearch().getResearchInfo());
        if (deletedTradeRoutes > 0) {
            selectEmpireScreen = true;
            emitLostRouteMEssage(deletedTradeRoutes, "LOST_ONE_TRADEROUTE", "LOST_TRADEROUTES", sectorname, coord,
                    empire);
        }
        int deletedResourceRoutes = 0;
        deletedResourceRoutes += system.checkResourceRoutesExistence();
        deletedResourceRoutes += system.checkResourceRoutes(empire.getResearch().getResearchInfo());
        if (deletedResourceRoutes > 0) {
            selectEmpireScreen = true;
            emitLostRouteMEssage(deletedResourceRoutes, "LOST_ONE_RESOURCEROUTE", "LOST_RESOURCEROUTES", sectorname,
                    coord, empire);
        }

        if (selectEmpireScreen)
            res.getClientWorker().setEmpireViewFor(major);
    }

    public void calcIntelligenceBonus(SystemProd production, Intelligence intel) {
        intel.addInnerSecurityBonus(production.getInnerSecurityBonus());
        intel.addEconomyBonus(production.getEconomySpyBonus(), 0);
        intel.addEconomyBonus(production.getEconomySabotageBonus(), 1);
        intel.addScienceBonus(production.getResearchSpyBonus(), 0);
        intel.addScienceBonus(production.getResearchSabotageBonus(), 1);
        intel.addMilitaryBonus(production.getMilitarySpyBonus(), 0);
        intel.addMilitaryBonus(production.getMilitarySabotageBonus(), 1);
    }

    public void calcMorale(StarSystem system, Array<TroopInfo> troopInfo) {
        //if the system was conquered the morale is reduced each round a bit
        if (system.isTaken() && system.getMorale() > 70)
            system.setMorale(-((int) (RandUtil.random() * 2)));
        system.includeTroopMoraleValue(troopInfo);
    }

    //Function calculates possible effects from sectors or systems which are iterated upon later
    //onto earlier sectors or systems. Such as moral empire wide, which influences
    //all systems starting from (0,0).
    //These effects have effects onto other things which are calculated later,
    //the second time it is iterated over all sectors/systems (after CalcPreLoop())
    public void calcPreLoop() {
        //reset morale
        for (Iterator<Entry<String, Major>> iter = res.getRaceController().getMajors().entries().iterator(); iter
                .hasNext();) {
            Entry<String, Major> e = iter.next();
            e.value.getEmpire().setMoraleEmpireWide(0);
        }
        //calc new values
        for (int i = 0; i < res.getUniverseMap().getStarSystems().size; i++) {
            StarSystem system = res.getUniverseMap().getStarSystems().get(i);
            if (system.isSunSystem()) {
                boolean systemOnwerExists = system.isMajorized();
                if (systemOnwerExists) {
                    system.calculateEmpireWideMoraleProd(res.getBuildingInfos());
                }
                if (systemOnwerExists || (system.getMinorRace() != null)) {
                    Race owner = system.getOwner();
                    SystemProd production = system.getProduction();
                    int scanPower = production.getScanPower();
                    if (scanPower > 0)
                        system.putScannedSquare(production.getScanRange(), scanPower, owner);
                }
            }
        }
    }

    private class SectorSettings {
        public int scanpower;
        public boolean scanned;
        public boolean known;
        public boolean port;

        public SectorSettings(Sector sector, String race) {
            scanpower = sector.getScanPower(race);
            scanned = sector.getScanned(race);
            known = sector.getKnown(race);
            port = sector.getShipPort(race);
        }

        public SectorSettings(SectorSettings other) {
            scanpower = other.scanpower;
            scanned = other.scanned;
            known = other.known;
            port = other.port;
        }

        public boolean isSmaller(SectorSettings o) {
            return scanpower < o.scanpower || (!scanned && o.scanned) || (!known && o.known) || (!port && o.port);
        }
    }

    private ObjectMap<String, SectorSettings> giveDiploGoodies(Sector sector, DiplomaticAgreement agreement, String to,
            String from, boolean minorPort, ObjectMap<String, SectorSettings> newSectorSettings) {
        SectorSettings settings = newSectorSettings.get(to, new SectorSettings(sector, to));
        SectorSettings oldSettings = new SectorSettings(settings);

        DiscoverStatus fromDiscStatus = sector.getDiscoverStatus(from);
        if (agreement.getType() > DiplomaticAgreement.ALLIANCE.getType())
            settings.scanpower = Math.max(settings.scanpower, sector.getScanPower(from, false));

        if (fromDiscStatus.getValue() >= DiscoverStatus.DISCOVER_STATUS_KNOWN.getValue()) {
            if (agreement.getType() >= DiplomaticAgreement.FRIENDSHIP.getType())
                settings.scanned = true;
            if (agreement.getType() >= DiplomaticAgreement.COOPERATION.getType())
                settings.known = true;
        } else if (fromDiscStatus.getValue() >= DiscoverStatus.DISCOVER_STATUS_SCANNED.getValue())
            if (agreement.getType() >= DiplomaticAgreement.COOPERATION.getType())
                settings.scanned = true;

        if (sector.getOwnerId().equals(from)) {
            if (agreement.getType() >= DiplomaticAgreement.TRADE.getType())
                settings.scanned = true;
            if (agreement.getType() >= DiplomaticAgreement.FRIENDSHIP.getType())
                settings.known = true;
        }
        if (minorPort || sector.getShipPort(from))
            if (agreement.getType() >= DiplomaticAgreement.COOPERATION.getType())
                settings.port = true;

        if (oldSettings.isSmaller(settings))
            newSectorSettings.put(to, settings);
        return newSectorSettings;
    }

    private ObjectMap<String, SectorSettings> giveDiploGoodiesMajorOrMinor(Sector sector, Race left, Race right,
            boolean isMinor, ObjectMap<String, SectorSettings> newSectorSettings) {
        String rightID = right.getRaceId();
        DiplomaticAgreement agreement = left.getAgreement(rightID);
        if (agreement.getType() < DiplomaticAgreement.TRADE.getType())
            return newSectorSettings;
        String leftID = left.getRaceId();
        if (isMinor) {
            Minor minor = Minor.toMinor(right);
            newSectorSettings = giveDiploGoodies(sector, agreement, leftID, rightID,
                    sector.getCoordinates().equals(minor.getCoordinates()) && minor.getSpaceFlightNation(),
                    newSectorSettings);
        } else {
            newSectorSettings = giveDiploGoodies(sector, agreement, leftID, rightID, false, newSectorSettings);
            newSectorSettings = giveDiploGoodies(sector, agreement, rightID, leftID, false, newSectorSettings);
        }
        return newSectorSettings;
    }

    public void calcExtraVisibilityAndRangeDueToDiplomacy(Sector sector, ArrayMap<String, Major> majors,
            ArrayMap<String, Minor> minors) {
        ObjectMap<String, SectorSettings> newSectorSettings = new ObjectMap<String, NewRoundDataCalculator.SectorSettings>();
        for (int i = 0; i < majors.size; i++) {
            Major major = majors.getValueAt(i);
            for (int k = 0; k < minors.size; k++) {
                Minor minor = minors.getValueAt(k);
                newSectorSettings = giveDiploGoodiesMajorOrMinor(sector, major, minor, true, newSectorSettings);
            }
            for (int j = i; j < majors.size; j++) {
                newSectorSettings = giveDiploGoodiesMajorOrMinor(sector, major, majors.getValueAt(j), false,
                        newSectorSettings);
            }
        }

        for (ObjectMap.Entries<String, SectorSettings> iter = newSectorSettings.entries().iterator(); iter.hasNext();) {
            ObjectMap.Entry<String, SectorSettings> e = iter.next();
            String ID = e.key;
            SectorSettings settings = e.value;
            if (settings.scanned)
                sector.setScanned(ID);
            if (settings.known)
                sector.setKnown(ID);
            sector.setScanPower(settings.scanpower, ID);
            sector.setShipPort(settings.port, ID);
        }
    }
}
