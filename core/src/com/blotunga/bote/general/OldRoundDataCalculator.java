/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.general;

import com.badlogic.gdx.utils.Array;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.DiplomaticAgreement;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.ResearchComplexType;
import com.blotunga.bote.constants.ResearchStatus;
import com.blotunga.bote.constants.SystemOwningStatus;
import com.blotunga.bote.general.EmpireNews.EmpireNewsType;
import com.blotunga.bote.races.Empire;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.races.Minor;
import com.blotunga.bote.starsystem.AssemblyList;
import com.blotunga.bote.starsystem.BuildingInfo;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.starsystem.SystemManager;
import com.blotunga.bote.starsystem.SystemProd;
import com.blotunga.bote.utils.IntPoint;
import com.blotunga.bote.utils.Pair;
import com.blotunga.bote.utils.RandUtil;

public class OldRoundDataCalculator {
    private ScreenManager res;

    public OldRoundDataCalculator(ScreenManager res) {
        this.res = res;
    }

    public void creditsDestructionMoral(Major major, StarSystem system, Array<BuildingInfo> buildingInfos,
            float difficultyLevel) {
        SystemProd prod = system.getProduction();
        Empire empire = major.getEmpire();
        //the computer gets more credits
        if (!major.aHumanPlays())
            empire.setCredits((int) (prod.getCreditsProd() / difficultyLevel));
        else
            empire.setCredits(prod.getCreditsProd());

        //delete buildings which were marked
        if (system.destroyBuildings())
            system.calculateNumberOfWorkBuildings(buildingInfos);

        // Calculate variables before the planet grows, and put these in storage if someone owns it
        // calculateStorages() calculates also the morale, if we have a NeverReady in queue (like martial law) and this produces morale
        // then add these to the morale
        int list = system.getAssemblyList().getAssemblyListEntry(0).id;
        if (list > 0 && list < 10000 && buildingInfos.get(list - 1).getNeverReady() && system.getMorale() <= 85)
            prod.addMoraleProd(buildingInfos.get(list - 1).getMoraleProd());
    }

    public static int deritiumForTheAI(boolean human, StarSystem system, float difficultyLevel) {
        int dAdd = 0;
        if (!human && system.getProduction().getDeritiumProd() == 0) {
            //the higher the difficulty, the higher the chance that the AI gets more deritium
            int temp = (int) (RandUtil.random() * (difficultyLevel * 7.5));
            if (temp == 0)
                dAdd = 1;
        }
        return dAdd;
    }

    public void executeRebellion(StarSystem system, Major major) {
        Empire empire = major.getEmpire();
        IntPoint coord = system.getCoordinates();
        String sectorName = system.getName();

        String news = StringDB.getString("REBELLION_IN_SYSTEM", false, sectorName);
        EmpireNews message = new EmpireNews();
        message.CreateNews(news, EmpireNewsType.SOMETHING, "", coord);
        empire.addMsg(message);
        res.getClientWorker().setEmpireViewFor(major);

        message = new EmpireNews();
        message.CreateNews(major.getMoraleObserver().addEvent(18, major.getRaceMoralNumber(), sectorName),
                EmpireNewsType.SOMETHING, "", coord);
        empire.addMsg(message);

        Minor minor = system.getMinorRace();
        if (minor != null) {
            if (!system.isTaken()) {
                minor.setAgreement(major.getRaceId(), DiplomaticAgreement.NONE);
                major.setAgreement(minor.getRaceId(), DiplomaticAgreement.NONE);

                minor.setRelation(major.getRaceId(), (-1) * ((int) (RandUtil.random() * 50 + 20)));
                news = StringDB.getString("MINOR_CANCELS_MEMBERSHIP", false, minor.getName());
                message = new EmpireNews();
                message.CreateNews(news, EmpireNewsType.DIPLOMACY, "", coord);
                empire.addMsg(message);
            }
            minor.setOwner("");
            minor.setSubjugated(false);
            system.changeOwner(minor.getRaceId(), SystemOwningStatus.OWNING_STATUS_INDEPENDENT_MINOR);
        } else
            system.changeOwner("", SystemOwningStatus.OWNING_STATUS_REBELLED);

        //reset important variables
        system.setShipPort(false, major.getRaceId());
        system.getTroops().clear();
    }

    public void executeFamine(StarSystem system, Major major) {
        IntPoint coord = system.getCoordinates();
        Empire empire = major.getEmpire();
        system.letPlanetsShrink((system.getFoodStore()) * 0.01f);
        //if the morale is over 50 it will be reduced by famine
        if (system.getMorale() > 50)
            system.setMorale((int) (system.getFoodStore() / (system.getInhabitants() + 1))); //+1 to avoid div by 0
        system.setFoodStore(0);

        String news = StringDB.getString("FAMINE", false, system.getName());
        EmpireNews message = new EmpireNews();
        message.CreateNews(news, EmpireNewsType.SOMETHING, "", coord, false, 1);
        empire.addMsg(message);
        res.getClientWorker().setEmpireViewFor(major);
    }

    public void hanldePopulationEffects(StarSystem system, Major major) {
        float currentHabitants = system.getCurrentInhabitants();
        Empire empire = major.getEmpire();
        empire.addPopSupportCosts((int) (currentHabitants * GameConstants.POPSUPPORT_MULTI));
        //function returns true if a new resource route can be established
        if (system.setInhabitants(currentHabitants)) {
            //if the special research "minimum 1 trade route" was researched, show this only at the second route
            boolean minOneRoute = empire.getResearch().getResearchInfo().getResearchComplex(ResearchComplexType.TRADE)
                    .getFieldStatus(3) == ResearchStatus.RESEARCHED;
            if (minOneRoute == false || (int) (system.getInhabitants() / GameConstants.TRADEROUTEHAB) > 1)
                systemMessage(system, major, "ENOUGH_HABITANTS_FOR_NEW_TRADEROUTE", EmpireNewsType.ECONOMY, 4);
        }
    }

    public void systemMessage(StarSystem system, Major major, String key, EmpireNewsType type, int flag) {
        String news = StringDB.getString(key, false, system.getName());
        EmpireNews message = new EmpireNews();
        message.CreateNews(news, type, "", system.getCoordinates(), false, flag);
        major.getEmpire().addMsg(message);
        res.getClientWorker().setEmpireViewFor(major);
    }

    private void militaryMessage(StarSystem system, Major major, String key, String objectName) {
        String s = StringDB.getString(key, false, objectName, system.getName());
        EmpireNews message = new EmpireNews();
        message.CreateNews(s, EmpireNewsType.MILITARY, system.getName(), system.getCoordinates());
        major.getEmpire().addMsg(message);
    }

    private void creditsIfBought(AssemblyList assemblyList, int ipProd, Empire empire) {
        //if the building was bought, then add the build costs to the empire's credits, this value can't be higher than the build cost
        if (assemblyList.getWasBuildingBought()) {
            int goods = ipProd;
            if (goods > assemblyList.getBuildCosts())
                goods = assemblyList.getBuildCosts();
            empire.setCredits(goods);
            assemblyList.setWasBuildingBought(false);
        }
    }

    private void finishBuild(int toBuild, StarSystem system, Major major, Array<BuildingInfo> buildingInfos) {
        int list = toBuild;
        IntPoint coord = system.getCoordinates();
        //check if building of upgrade
        if (list > 0 && list < 10000 && !buildingInfos.get(list - 1).getNeverReady()) { //a building was built
            EmpireNews message = new EmpireNews();
            message.CreateNews(buildingInfos.get(list - 1).getBuildingName(), EmpireNewsType.ECONOMY, system.getName(),
                    coord);
            major.getEmpire().addMsg(message);
            //build building
            res.getUniverseMap().buildBuilding(list, coord);
            //if there are enough workers get the building online
            int checkValue = system.setNewBuildingOnline(buildingInfos);
            //generate message that the building couldn't be brought online
            SystemManager manager = system.getManager();
            if (checkValue == 1 && !manager.isActive())
                systemMessage(system, major, "NOT_ENOUGH_WORKER", EmpireNewsType.SOMETHING, 1);
            else if (checkValue == 2 && !manager.isActive())
                systemMessage(system, major, "NOT_ENOUGH_ENERGY", EmpireNewsType.SOMETHING, 2);
        } else if (list < 0) { // an upgrade was completed
            list *= (-1);
            EmpireNews message = new EmpireNews();
            message.CreateNews(buildingInfos.get(list - 1).getBuildingName(), EmpireNewsType.ECONOMY, system.getName(),
                    coord, true);
            major.getEmpire().addMsg(message);
            //get predecessors, buildings with runningNumber = predecessorID will be deleted by updateBuildings()
            int predecessorID = buildingInfos.get(list - 1).getPredecessorId();
            BuildingInfo predecessorBuilding = buildingInfos.get(predecessorID - 1);
            boolean wasOnline = false;
            Pair<Integer, Boolean> result = system
                    .updateBuildings(predecessorID, predecessorBuilding.getNeededEnergy());
            wasOnline = result.getSecond();
            int numberOfNewBuildings = result.getFirst();
            for (int z = 0; z < numberOfNewBuildings; z++) {
                res.getUniverseMap().buildBuilding(list, coord);

                //if the new building needs energy, then we try to bring it online
                SystemManager manager = system.getManager();
                if (res.getBuildingInfo(list).getNeededEnergy() > 0) {
                    manager.upgradeIgnoredBuilding(predecessorID, list);
                    if (wasOnline && system.setNewBuildingOnline(buildingInfos) == 2 && !manager.isActive())
                        systemMessage(system, major, "NOT_ENOUGH_ENERGY", EmpireNewsType.SOMETHING, 2);
                }
            }
        } else if (list >= 10000 && list < 20000) { //a ship is built
            res.getUniverseMap().buildShip(list, coord, major.getRaceId());
            militaryMessage(system, major, "SHIP_BUILDING_FINISHED", res.getShipInfos().get(list - 10000)
                    .getShipTypeAsString());
        } else if (list >= 20000) {
            res.getUniverseMap().buildTroop(list - 20000, coord);
            militaryMessage(system, major, "TROOP_BUILDING_FINISHED", res.getTroopInfos().get(list - 20000).getName());
        }
    }

    public void build(StarSystem system, Major major, Array<BuildingInfo> buildingInfos) {
        AssemblyList assemblyList = system.getAssemblyList();
        int list = assemblyList.getAssemblyListEntry(0).id;
        int entryCount = assemblyList.getAssemblyListEntry(0).count;
        if (list != 0) {
            int ipProd = system.calcIndustryPointsProd(buildingInfos, list);
            if (assemblyList.calculateBuildInAssemblyList(ipProd)) {
                creditsIfBought(assemblyList, ipProd, major.getEmpire());
                finishBuild(list, system, major, buildingInfos);
                // after calculateAssemblylist, clearAssemblyList is called, if the order is completed. Normally after clearAssemblyList() we have to call calculateVariables()
                // because of credits throug trade if nothing is in the assemblyList. Here it's not yet necessary though because it will be called later anyway for the system
                if (entryCount > 1)
                    updateAssemblyListResources(list, major, system);

                assemblyList.clearAssemblyList(system.getCoordinates(), res);
                //if the assemblyList is empty after the last building, generate a message
                if (assemblyList.getAssemblyListEntry(0).id == 0)
                    systemMessage(system, major, "EMPTY_BUILDLIST", EmpireNewsType.SOMETHING, 0);
            }
            assemblyList.calculateNeededResourcesForUpdate(buildingInfos, system.getAllBuildings(), major.getEmpire()
                    .getResearch().getResearchInfo());
        }
        system.trainTroops();
    }

    private void updateAssemblyListResources(int id, Major major, StarSystem system) {
        id = Math.abs(id);
        //difficulty is added here
        float difficulty = res.getGamePreferences().difficulty.getLevel();
        //if it's a human he shouldn't get a bonus from the difficulty
        if (major.isHumanPlayer())
            difficulty = 1.0f;
        //if it's neverready, then the difficulty is 1
        if (id > 0 && id < 10000 && res.getBuildingInfo(id).getNeverReady())
            difficulty = 1.0f;

        int runningNumber = id;
        if (id < 0)
            runningNumber *= (-1);
        if (id >= 10000 && id < 20000) {
            system.getAssemblyList().calculateNeededResources(null, res.getShipInfos().get(id - 10000), null,
                    system.getAllBuildings(), id, major.getEmpire().getResearch().getResearchInfo(), difficulty);
        } else if (id >= 20000) {
            system.getAssemblyList().calculateNeededResources(null, null, res.getTroopInfos().get(id - 20000),
                    system.getAllBuildings(), id, major.getEmpire().getResearch().getResearchInfo(), difficulty);
        } else {
            system.getAssemblyList().calculateNeededResources(res.getBuildingInfo(runningNumber), null, null,
                    system.getAllBuildings(), id, major.getEmpire().getResearch().getResearchInfo(), difficulty);
        }
    }
}
