/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.trade;

import java.util.Arrays;
import java.util.Iterator;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.ObjectMap.Entry;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.constants.ResourceTypes;
import com.blotunga.bote.general.EmpireNews;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.general.EmpireNews.EmpireNewsType;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.utils.IntPoint;

public class Trade {
    public static class TradeStruct {
        public int res;
        public int number;
        public IntPoint system;
        public int price;

        TradeStruct() {
            res = 0;
            number = 0;
            system = new IntPoint(-1, -1);
            price = 0;
        }
    }

    private TradeHistory tradeHistory; 			// trade history object which stores the price history
    private int[] resourcePrice;				// the current price of the resources in the global market
    private int[] resourcePriceAtRoundStart;	// the price of the resource at the start of a turn (it should be set before building, otherwise you could lower the price in the same turn and then buy cheap, then up the price again)
    private Array<TradeStruct> tradeActions;	// the number of resources which we want to buy or sell (negative values mean sell)
    private int quantity;						// the quantity of items traded with each click
    private float tax;							// variable which holds the current tax on trade (race dependent)
    private int[] taxes;						// tax money on resources received for building orders, not the taxes which we make from normal trade
    private double[] monopolBuy;				// do we want to buy a monopoly? it is queried in each turn and if different from null then we want to buy one and represents the price

    public Trade() {
        tradeHistory = new TradeHistory();
        resourcePrice = new int[ResourceTypes.IRIDIUM.getType() + 1];
        resourcePriceAtRoundStart = new int[ResourceTypes.IRIDIUM.getType() + 1];
        tradeActions = new Array<TradeStruct>(false, 10, TradeStruct.class);
        quantity = 0;
        tax = 0.0f;
        taxes = new int[ResourceTypes.IRIDIUM.getType() + 1];
        monopolBuy = new double[ResourceTypes.IRIDIUM.getType() + 1];
        reset();
    }

    public void reset() {
        tradeHistory.reset();
        for (int i = ResourceTypes.TITAN.getType(); i <= ResourceTypes.IRIDIUM.getType(); i++) {
            if (i == ResourceTypes.TITAN.getType()) {
                resourcePrice[i] = 800;
            } else if (i == ResourceTypes.DEUTERIUM.getType()) {
                resourcePrice[i] = 500;
            } else if (i == ResourceTypes.DURANIUM.getType()) {
                resourcePrice[i] = 1000;
            } else if (i == ResourceTypes.CRYSTAL.getType()) {
                resourcePrice[i] = 2000;
            } else if (i == ResourceTypes.IRIDIUM.getType()) {
                resourcePrice[i] = 3000;
            }
            resourcePriceAtRoundStart[i] = resourcePrice[i];
            taxes[i] = 0;
            monopolBuy[i] = 0;
        }
        quantity = 100;
        tax = 1.0f;
        tradeActions.clear();
    }

    public TradeHistory getTradeHistory() {
        return tradeHistory;
    }

    public int[] getResourcePrice() {
        return resourcePrice;
    }

    public int[] getResourcePriceAtRoundStart() {
        return resourcePriceAtRoundStart;
    }

    public float getTax() {
        return tax;
    }

    public int[] getTaxesFromBuying() {
        return taxes;
    }

    public int getQuantity() {
        return quantity;
    }

    public double[] getMonopolBuying() {
        return monopolBuy;
    }

    public Array<TradeStruct> getTradeActions() {
        return tradeActions;
    }

    public void setResourcePrice(int res, int price) {
        resourcePrice[res] = price;
    }

    public void setTax(float newTax) {
        tax = newTax;
    }

    public void setQuantity(int newQuantity) {
        quantity = newQuantity;
    }

    public void setMonopolBuying(int res, double costs) {
        monopolBuy[res] = costs;
    }

    public void buyMonopol(int res, double monopolCosts) {
        monopolBuy[res] = monopolCosts;
    }

    /**
     * Function buys the number of resources for the system, and adds the order into tradeActions.
     * Afterwards it calculates the price of the resource after buying. Taxes are not yet included here
     * @param res
     * @param number
     * @param system
     * @param empireCredits
     * @param notAtMarket
     * @return
     */
    public int buyResource(int res, long number, IntPoint system, long empireCredits, boolean notAtMarket) {
        int oldResPrice = resourcePrice[res];
        TradeStruct ts = new TradeStruct();
        ts.res = res;
        ts.number = (int) number;
        ts.system = system;
        ts.price = 0;

        // Each 100 units raises the price on the market
        for (int i = 0; i < Math.ceil(number / 100.0f); i++) {
            // If bNotAtMarket == true then the resource price is from the beginning of the turn, because
            // if build orders have to be bought, these shouldn't be dependent from the current market prices
            // but from the prices which were in the beginning of the turn. Multiple buying still raises costs
            if (notAtMarket) {
                ts.price += (int) ((resourcePriceAtRoundStart[res] * number) / 1000.0);
            } else {
                ts.price += (int) ((resourcePrice[res] * number) / 1000.0);
            }

            // for each 100 units the prices gets risen by (1+resourcenumber) * 5
            resourcePrice[res] += ((res + 1) * 5);
        }
        if ((int) Math.ceil(number / 100.0) != 0)
            ts.price /= (int) Math.ceil(number / 100.0);

        // Only if notAtMarket == false will the action registered otherwise only the prices will raise on the
        // market and taxes calculated
        if (notAtMarket) {
            taxes[res] += (int) (ts.price * tax) - ts.price;
            return 0;
        }

        // if the price is 0, we set it on 1 (nothing is free)
        if (ts.price == 0)
            ts.price = 1;

        // now we check if we have enough credits for these, otherwise we return 0 and set the price back
        if ((int) Math.ceil(ts.price * tax) > empireCredits) {
            resourcePrice[res] = oldResPrice;
            return 0;
        }

        tradeActions.add(ts);
        return (int) Math.ceil(ts.price * tax);
    }

    public int buyResource(int res, long number, IntPoint system, long empireCredits) {
        return buyResource(res, number, system, empireCredits, false);
    }

    /**
     * Function sells the number of resources from the system, and adds the order into tradeActions.
     * Afterwards it calculates the price of the resource after selling. Taxes are not yet included here
     * @param res
     * @param number
     * @param system
     * @param notAtMarket
     * @return
     */
    public void sellResource(int res, long number, IntPoint system, boolean notAtMarket) {
        TradeStruct ts = new TradeStruct();
        ts.res = res;
        ts.number = (int) number;
        ts.system = system;
        ts.price = 0;

        // Each 100 units lowers the price on the market
        for (int i = 0; i < Math.ceil(number / 100.0f); i++) {
            // If bNotAtMarket == true then the resource price is from the beginning of the turn, because
            // if build orders have to be bought, these shouldn't be dependent from the current market prices
            // but from the prices which were in the beginning of the turn. Multiple buying still raises costs
            if (notAtMarket) {
                ts.price -= (int) ((resourcePriceAtRoundStart[res] * number) / 1000.0);
            } else {
                ts.price -= (int) ((resourcePrice[res] * number) / 1000.0);
            }

            // for each 100 units the prices gets lowered by (1+resourcenumber) * 10
            if ((res + 1) * 10 > resourcePrice[res])
                resourcePrice[res] = 1;
            else
                resourcePrice[res] -= ((res + 1) * 10);
        }
        if ((int) Math.ceil(number / 100.0) != 0)
            ts.price /= (int) Math.ceil(number / 100.0);

        // Only if notAtMarket == false will the action registered otherwise only the prices will be lowered on the
        // market and taxes calculated
        if (notAtMarket) {
            taxes[res] += (int) (ts.price * tax) - ts.price;
            return;
        }

        // if the price is 0, we set it on 1 (nothing is free)
        if (ts.price == 0)
            ts.price = -1;

        tradeActions.add(ts);
    }

    public void sellResource(int res, long number, IntPoint system) {
        sellResource(res, number, system, false);
    }

    /**
     * Function calculates all of the trade actions, stores resources or pays out credits
     * which were received to the empire
     * @param major
     * @param system
     * @param taxes
     */
    public int[] calculateTradeActions(Major major, ResourceManager manager) {
        int sum[][][] = new int[manager.getGridSizeX()][manager.getGridSizeY()][ResourceTypes.IRIDIUM.getType() + 1];
        int taxes[] = { 0, 0, 0, 0, 0 };
        for (int[][] row : sum)
            for (int[] innerRow : row)
                Arrays.fill(innerRow, 0);

        boolean didSome = false;
        for (int i = 0; i < tradeActions.size;) {
            TradeStruct ta = tradeActions.get(i);
            IntPoint coord = ta.system;
            int res = ta.res;
            //store the resources in the StarSystem if they were bought
            if (ta.price > 0) {
                manager.getUniverseMap().getStarSystemAt(coord).addResourceStore(res, ta.number);
                sum[coord.x][coord.y][res] += ta.number;
                didSome = true;
            } else
                major.getEmpire().setCredits(-ta.price);

            //here check for monopols, if somebody has a monopol on a resource and we know him then we have to pay taxes
            //we also pay taxes to ourselves which get deducted later
            if (manager.getMonopolOwner(res).isEmpty() == false)
                if (manager.getMonopolOwner(res).equals(major.getRaceId())
                        || major.isRaceContacted(manager.getMonopolOwner(res)))
                    taxes[res] += (int) Math.ceil(Math.abs(ta.price * tax - ta.price));
            //remove after handling
            tradeActions.removeIndex(i);
        }
        //generate message that the resources arrived 
        if (didSome)
            for (int x = 0; x < manager.getGridSizeX(); x++)
                for (int y = 0; y < manager.getGridSizeY(); y++)
                    for (int i = 0; i <= ResourceTypes.IRIDIUM.getType(); i++) {
                        if (sum[x][y][i] > 0) {
                            String text = String.format("%d %s", sum[x][y][i],
                                    StringDB.getString(ResourceTypes.fromResourceTypes(i).getName()));
                            StarSystem ss = manager.getUniverseMap().getStarSystemAt(x, y);
                            EmpireNews message = new EmpireNews();
                            message.CreateNews(StringDB.getString("GET_RESOURCES", false, text, ss.getName()),
                                    EmpireNewsType.ECONOMY, "", ss.getCoordinates());
                            major.getEmpire().addMsg(message);
                        }
                    }
        return taxes;
    }

    /**
     * Function calculates the price of the resources in relation with other markets
     * @param majors
     * @param currentMajor
     */
    public void calculatePrices(ArrayMap<String, Major> majors, Major currentMajor) {
        //the price from a different market can be calculated into our prices of course only if we know the other race
        float newPrices[] = new float[ResourceTypes.IRIDIUM.getType() + 1];
        for (int j = ResourceTypes.TITAN.getType(); j <= ResourceTypes.IRIDIUM.getType(); j++) {
            newPrices[j] = currentMajor.getTrade().getResourcePrice()[j];
            int count = 1;
            for (Iterator<Entry<String, Major>> iter = majors.entries().iterator(); iter.hasNext();) {
                Entry<String, Major> e = iter.next();
                if (e.key.equals(currentMajor.getRaceId()) && currentMajor.isRaceContacted(e.key)) {
                    Major major = e.value;
                    newPrices[j] += major.getTrade().getResourcePrice()[j];
                    count++;
                }
            }
            newPrices[j] /= count;
            resourcePrice[j] = (int) Math.ceil(newPrices[j]);
            resourcePriceAtRoundStart[j] = resourcePrice[j];
            taxes[j] = 0;
        }
    }
}
