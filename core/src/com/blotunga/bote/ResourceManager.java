/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.TextureAtlasLoader;
import com.badlogic.gdx.assets.loaders.TextureLoader;
import com.badlogic.gdx.assets.loaders.TextureLoader.TextureParameter;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.TextureData;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGeneratorLoader;
import com.badlogic.gdx.graphics.g2d.freetype.FreetypeFontLoader;
import com.badlogic.gdx.graphics.g2d.freetype.FreetypeFontLoader.FreeTypeFontLoaderParameter;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.IntMap;
import com.badlogic.gdx.utils.IntSet;
import com.badlogic.gdx.utils.ObjectFloatMap;
import com.badlogic.gdx.utils.ObjectIntMap;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.ObjectSet;
import com.blotunga.bote.GameSettings.GalaxyShowState;
import com.blotunga.bote.achievements.AchievementManager;
import com.blotunga.bote.achievements.AchievementsList;
import com.blotunga.bote.ai.AIPrios;
import com.blotunga.bote.ai.SectorAI;
import com.blotunga.bote.constants.AnomalyType;
import com.blotunga.bote.constants.AnswerStatus;
import com.blotunga.bote.constants.CombatTactics;
import com.blotunga.bote.constants.Difficulties;
import com.blotunga.bote.constants.DiplomaticAgreement;
import com.blotunga.bote.constants.EntityType;
import com.blotunga.bote.constants.GalaxyShapes;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.PlanetSize;
import com.blotunga.bote.constants.PlanetType;
import com.blotunga.bote.constants.PlayerRaces;
import com.blotunga.bote.constants.ResearchComplexType;
import com.blotunga.bote.constants.ResearchStatus;
import com.blotunga.bote.constants.ResearchType;
import com.blotunga.bote.constants.ResourceTypes;
import com.blotunga.bote.constants.ShipOrder;
import com.blotunga.bote.constants.ShipRange;
import com.blotunga.bote.constants.ShipSize;
import com.blotunga.bote.constants.ShipSpecial;
import com.blotunga.bote.constants.ShipType;
import com.blotunga.bote.constants.StarType;
import com.blotunga.bote.constants.SystemOwningStatus;
import com.blotunga.bote.constants.VictoryType;
import com.blotunga.bote.constants.WorkerType;
import com.blotunga.bote.constants.WormholeLinkType;
import com.blotunga.bote.galaxy.Anomaly;
import com.blotunga.bote.galaxy.MapTile;
import com.blotunga.bote.galaxy.Planet;
import com.blotunga.bote.galaxy.ResourceRoute;
import com.blotunga.bote.galaxy.Sector;
import com.blotunga.bote.galaxy.UniverseMap;
import com.blotunga.bote.galaxy.WormholeLinker;
import com.blotunga.bote.general.ClientWorker;
import com.blotunga.bote.general.EmpireNews;
import com.blotunga.bote.general.GameResources;
import com.blotunga.bote.general.GameStatistics;
import com.blotunga.bote.general.SoundManager;
import com.blotunga.bote.intel.DiplomacyIntelObject;
import com.blotunga.bote.intel.EconIntelObject;
import com.blotunga.bote.intel.IntelAssignment;
import com.blotunga.bote.intel.IntelObject;
import com.blotunga.bote.intel.IntelReports;
import com.blotunga.bote.intel.Intelligence;
import com.blotunga.bote.intel.MilitaryIntelObject;
import com.blotunga.bote.intel.ScienceIntelObject;
import com.blotunga.bote.races.Alien;
import com.blotunga.bote.races.DiplomacyInfo;
import com.blotunga.bote.races.Empire;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.races.MajorJoining;
import com.blotunga.bote.races.Minor;
import com.blotunga.bote.races.MoraleObserver;
import com.blotunga.bote.races.Race;
import com.blotunga.bote.races.RaceController;
import com.blotunga.bote.races.RaceDesign;
import com.blotunga.bote.races.Research;
import com.blotunga.bote.races.ResearchComplex;
import com.blotunga.bote.races.ResearchInfo;
import com.blotunga.bote.races.VictoryObserver;
import com.blotunga.bote.races.WeaponObserver;
import com.blotunga.bote.races.Race.RaceType;
import com.blotunga.bote.races.starmap.ExpansionSpeed;
import com.blotunga.bote.ships.BeamWeapons;
import com.blotunga.bote.ships.FireArc;
import com.blotunga.bote.ships.Hull;
import com.blotunga.bote.ships.Shield;
import com.blotunga.bote.ships.Ship;
import com.blotunga.bote.ships.ShipHistory;
import com.blotunga.bote.ships.ShipHistoryStruct;
import com.blotunga.bote.ships.ShipInfo;
import com.blotunga.bote.ships.ShipMap;
import com.blotunga.bote.ships.Ships;
import com.blotunga.bote.ships.TorpedoWeapons;
import com.blotunga.bote.starsystem.AssemblyList;
import com.blotunga.bote.starsystem.AssemblyList.AssemblyListEntry;
import com.blotunga.bote.starsystem.Building;
import com.blotunga.bote.starsystem.BuildingInfo;
import com.blotunga.bote.starsystem.GlobalBuildings;
import com.blotunga.bote.starsystem.GlobalStorage;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.starsystem.SystemManager;
import com.blotunga.bote.starsystem.SystemProd;
import com.blotunga.bote.starsystem.Worker;
import com.blotunga.bote.trade.Trade;
import com.blotunga.bote.trade.TradeHistory;
import com.blotunga.bote.trade.TradeRoute;
import com.blotunga.bote.troops.Troop;
import com.blotunga.bote.troops.TroopInfo;
import com.blotunga.bote.utils.ArrayListSerializer;
import com.blotunga.bote.utils.ArrayMapSerializer;
import com.blotunga.bote.utils.ArraySerializer;
import com.blotunga.bote.utils.DynamicTextureManager;
import com.blotunga.bote.utils.IntArraySerializer;
import com.blotunga.bote.utils.IntPoint;
import com.blotunga.bote.utils.IntSetSerializer;
import com.blotunga.bote.utils.KryoV;
import com.blotunga.bote.utils.ObjectFloatMapSerializer;
import com.blotunga.bote.utils.ObjectIntMapSerializer;
import com.blotunga.bote.utils.ObjectMapSerializer;
import com.blotunga.bote.utils.ObjectSetSerializer;
import com.blotunga.bote.utils.Pair;
import com.blotunga.bote.utils.RandUtil;
import com.blotunga.bote.utils.ShipNameGenerator;
import com.blotunga.bote.utils.StarNameGenerator;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.KryoException;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.esotericsoftware.kryo.serializers.FieldSerializer;
import com.esotericsoftware.kryo.serializers.DefaultSerializers.EnumSerializer;

public abstract class ResourceManager extends Game {
    private RaceController raceController;
    private StarNameGenerator starNameGenerator;
    private ShipNameGenerator shipNameGenerator;
    private Array<ShipInfo> shipInfos;
    private Array<TroopInfo> troopInfos;
    private Array<BuildingInfo> buildingInfos;
    private GlobalBuildings globalBuildings;
    private int currentRound;
    private double starDate;
    private GameStatistics statistics;
    private TextureAtlas symbolTextures;
    private AssetManager assetManager;
    private double[] badMapModifiers;	///< some sectors are dangerous so should not be crossed except by purpose
    private GamePreferences gamePreferences;
    private GameSettings gameSettings;
    private String[] monopolOwner;		// which major race has the monopoly on a resource
    private SectorAI sectorAI;
    private AIPrios aiPrios;
    private boolean endRoundPressed;
    private RandomEventCtrl randomEventCtrl;
    private MajorJoining majorJoining;
    private boolean saveSeed = false;
    private transient volatile boolean endturnProcessing;
    private transient UniverseMap universeMap;
    private transient Music music;
    private transient ClientWorker clientWorker;
    private transient SoundManager soundMgr;
    private transient FileHandleResolver fhResolver;
    private transient Kryo kryo;
    private transient int autoTurns = 0;
    private transient boolean seeAll = false;
    private transient Boolean useMipMaps = null;
    protected transient SkinManager skinManager;
    private transient DynamicTextureManager dynamicTextureManager;
    private transient ObjectMap<String, Pixmap> sunPixmaps;
    private transient IntMap<String> shipImgNames;
    private transient String forceMajor = "";
    private transient AchievementManager achievementManager;
    private transient PlatformApiIntegration apiIntegrator;
    protected transient AndroidIntegration integrator;
    private transient boolean continueAfterVictory;

    private boolean initialized;

    public ResourceManager(PlatformApiIntegration apiIntegrator) {
        this((AndroidIntegration) null, apiIntegrator);
    }

    public ResourceManager(String[] arg, PlatformApiIntegration apiIntegration) {
        this(apiIntegration);
        for (int i = 0; i < arg.length; i++) {
            if (arg[i].startsWith("--autoturns="))
                autoTurns = Integer.parseInt(arg[i].substring(12, arg[i].length()));
            if (arg[i].equals("--see-all"))
                seeAll = true;
            if (arg[i].startsWith("--race="))
                forceMajor = arg[i].substring(7, arg[i].length());
        }
    }

    public ResourceManager(AndroidIntegration integrator, PlatformApiIntegration apiIntegrator) {
        initialized = false;
        this.integrator = integrator; 
        this.apiIntegrator = apiIntegrator;
        this.fhResolver = integrator != null ? integrator.getFileHandleResolver() : null;
        dynamicTextureManager = new DynamicTextureManager();
        sunPixmaps = new ObjectMap<String, Pixmap>();
        symbolTextures = null;
        assetManager = new AssetManager();
        FileHandleResolver resolver = new InternalFileHandleResolver();
        assetManager.setLoader(FreeTypeFontGenerator.class, new FreeTypeFontGeneratorLoader(resolver));
        assetManager.setLoader(BitmapFont.class, ".ttf", new FreetypeFontLoader(resolver));
        if (fhResolver != null) {
            assetManager.setLoader(TextureAtlas.class, ".pack", new TextureAtlasLoader(fhResolver));
            assetManager.setLoader(Texture.class, ".png", new TextureLoader(fhResolver));
            assetManager.setLoader(Texture.class, ".jpg", new TextureLoader(fhResolver));
        }
        monopolOwner = new String[ResourceTypes.IRIDIUM.getType() + 1];
        randomEventCtrl = new RandomEventCtrl(this);
        majorJoining = new MajorJoining(this);
        endRoundPressed = false;
        soundMgr = new SoundManager(this);
        clientWorker = new ClientWorker();
        shipImgNames = new IntMap<String>();
        achievementManager = new AchievementManager(this);
        initKryo();
    }

    public void Init(int maxTextureSize) {
        if (!initialized) {
            currentRound = 1;
            starDate = 121000.0;
            gamePreferences = new GamePreferences();
            gameSettings = new GameSettings();
            loadGameSettings();
            if (maxTextureSize >= 4096)
                gameSettings.backgroundMod = "";
            if(Gdx.app.getType() != ApplicationType.Android)
                gameSettings.setDisableLowMemProfile();
            else {
                if((integrator != null) && (integrator.getAvailableMemInfo() > GameConstants.MIN_MEM_FOR_BACKGROUNDS))
                    gameSettings.setDisableLowMemProfile();
                else
                    gameSettings.setEnableLowMemProfile();
            }
            if (gameSettings.autoSignIn)
                apiIntegrator.signIn();
            gamePreferences.autoTurns = autoTurns;
            gamePreferences.seeAllofMap = seeAll;
            statistics = new GameStatistics();
            raceController = new RaceController(this);
            starNameGenerator = new StarNameGenerator();
            shipNameGenerator = new ShipNameGenerator();
            raceController.init();
            shipInfos = new Array<ShipInfo>(true, 16);
            readShipInfosFromFile(GameConstants.getLocalePrefix());
            troopInfos = new Array<TroopInfo>(true, 16);
            readTroopInfosFromFile(GameConstants.getLocalePrefix());
            buildingInfos = new Array<BuildingInfo>(true, 16);
            readBuildingInfosFromFile(GameConstants.getLocalePrefix());
            globalBuildings = new GlobalBuildings();
            universeMap = null;
            Arrays.fill(monopolOwner, "");
            sectorAI = new SectorAI(this);
            aiPrios = new AIPrios(this);
            AchievementsList.initAchievements();
            if (!achievementManager.isInitialized())
                achievementManager.initialize();
            continueAfterVictory = false;
            initialized = true;
        }
    }

    public void initSkin() {
        clearSkin();
        skinManager = new SkinManager(this);
    }

    public void clearSkin() {
        if(skinManager != null) {
            skinManager.dispose();
            skinManager = null;
        }        
    }

    public Skin getSkin() {
        if(skinManager != null)
            return skinManager.getSkin();
        else
            return assetManager.get(GameConstants.UISKIN_PATH);
    }

    public void uninit() { //we set this when we hide the main menu so that we can reinit for a new game
        initialized = false;
    }

    public boolean isInitialized() {
        return initialized;
    }

    private void initKryo() {
        kryo = new KryoV();
        kryo.setRegistrationRequired(true);
        kryo.getFieldSerializerConfig().setOptimizedGenerics(true);
//        com.esotericsoftware.minlog.Log.TRACE();
        register();
    }

    private void register() {
        int serializerID = 1000;
        kryo.register(Array.class, new ArraySerializer(), serializerID++);
        kryo.register(StarSystem.class, serializerID++);
        kryo.register(MapTile.class, serializerID++);
        kryo.register(Sector.class, serializerID++);
        kryo.register(IntPoint.class, serializerID++);
        kryo.register(Planet.class, new FieldSerializer<Planet>(kryo, Planet.class), serializerID++);
        kryo.register(StarType.class, new EnumSerializer(StarType.class), serializerID++);
        kryo.register(boolean[].class, serializerID++); // found in Planets for example
        kryo.register(PlanetSize.class, new EnumSerializer(PlanetSize.class), serializerID++);
        kryo.register(PlanetType.class, new EnumSerializer(PlanetType.class), serializerID++);
        kryo.register(AssemblyList.class, serializerID++);
        kryo.register(int[].class, serializerID++);
        kryo.register(SystemManager.class, serializerID++);
        kryo.register(IntArray.class, new IntArraySerializer(), serializerID++);
        kryo.register(ArrayList.class, new ArrayListSerializer(), serializerID++);
        kryo.register(ShipOrder.class, new EnumSerializer(ShipOrder.class), serializerID++);
        kryo.register(ObjectSet.class, new ObjectSetSerializer(), serializerID++);
        kryo.register(IntSet.class, new IntSetSerializer(), serializerID++);
        kryo.register(WorkerType.class, new EnumSerializer(WorkerType.class), serializerID++);
        kryo.register(SystemOwningStatus.class, new EnumSerializer(SystemOwningStatus.class), serializerID++);
        kryo.register(SystemProd.class, serializerID++);
        kryo.register(ShipSize.class, new EnumSerializer(ShipSize.class), serializerID++);
        kryo.register(MapTile.DiscoverStatus.class, new EnumSerializer(MapTile.DiscoverStatus.class), serializerID++);
        kryo.register(GameResources.class, serializerID++);
        kryo.register(Worker.class, serializerID++);
        kryo.register(ObjectMap.class, new ObjectMapSerializer(), serializerID++);
        kryo.register(ObjectIntMap.class, new ObjectIntMapSerializer(), serializerID++);
        kryo.register(ObjectFloatMap.class, new ObjectFloatMapSerializer(), serializerID++);
        kryo.register(ArrayMap.class, new ArrayMapSerializer(), serializerID++);
        kryo.register(Alien.class, serializerID++);
        kryo.register(Race.class, serializerID++);
        kryo.register(Major.class, serializerID++);
        kryo.register(MoraleObserver.class, serializerID++);
        kryo.register(Minor.class, serializerID++);
        kryo.register(ShipMap.class, serializerID++);
        kryo.register(Ships.class, serializerID++);
        kryo.register(Ship.class, serializerID++);
        kryo.register(CombatTactics.class, new EnumSerializer(CombatTactics.class), serializerID++);
        kryo.register(BeamWeapons.class, serializerID++);
        kryo.register(TorpedoWeapons.class, serializerID++);
        kryo.register(FireArc.class, serializerID++);
        kryo.register(Hull.class, serializerID++);
        kryo.register(Shield.class, serializerID++);
        kryo.register(ShipRange.class, new EnumSerializer(ShipRange.class), serializerID++);
        kryo.register(ShipSpecial.class, new EnumSerializer(ShipSpecial.class), serializerID++);
        kryo.register(ShipSpecial[].class, serializerID++);
        kryo.register(ShipType.class, new EnumSerializer(ShipType.class), serializerID++);
        kryo.register(DiplomaticAgreement.class, new EnumSerializer(DiplomaticAgreement.class), serializerID++);
        kryo.register(AnswerStatus.class, new EnumSerializer(AnswerStatus.class), serializerID++);
        kryo.register(DiplomacyInfo.class, serializerID++);
        kryo.register(EmpireNews.class, serializerID++);
        kryo.register(EmpireNews.EmpireNewsType.class, serializerID++);
        kryo.register(Empire.class, serializerID++);
        kryo.register(Research.class, serializerID++);
        kryo.register(ResearchInfo.class, serializerID++);
        kryo.register(ResearchComplexType.class, new EnumSerializer(ResearchComplexType.class), serializerID++);
        kryo.register(ResearchStatus.class, new EnumSerializer(ResearchStatus.class), serializerID++);
        kryo.register(ResearchStatus[].class, serializerID++);
        kryo.register(ResearchType.class, new EnumSerializer(ResearchType.class), serializerID++);
        kryo.register(RaceDesign.class, serializerID++);
        kryo.register(Color.class, serializerID++);
        kryo.register(String[].class, serializerID++);
        kryo.register(Intelligence.class, serializerID++);
        kryo.register(IntelAssignment.class, serializerID++);
        kryo.register(IntelReports.class, serializerID++);
        kryo.register(IntelObject.class, serializerID++);
        kryo.register(EconIntelObject.class, serializerID++);
        kryo.register(ScienceIntelObject.class, serializerID++);
        kryo.register(MilitaryIntelObject.class, serializerID++);
        kryo.register(DiplomacyIntelObject.class, serializerID++);
        kryo.register(ObjectIntMap[].class, serializerID++);
        kryo.register(ResearchComplex.class, serializerID++);
        kryo.register(ResearchComplex[].class, serializerID++);
        kryo.register(RaceType.class, new EnumSerializer(RaceType.class), serializerID++);
        kryo.register(ShipHistory.class, serializerID++);
        kryo.register(ShipInfo.class, serializerID++);
        kryo.register(ShipHistoryStruct.class, serializerID++);
        kryo.register(Trade.class, serializerID++);
        kryo.register(Trade.TradeStruct.class, serializerID++);
        kryo.register(TradeHistory.class, serializerID++);
        kryo.register(double[].class, serializerID++);
        kryo.register(GlobalBuildings.class, serializerID++);
        kryo.register(Building.class, serializerID++);
        kryo.register(Anomaly.class, serializerID++);
        kryo.register(AnomalyType.class, new EnumSerializer(AnomalyType.class), serializerID++);
        kryo.register(ResourceRoute.class, serializerID++);
        kryo.register(TradeRoute.class, serializerID++);
        kryo.register(ResourceTypes.class, new EnumSerializer(ResourceTypes.class), serializerID++);
        kryo.register(GlobalStorage.class, serializerID++);
        kryo.register(GlobalStorage.StorageStruct.class, serializerID++);
        kryo.register(GamePreferences.class, serializerID++);
        kryo.register(Difficulties.class, new EnumSerializer(Difficulties.class), serializerID++);
        kryo.register(ExpansionSpeed.class, new EnumSerializer(ExpansionSpeed.class), serializerID++);
        kryo.register(ShipNameGenerator.class, serializerID++);
        kryo.register(VictoryObserver.class, serializerID++);
        kryo.register(VictoryType.class, new EnumSerializer(VictoryType.class), serializerID++);
        kryo.register(Troop.class, serializerID++);
        kryo.register(TroopInfo.class, serializerID++);
        kryo.register(WeaponObserver.class, serializerID++);
        kryo.register(WeaponObserver.BeamWeaponsObserverStruct.class, serializerID++);
        kryo.register(WeaponObserver.TubeWeaponsObserverStruct.class, serializerID++);
        kryo.register(MajorJoining.class, serializerID++);
        kryo.register(GalaxyShapes.class, new EnumSerializer(GalaxyShapes.class), serializerID++);
        kryo.register(EntityType.class, new EnumSerializer(EntityType.class), serializerID++);
        kryo.register(WormholeLinkType.class, new EnumSerializer(WormholeLinkType.class), serializerID++);
        kryo.register(WormholeLinker.class, serializerID++);
        kryo.register(GameSettings.class, serializerID++);
        kryo.register(Date.class, serializerID++);
        kryo.register(AssemblyListEntry.class, serializerID++);
        kryo.register(GalaxyShowState.class, serializerID++);
        kryo.register(AssemblyListEntry[].class, serializerID++);
    }

    public Kryo getKryo() {
        return kryo;
    }

    public Array<ShipInfo> readShipInfosFromFile(String locale) {
        if (locale.equals("/en"))
            locale = "";
        shipInfos.clear();
        shipImgNames.clear();
        FileHandle statHandle = Gdx.files.internal("data/ships/shipstatlist.txt");
        String statContent = statHandle.readString("ISO-8859-1");

        String[] statInputs = statContent.split("\n");
        boolean torpedo = false;
        String[] statData = new String[40];
        String[] statTorpedoData = new String[9];
        String[] statBeamData = new String[12];
        Array<TorpedoWeapons> torpedoes = new Array<TorpedoWeapons>(true, 3);
        Array<BeamWeapons> beams = new Array<BeamWeapons>(true, 3);

        if (statInputs.length != 0) {
            int i = 0;
            int weapons = 0;
            for (String input : statInputs) {
                input = input.trim();
                if (!input.equals("$END_OF_SHIPDATA$")) {
                    if (input.equals("$Torpedo$")) {
                        weapons = 9;    // 9 records for a torpedo
                        torpedo = true;
                    } else if (input.equals("$Beam$")) {
                        weapons = 12;   // 12 lines for beam weapons
                        torpedo = false;
                    } else if (torpedo == true && weapons > 0) {
                        statTorpedoData[9 - weapons] = input;
                        weapons--;
                        if (weapons == 0) {
                            TorpedoWeapons torpedoWeapon = new TorpedoWeapons();
                            int k = 0;
                            torpedoWeapon.modifyTorpedoWeapon(Integer.parseInt(statTorpedoData[k++]),
                                    Integer.parseInt(statTorpedoData[k++]), Integer.parseInt(statTorpedoData[k++]),
                                    Integer.parseInt(statTorpedoData[k++]), statTorpedoData[k++],
                                    Integer.parseInt(statTorpedoData[k++]) != 0, Integer.parseInt(statTorpedoData[k++]));
                            torpedoWeapon.getFireArc().setValues(Integer.parseInt(statTorpedoData[k++]),
                                    Integer.parseInt(statTorpedoData[k++]));
                            torpedoes.add(torpedoWeapon);
                        }
                    } else if (torpedo == false && weapons > 0) {
                        statBeamData[12 - weapons] = input;
                        weapons--;
                        if (weapons == 0) {
                            BeamWeapons beamWeapon = new BeamWeapons();
                            int k = 0;
                            beamWeapon.modifyBeamWeapon(Integer.parseInt(statBeamData[k++]), Integer.parseInt(statBeamData[k++]),
                                    Integer.parseInt(statBeamData[k++]), statBeamData[k++], Integer.parseInt(statBeamData[k++]) != 0,
                                    Integer.parseInt(statBeamData[k++]) != 0, Integer.parseInt(statBeamData[k++]),
                                    Integer.parseInt(statBeamData[k++]), Integer.parseInt(statBeamData[k++]),
                                    Integer.parseInt(statBeamData[k++]));
                            beamWeapon.getFireArc().setValues(Integer.parseInt(statBeamData[k++]),
                                    Integer.parseInt(statBeamData[k++]));
                            beams.add(beamWeapon);
                        }
                    } else {
                        statData[i] = input;
                        i++;
                    }
                } else {
                    ShipInfo shipInfo = new ShipInfo(this);
                    int k = 0;
                    shipInfo.setID(Integer.parseInt(statData[k++]));
                    shipImgNames.put((shipInfo.getID() - 10000), statData[k++]);
                    shipInfo.setRace(Integer.parseInt(statData[k++]));
                    if (shipInfo.getRace() == PlayerRaces.UNKNOWNRACE.getType())
                        shipInfo.setRace(PlayerRaces.MINORNUMBER.getType());
                    shipInfo.setType(ShipType.fromInt(Integer.parseInt(statData[k++])));
                    shipInfo.setSize(ShipSize.fromInt(Integer.parseInt(statData[k++])));
                    shipInfo.setManeuverabilty(Integer.parseInt(statData[k++]));
                    shipInfo.setBioTech(Integer.parseInt(statData[k++]));
                    shipInfo.setEnergyTech(Integer.parseInt(statData[k++]));
                    shipInfo.setCompTech(Integer.parseInt(statData[k++]));
                    shipInfo.setPropulsionTech(Integer.parseInt(statData[k++]));
                    shipInfo.setConstructionTech(Integer.parseInt(statData[k++]));
                    shipInfo.setWeaponTech(Integer.parseInt(statData[k++]));
                    shipInfo.setNeededIndustry(Integer.parseInt(statData[k++]));
                    shipInfo.setNeededTitan(Integer.parseInt(statData[k++]));
                    shipInfo.setNeededDeuterium(Integer.parseInt(statData[k++]));
                    shipInfo.setNeededDuranium(Integer.parseInt(statData[k++]));
                    shipInfo.setNeededCrystal(Integer.parseInt(statData[k++]));
                    shipInfo.setNeededIridium(Integer.parseInt(statData[k++]));
                    shipInfo.setNeededDeritium(Integer.parseInt(statData[k++]));
                    shipInfo.setOnlyInSystem(statData[k++]);
                    int baseHull = Integer.parseInt(statData[k++]);
                    int hullMaterial = Integer.parseInt(statData[k++]);
                    shipInfo.getHull().modifyHull(Integer.parseInt(statData[k++]) != 0, baseHull, hullMaterial,
                            Integer.parseInt(statData[k++]) != 0, Integer.parseInt(statData[k++]) != 0);
                    shipInfo.getShield().modifyShield(Integer.parseInt(statData[k++]), Integer.parseInt(statData[k++]),
                            Integer.parseInt(statData[k++]) != 0);
                    shipInfo.setSpeed(Integer.parseInt(statData[k++]));
                    shipInfo.setRange(ShipRange.fromInt(Integer.parseInt(statData[k++])));
                    shipInfo.setScanPower(Integer.parseInt(statData[k++]));
                    shipInfo.setScanRange(Integer.parseInt(statData[k++]));
                    shipInfo.setStealthGrade(Integer.parseInt(statData[k++]));
                    shipInfo.setStorageRoom(Integer.parseInt(statData[k++]));
                    shipInfo.setColonizePoints(Integer.parseInt(statData[k++]));
                    shipInfo.setStationBuildPoints(Integer.parseInt(statData[k++]));
                    shipInfo.setMaintenanceCosts(Integer.parseInt(statData[k++]));
                    shipInfo.setSpecial(0, ShipSpecial.fromInt(Integer.parseInt(statData[k++])));
                    shipInfo.setSpecial(1, ShipSpecial.fromInt(Integer.parseInt(statData[k++])));
                    shipInfo.setObsoletesClass(Integer.parseInt(statData[k++]));
                    shipInfo.getTorpedoWeapons().addAll(torpedoes);
                    shipInfo.getBeamWeapons().addAll(beams);
                    shipInfo.calculateFinalCosts();
                    shipInfo.setStartOrder();
                    shipInfo.setStartTactics();
                    shipInfos.add(shipInfo);
                    torpedoes.clear();
                    beams.clear();
                    i = 0;
                }
            }
        }

        FileHandle infoHandle = Gdx.files.internal("data" + locale + "/ships/shipinfolist.txt");
        String infoContent = infoHandle.readString(GameConstants.getCharset(locale));
        String[] infoInput = infoContent.split("\n");
        String infoData[] = new String[3];
        if (statInputs.length != 0) {
            int i = 0;
            for (String input : infoInput) {
                input = input.trim();
                if (!input.equals("$END_OF_SHIPDATA$")) {
                    infoData[i] = input;
                    i++;
                } else {
                    int k = 0;
                    ShipInfo si = shipInfos.get(Integer.parseInt(infoData[k++]));
                    si.setShipClass(infoData[k++]);
                    si.setDescription(infoData[k++]);
                    i = 0;
                }
            }
        }
        return new Array<ShipInfo>(shipInfos);
    }

    public Array<TroopInfo> readTroopInfosFromFile(String locale) {
        if (locale.equals("/en"))
            locale = "";

        troopInfos.clear();
        FileHandle handle = Gdx.files.internal("data/troops/troopstatlist.txt");
        String content = handle.readString("ISO-8859-1");
        String[] inputs = content.split("\n");
        String[] data = new String[20];
        int i = 0;
        for (String input : inputs) {
            input = input.trim();
            if (!input.startsWith("//") && !input.isEmpty()) {
                data[i++] = input;
                if (i == 20) {
                    i = 0;
                    String owner = data[i++];
                    String fileName = data[i++];
                    fileName = fileName.substring(0, fileName.lastIndexOf('.')); // trim extension
                    int offense = Integer.parseInt(data[i++]);
                    int defense = Integer.parseInt(data[i++]);
                    int cost = Integer.parseInt(data[i++]);
                    int[] techs = new int[6];
                    for (int j = 0; j < techs.length; j++)
                        techs[j] = Integer.parseInt(data[i++]);
                    int[] res = new int[5];
                    for (int j = 0; j < res.length; j++)
                        res[j] = Integer.parseInt(data[i++]);

                    int neededIndustry = Integer.parseInt(data[i++]);
                    int ID = Integer.parseInt(data[i++]);
                    int size = Integer.parseInt(data[i++]);
                    int moralValue = Integer.parseInt(data[i++]);

                    TroopInfo ti = new TroopInfo("", "", fileName, offense, defense, cost, techs, res,
                            neededIndustry, ID, owner, size, moralValue);
                    troopInfos.add(ti);
                    i = 0;
                }
            }
        }

        FileHandle infoHandle = Gdx.files.internal("data" + locale + "/troops/troopinfolist.txt");
        String infoContent = infoHandle.readString(GameConstants.getCharset(locale));
        String[] infoInput = infoContent.split("\n");
        String infoData[] = new String[3];
        i = 0;
        for (int j = 0; j < infoInput.length; j++) {
            infoData[i++] = infoInput[j].trim();
            if (i == 3) {
                i = 0;
                int k = 0;
                TroopInfo ti = troopInfos.get(Integer.parseInt(infoData[k++]));
                ti.setName(infoData[k++]);
                ti.setDescription(infoData[k++]);
            }
        }

        return new Array<TroopInfo>(troopInfos);
    }

    public Array<BuildingInfo> readBuildingInfosFromFile(String locale) {
        if (locale.equals("/en"))
            locale = "";

        buildingInfos.clear();
        FileHandle handle = Gdx.files.internal("data/buildings/buildingstatlist.txt");
        String content = handle.readString(GameConstants.getCharset());
        String[] inputs = content.split("\n");
        String[] data = new String[136];
        int i = 0;
        for (int j = 0; j < inputs.length; j++) {
            data[i++] = inputs[j].trim();
            if (i == 136) {
                i = 0;
                BuildingInfo info = new BuildingInfo();
                info.setRunningNumber(Integer.parseInt(data[i++]));
                info.setOwnerOfBuilding(Integer.parseInt(data[i++]));
                info.setUpgradeable(Integer.parseInt(data[i++]) != 0);
                String tmp = data[i++];
                tmp = tmp.substring(0, tmp.lastIndexOf('.'));
                info.setGraphicFileName(tmp);
                info.setMaxInSystem(Integer.parseInt(data[i++]), Integer.parseInt(data[i++]));
                info.setMaxInEmpire(Integer.parseInt(data[i++]), Integer.parseInt(data[i++]));
                info.setOnlyHomePlanet(Integer.parseInt(data[i++]) != 0);
                info.setOnlyOwnColony(Integer.parseInt(data[i++]) != 0);
                info.setOnlyMinorRace(Integer.parseInt(data[i++]) != 0);
                info.setOnlyTakenSystem(Integer.parseInt(data[i++]) != 0);
                info.setOnlyInSystemWithName(data[i++]);
                info.setMinInhabitants(Integer.parseInt(data[i++]));
                info.setMinInSystem(Integer.parseInt(data[i++]), Integer.parseInt(data[i++]));
                info.setMinInEmpire(Integer.parseInt(data[i++]), Integer.parseInt(data[i++]));
                info.setOnlyRace(Integer.parseInt(data[i++]) != 0);
                info.setPlanetTypes(PlanetType.PLANETCLASS_A.getPlanetClass(), Integer.parseInt(data[i++]) != 0);
                info.setPlanetTypes(PlanetType.PLANETCLASS_B.getPlanetClass(), Integer.parseInt(data[i++]) != 0);
                info.setPlanetTypes(PlanetType.PLANETCLASS_C.getPlanetClass(), Integer.parseInt(data[i++]) != 0);
                info.setPlanetTypes(PlanetType.PLANETCLASS_E.getPlanetClass(), Integer.parseInt(data[i++]) != 0);
                info.setPlanetTypes(PlanetType.PLANETCLASS_F.getPlanetClass(), Integer.parseInt(data[i++]) != 0);
                info.setPlanetTypes(PlanetType.PLANETCLASS_G.getPlanetClass(), Integer.parseInt(data[i++]) != 0);
                info.setPlanetTypes(PlanetType.PLANETCLASS_H.getPlanetClass(), Integer.parseInt(data[i++]) != 0);
                info.setPlanetTypes(PlanetType.PLANETCLASS_I.getPlanetClass(), Integer.parseInt(data[i++]) != 0);
                info.setPlanetTypes(PlanetType.PLANETCLASS_J.getPlanetClass(), Integer.parseInt(data[i++]) != 0);
                info.setPlanetTypes(PlanetType.PLANETCLASS_K.getPlanetClass(), Integer.parseInt(data[i++]) != 0);
                info.setPlanetTypes(PlanetType.PLANETCLASS_L.getPlanetClass(), Integer.parseInt(data[i++]) != 0);
                info.setPlanetTypes(PlanetType.PLANETCLASS_M.getPlanetClass(), Integer.parseInt(data[i++]) != 0);
                info.setPlanetTypes(PlanetType.PLANETCLASS_N.getPlanetClass(), Integer.parseInt(data[i++]) != 0);
                info.setPlanetTypes(PlanetType.PLANETCLASS_O.getPlanetClass(), Integer.parseInt(data[i++]) != 0);
                info.setPlanetTypes(PlanetType.PLANETCLASS_P.getPlanetClass(), Integer.parseInt(data[i++]) != 0);
                info.setPlanetTypes(PlanetType.PLANETCLASS_Q.getPlanetClass(), Integer.parseInt(data[i++]) != 0);
                info.setPlanetTypes(PlanetType.PLANETCLASS_R.getPlanetClass(), Integer.parseInt(data[i++]) != 0);
                info.setPlanetTypes(PlanetType.PLANETCLASS_S.getPlanetClass(), Integer.parseInt(data[i++]) != 0);
                info.setPlanetTypes(PlanetType.PLANETCLASS_T.getPlanetClass(), Integer.parseInt(data[i++]) != 0);
                info.setPlanetTypes(PlanetType.PLANETCLASS_Y.getPlanetClass(), Integer.parseInt(data[i++]) != 0);
                info.setBioTech(Integer.parseInt(data[i++]));
                info.setEnergyTech(Integer.parseInt(data[i++]));
                info.setComputerTech(Integer.parseInt(data[i++]));
                info.setPropulsionTech(Integer.parseInt(data[i++]));
                info.setConstructionTech(Integer.parseInt(data[i++]));
                info.setWeaponTech(Integer.parseInt(data[i++]));
                info.setNeededIndustry(Integer.parseInt(data[i++]));
                info.setNeededEnergy(Integer.parseInt(data[i++]));
                info.setNeededTitan(Integer.parseInt(data[i++]));
                info.setNeededDeuterium(Integer.parseInt(data[i++]));
                info.setNeededDuranium(Integer.parseInt(data[i++]));
                info.setNeededCrystal(Integer.parseInt(data[i++]));
                info.setNeededIridium(Integer.parseInt(data[i++]));
                info.setNeededDeritium(Integer.parseInt(data[i++]));
                info.setFoodProd(Integer.parseInt(data[i++]));
                info.setIndustryPointsProd(Integer.parseInt(data[i++]));
                info.setEnergyProd(Integer.parseInt(data[i++]));
                info.setSecurityPointsProd(Integer.parseInt(data[i++]));
                info.setResearchPointsProd(Integer.parseInt(data[i++]));
                info.setTitanProd(Integer.parseInt(data[i++]));
                info.setDeuteriumProd(Integer.parseInt(data[i++]));
                info.setDuraniumProd(Integer.parseInt(data[i++]));
                info.setCrystalProd(Integer.parseInt(data[i++]));
                info.setIridiumProd(Integer.parseInt(data[i++]));
                info.setDeritiumProd(Integer.parseInt(data[i++]));
                info.setCreditsProd(Integer.parseInt(data[i++]));
                info.setMoraleProd(Integer.parseInt(data[i++]));
                info.setMoraleProdEmpire(Integer.parseInt(data[i++]));
                info.setFoodBonus(Integer.parseInt(data[i++]));
                info.setIndustryBonus(Integer.parseInt(data[i++]));
                info.setEnergyBonus(Integer.parseInt(data[i++]));
                info.setSecurityBonus(Integer.parseInt(data[i++]));
                info.setResearchBonus(Integer.parseInt(data[i++]));
                info.setTitanBonus(Integer.parseInt(data[i++]));
                info.setDeuteriumBonus(Integer.parseInt(data[i++]));
                info.setDuraniumBonus(Integer.parseInt(data[i++]));
                info.setCrystalBonus(Integer.parseInt(data[i++]));
                info.setIridiumBonus(Integer.parseInt(data[i++]));
                info.setDeritiumBonus(Integer.parseInt(data[i++]));
                info.setAllResourceBonus(Integer.parseInt(data[i++]));
                info.setCreditsBonus(Integer.parseInt(data[i++]));
                info.setBioTechBonus(Integer.parseInt(data[i++]));
                info.setEnergyTechBonus(Integer.parseInt(data[i++]));
                info.setComputerTechBonus(Integer.parseInt(data[i++]));
                info.setPropulsionTechBonus(Integer.parseInt(data[i++]));
                info.setConstructionTechBonus(Integer.parseInt(data[i++]));
                info.setWeaponTechBonus(Integer.parseInt(data[i++]));
                info.setInnerSecurityBonus(Integer.parseInt(data[i++]));
                info.setEconomySpyBonus(Integer.parseInt(data[i++]));
                info.setEconomySabotageBonus(Integer.parseInt(data[i++]));
                info.setResearchSpyBonus(Integer.parseInt(data[i++]));
                info.setResearchSabotageBonus(Integer.parseInt(data[i++]));
                info.setMilitarySpyBonus(Integer.parseInt(data[i++]));
                info.setMilitarySabogateBonus(Integer.parseInt(data[i++]));
                info.setShipYard(Integer.parseInt(data[i++]) != 0);
                info.setBuildableShipSizes(ShipSize.fromInt(Integer.parseInt(data[i++])));
                info.setShipYardSpeed(Integer.parseInt(data[i++]));
                info.setBarrack(Integer.parseInt(data[i++]) != 0);
                info.setBarrackSpeed(Integer.parseInt(data[i++]));
                info.setHitPoints(Integer.parseInt(data[i++]));
                info.setShieldPower(Integer.parseInt(data[i++]));
                info.setShieldPowerBonus(Integer.parseInt(data[i++]));
                info.setShipDefend(Integer.parseInt(data[i++]));
                info.setShipDefendBonus(Integer.parseInt(data[i++]));
                info.setGroundDefend(Integer.parseInt(data[i++]));
                info.setGroundDefendBonus(Integer.parseInt(data[i++]));
                info.setScanPower(Integer.parseInt(data[i++]));
                info.setScanPowerBonus(Integer.parseInt(data[i++]));
                info.setScanRange(Integer.parseInt(data[i++]));
                info.setScanRangeBonus(Integer.parseInt(data[i++]));
                info.setShipTraining(Integer.parseInt(data[i++]));
                info.setTroopTraining(Integer.parseInt(data[i++]));
                info.setResistance(Integer.parseInt(data[i++]));
                info.setAddedTradeRoutes(Integer.parseInt(data[i++]));
                info.setIncomeOnTradeRoutes(Integer.parseInt(data[i++]));
                info.setShipRecycling(Integer.parseInt(data[i++]));
                info.setBuildingBuildSpeed(Integer.parseInt(data[i++]));
                info.setUpdateBuildSpeed(Integer.parseInt(data[i++]));
                info.setShipBuildSpeed(Integer.parseInt(data[i++]));
                info.setTroopBuildSpeed(Integer.parseInt(data[i++]));
                info.setPredecessorId(Integer.parseInt(data[i++]));
                info.setAlwaysOnline(Integer.parseInt(data[i++]) != 0);
                info.setWorker(Integer.parseInt(data[i++]) != 0);
                info.setNeverReady(Integer.parseInt(data[i++]) != 0);
                info.setBuildingEquivalent(0, 0);
                for (int p = PlayerRaces.MAJOR1.getType(); p <= PlayerRaces.MAJOR6.getType(); p++)
                    info.setBuildingEquivalent(p, Integer.parseInt(data[i++]));
                for (int res = ResourceTypes.TITAN.getType(); res <= ResourceTypes.DERITIUM.getType(); res++)
                    info.setResourceDistributor(res, Integer.parseInt(data[i++]) != 0);
                info.setNeededSystems(Integer.parseInt(data[i++]));
                buildingInfos.add(info);
                i = 0;
            }
        }

        FileHandle infoHandle = Gdx.files.internal("data" + locale + "/buildings/buildinginfolist.txt");
        String infoContent = infoHandle.readString(GameConstants.getCharset(locale));
        String[] infoInput = infoContent.split("\n");
        String infoData[] = new String[3];
        i = 0;
        for (int j = 0; j < infoInput.length; j++) {
            infoData[i++] = infoInput[j].trim();
            if (i == 3) {
                i = 0;
                int k = 0;
                BuildingInfo bi = buildingInfos.get(Integer.parseInt(infoData[k++]) - 1);
                bi.setBuildingName(infoData[k++]);
                bi.setBuildingDescription(infoData[k++]);
            }
        }

        return new Array<BuildingInfo>(buildingInfos);
    }

    public RaceController getRaceController() {
        return raceController;
    }

    public StarNameGenerator getStarNameGenerator() {
        return starNameGenerator;
    }

    public UniverseMap getUniverseMap() {
        return universeMap;
    }

    public Array<ShipInfo> getShipInfos() {
        return shipInfos;
    }

    public void setShipInfos(Array<ShipInfo> infos) {
        shipInfos = infos;
        for (int i = 0; i < infos.size; i++)
            infos.get(i).setResourceManager(this);
    }

    public String getShipImgName(int ID) {
        return shipImgNames.get(ID, "ImageMissing");
    }

    public Array<TroopInfo> getTroopInfos() {
        return troopInfos;
    }

    public Array<BuildingInfo> getBuildingInfos() {
        return buildingInfos;
    }

    public BuildingInfo getBuildingInfo(int id) {
        return buildingInfos.get(id - 1);
    }

    public void createUniverse() {
        universeMap = new UniverseMap((ScreenManager) this); //make sure this is created before all else, especially for loading
        universeMap.prepareData();
    }

    public GlobalBuildings getGlobalBuildings() {
        return globalBuildings;
    }

    public int getCurrentRound() {
        return currentRound;
    }

    public void setCurrentRound(int val) {
        currentRound = val;
    }

    public void incCurrentRound() {
        currentRound++;
    }

    public void incStarDate(double val) {
        starDate += val;
    }

    public double getStarDate() {
        return starDate;
    }

    public void setStarDate(double val) {
        starDate = val;
    }

    public GameStatistics getStatistics() {
        return statistics;
    }

    public void saveResourceManager(Kryo kryo, Output output) {
        output.writeInt(currentRound);
        Date date = new Date();
        long time = date.getTime();
        date.setTime((time / 1000) * 1000);
        kryo.writeObject(output, date); //save current date
        output.writeBoolean(saveSeed);
        Pair<Long, Long> p = RandUtil.getState();
        output.writeLong(p.getFirst().longValue());
        output.writeLong(p.getSecond().longValue());
        output.writeDouble(starDate);
        kryo.writeObject(output, globalBuildings);
        kryo.writeObject(output, shipInfos);
        kryo.writeObject(output, shipNameGenerator);
        kryo.writeObject(output, monopolOwner);
        kryo.writeObject(output, majorJoining);
    }

    @SuppressWarnings("unchecked")
    public void loadResourceManager(Kryo kryo, Input input) {
        currentRound = input.readInt();
        if (kryo instanceof KryoV) {
            KryoV kv = (KryoV) kryo;
            if (kv.getSaveVersion() > 8)
                kryo.readObject(input, Date.class); //skip date
            if (kv.getSaveVersion() > 7) {
                saveSeed = input.readBoolean();
                long seed0 = input.readLong();
                long seed1 = input.readLong();
                if (saveSeed)
                    RandUtil.setState(seed0, seed1);
            }
        }
        starDate = input.readDouble();
        globalBuildings = kryo.readObject(input, GlobalBuildings.class);
        shipInfos = kryo.readObject(input, Array.class);
        for (int i = 0; i < shipInfos.size; i++)
            shipInfos.get(i).setResourceManager(this);
        shipNameGenerator = kryo.readObject(input, ShipNameGenerator.class);
        monopolOwner = kryo.readObject(input, String[].class);
        majorJoining = kryo.readObject(input, MajorJoining.class);
        majorJoining.setResourceManager(this);
    }

    public ShipNameGenerator getShipNameGenerator() {
        return shipNameGenerator;
    }

    public TextureAtlas.AtlasRegion getSymbolTextures(String name) {
        if(symbolTextures == null)
            symbolTextures = assetManager.get("graphics/symbols/symbols.pack");
        TextureAtlas.AtlasRegion region = symbolTextures.findRegion(name);
        if (region == null)
            region = symbolTextures.findRegion("Default");
        return region;
    }

    public AssetManager getAssetManager() {
        return assetManager;
    }

    public void setGameLoaded(String path) {
        gamePreferences.gameLoaded = path;
    }

    public boolean isGameLoaded() {
        return !gamePreferences.gameLoaded.isEmpty();
    }

    public void setGridSizeX(int gridSizeX) {
        gamePreferences.gridSizeX = gridSizeX;
    }

    public int getGridSizeX() {
        return gamePreferences.gridSizeX;
    }

    public void setGridSizeY(int gridSizeY) {
        gamePreferences.gridSizeY = gridSizeY;
    }

    public int getGridSizeY() {
        return gamePreferences.gridSizeY;
    }

    public void setGamePreferences(GamePreferences gamePreferences) {
        this.gamePreferences = new GamePreferences(gamePreferences);
    }

    public GamePreferences getGamePreferences() {
        return gamePreferences;
    }

    public void saveGameSettings() {
        if (gameSettings != null) {
            try {
                Output output = new Output(new FileOutputStream(Gdx.files.getLocalStoragePath() + "/settings.ini"));
                kryo.writeObject(output, gameSettings);
                output.close();
            } catch (IOException e) {
            } catch (KryoException e) {
            }
        }
    }

    public GameSettings loadGameSettings() {
        try {
            Input input = new Input(new FileInputStream(Gdx.files.getLocalStoragePath() + "/settings.ini"));
            gameSettings = new GameSettings(kryo.readObject(input, GameSettings.class)); //we use the copy constructor because if new fields were added to the array, that way we can initialize it
            input.close();
        } catch (IOException e) {
            //if file not found, do nothing
        } catch (KryoException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(gameSettings.useCompressedSave == false) { //convert all savegames
            FileHandle dirHandle = new FileHandle(GameConstants.getSaveLocation());
            if (!dirHandle.exists())
                dirHandle.mkdirs();
            for (FileHandle entry : dirHandle.list()) {
                if (!entry.isDirectory() && entry.extension().equals("sav")) {
                    FileInputStream in = null;
                    DeflaterOutputStream out = null;
                    String path = GameConstants.getSaveLocation() + entry.name();
                    try {
                        in = new FileInputStream(path);
                        out = new DeflaterOutputStream(new FileOutputStream(path + "tmp"), new Deflater(Deflater.DEFAULT_COMPRESSION));
                        byte[] buf = new byte[4 * 1024 * 1024];
                        int bytesRead;
                        while ((bytesRead = in.read(buf)) > 0) {
                            out.write(buf, 0, bytesRead);
                        }
                    } catch (IOException e) {
                    } finally {
                        if(in != null)
                            try {
                                in.close();
                            } catch (IOException e) {}
                        if(out != null)
                            try {
                                out.close();
                            } catch (IOException e) {}
                    }
                    File file = new File(path + "tmp");
                    File file2 = new File(path);
                    file.renameTo(file2);
                }
            }
            gameSettings.useCompressedSave = true;
            saveGameSettings();
        }
        return gameSettings;
    }

    public GameSettings getGameSettings() {
        return gameSettings;
    }

    public void initBadMapModifiers() {
        badMapModifiers = new double[getGridSizeX() * getGridSizeY()];
        Arrays.fill(badMapModifiers, 0.0);
    }

    public double[] getBadMapModifiers() {
        return badMapModifiers;
    }

    public void setBadMapModifiers(double[] badMapModifiers) {
        this.badMapModifiers = badMapModifiers;
    }

    public void synchronizeWithAnomalies(Array<StarSystem> systems) {
        for (int i = 0; i < systems.size; i++) {
            Anomaly a = systems.get(i).getAnomaly();
            if (systems.get(i).getAnomaly() != null)
                badMapModifiers[i] = a.getWaySearchWeight();
        }
    }

    public int coordsToIndex(int x, int y) {
        return x + getGridSizeX() * y;
    }

    public int coordsToIndex(IntPoint p) {
        return coordsToIndex(p.x, p.y);
    }

    public String getMonopolOwner(int res) {
        return monopolOwner[res];
    }

    public String[] getMonopolOwners() {
        return monopolOwner;
    }

    public void setMonopolOwners(String[] owners) {
        monopolOwner = owners;
    }

    public void setMonopolOwner(int res, String ownerID) {
        monopolOwner[res] = ownerID;
    }

    public SectorAI getSectorAI() {
        return sectorAI;
    }

    public AIPrios getAIPrios() {
        return aiPrios;
    }

    public RandomEventCtrl getRandomEventCtrl() {
        return randomEventCtrl;
    }

    public MajorJoining getMajorJoining() {
        return majorJoining;
    }

    public void setMusic(String musicPath) {
        this.music = Gdx.audio.newMusic(Gdx.files.internal(musicPath));
    }

    public Music getMusic() {
        return music;
    }

    public ClientWorker getClientWorker() {
        return clientWorker;
    }

    public SoundManager getSoundManager() {
        return soundMgr;
    }

    public String getForceMajor() {
        return forceMajor;
    }

    public IntMap<String> getShipImageNames() {
        return shipImgNames;
    }

    public void setSaveSeed(boolean save)  {
        saveSeed = save;
    }

    public boolean shouldSaveSeed() {
        return saveSeed;
    }

    public void setContinueAfterVictory(boolean set) {
        continueAfterVictory = set;
    }

    public boolean isContinueAfterVictory() {
        return continueAfterVictory;
    }

    public boolean fileMissing(String filePath) {
        return (fhResolver != null && !fhResolver.resolve(filePath).exists()) || (fhResolver == null && !Gdx.files.internal(filePath).exists());
    }

    public Texture loadTextureImmediate(String filePath) {
        if (assetManager.isLoaded(filePath)) // because exists() is slow
            return assetManager.get(filePath);

        //long start = System.nanoTime();
        if (fileMissing(filePath)) {
            String path = filePath.substring(0, filePath.lastIndexOf('/'));
            System.err.println("Missing file: " + filePath);
            if (path.endsWith("races")) //races have no ImageMissing file
                path = path.replace("races", "buildings");
            if (path.endsWith("planets")) //planets have no ImageMissing file
                path = path.replace("planets", "buildings");
            filePath = path + "/" + "ImageMissing.png";
        }

        TextureParameter param = new TextureParameter();
        param.genMipMaps = useMipMaps();
        param.minFilter = useMipMaps() ? TextureFilter.MipMapLinearLinear : TextureFilter.Linear;
        param.magFilter = TextureFilter.Linear;
        assetManager.load(filePath, Texture.class, param);
        assetManager.finishLoading();
        //System.err.println("File " + filePath + " Elapsed: " + (System.nanoTime() - start));
        return assetManager.get(filePath);
    }

    public void unLoadBackgounds() {
        String prefix = raceController.getPlayerRace().getPrefix();

        Array<String> textures = new Array<String>();
        String path = "graphics/backgrounds/" + prefix;
        textures.add("graphics/galaxy/" + getGameSettings().backgroundMod + prefix + "galaxy.jpg");
        textures.add(path + "menuV2" + ".png");
        textures.add(path + "galaxyV3" + ".png");
        textures.add(path + "buildmenu" + ".jpg");
        textures.add(path + "workmenu" + ".jpg");
        textures.add(path + "diploinmenu" + ".jpg");
        textures.add(path + "emptyur" + ".jpg");
        textures.add(path + "intelassignmenu" + ".jpg");
        textures.add(path + "intelsabmenu" + ".jpg");
        textures.add(path + "newsovmenu" + ".jpg");
        textures.add(path + "shipovmenu" + ".jpg");
        textures.add(path + "trademenu" + ".jpg");
        textures.add(path + "urmenu" + ".jpg");
        textures.add(path + "demomenu" + ".jpg");
        textures.add(path + "diplomacyV3" + ".jpg");
        textures.add(path + "energymenu" + ".jpg");
        textures.add(path + "intelattackmenu" + ".jpg");
        textures.add(path + "intelspymenu" + ".jpg");
        textures.add(path + "overviewmenu" + ".jpg");
        textures.add(path + "systemovmenu" + ".jpg");
        textures.add(path + "tradetransfermenu" + ".jpg");
        textures.add(path + "victorymenu" + ".jpg");
        textures.add(path + "designmenu" + ".jpg");
        textures.add(path + "diplooutmenu" + ".jpg");
        textures.add(path + "fleetmenu" + ".jpg");
        textures.add(path + "intelinfomenu" + ".jpg");
        textures.add(path + "researchmenu" + ".jpg");
        textures.add(path + "systrademenu" + ".jpg");
        textures.add(path + "tradeV3" + ".jpg");
        textures.add(path + "diploinfomenu" + ".jpg");
        textures.add(path + "emptyscreen" + ".jpg");
        textures.add(path + "intelreportmenu" + ".jpg");
        textures.add(path + "monopolmenu" + ".jpg");
        textures.add(path + "researchV3" + ".jpg");
        textures.add(path + "top5menu" + ".jpg");
        textures.add(path + "transportmenu" + ".jpg");
        for (String s : textures)
            if (assetManager.isLoaded(s))
                assetManager.unload(s);
    }

    public BitmapFont getRacialFont(String raceFontName, int size) {
        //HACK//FIXME: to force the best font so far - fonts have to be edited to have roughly the same width
        raceFontName = "League Gothic";
        //HACK to compensate for some sizes
        if (raceFontName.equals("LilyUPC"))
            size += 4;
        if (raceFontName.equals("League Gothic"))
            size += 2;
        if (raceFontName.equals("Final Frontier"))
            size -= 3;
        if (raceFontName.equals("Mandala"))
            size -= 3;
        if (raceFontName.equals("Liberation Serif"))
            size -= 1;
        if (raceFontName.equals("KlingonDagger"))
            size -= 4;
        if (size < 1)
            size = 1;

        if (!getAssetManager().isLoaded(raceFontName + "size" + size + ".ttf")) {
            FreeTypeFontLoaderParameter param = new FreeTypeFontLoaderParameter();
            param.fontFileName = "fonts/" + raceFontName + ".ttf";
            param.fontParameters.size = size;
            param.fontParameters.genMipMaps = useMipMaps();
            param.fontParameters.minFilter = useMipMaps() ? TextureFilter.MipMapLinearLinear : TextureFilter.Linear;
            param.fontParameters.magFilter = TextureFilter.Linear;
            getAssetManager().load(raceFontName + "size" + size + ".ttf", BitmapFont.class, param);
            getAssetManager().finishLoading();
        }
        BitmapFont font = getAssetManager().get(raceFontName + "size" + size + ".ttf", BitmapFont.class);
        return font;
    }

    public void setProcessingTurn(boolean processing) {
        endturnProcessing = processing;
    }

    public boolean processingTurn() {
        return endturnProcessing;
    }

    public void setEndRoundPressed(boolean pressed) {
        endRoundPressed = pressed;
    }

    public boolean isEndRoundPressed() {
        return endRoundPressed;
    }

    public FileHandleResolver getResolver() {
        return fhResolver;
    }

    public DynamicTextureManager getDynamicTextureManager() {
        return dynamicTextureManager;
    }

    public void initSunPixmaps(final TextureAtlas atlas) {
        if(sunPixmaps.size == 0) {
            for(int i = 0; i < atlas.getRegions().size; i++){
                AtlasRegion sunRgn = atlas.getRegions().get(i);
                Pixmap sunpixmap = new Pixmap(GameConstants.screenshotMulti, GameConstants.screenshotMulti, Format.RGBA8888);
                TextureData data = sunRgn.getTexture().getTextureData();
                if(!data.isPrepared())
                    data.prepare();
                Pixmap pixmap = data.consumePixmap();
                sunpixmap.drawPixmap(pixmap, sunRgn.getRegionX(), sunRgn.getRegionY(), sunRgn.getRegionWidth(), sunRgn.getRegionHeight(), 0, 0, GameConstants.screenshotMulti, GameConstants.screenshotMulti);
                if(data.disposePixmap())
                    pixmap.dispose();
                sunPixmaps.put(sunRgn.name, sunpixmap);
            }
        }
    }

    public TextureRegion getUiTexture(String name) {
        return new TextureRegion(assetManager.get("graphics/ui/ui.pack", TextureAtlas.class).findRegion(name));
    }

    public Pixmap getSunPixmap(String name) {
        return sunPixmaps.get(name);
    }

    public AchievementManager getAchievementManager() {
        return achievementManager;
    }

    public PlatformApiIntegration getPlatformApiIntegration() {
        return apiIntegrator;
    }

    public void memInfo() {
        Runtime runtime = Runtime.getRuntime();

        NumberFormat format = NumberFormat.getInstance();

        StringBuilder sb = new StringBuilder();
        long maxMemory = runtime.maxMemory();
        long allocatedMemory = runtime.totalMemory();
        long freeMemory = runtime.freeMemory();

        sb.append("free memory: " + format.format(freeMemory / 1024) + "<br/>");
        sb.append("allocated memory: " + format.format(allocatedMemory / 1024) + "<br/>");
        sb.append("max memory: " + format.format(maxMemory / 1024) + "<br/>");
        sb.append("total free memory: " + format.format((freeMemory + (maxMemory - allocatedMemory)) / 1024) + "<br/>");
        System.out.println(sb);
    }

    @Override
    public void dispose() {
        super.dispose();
        uninit();
        if (achievementManager != null && achievementManager.isInitialized())
            achievementManager.dispose();
        assetManager.dispose();
        for (ObjectMap.Entry<String, Pixmap> e : sunPixmaps)
            e.value.dispose();
        sunPixmaps.clear();
        if (music != null && music.isPlaying())
            music.dispose();
    }

    public boolean useMipMaps() {
        if(useMipMaps == null)
            useMipMaps = !Gdx.gl.glGetString(GL20.GL_RENDERER).trim().equals("NVIDIA Tegra 3")
                    && !Gdx.gl.glGetString(GL20.GL_RENDERER).trim().equals("NVIDIA Tegra")
                    && !Gdx.gl.glGetString(GL20.GL_RENDERER).trim().equals("PowerVR SGX 540");
        return useMipMaps;
    }
}
