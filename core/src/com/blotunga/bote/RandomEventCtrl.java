/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote;

import java.util.Iterator;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.ObjectMap.Entry;
import com.blotunga.bote.events.EventRandom;
import com.blotunga.bote.galaxy.Sector;
import com.blotunga.bote.general.EmpireNews;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.general.EmpireNews.EmpireNewsType;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.races.Minor;
import com.blotunga.bote.ships.ShipMap;
import com.blotunga.bote.ships.Ships;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.utils.IntPoint;
import com.blotunga.bote.utils.RandUtil;

public class RandomEventCtrl {
    enum GLOBALEVENTTYPE {
        GLOBALEVENTSYSTEM,
        GLOBALEVENTRESEARCH,
        GLOBALEVENTMINOR
    }

    enum SYSTEMEVENTTYPE {
        SYSTEMEVENTMORALBOOST,
		// Moral bonus
        SYSTEMEVENTMORALMALUS,		// Moral malus
        SYSTEMEVENTPLANETMOVEMENT,
        SYSTEMEVENTDEMOGRAPHIC
    }

    enum EXPLOREEVENTTYPE {
        ALIENTEC,
        EVENTSHIPXP
    }

    private ResourceManager manager;

    public RandomEventCtrl(ResourceManager manager) {
        this.manager = manager;
    }

    public boolean isActivated() {
        return manager.getGamePreferences().randomIsActivated;
    }

    /**
     * In this function, we calculate whether an event fails or succeeds due to given probabilities.
     * Functions executed in succession only decide whether an event fails due to constraints
     * such as population already being at minimum.
     * @param race
     */
    public void calcEvents(Major race) {
        if (!isActivated())
            return;

        int eventType = (int) (RandUtil.random() * (GLOBALEVENTTYPE.GLOBALEVENTMINOR.ordinal() + 1));
        if (eventType == GLOBALEVENTTYPE.GLOBALEVENTSYSTEM.ordinal()) {//system affecting event
            //Calculate whether such event happens. The more systems we have, the more
            //likely it is that one of them is affected.
            Array<IntPoint> systems = race.getEmpire().getSystemList();
            int size = systems.size;
            int prob = Math.min(manager.getGamePreferences().randomProbPerSystem * size, 1000);
            if ((int) (RandUtil.random() * 999) >= prob)
                return;
            int whichSystem = (int) (RandUtil.random() * size);
            IntPoint coord = systems.get(whichSystem);
            //Major home systems are generally unaffected. Unbalances too much.
            if (race.getCoordinates().equals(coord))
                return;
            for (int i = 0; i < 100; i++)
                if (systemEvent(coord, race))
                    break;
        } else if (eventType == GLOBALEVENTTYPE.GLOBALEVENTRESEARCH.ordinal()) {
            if ((int) (RandUtil.random() * 99) >= manager.getGamePreferences().randomGlobalProb)
                return;
            globalEventResearch(race);
        } else { //event_type == GLOBALEVENTMINOR
            Array<Minor> possibleMinors = new Array<Minor>();
            ArrayMap<String, Minor> minors = manager.getRaceController().getMinors();
            for (Iterator<Entry<String, Minor>> iter = minors.entries().iterator(); iter.hasNext();) {
                ObjectMap.Entry<String, Minor> e = iter.next();
                if (race.isRaceContacted(e.key) && e.value.getRelation(race.getRaceId()) < 85
                        && !e.value.isSubjugated())
                    possibleMinors.add(e.value);
            }
            int size = possibleMinors.size;
            int prob = Math.min(manager.getGamePreferences().randomProbPerMinor * size, 1000);
            if ((int) (RandUtil.random() * 999) >= prob)
                return;
            globalEventMinor(race, possibleMinors.get((int) (RandUtil.random() * size)));
        }
    }

    /**
     * Calculates events when a sector is explored
     * @param coord
     * @param race
     * @param ships
     */
    public void calcExploreEvent(IntPoint coord, Major race, ShipMap ships) {
        if (!isActivated())
            return;

        if ((int) (RandUtil.random() * 99) >= manager.getGamePreferences().randomGlobalProb)
            return; //no event

        int eventNumber = (int) (RandUtil.random() * 2);
        String msgText = "";
        EmpireNewsType type = EmpireNewsType.NO_TYPE;
        String sectorName = manager.getUniverseMap().getStarSystemAt(coord).coordsName(true);

        if (eventNumber == EXPLOREEVENTTYPE.ALIENTEC.ordinal()) {
            //raise research by (50 -> 150) * number of systems
            int systemCount = race.getEmpire().getSystemList().size;
            int add = (50 + (int) (RandUtil.random() * 101)) * systemCount;
            race.getEmpire().addResearchPoints(add);

            msgText = StringDB.getString("ALIENTEC", false, sectorName);
            type = EmpireNewsType.RESEARCH;

            if (race.isHumanPlayer()) {
                EventRandom empireEvent = new EventRandom(manager, "alientech", StringDB.getString("ALIENTECHEADLINE"),
                        StringDB.getString("ALIENTECLONG", false, sectorName));
                race.getEmpire().pushEvent(empireEvent);
            }
        } else if (eventNumber == EXPLOREEVENTTYPE.EVENTSHIPXP.ordinal()) {
            for (int i = 0; i < ships.getSize(); i++) {
                Ships ship = ships.getAt(i);
                if (ship.getOwnerId().equals(race.getRaceId()) && ship.getCoordinates().equals(coord)) {
                    int additionalExp = (int) (RandUtil.random() * 401) + 50;
                    ship.setCrewExperience(additionalExp);
                }
            }
            msgText = StringDB.getString("EVENTSHIPXP", false, sectorName);
            type = EmpireNewsType.MILITARY;
        }

        if (!msgText.isEmpty()) {
            EmpireNews message = new EmpireNews();
            message.CreateNews(msgText, type, "", coord);
            race.getEmpire().addMsg(message);
            manager.getClientWorker().setEmpireViewFor(race);
        }
    }

    /**
     * Function calculates ship events
     */
    public void calcShipEvents() {
        if (!isActivated())
            return;

        //Hull virus
        for (int x = 0; x < manager.getGridSizeX(); x++)
            for (int y = 0; y < manager.getGridSizeY(); y++) {
                if ((int) (RandUtil.random() * 1000) != 0)
                    continue;

                Sector sector = manager.getUniverseMap().getStarSystemAt(x, y);
                //no ships, do nothing
                if (!sector.getIsShipInSector())
                    continue;

                for (int i = 0; i < manager.getUniverseMap().getShipMap().getSize(); i++) {
                    Ships ship = manager.getUniverseMap().getShipMap().getAt(i);
                    if (ship.isAlien())
                        continue;

                    if (!ship.getCoordinates().equals(sector.getCoordinates()))
                        continue;

                    int currentHull = ship.getHull().getCurrentHull();
                    ship.getHull().setCurrentHull(-(currentHull - 1), true);

                    for (int j = 0; j < ship.getFleetSize(); j++) {
                        Ships s = ship.getFleet().getAt(j);
                        currentHull = s.getHull().getCurrentHull();
                        s.getHull().setCurrentHull(-(currentHull - 1), true);
                    }
                }

                //create message for all majors that have ships here
                ArrayMap<String, Major> majors = manager.getRaceController().getMajors();
                for (int i = 0; i < majors.size; i++) {
                    String majorID = majors.getKeyAt(i);
                    Major major = majors.getValueAt(i);

                    if (!sector.getOwnerOfShip(majorID, true))
                        continue;

                    String sectorName = sector.coordsName(true);

                    String msgText = StringDB.getString("EVENTHULLVIRUS", false, sectorName);
                    EmpireNews message = new EmpireNews();
                    message.CreateNews(msgText, EmpireNewsType.MILITARY, sector.getName(), sector.getCoordinates());
                    major.getEmpire().addMsg(message);

                    if (major.isHumanPlayer()) {
                        manager.getClientWorker().setEmpireViewFor(major);
                        major.getEmpire().pushEvent(new EventRandom(manager, "HullVirus", msgText, ""));
                    }
                }
            }
    }

    private boolean systemEvent(IntPoint coord, Major race) {
        String msgText = "";
        int eventNumber = (int) (RandUtil.random() * SYSTEMEVENTTYPE.SYSTEMEVENTDEMOGRAPHIC.ordinal() + 1);
        StarSystem ss = manager.getUniverseMap().getStarSystemAt(coord);

        if (eventNumber == SYSTEMEVENTTYPE.SYSTEMEVENTMORALBOOST.ordinal()) {
            msgText = StringDB.getString("SYSTEMEVENTMORALBOOST", false, ss.getName());
            ss.setMorale(10);
        } else if (eventNumber == SYSTEMEVENTTYPE.SYSTEMEVENTMORALMALUS.ordinal()) {
            msgText = StringDB.getString("SYSTEMEVENTMORALMALUS", false, ss.getName());
            ss.setMorale(-10);
        } else if (eventNumber == SYSTEMEVENTTYPE.SYSTEMEVENTPLANETMOVEMENT.ordinal())
            msgText = ss.systemEventPlanetMovement();
        else if (eventNumber == SYSTEMEVENTTYPE.SYSTEMEVENTDEMOGRAPHIC.ordinal())
            msgText = ss.systemEventDemographic(race);
        if (!msgText.isEmpty()) {
            EmpireNews message = new EmpireNews();
            message.CreateNews(msgText, EmpireNewsType.SOMETHING, "", coord);
            race.getEmpire().addMsg(message);
            manager.getClientWorker().setEmpireViewFor(race);
        }
        return !msgText.isEmpty();
    }

    private void globalEventResearch(Major race) {
        String msgText = StringDB.getString("GLOBALEVENTRESEARCH");
        race.getEmpire().addResearchPoints(race.getEmpire().getResearchPoints());
        EmpireNews message = new EmpireNews();
        message.CreateNews(msgText, EmpireNewsType.RESEARCH);
        race.getEmpire().addMsg(message);

        if (race.isHumanPlayer()) {
            manager.getClientWorker().setEmpireViewFor(race);
            EventRandom empireEvent = new EventRandom(manager, "Breakthrough", StringDB.getString("BREAKTHROUGH"),
                    StringDB.getString("GLOBALEVENTRESEARCH"));
            race.getEmpire().pushEvent(empireEvent);
        }
    }

    private void globalEventMinor(Major race, Minor minor) {
        minor.setRelation(race.getRaceId(), (int) (RandUtil.random() * 101) - minor.getRelation(race.getRaceId()));
        String msgText = StringDB.getString("GLOBALEVENTMINOR", false, minor.getName());
        EmpireNews message = new EmpireNews();
        message.CreateNews(msgText, EmpireNewsType.DIPLOMACY);
        race.getEmpire().addMsg(message);
    }
}
