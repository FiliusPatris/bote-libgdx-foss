/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ships;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.ObjectIntMap;
import com.badlogic.gdx.utils.ObjectSet;
import com.badlogic.gdx.utils.ObjectSet.ObjectSetIterator;
import com.blotunga.bote.constants.AnomalyType;
import com.blotunga.bote.constants.CombatTactics;
import com.blotunga.bote.constants.DiplomaticAgreement;
import com.blotunga.bote.constants.ShipSpecial;
import com.blotunga.bote.constants.ShipType;
import com.blotunga.bote.galaxy.Anomaly;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.races.Race;
import com.blotunga.bote.races.RaceController;
import com.blotunga.bote.utils.IntPoint;
import com.blotunga.bote.utils.RandUtil;
import com.blotunga.bote.utils.Vec3i;

public class Combat {
    private Array<CombatShip> involvedShips_;				///< the ships which are involved in combat
    private ArrayMap<String, Array<CombatShip>> enemies_;	///< all the enemies of an empire
    private Array<Torpedo> ct_;								///< list of all fired and existing torpedoes
    private boolean ready_;									///< preparations are done for combat
    private int time_;										///< the current time (round) in combat
    private boolean attackedSomebody_;						///< stores whether somebody attacked somebody in this battle
    private ObjectSet<String> involvedRaces_;				///< the list of involved races
    private RaceController races_;							///< the RaceController
    private ArrayMap<Ships, ObjectSet<Ships>> killedShips_;	///< this stores which ship has killed what other ships
    private ObjectIntMap<String> winner_;
    private int timeSinceLastFire_;

    public Combat(RaceController races) {
        this.races_ = races;
        init();
    }

    public Combat(Combat other) {
        races_ = other.races_;
        init();
        for (int i = 0; i < other.involvedShips_.size; i++)
            involvedShips_.add(new CombatShip(other.involvedShips_.get(i)));
        for (ObjectSetIterator<String> iter = other.involvedRaces_.iterator(); iter.hasNext(); )
            involvedRaces_.add(iter.next());
        ready_ = other.ready_;
    }

    private void init() {
        involvedShips_ = new Array<CombatShip>();
        enemies_ = new ArrayMap<String, Array<CombatShip>>();
        ct_ = new Array<Torpedo>();
        ready_ = false;
        time_ = 0;
        attackedSomebody_ = false;
        involvedRaces_ = new ObjectSet<String>();
        killedShips_ = new ArrayMap<Ships, ObjectSet<Ships>>(false, 16);
        timeSinceLastFire_ = 0;
    }

    public void reset() {
        involvedShips_.clear();
        enemies_.clear();
        ct_.clear();
        ready_ = false;
        time_ = 0;
        attackedSomebody_ = false;
        involvedRaces_.clear();
        killedShips_.clear();
    }

    public boolean isReadyForCombat() {
        return ready_;
    }

    /**
     * This fills the members for combat
     * @param ships
     * @param anomaly
     */
    public void setInvolvedShips(Array<Ships> ships, Anomaly anomaly) {
        reset();
        for (int i = 0; i < ships.size; i++)
            involvedRaces_.add(ships.get(i).getOwnerId());

        for (int i = 0; i < races_.getRaces().size; i++) {
            Race firstRace = races_.getRaces().getValueAt(i);
            String firstID = races_.getRaces().getKeyAt(i);
            if (!ready_) {
                for (int j = 0; j < races_.getRaces().size; j++) {
                    Race otherRace = races_.getRaces().getValueAt(j);
                    String otherID = races_.getRaces().getKeyAt(j);
                    if (!firstID.equals(otherID) && involvedRaces_.contains(firstID)
                            && involvedRaces_.contains(otherID))
                        if (Combat.checkDiplomacyStatus(firstRace, otherRace)) {
                            ready_ = true;
                            break;
                        }
                }
            }
        }
        if (ready_) {
            for (int i = 0; i < ships.size; i++) {
                Ships ship = ships.get(i);
                CombatShip cs = new CombatShip(ship);
                cs.setManeuverability(ship.getManeuverabilty());
                cs.coord = new Vec3i(0, 0, 0);

                if (anomaly != null) {
                    if (anomaly.getType() == AnomalyType.METNEBULA || anomaly.getType() == AnomalyType.TORIONGASNEBULA)
                        cs.canUseShields = false;
                    if (anomaly.getType() == AnomalyType.TORIONGASNEBULA)
                        cs.canUseTorpedoes = false;
                    if (anomaly.getType() == AnomalyType.BINEBULA)
                        cs.fasterShieldRecharge = true;
                }

                if (ship.isCloakOn())
                    cs.cloak = (int) (RandUtil.random() * 21 + 50);
                involvedShips_.add(cs);
            }
        }
    }

    //TODO: ??? applyShipFormation and applyShipTactic ???
    /**
     * This function has to be called before calculateCombat()
     */
    public void preCombatCalculation() {
        if (!ready_)
            return;
        //1. set the position of the ships at the start of the battle.
        // The center is 0, 0, 0 and the fleets are 200 units away
        // there are 7 positions around the center, 6 are for the main races and one extra

        int racePos = 0;
        for (ObjectSetIterator<String> iter = involvedRaces_.iterator(); iter.hasNext();) {
            String entry = iter.next();
            //the number of the ship of this race on this position - needed for formations
            int shipPos = 0;
            // does the race have the flagship in battle?
            boolean flagship = false;
            // does the race have a ship with commandship ability in battle?
            boolean commandship = false;
            // number of different ships (for mixed fleet bonus)
            int shipTypeScout = 0;
            int shipTypeFighter = 0;
            int shipTypeFrigate = 0;
            int shipTypeDestroyer = 0;
            int shipTypeCruiser = 0;
            int shipTypeHeavyDestroyer = 0;
            int shipTypeHeavyCruiser = 0;
            int shipTypeBattleShip = 0;
            int shipTypeDreadnought = 0;

            for (int i = 0; i < involvedShips_.size; i++) {
                CombatShip cs = involvedShips_.get(i);
                if (!cs.ship.getOwnerId().equals(entry))
                    continue;

                cs.coord = givePosition(racePos, shipPos++);

                //if a ship is in retreat mode, its bonuses won't apply to other ships
                if (cs.ship.getCombatTactics() == CombatTactics.CT_RETREAT)
                    continue;

                if (cs.ship.isFlagShip())
                    flagship = true;
                if (cs.ship.hasSpecial(ShipSpecial.COMMANDSHIP))
                    commandship = true;
                //count ship types for mixed fleet bonus
                if (cs.ship.getShipType() == ShipType.SCOUT)
                    shipTypeScout++;
                if (cs.ship.getShipType() == ShipType.FIGHTER)
                    shipTypeFighter++;
                if (cs.ship.getShipType() == ShipType.FRIGATE)
                    shipTypeFrigate++;
                if (cs.ship.getShipType() == ShipType.DESTROYER)
                    shipTypeDestroyer++;
                if (cs.ship.getShipType() == ShipType.CRUISER)
                    shipTypeCruiser++;
                if (cs.ship.getShipType() == ShipType.HEAVY_DESTROYER)
                    shipTypeHeavyDestroyer++;
                if (cs.ship.getShipType() == ShipType.HEAVY_CRUISER)
                    shipTypeHeavyCruiser++;
                if (cs.ship.getShipType() == ShipType.BATTLESHIP)
                    shipTypeBattleShip++;
                if (cs.ship.getShipType() == ShipType.DREADNOUGHT)
                    shipTypeDreadnought++;
            }
            //mixed fleet bonus: bonus per ship for the first five ships per type
            int mod = 0;
            if (shipTypeScout > 0)
                mod += Math.min(shipTypeScout, 5);
            if (shipTypeFighter > 0)
                mod += Math.min(shipTypeFighter, 5);
            if (shipTypeFrigate > 0)
                mod += Math.min(shipTypeFrigate, 5);
            if (shipTypeDestroyer > 0)
                mod += Math.min(shipTypeDestroyer, 5);
            if (shipTypeCruiser > 0)
                mod += Math.min(shipTypeCruiser, 5);
            if (shipTypeHeavyDestroyer > 0)
                mod += Math.min(shipTypeHeavyDestroyer, 5);
            if (shipTypeHeavyCruiser > 0)
                mod += Math.min(shipTypeHeavyCruiser, 5);
            if (shipTypeBattleShip > 0)
                mod += Math.min(shipTypeBattleShip, 5);
            if (shipTypeDreadnought > 0)
                mod += Math.min(shipTypeDreadnought, 5);

            //now add the bonus/malus for the ships
            for (int i = 0; i < involvedShips_.size; i++) {
                CombatShip cs = involvedShips_.get(i);
                if (cs.ship.getOwnerId().equals(entry)) {
                    if (commandship)
                        cs.modifier += 10;
                    if (flagship)
                        cs.modifier += 20;
                    cs.modifier += mod;
                    cs.modifier += cs.getCrewExperienceModifier();
                    if (cs.modifier <= 0)
                        cs.modifier = 1;
                } else {
                    if (Combat.checkDiplomacyStatus(races_.getRace(entry), races_.getRace(cs.ship.getOwnerId()))) {
                        Array<CombatShip> csAr = enemies_.get(entry);
                        if (csAr == null)
                            csAr = new Array<CombatShip>();
                        csAr.add(cs);
                        enemies_.put(entry, csAr);
                    }
                }
            }
            racePos++;
        }
    }

    private static Vec3i givePosition(int pos, int posNumber) {
        Vec3i p = new Vec3i();
        p.x = (int) (RandUtil.random() * 30);
        p.y = (int) (RandUtil.random() * 30);
        p.z = (int) (RandUtil.random() * 30);

        switch (pos % 6) {
            case 0:
                p.x = 200 + posNumber * 5;
                break;
            case 1:
                p.x = -200 - posNumber * 5;
                break;
            case 2:
                p.y = 200 + posNumber * 5;
                break;
            case 3:
                p.y = -200 - posNumber * 5;
                break;
            case 4:
                p.z = 200 + posNumber * 5;
                break;
            case 5:
                p.z = -200 - posNumber * 5;
                break;
        }
        return p;
    }

    /**
     * The function calculates the combat
     * @return - A map with the winner
     */
    public ObjectIntMap<String> calculateCombat() {
        while (ready_)
            calculateCombatSingleTick();
        calculateWinner();
        return winner_;
    }

    public void calculateCombatSingleTick() {
        if (ready_) {
            ready_ = false;
            time_++;
            timeSinceLastFire_++;
            if (time_ > Short.MAX_VALUE) {
                ready_ = false;
                return;
            }
            //if we have not attacked anyone for 20 rounds, then we stop (for example all ships are cloaked)
            if (!attackedSomebody_ && time_ > 20) {
                ready_ = false;
                return;
            }
            if (attackedSomebody_ && timeSinceLastFire_ > 30) { //stop because nothing is going on
                ready_ = false;
                return;
            }

            //calculate route for all ships and set possible target
            for (int i = 0; i < involvedShips_.size; i++) {
                //if it's cloaked yet we can detect it, make it visible, otherwise hide it
                CombatShip cs = involvedShips_.get(i);
                if (cs.target != null && cs.target.cloak > 0) {
                    if (cs.ship.getScanPower() > cs.target.ship.getStealthGrade() * 20)
                        cs.target.shootCloaked = true;
                    else {
                        cs.target = null;
                        cs.fire.phaserIsShooting = false;
                    }
                }

                //the ship has no target yet
                if (cs.target == null && cs.ship.getCombatTactics() == CombatTactics.CT_ATTACK) {
                    if (!setTarget(i))
                        continue;
                } else if (cs.ship.getCombatTactics() == CombatTactics.CT_RETREAT) {
                    //consider combat as happened - so it won't be stopped after 20 ticks - a retreat is longer
                    attackedSomebody_ = true;
                }

                ready_ = true;
                cs.calculateNextPosition();

                //if we have a target, we'll attack it
                if (cs.target != null) {
                    attackedSomebody_ = true;
                    // --- BEAM WEAPONS ---
                    //as long we have destroyed something with them, we can aquire a new target
                    IntPoint value = new IntPoint(0, 0);
                    do {
                        timeSinceLastFire_ = 0;
                        value = cs.attackEnemyWithBeam(value);
                        //if value not -1, then we have destroyed a ship an can target another
                        if (value.x != -1) {
                            //null current target and pick a new one
                            for (int j = 0; j <= involvedShips_.size; j++)
                                if (involvedShips_.get(j) == cs.target) {
                                    if (checkShipStayInCombat(j)) //should be false every time
                                        System.err.println("ERROR in CalculateCombat()");
                                    break;
                                }

                            if (!setTarget(i))
                                break;
                        }

                    } while (value.x != -1);
                    // --- TORPEDO WEAPONS ---
                    //check if we have a target, otherwise get a new one
                    if (cs.target == null) {
                        if (!setTarget(i))
                            continue;
                    }

                    if (cs.canUseTorpedoes) {
                        timeSinceLastFire_ = 0;
                        cs.attackEnemyWithTorpedo(ct_);
                    }
                }
            }

            //go through torpedoes and calculate their trajectory
            for (int i = 0; i < ct_.size; i++) {
                Torpedo torpedo = ct_.get(i);
                if (torpedo.fly(involvedShips_)) {
                    torpedo = null;
                    ct_.removeIndex(i--);
                }
            }

            if (ct_.size != 0)
                ready_ = true;

            //if there are only unmovable ships
            boolean onlyRetreatShipsWithSpeedNull = true;

            for (int i = 0; i < involvedShips_.size; i++) {
                if (!checkShipStayInCombat(i)) {
                    involvedShips_.removeIndex(i--);
                    continue;
                }

                CombatShip cs = involvedShips_.get(i);
                if (cs.ship.getCombatTactics() != CombatTactics.CT_RETREAT || cs.maneuverability > 0)
                    onlyRetreatShipsWithSpeedNull = false;

                cs.gotoNextPosition();
                //if the shields still hold
                if (cs.ship.getShield().getCurrentShield() > 0) {
                    cs.ship.getShield().rechargeShields();
                    if (cs.fasterShieldRecharge)
                        cs.ship.getShield().rechargeShields();
                }
            }

            if (onlyRetreatShipsWithSpeedNull) {
                involvedShips_.clear();
                //the result shall be undecided
                attackedSomebody_ = false;
            }

            if (involvedShips_.size == 0) {
                ready_ = false;
                return;
            }
        }
    }

    /**
     * The function calculates the winner of combat - to be used only when ready_ is false
     * @return - A map with the winner
     */
    public void calculateWinner() {
        winner_ = new ObjectIntMap<String>();
        //after the battle remove all ships with retreat command
        for (int i = 0; i < involvedShips_.size; i++)
            if (involvedShips_.get(i).ship.getCombatTactics() == CombatTactics.CT_RETREAT)
                involvedShips_.removeIndex(i--);

        if (attackedSomebody_) {
            //After the battle check who has still some ships left. These races have won.
            for (ObjectSetIterator<String> iter = involvedRaces_.iterator(); iter.hasNext();) {
                String entry = iter.next();
                winner_.put(entry, 2);
            }
            for (int i = 0; i < involvedShips_.size; i++)
                winner_.put(involvedShips_.get(i).ship.getOwnerId(), 1);

            //if a draw was reached or there are no more winners and these have no diplomatic agreements to not attack eachother
            for (int i = 0; i < races_.getRaces().size; i++) {
                Race firstRace = races_.getRaces().getValueAt(i);
                String firstID = races_.getRaces().getKeyAt(i);
                if (involvedRaces_.contains(firstID)) {
                    for (ObjectSetIterator<String> iter = involvedRaces_.iterator(); iter.hasNext();) {
                        String entry = iter.next();
                        if (!entry.equals(firstID) && winner_.get(firstID, 0) == 1 && winner_.get(entry, 0) == 1) {
                            if (Combat.checkDiplomacyStatus(firstRace, races_.getRace(entry))) {
                                winner_.put(firstID, 3);
                                winner_.put(entry, 3);
                            }
                        }
                    }
                }
            }
        } else {
            for (ObjectSetIterator<String> iter = involvedRaces_.iterator(); iter.hasNext();) {
                String entry = iter.next();
                winner_.put(entry, 3);
            }
        }
    }

    public ObjectIntMap<String> getWinner() {
        return winner_;
    }

    /**
     * The function returns the killed ships
     * @return
     */
    public ArrayMap<Ships, ObjectSet<Ships>> getKilledShipInfos() {
        return killedShips_;
    }

    public static double getWinningChance(Race ourRace, Array<Ships> involvedShips, RaceController races,
            ObjectSet<String> friends, ObjectSet<String> enemies, Anomaly anomaly, boolean limit) {
        return getWinningChance(ourRace, involvedShips, races, friends, enemies, anomaly, false, limit);
    }

    public static double getWinningChance(Race ourRace, Array<Ships> involvedShips, RaceController races,
            ObjectSet<String> friends, ObjectSet<String> enemies, Anomaly anomaly, boolean includeFleet, boolean limit) {
        double winningChance = 0.5;
        double ourStrength = 0.0;
        double enemyStrength = 0.0;
        double ourOffensive = 0.0;
        double enemyOffensive = 0.0;

        friends.clear();
        enemies.clear();

        boolean canUseShields = true;
        boolean canUseTorpedos = true;

        if (anomaly != null) {
            if (anomaly.getType() == AnomalyType.METNEBULA || anomaly.getType() == AnomalyType.TORIONGASNEBULA)
                canUseShields = false;
            if (anomaly.getType() == AnomalyType.TORIONGASNEBULA)
                canUseTorpedos = false;
        }
        for (int i = 0; i < involvedShips.size; i++) {
            Ships ship = involvedShips.get(i);

            double offensive = ship.getCompleteOffensivePower(true, canUseTorpedos, includeFleet);
            double defensive = ship.getCompleteDefensivePower(canUseShields, true, includeFleet) / 2.0;

            if (ship.getOwnerId().equals(ourRace.getRaceId())) {
                friends.add(ourRace.getRaceId());
                ourOffensive += offensive;
                ourStrength += offensive + defensive;
            } else {
                Race otherRace = ship.getOwner();
                if (checkDiplomacyStatus(ourRace, otherRace)) {
                    enemies.add(otherRace.getRaceId());
                    enemyOffensive += offensive;
                    enemyStrength += offensive + defensive;
                } else {
                    friends.add(otherRace.getRaceId());
                    ourOffensive += offensive;
                    ourStrength += offensive + defensive;
                }
            }
        }

        if (ourOffensive == 0.0)
            winningChance = 0.0;
        else if (enemyOffensive == 0.0)
            winningChance = 1.0;
        else if (enemyStrength > 0.0)
            winningChance = ourStrength / enemyStrength / 2.0;

        if (limit) {
            winningChance = Math.min(0.99, winningChance);
            winningChance = Math.max(0.01, winningChance);
        }

        return winningChance;
    }

    /**
     * The function
     * @param raceA
     * @param raceB
     * @return
     */
    public static boolean checkDiplomacyStatus(Race raceA, Race raceB) {
        if (raceA.getAgreement(raceB.getRaceId()) == DiplomaticAgreement.NONE
                || raceA.getAgreement(raceB.getRaceId()) == DiplomaticAgreement.TRADE
                || raceA.getAgreement(raceB.getRaceId()) == DiplomaticAgreement.WAR) {
            if (raceA.isMajor() && raceB.isMajor()) {
                if (!Major.toMajor(raceA).getDefencePact(raceB.getRaceId()))
                    return true;
                else
                    return false;
            } else
                return true;
        } else
            return false;
    }

    /**
     * The function sets the i-th ships target
     * @param i
     * @return true if a target could be assigned
     */
    private boolean setTarget(int i) {
        CombatShip cs = involvedShips_.get(i);
        Ships ship = cs.ship;
        String owner = ship.getOwnerId();

        while (enemies_.get(owner) != null && enemies_.get(owner).size > 0) {
            int random = (int) (RandUtil.random() * enemies_.get(owner).size);
            CombatShip targetShip = enemies_.get(owner).get(random);

            if (!targetShip.ship.isAlive()) {
                System.err.println("ERROR in Combat SetTarget");
                break;
            }

            if (targetShip.cloak == 0) {
                cs.target = targetShip;
                cs.fire.phaserIsShooting = false;
            } else if (ship.getScanPower() > targetShip.ship.getStealthGrade() * 20) {
                targetShip.shootCloaked = true;
                cs.fire.phaserIsShooting = false;
            }

            if (cs.reCloak == 255) {
                if (ship.canCloak() && cs.cloak == 0) {
                    cs.cloak = (int) (RandUtil.random() * 21 + 50);
                    cs.shootCloaked = false;
                    cs.reCloak = 0;
                }
            }
            return true;
        }

        return false;
    }

    /**
     * This function checks if the ship can continue in the battle. If the ship is destroyed or retreated then it can't and then this function removes it from combat
     * @param i
     * @return
     */
    private boolean checkShipStayInCombat(int i) {
        CombatShip cs = involvedShips_.get(i);

        boolean isAlive = true;
        if (!cs.ship.isAlive()) {
            isAlive = false;
            ObjectSet<Ships> ships = killedShips_.get(cs.killedByShip);
            if (ships == null)
                ships = new ObjectSet<Ships>();
            ships.add(cs.ship);
            killedShips_.put(cs.killedByShip, ships);
        } else if ((cs.ship.getCombatTactics() == CombatTactics.CT_RETREAT) && (cs.retreatCounter == 0)
                && (cs.route.size == 0)) {
            isAlive = false;
        }

        //before flying to the next position, check if the ship s still alive, otherwise remove from involvedShips
        if (!isAlive) {
            //if someone had it targeted, remove the target
            for (int j = 0; j < involvedShips_.size; j++) {
                CombatShip otherCs = involvedShips_.get(j);
                if (otherCs.target == cs) {
                    otherCs.fire.phaserIsShooting = false;
                    otherCs.target = null;
                    for (int t = 0; t < otherCs.fire.phaser.size; t++)
                        if (otherCs.fire.phaser.get(t) > otherCs.ship.getBeamWeapons().get(t).getRechargeTime())
                            otherCs.fire.phaser.set(t, otherCs.ship.getBeamWeapons().get(t).getRechargeTime());
                }
            }

            for (int j = 0; j < enemies_.size; j++) {
                Array<CombatShip> vec = enemies_.getValueAt(j);
                for (int k = 0; k < vec.size; k++)
                    if (vec.get(k) == cs) {
                        vec.removeIndex(k);
                        break;
                    }
            }
            return false;
        }

        return true;
    }

    public Array<CombatShip> getInvolvedShips() {
        return involvedShips_;
    }

    public Array<Torpedo> getTorpedoes() {
        return ct_;
    }
}
