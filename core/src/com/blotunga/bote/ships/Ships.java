/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ships;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap.Entry;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.constants.CombatTactics;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.ResearchComplexType;
import com.blotunga.bote.constants.ShipOrder;
import com.blotunga.bote.constants.ShipRange;
import com.blotunga.bote.galaxy.Sector;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.races.Race;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.utils.IntPoint;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

public class Ships extends Ship {
    private ShipMap fleet;
    private transient int key; 		// key of this ship in the shipmap
    private transient int mapTileKey;
    private transient boolean leaderCurrent;
    private transient Array<Image> pathImgs;

    public enum RetreatMode {
        RETREAT_MODE_STAY, RETREAT_MODE_SPLITTED, RETREAT_MODE_COMPLETE
    }

    public class StationWorkResult {
        public boolean removeLeader;
        public boolean finished;

        public StationWorkResult() {
            removeLeader = false;
            finished = false;
        }
    }

    public Ships() {
        this((ResourceManager) null);
    }

    public Ships(ResourceManager res) {
        super(res);
        fleet = new ShipMap();
        key = 0;
        mapTileKey = 0;
        leaderCurrent = true;
    }

    public Ships(Ships ships) {
        super(ships);
        fleet = new ShipMap();
        fleet.append(ships.fleet);
        key = ships.key;
        mapTileKey = ships.mapTileKey;
        leaderCurrent = ships.leaderCurrent;
    }

    public Ships(Ship ship) {
        super(ship);
        fleet = new ShipMap();
        key = 0;
        mapTileKey = 0;
        leaderCurrent = true;
    }

    public int currentShip() {
        return fleet.currentShip();
    }

    public int getFleetSize() {
        return fleet.getSize();
    }

    public ShipMap getFleet() {
        return fleet;
    }

    public int getKey() {
        return key;
    }

    public int getMapTileKey() {
        return mapTileKey;
    }

    public boolean leaderIsCurrent() {
        return leaderCurrent;
    }

    public float getStartInhabitants() {
        return getColonizePoints() * 4;
    }

    public String getRangeAsString() {
        return StringDB.getString(getRange(true).getName());
    }

    public String getFleetName() {
        String info = getName();
        if (info.length() > 4 && info.charAt(3) == ' ')
            info = info.substring(4);
        if (info.length() > 2 && info.lastIndexOf(' ') > 0)
            info = info.substring(0, info.lastIndexOf(' '));
        return info + " " + StringDB.getString("GROUP");
    }

    private void adoptOrdersFrom(Ships ship) {
        super.adoptOrdersFrom(ship);
        for (Entry<Integer, Ships> s : fleet.getShipMap())
            s.value.adoptOrdersFrom(ship);
    }

    public void addShipToFleet(Ships ship) {
        ShipOrder order = getCurrentOrder();
        if (!ship.canHaveOrder(order, false))
            unsetCurrentOrder();
        ship.adoptOrdersFrom(this);
        fleet.add(ship);
        if (ship.hasFleet()) {
            fleet.append(ship.fleet);
            ship.reset();
        }
    }

    public void setCurrentShip(int index) {
        if (index == fleet.getSize() - 1) {
            leaderCurrent = true;
            return;
        }
        leaderCurrent = false;
        fleet.setCurrentShip(index);
    }

    public void removeShipFromFleet(int index) {
        fleet.eraseAt(index);
        if (!hasFleet())
            reset();
    }

    /*
     * strip this Ships from destroyed ships, return true if the leading ship is still alive, false if the leader is dead and possibly ships in the fleet remain
     */
    @Override
    public boolean removeDestroyed(Race owner, int round, String event, String status, Array<String> destroyedShips) {
        return removeDestroyed(owner, round, event, status, destroyedShips, "");
    }

    @Override
    public boolean removeDestroyed(Race owner, int round, String event, String status, Array<String> destroyedShips,
            String anomaly) {
        for (int i = 0; i < fleet.getSize(); i++) {
            if (fleet.getAt(i).removeDestroyed(owner, round, event, status, destroyedShips, anomaly))
                continue;
            removeShipFromFleet(i--);
        }
        return super.removeDestroyed(owner, round, event, status, destroyedShips, anomaly);
    }

    @Override
    public void reset() {
        fleet.reset();
        leaderCurrent = true;
    }

    public void applyTraining(int xp) {
        boolean veteran = hasVeteran();
        super.applyTraining(xp, veteran);
        for (Entry<Integer, Ships> s : fleet.getShipMap())
            s.value.applyTraining(xp, veteran);
    }

    public void applyIonStormEffects(ShipMap toShips) {
        for (int i = 0; i < fleet.getSize(); i++) {
            Ships s = fleet.getAt(i);
            if (s.applyIonStormEffects()) {
                if (s.getCurrentOrder() == ShipOrder.IMPROVE_SHIELDS) {
                    s.unsetCurrentOrder();
                    toShips.add(s);
                    removeShipFromFleet(i--);
                }
            }
        }
        if (super.applyIonStormEffects()) {
            if (super.getCurrentOrder() == ShipOrder.IMPROVE_SHIELDS) {
                super.unsetCurrentOrder();
                if (hasFleet())
                    toShips.add(giveFleetToFleetsFirstShip());
            }
        }
    }

    @Override
    public void addSpecialResearchBoni(Race owner, ResearchComplexType type) {
        super.addSpecialResearchBoni(owner, type);
        for (int i = 0; i < fleet.getSize(); i++)
            fleet.getAt(i).addSpecialResearchBoni(owner, type);
    }

    /**
     * remove any possible flagship status of the leader and the fleet in this Ships
     * @return was it found and removed (process terminates, as only one flagship can exist)
     */
    @Override
    public boolean unassignFlagship() {
        for (Entry<Integer, Ships> s : fleet.getShipMap())
            if (s.value.unassignFlagship())
                return true;
        return super.unassignFlagship();
    }

    public void setKey(int key) {
        this.key = key;
    }

    public void setMapTileKey(int mtk) {
        this.mapTileKey = mtk;
    }

    public void setCoordinatesSuper(IntPoint p) {
        super.setCoordinates(p);
    }

    /**
     * @param p
     * @param newOwner - used when transferring ownership of ships in case of sabotage actions
     */
    public void setCoordinates(IntPoint p, String newOwner) {
        boolean newlyCreated = getCoordinates().equals(new IntPoint());
        if (!newlyCreated) {
            StarSystem system = resourceManager.getUniverseMap().getStarSystemAt(getCoordinates());
            system.eraseShip(this);
        }
        if (!newOwner.isEmpty())
            setOwner(newOwner);
        super.setCoordinates(p);
        for (Entry<Integer, Ships> s : fleet.getShipMap())
            s.value.setCoordinatesSuper(p);
        if (!newlyCreated) {
            StarSystem system = resourceManager.getUniverseMap().getStarSystemAt(getCoordinates());
            system.addShip(this);
        }
    }

    @Override
    public void setCoordinates(IntPoint p) {
        setCoordinates(p, "");
    }

    @Override
    public void setOwner(String id) {
        super.setOwner(id);
        for (Entry<Integer, Ships> s : fleet.getShipMap())
            s.value.setOwner(id);
    }

    @Override
    public void setCloak(boolean cloakOn) {
        super.setCloak(cloakOn);
        for (Entry<Integer, Ships> s : fleet.getShipMap())
            s.value.setCloak(cloakOn);
    }

    @Override
    public void setCurrentOrder(ShipOrder order) {
        super.setCurrentOrder(order);
        for (Entry<Integer, Ships> s : fleet.getShipMap())
            s.value.setCurrentOrder(order);
    }

    @Override
    public void setTerraform() {
        this.setTerraform(-1);
    }

    @Override
    public void setTerraform(int planetNumber) {
        super.setTerraform(planetNumber);
        for (Entry<Integer, Ships> s : fleet.getShipMap())
            s.value.setTerraform(planetNumber);
    }

    public void setCombatTactics(CombatTactics tactics) {
        this.setCombatTactics(tactics, true);
    }

    @Override
    public void setCombatTactics(CombatTactics tactics, boolean propagateToFleet) {
        this.setCombatTactics(tactics, propagateToFleet, true);
    }

    public void setCombatTactics(CombatTactics tactics, boolean propagateToFleet, boolean alsoIfRetreat) {
        super.setCombatTactics(tactics, alsoIfRetreat);
        if (propagateToFleet)
            for (Entry<Integer, Ships> s : fleet.getShipMap())
                s.value.setCombatTactics(tactics, false, alsoIfRetreat);
    }

    @Override
    public void setTargetCoord(IntPoint target) {
        setTargetCoord(target, false);
    }

    @Override
    public void setTargetCoord(IntPoint target, boolean simpleSetter) {
        super.setTargetCoord(target, simpleSetter);
        for (Entry<Integer, Ships> s : fleet.getShipMap())
            s.value.setTargetCoord(target, simpleSetter);
    }

    @Override
    public void setCurrentOrderAccordingToType() {
        super.setCurrentOrderAccordingToType();
        adoptOrdersFrom(this);
    }

    @Override
    public void setCombatTacticsAccordingToType() {
        super.setCombatTacticsAccordingToType();
        for (Entry<Integer, Ships> s : fleet.getShipMap())
            s.value.setCombatTacticsAccordingToType();
    }

    public ShipRange getRange(boolean considerFleet) {
        int index = Math.min(range.getRange(), ShipRange.LONG.getRange());
        ShipRange range = ShipRange.fromInt(index);
        if (!considerFleet)
            return range;
        for (Entry<Integer, Ships> s : fleet.getShipMap())
            index = Math.min(s.value.getRange(true).getRange(), index);
        range = ShipRange.fromInt(index);
        return range;
    }

    @Override
    public void unsetCurrentOrder() {
        super.unsetCurrentOrder(true);
        for (Entry<Integer, Ships> s : fleet.getShipMap())
            s.value.unsetCurrentOrder(true);
    }

    @Override
    public void unsetCurrentOrder(boolean updateHistory) {
        super.unsetCurrentOrder(updateHistory);
        for (Entry<Integer, Ships> s : fleet.getShipMap())
            s.value.unsetCurrentOrder(updateHistory);
    }

    /**
     * Function calculates the speed of the fleet
     * @param considerFleet
     * @return
     */
    public int getSpeed(boolean considerFleet) {
        int sp = super.getSpeed();
        if (!considerFleet)
            return sp;
        for (Entry<Integer, Ships> s : fleet.getShipMap())
            sp = Math.min(s.value.getSpeed(), sp);
        return sp;
    }

    @Override
    public int getCompleteOffensivePower() {
        return getCompleteOffensivePower(true, true, false);
    }

    @Override
    public int getCompleteOffensivePower(boolean beams, boolean torpedoes) {
        return getCompleteOffensivePower(beams, torpedoes, false);
    }

    public int getCompleteOffensivePower(boolean beams, boolean torpedoes, boolean includeFleet) {
        int power = super.getCompleteOffensivePower(beams, torpedoes);
        if (includeFleet)
            for (Entry<Integer, Ships> s : fleet.getShipMap())
                power += s.value.getCompleteOffensivePower(beams, torpedoes, false);
        return power;
    }

    @Override
    public int getCompleteDefensivePower() {
        return getCompleteDefensivePower(true, true, false);
    }

    @Override
    public int getCompleteDefensivePower(boolean useShields, boolean useHull) {
        return getCompleteDefensivePower(useShields, useHull, false);
    }

    public int getCompleteDefensivePower(boolean useShields, boolean useHull, boolean includeFleet) {
        int power = super.getCompleteDefensivePower(useShields, useHull);
        if (includeFleet)
            for (Entry<Integer, Ships> s : fleet.getShipMap())
                power += s.value.getCompleteOffensivePower(useShields, useHull, false);
        return power;
    }

    @Override
    public ShipRange getRange() {
        ShipRange rng = ShipRange.fromInt(Math.min(super.getRange().getRange(), ShipRange.LONG.getRange()));
        for (Entry<Integer, Ships> s : fleet.getShipMap())
            rng = ShipRange.fromInt(Math.min(s.value.getRange().getRange(), rng.getRange()));
        return rng;
    }

    public RetreatMode calcReatreatMode() {
        //check the leader's order; if it retreats, try a complete retreat, if it stays, try a complete stay
        //otherwise the fleet is disassembled into its single ships
        RetreatMode result = combatTactics == CombatTactics.CT_RETREAT ? RetreatMode.RETREAT_MODE_COMPLETE
                : RetreatMode.RETREAT_MODE_STAY;
        for (Entry<Integer, Ships> s : fleet.getShipMap()) {
            CombatTactics tactics = s.value.getCombatTactics();
            if (result == RetreatMode.RETREAT_MODE_COMPLETE) {
                if (tactics != CombatTactics.CT_RETREAT)
                    return RetreatMode.RETREAT_MODE_SPLITTED;
            } else { // result == RETREAT_MODE_STAY
                if (tactics == CombatTactics.CT_RETREAT)
                    return RetreatMode.RETREAT_MODE_SPLITTED;
            }
        }
        return result;
    }

    /**
     * Function calculates the ship type of the fleet. If only one type of ship is in the fleet, then it returns that one's type. If there are different
     * types of ships, then it returns -1.
     * @return
     */
    public int getFleetShipType() {
        for (Entry<Integer, Ships> s : fleet.getShipMap()) {
            if (s.value.getShipType() != shipType)
                return -1;
        }
        return shipType.getType();
    }

    /*
     * calculates the minimum stealthpower of the fleet
     */
    @Override
    public int getStealthPower() {
        int sp = super.getStealthPower();
        for (Entry<Integer, Ships> s : fleet.getShipMap()) {
            if (sp == 0)
                return 0;
            sp = Math.min(sp, s.value.getStealthPower());
        }
        return sp;
    }

    /*
     * Are the hull of this ship and all the hulls of the ships in its fleet at their maximums ?
     * @return false if yes
     */
    @Override
    public boolean needsRepair() {
        for (Entry<Integer, Ships> s : fleet.getShipMap())
            if (s.value.needsRepair())
                return true;
        return super.needsRepair();
    }

    @Override
    public boolean canHaveOrder(ShipOrder order, boolean requireNew) {
        return canHaveOrder(order, requireNew, true);
    }

    public boolean canHaveOrder(ShipOrder order, boolean requireNew, boolean requireAllCan) {
        if (hasFleet()) {
            if (order == ShipOrder.ASSIGN_FLAGSHIP)
                return false;
            if (requireAllCan) {
                for (Entry<Integer, Ships> s : fleet.getShipMap())
                    if (!s.value.canHaveOrder(order, requireNew))
                        return false;
            } else {
                for (Entry<Integer, Ships> s : fleet.getShipMap())
                    if (s.value.canHaveOrder(order, requireNew, false))
                        return true;
            }
        }
        return super.canHaveOrder(order, requireNew);
    }

    public boolean hasFleet() {
        return !fleet.empty();
    }

    public boolean fleetHasTroops() {
        for (Entry<Integer, Ships> s : fleet.getShipMap())
            if (s.value.fleetHasTroops())
                return true;
        return super.hasTroops();
    }

    public boolean hasVeteran() {
        for (Entry<Integer, Ships> s : fleet.getShipMap())
            if (s.value.hasVeteran())
                return true;
        return super.isVeteran();
    }

    @Override
    public boolean hasTarget() {
        // targets should always be the same among the leader and fleet of Ships
        return super.hasTarget();
    }

    public boolean canCloak(boolean considerFleet) {
        if (considerFleet)
            for (Entry<Integer, Ships> s : fleet.getShipMap())
                if (!s.value.canCloak(considerFleet))
                    return false;
        return super.canCloak();
    }

    public void traditionalRepair(boolean atShipPort, boolean fasterShieldRecharge) {
        super.repair(atShipPort, fasterShieldRecharge);
        for (Entry<Integer, Ships> s : fleet.getShipMap())
            s.value.traditionalRepair(atShipPort, fasterShieldRecharge);
    }

    public void repairCommand(boolean atShipPort, boolean fasterShieldRecharge, ShipMap ships) {
        if (!atShipPort) {
            unsetCurrentOrder();
            return;
        }
        for (int i = 0; i < fleet.getSize(); i++) {
            Ships s = fleet.getAt(i);
            s.repairCommand(atShipPort, fasterShieldRecharge, ships);
            if (!s.needsRepair()) {
                ships.add(s);
                removeShipFromFleet(i--);
            }
        }
        super.repair(atShipPort, fasterShieldRecharge);
        if (!super.needsRepair()) {
            super.unsetCurrentOrder();
            if (hasFleet())
                ships.add(giveFleetToFleetsFirstShip());
        }
    }

    public void extractDeuterium(ShipMap ships) {
        for (int i = 0; i < fleet.getSize(); i++) {
            Ships s = fleet.getAt(i);
            s.extractDeuterium(ships);
            if (!s.canExtractDeuterium()) {
                ships.add(s);
                s.unsetCurrentOrder();
                removeShipFromFleet(i--);
            }
        }
        super.extractDeuterium();
        if (!super.canExtractDeuterium()) {
            super.unsetCurrentOrder();
            if (hasFleet())
                ships.add(giveFleetToFleetsFirstShip());
        }
    }

    public void retreatFleet(IntPoint retreatSector) {
        retreatFleet(retreatSector, null);
    }

    public void retreatFleet(IntPoint retreatSector, CombatTactics newCombatTactics) {
        StarSystem system = resourceManager.getUniverseMap().getStarSystemAt(getCoordinates());
        system.eraseShip(this);
        for (Entry<Integer, Ships> s : fleet.getShipMap()) {
            Ship ship = s.value;
            ship.retreatShip(retreatSector, newCombatTactics);
        }
        system = resourceManager.getUniverseMap().getStarSystemAt(getCoordinates());
        system.addShip(this);
    }

    public void retreat(IntPoint retreatSector) {
        this.retreat(retreatSector, null);
    }

    public void retreat(IntPoint retreatSector, CombatTactics newCombatTactics) {
        StarSystem system = resourceManager.getUniverseMap().getStarSystemAt(getCoordinates());
        system.eraseShip(this);
        super.retreatShip(retreatSector, newCombatTactics);
        system = resourceManager.getUniverseMap().getStarSystemAt(getCoordinates());
        system.addShip(this);
    }

    public void calcEffects(Sector sector, Race race, boolean deactivatedShipScanner, boolean betterScanner) {
        super.calcEffectsForSingleShip(sector, race, deactivatedShipScanner, betterScanner, false);
        // if the ship has a fleet, consider the ships in the fleet also
        for (Entry<Integer, Ships> s : fleet.getShipMap()) {
            Ship leader = s.value;
            leader.calcEffectsForSingleShip(sector, race, deactivatedShipScanner, betterScanner, true);
        }
    }

    public StationWorkResult buildStation(ShipOrder order, Sector sector, Major major, int id) {
        StationWorkResult result = new StationWorkResult();
        // if the ship leads a fleet, first check the fleet, and if needed remove the ship from the fleet
        for (int i = 0; i < fleet.getSize(); i++) {
            Ships ship = fleet.getAt(i);
            if (ship.buildStation(order, sector, major, id).finished) {
                result.finished = true;
                removeShipFromFleet(i--);
                unsetCurrentOrder();
                return result;
            }
        }
        if (super.buildStationbyShip(order, sector, major, id)) {
            result.finished = true;
            result.removeLeader = true;
            unsetCurrentOrder(false);
        }
        return result;
    }

    public Ships giveFleetToFleetsFirstShip() {
        if (hasFleet()) {
            Ships newFleetShip = fleet.getAt(0);
            for (int i = 1; i < fleet.getSize(); i++) {
                newFleetShip.addShipToFleet(fleet.getAt(i));
            }
            reset();
            return newFleetShip;
        }
        return null;
    }

    @Override
    public void scrap(Major major, StarSystem system, boolean disassembly) {
        for (Entry<Integer, Ships> s : fleet.getShipMap())
            s.value.scrap(major, system, disassembly);
        super.scrap(major, system, disassembly);
    }

    @Override
    public void setResourceManager(ResourceManager res) {
        super.setResourceManager(res);
        fleet.setResourceManager(resourceManager);
    }

    public Table getShieldRegion(int width, int height) {
        int shieldPercent = -1;
        int div = 20;
        if (getShield().getMaxShield() > 0)
            shieldPercent = getShield().getCurrentShield() * div / getShield().getMaxShield();
        Color color;
        if (shieldPercent == -1)
            color = new Color(0, 0, 0, 0);
        else
            color = new Color((240 - shieldPercent * 12) / 255.0f, 80 / 255.0f, (shieldPercent * 12) / 255.0f, 1.0f);
        SpriteDrawable drawable = ((ScreenManager) resourceManager).getScreen().getTintedDrawable(GameConstants.UI_BG_SIMPLE, color);
        Table table = new Table();
        table.align(Align.bottom);
        float cHeight = height/(div+10);
        cHeight = Math.max(cHeight, 1);
        float cWidth = width - 2 * width/10;
        cWidth = Math.max(cWidth, 1);
        float pHeight = height/div - height/(div+10);
        for (int ii = 0; ii < shieldPercent; ii++) {
            if( (pHeight + cHeight) * ii > height)
                break;
            table.add(new Image(drawable)).width(cWidth).height(cHeight).spaceLeft(width/10).spaceRight(width/10);
            table.row();
            table.add().height(pHeight);
            table.row();
        }
        return table;
    }

    public Table getHullRegion(int width, int height) {
        int div = 20;
        int hullPercent = -1;
        Color color;
        if (getHull().getMaxHull() > 0)
            hullPercent = getHull().getCurrentHull() * div / getHull().getMaxHull();
        if (hullPercent == -1)
            color = new Color(0, 0, 0, 0);
        else
            color = new Color((240 - hullPercent * 12) / 255.0f, (hullPercent * 12) / 255.0f, 0, 1.0f);
        SpriteDrawable drawable = ((ScreenManager) resourceManager).getScreen().getTintedDrawable(GameConstants.UI_BG_SIMPLE, color);
        Table table = new Table();
        table.align(Align.bottom);
        float cHeight = height/(div+10);
        cHeight = Math.max(cHeight, 1);
        float cWidth = width - 2 * width/10;
        cWidth = Math.max(cWidth, 1);
        float pHeight = height/div - height/(div+10);
        for (int ii = 0; ii < hullPercent; ii++) {
            if( (pHeight + cHeight) * ii > height)
                break;
            table.add(new Image(drawable)).width(cWidth).height(cHeight).spaceLeft(width/10).spaceRight(width/10);
            table.row();
            table.add().height(pHeight);
            table.row();
        }
        return table;
    }

    public void getTooltip(Table table, Skin skin, String headerFont, Color headerColor, String textFont, Color textColor) {
        getTooltip(true, table, skin, headerFont, headerColor, textFont, textColor);
    }

    public void getTooltip(boolean showFleet, Table table, Skin skin, String headerFont, Color headerColor, String textFont, Color textColor) {
        if(showFleet && hasFleet()) {
            FleetInfoForGetTooltip info = new FleetInfoForGetTooltip(getFleetShipType(), getRange(true), getSpeed(true));
            super.getTooltip(info, table, skin, headerFont, headerColor, textFont, textColor);
        } else
            super.getTooltip(null, table, skin, headerFont, headerColor, textFont, textColor);
    }

    @Override
    public void write(Kryo kryo, Output output) {
        super.write(kryo, output);
        fleet.writeShipMap(kryo, output);
    }

    @Override
    public void read(Kryo kryo, Input input) {
        super.read(kryo, input);
        fleet.readShipMap(kryo, input);
    }

    @Override
    public void dispose() {
        if (pathImgs != null) {
            for (Image img : pathImgs)
                img.remove();
            pathImgs.clear();
        }
        fleet.dispose();
        super.dispose();
    }

    @Override
    public int hashCode() {
        return name.hashCode() + ownerID.hashCode() + shipClass.hashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof Ships)
            if (((Ships)other).name.equals(name) && ((Ships)other).ID == ID
                    && ((Ships)other).ownerID.equals(ownerID))
                return true;
        return false;
    }

    public Array<Image> getPathImgs() {
        if (pathImgs == null)
            pathImgs = new Array<Image>();
        return pathImgs;
    }
}
