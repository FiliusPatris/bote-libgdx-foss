/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ships;

import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.ObjectMap.Entry;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.constants.ShipType;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.utils.IntPoint;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

public class ShipMap implements Disposable {
    private ArrayMap<Integer, Ships> ships;// internal container with the Ships
    private Integer nextKey;// used to give newly added Ships their keys
    private int currentShip; // helper variable
    private int fleetShip;	// helper variable
    private boolean mapTile;
    private Array<StarSystem> systems;
    private transient ResourceManager manager;

    public ShipMap() {
        this(null, false, null);
    }

    //This counter is incremented each time a ships is added, and only resetted when the shipmap is completely
    //empty, when loading a savegame and at turn change.
    public ShipMap(Array<StarSystem> systems, boolean mapTile, ResourceManager manager) {
        nextKey = 0;
        this.mapTile = mapTile;
        this.systems = systems;
        this.manager = manager;
        ships = new ArrayMap<Integer, Ships>(false, 16);
        currentShip = -1;
        fleetShip = -1;
    }

    private Integer nextKey() {
        return nextKey++;
    }

    /**
     * returns the the first shis found of a given type
     * @param type
     * @return null if non found
     */
    public Ships findShipOfType(ShipType type) {
        for (Iterator<Entry<Integer, Ships>> iter = ships.entries().iterator(); iter.hasNext();) {
            Entry<Integer, Ships> e = iter.next();
            if (e.value.getShipType() == type)
                return e.value;
        }
        return null;
    }

    public int add(Ships ship) {
        int key = nextKey();
        if (systems != null) {
            IntPoint coord = ship.getCoordinates();
            systems.get(manager.coordsToIndex(coord)).addShip(ship);
        }
        if (getSize() == 1) {
            fleetShip = 0;
            currentShip = 0;
        }
        if (mapTile)
            ship.setMapTileKey(key);
        else
            ship.setKey(key);
        ships.put(key, ship);
        return key;
    }

    public void append(ShipMap other) {
        for (Entry<Integer, Ships> s : other.ships)
            add(s.value);
    }

    public void reset() {
        ships.clear();
        currentShip = -1;
        fleetShip = -1;
        nextKey = 0;
        if (systems != null) {
            for (StarSystem s : systems)
                s.clearShips();
        }
    }

    public boolean empty() {
        return ships.size == 0;
    }

    private int updateSpecialShip(int ship, int toErase) {
        if (ship != toErase)
            return ship;
        if (ship != 0)
            return --ship;
        else if (ship != ships.size - 1)
            return ++ship;
        return -1;
    }

    public void eraseAt(int index) {
        currentShip = updateSpecialShip(currentShip, index);
        fleetShip = updateSpecialShip(fleetShip, index);
        if (systems != null) {
            IntPoint coord = ships.getValueAt(index).getCoordinates();
            systems.get(manager.coordsToIndex(coord)).eraseShip(ships.getValueAt(index));
        }
        final Ships ship = ships.getValueAt(index);
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                ship.dispose();
            }
        });
        ships.removeIndex(index);
        if (empty())
            reset();
    }

    public Ships getAt(int index) {
        return ships.getValueAt(index);
    }

    public ArrayMap<Integer, Ships> getShipMap() {
        return ships;
    }

    public int getSize() {
        return ships.size;
    }

    public void setCurrentShip(int index) {
        if (index < ships.size)
            currentShip = index;
        else
            currentShip = ships.size - 1;
    }

    public int currentShip() {
        return currentShip;
    }

    public void setFleetShip(int index) {
        if (index < ships.size)
            fleetShip = index;
        else
            fleetShip = ships.size - 1;
    }

    public int fleetShip() {
        return fleetShip;
    }

    public Ships getLeader(Ships ship) {
        for (Entry<Integer, Ships> e : ships) {
            if (e.value == ship)
                return e.value;

            if (!e.value.getOwnerId().equals(ship.getOwnerId()))
                continue;
            for (int i = 0; i < e.value.getFleet().getSize(); i++) {
                if (e.value.getFleet().ships.getValueAt(i) == ship)
                    return e.value;
            }
        }
        return null;
    }

    public void setStarSystems(Array<StarSystem> systems) {
        this.systems = systems;
    }

    public void setResourceManager(ResourceManager res) {
        this.manager = res;
        for (Iterator<Entry<Integer, Ships>> iter = ships.entries().iterator(); iter.hasNext();) {
            Entry<Integer, Ships> e = iter.next();
            e.value.setResourceManager(res);
        }
    }

    public void writeShipMap(Kryo kryo, Output output) {
        output.writeInt(ships.size);
        for (int i = 0; i < ships.size; i++)
            kryo.writeObject(output, ships.getValueAt(i));
    }

    public void readShipMap(Kryo kryo, Input input) {
        reset();
        int size = input.readInt();
        for (int i = 0; i < size; i++) {
            Ships s = kryo.readObject(input, Ships.class);
            add(s);
        }
    }

    @Override
    public void dispose() {
        for (int i = 0; i < ships.size; i++)
            ships.getValueAt(i).dispose();
    }

    public void writeEndofRoundData(Kryo kryo, Output output, String majorID) {
        Array<Ships> sl = new Array<Ships>();
        for (int i = 0; i < ships.size; i++)
            if (ships.getValueAt(i).getOwnerId().equals(majorID))
                sl.add(ships.getValueAt(i));
        kryo.writeObject(output, sl);
    }

    @SuppressWarnings("unchecked")
    public void readEndofRoundData(Kryo kryo, Input input, String majorID) {
        for (int i = 0; i < ships.size; i++) {
            if (ships.getValueAt(i).getOwnerId().equals(majorID)) {
                eraseAt(i);
                i--;
            }
        }
        Array<Ships> sl = kryo.readObject(input, Array.class);
        for (int i = 0; i < sl.size; i++) {
            Ships ship = sl.get(i);
            ship.setResourceManager(manager);
            add(ship);
        }
    }

    public void writeNextRoundData(Kryo kryo, Output output, IntPoint currentCombatSector) {
        int count = 0;
        for (int i = 0; i < ships.size; i++)
            if (ships.getValueAt(i).getCoordinates().equals(currentCombatSector))
                count++;
        output.writeInt(count);
        // send ships only from this sector
        for (int i = 0; i < ships.size; i++) {
            Ships ship = ships.get(i);
            if (ship.getCoordinates().equals(currentCombatSector))
                ship.write(kryo, output);
        }
    }

    public void readNextRoundData(Kryo kryo, Input input, IntPoint currentCombatSector) {
        for (int i = 0; i < ships.size; i++) {
            Ships ship = ships.get(i);
            if (ship.getCoordinates().equals(currentCombatSector)) {
                ships.removeIndex(i);
                i--;
            }
        }
        int count = input.readInt();
        for (int i = 0; i < count; i++) {
            Ships ship = kryo.readObject(input, Ships.class);
            ship.setResourceManager(manager);
            add(ship);
        }
    }
}
