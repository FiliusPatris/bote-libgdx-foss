/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ships;

import com.blotunga.bote.constants.GameConstants;

public class ShipHistoryStruct implements Comparable<ShipHistoryStruct> {
    public enum ShipSortType {
        BY_NAME,
        BY_TYPE,
        BY_CLASS,
        BY_SECTORNAME,
        BY_CURRENTSECTOR,
        BY_TASK,
        BY_DESTROYTYPE,
        BY_ROUNDBUILD,
        BY_ROUNDDESTROY,
        BY_EXP,
        BY_TARGET
    }

    private static ShipSortType sortType = ShipSortType.BY_NAME;
    public String shipName;
    public String shipType;
    public String shipClass;
    public String sectorName;
    public String currentSector;
    public String currentTask;
    public String kindOfDestroy;
    public String target;
    public int buildRound;
    public int destroyRound;
    public int experience;

    public ShipHistoryStruct() {
        shipName = "";
        shipType = "";
        shipClass = "";
        sectorName = "";
        currentSector = "";
        currentTask = "";
        kindOfDestroy = "";
        target = "";
        buildRound = 0;
        destroyRound = 0;
        experience = 0;
    }

    public ShipHistoryStruct(String shipName, String shipType, String shipClass, String sectorName,
            String currentSector, String currentTask, String kindOfDestroy, String target, int buildRound,
            int destroyRound, int experience) {
        this.shipName = shipName;
        this.shipType = shipType;
        this.shipClass = shipClass;
        this.sectorName = sectorName;
        this.currentSector = currentSector;
        this.currentTask = currentTask;
        this.kindOfDestroy = kindOfDestroy;
        this.target = target;
        this.buildRound = buildRound;
        this.destroyRound = destroyRound;
        this.experience = experience;
    }

    public ShipHistoryStruct(ShipHistoryStruct ship) {
        this.shipName = ship.shipName;
        this.shipType = ship.shipType;
        this.shipClass = ship.shipClass;
        this.sectorName = ship.sectorName;
        this.currentSector = ship.currentSector;
        this.currentTask = ship.currentTask;
        this.kindOfDestroy = ship.kindOfDestroy;
        this.target = ship.target;
        this.buildRound = ship.buildRound;
        this.destroyRound = ship.destroyRound;
        this.experience = ship.experience;
    }

    public static void setSortType(ShipSortType st) {
        sortType = st;
    }

    public static ShipSortType getSortType() {
        return sortType;
    }

    @Override
    public int compareTo(ShipHistoryStruct other) {
        switch (sortType) {
            case BY_NAME:
                return shipName.compareTo(other.shipName);
            case BY_TYPE:
                return shipType.compareTo(other.shipType);
            case BY_CLASS:
                return shipClass.compareTo(other.shipClass);
            case BY_SECTORNAME:
                return sectorName.compareTo(other.sectorName);
            case BY_CURRENTSECTOR:
                return currentSector.compareTo(other.currentSector);
            case BY_TASK:
                return currentTask.compareTo(other.currentTask);
            case BY_DESTROYTYPE:
                return kindOfDestroy.compareTo(other.kindOfDestroy);
            case BY_ROUNDBUILD:
                return GameConstants.compare(buildRound, other.buildRound);
            case BY_ROUNDDESTROY:
                return GameConstants.compare(destroyRound, other.destroyRound);
            case BY_EXP:
                return GameConstants.compare(experience, other.experience);
            case BY_TARGET:
                return target.compareTo(other.target);
        }
        return 0;
    }
}
