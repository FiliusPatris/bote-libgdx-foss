/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ships;

import com.badlogic.gdx.utils.Array;

public class ShipHistory {
    private Array<ShipHistoryStruct> shipHistory;

    public ShipHistory() {
        shipHistory = new Array<ShipHistoryStruct>(true, 16, ShipHistoryStruct.class);
    }

    /**
     * The ship is added to the array
     * @param ship
     * @param buildsector - where the ship was built
     * @param round - current round
     */
    public void addShip(ShipHistoryStruct ship, String buildsector, int round) {
        for (int i = 0; i < shipHistory.size; i++)
            if (ship.shipName.equals(shipHistory.get(i).shipName)) {
                return;
            }
        ShipHistoryStruct temp = new ShipHistoryStruct(ship);
        temp.sectorName = buildsector;
        temp.currentSector = buildsector;
        temp.buildRound = round;
        boolean reversed = false;
        if (shipHistory.size != 0 && shipHistory.get(0).compareTo(shipHistory.get(shipHistory.size - 1)) > 0)
            reversed = true;
        shipHistory.add(temp);
        shipHistory.sort();
        if (reversed)
            shipHistory.reverse();
    }

    /**
     * @param ship
     * @param sector
     * @param destroyRound
     * @param destroyType
     * @param status
     * @return true if the shiphistory could be updated, false otherwise
     */
    public boolean modifyShip(ShipHistoryStruct ship, String sector, int destroyRound, String destroyType, String status) {
        for (int i = 0; i < shipHistory.size; i++) {
            if (shipHistory.get(i).shipName.equals(ship.shipName)) {
                ShipHistoryStruct temp = new ShipHistoryStruct(ship);
                temp.sectorName = shipHistory.get(i).sectorName;
                temp.buildRound = shipHistory.get(i).buildRound;
                temp.currentSector = sector;

                if (destroyRound != 0) {
                    temp.destroyRound = destroyRound;
                    temp.kindOfDestroy = destroyType;
                    temp.sectorName = sector;
                    temp.currentTask = status;
                }
                shipHistory.set(i, temp);
                return true;
            }
        }
        return false;
    }

    public boolean modifyShip(ShipHistoryStruct ship, String sector) {
        return modifyShip(ship, sector, 0, "", "");
    }

    public boolean modifyShip(ShipHistoryStruct ship, String sector, int destroyRound) {
        return modifyShip(ship, sector, destroyRound, "", "");
    }

    public ShipHistoryStruct getShipHistory(int i) {
        return shipHistory.get(i);
    }

    public Array<ShipHistoryStruct> getShipHistoryArray() {
        return shipHistory;
    }

    public int getSizeOfShipHistory() {
        return shipHistory.size;
    }

    public boolean isShipAlive(int i) {
        return shipHistory.get(i).destroyRound == 0;
    }

    /**
     * Function returns the number of the living ships if the parameter is true, otherwise the number of destroyed ships
     * @param shipAlive
     * @return
     */
    public int getNumberOfShips(boolean shipAlive) {
        int number = 0;
        for (int i = 0; i < getSizeOfShipHistory(); i++) {
            if (isShipAlive(i) && shipAlive)
                number++;
            else if (!isShipAlive(i) && !shipAlive)
                number++;
        }
        return number;
    }
}
