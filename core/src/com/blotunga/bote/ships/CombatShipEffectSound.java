/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ships;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.utils.Disposable;

public class CombatShipEffectSound implements Disposable {
    public enum EffectType {
        EFFECT_PHASER("laser_long"),
        EFFECT_PHASER_PULSING("laser_short"),
        EFFECT_PHOTON("photon");

        private String fileName;
        private EffectType(String fileName) {
            this.fileName = fileName;
        }

        public String getPath() {
            return fileName + ".ogg";
        }
    }

    public interface CombatShipEffectHandler {
        public long playEffect(EffectType type);
        public void stopEffect(EffectType type, long id);
    }

    private Sound snd;

    public CombatShipEffectSound(EffectType type) {
        snd = Gdx.audio.newSound(Gdx.files.internal("sounds/" + type.getPath()));
    }

    public Sound getSound() {
        return snd;
    }

    @Override
    public void dispose() {
        snd.stop();
        snd.dispose();
    }
}
