/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ships;

import com.blotunga.bote.constants.ResourceTypes;

/**
 * @author dragon
 * @version 1
 */
public class Hull {
    private boolean doubleHull;
    private boolean ablative;		///< damage which would hit the hull every time is not applied
    private boolean polarisation;	///< 10% less damage
    private int baseHull;
    private int currentHull;
    private int hullMaterial;
    private int maxHull;

    public Hull() {
        doubleHull = false;
        ablative = false;
        polarisation = false;
        baseHull = 0;
        currentHull = 0;
        hullMaterial = 0;
        maxHull = 0;
    }

    public Hull(Hull oldHull) {
        doubleHull = oldHull.doubleHull;
        ablative = oldHull.ablative;
        polarisation = oldHull.polarisation;
        baseHull = oldHull.baseHull;
        currentHull = oldHull.currentHull;
        hullMaterial = oldHull.hullMaterial;
        maxHull = oldHull.maxHull;
    }

    public boolean isDoubleHull() {
        return doubleHull;
    }

    public boolean isAblative() {
        return ablative;
    }

    public boolean isPolarisation() {
        return polarisation;
    }

    public int getBaseHull() {
        return baseHull;
    }

    public int getCurrentHull() {
        return currentHull;
    }

    public int getHullMaterial() {
        return hullMaterial;
    }

    public int getMaxHull() {
        return maxHull;
    }

    public void setCurrentHull(int add) {
        setCurrentHull(add, false);
    }

    public void setCurrentHull(int add, boolean ignoreHullSpecials) {
        //for polarised shields deduct 10% damage; ablative deducts 25%
        if (!ignoreHullSpecials) {
            double multi = 0.0f;
            if (isPolarisation() && (add < 0))
                multi += 0.1;
            if (isAblative() && (add < 0))
                multi += 0.25;

            add -= (int) (add * multi);
        }

        //check if values are still within ranges
        if ((long) (add + currentHull) >= (long) maxHull)
            currentHull = maxHull;
        else if ((long) (add + currentHull) <= 0)
            currentHull = 0;
        else
            currentHull += add;
    }

    public void modifyHull(boolean doubleHull, int baseHull, int hullMaterial, boolean ablative, boolean polarisation) {
        this.doubleHull = doubleHull;
        this.ablative = ablative;
        this.polarisation = polarisation;
        this.baseHull = baseHull;
        this.hullMaterial = hullMaterial;
        float multi = 1.0f;

        if (hullMaterial == ResourceTypes.DURANIUM.getType()) {
            multi = 1.5f;
        } else if (hullMaterial == ResourceTypes.IRIDIUM.getType()) {
            multi = 2.0f;
        }
        maxHull = (int) (baseHull * multi);
        if (doubleHull)
            maxHull = (int) (maxHull * 1.5f);
        currentHull = maxHull;
    }

    /**
     * Function repairs hull for each call of the function by 10%
     */
    public void repairHull() {
        int maxRepair = maxHull - currentHull;
        int repair = maxHull / 10;
        if (repair < 20) // repair at least 20
            repair = 20;
        if (repair > maxRepair)
            repair = maxRepair;
        currentHull += repair;
    }
}
