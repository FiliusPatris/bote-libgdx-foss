/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote;

import com.badlogic.gdx.utils.Array;
import com.blotunga.bote.achievements.AchievementsList;
import com.blotunga.bote.ui.optionsview.SaveInfo;

public interface PlatformApiIntegration {
    public interface StorageIntegration {
        public interface StorageIntegrationCallback {
            void signalBusy(boolean busy);
        }

        public void setDirectory(String directory);
        public String getDirectory();
        public void exportSaves(Array<SaveInfo> saveInfos);
        public void importSaves();
    }

    public enum PlatformType {
        PlatformGoogle("googlesignin", true, "Sign in with Google"),
        PlatformAmazon("", false, ""),
        PlatformStandalone("", false, "");

        String image;
        boolean ninePatch;
        String loginText;
        PlatformType(String image, boolean ninePatch, String loginText) {
            this.image = image;
            this.ninePatch = ninePatch;
            this.loginText = loginText;
        }

        public String getImageName() {
            return image;
        }

        public boolean isNinePatch() {
            return ninePatch;
        }

        public String getExtension() {
            return ninePatch ? ".9.png" : ".png";
        }

        public String getLoginText() {
            return loginText;
        }
    }

    public void signIn();
    public void signOut(); //explicite is false by default
    public void signOut(boolean explicite);
    public boolean isConnected();
    public void rateGame();
    public void unlockAchievement(AchievementsList achievement);
    public void incrementAchievement(AchievementsList achievement, int value);
    public void showAchievements();
    public boolean networkAvailable();
    public PlatformType getPlatformType();
    public void setCallback(PlatformCallback callback);
    public DriveIntegration getDriveIntegration();
    public StorageIntegration getStorageIntegration();
}
