/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.intel;

import java.util.Arrays;
import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.ObjectIntMap;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.ObjectMap.Entry;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.constants.CombatTactics;
import com.blotunga.bote.constants.DiplomaticAgreement;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.RaceProperty;
import com.blotunga.bote.constants.ResearchComplexType;
import com.blotunga.bote.constants.ResearchStatus;
import com.blotunga.bote.constants.ResearchType;
import com.blotunga.bote.constants.WorkerType;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.intel.IntelObject.IntelObjectSortType;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.races.Minor;
import com.blotunga.bote.races.starmap.StarMap;
import com.blotunga.bote.ships.Ships;
import com.blotunga.bote.starsystem.BuildingInfo;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.utils.IntPoint;
import com.blotunga.bote.utils.RandUtil;

public class IntelCalc {
    private ResourceManager manager;

    private class MajorBox {
        private Major major;

        public MajorBox(Major major) {
            this.major = major;
        }

        public void set(Major major) {
            this.major = major;
        }

        public Major get() {
            return major;
        }
    }

    public IntelCalc(ResourceManager manager) {
        this.manager = manager;
    }

    /**
     * This function calculates all of the intel actions and also it makes all changes. It also sends the relevant messages to the affected empires
     * @param race
     */
    public void startCalc(Major race, ArrayMap<String, Major> majors) {
        ObjectIntMap<String> spySP = new ObjectIntMap<String>();
        ObjectIntMap<String> sabSP = new ObjectIntMap<String>();

        Intelligence intel = race.getEmpire().getIntelligence();
        int oldNrSabotageReports = 0;
        for (int j = 0; j < intel.getIntelReports().getNumberOfReports(); j++)
            if (intel.getIntelReports().getReport(j).isSabotage()
                    &&intel.getIntelReports().getReport(j).getOwner().equals(race.getRaceId()))
                oldNrSabotageReports++;

        //A possible sabotage will be executed before all actions. For this only the accumulated SP are used
        IntelObject attemptObj = intel.getIntelReports().getAttemptObject();
        if (attemptObj != null) {
            long ourSP = intel.getSPStorage(1, attemptObj.getEnemy());
            //+ add eventual bonus
            ourSP += ourSP * intel.getBonus(attemptObj.getType(), 1) / 100;
            executeAttempt(race, (int)ourSP);
        }

        //First we get the intel counterparts. If an opponent has more than 0 security points assigned they can be a target
        for (int i = 0; i < majors.size; i++) {
            String majorID = majors.getKeyAt(i);
            if (!majorID.equals(race.getRaceId())) {
                long points = (long) intel.getSecurityPoints() * intel.getAssignment().getGlobalSpyPercentage(majorID) / 100;
                spySP.put(majorID, (int) points);
                points = (long) intel.getSecurityPoints() * intel.getAssignment().getGlobalSabotagePercentage(majorID) / 100;
                sabSP.put(majorID, (int) points);
            }
        }

        //now we can start intel actions
        for (int i = 0; i < majors.size; i++) {
            String majorID = majors.getKeyAt(i);
            Major major = majors.getValueAt(i);
            if (!majorID.equals(race.getRaceId())) {
                //are there points against a race or points in storage?
                if (spySP.get(majorID, 0) > 0 || intel.getSPStorage(0, majorID) > 0) {
                    //go trough the special areas
                    //economy == 0, science == 1, military == 2, diplomacy ==3
                    int b = (int) (RandUtil.random() * 4);
                    int count = 0;
                    while (count < 4) {
                        //SP * assignment / 100 + storage * assigntment / 100
                        long effectiveSP = (long) spySP.get(majorID, 0) * intel.getAssignment().getSpyPercentages(majorID, b)
                                / 100 + intel.getSPStorage(0, majorID)
                                * intel.getAssignment().getSpyPercentages(majorID, b) / 100;
                        effectiveSP += effectiveSP * intel.getBonus(b, 0) / 100;
                        if (effectiveSP > 0) {
                            Major responsibleRace = race;
                            MajorBox majorBox = new MajorBox(responsibleRace);
                            int actions = isSuccess(major, (int) effectiveSP, true, majorBox, b);
                            responsibleRace = majorBox.get();
                            deleteConsumedPoints(race, major, true, b, false);
                            //now start action
                            int stop = 0; // stop after a while if for some reason they can't be executed
                            while (actions > 0 && stop < 100) {
                                if (executeAction(race, major, responsibleRace, b, true)) {
                                    actions--;
                                    //the relation of the affected empire will diminish against the responsible race
                                    if (responsibleRace != null)
                                        major.setRelation(responsibleRace.getRaceId(), -(int) (RandUtil.random() * 5));
                                }
                                stop++;
                            }
                        }
                        if (b == 3)
                            b = 0;
                        else
                            b++;
                        count++;
                    }
                }

                if (sabSP.get(majorID, 0) > 0 || intel.getSPStorage(1, majorID) > 0) {
                    //go trough the special areas
                    //economy == 0, science == 1, military == 2, diplomacy ==3
                    int b = (int) (RandUtil.random() * 4);
                    int count = 0;
                    while (count < 4) {
                        //SP * assignment / 100 + storage * assigntment / 100
                        long effectiveSP = (long) sabSP.get(majorID, 0)
                                * intel.getAssignment().getSabotagePercentages(majorID, b) / 100
                                + intel.getSPStorage(1, majorID)
                                * intel.getAssignment().getSabotagePercentages(majorID, b) / 100;
                        effectiveSP += effectiveSP * intel.getBonus(b, 1) / 100;
                        if (effectiveSP > 0) {
                            Major responsibleRace = race;
                            MajorBox majorBox = new MajorBox(responsibleRace);
                            int actions = isSuccess(major, (int) effectiveSP, false, majorBox, b);
                            responsibleRace = majorBox.get();
                            deleteConsumedPoints(race, major, false, b, false);
                            //now start action
                            int stop = 0; // stop after a while if for some reason they can't be executed
                            while (actions > 0 && stop < 100) {
                                if (executeAction(race, major, responsibleRace, b, false)) {
                                    actions--;
                                    //the relation of the affected empire will diminish against the responsible race
                                    if (responsibleRace != null)
                                        major.setRelation(responsibleRace.getRaceId(), -(int) (RandUtil.random() * 10));
                                }
                                stop++;
                            }
                        }
                        if (b == 3)
                            b = 0;
                        else
                            b++;
                        count++;
                    }
                }
            }
        }
        IntelObject.setSortType(IntelObjectSortType.BY_ROUND);
        intel.getIntelReports().sortAllReports(true);

        int newNrSabotageReports = 0;
        for (int j = 0; j < intel.getIntelReports().getNumberOfReports(); j++)
            if (intel.getIntelReports().getReport(j).isSabotage()
                    &&intel.getIntelReports().getReport(j).getOwner().equals(race.getRaceId()))
                newNrSabotageReports++;
        intel.getIntelReports().deleteOlder(manager.getCurrentRound() - GameConstants.TURNS_TO_KEEP_INTEL_MSG);
        manager.getUniverseMap().getVictoryObserver().addSabotageAction(race.getRaceId(), newNrSabotageReports - oldNrSabotageReports);
    }

    /**
     * The function adds inner security points and intel storage points of a race to the existing ones
     * @param race
     */
    public void addPoints(Major race) {
        Intelligence intel = race.getEmpire().getIntelligence();

        //add points to the inner security
        long add = (long) intel.getSecurityPoints() * intel.getAssignment().getInnerSecurityPercentage() / 100;
        add += add * intel.getInnerSecurityBonus() / 100;
        intel.addInnerSecurityPoints((int) add);

        //add points to the different storages - bonuses are not added here
        ArrayMap<String, Major> majors = manager.getRaceController().getMajors();
        for (int i = 0; i < majors.size; i++) {
            String majorID = majors.getKeyAt(i);
            if (!majorID.equals(race.getRaceId())) {
                //espionage
                long depot = (long) intel.getSecurityPoints() * intel.getAssignment().getGlobalSpyPercentage(majorID)
                        * intel.getAssignment().getSpyPercentages(majorID, 4) / 10000;
                intel.addSPStoragePoints(0, majorID, (int)depot);
                //sabotage
                depot = (long) intel.getSecurityPoints() * intel.getAssignment().getGlobalSabotagePercentage(majorID)
                        * intel.getAssignment().getSabotagePercentages(majorID, 4) / 10000;
                intel.addSPStoragePoints(1, majorID, (int)depot);
            }
        }
    }

    /**
     * The function substracts a race specific percentage from the different depots. It should be called after executing all other functions
     * @param race
     * @param perc
     */
    public void reduceDepotPoints(Major race, int perc) {
        Intelligence intel = race.getEmpire().getIntelligence();

        //if perc == -1 the race specific percentage is used
        if (perc == -1) {
            int count = 0;
            perc = 0;
            if (race.isRaceProperty(RaceProperty.FINANCIAL)) {
                perc += 15;
                count++;
            }
            if (race.isRaceProperty(RaceProperty.WARLIKE)) {
                perc += 5;
                count++;
            }
            if (race.isRaceProperty(RaceProperty.AGRARIAN)) {
                perc += 10;
                count++;
            }
            if (race.isRaceProperty(RaceProperty.INDUSTRIAL)) {
                perc += 5;
                count++;
            }
            if (race.isRaceProperty(RaceProperty.SECRET)) {
                perc -= 10;
                count++;
            }
            if (race.isRaceProperty(RaceProperty.SCIENTIFIC)) {
                perc += 0;
                count++;
            }
            if (race.isRaceProperty(RaceProperty.PRODUCER)) {
                perc += 0;
                count++;
            }
            if (race.isRaceProperty(RaceProperty.PACIFIST)) {
                perc += 35;
                count++;
            }
            if (race.isRaceProperty(RaceProperty.SNEAKY)) {
                perc -= 5;
                count++;
            }
            if (race.isRaceProperty(RaceProperty.SOLOING)) {
                perc -= 20;
                count++;
            }
            if (race.isRaceProperty(RaceProperty.HOSTILE)) {
                perc += 10;
                count++;
            }

            if (count == 0)
                perc = 20;
            else {
                perc /= count;
                perc += 20;
            }

            if (perc > 100)
                perc = 100;
            else if (perc < 0)
                perc = 0;
        }
        //get percentage from the different depots
        //first from the inner security
        long diff = intel.getInnerSecurityStorage() * perc / 100;
        intel.addInnerSecurityPoints((int)-diff);

        ArrayMap<String, Major> majors = manager.getRaceController().getMajors();
        for (int i = 0; i < majors.size; i++) {
            String majorID = majors.getKeyAt(i);
            if (!majorID.equals(race.getRaceId())) {
                //if a race has no more systems - was eliminated then all points disappear
                int oldperc = perc;
                if (majors.getValueAt(i).getEmpire().countSystems() == 0)
                    perc = 100;
                //storage of espionage
                diff = intel.getSPStorage(0, majorID) * perc / 100;
                intel.addSPStoragePoints(0, majorID, (int)-diff);
                //sabotage depot
                diff = intel.getSPStorage(1, majorID) * perc / 100;
                intel.addSPStoragePoints(1, majorID, (int)-diff);
                perc = oldperc;
            }
        }
    }

    /**
     * Function calculates whether an intel action of a race is succesful
     * @param enemyRace
     * @param ourSP
     * @param isSpy
     * @param responsibleBox - Box class that holds the responsible major, thus we can change it's value
     * @param type
     * @return
     */
    private int isSuccess(Major enemyRace, int ourSP, boolean isSpy, MajorBox responsibleBox, int type) {
        Gdx.app.debug("IntelCalc", "IsSuccess() begin...");
        int actions = 0;

        int enemyInnerSec = getCompleteInnerSecPoints(enemyRace);
        Gdx.app.debug("IntelCalc", String.format("inner security of %s is %d", enemyRace.getRaceId(), enemyInnerSec));

        int agg = responsibleBox.get().getEmpire().getIntelligence()
                .getAggressiveness(isSpy == false ? 1 : 0, enemyRace.getRaceId());
        Gdx.app.debug("IntelCalc", String.format("intel aggressiveness is %d", agg));
        agg = 3 - agg; // -> we rotate the values

        //espionage has higher chances than sabotage
        int minDiff = 0;
        if (isSpy) {
            for (int i = 0; i < 3; i++)
                minDiff += (int) (RandUtil.random() * 500 * agg) + 1;
            minDiff /= 3;
        } else {
            for (int i = 0; i < 3; i++)
                minDiff += (int) (RandUtil.random() * 1000 * agg) + 1;
            minDiff /= 3;
        }

        Gdx.app.debug("IntelCalc", String.format("SP of race %s %d > enemies inner security %d + random %d",
                responsibleBox.get().getRaceId(), ourSP, enemyInnerSec, minDiff));
        if (ourSP > (enemyInnerSec + minDiff)) {
            // if we have much more points than the enemy, then more actions can be started
            actions = ourSP / (enemyInnerSec + minDiff);
            //if our intel points are twice their inner security points, then we can hide our identity
            if (!isSpy && ourSP > (2 * enemyInnerSec)) {
                //if our points are thrice as high hten we can blame others
                if (ourSP > (3 * enemyInnerSec)) {
                    String responsibleRaceID = responsibleBox.get().getEmpire().getIntelligence().getResponsibleRace();
                    Major responsibleRace = Major.toMajor(manager.getRaceController().getRace(responsibleRaceID));
                    responsibleBox.set(responsibleRace);
                    if (responsibleRace != null) {
                        if (responsibleRaceID.equals(enemyRace.getRaceId()))
                            responsibleBox.set(null);
                        else if (!enemyRace.isRaceContacted(responsibleRaceID))
                            responsibleBox.set(null);
                    }
                } else
                    responsibleBox.set(null);
            }
        } else if ((ourSP * agg) < (enemyInnerSec + minDiff) && (int) (RandUtil.random() * 2) == 0)
            createMsg(responsibleBox.get(), enemyRace, type);
        Gdx.app.debug("IntelCalc", String.format("number of starting intel actions: %d", actions));
        return actions;
    }

    /**
     * The function removes the points consumed by an intel action on the side of the victim and the agressor
     * @param ourRace
     * @param enemyRace
     * @param isSpy
     * @param type
     * @param isAttempt
     */
    private void deleteConsumedPoints(Major ourRace, Major enemyRace, boolean isSpy, int type, boolean isAttempt) {
        Gdx.app.debug("IntelCalc", "DeleteConsumedPoints() begin...");
        Intelligence ourIntel = ourRace.getEmpire().getIntelligence();
        Intelligence intelEnemy = enemyRace.getEmpire().getIntelligence();

        //read the inner security of the victim
        long enemyInnerSecPoints = (long) intelEnemy.getSecurityPoints()
                * intelEnemy.getAssignment().getInnerSecurityPercentage() / 100;
        //+ bonus
        enemyInnerSecPoints += enemyInnerSecPoints * intelEnemy.getInnerSecurityBonus() / 100;
        //in storage the bonus is included
        int enemyInnerSecDepot = intelEnemy.getInnerSecurityStorage();
        Gdx.app.debug("intel", String.format("enemies inner security points: %d - enemies inner security depot: %d",
                enemyInnerSecPoints, enemyInnerSecDepot));

        //now check the generated points of the attacker - we use long so that if the points are high using the multiply and divide we don't overflow
        long racePoints = 0;
        long raceDepot = 0;
        if (isSpy && !isAttempt) {
            racePoints = (long) ourIntel.getSecurityPoints()
                    * ourIntel.getAssignment().getGlobalSpyPercentage(enemyRace.getRaceId())
                    * ourIntel.getAssignment().getSpyPercentages(enemyRace.getRaceId(), type) / 10000;
            racePoints += racePoints * ourIntel.getBonus(type, 0) / 100;

            raceDepot = (long) ourIntel.getSPStorage(0, enemyRace.getRaceId())
                    * ourIntel.getAssignment().getSpyPercentages(enemyRace.getRaceId(), type) / 100;
            raceDepot += raceDepot * ourIntel.getBonus(type, 0) / 100;
        } else if (!isSpy && !isAttempt) {
            racePoints = (long) ourIntel.getSecurityPoints()
                    * ourIntel.getAssignment().getGlobalSabotagePercentage(enemyRace.getRaceId())
                    * ourIntel.getAssignment().getSabotagePercentages(enemyRace.getRaceId(), type) / 10000;
            racePoints += racePoints * ourIntel.getBonus(type, 1) / 100;

            raceDepot = (long) ourIntel.getSPStorage(1, enemyRace.getRaceId())
                    * ourIntel.getAssignment().getSabotagePercentages(enemyRace.getRaceId(), type) / 100;
            raceDepot += raceDepot * ourIntel.getBonus(type, 1) / 100;
        } else if (isAttempt) {
            raceDepot = (long) ourIntel.getSPStorage(1, enemyRace.getRaceId())
                    * ourIntel.getAssignment().getSabotagePercentages(enemyRace.getRaceId(), type) / 100;
            raceDepot += raceDepot * ourIntel.getBonus(type, 1) / 100;
        }
        Gdx.app.debug("intel", String.format("racePoints: %d - raceDepot: %d", racePoints, raceDepot));
        //first remove the points from both - first the points which don't come from the storage
        int temp = (int) racePoints + (int) raceDepot - (int)enemyInnerSecPoints;
        if (temp > 0)
            intelEnemy.addInnerSecurityPoints(-temp);

        temp = (int)enemyInnerSecPoints + enemyInnerSecDepot - (int) racePoints;
        if (temp > 0)
            ourIntel.addSPStoragePoints(isSpy == false ? 1 : 0, enemyRace.getRaceId(), -temp);
    }

    /**
     * This function calls the correspoding action function
     * @param race
     * @param enemyRace
     * @param responsibleRace
     * @param type
     * @param isSpy
     * @return
     */
    private boolean executeAction(Major race, Major enemyRace, Major responsibleRace, int type, boolean isSpy) {
        String responsibleRaceID = responsibleRace == null ? "" : responsibleRace.getRaceId();
        if (isSpy) {
            switch (type) {
                case 0:
                    return executeEconomySpy(race, enemyRace, responsibleRaceID, true);
                case 1:
                    return executeScienceSpy(race, enemyRace, responsibleRaceID, true);
                case 2:
                    return executeMilitarySpy(race, enemyRace, responsibleRaceID, true);
                case 3:
                    return executeDiplomacySpy(race, enemyRace, responsibleRaceID, true);
            }
        } else {
            switch (type) {
                case 0:
                    return executeEconomySabotage(race, enemyRace, responsibleRaceID, -1);
                case 1:
                    return executeScienceSabotage(race, enemyRace, responsibleRaceID, -1);
                case 2:
                    return executeMilitarySabotage(race, enemyRace, responsibleRaceID, -1);
                case 3:
                    return executeDiplomacySabogate(race, enemyRace, responsibleRaceID, -1);
            }
        }
        return false;
    }

    /**
     * This function executes an economic espionage action
     * @param race
     * @param enemyRace
     * @param responsibleRace
     * @param createText
     * @return
     */
    private boolean executeEconomySpy(Major race, Major enemyRace, String responsibleRace, boolean createText) {
        //There are two types of espionage in economy, first we can find out their credits, or we can check buildings in a system
        //1st try: credits
        if ((int) (RandUtil.random() * 8) == 0) {
            int credits = enemyRace.getEmpire().getCredits();
            EconIntelObject report = new EconIntelObject(race.getRaceId(), enemyRace.getRaceId(),
                    manager.getCurrentRound(), true, credits);
            if (createText)
                report.createText(manager, 1, responsibleRace);
            race.getEmpire().getIntelligence().getIntelReports().addReport(report);
            return true;
        }

        //2nd try: buildings - it's entirely random
        Array<IntPoint> sectors = new Array<IntPoint>();
        for (int y = 0; y < manager.getGridSizeY(); y++)
            for (int x = 0; x < manager.getGridSizeX(); x++)
                if (manager.getUniverseMap().getStarSystemAt(x, y).getOwnerId().equals(enemyRace.getRaceId()))
                    sectors.add(new IntPoint(x, y));
        if (sectors.size != 0) {
            int random = (int) (RandUtil.random() * sectors.size);

            //with the economy spy we can find out about all buildings except science and espionage - which one we find out about is random, but buildings that need workers have a higher chance
            if ((int) (RandUtil.random() * 4) == 0) { //25% chance of getting info about a building that doesn't requires workers
                int number = manager.getUniverseMap().getStarSystemAt(sectors.get(random)).getAllBuildings().size;
                if (number > 0) {
                    int j = 0;
                    for (int i = (int) (RandUtil.random() * number); i < number; i++) {
                        BuildingInfo bi = manager.getBuildingInfo(manager.getUniverseMap()
                                .getStarSystemAt(sectors.get(random)).getAllBuildings().get(i).getRunningNumber());
                        if (!bi.getWorker()) {
                            //if the building has no economic related output, it can't be spied on
                            if (!bi.isBarrack() && bi.getResearchPointsProd() == 0 && bi.getGroundDefend() == 0
                                    && bi.getGroundDefendBonus() == 0 && bi.getMilitarySabogateBonus() == 0
                                    && bi.getMilitarySpyBonus() == 0 && bi.getResearchBonus() == 0
                                    && bi.getResearchSabotageBonus() == 0 && bi.getResearchSpyBonus() == 0
                                    && bi.getResistance() == 0 && bi.getScanPower() == 0 && bi.getScanPowerBonus() == 0
                                    && bi.getScanRange() == 0 && bi.getScanRangeBonus() == 0
                                    && bi.getSecurityBonus() == 0 && bi.getShieldPower() == 0
                                    && bi.getShieldPowerBonus() == 0 && bi.getShipDefend() == 0
                                    && bi.getShipDefendBonus() == 0 && bi.getShipTraining() == 0 && !bi.isShipYard()
                                    && bi.getSecurityPointsProd() == 0 && bi.getTroopTraining() == 0) {
                                int id = bi.getRunningNumber();
                                int n = manager.getUniverseMap().getStarSystemAt(sectors.get(random))
                                        .getNumberofBuilding(id);
                                EconIntelObject report = new EconIntelObject(race.getRaceId(), enemyRace.getRaceId(),
                                        manager.getCurrentRound(), true, sectors.get(random), id, n);
                                if (createText)
                                    report.createText(manager, 0, responsibleRace);
                                race.getEmpire().getIntelligence().getIntelReports().addReport(report);
                                return true;
                            }
                        }
                        if (i == number - 1)
                            i = 0;
                        j++;
                        if (j == number)
                            break;
                    }
                }

            }
            //otherwise get info about worker buildings
            int buildingTypes[] = new int[WorkerType.IRIDIUM_WORKER.getType() + 1];
            Arrays.fill(buildingTypes, 0);
            int start = (int) (RandUtil.random() * (WorkerType.IRIDIUM_WORKER.getType() + 1));
            int j = 0;
            for (int i = start; i <= WorkerType.IRIDIUM_WORKER.getType(); i++) {
                if (i != WorkerType.SECURITY_WORKER.getType() && i != WorkerType.RESEARCH_WORKER.getType()) {
                    WorkerType worker = WorkerType.fromWorkerType(i);
                    buildingTypes[i] = manager.getUniverseMap().getStarSystemAt(sectors.get(random))
                            .getNumberOfWorkBuildings(worker, 0);
                    if (buildingTypes[i] > 0) {
                        start = i;
                        break;
                    }
                }
                if (i == WorkerType.IRIDIUM_WORKER.getType())
                    i = WorkerType.FOOD_WORKER.getType();
                j++;
                if (j == WorkerType.IRIDIUM_WORKER.getType())
                    break;
            }
            if (buildingTypes[start] > 0) {
                WorkerType worker = WorkerType.fromWorkerType(start);
                EconIntelObject report = new EconIntelObject(race.getRaceId(), enemyRace.getRaceId(),
                        manager.getCurrentRound(), true, sectors.get(random), manager.getUniverseMap()
                                .getStarSystemAt(sectors.get(random)).getNumberOfWorkBuildings(worker, 1),
                        buildingTypes[start]);
                if (createText)
                    report.createText(manager, 0, responsibleRace);
                race.getEmpire().getIntelligence().getIntelReports().addReport(report);
                return true;
            }
        }
        return false;
    }

    /**
     * This function executes an science espionage action
     * @param race
     * @param enemyRace
     * @param responsibleRace
     * @param createText
     * @return
     */
    private boolean executeScienceSpy(Major race, Major enemyRace, String responsibleRace, boolean createText) {
        //There are 4 types of science espionage. First we can spy on the science buildings of a system. Secondly we can find out the number of total research points.
        //Thirdly we can find out about one of the techlevels and the fourth option is finding out about the special research

        //4th type = special research
        if ((int) (RandUtil.random() * 8) == 0) {
            ResearchComplexType specialTech = ResearchComplexType.NONE;
            int choosen = -1;
            int t = (int) (RandUtil.random() * ResearchComplexType.COMPLEX_COUNT.getType());
            int j = 0;
            for (int i = t; i < ResearchComplexType.COMPLEX_COUNT.getType(); i++) {
                ResearchComplexType complex = ResearchComplexType.fromInt(i);
                if (enemyRace.getEmpire().getResearch().getResearchInfo().getResearchComplex(complex)
                        .getComplexStatus() == ResearchStatus.RESEARCHED) {
                    specialTech = complex;
                    for (int k = 1; k <= 3; k++)
                        if (enemyRace.getEmpire().getResearch().getResearchInfo().getResearchComplex(complex)
                                .getFieldStatus(k) == ResearchStatus.RESEARCHED) {
                            choosen = k;
                            break;
                        }
                    break;
                }

                if (i == ResearchComplexType.COMPLEX_COUNT.getType() - 1)
                    i = 0;
                j++;
                if (j == ResearchComplexType.COMPLEX_COUNT.getType())
                    break;
            }
            //If the empire has researched a Complex then create report
            if (specialTech != ResearchComplexType.NONE) {
                ScienceIntelObject report = new ScienceIntelObject(race.getRaceId(), enemyRace.getRaceId(),
                        manager.getCurrentRound(), true, -1, -1, specialTech, choosen);
                if (createText)
                    report.createText(manager, 3, responsibleRace);
                race.getEmpire().getIntelligence().getIntelReports().addReport(report);
                return true;
            }
        }

        //3rd type: Trying to find out about techlevel
        if ((int) (RandUtil.random() * 5) == 0) {
            int techLevel = -1;
            int techType = (int) (RandUtil.random() * 6);
            techLevel = enemyRace.getEmpire().getResearch().getResearchLevel(ResearchType.fromType(techType));
            ScienceIntelObject report = new ScienceIntelObject(race.getRaceId(), enemyRace.getRaceId(),
                    manager.getCurrentRound(), true, techLevel, techType, ResearchComplexType.NONE, -1);
            if (createText)
                report.createText(manager, 2, responsibleRace);
            race.getEmpire().getIntelligence().getIntelReports().addReport(report);
            return true;
        }

        //2nd type: trying to find out about the produced research points
        if ((int) (RandUtil.random() * 6) == 0) {
            int rp = enemyRace.getEmpire().getResearchPoints();
            ScienceIntelObject report = new ScienceIntelObject(race.getRaceId(), enemyRace.getRaceId(),
                    manager.getCurrentRound(), true, rp);
            if (createText)
                report.createText(manager, 1, responsibleRace);
        }

        //if nothing was tried until now, try the 1st type
        Array<IntPoint> sectors = new Array<IntPoint>();
        for (int y = 0; y < manager.getGridSizeY(); y++)
            for (int x = 0; x < manager.getGridSizeX(); x++)
                if (manager.getUniverseMap().getStarSystemAt(x, y).getOwnerId().equals(enemyRace.getRaceId()))
                    sectors.add(new IntPoint(x, y));
        if (sectors.size != 0) {
            int random = (int) (RandUtil.random() * sectors.size);
            //we can find out about laboratories and other buildings that are related to science
            if ((int) (RandUtil.random() * 4) == 0) { //25% chance of getting info about a building that doesn't requires workers
                int number = manager.getUniverseMap().getStarSystemAt(sectors.get(random)).getAllBuildings().size;
                if (number > 0) {
                    int j = 0;
                    for (int i = (int) (RandUtil.random() * number); i < number; i++) {
                        BuildingInfo bi = manager.getBuildingInfo(manager.getUniverseMap()
                                .getStarSystemAt(sectors.get(random)).getAllBuildings().get(i).getRunningNumber());
                        if (!bi.getWorker()) {
                            //if the building has no economic related output, it can't be spied on
                            if (bi.getResearchPointsProd() > 0 || bi.getResearchBonus() > 0 || bi.getBioTechBonus() > 0
                                    || bi.getComputerTechBonus() > 0 || bi.getConstructionTechBonus() > 0
                                    || bi.getEnergyTechBonus() > 0 || bi.getPropulsionTechBonus() > 0
                                    || bi.getWeaponTechBonus() > 0) {
                                int id = bi.getRunningNumber();
                                int n = manager.getUniverseMap().getStarSystemAt(sectors.get(random))
                                        .getNumberofBuilding(id);
                                ScienceIntelObject report = new ScienceIntelObject(race.getRaceId(),
                                        enemyRace.getRaceId(), manager.getCurrentRound(), true, sectors.get(random),
                                        id, n);
                                if (createText)
                                    report.createText(manager, 0, responsibleRace);
                                race.getEmpire().getIntelligence().getIntelReports().addReport(report);
                                return true;
                            }
                        }
                        if (i == number - 1)
                            i = 0;
                        j++;
                        if (j == number)
                            break;
                    }
                }

            }

            //otherwise we spy on buildings that need workers
            int buildings = manager.getUniverseMap().getStarSystemAt(sectors.get(random))
                    .getNumberOfWorkBuildings(WorkerType.RESEARCH_WORKER, 0);
            if (buildings > 0) {
                ScienceIntelObject report = new ScienceIntelObject(race.getRaceId(), enemyRace.getRaceId(),
                        manager.getCurrentRound(), true, sectors.get(random), manager.getUniverseMap()
                                .getStarSystemAt(sectors.get(random))
                                .getNumberOfWorkBuildings(WorkerType.RESEARCH_WORKER, 1), buildings);
                if (createText)
                    report.createText(manager, 0, responsibleRace);
                race.getEmpire().getIntelligence().getIntelReports().addReport(report);
                return true;
            }
        }
        return false;
    }

    /**
     * The function executes a military espionage action
     * @param race
     * @param enemyRace
     * @param responsibleRace
     * @param createText
     * @return
     */
    private boolean executeMilitarySpy(Major race, Major enemyRace, String responsibleRace, boolean createText) {
        //There are three types of military espionage. First we can spy on military and security buildings. Then we can spy on troops in a system and third we can spy on ships and outpost in a system

        //3rd type - ships and stations
        if ((int) (RandUtil.random() * 3) == 0) { //33% chance
            Array<Ships> ships = new Array<Ships>();
            Array<Ships> stations = new Array<Ships>();
            for (int i = 0; i < manager.getUniverseMap().getShipMap().getSize(); i++) {
                Ships s = manager.getUniverseMap().getShipMap().getAt(i);
                if (s.getOwnerId().equals(enemyRace.getRaceId())) {
                    if (s.isStation())
                        stations.add(s);
                    else
                        ships.add(s);
                }
            }
            MilitaryIntelObject report = null;
            //if there are stations - 33% chance to spy on them
            if (stations.size > 0 && (int) (RandUtil.random() * 3) == 0) {
                int t = (int) (RandUtil.random() * stations.size);
                report = new MilitaryIntelObject(race.getRaceId(), enemyRace.getRaceId(), manager.getCurrentRound(),
                        true, stations.get(t).getCoordinates(), stations.get(t).getID(), 1, false, true, false);
            } else if (ships.size > 0) {
                int t = (int) (RandUtil.random() * ships.size);
                int number = 0;
                for (int i = 0; i < ships.size; i++)
                    if (ships.get(i).getCoordinates().equals(ships.get(t).getCoordinates())) {
                        number++;
                        number += ships.get(i).getFleetSize();
                    }
                report = new MilitaryIntelObject(race.getRaceId(), enemyRace.getRaceId(), manager.getCurrentRound(),
                        true, ships.get(t).getCoordinates(), ships.get(t).getID(), number, false, true, false);
            }
            if (report != null) {
                if (createText)
                    report.createText(manager, 2, responsibleRace);
                race.getEmpire().getIntelligence().getIntelReports().addReport(report);
                return true;
            }
        }

        //2nd type - stationed troops
        if ((int) (RandUtil.random() * 4) == 0) {
            Array<IntPoint> troopSectors = new Array<IntPoint>();
            for (int y = 0; y < manager.getGridSizeY(); y++)
                for (int x = 0; x < manager.getGridSizeX(); x++)
                    if (manager.getUniverseMap().getStarSystemAt(x, y).getOwnerId().equals(enemyRace.getRaceId()))
                        if (manager.getUniverseMap().getStarSystemAt(x, y).getTroops().size != 0)
                            troopSectors.add(new IntPoint(x, y));

            if (troopSectors.size != 0) {
                int random = (int) (RandUtil.random() * troopSectors.size);
                MilitaryIntelObject report;
                StarSystem ss = manager.getUniverseMap().getStarSystemAt(troopSectors.get(random));
                if (ss.getTroops().size == 1)
                    report = new MilitaryIntelObject(race.getRaceId(), enemyRace.getRaceId(),
                            manager.getCurrentRound(), true, troopSectors.get(random),
                            ss.getTroops().get(0).getID() + 20000, 1, false, false, true);
                else
                    report = new MilitaryIntelObject(race.getRaceId(), enemyRace.getRaceId(),
                            manager.getCurrentRound(), true, troopSectors.get(random), 20000, ss.getTroops().size,
                            false, false, true);
                if (createText)
                    report.createText(manager, 1, responsibleRace);
                race.getEmpire().getIntelligence().getIntelReports().addReport(report);
                return true;
            }
        }

        //1st type - buildings
        Array<IntPoint> sectors = new Array<IntPoint>();
        for (int y = 0; y < manager.getGridSizeY(); y++)
            for (int x = 0; x < manager.getGridSizeX(); x++)
                if (manager.getUniverseMap().getStarSystemAt(x, y).getOwnerId().equals(enemyRace.getRaceId()))
                    sectors.add(new IntPoint(x, y));
        if (sectors.size > 0) {
            int random = (int) (RandUtil.random() * sectors.size);
            StarSystem ss = manager.getUniverseMap().getStarSystemAt(sectors.get(random));
            //we can find out about buildings that are related to military or intelligence. This happens randomly, and all types have equal chance
            if ((int) (RandUtil.random() * 3) == 0) { //33% chance of spying on a worker needing building
                int buildings = ss.getNumberOfWorkBuildings(WorkerType.SECURITY_WORKER, 0);
                if (buildings > 0) {
                    MilitaryIntelObject report = new MilitaryIntelObject(race.getRaceId(), enemyRace.getRaceId(),
                            manager.getCurrentRound(), true, sectors.get(random), ss.getNumberOfWorkBuildings(
                                    WorkerType.SECURITY_WORKER, 1), buildings, true, false, false);
                    if (createText)
                        report.createText(manager, 0, responsibleRace);
                    race.getEmpire().getIntelligence().getIntelReports().addReport(report);
                    return true;
                }
            }

            int number = ss.getAllBuildings().size;
            if (number > 0) {
                int j = 0;
                for (int i = (int) (RandUtil.random() * number); i < number; i++) {
                    BuildingInfo bi = manager.getBuildingInfo(ss.getAllBuildings().get(i).getRunningNumber());
                    if (!bi.getWorker()) {
                        if (bi.isShipYard() || bi.getSecurityPointsProd() > 0 || bi.isBarrack()
                                || bi.getBarrackSpeed() > 0 || bi.getEconomySabotageBonus() > 0
                                || bi.getEconomySpyBonus() > 0 || bi.getGroundDefend() > 0
                                || bi.getGroundDefendBonus() > 0 || bi.getMilitarySabogateBonus() > 0
                                || bi.getMilitarySpyBonus() > 0 || bi.getResistance() > 0 || bi.getScanPower() > 0
                                || bi.getScanPowerBonus() > 0 || bi.getScanRange() > 0 || bi.getScanRangeBonus() > 0
                                || bi.getSecurityBonus() > 0 || bi.getShieldPower() > 0 || bi.getShieldPowerBonus() > 0
                                || bi.getShipBuildSpeed() > 0 || bi.getShipDefend() > 0 || bi.getShipDefendBonus() > 0
                                || bi.getShipTraining() > 0 || bi.getTroopBuildSpeed() > 0 || bi.getTroopTraining() > 0) {
                            int id = bi.getRunningNumber();
                            int n = ss.getNumberofBuilding(id);
                            MilitaryIntelObject report = new MilitaryIntelObject(race.getRaceId(),
                                    enemyRace.getRaceId(), manager.getCurrentRound(), true, sectors.get(random), id, n,
                                    true, false, false);
                            if (createText)
                                report.createText(manager, 0, responsibleRace);
                            race.getEmpire().getIntelligence().getIntelReports().addReport(report);
                            return true;
                        }
                    }
                    if (i == number - 1)
                        i = 0;
                    j++;
                    if (j == number)
                        break;
                }
            }
        }
        return false;
    }

    /**
     * Function executes a diplomatic espionage action
     * @param race
     * @param enemyRace
     * @param responsibleRace
     * @param createText
     * @return
     */
    private boolean executeDiplomacySpy(Major race, Major enemyRace, String responsibleRace, boolean createText) {
        //There are 4 types of diplomatic espionage. First we can find out if a major knows a minor. Second we can find out what treaty a major has with a known Minor.
        //Thirdly we can find out what treaty a major has with a different major, fourth we can find out the relationship between races
        Array<String> minors = new Array<String>();
        ArrayMap<String, Minor> minormap = manager.getRaceController().getMinors();
        for (Iterator<Entry<String, Minor>> iter = minormap.entries().iterator(); iter.hasNext();) {
            ObjectMap.Entry<String, Minor> e = iter.next();
            if (enemyRace.isRaceContacted(e.key))
                minors.add(e.key);
        }
        Array<String> majors = new Array<String>();
        ArrayMap<String, Major> majormap = manager.getRaceController().getMajors();
        for (int i = 0; i < majormap.size; i++) {
            String mID = majormap.getKeyAt(i);
            if (!mID.equals(race.getRaceId()) && !mID.equals(enemyRace.getRaceId()))
                if (enemyRace.isRaceContacted(mID))
                    majors.add(mID);
        }

        //4th type - get relationship
        if ((int) (RandUtil.random() * 3) == 0) {
            DiplomacyIntelObject report = null;
            if ((int) (RandUtil.random() * 2) == 0 && minors.size > 0) {
                String minorID = minors.get((int) (RandUtil.random() * minors.size));
                Minor minor = minormap.get(minorID);
                IntPoint coord = minor.getCoordinates();
                if (!coord.equals(new IntPoint()))
                    report = new DiplomacyIntelObject(race.getRaceId(), enemyRace.getRaceId(),
                            manager.getCurrentRound(), true, coord, DiplomaticAgreement.NONE,
                            minor.getRelation(enemyRace.getRaceId()));
            } else if (majors.size > 0) {
                String majorID = majors.get((int) (RandUtil.random() * majors.size));
                report = new DiplomacyIntelObject(race.getRaceId(), enemyRace.getRaceId(), manager.getCurrentRound(),
                        true, majorID, DiplomaticAgreement.NONE, 0, majormap.get(majorID).getRelation(
                                enemyRace.getRaceId()));
            }
            if (report != null) {
                if (createText)
                    report.createText(manager, 3, responsibleRace);
                race.getEmpire().getIntelligence().getIntelReports().addReport(report);
                return true;
            }
        }

        //3rd type - try to find out agreement with another major
        if ((int) (RandUtil.random() * 4) == 0) {
            if (majors.size > 0) {
                String majorID = majors.get((int) (RandUtil.random() * majors.size));
                DiplomaticAgreement agreement = DiplomaticAgreement.NONE;
                int duration = 0;
                if ((int) (RandUtil.random() * 3) == 0 && enemyRace.getDefencePact(majorID)) {
                    agreement = DiplomaticAgreement.DEFENCEPACT;
                    duration = enemyRace.getDefencePactDuration(majorID);
                } else {
                    agreement = enemyRace.getAgreement(majorID);
                    duration = enemyRace.getAgreementDuration(majorID);
                }

                DiplomacyIntelObject report = new DiplomacyIntelObject(race.getRaceId(), enemyRace.getRaceId(),
                        manager.getCurrentRound(), true, majorID, agreement, duration, majormap.get(majorID)
                                .getRelation(enemyRace.getRaceId()));
                if (createText)
                    report.createText(manager, 2, responsibleRace);
                race.getEmpire().getIntelligence().getIntelReports().addReport(report);
                return true;
            }
        }

        //2nd type - try to find out about treaty with minor
        if ((int) (RandUtil.random() * 2) == 0) {
            if (minors.size > 0) {
                DiplomacyIntelObject report = null;
                String minorID = minors.get((int) (RandUtil.random() * minors.size));
                Minor minor = minormap.get(minorID);
                IntPoint coord = minor.getCoordinates();
                if (!coord.equals(new IntPoint()))
                    report = new DiplomacyIntelObject(race.getRaceId(), enemyRace.getRaceId(),
                            manager.getCurrentRound(), true, coord, minor.getAgreement(enemyRace.getRaceId()),
                            minor.getRelation(enemyRace.getRaceId()));
                if (report != null) {
                    if (createText)
                        report.createText(manager, 1, responsibleRace);
                    race.getEmpire().getIntelligence().getIntelReports().addReport(report);
                    return true;
                }
            }
        }

        //1st type - find out if they know a minors
        if (minors.size > 0) {
            DiplomacyIntelObject report = null;
            String minorID = minors.get((int) (RandUtil.random() * minors.size));
            Minor minor = minormap.get(minorID);
            IntPoint coord = minor.getCoordinates();
            if (!coord.equals(new IntPoint()))
                report = new DiplomacyIntelObject(race.getRaceId(), enemyRace.getRaceId(), manager.getCurrentRound(),
                        true, coord);
            if (report != null) {
                if (createText)
                    report.createText(manager, 0, responsibleRace);
                race.getEmpire().getIntelligence().getIntelReports().addReport(report);
                return true;
            }
        }
        return false;
    }

    /**
     * The function executes an economic sabotage action
     * @param race
     * @param enemyRace
     * @param responsibleRace
     * @param reportNumber - number of the object on which the action is executed on
     * @return
     */
    private boolean executeEconomySabotage(Major race, Major enemyRace, String responsibleRace, int reportNumber) {
        //The corresponding action is executed. This will generate an intel report.
        //To make it faster, text generation won't happen here. As soon as the report is there the action will be generated from it.
        //After this the old report is removed and a new one will replace it
        int oldReportNumber = reportNumber;
        int newReportNumber = oldReportNumber + 1;
        IntelReports intelReports = race.getEmpire().getIntelligence().getIntelReports();
        if (reportNumber == -1) {
            oldReportNumber = intelReports.getNumberOfReports();
            executeEconomySpy(race, enemyRace, responsibleRace, false);
            newReportNumber = intelReports.getNumberOfReports();
        }
        //if the espionage action was successful, then this can be turned into a sabotage action
        if (newReportNumber > oldReportNumber) {
            EconIntelObject report = (EconIntelObject) intelReports.getReport(oldReportNumber);
            int credits = report.getCredits() / 2;
            //1st possibility: steal credits
            if (credits > 0) {
                credits = (int) (RandUtil.random() * credits) + 1;
                race.getEmpire().setCredits(credits);
                enemyRace.getEmpire().setCredits(-credits);
                intelReports.removeReport(oldReportNumber);
                report = new EconIntelObject(race.getRaceId(), enemyRace.getRaceId(), manager.getCurrentRound(), false,
                        credits);
                report.createText(manager, 1, responsibleRace);
                intelReports.addReport(report);
                enemyRace.getEmpire().getIntelligence().getIntelReports().addReport(new EconIntelObject(report));
                return true;
            }
            //2nd possibility: destroy buildings/kill population
            int buildings = report.getNumber();
            if (buildings > 0) {
                IntPoint coord = report.getCoord();
                StarSystem ss = manager.getUniverseMap().getStarSystemAt(coord);
                //16.6% chance to poison food supply - which kills people
                if ((int) (RandUtil.random() * 6) == 0) {
                    int currentHabs = (int) ss.getCurrentInhabitants();
                    currentHabs /= 2;
                    if (currentHabs > 0) {
                        currentHabs = (int) (RandUtil.random() * currentHabs) + 1;
                        intelReports.removeReport(oldReportNumber);
                        report = new EconIntelObject(race.getRaceId(), enemyRace.getRaceId(),
                                manager.getCurrentRound(), false, coord, 0, currentHabs);
                        ss.letPlanetsShrink(-(float) currentHabs);
                        report.createText(manager, 2, responsibleRace);
                        intelReports.addReport(report);
                        enemyRace.getEmpire().getIntelligence().getIntelReports()
                                .addReport(new EconIntelObject(report));
                        return true;
                    }
                }
                //maxium half of the buildings can be destroyed in one attempt
                buildings /= 2;
                if (buildings == 0) //at least one should be destoyed
                    buildings++;
                buildings = (int) (RandUtil.random() * buildings) + 1;
                int id = report.getID();
                //now destroy the building in the system
                int destoyed = 0;
                for (int i = 0; i < ss.getAllBuildings().size; i++)
                    if (ss.getAllBuildings().get(i).getRunningNumber() == id) {
                        ss.getAllBuildings().removeIndex(i--);
                        destoyed++;
                        buildings--;
                        if (buildings == 0)
                            break;
                    }

                if (destoyed > 0) {
                    intelReports.removeReport(oldReportNumber);
                    report = new EconIntelObject(race.getRaceId(), enemyRace.getRaceId(), manager.getCurrentRound(),
                            false, coord, id, destoyed);
                    report.createText(manager, 0, responsibleRace);
                    intelReports.addReport(report);
                    enemyRace.getEmpire().getIntelligence().getIntelReports().addReport(new EconIntelObject(report));
                    return true;
                }
            }
            intelReports.removeReport(oldReportNumber);
        }
        return false;
    }

    /**
     * The function executes an science sabotage action
     * @param race
     * @param enemyRace
     * @param responsibleRace
     * @param reportNumber
     * @return
     */
    private boolean executeScienceSabotage(Major race, Major enemyRace, String responsibleRace, int reportNumber) {
        //The corresponding action is executed. This will generate an intel report.
        //To make it faster, text generation won't happen here. As soon as the report is there the action will be generated from it.
        //After this the old report is removed and a new one will replace it
        int oldReportNumber = reportNumber;
        int newReportNumber = oldReportNumber + 1;
        IntelReports intelReports = race.getEmpire().getIntelligence().getIntelReports();
        if (reportNumber == -1) {
            oldReportNumber = intelReports.getNumberOfReports();
            executeScienceSpy(race, enemyRace, responsibleRace, false);
            newReportNumber = intelReports.getNumberOfReports();
        }
        //if the espionage action was successful, then this can be turned into a sabotage action
        if (newReportNumber > oldReportNumber) {
            ScienceIntelObject report = (ScienceIntelObject) intelReports.getReport(oldReportNumber);

            //4. - special research
            //-> it cannot be sabotaged, so if a report comes up, it will be deleted and we try to generate a new one until it won't be related to special research
            ResearchComplexType specialTech = report.getSpecialTechComplex();
            while (specialTech != ResearchComplexType.NONE) {
                intelReports.removeReport(oldReportNumber);
                oldReportNumber = intelReports.getNumberOfReports();
                boolean isTrue = false;
                do {
                    isTrue = executeScienceSpy(race, enemyRace, responsibleRace, false);
                } while (!isTrue);
                report = (ScienceIntelObject) intelReports.getReport(oldReportNumber);
                specialTech = report.getSpecialTechComplex();
            }

            //3. - sabotage a tech area
            int techType = report.getTechType();
            if (techType != -1) {
                int currentRP = enemyRace.getEmpire().getResearch().getResearchLevel(ResearchType.fromType(techType));
                if (currentRP > 0) {
                    currentRP = (int) (RandUtil.random() * currentRP) + 1;
                    int techLevel = report.getTechLevel();
                    enemyRace.getEmpire().getResearch().setRP(ResearchType.fromType(techType), currentRP);
                    intelReports.removeReport(oldReportNumber);
                    report = new ScienceIntelObject(race.getRaceId(), enemyRace.getRaceId(), manager.getCurrentRound(),
                            false, techLevel, techType, ResearchComplexType.NONE, -1);
                    report.createText(manager, 2, responsibleRace);
                    intelReports.addReport(report);
                    enemyRace.getEmpire().getIntelligence().getIntelReports().addReport(new ScienceIntelObject(report));
                    return true;
                }
            }

            //2. - steal research points
            int rp = report.getRP();
            if (rp > enemyRace.getEmpire().getResearchPoints())
                rp = enemyRace.getEmpire().getResearchPoints();
            if (rp > 0) {
                race.getEmpire().addResearchPoints(rp);
                enemyRace.getEmpire().addResearchPoints(-rp);
                intelReports.removeReport(oldReportNumber);
                report = new ScienceIntelObject(race.getRaceId(), enemyRace.getRaceId(), manager.getCurrentRound(),
                        false, rp);
                report.createText(manager, 1, responsibleRace);
                enemyRace.getEmpire().getIntelligence().getIntelReports().addReport(new ScienceIntelObject(report));
                return true;
            }

            //1. - try destorying science buildings
            int buildings = report.getNumber();
            if (buildings > 0) {
                //we can destroy maximum half of the buildings
                buildings /= 2;
                if (buildings == 0)
                    buildings++;
                buildings = (int) (RandUtil.random() * buildings) + 1;
                IntPoint coord = report.getCoord();
                int id = report.getID();
                int destroyed = 0;
                StarSystem ss = manager.getUniverseMap().getStarSystemAt(coord);
                for (int i = 0; i < ss.getAllBuildings().size; i++)
                    if (ss.getAllBuildings().get(i).getRunningNumber() == id) {
                        ss.getAllBuildings().removeIndex(i--);
                        buildings--;
                        destroyed++;
                        if (buildings == 0)
                            break;
                    }
                if (destroyed > 0) {
                    intelReports.removeReport(oldReportNumber);
                    report = new ScienceIntelObject(race.getRaceId(), enemyRace.getRaceId(), manager.getCurrentRound(),
                            false, coord, id, destroyed);
                    report.createText(manager, 0, responsibleRace);
                    intelReports.addReport(report);
                    enemyRace.getEmpire().getIntelligence().getIntelReports().addReport(new ScienceIntelObject(report));
                    return true;
                }
            }
            intelReports.removeReport(oldReportNumber);
        }
        return false;
    }

    /**
     * The function executes an military sabotage action
     * @param race
     * @param enemyRace
     * @param responsibleRace
     * @param reportNumber
     * @return
     */
    public boolean executeMilitarySabotage(Major race, Major enemyRace, String responsibleRace, int reportNumber) {
        //The corresponding action is executed. This will generate an intel report.
        //To make it faster, text generation won't happen here. As soon as the report is there the action will be generated from it.
        //After this the old report is removed and a new one will replace it
        int oldReportNumber = reportNumber;
        int newReportNumber = oldReportNumber + 1;
        IntelReports intelReports = race.getEmpire().getIntelligence().getIntelReports();
        if (reportNumber == -1) {
            oldReportNumber = intelReports.getNumberOfReports();
            executeMilitarySpy(race, enemyRace, responsibleRace, false);
            newReportNumber = intelReports.getNumberOfReports();
        }
        //if the espionage action was successful, then this can be turned into a sabotage action
        if (newReportNumber > oldReportNumber) {
            MilitaryIntelObject report = (MilitaryIntelObject) intelReports.getReport(oldReportNumber);

            //3rd type: destroy, steal, damage ships or destroy/damage stations
            if (report.isShip()) {
                Ships ship = null;
                Array<IntPoint> allShips = new Array<IntPoint>();
                for (int i = 0; i < manager.getUniverseMap().getShipMap().getSize(); i++) {
                    Ships s = manager.getUniverseMap().getShipMap().getAt(i);
                    if (s.getCoordinates().equals(report.getCoord()) && !s.getOwnerId().equals(report.getOwner())) {
                        //if the ship a fleet has then our ship can be somewhere inside the fleet
                        for (int j = 0; j < s.getFleetSize(); j++) {
                            Ships tmp = s.getFleet().getAt(j);
                            if (tmp.getID() == report.getID())
                                allShips.add(new IntPoint(i, j));
                        }
                        if (s.getID() == report.getID())
                            allShips.add(new IntPoint(i, -1));
                    }
                }
                //get a random ship from the list
                if (allShips.size > 0) {
                    int random = (int) (RandUtil.random() * allShips.size);
                    IntPoint n = allShips.get(random);
                    Ships s = manager.getUniverseMap().getShipMap().getAt(n.x);
                    // make the affected ship a ship without a fleet in case it had one,
                    // and a ship in the global shipmap in case it was a ship in a fleet
                    if (n.y == -1) {
                        if (s.hasFleet()) {
                            Ships newFleetShip = s.giveFleetToFleetsFirstShip();
                            manager.getUniverseMap().getShipMap().add(newFleetShip);
                        }
                        ship = s;
                    } else {
                        Ships tmp = s.getFleet().getAt(n.y);
                        ship = tmp;
                        s.removeShipFromFleet(n.y);
                        manager.getUniverseMap().getShipMap().add(ship);
                    }
                }

                if (ship == null)
                    return false;

                //now we can sabotage a ship/station
                //a station will be damaged or with a smaller chance destroyed
                //a ship will be either damaged, or with a smaller chance destroyed or stolen

                //steal ship
                if ((int) (RandUtil.random() * 2) == 0 && !ship.isStation()) {
                    intelReports.removeReport(oldReportNumber);
                    report = new MilitaryIntelObject(race.getRaceId(), enemyRace.getRaceId(),
                            manager.getCurrentRound(), false, ship.getCoordinates(), ship.getID(), 1, false, true,
                            false);
                    //remove ship/station from the ship history and add to the destroyed ones
                    enemyRace.getShipHistory().modifyShip(ship.shipHistoryInfo(),
                            manager.getUniverseMap().getStarSystemAt(ship.getCoordinates()).coordsName(true),
                            manager.getCurrentRound(), StringDB.getString("SABOTAGE"), StringDB.getString("MISSED"));

                    //move ship to an own system
                    IntPoint newCoord = StarMap.getNearestPort(manager, ship.getCoordinates(), race.getRaceId());
                    ship.setCoordinates(newCoord, race.getRaceId());
                    //transfer ship (setcoordinates already changes the owner)
                    ship.setFlagShip(false);
                    ship.unsetCurrentOrder();
                    ship.setCombatTactics(CombatTactics.CT_AVOID);
                    ship.addSpecialResearchBoni(race);

                    //If the ship was stolen once already then it is already in the ship history. In this case it only needs to be modified
                    if (race.getShipHistory().modifyShip(ship.shipHistoryInfo(),
                            manager.getUniverseMap().getStarSystemAt(ship.getCoordinates()).coordsName(true), 0) == false)
                        race.getShipHistory().addShip(ship.shipHistoryInfo(),
                                manager.getUniverseMap().getStarSystemAt(ship.getCoordinates()).coordsName(true),
                                manager.getCurrentRound());
                    ship.setTargetCoord(new IntPoint());

                    report.createText(manager, 3, responsibleRace);
                    intelReports.addReport(report);
                    enemyRace.getEmpire().getIntelligence().getIntelReports()
                            .addReport(new MilitaryIntelObject(report));
                    return true;
                } else if (((int) (RandUtil.random() * 4) == 0 && ship.isStation())
                        || ((int) (RandUtil.random() * 2) == 0 && !ship.isStation())) { //destroy ship
                    intelReports.removeReport(oldReportNumber);
                    report = new MilitaryIntelObject(race.getRaceId(), enemyRace.getRaceId(),
                            manager.getCurrentRound(), false, ship.getCoordinates(), ship.getID(), 1, false, true,
                            false);
                    //remove ship from shiphistory and add a destroyed one
                    enemyRace.getShipHistory().modifyShip(ship.shipHistoryInfo(),
                            manager.getUniverseMap().getStarSystemAt(ship.getCoordinates()).coordsName(true),
                            manager.getCurrentRound(), StringDB.getString("SABOTAGE"), StringDB.getString("DESTROYED"));
                    int idx = manager.getUniverseMap().getShipMap().getShipMap().indexOfValue(ship, true);
                    manager.getUniverseMap().getShipMap().eraseAt(idx);
                    report.createText(manager, 2, responsibleRace);
                    intelReports.addReport(report);
                    enemyRace.getEmpire().getIntelligence().getIntelReports()
                            .addReport(new MilitaryIntelObject(report));
                    return true;
                } else {//damage ship/station
                    int hull = ship.getHull().getCurrentHull();
                    if (hull > 0) {
                        int newHull = (int) (RandUtil.random() * hull) + 1;
                        ship.getHull().setCurrentHull(newHull - hull);
                        intelReports.removeReport(oldReportNumber);
                        report = new MilitaryIntelObject(race.getRaceId(), enemyRace.getRaceId(),
                                manager.getCurrentRound(), false, ship.getCoordinates(), ship.getID(), 1, false, true,
                                false);
                        report.createText(manager, 4, responsibleRace);
                        intelReports.addReport(report);
                        enemyRace.getEmpire().getIntelligence().getIntelReports()
                                .addReport(new MilitaryIntelObject(report));
                        return true;
                    }
                }
            }

            //2. type - kill troops
            int troopNumber = report.getNumber();
            if (troopNumber > 0 && report.isTroop()) {
                troopNumber /= 2;
                if (troopNumber == 0)
                    troopNumber++;
                troopNumber = (int) (RandUtil.random() * troopNumber) + 1;
                IntPoint coord = report.getCoord();
                int id = report.getID();
                int destroyed = 0;
                StarSystem ss = manager.getUniverseMap().getStarSystemAt(coord);
                for (int i = 0; i < ss.getTroops().size; i++) {
                    ss.getTroops().removeIndex(i--);
                    troopNumber--;
                    destroyed++;
                    if (troopNumber == 0)
                        break;
                }
                if (destroyed > 0) {
                    intelReports.removeReport(oldReportNumber);
                    report = new MilitaryIntelObject(race.getRaceId(), enemyRace.getRaceId(),
                            manager.getCurrentRound(), false, coord, id, destroyed, false, false, true);
                    report.createText(manager, 1, responsibleRace);
                    intelReports.addReport(report);
                    enemyRace.getEmpire().getIntelligence().getIntelReports()
                            .addReport(new MilitaryIntelObject(report));
                    return true;
                }
            }

            //1st type - destroy military buildings
            int buildings = report.getNumber();
            if (buildings > 0 && report.isBuilding()) {
                buildings /= 2;
                if (buildings == 0)
                    buildings++;
                buildings = (int) (RandUtil.random() * buildings) + 1;
                IntPoint coord = report.getCoord();
                int id = report.getID();
                int destroyed = 0;
                StarSystem ss = manager.getUniverseMap().getStarSystemAt(coord);
                for (int i = 0; i < ss.getAllBuildings().size; i++)
                    if (ss.getAllBuildings().get(i).getRunningNumber() == report.getID()) {
                        ss.getAllBuildings().removeIndex(i--);
                        buildings--;
                        destroyed++;
                        if (buildings == 0)
                            break;
                    }
                if (destroyed > 0) {
                    intelReports.removeReport(oldReportNumber);
                    report = new MilitaryIntelObject(race.getRaceId(), enemyRace.getRaceId(),
                            manager.getCurrentRound(), false, coord, id, destroyed, true, false, false);
                    report.createText(manager, 0, responsibleRace);
                    intelReports.addReport(report);
                    enemyRace.getEmpire().getIntelligence().getIntelReports()
                            .addReport(new MilitaryIntelObject(report));
                    return true;
                }
            }
            intelReports.removeReport(oldReportNumber);
        }
        return false;
    }

    /**
     * The function executes a diplomatic sabotage action
     * @param race
     * @param enemyRace
     * @param responsibleRace
     * @param reportNumber
     * @return
     */
    public boolean executeDiplomacySabogate(Major race, Major enemyRace, String responsibleRace, int reportNumber) {
        //The corresponding action is executed. This will generate an intel report.
        //To make it faster, text generation won't happen here. As soon as the report is there the action will be generated from it.
        //After this the old report is removed and a new one will replace it
        int oldReportNumber = reportNumber;
        int newReportNumber = oldReportNumber + 1;
        IntelReports intelReports = race.getEmpire().getIntelligence().getIntelReports();
        if (reportNumber == -1) {
            oldReportNumber = intelReports.getNumberOfReports();
            executeDiplomacySpy(race, enemyRace, responsibleRace, false);
            newReportNumber = intelReports.getNumberOfReports();
        }
        //if the espionage action was successful, then this can be turned into a sabotage action
        if (newReportNumber > oldReportNumber) {
            DiplomacyIntelObject report = (DiplomacyIntelObject) intelReports.getReport(oldReportNumber);
            //in a diplomatic sabotage we can sabotage either the relationship between two majors or between a major and a minor
            //another option is to improve our relations with the minor or the majors
            if (!report.getMajorRaceID().isEmpty()) {
                String majorID = report.getMajorRaceID();
                Major major = Major.toMajor(manager.getRaceController().getRace(majorID));
                //two options here, either make the relationship between enemy and them worse or get better relations with our enemy
                if ((int) (RandUtil.random() * 5) == 0) {
                    int relationAdd = (int) (RandUtil.random() * 20) + 1;
                    enemyRace.setRelation(race.getRaceId(), relationAdd);
                    intelReports.removeReport(oldReportNumber);
                    report = new DiplomacyIntelObject(race.getRaceId(), enemyRace.getRaceId(),
                            manager.getCurrentRound(), false, enemyRace.getRaceId(), DiplomaticAgreement.NONE, 0, 0);
                    report.createText(manager, 0, responsibleRace);
                    intelReports.addReport(report);
                    enemyRace.getEmpire().getIntelligence().getIntelReports()
                            .addReport(new DiplomacyIntelObject(report));
                    return true;
                } else {
                    int relationSub = -((int) (RandUtil.random()) * 20 + 1);
                    //if it's between AIs then only a fourth is applied, because AIs shouldn't always hit eachother hard
                    if (!race.aHumanPlays() && !enemyRace.aHumanPlays() && !major.aHumanPlays())
                        relationSub /= 4;
                    enemyRace.setRelation(major.getRaceId(), relationSub);
                    major.setRelation(enemyRace.getRaceId(), relationSub);
                    intelReports.removeReport(oldReportNumber);
                    report = new DiplomacyIntelObject(race.getRaceId(), enemyRace.getRaceId(),
                            manager.getCurrentRound(), false, majorID, DiplomaticAgreement.NONE, 0, 0);
                    report.createText(manager, 1, responsibleRace);
                    enemyRace.getEmpire().getIntelligence().getIntelReports()
                            .addReport(new DiplomacyIntelObject(report));
                    return true;
                }
            } else { //between major/minor
                Minor minor = Minor.toMinor(manager.getUniverseMap().getStarSystemAt(report.getMinorRaceCoord())
                        .isHomeOf());
                if (minor != null) {
                    if ((int) (RandUtil.random() * 2) == 0 && minor.isRaceContacted(race.getRaceId())
                            && minor.getRelation(race.getRaceId()) < 90) {
                        //no report goes to the enemy from here because they are not involved - just our relation gets better with the minor
                        int relationAdd = (int) (RandUtil.random() * 20) + 1;
                        minor.setRelation(race.getRaceId(), relationAdd);
                        intelReports.removeReport(oldReportNumber);
                        IntPoint coord = minor.getCoordinates();
                        if (!coord.equals(new IntPoint()))
                            report = new DiplomacyIntelObject(race.getRaceId(), enemyRace.getRaceId(),
                                    manager.getCurrentRound(), false, coord);
                        if (report != null) {
                            report.createText(manager, 2, responsibleRace);
                            intelReports.addReport(report);
                        }
                    } else {
                        int relationSub = -((int) (RandUtil.random() * 20) + 1);
                        minor.setRelation(enemyRace.getRaceId(), relationSub);
                        intelReports.removeReport(oldReportNumber);
                        IntPoint coord = minor.getCoordinates();
                        if (!coord.equals(new IntPoint()))
                            report = new DiplomacyIntelObject(race.getRaceId(), enemyRace.getRaceId(),
                                    manager.getCurrentRound(), false, coord);
                        if (report != null) {
                            report.createText(manager, 3, responsibleRace);
                            intelReports.addReport(report);
                            enemyRace.getEmpire().getIntelligence().getIntelReports()
                                    .addReport(new DiplomacyIntelObject(report));
                            return true;
                        }
                    }
                }
            }
            intelReports.removeReport(oldReportNumber);
        }
        return false;
    }

    /**
     * The function creates a report that says that an intel attempt was made on a race
     * @param responsibleRace
     * @param enemyRace
     * @param type - type of the action - 0 == economy, 1 == science, 2 == military, 3 == dipolomacy
     */
    private void createMsg(Major responsibleRace, Major enemyRace, int type) {
        FileHandle handle = Gdx.files.internal("data" + GameConstants.getLocalePrefix() + "/races/majorintel.txt");
        String content = handle.readString(GameConstants.getCharset());
        String[] input = content.split("\n");
        if (input.length != 0) {
            int startPos = 0;
            while (startPos < input.length) {
                String line = input[startPos++].trim();
                if (line.startsWith(enemyRace.getRaceId())) {
                    String[] lineContent = line.split(":");
                    if (lineContent.length == 2) {
                        String text = lineContent[1].trim();
                        String s = responsibleRace.getEmpireNameWithAssignedArticle();
                        text = text.replace("$race$", s);
                        IntelObject report = null;
                        switch (type) {
                            case 0:
                                report = new EconIntelObject(responsibleRace.getRaceId(), enemyRace.getRaceId(),
                                        manager.getCurrentRound(), true, 0);
                                break;
                            case 1:
                                report = new ScienceIntelObject(responsibleRace.getRaceId(), enemyRace.getRaceId(),
                                        manager.getCurrentRound(), true, 0);
                                break;
                            case 2:
                                report = new MilitaryIntelObject(responsibleRace.getRaceId(), enemyRace.getRaceId(),
                                        manager.getCurrentRound(), true, new IntPoint(), 0, 0, false, false, false);
                                break;
                            case 3:
                                report = new DiplomacyIntelObject(responsibleRace.getRaceId(), enemyRace.getRaceId(),
                                        manager.getCurrentRound(), true, new IntPoint());
                                break;
                        }
                        if (report != null) {
                            report.setEnemyDesc(text);
                            enemyRace.getEmpire().getIntelligence().getIntelReports().addReport(report);
                        }
                    } else
                        Gdx.app.error("IntelCalc", "Error reading majorintel.txt");
                }
            }
        }
    }

    /**
     * The function executes an intel attempt
     * @param race
     * @param ourSP
     * @return true if it was successful, false otherwise
     */
    private boolean executeAttempt(Major race, int ourSP) {
        Major responsibleRace = race;
        IntelObject attemptObj = race.getEmpire().getIntelligence().getIntelReports().getAttemptObject();
        if (attemptObj != null) {
            Major enemy = Major.toMajor(manager.getRaceController().getRace(attemptObj.getEnemy()));
            if (enemy == null)
                return false;
            deleteConsumedPoints(race, enemy, false, attemptObj.getType(), true);
            //in case of attempts the points will be reduced a bit by saying here that this is a spy action
            MajorBox responsibleBox = new MajorBox(responsibleRace);
            if (isSuccess(enemy, ourSP, true, responsibleBox, attemptObj.getType()) != 0) {
                //the morale gets worse
                responsibleRace = responsibleBox.get();
                if (responsibleRace != null)
                    enemy.setRelation(responsibleRace.getRaceId(), -(int) (RandUtil.random() * 10));
                //if the attempt was successful, then the report has to be removed
                for (int i = 0; i < race.getEmpire().getIntelligence().getIntelReports().getNumberOfReports(); i++) {
                    IntelObject intelObj = race.getEmpire().getIntelligence().getIntelReports().getReport(i);
                    if (intelObj.isSpy())
                        if (intelObj.getRound() == attemptObj.getRound())
                            if (intelObj.getEnemy().equals(attemptObj.getEnemy()))
                                if (intelObj.getType() == attemptObj.getType())
                                    if (intelObj.getOwnerDesc().equals(attemptObj.getOwnerDesc())) {
                                        boolean returnValue = false;
                                        String param = responsibleRace == null ? "" : responsibleRace.getRaceId();
                                        switch (attemptObj.getType()) {
                                            case 0:
                                                returnValue = executeEconomySabotage(race, enemy, param, i);
                                                break;
                                            case 1:
                                                returnValue = executeScienceSabotage(race, enemy, param, i);
                                                break;
                                            case 2:
                                                returnValue = executeMilitarySabotage(race, enemy, param, i);
                                                break;
                                            case 3:
                                                returnValue = executeDiplomacySabogate(race, enemy, param, i);
                                                break;
                                        }
                                        intelObj = null;
                                        race.getEmpire().getIntelligence().getIntelReports().removeAttemptObject();
                                        return returnValue;
                                    }
                }
            }
        }
        race.getEmpire().getIntelligence().getIntelReports().removeAttemptObject();
        return false;
    }

    /**
     * The function returns the current inner security points of an empire
     * @param enemyRace
     * @return
     */
    private int getCompleteInnerSecPoints(Major enemyRace) {
        Intelligence intel = enemyRace.getEmpire().getIntelligence();

        //the storage of inner SP + produced SP * percentage + bonus on inner sec + percentage
        long enemyInnerSec = (int) intel.getSecurityPoints() * intel.getAssignment().getInnerSecurityPercentage() / 100;
        // + bonus on the inner security (it can be also negative)
        enemyInnerSec += enemyInnerSec * intel.getInnerSecurityBonus() / 100;
        // + percentage
        enemyInnerSec += intel.getAssignment().getInnerSecurityPercentage();
        // + storage (contains the bonus)
        enemyInnerSec += intel.getInnerSecurityStorage();

        return Math.max(0, (int)enemyInnerSec);
    }
}
