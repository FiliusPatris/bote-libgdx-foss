/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.intel;

import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.utils.IntPoint;

public class EconIntelObject extends IntelObject {
    private IntPoint coord;	///< cordinates of the economic hit
    private int number;		///< number of espionaged/sabotaged buildings
    private int id;			///< id of the buildings that should be sabotaged
    private int credits;	///< number of credits

    public EconIntelObject() {
        this("", "", 0, true, new IntPoint(), 0, 0);
    }

    public EconIntelObject(String ownerID, String enemyID, int round, boolean isSpy, IntPoint coord, int id, int number) {
        super(ownerID, enemyID, round, isSpy, 0);
        this.coord = coord;
        this.number = number;
        this.id = id;
        credits = 0;
    }

    public EconIntelObject(String ownerID, String enemyID, int round, boolean isSpy, int credits) {
        super(ownerID, enemyID, round, isSpy, 0);
        this.credits = credits;
        coord = new IntPoint();
        id = 0;
        number = 0;
    }

    public EconIntelObject(EconIntelObject other) {
        super(other);
        this.coord = new IntPoint(other.coord);
        this.id = other.id;
        this.number = other.number;
        this.credits = other.credits;
    }

    public IntPoint getCoord() {
        return coord;
    }

    public int getID() {
        return id;
    }

    public int getNumber() {
        return number;
    }

    public int getCredits() {
        return credits;
    }

    private String generateText(ResourceManager manager, String text) {
        String s;
        Major enemyRace = Major.toMajor(manager.getRaceController().getRace(enemy));
        if (enemyRace != null) {
            s = enemyRace.getEmpireNameWithArticle();
            text = text.replace("$race$", s);
        }
        if (!coord.equals(new IntPoint())) {
            s = manager.getUniverseMap().getStarSystemAt(coord).coordsName(true);
            text = text.replace("$system$", s);
        }
        if (id != 0) {
            s = manager.getBuildingInfo(id).getBuildingName();
            text = text.replace("$building$", s);
        }
        if (number != 0) {
            s = "" + number;
            text = text.replace("$number$", s);
        }
        s = "" + credits;
        text = text.replace("$credits$", s);
        return text;
    }

    @Override
    public void createText(ResourceManager manager, int n, String param) {
        String actionText = getTextFromFile(n, true);
        ownerDesc = generateText(manager, actionText);

        if (isSabotage()) {
            actionText = getTextFromFile(n, false);
            enemyDesc = generateText(manager, actionText);
            if (!param.isEmpty()) {
                Major paramRace = Major.toMajor(manager.getRaceController().getRace(param));
                if (paramRace != null) {
                    String s = paramRace.getEmpireNameWithArticle();
                    actionText = StringDB.getString("KNOW_RESPONSIBLE_SABOTAGERACE", false, s);
                }
            } else
                actionText = StringDB.getString("DO_NOT_KNOW_RESPONSIBLE_RACE");
            enemyDesc += " " + actionText;
        }
    }
}
