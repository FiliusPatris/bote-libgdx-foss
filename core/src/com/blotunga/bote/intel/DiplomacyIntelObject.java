/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.intel;

import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.constants.DiplomaticAgreement;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.utils.IntPoint;

public class DiplomacyIntelObject extends IntelObject {
    private IntPoint minorRaceCoord;		///< home of the affected minor
    private String majorID;					///< the major with whom the victim has an agreement
    private DiplomaticAgreement agreement;	///< the agreement type that they have
    private int duration;					///< the duration of the treaty
    private int relationship;				///< the relationship between the target and the other minor or major

    public DiplomacyIntelObject() {
        this("", "", 0, false, "", DiplomaticAgreement.NONE, 0, 0);
    }

    public DiplomacyIntelObject(String ownerId, String enemyID, int round, boolean isSpy, IntPoint minorRaceCoord) {
        this(ownerId, enemyID, round, isSpy, minorRaceCoord, DiplomaticAgreement.NONE, 0);
    }

    public DiplomacyIntelObject(String ownerID, String enemyID, int round, boolean isSpy, IntPoint minorRaceCoord,
            DiplomaticAgreement agreement, int relationship) {
        super(ownerID, enemyID, round, isSpy, 3);
        this.minorRaceCoord = minorRaceCoord;
        this.agreement = agreement;
        this.relationship = relationship;
        this.duration = 0;
        this.majorID = "";
    }

    public DiplomacyIntelObject(String ownerID, String enemyID, int round, boolean isSpy, String majorRaceID,
            DiplomaticAgreement agreement, int duration, int relationship) {
        super(ownerID, enemyID, round, isSpy, 3);
        this.majorID = majorRaceID;
        this.agreement = agreement;
        this.duration = duration;
        this.relationship = relationship;
        this.minorRaceCoord = new IntPoint();
    }

    public DiplomacyIntelObject(DiplomacyIntelObject other) {
        super(other);
        this.minorRaceCoord = new IntPoint(other.minorRaceCoord);
        this.majorID = other.majorID;
        this.agreement = other.agreement;
        this.duration = other.duration;
        this.relationship = other.relationship;
    }

    public IntPoint getMinorRaceCoord() {
        return minorRaceCoord;
    }

    public String getMajorRaceID() {
        return majorID;
    }

    public DiplomaticAgreement getAgreement() {
        return agreement;
    }

    public int getDuration() {
        return duration;
    }

    public int getRelationship() {
        return relationship;
    }

    @Override
    public void createText(ResourceManager manager, int n, String param) {
        String actionText = getTextFromFile(n, true);
        String s = "";
        if (n != 3) {
            Major enemyRace = Major.toMajor(manager.getRaceController().getRace(enemy));
            if (enemyRace != null) {
                if (isSpy)
                    s = enemyRace.getEmpireNameWithArticle();
                else
                    s = enemyRace.getEmpireNameWithAssignedArticle();
                actionText = actionText.replace("$race$", s);
            }

            if (!minorRaceCoord.equals(new IntPoint())) {
                s = manager.getRaceController()
                        .getMinorRace(manager.getUniverseMap().getStarSystemAt(minorRaceCoord).getName()).getName();
                actionText = actionText.replace("$minor$", s);
            }
            switch (agreement) {
                case DEFENCEPACT:
                    s = StringDB.getString("DEFENCE_PACT_WITH_ARTICLE");
                    break;
                case WAR:
                    s = StringDB.getString("WAR_WITH_ARTICLE");
                    break;
                case NONE:
                    s = StringDB.getString("NO_AGREEMENT").toLowerCase();
                    break;
                case NAP:
                    s = StringDB.getString("NON_AGGRESSION_WITH_ARTICLE");
                    break;
                case TRADE:
                    s = StringDB.getString("TRADE_AGREEMENT_WITH_ARTICLE");
                    break;
                case FRIENDSHIP:
                    s = StringDB.getString("FRIENDSHIP_WITH_ARTICLE");
                    break;
                case COOPERATION:
                    s = StringDB.getString("COOPERATION_WITH_ARTICLE");
                    break;
                case ALLIANCE:
                    s = StringDB.getString("ALLIANCE_WITH_ARTICLE");
                    break;
                case MEMBERSHIP:
                    s = StringDB.getString("MEMBERSHIP_WITH_ARTICLE");
                    break;
                default:
                    s = "";
                    break;
            }

            actionText = actionText.replace("$agreement$", s);
            if (!majorID.isEmpty()) {
                Major major = Major.toMajor(manager.getRaceController().getRace(majorID));
                if (major != null) {
                    s = major.getEmpireNameWithAssignedArticle();
                    actionText = actionText.replace("$major$", s);
                }

                if (agreement == DiplomaticAgreement.NONE || agreement == DiplomaticAgreement.WAR)
                    actionText = actionText.replace("($duration$) ", "");
                else {
                    if (duration == 0)
                        actionText = actionText.replace("$duration$", StringDB.getString("UNLIMITED"));
                    else {
                        s = String.format("%d %s", duration, StringDB.getString("ROUNDS"));
                        actionText = actionText.replace("$duration$", s);
                    }
                }
            }
        } else {//relationshiptext
            Major enemyRace = Major.toMajor(manager.getRaceController().getRace(enemy));
            if (enemyRace != null) {
                s = enemyRace.getEmpireNameWithAssignedArticle();
                actionText = actionText.replace("$race$", s);
            }
            if (!majorID.isEmpty()) {
                Major major = Major.toMajor(manager.getRaceController().getRace(majorID));
                if (major != null)
                    s = major.getEmpireNameWithAssignedArticle();
            } else if (!minorRaceCoord.equals(new IntPoint()))
                s = StringDB.getString("FEMALE_ARTICLE")
                        + " "
                        + manager.getRaceController()
                                .getMinorRace(manager.getUniverseMap().getStarSystemAt(minorRaceCoord).getName())
                                .getName();
            actionText = actionText.replace("$major$", s);

            if (relationship < 5)
                s = StringDB.getString("HATEFUL");
            else if (relationship < 15)
                s = StringDB.getString("FURIOUS");
            else if (relationship < 25)
                s = StringDB.getString("HOSTILE");
            else if (relationship < 35)
                s = StringDB.getString("ANGRY");
            else if (relationship < 45)
                s = StringDB.getString("NOT_COOPERATIVE");
            else if (relationship < 55)
                s = StringDB.getString("NEUTRAL");
            else if (relationship < 65)
                s = StringDB.getString("COOPERATIVE");
            else if (relationship < 75)
                s = StringDB.getString("FRIENDLY");
            else if (relationship < 85)
                s = StringDB.getString("OPTIMISTIC");
            else if (relationship < 95)
                s = StringDB.getString("ENTHUSED");
            else
                s = StringDB.getString("DEVOTED");
            actionText = actionText.replace("$relation$", s);

            if (!minorRaceCoord.equals(new IntPoint())) {
                s = manager.getRaceController()
                        .getMinorRace(manager.getUniverseMap().getStarSystemAt(minorRaceCoord).getName()).getName();
                actionText = actionText.replace("$minor$", s);
            }
        }
        ownerDesc = actionText;
        //generate message for victim
        if (isSabotage()) {
            actionText = getTextFromFile(n, false);
            Major ownerRace = Major.toMajor(manager.getRaceController().getRace(owner));
            if (owner != null) {
                s = ownerRace.getEmpireNameWithAssignedArticle();
                actionText = actionText.replace("$race$", s);
            }
            if (!minorRaceCoord.equals(new IntPoint())) {
                s = manager.getRaceController()
                        .getMinorRace(manager.getUniverseMap().getStarSystemAt(minorRaceCoord).getName()).getName();
                actionText = actionText.replace("$minor$", s);
            }
            if (!majorID.isEmpty()) {
                Major major = Major.toMajor(manager.getRaceController().getRace(majorID));
                if (major != null) {
                    s = major.getEmpireNameWithArticle();
                    actionText = actionText.replace("$major$", s);
                }
            }
            enemyDesc = actionText;
            if (!param.isEmpty()) {
                Major paramRace = Major.toMajor(manager.getRaceController().getRace(param));
                if (paramRace != null) {
                    s = paramRace.getEmpireNameWithArticle();
                    actionText = StringDB.getString("KNOW_RESPONSIBLE_SABOTAGERACE", false, s);
                }
            } else
                actionText = StringDB.getString("DO_NOT_KNOW_RESPONSIBLE_RACE");
            enemyDesc += " " + actionText;
        }
    }
}
