/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.intel;

import java.util.Arrays;

import com.badlogic.gdx.utils.ObjectIntMap;

public class Intelligence {
    @SuppressWarnings("unused")
    private String raceID;          ///< race which owns this Intelligence object
    private String responsibleRace; ///< race which can be blamed for our actions
    private int securityPoints;     ///< global number of produced intelligence points
    private int innerStorage;       ///< number of SP for internal security
    private ObjectIntMap<String> spStorage[]; ///< stores the cumulated espionage and sabotage points
    private int innerSecurityBonus; ///< bonus on inner security
    private int economyBonus[];     ///< economy bonus on espionage and sabotage
    private int scienceBonus[];     ///< science bouns on espionage and sabotage
    private int militaryBonus[];    ///< military bonus on espionage and sabotage
    private ObjectIntMap<String> agressiveness[]; ///< the agressiveness of espionage actions
    private IntelAssignment assignment; ///< the settings for the different intelligence fields
    private IntelReports reports;   ///< all intel reports
    transient private IntelInfo intelInfo;    ///< info about other races - doesn't needs to be serialized as it's calculated on demand

    @SuppressWarnings("unchecked")
    public Intelligence() {
        spStorage = new ObjectIntMap[2];
        agressiveness = new ObjectIntMap[2];
        for (int i = 0; i < 2; i++) {
            spStorage[i] = new ObjectIntMap<String>();
            agressiveness[i] = new ObjectIntMap<String>();
        }
        economyBonus = new int[2];
        scienceBonus = new int[2];
        militaryBonus = new int[2];
        assignment = new IntelAssignment();
        reports = new IntelReports();
        intelInfo = new IntelInfo();
        reset();
    }

    public void reset() {
        for (int i = 0; i < 2; i++) {
            spStorage[i].clear();
            agressiveness[i].clear();
        }
        raceID = "";
        responsibleRace = "";
        securityPoints = 0;
        innerStorage = 0;
        reports.removeAllReports();
        intelInfo.reset();
        clearBonuses();
    }

    public void clearBonuses() {
        innerSecurityBonus = 0;
        Arrays.fill(economyBonus, 0);
        Arrays.fill(scienceBonus, 0);
        Arrays.fill(militaryBonus, 0);
    }

    public String getResponsibleRace() {
        return responsibleRace;
    }

    public int getSecurityPoints() {
        return securityPoints;
    }

    public int getInnerSecurityStorage() {
        return innerStorage;
    }

    /**
     * Returns the accumulated points for espionage or sabotage
     * @param type - 0 = espionage, 1 = sabotage
     * @param race
     * @return
     */
    public int getSPStorage(int type, String race) {
        return spStorage[type].get(race, 0);
    }

    /**
     * The function returns a bonus on a security type
     * @param bonus - economy == 0, science == 1, military == 2, diplomacy == 3, inner security == 4
     * @param type - espionage == 0, sabotage == 1
     * @return
     */
    public int getBonus(int bonus, int type) {
        switch (bonus) {
            case 0:
                return getEconomyBonus(type);
            case 1:
                return getScienceBonus(type);
            case 2:
                return getMilitaryBonus(type);
            case 3:
                return 0;
            case 4:
                return getInnerSecurityBonus();
            default:
                return 0;
        }
    }

    public int getInnerSecurityBonus() {
        return (innerSecurityBonus < -100) ? -100 : innerSecurityBonus;
    }

    public int getEconomyBonus(int type) {
        return (economyBonus[type] < -100) ? -100 : economyBonus[type];
    }

    public int getScienceBonus(int type) {
        return (scienceBonus[type] < -100) ? -100 : scienceBonus[type];
    }

    public int getMilitaryBonus(int type) {
        return (militaryBonus[type] < -100) ? -100 : militaryBonus[type];
    }

    /**
     * Returns the agressivity of intel activities
     * @param type - espionage == 0, sabotage == 1
     * @param race
     * @return
     */
    public int getAggressiveness(int type, String race) {
        return agressiveness[type].get(race, 0);
    }

    public IntelReports getIntelReports() {
        return reports;
    }

    public IntelInfo getIntelInfo() {
        return intelInfo;
    }

    public IntelAssignment getAssignment() {
        return assignment;
    }

    public IntelAssignment setAssignment() {
        return assignment;
    }

    public void setRaceID(String raceID) {
        this.raceID = raceID;
        setResponsibleRace(raceID);
    }

    /**
     * Sets the aggressivity of intel actions
     * @param type - espionage == 0, sabotage == 1
     * @param race
     * @param value - cautious == 0, normal == 1, aggressive == 2
     */
    public void setAgressiveness(int type, String race, int value) {
        agressiveness[type].put(race, value);
    }

    public void setResponsibleRace(String responsibleRace) {
        this.responsibleRace = responsibleRace;
    }

    /**
     * Function adds a value to an existing value of inner security. Also we check if the new value is in range
     * @param add
     */
    public void addInnerSecurityPoints(int add) {
        if (innerStorage + add < 0)
            innerStorage = 0;
        else
            innerStorage += add;
    }

    /**
     * Function adds the points to the corresponding storage of a race. It also checks the range
     * @param type - espionage == 0, sabotage == 1
     * @param race
     * @param add
     */
    public void addSPStoragePoints(int type, String race, int add) {
        if (spStorage[type].get(race, 0) + add < 0)
            spStorage[type].put(race, 0);
        else
            spStorage[type].put(race, spStorage[type].get(race, 0) + add);
    }

    public void addInnerSecurityBonus(int add) {
        innerSecurityBonus += add;
    }

    public void addEconomyBonus(int add, int type) {
        economyBonus[type] += add;
    }

    public void addScienceBonus(int add, int type) {
        scienceBonus[type] += add;
    }

    public void addMilitaryBonus(int add, int type) {
        militaryBonus[type] += add;
    }

    public void clearSecurityPoints() {
        securityPoints = 0;
    }

    public void addSecurityPoints(int add) {
        if (securityPoints + add < 0)
            securityPoints = 0;
        else
            securityPoints += add;
    }
}
