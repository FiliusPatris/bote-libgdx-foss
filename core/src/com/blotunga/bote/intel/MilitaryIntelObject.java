/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.intel;

import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.constants.ShipType;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.utils.IntPoint;

public class MilitaryIntelObject extends IntelObject {
    private IntPoint coord;		///< coordinates of the espionage
    private int id;				///< id of the target ship/building/ship
    private int number;			///< number of troops/ships/buildings
    private boolean troop;		///< is this a troop sabotaging object?
    private boolean ship;		///< is this a ship sabotaging object?
    private boolean building;	///< is this a building sabotaging object?

    public MilitaryIntelObject() {
        this("", "", 0, true, new IntPoint(), 0, 0, false, false, false);
    }

    public MilitaryIntelObject(String ownerID, String enemyID, int round, boolean isSpy, IntPoint coord, int id,
            int number, boolean building, boolean ship, boolean troop) {
        super(ownerID, enemyID, round, isSpy, 2);
        this.coord = coord;
        this.id = id;
        this.number = number;
        this.troop = troop;
        this.building = building;
        this.ship = ship;
    }

    public MilitaryIntelObject(MilitaryIntelObject other) {
        super(other);
        this.building = other.building;
        this.ship = other.ship;
        this.troop = other.troop;
        this.coord = new IntPoint(other.coord);
        this.id = other.id;
        this.number = other.number;
    }

    public IntPoint getCoord() {
        return coord;
    }

    public int getID() {
        return id;
    }

    public int getNumber() {
        return number;
    }

    public boolean isBuilding() {
        return building;
    }

    public boolean isShip() {
        return ship;
    }

    public boolean isTroop() {
        return troop;
    }

    private String generateText(ResourceManager manager, String text) {
        String s;
        Major enemyRace = Major.toMajor(manager.getRaceController().getRace(enemy));
        if (enemyRace != null) {
            s = enemyRace.getEmpireNameWithArticle();
            text = text.replace("$race$", s);
        }
        if (!coord.equals(new IntPoint())) {
            s = manager.getUniverseMap().getStarSystemAt(coord).coordsName(true);
            text = text.replace("$system$", s);
        }
        if (id != 0) {
            if (building && id < 10000) {
                s = manager.getBuildingInfo(id).getBuildingName();
                text = text.replace("$building$", s);
            } else if (ship && id < 20000) {
                if (manager.getShipInfos().get(id - 10000).getShipType() == ShipType.OUTPOST)
                    s = String.format("%s-%s %s", manager.getShipInfos().get(id - 10000).getShipClass(),
                            StringDB.getString("CLASS"), StringDB.getString("OUTPOST"));
                else if (manager.getShipInfos().get(id - 10000).getShipType() == ShipType.STARBASE)
                    s = String.format("%s-%s %s", manager.getShipInfos().get(id - 10000).getShipClass(),
                            StringDB.getString("CLASS"), StringDB.getString("STARBASE"));
                else {
                    if (number == 1)
                        s = String.format("%s-%s %s", manager.getShipInfos().get(id - 10000).getShipClass(),
                                StringDB.getString("CLASS"), StringDB.getString("SHIP"));
                    else
                        s = StringDB.getString("SHIPS");
                }
                text = text.replace("$ship$", s);
            } else if (troop) {
                if (number == 1)
                    s = manager.getTroopInfos().get(id - 20000).getName();
                else
                    s = StringDB.getString("TROOPS");
                text = text.replace("$troop$", s);
            }
        }
        if (number != 0) {
            s = "" + number;
            text = text.replace("$number$", s);
        }
        return text;
    }

    @Override
    public void createText(ResourceManager manager, int n, String param) {
        String actionText = getTextFromFile(n, true);
        ownerDesc = generateText(manager, actionText);

        if (isSabotage()) {
            actionText = getTextFromFile(n, false);
            enemyDesc = generateText(manager, actionText);
            if (!param.isEmpty()) {
                Major paramRace = Major.toMajor(manager.getRaceController().getRace(param));
                if (paramRace != null) {
                    String s = paramRace.getEmpireNameWithArticle();
                    actionText = StringDB.getString("KNOW_RESPONSIBLE_SABOTAGERACE", false, s);
                }
            } else
                actionText = StringDB.getString("DO_NOT_KNOW_RESPONSIBLE_RACE");
            enemyDesc += " " + actionText;
        }
    }
}
