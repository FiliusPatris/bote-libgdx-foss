/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.intel;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.constants.GameConstants;

public abstract class IntelObject implements Comparable<IntelObject> {
    public enum IntelObjectSortType {
        BY_ENEMY,
        BY_ROUND,
        BY_TYPE,
        BY_ISSPY,
    }

    private static IntelObjectSortType sortType = IntelObjectSortType.BY_ENEMY;
    protected String owner;			///< the race which started the action
    protected String enemy;			///< the race which should be hit by the action
    protected int type;				///< type of the action (economy == 0, science == 1, military == 2, diplomacy == 3)
    protected String ownerDesc;		///< description for the owner
    protected String enemyDesc;		///< description for the target
    protected boolean isSpy;		///< true if espionage false if sabotage
    protected int round;			///< round when this object was created

    public static void setSortType(IntelObjectSortType st) {
        sortType = st;
    }

    @Override
    public int compareTo(IntelObject o) {
        switch (sortType) {
            case BY_ENEMY:
                return enemy.compareTo(o.enemy);
            case BY_ROUND:
                return GameConstants.compare(round, o.round);
            case BY_TYPE:
                return GameConstants.compare(type, o.type);
            case BY_ISSPY:
                return ((isSpy && o.isSpy) || (isSabotage() && o.isSabotage())) ? 0 : (isSpy ? 1 : 0) > (o.isSpy ? 1
                        : 0) ? -1 : 1;
        }
        return 0;
    }

    public IntelObject() {
        owner = "";
        enemy = "";
        type = 0;
        isSpy = true;
        round = 0;
        enemyDesc = "";
        ownerDesc = "";
    }

    public IntelObject(String ownerID, String enemyID, int round, boolean isSpy, int type) {
        this.owner = ownerID;
        this.enemy = enemyID;
        this.round = round;
        this.isSpy = isSpy;
        this.type = type;
        this.enemyDesc = "";
        this.ownerDesc = "";
    }

    public IntelObject(IntelObject other) {
        this.owner = other.owner;
        this.enemy = other.enemy;
        this.round = other.round;
        this.isSpy = other.isSpy;
        this.type = other.type;
        this.enemyDesc = other.enemyDesc;
        this.ownerDesc = other.ownerDesc;
    }

    /**
     * Function that returns the owner
     * @return
     */
    public String getOwner() {
        return owner;
    }

    /**
     * Function that returns the target
     * @return
     */
    public String getEnemy() {
        return enemy;
    }

    /**
     * Returns the type
     * @return 0 == economy, 1 == science, 2 == military, 3 == diplomacy
     */
    public int getType() {
        return type;
    }

    /**
     * Returns the description of the object meant to the owner
     * @return
     */
    public String getOwnerDesc() {
        return ownerDesc;
    }

    /**
     * Returns the description of the object meant to the target
     * @return
     */
    public String getEnemyDesc() {
        return enemyDesc;
    }

    /**
     * @return - true if espionage object, false if sabotage
     */
    public boolean isSpy() {
        return isSpy;
    }

    /**
     * @return - true if sabotage object, false if espionage
     */
    public boolean isSabotage() {
        return !isSpy;
    }

    /**
     * Function returns the round in which the object was created
     * @return
     */
    public int getRound() {
        return round;
    }

    protected String getTextFromFile(int n, boolean forAttacker) {
        String fileName;
        fileName = "data" + GameConstants.getLocalePrefix() + "/races/";
        if (isSpy)
            fileName += "majorintelspyoff.txt";
        else if (forAttacker)
            fileName += "majorintelsaboff.txt";
        else
            fileName += "majorintelsabdef.txt";
        String actionText = "";
        FileHandle handle = Gdx.files.internal(fileName);
        String content = handle.readString(GameConstants.getCharset());
        String[] input = content.split("\n");
        String searchValue = forAttacker ? owner : enemy;
        if (input.length != 0) {
            int startPos = 0;
            while (startPos < input.length) {
                String line = input[startPos++].trim();
                if (line.startsWith(searchValue)) {
                    String[] lineContent = line.split(":");
                    if (lineContent.length == 4) {
                        int tmpType = Integer.parseInt(lineContent[1].trim());
                        if (tmpType == type) {
                            int tmpN = Integer.parseInt(lineContent[2].trim());
                            if (tmpN == n) {
                                actionText = lineContent[3].trim();
                                break;
                            }
                        }
                    } else {
                        Gdx.app.error("DiplomacyIntelObject", "Error reading " + fileName);
                        return "";
                    }
                }
            }
        }
        return actionText.trim();
    }

    /**
     * Function generates a text which describes an action
     * @param manager
     * @param n - number of different text types - normally 0
     * @param param - here comes the race ID from which the target should think the action came from
     */
    public abstract void createText(ResourceManager manager, int n, String param);

    /**
     * Sets a description for the creator of the object
     * @param s
     */
    public void setOwnerDesc(String s) {
        ownerDesc = s;
    }

    /**
     * Sets a description for the enemy
     * @param s
     */
    public void setEnemyDesc(String s) {
        enemyDesc = s;
    }
}
