/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.intel;

import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.constants.ResearchComplexType;
import com.blotunga.bote.constants.ResearchType;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.utils.IntPoint;

public class ScienceIntelObject extends IntelObject {
    private int fp;									///< total research points
    private IntPoint coord;							///< coordinate of the system
    private int number;								///< number of buildings that we spied/sabotaged on
    private int id;									///< id of the buildings that have to be sabotaged
    private int techLevel;							///< current techlevel
    private int techType;							///< type of the tech which we found out about
    private ResearchComplexType specialTechComplex;	///< special complex on which we spied
    private int chosenSpecialTech;					///< selected bonus of the complex

    public ScienceIntelObject() {
        this("", "", 0, true, new IntPoint(), 0, 0);
    }

    public ScienceIntelObject(String ownerID, String enemyID, int round, boolean isSpy, IntPoint coord, int id,
            int number) {
        super(ownerID, enemyID, round, isSpy, 1);
        this.coord = coord;
        this.number = number;
        this.id = id;
        this.fp = 0;
        this.techLevel = -1;
        this.techType = -1;
        this.specialTechComplex = ResearchComplexType.NONE;
        this.chosenSpecialTech = -1;
    }

    public ScienceIntelObject(String ownerID, String enemyID, int round, boolean isSpy, int fp) {
        super(ownerID, enemyID, round, isSpy, 1);
        this.fp = fp;
        this.coord = new IntPoint();
        this.number = 0;
        this.id = 0;
        this.techLevel = -1;
        this.techType = -1;
        this.specialTechComplex = ResearchComplexType.NONE;
        this.chosenSpecialTech = -1;
    }

    public ScienceIntelObject(String ownerID, String enemyID, int round, boolean isSpy, int techLevel, int techType,
            ResearchComplexType specialTechComplex, int chosenSpecialTech) {
        super(ownerID, enemyID, round, isSpy, 1);
        this.techLevel = techLevel;
        this.techType = techType;
        this.specialTechComplex = specialTechComplex;
        this.chosenSpecialTech = chosenSpecialTech;
        this.fp = 0;
        this.coord = new IntPoint();
        this.id = 0;
        this.number = 0;
    }

    public ScienceIntelObject(ScienceIntelObject other) {
        super(other);
        this.number = other.number;
        this.fp = other.fp;
        this.coord = new IntPoint(other.coord);
        this.chosenSpecialTech = other.chosenSpecialTech;
        this.id = other.id;
        this.specialTechComplex = other.specialTechComplex;
        this.techLevel = other.techLevel;
        this.techType = other.techType;
    }

    public int getRP() {
        return fp;
    }

    public IntPoint getCoord() {
        return coord;
    }

    public int getID() {
        return id;
    }

    public int getNumber() {
        return number;
    }

    public int getTechLevel() {
        return techLevel;
    }

    public int getTechType() {
        return techType;
    }

    public ResearchComplexType getSpecialTechComplex() {
        return specialTechComplex;
    }

    public int getChosenSpecialTech() {
        return chosenSpecialTech;
    }

    private String generateText(ResourceManager manager, String text) {
        String s;
        Major enemyRace = Major.toMajor(manager.getRaceController().getRace(enemy));
        if (enemyRace != null) {
            s = enemyRace.getEmpireNameWithArticle();
            text = text.replace("$race$", s);
        }
        if (!coord.equals(new IntPoint())) {
            s = manager.getUniverseMap().getStarSystemAt(coord).coordsName(true);
            text = text.replace("$system$", s);
        }
        if (id != 0) {
            s = manager.getBuildingInfo(id).getBuildingName();
            text = text.replace("$building$", s);
        }
        if (number != 0) {
            s = "" + number;
            text = text.replace("$number$", s);
        }
        if (techLevel != -1) {
            ResearchType rt = ResearchType.fromType(techType);
            s = StringDB.getString(rt.getKey());
            text = text.replace("$techtype$", s);
            s = "" + techLevel;
            text = text.replace("$techlevel$", s);
        }
        if (specialTechComplex != ResearchComplexType.NONE) {
            s = enemyRace.getEmpire().getResearch().getResearchInfo().getResearchComplex(specialTechComplex)
                    .getComplexName();
            text = text.replace("$specialtech$", s);
            s = enemyRace.getEmpire().getResearch().getResearchInfo().getResearchComplex(specialTechComplex)
                    .getFieldName(chosenSpecialTech);
            text = text.replace("$choosenspecial$", s);
        }
        text = text.replace("$FP$", "" + fp);
        return text;
    }

    @Override
    public void createText(ResourceManager manager, int n, String param) {
        String actionText = getTextFromFile(n, true);
        ownerDesc = generateText(manager, actionText);

        if (isSabotage()) {
            actionText = getTextFromFile(n, false);
            enemyDesc = generateText(manager, actionText);
            if (!param.isEmpty()) {
                Major paramRace = Major.toMajor(manager.getRaceController().getRace(param));
                if (paramRace != null) {
                    String s = paramRace.getEmpireNameWithArticle();
                    actionText = StringDB.getString("KNOW_RESPONSIBLE_SABOTAGERACE", false, s);
                }
            } else
                actionText = StringDB.getString("DO_NOT_KNOW_RESPONSIBLE_RACE");
            enemyDesc += " " + actionText;
        }
    }
}
