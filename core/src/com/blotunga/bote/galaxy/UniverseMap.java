/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.galaxy;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.concurrent.Semaphore;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.IntSet;
import com.badlogic.gdx.utils.ObjectIntMap;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.ObjectMap.Entry;
import com.badlogic.gdx.utils.ObjectSet;
import com.badlogic.gdx.utils.ObjectSet.ObjectSetIterator;
import com.blotunga.bote.DefaultScreen;
import com.blotunga.bote.GamePreferences;
import com.blotunga.bote.ScreenManager;
import com.blotunga.bote.achievements.AchievementsList;
import com.blotunga.bote.ai.CombatAI;
import com.blotunga.bote.ai.ResearchAI;
import com.blotunga.bote.ai.ShipAI;
import com.blotunga.bote.ai.SystemAI;
import com.blotunga.bote.constants.*;
import com.blotunga.bote.events.EventAlienEntity;
import com.blotunga.bote.events.EventBlockade;
import com.blotunga.bote.events.EventBombardment;
import com.blotunga.bote.events.EventGameOver;
import com.blotunga.bote.events.EventRaceKilled;
import com.blotunga.bote.events.EventResearch;
import com.blotunga.bote.events.EventScreen;
import com.blotunga.bote.events.EventVictory;
import com.blotunga.bote.general.EmpireNews;
import com.blotunga.bote.general.NewRoundDataCalculator;
import com.blotunga.bote.general.OldRoundDataCalculator;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.general.EmpireNews.EmpireNewsType;
import com.blotunga.bote.intel.IntelCalc;
import com.blotunga.bote.intel.IntelObject;
import com.blotunga.bote.intel.Intelligence;
import com.blotunga.bote.races.Alien;
import com.blotunga.bote.races.DiplomacyController;
import com.blotunga.bote.races.DiplomacyInfo;
import com.blotunga.bote.races.Empire;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.races.Minor;
import com.blotunga.bote.races.MoraleObserver;
import com.blotunga.bote.races.Race;
import com.blotunga.bote.races.Research;
import com.blotunga.bote.races.ResearchComplex;
import com.blotunga.bote.races.ResearchInfo;
import com.blotunga.bote.races.VictoryObserver;
import com.blotunga.bote.races.Race.RaceSpecialAbilities;
import com.blotunga.bote.races.starmap.ExpansionSpeed;
import com.blotunga.bote.races.starmap.StarMap;
import com.blotunga.bote.races.starmap.StarMap.RangeTypes;
import com.blotunga.bote.ships.Combat;
import com.blotunga.bote.ships.ShipHistory;
import com.blotunga.bote.ships.ShipHistoryStruct;
import com.blotunga.bote.ships.ShipInfo;
import com.blotunga.bote.ships.ShipMap;
import com.blotunga.bote.ships.Ships;
import com.blotunga.bote.ships.Ships.RetreatMode;
import com.blotunga.bote.ships.Ships.StationWorkResult;
import com.blotunga.bote.starsystem.AttackSystem;
import com.blotunga.bote.starsystem.Building;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.starsystem.StarSystem.SystemSortType;
import com.blotunga.bote.starsystem.SystemProd;
import com.blotunga.bote.starsystem.SystemProd.ResearchBonus;
import com.blotunga.bote.troops.TroopInfo;
import com.blotunga.bote.ui.optionsview.SaveInfo;
import com.blotunga.bote.ui.screens.CombatSimulator;
import com.blotunga.bote.ui.screens.UniverseRenderer;
import com.blotunga.bote.utils.*;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.KryoException;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

public class UniverseMap implements Disposable {
    private Array<StarSystem> starSystems;
    private ShipMap shipMap;
    private final ScreenManager resourceManager; ///< this extends the resourceManager so it's safe to use
    private UniverseRenderer renderer;
    private IntPoint selectedCoord; ///< this holds the selected and owned starsystem which was last clicked
    private IntSet combatSectors; ///< holds the sectors in which a battle has occured
    private IntPoint currentCombatSector; ///< as the name implies
    private boolean combatCalc;	///< a battle is calculated
    private ArrayMap<String, CombatOrder> combatOrders;
    private CombatOrder combatOrder;
    private ObjectMap<String, ObjectMap<IntPoint, IntPoint>> shipRetreatSectors; //retreat sectors for all ships after all battles
    private VictoryObserver victoryObserver;
    private int nextAutoSaveIndex;
    private ArrayMap<IntPoint, WormholeLinker> wormholeList;
    private RememberedShip previouslyJumpedToShip = new RememberedShip("", 0);
    private transient Semaphore endturnSema;
    private transient Array<StarSystem> expansionSystems;
    private ObjectMap<String, ResearchComplexType> finishedSpecialResearch;
    private ViewTypes oldView = ViewTypes.GALAXY_VIEW;

    private class RememberedShip {
        public int index;
        public String name;

        public RememberedShip(String name, int index) {
            this.index = index;
            this.name = name;
        }
    }

    public UniverseMap(ScreenManager res) {
        this.resourceManager = res;
        renderer = null;
        starSystems = new Array<StarSystem>(false, resourceManager.getGridSizeX() * resourceManager.getGridSizeY());
        shipMap = new ShipMap(starSystems, false, res);
        selectedCoord = new IntPoint();
        combatSectors = new IntSet();
        currentCombatSector = new IntPoint();
        combatCalc = false;
        shipRetreatSectors = new ObjectMap<String, ObjectMap<IntPoint, IntPoint>>();
        combatOrders = new ArrayMap<String, CombatOrder>();
        combatOrder = CombatOrder.NONE;
        wormholeList = new ArrayMap<IntPoint, WormholeLinker>();
        nextAutoSaveIndex = -1;
        endturnSema = new Semaphore(0);
        expansionSystems = new Array<StarSystem>();
        finishedSpecialResearch = new ObjectMap<String, ResearchComplexType>();
    }

    public void saveUniverse(final String filePath) {
        Gdx.app.debug("UniverseMap", "Saving Universe");
        Thread th = new Thread(new Runnable() {
            @Override
            public void run() {
                getRenderer().saveScreenShot(filePath.replace(".sav", ".png"));
            }
        });
        th.start();

        try {
            Kryo kryo = resourceManager.getKryo();
            ((KryoV) kryo).setSaveVersion(resourceManager.getGamePreferences().saveGameVersion());
            Output output = new Output(new DeflaterOutputStream(new FileOutputStream(GameConstants.getSaveLocation() + filePath), new Deflater(Deflater.DEFAULT_COMPRESSION)));
            kryo.writeObject(output, resourceManager.getGamePreferences());
            output.writeString(resourceManager.getRaceController().getPlayerRaceString());
            resourceManager.saveResourceManager(kryo, output);
            kryo.writeObject(output, starSystems);
            resourceManager.getRaceController().saveRaceController(kryo, output);
            shipMap.writeShipMap(kryo, output);
            kryo.writeObject(output, victoryObserver);
            kryo.writeObject(output, wormholeList);
            output.close();
        } catch (IOException e) {
            System.out.println("Error writing savagame: ");
            e.printStackTrace();
        }
        try {
            th.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    private void loadUniverse(String filePath) {
        System.out.println("Loading universe");
        GamePreferences prefs = new GamePreferences();
        try {
            Kryo kryo = resourceManager.getKryo();
            Input input = new Input(new InflaterInputStream(new FileInputStream(GameConstants.getSaveLocation() + filePath)));
            prefs = kryo.readObject(input, GamePreferences.class);
            ((KryoV) kryo).setSaveVersion(prefs.saveGameVersion());
            resourceManager.setGamePreferences(prefs);
            String savedRace = input.readString();
            if (!resourceManager.getForceMajor().isEmpty())
                resourceManager.getRaceController().setPlayerRace(resourceManager.getForceMajor());
            resourceManager.loadResourceManager(kryo, input);
            starSystems = kryo.readObject(input, Array.class);
            resourceManager.getRaceController().loadRaceController(kryo, input);
            if(resourceManager.getRaceController().getPlayerRace() == null)
                resourceManager.getRaceController().setPlayerRace(savedRace);
            shipMap.setStarSystems(starSystems);
            shipMap.readShipMap(kryo, input);
            shipMap.setResourceManager(resourceManager);
            victoryObserver = kryo.readObject(input, VictoryObserver.class);
            if (prefs.saveGameVersion() >= 3) {
                wormholeList = kryo.readObject(input, ArrayMap.class);
            }
            input.close();
        } catch (IOException e) {
            System.out.println("Error reading savegame: ");
            e.printStackTrace();
        }

        expansionSystems.clear();
        for (int i = 0; i < starSystems.size; i++) {
            StarSystem ss = starSystems.get(i);
            ss.setResourceManager(resourceManager);
            if (prefs.saveGameVersion() < 3) {
                initWormholes(ss);
                linkWormholes(false);
            }

            for (Iterator<Entry<String, ShipMap>> iter = ss.ships.entries().iterator(); iter.hasNext();) {
                Entry<String, ShipMap> e = iter.next();
                e.value.setResourceManager(resourceManager);
            }

            if (ss.isExpansionSector(resourceManager.getRaceController().getPlayerRaceString()))
                expansionSystems.add(ss);
        }

        for(int i = 0; i < shipMap.getSize(); i++) {//remove stray ships in older versions
            Ships ship = shipMap.getAt(i);
            Race owner = ship.getOwner();
            if (owner == null) {
                shipMap.eraseAt(i--);
            }
        }

        StarSystem.SystemSortType oldSortType = StarSystem.getSortType();
        StarSystem.setSortType(SystemSortType.BY_SECTORVALUE);
        expansionSystems.sort();
        expansionSystems.reverse();
        StarSystem.setSortType(oldSortType);

        for (Iterator<Entry<String, Major>> iter = resourceManager.getRaceController().getMajors().entries().iterator(); iter
                .hasNext();) {
            Entry<String, Major> e = iter.next();
            e.value.getEmpire().generateSystemList(starSystems, resourceManager);
        }

//FIXME: remove this at some point, this fixes ship history for existing saves
        for(int j = 0; j < resourceManager.getRaceController().getMajors().size; j++) {
            ShipHistory hist = resourceManager.getRaceController().getMajors().getValueAt(j).getShipHistory();
            for(int k = 0; k < hist.getShipHistoryArray().size; k++) {
                ShipHistoryStruct sh = hist.getShipHistoryArray().get(k);
                int l = -1;
                for(int i = 0; i < shipMap.getSize(); i++) {
                    Ships ship = shipMap.getAt(i);
                    if(sh.shipName.equals(ship.getName())) {
                        l = k;
                        break;
                    }
                    if(ship.hasFleet())
                        for(int m = 0; m < ship.getFleetSize(); m++) {
                            Ships fs = ship.getFleet().getAt(m);
                            if(sh.shipName.equals(fs.getName())) {
                                l = k;
                                break;
                            }
                        }
                }
                if(l == -1 && sh.destroyRound == 0) {//not found in shipMap
                    sh.destroyRound = resourceManager.getCurrentRound();
                    sh.currentTask = StringDB.getString("DESTROYED");
                    sh.kindOfDestroy = StringDB.getString("UNKNOWN");
                }
            }
        }
        System.out.println("Loaded: " + starSystems.size);
    }

    public void setRetRenderer(UniverseRenderer renderer) {
        this.renderer = renderer;
    }

    public void prepareData() {
        MoraleObserver.initMoraleMatrix();
        resourceManager.initBadMapModifiers();

        if (!resourceManager.isGameLoaded()) {
            resourceManager.getShipNameGenerator().Init(resourceManager);
            if (resourceManager.getGamePreferences().seeAllofMap) {
                // all races should know if eachother
                ArrayMap<String, Race> races = resourceManager.getRaceController().getRaces();
                for (Iterator<Entry<String, Race>> iter = races.entries().iterator(); iter.hasNext();) {
                    Entry<String, Race> e1 = iter.next();
                    for (int i = 0; i < races.size; i++) {
                        String secondRaceId = races.getKeyAt(i);
                        Race secondRace = races.getValueAt(i);
                        if ((!e1.key.equals(secondRaceId)) && (e1.value.isMajor()) && (secondRace.isMajor())) {
                            e1.value.setIsRaceContacted(secondRaceId, true);
                        }
                    }
                }
            }

            generateSectors();
            applyShipsAtStartup();
            applyBuildingsAtStartup();
            applyTroopsAtStartup();
            victoryObserver = new VictoryObserver();
            victoryObserver.observe(resourceManager);

        } else {
            System.out.println("LOADING");
            loadUniverse(resourceManager.getGamePreferences().gameLoaded);
        }

        for (Iterator<Entry<String, Major>> iter = resourceManager.getRaceController().getMajors().entries().iterator(); iter
                .hasNext();) {
            Entry<String, Major> e = iter.next();
            e.value.setHumanPlayer(e.key.equals(resourceManager.getRaceController().getPlayerRaceString()));
        }
        String musicPath = "sounds/" + resourceManager.getRaceController().getPlayerRace().getPrefix() + "music.ogg";
        resourceManager.setMusic(musicPath);
        resourceManager.getMusic().setLooping(true);
        resourceManager.getMusic().setVolume(resourceManager.getGameSettings().musicVolume);
        resourceManager.getMusic().play();
        resourceManager.getClientWorker().setNextActiveView(ViewTypes.GALAXY_VIEW,
                resourceManager.getRaceController().getPlayerRace()); // reset to galaxy by default

        generateStarMap();
        resourceManager.getStatistics().calcStats(resourceManager);
        resourceManager.setEndRoundPressed(false);
    }

    public StarSystem getStarSystemAt(IntPoint p) {
        return getStarSystemAt(p.x, p.y);
    }

    public StarSystem getStarSystemAt(int x, int y) {
        return starSystems.get(x + y * resourceManager.getGridSizeX());
    }

    public StarSystem getCurrentStarSystem() {
        return getStarSystemAt(getSelectedCoordValue());
    }

    private void generateSectors() {
        int[][] shape = new int[resourceManager.getGridSizeX()][resourceManager.getGridSizeY()];
        if (!resourceManager.getGamePreferences().generateFromShape.getImage().isEmpty()) {
            shape = ShapeReader.getShapeFromFile("data/galaxy/"
                    + resourceManager.getGamePreferences().generateFromShape.getImage() + ".png", resourceManager);
        } else {
            for (int j = 0; j < resourceManager.getGridSizeY(); j++)
                for (int i = 0; i < resourceManager.getGridSizeX(); i++)
                    shape[i][j] = 1;
        }
        // first generate sectors with stars in them
        for (int j = 0; j < resourceManager.getGridSizeY(); j++)
            for (int i = 0; i < resourceManager.getGridSizeX(); i++) {
                StarSystem st = new StarSystem(StringDB.getString("SECTOR") + " " + i + "," + j, new IntPoint(i, j),
                        resourceManager);
                starSystems.add(st);
            }
        ArrayMap<String, Major> majors = resourceManager.getRaceController().getRandomMajors();

        // minimum distance between homesystems of major races
        int minDiff = (int) Math.sqrt((double) resourceManager.getGridSizeX() * resourceManager.getGridSizeY()) / 2 + 2
                - majors.size;
        // minimum 4 squares distance
        minDiff = Math.max(minDiff, 4);
        minDiff = Math.min(50, minDiff);
        ObjectMap<String, IntPoint> raceCoordinates = new ObjectMap<String, IntPoint>();
        int stopAt = Math.max((resourceManager.getGridSizeX() * resourceManager.getGridSizeY()) / 4, 20);
        for (int i = 0; i < majors.size;) {
            String currentRaceId = majors.getKeyAt(i);
            boolean restart = false;
            int count = 0;
            IntPoint coord;
            do {
                coord = new IntPoint((int) (RandUtil.random() * resourceManager.getGridSizeX()),
                        (int) (RandUtil.random() * resourceManager.getGridSizeY()));

                for (int j = 0; j < i; j++) {
                    IntPoint theirCoord = raceCoordinates.get(majors.getKeyAt(j));
                    if ((shape[coord.x][coord.y] == 0)
                            || (Math.abs(coord.x - theirCoord.x) < minDiff && Math.abs(coord.y - theirCoord.y) < minDiff)) {
                        coord = new IntPoint(-1, -1);
                        break;
                    }
                }
                if (++count > stopAt)
                    restart = true;
            } while (coord.equals(new IntPoint(-1, -1)) && restart == false);

            raceCoordinates.put(currentRaceId, coord);
            i++;
            if (restart) {
                raceCoordinates.clear();
                i = 0;
            }
        }

        ArrayMap<String, Minor> minors = resourceManager.getRaceController().getMinors();
        Array<String> minorRaceSystemNames = new Array<String>();
        for (Iterator<Entry<String, Minor>> iter = minors.entries().iterator(); iter.hasNext();) {
            Entry<String, Minor> e = iter.next();
            if (!e.value.isAlien())
                minorRaceSystemNames.add(e.value.getHomeSystemName());
        }

        StarNameGenerator gen = resourceManager.getStarNameGenerator();
        gen.InitMinorRacesNames(minorRaceSystemNames);

        //in case of a shape double the star density
        if (!resourceManager.getGamePreferences().generateFromShape.getImage().isEmpty())
            resourceManager.getGamePreferences().starDensity = Math.min(
                    resourceManager.getGamePreferences().starDensity * 2, 100);

        for (Iterator<Entry<String, Major>> iter = majors.entries().iterator(); iter.hasNext();) {
            Entry<String, Major> e = iter.next();
            Major race = e.value;
            IntPoint raceCoord = raceCoordinates.get(e.key);
            StarSystem system = getStarSystemAt(raceCoord.x, raceCoord.y);

            system.setHomeOf(e.key);
            system.setName(race.getHomeSystemName());
            race.setRaceCoordinates(raceCoord);
            system.setSunsystem(true);
            system.setFullKnown(e.key);
            system.setColonyOwner(e.key);
            system.createPlanets(e.key);
            system.changeOwner(e.key, SystemOwningStatus.OWNING_STATUS_COLONIZED_MEMBERSHIP_OR_HOME);
            system.addResourceStore(ResourceTypes.TITAN.getType(), 1000);
            system.addResourceStore(ResourceTypes.DERITIUM.getType(), 3);

            int nearbysystems = resourceManager.getGamePreferences().nearbySystems;
            nearbysystems = Math.max(Math.min(nearbysystems, 8), 0);
            IntPoint[] temp1 = {
                    new IntPoint(raceCoord.x - 1, raceCoord.y - 1),
                    new IntPoint(raceCoord.x, raceCoord.y - 1),
                    new IntPoint(raceCoord.x + 1, raceCoord.y - 1),
                    new IntPoint(raceCoord.x - 1, raceCoord.y),
                    new IntPoint(raceCoord.x + 1, raceCoord.y),
                    new IntPoint(raceCoord.x - 1, raceCoord.y + 1),
                    new IntPoint(raceCoord.x, raceCoord.y + 1),
                    new IntPoint(raceCoord.x + 1, raceCoord.y + 1)};
            Array<IntPoint> inner = new Array<IntPoint>();
            inner.addAll(temp1);
            IntPoint[] temp2 = {
                    new IntPoint(raceCoord.x - 2, raceCoord.y - 2),
                    new IntPoint(raceCoord.x - 1, raceCoord.y - 2),
                    new IntPoint(raceCoord.x, raceCoord.y - 2),
                    new IntPoint(raceCoord.x + 1, raceCoord.y - 2),
                    new IntPoint(raceCoord.x + 2, raceCoord.y - 2),
                    new IntPoint(raceCoord.x - 2, raceCoord.y - 1),
                    new IntPoint(raceCoord.x + 2, raceCoord.y - 1),
                    new IntPoint(raceCoord.x - 2, raceCoord.y),
                    new IntPoint(raceCoord.x + 2, raceCoord.y),
                    new IntPoint(raceCoord.x - 2, raceCoord.y + 1),
                    new IntPoint(raceCoord.x + 2, raceCoord.y + 1),
                    new IntPoint(raceCoord.x - 2, raceCoord.y + 2),
                    new IntPoint(raceCoord.x - 1, raceCoord.y + 2),
                    new IntPoint(raceCoord.x, raceCoord.y + 2),
                    new IntPoint(raceCoord.x + 1, raceCoord.y + 2),
                    new IntPoint(raceCoord.x + 2, raceCoord.y + 2)};
            Array<IntPoint> outer = new Array<IntPoint>();
            outer.addAll(temp2);

            while (nearbysystems > 0) {
                Array<IntPoint> candidates = inner;
                if (inner.size == 0)
                    candidates = outer;
                int index = (int) (RandUtil.random() * candidates.size);
                IntPoint p = candidates.get(index);
                candidates.removeIndex(index);
                if (!isOnMap(p))
                    continue;
                nearbysystems--;
                StarSystem ss = getStarSystemAt(p);
                if (ss.isSunSystem())
                    continue;
                ss.generateSector(100, resourceManager.getGamePreferences().minorDensity);
            }

            // scan around the home system
            for (int y = -1; y <= 1; y++)
                for (int x = -1; x <= 1; x++) {
                    IntPoint pt = new IntPoint(raceCoord.x + x, raceCoord.y + y);
                    if (pt.equals(raceCoord))
                        continue;
                    if (pt.inRect(0, 0, resourceManager.getGridSizeX(), resourceManager.getGridSizeY()))
                        getStarSystemAt(pt.x, pt.y).setScanned(e.key);
                }
        }

        Array<IntPoint> sectorsToGenerate = new Array<IntPoint>();
        for (int y = 0; y < resourceManager.getGridSizeY(); y++)
            for (int x = 0; x < resourceManager.getGridSizeX(); x++)
                if ((!getStarSystemAt(x, y).isSunSystem()) && (shape[x][y] == 1))
                    sectorsToGenerate.add(new IntPoint(x, y));
        while (sectorsToGenerate.size != 0) {
            int sector = (int) (RandUtil.random() * sectorsToGenerate.size);
            int x = sectorsToGenerate.get(sector).x;
            int y = sectorsToGenerate.get(sector).y;
            sectorsToGenerate.removeIndex(sector);

            //generate sector,
            //the probabilities depend from the systems next to this
            //if there is a major race nearby, no system will be generated as these already have sectors nearby
            int sunSystems = 0;
            int minorRaces = 0;
            int anomalies = 0;
            for (int j = -1; j <= 1; j++)
                for (int i = -1; i <= 1; i++) {
                    IntPoint pt = new IntPoint(x + i, y + j);
                    if (pt.inRect(0, 0, resourceManager.getGridSizeX(), resourceManager.getGridSizeY())) {
                        if (getStarSystemAt(pt.x, pt.y).isSunSystem()) {
                            if (getStarSystemAt(pt.x, pt.y).getMinorRace() != null)
                                minorRaces++;
                            sunSystems++;
                        } else if (getStarSystemAt(pt.x, pt.y).getAnomaly() != null) {
                            anomalies++;
                        }

                        for (Iterator<Entry<String, IntPoint>> iter = raceCoordinates.entries().iterator(); iter.hasNext();) {
                            Entry<String, IntPoint> e = iter.next();
                            if (e.value.x == x + i && e.value.y == y + j) {
                                sunSystems += 100;
                                anomalies += 100;
                            }
                        }
                    }
                }

            int sunSystemProb = resourceManager.getGamePreferences().starDensity - sunSystems * 15;
            int minorRaceProb = resourceManager.getGamePreferences().minorDensity - minorRaces * 15;
            if (minorRaceProb < 0)
                minorRaceProb = 0;
            if (sunSystemProb > 0)
                getStarSystemAt(x, y).generateSector(sunSystemProb, minorRaceProb);

            //generate possible anomaly
            if (!getStarSystemAt(x, y).isSunSystem()) {
                if ((int) (RandUtil.random() * 100) >= (100 - (resourceManager.getGamePreferences().anomalyDensity - anomalies * 10)))
                    getStarSystemAt(x, y).createAnomaly();
            }
        }

        Array<String> usedMinors = new Array<String>();
        for (int y = 0; y < resourceManager.getGridSizeY(); y++) {
            for (int x = 0; x < resourceManager.getGridSizeX(); x++) {
                StarSystem system = getStarSystemAt(x, y);
                //set up minors
                if (system.getMinorRace() != null) {
                    Minor minor = (Minor) system.isHomeOf();
                    minor.setRaceCoordinates(new IntPoint(x, y));
                    system.changeOwner(minor.getRaceId(), SystemOwningStatus.OWNING_STATUS_INDEPENDENT_MINOR);
                    usedMinors.add(minor.getRaceId());
                    if (minor.getSpaceFlightNation())
                        system.createDeritiumForSpaceflightMinor();
                }

                initWormholes(system);
            }
        }
        Array<String> delMinors = new Array<String>();
        for (Iterator<Entry<String, Minor>> iter = minors.entries().iterator(); iter.hasNext();) {
            Entry<String, Minor> e = iter.next();
            if (!usedMinors.contains(e.key, false))
                if (!e.value.isAlien())
                    delMinors.add(e.key);
        }
        for (String m : delMinors) {
            removeShips(m);
            resourceManager.getRaceController().removeRace(m);
        }

        linkWormholes(false);
        System.out.println("DONE");
    }

    private void initWormholes(StarSystem system) {
        //create list of wormholes
        if (system.getAnomaly() != null && system.getAnomaly().getType() == AnomalyType.WORMHOLE) {
            WormholeLinker linker = new WormholeLinker();
            wormholeList.put(system.getCoordinates(), linker);
        }
    }

    private void linkWormholes(boolean unstableAlso) {
        ArrayMap<IntPoint, WormholeLinker> tmpWormholeList = new ArrayMap<IntPoint, WormholeLinker>(wormholeList);
        for (Entry<IntPoint, WormholeLinker> wormholeEntry : wormholeList) {
            WormholeLinker wormhole = wormholeEntry.value;
            if (unstableAlso && ((int) (RandUtil.random() * 5) == 0)) { //reset wormholes, unstable wormholes have a 20% change of reconnecting at the end of a round
                if (wormhole.getLinktype() == WormholeLinkType.LINK_UNSTABLE) {
                    wormhole.setLinktype(WormholeLinkType.LINK_NONE);
                    if (wormholeList.containsKey(wormhole.getTarget())
                            && wormholeList.get(wormhole.getTarget()).getLinktype() != WormholeLinkType.LINK_STABLE)
                        wormholeList.get(wormhole.getTarget()).setLinktype(WormholeLinkType.LINK_NONE);
                }
            }
            while (wormhole.getLinktype() == WormholeLinkType.LINK_NONE) {
                int rand = (int) (RandUtil.random() * 5); //wormholes have a 20% chance of being stable
                if (rand == 0) { //20% chance of leading somewhere random and let the ships strand
                    setRandomUnstableWh(wormhole, wormholeEntry.key);
                    break;
                }

                if(tmpWormholeList.size == 0) {
                    setRandomUnstableWh(wormhole, wormholeEntry.key);
                    break;
                }

                int i = (int) (RandUtil.random() * tmpWormholeList.size);
                WormholeLinker other = tmpWormholeList.getValueAt(i);
                IntPoint otherCoord = tmpWormholeList.getKeyAt(i);
                while (other.getLinktype() != WormholeLinkType.LINK_NONE || otherCoord.equals(wormholeEntry.key)) {
                    tmpWormholeList.removeIndex(i);
                    if(tmpWormholeList.size == 0) {
                        setRandomUnstableWh(wormhole, wormholeEntry.key);
                        break;
                    }
                    i = (int) (RandUtil.random() * tmpWormholeList.size);
                    other = tmpWormholeList.getValueAt(i);
                    otherCoord = tmpWormholeList.getKeyAt(i);
                }

                if (other.getLinktype() == WormholeLinkType.LINK_NONE && !otherCoord.equals(wormholeEntry.key)) {
                    wormhole.setTarget(otherCoord);
                    other.setTarget(wormholeEntry.key);
                    if (rand == 1 && !unstableAlso) {
                        wormhole.setLinktype(WormholeLinkType.LINK_STABLE);
                        other.setLinktype(WormholeLinkType.LINK_STABLE);
                        Gdx.app.debug("UniverseMap", "Wormhole at: " + wormholeEntry.key  + " linked stably to " + wormhole.getTarget());
                    } else { //randomly link with another wormhole
                        wormhole.setLinktype(WormholeLinkType.LINK_UNSTABLE);
                        other.setLinktype(WormholeLinkType.LINK_UNSTABLE);
                        Gdx.app.debug("UniverseMap", "Wormhole at: " + wormholeEntry.key  + " linked unstably to other wormhole " + wormhole.getTarget());
                    }
                    break;
                }
            }
        }
    }

    private void setRandomUnstableWh(WormholeLinker wormhole, IntPoint coord) {
        wormhole.setLinktype(WormholeLinkType.LINK_UNSTABLE);
        wormhole.setTarget(new IntPoint((int) (RandUtil.random() * resourceManager.getGridSizeX()),
                (int) (RandUtil.random() * resourceManager.getGridSizeY())));
        Gdx.app.debug("UniverseMap", "Wormhole at: " + coord  + " linked unstably to " + wormhole.getTarget());
    }

    private void applyShipsAtStartup() {
        FileHandle handle = Gdx.files.internal("data/ships/startships.txt");
        String input = handle.readString("ISO-8859-1");
        String[] inputs = input.split("\n");
        for (String s : inputs) {
            if (s.startsWith("//") || s.trim().isEmpty())
                continue;
            String[] info = s.split(":");
            String owner = info[0].trim();
            String systemName = info[1].trim();
            String[] shipTypes = info[2].split(",");
            boolean foundSector = false;
            for (int y = 0; y < resourceManager.getGridSizeY(); y++) {
                for (int x = 0; x < resourceManager.getGridSizeX(); x++)
                    if (getStarSystemAt(x, y).getName().equals(systemName)) {
                        foundSector = true;
                        for (String st : shipTypes) {
                            for (int i = 0; i < resourceManager.getShipInfos().size; i++) {
                                ShipInfo inf = resourceManager.getShipInfos().get(i);
                                if (inf.getShipClass().equals(st.trim())) {
                                    buildShip(inf.getID(), new IntPoint(x, y), owner);
                                    break;
                                }
                            }
                        }
                    }
                if (foundSector)
                    break;
            }
        }
        float frequency = resourceManager.getGamePreferences().alienFrequency;
        if (frequency > 0.0) {
            //if there is an Ehlen the defender (Station) should be buit
            Minor ehlen = Minor.toMinor(resourceManager.getRaceController().getRace("EHLEN"));
            if (ehlen != null) {
                for (int i = 0; i < resourceManager.getShipInfos().size; i++) {
                    ShipInfo shipInfo = resourceManager.getShipInfos().get(i);
                    if (!shipInfo.getOnlyInSystem().equals(ehlen.getHomeSystemName()))
                        continue;

                    buildShip(shipInfo.getID(), ehlen.getCoordinates(), ehlen.getRaceId());
                    break;
                }
            }

            //put the battlestation in a random system on the galaxy map
            Array<StarSystem> possibleStarSystems = new Array<StarSystem>();
            for (int i = 0; i < starSystems.size; i++) {
                StarSystem ss = starSystems.get(i);
                if (!ss.isSunSystem())
                    continue;
                if (!ss.isFree())
                    continue;
                if (ss.getMinorRace() != null)
                    continue;
                if (ss.getAnomaly() != null)
                    continue;

                possibleStarSystems.add(ss);
            }
            StarSystem battleStationSystem = null;
            if (possibleStarSystems.size > 0)
                battleStationSystem = possibleStarSystems.get((int) (RandUtil.random() * possibleStarSystems.size));
            if (battleStationSystem != null) {
                for (int i = 0; i < resourceManager.getShipInfos().size; i++) {
                    ShipInfo shipInfo = resourceManager.getShipInfos().get(i);
                    if (!shipInfo.getOnlyInSystem().equals(Aliens.KAMPFSTATION.getId()))
                        continue;
                    buildShip(shipInfo.getID(), battleStationSystem.getCoordinates(), Aliens.KAMPFSTATION.getId());
                    break;
                }
            }
        }
    }

    private void applyBuildingsAtStartup() {
        FileHandle handle = Gdx.files.internal("data/buildings/startbuildings.txt");
        String input = handle.readString("ISO-8859-1");
        String[] inputs = input.split("\n");
        for (String s : inputs) {
            if (s.startsWith("//") || s.trim().isEmpty())
                continue;
            String[] info = s.split(":");
            String systemName = info[0].trim();
            boolean found = false;
            for (int y = 0; y < resourceManager.getGridSizeY(); y++) {
                for (int x = 0; x < resourceManager.getGridSizeX(); x++) {
                    if (getStarSystemAt(x, y).getName().equals(systemName)) {
                        String[] buildings = info[1].trim().split(",");
                        found = true;
                        for (String data : buildings)
                            buildBuilding(Integer.parseInt(data.trim()), new IntPoint(x, y));
                        break;
                    }
                }
                if (found)
                    break;
            }
        }

        for (Iterator<Entry<String, Major>> iter = resourceManager.getRaceController().getMajors().entries().iterator();
                iter.hasNext();) {
            Entry<String, Major> e = iter.next();
            e.value.createStarMap();
        }

        resourceManager.synchronizeWithAnomalies(starSystems);
        for (StarSystem system : starSystems) {
            if (system.isSunSystem()) {
                system.setInhabitants(system.getCurrentInhabitants());
                for (Iterator<Entry<String, Major>> iter = resourceManager.getRaceController().getMajors().entries()
                        .iterator(); iter.hasNext();) {
                    Entry<String, Major> e = iter.next();
                    if (system.getOwnerId().equals(e.key)) {
                        Major major = e.value;
                        //calculate buildings
                        system.calculateNumberOfWorkBuildings(resourceManager.getBuildingInfos());
                        system.setWorkersIntoBuildings();
                        system.calculateVariables();
                        //calculate all research and security points and assign them
                        int currentPoints = 0;
                        currentPoints = system.getProduction().getResearchProd();
                        major.getEmpire().addResearchPoints(currentPoints);
                        currentPoints = system.getProduction().getSecurityProd();
                        major.getEmpire().addSecurityPoints(currentPoints);
                        //calculate ship support costs
                        float currentInhabitants = system.getCurrentInhabitants();
                        major.getEmpire().addPopSupportCosts(
                                (int) (currentInhabitants * GameConstants.POPSUPPORT_MULTI));
                    }
                }
                for (int i = 0; i < system.getAllBuildings().size; i++) {
                    int id = system.getAllBuildings().get(i).getRunningNumber();
                    String raceId = system.getOwnerId();
                    if (resourceManager.getBuildingInfo(id).getMaxInEmpire() > 0)
                        resourceManager.getGlobalBuildings().addGlobalBuilding(raceId, id);
                }
            }
        }
        calcNewRoundData();
        calcShipEffects();

        for (Iterator<Entry<String, Major>> iter = resourceManager.getRaceController().getMajors().entries().iterator(); iter
                .hasNext();) {
            Entry<String, Major> e = iter.next();
            Major major = e.value;
            Empire empire = major.getEmpire();
            Research research = empire.getResearch();
            int researchLevels[] = research.getResearchLevels();
            for (int i = 0; i < resourceManager.getShipInfos().size; i++) {
                ShipInfo si = resourceManager.getShipInfos().get(i);
                if (si.getRace() == major.getRaceShipNumber()) {
                    if (si.isThisShipBuildableNow(researchLevels)) {
                        major.getWeaponObserver().checkBeamWeapons(si);
                        major.getWeaponObserver().checkTorpedoWeapons(si);
                    }
                }
            }

            empire.generateSystemList(starSystems, resourceManager);
            for (int i = 0; i < empire.getSystemList().size; i++) {
                IntPoint coord = empire.getSystemList().get(i);
                StarSystem system = getStarSystemAt(coord);
                system.calculateBuildableBuildings();
                system.calculateBuildableShips();
                system.calculateBuildableTroops(resourceManager.getTroopInfos(), empire.getResearch());
            }
        }
    }

    private void applyTroopsAtStartup() {
        FileHandle handle = Gdx.files.internal("data/troops/starttroops.txt");
        String input = handle.readString("ISO-8859-1");
        String[] inputs = input.split("\n");
        for (String s : inputs) {
            if (s.startsWith("//") || s.trim().isEmpty())
                continue;
            s = s.trim();
            String[] info = s.split(":");
            String systemName = info[0].trim();
            if (info.length > 1) {
                String[] troopTypes = info[1].split(",");
                boolean foundSector = false;
                for (int y = 0; y < resourceManager.getGridSizeY(); y++) {
                    for (int x = 0; x < resourceManager.getGridSizeX(); x++)
                        if (getStarSystemAt(x, y).getName().equals(systemName)) {
                            foundSector = true;
                            for (String tt : troopTypes) {
                                buildTroop(Integer.parseInt(tt.trim()), new IntPoint(x, y));
                            }
                        }
                    if (foundSector)
                        break;
                }
            }
        }
    }

    private void calcNewRoundData() {
        NewRoundDataCalculator newRoundDataCalculator = new NewRoundDataCalculator(resourceManager);
        newRoundDataCalculator.calcPreLoop();
        for (int i = 0; i < starSystems.size; i++) {
            StarSystem system = starSystems.get(i);
            String ownerId = system.getOwnerId();
            if (system.isMajorized()) {
                Major major = Major.toMajor(resourceManager.getRaceController().getRace(ownerId));
                if (major == null)
                    continue;

                Empire empire = major.getEmpire();
                newRoundDataCalculator.checkRoutes(system, major);
                system.calculateVariables();
                SystemProd production = system.getProduction();
                if (production.isShipyard())
                    system.setShipPort(true, ownerId);
                newRoundDataCalculator.calcMorale(system, resourceManager.getTroopInfos());
                newRoundDataCalculator.calcIntelligenceBonus(production, empire.getIntelligence());

                for (int res = ResourceTypes.TITAN.getType(); res <= ResourceTypes.DERITIUM.getType(); res++)
                    empire.setStorageAdd(res, system.getResourceStore(res));
            }
            newRoundDataCalculator.calcExtraVisibilityAndRangeDueToDiplomacy(system, resourceManager
                    .getRaceController().getMajors(), resourceManager.getRaceController().getMinors());
        }
        NewRoundDataCalculator.getIntelligenceBoniFromSpecialTechsAndSetThem(resourceManager.getRaceController()
                .getMajors());
    }

    public void buildBuilding(int id, IntPoint coord) {
        Building building = new Building(id);
        boolean isOnline = resourceManager.getBuildingInfo(id).getAlwaysOnline();
        building.setIsBuildingOnline(isOnline);
        getStarSystemAt(coord).addNewBuilding(building);
    }

    public int buildShip(int id, IntPoint coord, String ownerID) {
        Race owner = resourceManager.getRaceController().getRace(ownerID);
        id -= 10000;
        ShipInfo info = resourceManager.getShipInfos().get(id);
        Ships ship = new Ships(info);
        ship.setCoordinates(coord);
        ship.setOwner(ownerID);
        ship.setName(resourceManager.getShipNameGenerator()
                .generateShipName(ownerID, owner.getName(), info.isStation()));
        int newShip = shipMap.add(ship);

        if (!owner.isMajor())
            return newShip;

        ship.addSpecialResearchBoni(owner);
        ((Major) owner).getShipHistory().addShip(ship.shipHistoryInfo(), getStarSystemAt(coord).coordsName(true),
                resourceManager.getCurrentRound());

        // send the ship to the designated coordinates if one is selected
        StarSystem ss = getStarSystemAt(coord);
        if (!ss.getBuildTargetCoord().equals(new IntPoint()) && !ship.isStation()) {
            Pair<IntPoint, Array<IntPoint>> p = ((Major)owner).getStarMap().calcPath(coord, ss.getBuildTargetCoord(), 3 - ship.getRange(true).getRange(), ship.getSpeed(true));
            if (p.getSecond().size != 0) {
                ship.setTargetCoord(ss.getBuildTargetCoord());
                ship.setPath(p.getSecond());
            } else {
                StarSystem targetSS = getStarSystemAt(ss.getBuildTargetCoord());
                EmpireNews message = new EmpireNews();
                String targetName = targetSS.getScanned(owner.getRaceId()) ? targetSS.getName() : targetSS.coordsName(false);
                message.CreateNews(StringDB.getString("SET_TARGET_FAILED", false, targetName, ship.getName(), ss.getName()), EmpireNewsType.MILITARY, ss.getName(), ss.getCoordinates());
                ((Major) owner).getEmpire().addMsg(message);
            }
        }
        if (owner.getRaceId().equals(resourceManager.getRaceController().getPlayerRaceString()))
            resourceManager.getAchievementManager().incrementAchievement(AchievementsList.achievementShipwright.getAchievement(), 1);
        return newShip;
    }

    private void generateStarMap() {
        generateStarMap("");
    }

    private void generateStarMap(String onlyForRaceId) {
        for (Iterator<Entry<String, Major>> iter = resourceManager.getRaceController().getMajors().entries().iterator(); iter.hasNext();) {
            Entry<String, Major> e = iter.next();
            Major major = e.value;
            if (!onlyForRaceId.isEmpty() && !onlyForRaceId.equals(e.key))
                continue;
            major.createStarMap();
        }
        resourceManager.synchronizeWithAnomalies(starSystems);
        for (int i = 0; i < starSystems.size; i++) {
            StarSystem system = starSystems.get(i);
            for (Iterator<Entry<String, Major>> iter = resourceManager.getRaceController().getMajors().entries().iterator(); iter.hasNext();) {
                Entry<String, Major> e = iter.next();
                if (!onlyForRaceId.isEmpty() && !onlyForRaceId.equals(e.key))
                    continue;

                Major major = e.value;
                if (system.getProduction().isShipyard() && system.getOwnerId().equals(major.getRaceId())
                        || system.getShipPort(major.getRaceId())) {
                    major.getStarMap().addBase(system.getCoordinates(),
                            major.getEmpire().getResearch().getPropulsionTech());
                }
                if (system.isSunSystem()) {
                    if (system.getOwnerId().equals(e.key) || system.isFree()) {
                        major.getStarMap().addKnownSystem(system.getCoordinates());
                    }
                }

            }
        }
        //check that we can't move to territories where we have a non agression pact
        for (Iterator<Entry<String, Major>> iter = resourceManager.getRaceController().getMajors().entries().iterator(); iter.hasNext();) {
            Entry<String, Major> e = iter.next();
            if (!onlyForRaceId.isEmpty() && !onlyForRaceId.equals(e.key))
                continue;
            ObjectSet<String> napRaces = new ObjectSet<String>();
            for (int i = 0; i < resourceManager.getRaceController().getMajors().size; i++) {
                String otherRace = resourceManager.getRaceController().getMajors().getKeyAt(i);
                if (!e.key.equals(otherRace) && e.value.getAgreement(otherRace) == DiplomaticAgreement.NAP)
                    napRaces.add(otherRace);
            }
            e.value.getStarMap().synchronizeWithMap(starSystems, napRaces);
        }

        //set for outpost calculation
        for (Iterator<Entry<String, Major>> iter = resourceManager.getRaceController().getMajors().entries().iterator(); iter.hasNext();) {
            Entry<String, Major> e = iter.next();
            if (!onlyForRaceId.isEmpty() && !onlyForRaceId.equals(e.key))
                continue;
            Major major = e.value;
            if (!major.aHumanPlays())
                major.getStarMap().setBadAIBaseSectors(starSystems, e.key);
        }

        //calculate Explore targets
        for (Iterator<Entry<String, Major>> iter = resourceManager.getRaceController().getMajors().entries().iterator(); iter.hasNext();) {
            Entry<String, Major> e = iter.next();
            if (!onlyForRaceId.isEmpty() && !onlyForRaceId.equals(e.key))
                continue;
            Major major = e.value;
            if (!major.isHumanPlayer()) // the AI doesn't needs explore targets
                continue;

            Array<Ships> potentialShips = new Array<Ships>();
            for (int j = 0; j < shipMap.getSize(); j++) {
                Ships ship = shipMap.getAt(j);
                if (ship.getOwnerId().equals(e.key) && ship.hasTarget())
                    potentialShips.add(ship);
            }

            for (int i = 0; i < starSystems.size; i++) {
                StarSystem system = starSystems.get(i);
                if (!system.getFullKnown(major.getRaceId()) && major.getStarMap().getRangeValue(system.getCoordinates()) > 0) {
                    boolean found = false;
                    for (int j = 0; j < potentialShips.size; j++) {
                        Ships ship = potentialShips.get(j);
                        if ((ship.getTargetCoord().equals(system.getCoordinates()) || ship.getPath().contains(system.getCoordinates(), false))) {
                            found = true;
                            break;
                        }
                    }
                    if (!found)
                        major.getStarMap().addExploreTarget(system.getCoordinates());
                }
            }
        }
    }

    private void calcShipCombat() {
        if (!combatCalc)
            return;

        Array<Ships> involvedShips = new Array<Ships>();
        IntPoint p = new IntPoint(currentCombatSector);
        StarSystem ss = getStarSystemAt(p);
        for (int i = 0; i < shipMap.getSize(); i++) {
            Ships ship = shipMap.getAt(i);
            if (!ship.getCoordinates().equals(currentCombatSector))
                continue;

            involvedShips.add(ship);

            for (int j = 0; j < ship.getFleetSize(); j++)
                involvedShips.add(ship.getFleet().getAt(j));
        }

        boolean isCombat = CombatAI.calcCombatTactics(involvedShips, resourceManager.getRaceController(), combatOrders, ss.getAnomaly());
        if (!isCombat) {
            for (int i = 0; i < shipMap.getSize(); i++) {
                Ships ship = shipMap.getAt(i);
                if (!ship.getCoordinates().equals(currentCombatSector))
                    continue;
                CombatTactics leaderCombatTactic = ship.getCombatTactics();
                if (leaderCombatTactic != CombatTactics.CT_RETREAT)
                    ship.setCombatTactics(leaderCombatTactic, true, false);
            }
            return;
        }

        final Combat combat = new Combat(resourceManager.getRaceController());
        combat.setInvolvedShips(involvedShips, ss.getAnomaly());
        if (!combat.isReadyForCombat())
            return;

        combat.preCombatCalculation();
        ObjectIntMap<String> winner;
        if (combatOrder == CombatOrder.USER) {
            Gdx.app.postRunnable(new Runnable() {
                @Override
                public void run() {
                    resourceManager.setEndRoundPressed(true);
                    DefaultScreen screen = resourceManager.setView(ViewTypes.COMBAT_SIMULATOR, true);
                    ((CombatSimulator) screen).init(combat);
                }
            });
            do {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                winner = combat.getWinner();
            } while (winner == null);
            combatOrder = CombatOrder.NONE;
        } else
            winner = combat.calculateCombat();

        //generate possible retreat sector
        for (int i = 0; i < combatOrders.size; i++) {
            String race = combatOrders.getKeyAt(i);
            //random
            while (true) {
                int x = (int) (RandUtil.random() * 3) - 1;
                int y = (int) (RandUtil.random() * 3) - 1;
                IntPoint pt = new IntPoint(currentCombatSector.x + x, currentCombatSector.y + y);
                if (!pt.equals(currentCombatSector) && pt.x < resourceManager.getGridSizeX() && pt.x > -1
                        && pt.y < resourceManager.getGridSizeY() && pt.y > -1) {
                    ObjectMap<IntPoint, IntPoint> raceRetreatSectors = shipRetreatSectors.get(race);
                    if (raceRetreatSectors == null)
                        raceRetreatSectors = new ObjectMap<IntPoint, IntPoint>();
                    raceRetreatSectors.put(currentCombatSector, pt);
                    shipRetreatSectors.put(race, raceRetreatSectors);
                    break;
                }
            }
        }

        ArrayMap<String, Major> majors = resourceManager.getRaceController().getMajors();
        ArrayMap<String, Race> races = resourceManager.getRaceController().getRaces();
        for (int i = 0; i < races.size; i++) {
            String raceID = races.getKeyAt(i);
            Race race = races.getValueAt(i);
            String sectorName = ss.coordsName(ss.getKnown(raceID));

            if (winner.get(raceID, 0) == 1 && race.isMajor()) {
                victoryObserver.addCombatWin(raceID);

                Major major = Major.toMajor(race);
                EmpireNews message = new EmpireNews();
                message.CreateNews(StringDB.getString("WIN_COMBAT", false, sectorName), EmpireNewsType.MILITARY, "", p);
                major.getEmpire().addMsg(message);
                //win a minor battle
                String eventText = major.getMoraleObserver().addEvent(3, major.getRaceMoralNumber());
                message = new EmpireNews();
                message.CreateNews(eventText, EmpireNewsType.MILITARY, "", p);
                major.getEmpire().addMsg(message);
                resourceManager.getClientWorker().setEmpireViewFor(major);
                if (major.getRaceId().equals(resourceManager.getRaceController().getPlayerRaceString()))
                    resourceManager.getAchievementManager().unlockAchievement(AchievementsList.achievementGunsBlazing.getAchievement());
            } else if (winner.get(raceID, 0) == 2) {
                if (race.isMajor()) {
                    Major major = Major.toMajor(race);
                    EmpireNews message = new EmpireNews();
                    message.CreateNews(StringDB.getString("LOSE_COMBAT", false, sectorName), EmpireNewsType.MILITARY, "", p);
                    major.getEmpire().addMsg(message);
                    //win a minor battle
                    String eventText = major.getMoraleObserver().addEvent(6, major.getRaceMoralNumber());
                    message = new EmpireNews();
                    message.CreateNews(eventText, EmpireNewsType.MILITARY, "", p);
                    major.getEmpire().addMsg(message);
                    resourceManager.getClientWorker().setEmpireViewFor(major);
                }
                //the relationship to the winner will get worse. If two AI's battle then the effect is smaller
                for (int j = 0; j < majors.size; j++) {
                    String otherID = majors.getKeyAt(j);
                    if (!otherID.equals(raceID) && winner.get(otherID, 0) == 1) {
                        Major majorWin = majors.getValueAt(j);
                        if (!majorWin.aHumanPlays())
                            race.setRelation(otherID, -(int) (RandUtil.random() * 4));
                        else
                            race.setRelation(otherID, -(int) (RandUtil.random() * 6 + 5));
                    }
                }
            } else if (winner.get(raceID, 0) == 3 && race.isMajor()) {
                Major major = Major.toMajor(race);
                EmpireNews message = new EmpireNews();
                message.CreateNews(StringDB.getString("DRAW_COMBAT", false, sectorName), EmpireNewsType.MILITARY, "", p);
                major.getEmpire().addMsg(message);
                resourceManager.getClientWorker().setEmpireViewFor(major);
            }
        }

        //before destorying the ships check how many were destroyed by a bosean alien. Those will be reborn as bosean.
        ArrayMap<Ships, ObjectSet<Ships>> killedShips = combat.getKilledShipInfos();
        for (int i = 0; i < killedShips.size; i++) {
            Ships bosean = killedShips.getKeyAt(i);
            if (bosean == null || !bosean.getOwnerId().equals(Aliens.BOSEANER.getId()) || !bosean.isAlive())
                continue;

            int count = killedShips.getValueAt(i).size;
            if (count == 0)
                continue;

            Ships leader = shipMap.getLeader(bosean);
            if (leader == null) {
                System.err.println("Error: Found no leader ship of Bosean.\nPlease make a bug-report");
                continue;
            }
            for (int j = 0; j < count; j++) {
                int prob = (int) (15 / resourceManager.getGamePreferences().difficulty.getLevel());
                if (RandUtil.random() * 100 >= prob)
                    continue;

                int newShip = buildShip(bosean.getID(), currentCombatSector, bosean.getOwnerId());
                int idx = shipMap.getShipMap().indexOfKey(newShip);

                leader.addShipToFleet(shipMap.getAt(idx));
                shipMap.eraseAt(idx);
            }
        }

        //after the battle i must iterate the ships and get rid of all those that have no hull
        Array<String> destroyedShips = new Array<String>();
        for (int i = 0; i < shipMap.getSize(); i++) {
            Ships s = shipMap.getAt(i);
            if (!s.getCoordinates().equals(currentCombatSector))
                continue;
            //To ensure consistent combat behavior in a leader/fleet situation, set the combat behavior of every
            //ship in the fleet to the leader's behavior, unless it is RETREAT, this case will be handled in ship
            //retreat code
            CombatTactics leadersTactic = s.getCombatTactics();
            if (leadersTactic != CombatTactics.CT_RETREAT)
                s.setCombatTactics(leadersTactic, true, false);
            Race owner = s.getOwner();
            if (s.removeDestroyed(owner, resourceManager.getCurrentRound(), StringDB.getString("COMBAT"),
                    StringDB.getString("DESTROYED"), destroyedShips))
                continue;
            removeShip(i--);
        }

        for (int i = 0; i < majors.size; i++) {
            Major major = majors.getValueAt(i);
            if (winner.containsKey(major.getRaceId())) {
                for (int j = 0; j < destroyedShips.size; j++) {
                    String s = StringDB.getString("DESTROYED_SHIPS_IN_COMBAT", false, destroyedShips.get(j));
                    EmpireNews message = new EmpireNews();
                    message.CreateNews(s, EmpireNewsType.MILITARY, "", p);
                    major.getEmpire().addMsg(message);
                }
            }
        }
    }

    private void calcShipRetreat() {
        //move ships with a retreat order to a neighboring sector
        for (int i = 0; i < shipMap.getSize(); i++) {
            Ships ship = shipMap.getAt(i);
            String shipOwner = ship.getOwnerId();
            RetreatMode mode = ship.calcReatreatMode();
            if (mode == RetreatMode.RETREAT_MODE_STAY)
                continue;

            if (shipRetreatSectors.containsKey(shipOwner)) {
                ObjectMap<IntPoint, IntPoint> sectorRetreatPairs = shipRetreatSectors.get(shipOwner);
                IntPoint coord = ship.getCoordinates();
                if (sectorRetreatPairs.containsKey(coord)) {
                    IntPoint retreatSector = sectorRetreatPairs.get(coord);

                    if (ship.getCombatTactics() == CombatTactics.CT_RETREAT)
                        ship.retreat(retreatSector);
                    if (!ship.hasFleet())
                        continue;

                    //if all ships are in retreat, the entire fleet can retreat
                    boolean completeFleetRetreat = ship.getSpeed(true) > 0 && mode == RetreatMode.RETREAT_MODE_COMPLETE;

                    if (completeFleetRetreat) {
                        CombatTactics newCombatTactic = ship.getCombatTactics();
                        ship.retreatFleet(retreatSector, newCombatTactic);
                    } else {
                        ShipMap fleet = ship.getFleet();
                        shipMap.append(fleet);
                        ship.reset();
                    }
                }
            }
        }
        shipRetreatSectors.clear();
    }

    /**
     * Calculates the effects of the ships and stations on the map, sectors are scanned here, races get known
     */
    private void calcShipEffects() {
        calcShipRetreat();

        for (int i = 0; i < shipMap.getSize(); i++) {
            Ships ship = shipMap.getAt(i);
            String owner = ship.getOwnerId();
            Race race = resourceManager.getRaceController().getRace(owner);
            Major major = Major.toMajor(race);
            if (major != null)
                if (finishedSpecialResearch.get(major.getRaceId(), ResearchComplexType.NONE) != ResearchComplexType.NONE)
                    ship.addSpecialResearchBoni(major, finishedSpecialResearch.get(major.getRaceId()));
            IntPoint p = ship.getCoordinates();
            StarSystem system = getStarSystemAt(p);

            boolean deactivatedShipScanner = false;
            boolean betterScanner = false;
            Anomaly anomaly = system.getAnomaly();
            if (anomaly != null) {
                deactivatedShipScanner = anomaly.isShipScannerDeactivated();
                betterScanner = anomaly.getType() == AnomalyType.QUASAR;
            }

            ship.calcEffects(system, race, deactivatedShipScanner, betterScanner);

            //in case of a battle the station might have been removed, if not then set it again as shipport
            if (ship.isStation())
                system.setShipPort(true, owner);
        }
    }

    /**
     * This function calculates general stuff which are always calculated in the beginning of a new round like clear messages, calculate statistics etc
     */
    private void calcPreDataForNextRound() {
        resourceManager.incCurrentRound();
        for (Iterator<Entry<String, Major>> iter = resourceManager.getRaceController().getMajors().entries().iterator(); iter.hasNext();) {
            Entry<String, Major> e = iter.next();
            Major major = e.value;
            // clear all messages from last round
            major.getEmpire().clearMessagesAndEvents();
            // reset population support costs
            major.getEmpire().setPopSupportCosts(0);
            // calculate remaining agreement durations and if needed show correct screen
            if (major.decrementAgreementsDuration()) {
                resourceManager.getClientWorker().setEmpireViewFor(major);
            }
            //TODO: set major to human or ai??? - for multiplayer... long way there...
        }
        // for ships which can be built only in some systems (like ships of minor races) set the owner to MINORNUMBER.
        // The function which calculates the buildable ships in a system will then assign the value to the correct race.
        // Everybody who owns the system can build the ship then
        Array<ShipInfo> shipInfoArray = resourceManager.getShipInfos();
        for (int i = 0; i < shipInfoArray.size; i++)
            if (!shipInfoArray.get(i).getOnlyInSystem().isEmpty())
                shipInfoArray.get(i).setRace(PlayerRaces.MINORNUMBER.getType());

        for (StarSystem s : starSystems) {
            if (s.isSunSystem()) {
                s.setBlockade(0);
                s.clearDisabledProductions();
            }
        }
        resourceManager.incStarDate(Math.min(731,
                Math.max(14, 743 - Math.pow(resourceManager.getStatistics().getAverageTechLevel(), 3.0))));
    }

    /**
     * This function calculates all system attack
     */
    private void calcSystemAttack() {
        ObjectSet<String> killedMinors = new ObjectSet<String>();
        ObjectSet<IntPoint> fightInSystem = new ObjectSet<IntPoint>();

        for (int i = 0; i < shipMap.getSize(); i++) {
            Ships ship = shipMap.getAt(i);
            if (ship.getCurrentOrder() == ShipOrder.ATTACK_SYSTEM) {
                boolean attackSystem = true;
                //check if the attack was already executed or not
                if (fightInSystem.contains(ship.getCoordinates()))
                    attackSystem = false;

                //only if the ships are decloaked
                if (!ship.canHaveOrder(ShipOrder.ATTACK_SYSTEM, false)) {
                    ship.unsetCurrentOrder();
                    attackSystem = false;
                }

                if (!attackSystem)
                    continue;

                IntPoint p = ship.getCoordinates();
                StarSystem ss = getStarSystemAt(p);
                String defenderID = ss.getOwnerId();
                Gdx.app.debug("General", String.format("Attack of System ???, Defender %s", defenderID));
                ObjectSet<String> attackers = new ObjectSet<String>();
                for (int j = 0; j < shipMap.getSize(); j++) {
                    Ships otherShip = shipMap.getAt(j);
                    if (otherShip.getCoordinates().equals(p) && otherShip.getCurrentOrder() == ShipOrder.ATTACK_SYSTEM) {
                        String ownerId = otherShip.getOwnerId();
                        if (!ownerId.isEmpty())
                            attackers.add(ownerId);
                    }
                }

                AttackSystem systemAttack = new AttackSystem();
                Race defender = null;
                if (!defenderID.isEmpty())
                    defender = resourceManager.getRaceController().getRace(defenderID);
                if (defender != null && defender.isMinor() && !ss.isTaken())
                    systemAttack.Init(defender, ss, shipMap);
                else if (defender != null && defender.isMajor()
                        && systemAttack.isDefenderNotAttacker(defenderID, attackers))
                    systemAttack.Init(defender, ss, shipMap);
                else
                    //if nobody lives in the system (ie rebellion)
                    systemAttack.Init(null, ss, shipMap);

                if (!systemAttack.isAttack()) {
                    fightInSystem.add(p);
                    continue;
                }

                //A system attack reduces the morale in all systems which were taken by us and before it was owned by the major who's system is attacked then the morale is lowered
                if (!defenderID.isEmpty() && defender != null && defender.isMajor())
                    for (StarSystem system : starSystems)
                        if (system.isTaken() && system.getColonyOwner().equals(defenderID)
                                && systemAttack.isDefenderNotAttacker(defenderID, attackers))
                            system.setMorale(-(int) (RandUtil.random() * 5));

                Pair<Boolean, String> attackRes = systemAttack.calculate();
                String attacker = attackRes.getSecond();
                if (attackRes.getFirst()) {
                    Major major = Major.toMajor(resourceManager.getRaceController().getRace(attacker));

                    //if the system has a minor and the system wasn't conquered by others or it wasn't a member then we have to build buildings
                    if (ss.getMinorRace() != null && !ss.isTaken() && defender != null && defender.isMinor()) {
                        Minor minor = Minor.toMinor(defender);
                        minor.setSubjugated(true);
                        //if the system wasn't owned by a major, build buildings
                        ss.buildBuildingsForMinorRace(resourceManager.getBuildingInfos(), resourceManager.getStatistics().getAverageTechLevel(), minor);
                        //now the system is considered taken
                        ss.changeOwner(attacker, SystemOwningStatus.OWNING_STATUS_TAKEN, false);
                        minor.setOwner(attacker);
                        //relation gets a hit
                        minor.setRelation(attacker, -100);
                        //morale also
                        ss.setMorale(-(int) (RandUtil.random() * 25) - 10);
                        if (ss.getMorale() < 50)
                            ss.setMorale(50 - ss.getMorale());
                        String param = ss.getName();
                        String eventText = "";

                        //reset the minor's diplomacy news and send a message to all known empires about the subjugation
                        minor.getIncomingDiplomacyNews().clear();
                        minor.getOutgoingDiplomacyNews().clear();
                        ArrayMap<String, Major> majors = resourceManager.getRaceController().getMajors();
                        for (int j = 0; j < majors.size; j++) {
                            Major other = majors.getValueAt(j);
                            for (int k = 0; k < other.getOutgoingDiplomacyNews().size; k++) {
                                DiplomacyInfo info = other.getOutgoingDiplomacyNews().get(k);
                                if (info.fromRace.equals(minor.getRaceId()) || info.toRace.equals(minor.getRaceId()))
                                    other.getOutgoingDiplomacyNews().removeIndex(k--);
                            }
                            for (int k = 0; k < other.getIncomingDiplomacyNews().size; k++) {
                                DiplomacyInfo info = other.getIncomingDiplomacyNews().get(k);
                                if (info.fromRace.equals(minor.getRaceId()) || info.toRace.equals(minor.getRaceId()))
                                    other.getIncomingDiplomacyNews().removeIndex(k--);
                            }

                            //send message to all majors that know the minor
                            if (other.isRaceContacted(minor.getRaceId())) {
                                EmpireNews message = new EmpireNews();
                                message.CreateNews(StringDB.getString("MINOR_SUBJUGATED", false, minor.getName()),
                                        EmpireNewsType.MILITARY, param, p);
                                other.getEmpire().addMsg(message);
                                resourceManager.getClientWorker().setEmpireViewFor(other);
                            }
                        }
                        eventText = "";
                        if (major != null)
                            eventText = major.getMoraleObserver().addEvent(11, major.getRaceMoralNumber(), param);
                        if (!eventText.isEmpty()) {
                            EmpireNews message = new EmpireNews();
                            message.CreateNews(eventText, EmpireNewsType.MILITARY, param, p, false, 1);
                            major.getEmpire().addMsg(message);
                            resourceManager.getClientWorker().setEmpireViewFor(major);
                        }
                    } else if (ss.getMinorRace() != null && ss.isTaken() && defender != null && defender.isMajor()) { //if the system was liberated
                        //relationship gets worse
                        defender.setRelation(attacker, -(int) (RandUtil.random() * 50));
                        Minor minor = ss.getMinorRace();
                        minor.setRelation(attacker, (int) (RandUtil.random() * 50));
                        minor.setSubjugated(false);
                        //event to the looser
                        String param = ss.getName();
                        Major def = Major.toMajor(defender);
                        String eventText = "";
                        if (def != null)
                            eventText = def.getMoraleObserver().addEvent(17, def.getRaceMoralNumber(), param);
                        if (!eventText.isEmpty()) {
                            EmpireNews message = new EmpireNews();
                            message.CreateNews(eventText, EmpireNewsType.MILITARY, param, p);
                            def.getEmpire().addMsg(message);
                            resourceManager.getClientWorker().setEmpireViewFor(def);
                        }
                        //event to the liberator
                        param = minor.getName();
                        eventText = "";
                        if (major != null)
                            eventText = major.getMoraleObserver().addEvent(13, major.getRaceMoralNumber(), param);
                        if (!eventText.isEmpty()) {
                            EmpireNews message = new EmpireNews();
                            message.CreateNews(eventText, EmpireNewsType.MILITARY, param, p, false, 1);
                            major.getEmpire().addMsg(message);
                            resourceManager.getClientWorker().setEmpireViewFor(major);
                        }
                        //now the system is not taken
                        ss.changeOwner(minor.getRaceId(), SystemOwningStatus.OWNING_STATUS_INDEPENDENT_MINOR);
                        minor.setOwner("");
                        ss.setMorale((int) (RandUtil.random() * 50) + 25);
                    } else { // a different major had the system
                        SystemOwningStatus newOwningStatus = SystemOwningStatus.OWNING_STATUS_TAKEN;
                        //the relationship gets worse
                        if (defender != null && !defender.getRaceId().equals(attacker))
                            defender.setRelation(attacker, -(int) (RandUtil.random() * 50));
                        if (ss.getColonyOwner().equals(attacker)) {
                            newOwningStatus = SystemOwningStatus.OWNING_STATUS_COLONIZED_MEMBERSHIP_OR_HOME;
                            String param = ss.getName();
                            String eventText = "";
                            if (major != null)
                                eventText = major.getMoraleObserver().addEvent(14, major.getRaceMoralNumber(), param);
                            if (!eventText.isEmpty()) {
                                EmpireNews message = new EmpireNews();
                                message.CreateNews(eventText, EmpireNewsType.MILITARY, param, p, false, 1);
                                major.getEmpire().addMsg(message);
                                resourceManager.getClientWorker().setEmpireViewFor(major);
                            }
                            if (defender != null && !defender.getRaceId().equals(attacker) && defender.isMajor()) {
                                Major def = Major.toMajor(defender);
                                eventText = "";
                                if (def != null)
                                    eventText = def.getMoraleObserver().addEvent(17, def.getRaceMoralNumber(), param);
                                if (!eventText.isEmpty()) {
                                    EmpireNews message = new EmpireNews();
                                    message.CreateNews(eventText, EmpireNewsType.MILITARY, param, p);
                                    def.getEmpire().addMsg(message);
                                    resourceManager.getClientWorker().setEmpireViewFor(def);
                                }
                            }
                            ss.setMorale((int) (RandUtil.random() * 25) + 10);
                        } else //if it was a homesystem of a race
                        if (defender != null && defender.isMajor() && ss.getOwnerId().equals(defenderID)
                                && ss.getName().equals(Major.toMajor(defender).getHomeSystemName())) {
                            Major def = Major.toMajor(defender);
                            String param = ss.getName();
                            String eventText = "";
                            if (def != null)
                                eventText = def.getMoraleObserver().addEvent(15, def.getRaceMoralNumber(), param);
                            if (!eventText.isEmpty()) {
                                EmpireNews message = new EmpireNews();
                                message.CreateNews(eventText, EmpireNewsType.MILITARY, param, p);
                                def.getEmpire().addMsg(message);
                                resourceManager.getClientWorker().setEmpireViewFor(def);
                            }
                            //event to the conqueror
                            eventText = "";
                            if (major != null)
                                eventText = major.getMoraleObserver().addEvent(11, major.getRaceMoralNumber(), param);
                            if (!eventText.isEmpty()) {
                                EmpireNews message = new EmpireNews();
                                message.CreateNews(eventText, EmpireNewsType.MILITARY, param, p, false, 1);
                                major.getEmpire().addMsg(message);
                                resourceManager.getClientWorker().setEmpireViewFor(major);
                            }
                            ss.setMorale(-(int) (RandUtil.random() * 25) - 10);
                            if (ss.getMorale() < 50)
                                ss.setMorale(50 - ss.getMorale());
                        } else {
                            String param = ss.getName();
                            //if it was a member race, then it will be taken
                            if (ss.getMinorRace() != null) {
                                Minor minor = ss.getMinorRace();
                                minor.setSubjugated(true);
                                minor.setOwner(attacker);
                                //the relation suffers
                                minor.setRelation(attacker, -100);

                                minor.getIncomingDiplomacyNews().clear();
                                minor.getOutgoingDiplomacyNews().clear();
                                ArrayMap<String, Major> majors = resourceManager.getRaceController().getMajors();
                                for (int j = 0; j < majors.size; j++) {
                                    Major other = majors.getValueAt(j);
                                    for (int k = 0; k < other.getOutgoingDiplomacyNews().size; k++) {
                                        DiplomacyInfo info = other.getOutgoingDiplomacyNews().get(k);
                                        if (info.fromRace.equals(minor.getRaceId())
                                                || info.toRace.equals(minor.getRaceId()))
                                            other.getOutgoingDiplomacyNews().removeIndex(k--);
                                    }
                                    for (int k = 0; k < other.getIncomingDiplomacyNews().size; k++) {
                                        DiplomacyInfo info = other.getIncomingDiplomacyNews().get(k);
                                        if (info.fromRace.equals(minor.getRaceId())
                                                || info.toRace.equals(minor.getRaceId()))
                                            other.getIncomingDiplomacyNews().removeIndex(k--);
                                    }

                                    //send message to all majors that know the minor
                                    if (other.isRaceContacted(minor.getRaceId())) {
                                        EmpireNews message = new EmpireNews();
                                        message.CreateNews(
                                                StringDB.getString("MINOR_SUBJUGATED", false, minor.getName()),
                                                EmpireNewsType.MILITARY, param, p);
                                        other.getEmpire().addMsg(message);
                                        resourceManager.getClientWorker().setEmpireViewFor(other);
                                    }
                                }
                            }

                            if (defender != null)
                                ss.setMorale(-(int) (RandUtil.random() * 25) - 10);
                            else {
                                //if a rebelled system or a system of an eliminated major is taken, this can happen
                                newOwningStatus = SystemOwningStatus.OWNING_STATUS_COLONIZED_MEMBERSHIP_OR_HOME;
                                ss.setColonyOwner(attacker);
                            }

                            if (ss.getMorale() < 50)
                                ss.setMorale(50 - ss.getMorale());

                            //event to the previous owner
                            if (defender != null && !defenderID.equals(attacker) && defender.isMajor()) {
                                Major def = Major.toMajor(defender);
                                String eventText = "";
                                if (def != null)
                                    eventText = def.getMoraleObserver().addEvent(16, def.getRaceMoralNumber(), param);
                                if (!eventText.isEmpty()) {
                                    EmpireNews message = new EmpireNews();
                                    message.CreateNews(eventText, EmpireNewsType.MILITARY, param, p);
                                    def.getEmpire().addMsg(message);
                                    resourceManager.getClientWorker().setEmpireViewFor(def);
                                }
                            }
                            //send event to the conqueror
                            String eventText = "";
                            if (major != null)
                                eventText = major.getMoraleObserver().addEvent(11, major.getRaceMoralNumber(), param);
                            if (!eventText.isEmpty()) {
                                EmpireNews message = new EmpireNews();
                                message.CreateNews(eventText, EmpireNewsType.MILITARY, param, p, false, 1);
                                major.getEmpire().addMsg(message);
                                resourceManager.getClientWorker().setEmpireViewFor(major);
                            }
                        }
                        ss.changeOwner(attacker, newOwningStatus, false);
                    }
                    //after taking the system the ratial buildings are automatically destroyed
                    ss.removeSpecialRaceBuildings(resourceManager.getBuildingInfos());
                    ss.calculateNumberOfWorkBuildings(resourceManager.getBuildingInfos());
                    ss.freeAllWorkers();
                    ss.setWorkersIntoBuildings();

                    //if the defender was a major and it was eliminated then the attacker gets a huge morale boost
                    if (defender != null && defender.isMajor() && !attacker.isEmpty() && major != null
                            && systemAttack.isDefenderNotAttacker(defenderID, attackers)) {
                        Major def = Major.toMajor(defender);
                        def.getEmpire().generateSystemList(starSystems, resourceManager);
                        if (def.getEmpire().getSystemList().size == 0) {
                            String param = def.getName();
                            String eventText = "";
                            if (major != null)
                                eventText = major.getMoraleObserver().addEvent(0, major.getRaceMoralNumber(), param);
                            if (!eventText.isEmpty()) {
                                EmpireNews message = new EmpireNews();
                                message.CreateNews(eventText, EmpireNewsType.MILITARY, param, p, false, 1);
                                major.getEmpire().addMsg(message);
                                resourceManager.getClientWorker().setEmpireViewFor(major);
                            }
                        }
                    }

                    //add a succesful invasion event to the attacker
                    if (!attacker.isEmpty() && major != null && major.isHumanPlayer()) {
                        major.getEmpire().pushEvent(
                                new EventBombardment(resourceManager, "InvasionSuccess", StringDB.getString(
                                        "INVASIONSUCCESSEVENT_HEADLINE", false, ss.getName()), StringDB.getString(
                                        "INVASIONSUCCESSEVENT_TEXT_" + attacker, false, ss.getName())));
                        if (attacker.equals(resourceManager.getRaceController().getPlayerRaceString()))
                            resourceManager.getAchievementManager().unlockAchievement(AchievementsList.achievementWhenDiplomacyFails.getAchievement());
                    }
                    if (defender != null && defender.isMajor() && Major.toMajor(defender).isHumanPlayer()
                            && systemAttack.isDefenderNotAttacker(defenderID, attackers))
                        Major.toMajor(defender)
                                .getEmpire()
                                .pushEvent(
                                        new EventBombardment(resourceManager, "InvasionSuccess", StringDB.getString(
                                                "INVASIONSUCCESSEVENT_HEADLINE", false, ss.getName()), StringDB
                                                .getString("INVASIONSUCCESSEVENT_TEXT_" + defenderID, false,
                                                        ss.getName())));
                } else { //we have only bombarded not invaded
                    for (ObjectSetIterator<String> iter = attackers.iterator(); iter.hasNext();) {
                        String raceID = iter.next();
                        Major major = Major.toMajor(resourceManager.getRaceController().getRace(raceID));
                        if (major == null)
                            continue;

                        if (defender != null) {
                            if (!defenderID.equals(raceID))
                                defender.setRelation(raceID, -(int) (RandUtil.random() * 10));

                            if (!defender.isMinor() && (ss.getMinorRace() != null))
                                ss.getMinorRace().setRelation(major.getRaceId(), -(int) (RandUtil.random() * 5));
                        }
                    }

                    //if the inhabitants is zero then the system is lost
                    if (ss.getInhabitants() <= 0.000001f) {
                        //If there was a minor living on the system, then she will disappear. All diplomatic entries must be erased
                        if (ss.getMinorRace() != null) {
                            Minor minor = ss.getMinorRace();
                            calcEffectsMinorEliminated(minor);

                            // Event message: #21	Eliminate a Minor Race Entirely
                            for (ObjectSetIterator<String> iter = attackers.iterator(); iter.hasNext();) {
                                String raceID = iter.next();
                                Major major = Major.toMajor(resourceManager.getRaceController().getRace(raceID));
                                if (major == null)
                                    continue;

                                String param = minor.getName();
                                String eventText = "";
                                if (major != null)
                                    eventText = major.getMoraleObserver().addEvent(21, major.getRaceMoralNumber(),
                                            param);
                                EmpireNews message = new EmpireNews();
                                message.CreateNews(eventText, EmpireNewsType.MILITARY, param, p);
                                major.getEmpire().addMsg(message);
                            }

                            //save race for deleting
                            killedMinors.add(minor.getRaceId());
                        }

                        //in case of a major the number of systems is reduced
                        if (defender != null && defender.isMajor()
                                && systemAttack.isDefenderNotAttacker(defenderID, attackers)) {
                            Major def = Major.toMajor(defender);

                            String eventText = "";
                            String param = ss.getName();
                            if (def != null) {
                                if (ss.getName().equals(def.getHomeSystemName())) {
                                    //event homesystem lost
                                    eventText = def.getMoraleObserver().addEvent(15, def.getRaceMoralNumber(), param);
                                } else {
                                    //other system lost
                                    eventText = def.getMoraleObserver().addEvent(16, def.getRaceMoralNumber(), param);
                                }
                            }

                            if (!eventText.isEmpty()) {
                                EmpireNews message = new EmpireNews();
                                message.CreateNews(eventText, EmpireNewsType.MILITARY, param, p);
                                def.getEmpire().addMsg(message);
                                resourceManager.getClientWorker().setEmpireViewFor(def);
                            }
                        }

                        ss.changeOwner("", SystemOwningStatus.OWNING_STATUS_EMPTY);
                        ss.setColonyOwner("");

                        //if the defender was a major and it is eliminated then the attacker gets a morale boost
                        if (defender != null && defender.isMajor()
                                && systemAttack.isDefenderNotAttacker(defenderID, attackers)) {
                            Major def = Major.toMajor(defender);
                            def.getEmpire().generateSystemList(starSystems, resourceManager);

                            if (def.getEmpire().getSystemList().size == 0)
                                for (ObjectSetIterator<String> iter = attackers.iterator(); iter.hasNext();) {
                                    String raceID = iter.next();
                                    Major major = Major.toMajor(resourceManager.getRaceController().getRace(raceID));
                                    if (major == null)
                                        continue;

                                    String param = def.getName();
                                    String eventText = "";
                                    if (major != null)
                                        eventText = major.getMoraleObserver().addEvent(0,
                                                major.getRaceMoralNumber(), param);

                                    if (!eventText.isEmpty()) {
                                        EmpireNews message = new EmpireNews();
                                        message.CreateNews(eventText, EmpireNewsType.MILITARY, param, p, false, 1);
                                        major.getEmpire().addMsg(message);
                                        resourceManager.getClientWorker().setEmpireViewFor(major);
                                    }
                                }
                        }
                    } else { // the race wasn't eliminated
                        //event text only if buildings were destroyed or at least 3% population was destroyed
                        if (systemAttack.getDestroyedBuildings() > 0
                                || systemAttack.getKilledPop() >= ss.getInhabitants() * 0.03) {
                            String param = ss.getName();
                            for (ObjectSetIterator<String> iter = attackers.iterator(); iter.hasNext();) {
                                String raceID = iter.next();
                                Major major = Major.toMajor(resourceManager.getRaceController().getRace(raceID));
                                if (major == null)
                                    continue;

                                String eventText = "";
                                if (defender != null) //if the system was owned by someone else
                                    eventText = major.getMoraleObserver().addEvent(19, major.getRaceMoralNumber(),
                                            param);
                                else if (ss.getColonyOwner().equals(raceID) && defender == null) //if a rebelled system was taken back
                                    eventText = major.getMoraleObserver().addEvent(20, major.getRaceMoralNumber(),
                                            param);

                                if (!eventText.isEmpty()) {
                                    EmpireNews message = new EmpireNews();
                                    message.CreateNews(eventText, EmpireNewsType.MILITARY, param, p);
                                    major.getEmpire().addMsg(message);
                                    resourceManager.getClientWorker().setEmpireViewFor(major);
                                }
                            }

                            //also create message for the defender
                            if (defender != null && defender.isMajor()
                                    && systemAttack.isDefenderNotAttacker(defenderID, attackers)) {
                                Major def = Major.toMajor(defender);
                                String eventText = def.getMoraleObserver()
                                        .addEvent(22, def.getRaceMoralNumber(), param);

                                if (!eventText.isEmpty()) {
                                    EmpireNews message = new EmpireNews();
                                    message.CreateNews(eventText, EmpireNewsType.MILITARY, param, p);
                                    def.getEmpire().addMsg(message);
                                    resourceManager.getClientWorker().setEmpireViewFor(def);
                                }
                            }
                        }
                    }

                    //event graphics for the attacker
                    for (ObjectSetIterator<String> iter = attackers.iterator(); iter.hasNext();) {
                        String raceID = iter.next();
                        Major major = Major.toMajor(resourceManager.getRaceController().getRace(raceID));
                        if (major == null)
                            continue;

                        if (major.isHumanPlayer()) {
                            //bombardment
                            if (!systemAttack.isTroopsInvolved())
                                major.getEmpire().pushEvent(
                                        new EventBombardment(resourceManager, "Bombardment", StringDB.getString(
                                                "BOMBARDEVENT_HEADLINE", false, ss.getName()), StringDB.getString(
                                                "BOMBARDEVENT_TEXT_AGRESSOR_" + raceID, false, ss.getName())));
                            else if (ss.getInhabitants() > 0.000001f)//failed invasion
                                major.getEmpire()
                                        .pushEvent(
                                                new EventBombardment(resourceManager, "InvasionFailed",
                                                        StringDB.getString("INVASIONFAILUREEVENT_HEADLINE", false,
                                                                ss.getName()), StringDB.getString(
                                                                "INVASIONFAILUREEVENT_TEXT_" + raceID, false,
                                                                ss.getName())));
                        }
                    }
                    //for the defender
                    if (defender != null && defender.isMajor()
                            && systemAttack.isDefenderNotAttacker(defenderID, attackers)) {
                        Major def = Major.toMajor(defender);
                        if (def.isHumanPlayer()) {
                            //bombardment
                            if (!systemAttack.isTroopsInvolved())
                                def.getEmpire().pushEvent(
                                        new EventBombardment(resourceManager, "Bombardment", StringDB.getString(
                                                "BOMBARDEVENT_HEADLINE", false, ss.getName()), StringDB.getString(
                                                "BOMBARDEVENT_TEXT_DEFENDER_" + defenderID, false, ss.getName())));
                            else if (ss.getInhabitants() > 0.000001f)//failed invasion
                                def.getEmpire().pushEvent(
                                        new EventBombardment(resourceManager, "InvasionFailed", StringDB.getString(
                                                "INVASIONFAILUREEVENT_HEADLINE", false, ss.getName()), StringDB
                                                .getString("INVASIONFAILUREEVENT_TEXT_" + defenderID, false,
                                                        ss.getName())));
                        }
                    }
                }

                //add news
                for (int j = 0; j < systemAttack.getNews().size;) {
                    for (ObjectSetIterator<String> iter = attackers.iterator(); iter.hasNext();) {
                        String raceID = iter.next();
                        Major major = Major.toMajor(resourceManager.getRaceController().getRace(raceID));
                        if (major == null)
                            continue;

                        EmpireNews message = new EmpireNews();
                        message.CreateNews(systemAttack.getNews().get(j), EmpireNewsType.MILITARY, ss.getName(), p);
                        major.getEmpire().addMsg(message);
                        resourceManager.getClientWorker().setEmpireViewFor(major);
                    }
                    if (defender != null && defender.isMajor()
                            && systemAttack.isDefenderNotAttacker(defenderID, attackers)) {
                        Major def = Major.toMajor(defender);
                        EmpireNews message = new EmpireNews();
                        message.CreateNews(systemAttack.getNews().get(j), EmpireNewsType.MILITARY, ss.getName(), p);
                        def.getEmpire().addMsg(message);
                        resourceManager.getClientWorker().setEmpireViewFor(def);
                    }
                    systemAttack.getNews().removeIndex(j);
                }
                fightInSystem.add(p);
            }
        }

        //delete destroyed minors
        for (ObjectSetIterator<String> iter = killedMinors.iterator(); iter.hasNext();) {
            String minorID = iter.next();
            removeShips(minorID);
            resourceManager.getRaceController().removeRace(minorID);
        }

        //go trough ships and all ships without a hull should be removed
        if (fightInSystem.size > 0)
            for (int i = 0; i < shipMap.getSize();) {
                Ships ship = shipMap.getAt(i);
                if (ship.removeDestroyed(ship.getOwner(), resourceManager.getCurrentRound(),
                        StringDB.getString("SYSTEMATTACK"), StringDB.getString("DESTROYED"), null)) {
                    ++i;
                    continue;
                }
                removeShip(i);
            }
    }

    private void calcDiplomacy() {
        DiplomacyController.send(resourceManager);
        DiplomacyController.receive(resourceManager);

        ArrayMap<String, Major> majors = resourceManager.getRaceController().getMajors();
        for (int i = 0; i < majors.size; i++) {
            Major major = majors.getValueAt(i);
            if (major.getIncomingDiplomacyNews().size > 0) {
                resourceManager.getClientWorker().addSoundMessage(SndMgrValue.SNDMGR_MSG_DIPLOMATICNEWS, major, 0);
                resourceManager.getClientWorker().setEmpireViewFor(major);
            }
        }
    }

    //Ship helpers
    boolean buildStation(Ships ship, ShipOrder order, StarSystem system) {
        Major major = (Major) ship.getOwner();
        String owner = ship.getOwnerId();

        ShipType type = (order == ShipOrder.BUILD_STARBASE || order == ShipOrder.UPGRADE_STARBASE) ? ShipType.STARBASE : ShipType.OUTPOST;
        int id = major.bestBuildableVariant(type, resourceManager.getShipInfos());
        StationWorkResult result = ship.new StationWorkResult();
        if (system.isStationBuildable(order, owner)) {
            system.setIsStationBuilding(order, owner);
            if (system.getStartStationPoints(owner) == 0)
                system.setStartStationPoints(resourceManager.getShipInfos().get(id - 10000).getBaseIndustry(), owner);
            result = ship.buildStation(order, system, major, id);
        } else
            ship.unsetCurrentOrder();

        if (order != ShipOrder.BUILD_OUTPOST && result.finished) {
            //if we have built a station then we have to remove the old Outpost or station from the shiplist
            for (int i = 0; i < shipMap.getSize(); i++) {
                Ships s = shipMap.getAt(i);
                if (s.isStation() && s.getCoordinates().equals(system.getCoordinates())) {
                    s.scrap(major, system, false);
                    shipMap.eraseAt(i);
                    break;
                }
            }
        }

        if (owner.equals(resourceManager.getRaceController().getPlayerRaceString()) && order == ShipOrder.BUILD_OUTPOST && result.finished)
            resourceManager.getAchievementManager().unlockAchievement(AchievementsList.achievementFoothold.getAchievement());
        return result.removeLeader;
    }

    boolean attackStillValid(Ships ship, StarSystem system) {
        //if the population was eradicated or we already have it
        if (system.getInhabitants() <= 0.000001 || system.getOwnerId().equals(ship.getOwnerId()))
            return false;
        else if (!system.getOwnerId().isEmpty() && system.getOwnerId().equals(ship.getOwnerId())) {
            Race sectorOwner = resourceManager.getRaceController().getRace(system.getOwnerId());
            Race shipOwner = resourceManager.getRaceController().getRace(ship.getOwnerId());
            if (sectorOwner.getAgreement(ship.getOwnerId()) != DiplomaticAgreement.WAR
                    && !shipOwner.hasSpecialAbility(RaceSpecialAbilities.SPECIAL_NO_DIPLOMACY.getAbility()))
                return false;
        }
        return true;
    }

    private void removeShip(int index) {
        Ships ship = shipMap.getAt(index);
        if (ship.hasFleet()) {
            Ships newFleetShip = ship.giveFleetToFleetsFirstShip();
            shipMap.add(newFleetShip);
        }
        shipMap.eraseAt(index);
    }

    public void buildTroop(int id, IntPoint coord) {
        StarSystem ss = getStarSystemAt(coord);
        ss.addTroop(new TroopInfo(resourceManager.getTroopInfos().get(id)));
        Race race = ss.getOwner();
        if (race == null)
            return;

        int n = ss.getTroops().size - 1;

        //Special Research Troops
        ResearchInfo ri = race.getEmpire().getResearch().getResearchInfo();
        ResearchComplex rc = ri.getResearchComplex(ResearchComplexType.TROOPS);
        if (rc.getComplexStatus() == ResearchStatus.RESEARCHED) {
            //20% offensive
            if (rc.getFieldStatus(1) == ResearchStatus.RESEARCHED) {
                int power = ss.getTroops().get(n).getOffense();
                ss.getTroops().get(n).setOffense(power + (power * rc.getBonus(1)) / 100);
            } else if (rc.getFieldStatus(2) == ResearchStatus.RESEARCHED) {
                ss.getTroops().get(n).addExperiencePoints(rc.getBonus(2));
            }
        }
    }

    private void blockadeSystem(Ships ship, StarSystem system) {
        boolean blockadeStillActive = false;
        String systemOwner = system.getOwnerId();
        String shipOwner = ship.getOwnerId();
        Race shipRace = resourceManager.getRaceController().getRace(shipOwner);
        Race systemRace = resourceManager.getRaceController().getRace(systemOwner);

        if (!systemOwner.equals(shipOwner)) {
            if (system.isMajorized()) {
                if (shipRace.getAgreement(systemOwner).getType() < DiplomaticAgreement.FRIENDSHIP.getType()) {
                    int blockadeValue = system.getBlockade();
                    blockadeValue += ((int) (RandUtil.random() * 20) + 1);
                    blockadeStillActive = true;
                    ship.calcExp();
                    //if the ship leads a fleet, then all ships raise the blockade value
                    for (int i = 0; i < ship.getFleetSize(); i++) {
                        blockadeValue += ((int) (RandUtil.random() * 20) + 1);
                        blockadeStillActive = true;
                        ship.getFleet().getAt(i).calcExp();
                    }

                    system.setBlockade(blockadeValue);
                    systemRace.setRelation(shipOwner, -((int) (RandUtil.random() * (blockadeValue / 10 + 1))));
                }
            }
        }
        // if the blockade cannot be executed anymore, then it has to be removed
        if (!blockadeStillActive)
            ship.unsetCurrentOrder();
        // if the system is blockaded the traderoutes won't produce anymore
        if (system.getBlockade() > 0) {
            for (int i = 0; i < system.getTradeRoutes().size; i++)
                system.getTradeRoutes().get(i).setCredits(0);

            // create eventscreen for attacker and the blockaded one
            if (shipRace != null && shipRace.isMajor()) {
                Major m = (Major) shipRace;
                if (m.isHumanPlayer()) {
                    EventBlockade eventScreen = new EventBlockade(resourceManager, StringDB.getString(
                            "BLOCKADEEVENT_HEADLINE", false, system.getName()), StringDB.getString(
                            "BLOCKADEEVENT_TEXT_" + shipRace.getRaceId(), false, system.getName()));
                    m.getEmpire().pushEvent(eventScreen);
                }
            }
            if (system.isMajorized()) {
                Major m = (Major) systemRace;
                if (m.isHumanPlayer()) {
                    EventBlockade eventScreen = new EventBlockade(resourceManager, StringDB.getString(
                            "BLOCKADEEVENT_HEADLINE", false, system.getName()), StringDB.getString(
                            "BLOCKADEEVENT_TEXT_" + systemRace.getRaceId(), false, system.getName()));
                    m.getEmpire().pushEvent(eventScreen);
                }
            }
        }
    }

    //end ship helpers
    /**
     * Calculates ship orders except System Attack
     */
    private void calcShipOrders() {
        for (int i = 0; i < shipMap.getSize(); i++) {
            Ships ship = shipMap.getAt(i);

            StarSystem system = getStarSystemAt(ship.getCoordinates());
            ShipOrder currentOrder = ship.getCurrentOrder();

            if (currentOrder == ShipOrder.ATTACK_SYSTEM)
                if (!attackStillValid(ship, system))
                    ship.setCurrentOrderAccordingToType();

            Race race = ship.getOwner();
            if (race.isMinor())
                continue; //minors don't currently can do something else

            if (race.isMajor()) {
                Major major = (Major) race;

                switch (currentOrder) {
                    case COLONIZE: {
                        int terraformedPlanets = system.countOfTerraformedPlanets();
                        //check that the sector is unoccupied
                        if (!system.isColonizable(ship.getOwnerId())) {
                            ship.unsetCurrentOrder();
                            continue;
                        }
                        system.distributeColonists(ship.getStartInhabitants() / terraformedPlanets);
                        system.colonize(ship, major);
                        ship.unsetCurrentOrder();
                        major.addLostShipHistory(ship.shipHistoryInfo(), StringDB.getString("COLONIZATION") + " "
                                + system.getName(), StringDB.getString("DESTROYED"), resourceManager.getCurrentRound(),
                                ship.getCoordinates());
                        removeShip(i--);
                        continue;
                    }
                    case TERRAFORM: {
                        boolean terraformDone = false;
                        if (!system.getPlanet(ship.getTerraformingPlanet()).getIsTerraformed()) {
                            if (system.terraform(ship)) {
                                terraformDone = true;
                            }
                        } else { //if the planet for some reason is already terraformed
                            ship.setTerraform(-1);
                        }
                        //if a ship has a fleet, then those ships can also contribute to terraforming in case they have the ability
                        if (ship.hasFleet() && ship.getTerraformingPlanet() != -1) {
                            int colonizePointSum = ship.getColonizePoints();
                            for (int j = 0; j < ship.getFleetSize(); j++) {
                                Ships fship = ship.getFleet().getAt(j);
                                if (fship.getCurrentOrder() != ShipOrder.TERRAFORM)
                                    continue;

                                if (!system.getPlanet(ship.getTerraformingPlanet()).getIsTerraformed()) {
                                    int colonizePoints = fship.getColonizePoints();
                                    colonizePointSum += colonizePoints;
                                    if (system.terraform(fship)) {
                                        terraformDone = true;
                                        break;
                                    }
                                } else {
                                    ship.unsetCurrentOrder();
                                }
                            }
                            //give warning that colony points were
                            int terraFormingPlanet = ship.getTerraformingPlanet();
                            if (terraFormingPlanet != -1 && !terraformDone) {
                                int neededTerraformPoints = system.getPlanet(terraFormingPlanet)
                                        .getNeededTerraformPoints();
                                if (colonizePointSum > neededTerraformPoints) {
                                    String s = StringDB.getString("TERRAFORMING_POINTS_WASTED", false,
                                            system.getName(), String.valueOf(colonizePointSum - neededTerraformPoints));
                                    EmpireNews message = new EmpireNews();
                                    message.CreateNews(s, EmpireNewsType.SOMETHING, system.getName(),
                                            system.getCoordinates());
                                    major.getEmpire().addMsg(message);
                                    resourceManager.getClientWorker().setEmpireViewFor(major);
                                }
                            }
                        }

                        if (terraformDone) {
                            //A planet was terraformed successfully
                            ship.unsetCurrentOrder();
                            //generate message that terraforming is complete
                            String s = StringDB.getString("TERRAFORMING_FINISHED", false, system.getName());
                            EmpireNews message = new EmpireNews();
                            message.CreateNews(s, EmpireNewsType.SOMETHING, system.getName(), system.getCoordinates());
                            major.getEmpire().addMsg(message);
                            resourceManager.getClientWorker().addSoundMessage(SndMgrValue.SNDMGR_MSG_TERRAFORM_COMPLETE, major, 0);
                            resourceManager.getClientWorker().setEmpireViewFor(major);
                            //if we help a race with terraforming, we might get a boost in relationship
                            system.onTerraformPossibleMinor(major);
                            if (major.getRaceId().equals(resourceManager.getRaceController().getPlayerRaceString()))
                                resourceManager.getAchievementManager().unlockAchievement(AchievementsList.achievementGenesis.getAchievement());
                        }
                        break;
                    }
                    case BUILD_OUTPOST: //station work
                    case BUILD_STARBASE:
                    case UPGRADE_OUTPOST:
                    case UPGRADE_STARBASE:
                        if (!system.isStationBuildable(ship.getCurrentOrder(), ship.getOwnerId())) {
                            ship.unsetCurrentOrder();
                            continue;
                        }
                        if (buildStation(ship, currentOrder, system)) {
                            removeShip(i--);
                            continue;
                        }
                        break;
                    case DESTROY_SHIP:
                        ship.scrap(major, system, true);
                        shipMap.eraseAt(i--);
                        continue;
                    case ENCLOAK:
                        ship.setCloak(true);
                        ship.unsetCurrentOrder();
                        break;
                    case DECLOAK:
                        ship.setCloak(false);
                        ship.unsetCurrentOrder();
                        break;
                    case BLOCKADE_SYSTEM:
                        blockadeSystem(ship, system);
                        break;
                    case WAIT_SHIP_ORDER:
                        //Do nothing, but only for this round.
                        ship.unsetCurrentOrder();
                        break;
                    case EXPLORE_WORMHOLE:
                        WormholeLinker lnk = wormholeList.get(ship.getCoordinates());
                        ship.setCoordinates(lnk.getTarget());
                        lnk.explore(ship.getOwnerId());
                        StarSystem ss = getStarSystemAt(ship.getCoordinates());
                        if (ss.getAnomaly() != null && ss.getAnomaly().getType() == AnomalyType.WORMHOLE)
                            wormholeList.get(ss.getCoordinates()).explore(ship.getOwnerId());
                        ship.unsetCurrentOrder();
                        if (ship.getOwnerId().equals(resourceManager.getRaceController().getPlayerRaceString())) {
                            resourceManager.getAchievementManager().unlockAchievement(AchievementsList.achievementLostInSpace.getAchievement());
                        }
                        break;
                    case AUTO_EXPLORE:
                        if (Major.toMajor(ship.getOwner()) != null
                                && (ship.getTargetCoord().equals(new IntPoint()) || ship.getTargetCoord().equals(ship.getCoordinates())))
                            ship.setNewExploreCoord(Major.toMajor(ship.getOwner()).getStarMap());
                        break;
                    default: //REPAIR is done in calcShipMovement and ASSIGN_FLAGSHIP is executed immediately
                        break;
                }

                if (ship.isStation())
                    system.setShipPort(true, ship.getOwnerId());
            }
        }
    }

    private void checkShipsDestroyedByAnomaly() {
        for (int i = 0; i < shipMap.getSize(); i++) {
            Ships ship = shipMap.getAt(i);
            IntPoint coord = ship.getCoordinates();
            if (getStarSystemAt(coord).getAnomaly() == null)
                continue;
            String anomaly = getStarSystemAt(coord).getAnomaly().getMapName(coord);
            Race race = ship.getOwner();
            if (ship.removeDestroyed(race, resourceManager.getCurrentRound(), anomaly, StringDB.getString("DESTROYED"),
                    null, anomaly)) {
                continue;
            }
            removeShip(i--);
        }
    }

    /**
     * Function checks if somewhere a battle is ongoing. If humans are involved then they are notified and can order the ships
     * @return true if there is a battle
     */
    private boolean isShipCombat() {
        combatCalc = false;

        for (int i = 0; i < shipMap.getSize(); i++) {
            Ships ships = shipMap.getAt(i);
            IntPoint p = ships.getCoordinates();
            //if there already was a battle in a sector then it won't be another one
            if (ships.getCombatTactics() != CombatTactics.CT_ATTACK
                    || combatSectors.contains(resourceManager.coordsToIndex(p)))
                continue;
            for (int j = 0; j < shipMap.getSize(); j++) {
                Ships otherShips = shipMap.getAt(j);
                String owner1 = ships.getOwnerId();
                String owner2 = otherShips.getOwnerId();
                if (owner1.equals(owner2) || !otherShips.getCoordinates().equals(p))
                    continue;
                Race race1 = resourceManager.getRaceController().getRace(owner1);
                Race race2 = resourceManager.getRaceController().getRace(owner2);
                if (!Combat.checkDiplomacyStatus(race1, race2))
                    continue;
                combatCalc = true;
                currentCombatSector = p;
                combatSectors.add(resourceManager.coordsToIndex(p));
                combatOrders.clear();
                return true;
            }
        }
        return false;
    }

    /**
     * Function calculates ship movement and some other small stuffs related to ships
     */
    private void calcShipMovement() {
        //Check starmap so that we don't move one a territory which we have a NAP with
        ArrayMap<String, Major> majors = resourceManager.getRaceController().getMajors();
        boolean anomaly = false;
        for (int i = 0; i < majors.size; i++) {
            Major major = majors.getValueAt(i);
            major.getEmpire().setShipCosts(0);
            ObjectSet<String> races = new ObjectSet<String>();
            for (int j = 0; j < majors.size; j++) {
                String second = majors.getValueAt(j).getRaceId();
                if (!second.equals(major.getRaceId()) && major.getAgreement(second) == DiplomaticAgreement.NAP)
                    races.add(second);
                major.getStarMap().synchronizeWithMap(starSystems, races);
            }
        }

        ShipMap shipsFromFleets = new ShipMap();
        for (int i = 0; i < shipMap.getSize(); i++) {
            Ships ship = shipMap.getAt(i);
            IntPoint p = ship.getCoordinates();
            //check if a terrraformorder is still valid
            if (ship.getCurrentOrder() == ShipOrder.TERRAFORM)
                if (getStarSystemAt(p).getPlanet(ship.getTerraformingPlanet()).getIsTerraformed()) {
                    ship.setTerraform(-1);
                    ship.unsetCurrentOrder();
                }

            //check if station build orders are still valid
            //This is necessary, as it can happen, that another major, whose ships are near the start
            //of the shipmap, is still doing station work in this sector in the same round that another major,
            //whose ships come after the first major's in the shipmap, finished its station work in this sector.
            //If it weren't for this ordering in the shipmap, we could just unset the orders of all ships
            //following the finishing event in CalcShipOrders().
            if (ship.isDoingStationWork()
                    && !getStarSystemAt(p).isStationBuildable(ship.getCurrentOrder(), ship.getOwnerId()))
                ship.unsetCurrentOrder();

            IntPoint shipCoord = new IntPoint(ship.getCoordinates());
            IntPoint targetCoord = new IntPoint(ship.getTargetCoord());
            IntPoint nextCoord = new IntPoint();

            //handle Alien entities separately
            if (ship.isAlien() && ship.getSpeed(true) > 0) {
                //if it has no target then we generate one randomly
                if (ship.hasNothingToDo()) {
                    while (true) {
                        targetCoord = new IntPoint((int) (RandUtil.random() * resourceManager.getGridSizeX()),
                                (int) (RandUtil.random() * resourceManager.getGridSizeY()));
                        if (targetCoord.equals(shipCoord))
                            continue;

                        if (getStarSystemAt(targetCoord).getAnomaly() != null)
                            continue;

                        //anaerobic macrobes fly only in empty space or sectors with green suns
                        if (ship.getOwnerId().equals(Aliens.ANAEROBE_MAKROBE.getId()))
                            if (getStarSystemAt(targetCoord).isSunSystem()
                                    && !getStarSystemAt(targetCoord).getStarType().equals(StarType.GREEN_STAR))
                                continue;

                        ship.setTargetCoord(targetCoord);
                        break;
                    }
                } else if ((int) (RandUtil.random() * 3) != 0) //else it flies only every 3 rounds or so
                    targetCoord = new IntPoint();
            }

            if (!targetCoord.equals(new IntPoint())) {
                int range = 3 - ship.getRange(true).getRange();
                int speed = ship.getSpeed(true);

                Race race = null;
                StarMap starmap = null;
                //handle Alien entities separately
                if (ship.isAlien()) {
                    starmap = new StarMap(false, 0, resourceManager);
                    Array<IntPoint> exceptions = new Array<IntPoint>();
                    if (ship.getOwnerId().equals(Aliens.ANAEROBE_MAKROBE.getId())) {
                        for (StarSystem s : starSystems)
                            if (s.isSunSystem() && !s.getStarType().equals(StarType.GREEN_STAR)
                                    && !s.getCoordinates().equals(ship.getCoordinates()))
                                exceptions.add(s.getCoordinates());
                    }
                    starmap.setFullRangeMap(RangeTypes.SM_RANGE_NEAR.getType(), exceptions);
                    //anomalies were already included, because it is calculated in nextRound()
                    Pair<IntPoint, Array<IntPoint>> pair = starmap.calcPath(shipCoord, targetCoord, range, speed);
                    nextCoord = pair.getFirst();
                    ship.setPath(pair.getSecond());
                } else {
                    race = ship.getOwner();
                    if (race != null && race.isMajor()) {
                        Major m = (Major) race;
                        starmap = m.getStarMap();
                        Pair<IntPoint, Array<IntPoint>> pair = starmap.calcPath(shipCoord, targetCoord, range, speed);
                        nextCoord = pair.getFirst();
                        ship.setPath(pair.getSecond());
                    }
                }

                //fly to next coordinate
                if (!nextCoord.equals(new IntPoint())) {
                    ship.setCoordinates(nextCoord);
                    if (nextCoord.equals(targetCoord)) {
                        ship.getPath().clear();
                        ship.setTargetCoord(new IntPoint());
                    }

                    //calculates probability of a random event while exploring
                    if (race != null && race.isMajor() && !(getStarSystemAt(nextCoord).getFullKnown(ship.getOwnerId()))) {
                        resourceManager.getRandomEventCtrl().calcExploreEvent(nextCoord, (Major) race, shipMap);
                        if (race.getRaceId().equals(resourceManager.getRaceController().getPlayerRaceString())) {
                            resourceManager.getAchievementManager().unlockAchievement(AchievementsList.achievementToBoldlyGo.getAchievement());
                            resourceManager.getAchievementManager().incrementAchievement(AchievementsList.achievementExplorer.getAchievement(), 1);
                        }
                    }

                    int high = speed;
                    while (high > 0 && high < ship.getPath().size) {
                        ship.getPath().removeIndex(0);
                        high--;
                    }
                }
            } else {
                ship.getPath().clear();
            }

            StarSystem system = getStarSystemAt(ship.getCoordinates());
            //check if there is an anomaly for recharging shields
            boolean fasterShieldRecharge = false;
            if (system.getAnomaly() != null)
                if (system.getAnomaly().getType().equals(AnomalyType.BINEBULA))
                    fasterShieldRecharge = true;

            //after moving but still before a battle we recharge the shields
            //if we are in a shipport then of course the hull is also repaired
            //FIXME: The shipports are not yet updated for changes due to diplomacy at this spot.
            //If we declared war and are on a shipport of the former friend, the ship is repaired,
            //and a possible repair command isn't unset though it can no longer be set by the player this turn then.
            boolean port = system.getShipPort(ship.getOwnerId());
            if (ship.getCurrentOrder() == ShipOrder.REPAIR)
                ship.repairCommand(port, fasterShieldRecharge, shipsFromFleets);
            else
                ship.traditionalRepair(port, fasterShieldRecharge);

            //check if training applies
            if (system.isMajorized() && system.getOwnerId().equals(ship.getOwnerId())) {
                int xp = system.getProduction().getShipTraining();
                if (xp > 0)
                    ship.applyTraining(xp);
            }

            //if there is an anomaly, calculate the effects on the ships
            if (system.getAnomaly() != null) {
                system.getAnomaly().calcShipEffects(ship, shipsFromFleets);
                anomaly = true;
            }
        }

        shipMap.append(shipsFromFleets);

        if (!anomaly)
            return;
        checkShipsDestroyedByAnomaly();
    }

    private static boolean humanPlayerInCombat(ShipMap ships, IntPoint currentCombatSector) {
        for (int i = 0; i < ships.getSize(); i++) {
            Ships s = ships.getAt(i);
            if (!s.getCoordinates().equals(currentCombatSector))
                continue;
            if (s.getOwner().isMajor()) {
                Major m = (Major) s.getOwner();
                if (m.aHumanPlays())
                    return true;
            }
        }
        return false;
    }

    /**
     * This function calculates everything that's related to intelligence
     */
    private void calcIntelligence() {
        //The security has to be handled before research because actions there can affect it.
        ArrayMap<String, Major> majors = resourceManager.getRaceController().getRandomMajors();
        IntelCalc intel = new IntelCalc(resourceManager);

        //first we calculate the actions, before adding/reducing points for any race - thus all races have the same chance
        for (int i = 0; i < majors.size; i++)
            intel.startCalc(majors.getValueAt(i), majors);

        //then we add the points and then reduce the storage a bit
        for (int i = 0; i < majors.size; i++) {
            Major major = majors.getValueAt(i);
            String majorID = majors.getKeyAt(i);
            intel.addPoints(major);
            intel.reduceDepotPoints(major, -1);
            Intelligence intelligence = major.getEmpire().getIntelligence();
            intelligence.clearBonuses();
            if (intelligence.getIntelReports().isReportAdded()) {
                intelligence.getIntelReports().setIsReportAdded(false);
                intelligence.getIntelReports().sortAllReports();

                EmpireNews message = new EmpireNews();
                message.CreateNews(StringDB.getString("WE_HAVE_NEW_INTELREPORTS"), EmpireNewsType.SECURITY, 4);
                major.getEmpire().addMsg(message);
                resourceManager.getClientWorker().addSoundMessage(SndMgrValue.SNDMGR_MSG_INTELNEWS, major, 0);

                boolean addSpy = false;
                boolean addSab = false;
                for (int j = intelligence.getIntelReports().getAllReports().size - 1; j >= 0; j--) {
                    IntelObject intelObj = intelligence.getIntelReports().getReport(j);
                    if (!intelObj.getEnemy().equals(majorID)
                            && intelObj.getRound() == resourceManager.getCurrentRound()) {
                        String eventText = "";
                        if (intelObj.isSpy() && !addSpy) {
                            eventText = major.getMoraleObserver().addEvent(59, major.getRaceMoralNumber());
                            addSpy = true;
                        } else if (intelObj.isSabotage() && !addSab) {
                            eventText = major.getMoraleObserver().addEvent(60, major.getRaceMoralNumber());
                            addSab = true;
                        }
                        if (!eventText.isEmpty()) {
                            message = new EmpireNews();
                            message.CreateNews(eventText, EmpireNewsType.SECURITY, 4);
                            major.getEmpire().addMsg(message);
                        }
                    }
                    if (addSpy && addSab)
                        break;
                }
            }
        }

        for (int i = 0; i < majors.size; i++) {
            Major major = majors.getValueAt(i);
            if (major.isHumanPlayer())
                if (major.getEmpire().getIntelligence().getIntelReports().isReportAdded())
                    resourceManager.getClientWorker().setEmpireViewFor(major);
        }
    }

    private void calcResearch() {
        ArrayMap<String, SystemProd.ResearchBonus> researchBonus = new ArrayMap<String, SystemProd.ResearchBonus>();
        for (StarSystem system : starSystems) {
            if (!system.isMajorized())
                continue;
            SystemProd prod = system.getProduction();
            ResearchBonus bonus;
            if (!researchBonus.containsKey(system.getOwnerId())) {
                bonus = prod.new ResearchBonus();
                researchBonus.put(system.getOwnerId(), bonus);
            } else {
                bonus = researchBonus.get(system.getOwnerId());
            }
            bonus.add(prod.getResearchBonus());
        }

        ArrayMap<String, Major> majors = resourceManager.getRaceController().getMajors();
        for (Iterator<Entry<String, Major>> iter = majors.entries().iterator(); iter.hasNext();) {
            Entry<String, Major> e = iter.next();
            Major major = e.value;
            Empire empire = major.getEmpire();
            if (researchBonus.containsKey(e.key))
                empire.getResearch().setResearchBoni(researchBonus.get(e.key).bonuses);
            String[] news = empire.getResearch().calculateResearch(empire.getResearchPoints(), resourceManager, major.getRaceId());
            for (int j = 0; j < news.length; j++) {
                if (!news[j].isEmpty()) {
                    EmpireNews message = new EmpireNews();
                    if (j == 7) { // a special research can be researched
                        resourceManager.getClientWorker().addSoundMessage(SndMgrValue.SNDMGR_MSG_SCIENTISTNEWS, major, 0);
                        resourceManager.getClientWorker().setEmpireViewFor(major);
                        message.CreateNews(news[j], EmpireNewsType.RESEARCH, 1);
                    } else {
                        if (major.isHumanPlayer()) {
                            resourceManager.getClientWorker().setEmpireViewFor(major);
                            //create event screen for normal researches
                            if (j < 6) {
                                EventResearch eventScreen = new EventResearch(resourceManager,
                                        StringDB.getString("RESEARCHEVENT_HEADLINE"), j);
                                empire.pushEvent(eventScreen);
                            }
                        }
                        message.CreateNews(news[j], EmpireNewsType.RESEARCH);
                    }
                    empire.addMsg(message);
                }
            }
        }
        ResearchAI.calc(resourceManager);
    }

    private void clearAllPoints(ArrayMap<String, Major> majors) {
        for (int i = 0; i < majors.size; i++)
            majors.getValueAt(i).getEmpire().clearAllPoints();
    }

    private void updateGlobalBuildings(StarSystem system) {
        for (int i = 0; i < system.getAllBuildings().size; i++) {
            int id = system.getAllBuildings().get(i).getRunningNumber();
            if (!system.isMajorized())
                continue;
            String raceId = system.getOwnerId();
            if (resourceManager.getBuildingInfo(id).getMaxInEmpire() > 0)
                resourceManager.getGlobalBuildings().addGlobalBuilding(raceId, id);
        }
        for (int i = 0; i < GameConstants.ALE; i++) {
            int al = system.getAssemblyList().getAssemblyListEntry(i).id;
            if (al > 0 && al < 10000) {
                int id = Math.abs(al);
                if (!system.isMajorized())
                    continue;
                String raceId = system.getOwnerId();
                if (resourceManager.getBuildingInfo(id).getMaxInEmpire() > 0)
                    resourceManager.getGlobalBuildings().addGlobalBuilding(raceId, id);
            }
        }
    }

    private void calcOldRoundData() {
        OldRoundDataCalculator calc = new OldRoundDataCalculator(resourceManager);
        resourceManager.getGlobalBuildings().reset();
        clearAllPoints(resourceManager.getRaceController().getMajors());
        for (int i = 0; i < starSystems.size; i++) {
            StarSystem system = starSystems.get(i);
            //reset possible variables for terraforming and station build
            system.clearAllPoints(true);

            if (!system.isSunSystem())
                continue;

            if (!system.isMajorized()) {
                system.letPlanetsGrow();
                continue;
            }

            Major major = Major.toMajor(system.getOwner());
            if (major == null)
                continue;

            float difficultyLevel = resourceManager.getGamePreferences().difficulty.getLevel();
            //give the credits produced by the system to the empire, delete buildings calculate morale
            calc.creditsDestructionMoral(major, system, resourceManager.getBuildingInfos(), difficultyLevel);

            int diliAdd = OldRoundDataCalculator.deritiumForTheAI(major.aHumanPlays(), system, difficultyLevel);

            boolean isRebellion = system.calculateStorages(major.getEmpire().getResearch().getResearchInfo(), diliAdd);
            if (isRebellion)
                calc.executeRebellion(system, major);

            if (system.getFoodStore() < 0) {
                calc.executeFamine(system, major);
            } else {
                system.letPlanetsGrow();
            }

            if (!isRebellion) {
                calc.hanldePopulationEffects(system, major);
                system.calculateVariables();

                //aliens could affect energy so check
                if (system.checkEnergyBuildings())
                    calc.systemMessage(system, major, "BUILDING_TURN_OFF", EmpireNewsType.SOMETHING, 2);

                if (!major.aHumanPlays() || system.getAutoBuild()) {
                    SystemAI sai = new SystemAI(resourceManager);
                    sai.executeSystemAI(system.getCoordinates());
                }

                calc.build(system, major, resourceManager.getBuildingInfos());
                system.calculateNumberOfWorkBuildings(resourceManager.getBuildingInfos());
                system.setWorkersIntoBuildings();
            }
            updateGlobalBuildings(system);
            if (finishedSpecialResearch.get(major.getRaceId(), ResearchComplexType.NONE) == ResearchComplexType.TROOPS && system.getTroops().size != 0) {
                ResearchComplex rc = major.getEmpire().getResearch().getResearchInfo().getResearchComplex(ResearchComplexType.TROOPS);
                if (rc.getComplexStatus() == ResearchStatus.RESEARCHED)
                    for (int j = 0; j < system.getTroops().size; j++) {
                        //20% offensive
                        if (rc.getFieldStatus(1) == ResearchStatus.RESEARCHED) {
                            int power = system.getTroops().get(j).getOffense();
                            system.getTroops().get(j).setOffense(power + (power * rc.getBonus(1)) / 100);
                        } else if (rc.getFieldStatus(2) == ResearchStatus.RESEARCHED) {
                            system.getTroops().get(j).addExperiencePoints(rc.getBonus(2));
                        }
                    }
            }
        }
    }

    private void calcContactShipToMajorShip(Race race, StarSystem system) {
        ArrayMap<String, Major> majors = resourceManager.getRaceController().getMajors();
        for (int i = 0; i < majors.size; i++) {
            Major major = majors.getValueAt(i);
            String id = majors.getKeyAt(i);
            if (race.getRaceId().equals(id) || !system.getOwnerOfShip(id, true)
                    || !major.canBeContactedBy(race.getRaceId()))
                continue;
            calcContactCommutative(major, race, system);
        }
    }

    private void calcContactCommutative(Major major, Race contactedRace, StarSystem system) {
        major.contact(contactedRace, system);
        contactedRace.contact(major, system);
        if (major.isHumanPlayer())
            resourceManager.getClientWorker().setEmpireViewFor(major);
        if (contactedRace.isMajor() && Major.toMajor(contactedRace).isHumanPlayer())
            resourceManager.getClientWorker().setEmpireViewFor(Major.toMajor(contactedRace));
        if (major.getRaceId().equals(resourceManager.getRaceController().getPlayerRaceString())
                || contactedRace.getRaceId().equals(resourceManager.getRaceController().getPlayerRaceString()))
            resourceManager.getAchievementManager().unlockAchievement(AchievementsList.achievementFirstContact.getAchievement());
    }

    private void calcContactMinor(Major major, StarSystem system) {
        if (system.getMinorRace() == null)
            return;
        Minor minor = system.getMinorRace();
        if (minor.canBeContactedBy(major.getRaceId()))
            calcContactCommutative(major, minor, system);
    }

    private void calcContactNewRaces() {
        for (int i = 0; i < shipMap.getSize(); i++) {
            Ships ship = shipMap.getAt(i);
            String raceid = ship.getOwnerId();
            Race race = resourceManager.getRaceController().getRace(raceid);
            if (race == null)
                continue;

            if (race.hasSpecialAbility(RaceSpecialAbilities.SPECIAL_NO_DIPLOMACY.getAbility()))
                continue;

            IntPoint p = ship.getCoordinates();
            StarSystem system = getStarSystemAt(p);
            String ownerOfSector = system.getOwnerId();
            calcContactShipToMajorShip(race, system);
            if (ownerOfSector.isEmpty() || ownerOfSector.equals(raceid))
                continue;
            Race raceOfSector = resourceManager.getRaceController().getRace(ownerOfSector);
            if (raceOfSector == null)
                continue;

            if (race.isMinor()) {
                if (raceOfSector.canBeContactedBy(raceid) && raceOfSector.isMajor()) {
                    Major major = (Major) raceOfSector;
                    calcContactCommutative(major, race, system);
                }
                continue;
            }
            if (race.isMajor()) {
                Major major = (Major) race;
                calcContactMinor(major, system);
                if (!raceOfSector.canBeContactedBy(raceid))
                    continue;
                calcContactCommutative(major, raceOfSector, system);
            }
        }
    }

    private void calcEffectsMinorEliminated(Minor minor) {
        if (minor == null) {
            Gdx.app.error("UniverseMap", "calcEffectsMinorEliminated minor is NULL!");
            return;
        }

        minor.getIncomingDiplomacyNews().clear();
        minor.getOutgoingDiplomacyNews().clear();

        ArrayMap<String, Major> majors = resourceManager.getRaceController().getMajors();
        for (int i = 0; i < majors.size; i++) {
            Major major = majors.getValueAt(i);
            if (major == null)
                continue;

            for (int j = 0; j < major.getOutgoingDiplomacyNews().size; j++) {
                DiplomacyInfo info = major.getOutgoingDiplomacyNews().get(j);
                if (info.fromRace.equals(minor.getRaceId()) || info.toRace.equals(minor.getRaceId()))
                    major.getOutgoingDiplomacyNews().removeIndex(j--);
            }
            for (int j = 0; j < major.getIncomingDiplomacyNews().size; j++) {
                DiplomacyInfo info = major.getIncomingDiplomacyNews().get(j);
                if (info.fromRace.equals(minor.getRaceId()) || info.toRace.equals(minor.getRaceId()))
                    major.getIncomingDiplomacyNews().removeIndex(j--);
            }

            if (minor.isRaceContacted(major.getRaceId())) {
                String news = StringDB.getString("ELIMINATE_MINOR", false, minor.getName());
                EmpireNews message = new EmpireNews();
                if (minor.isAlien())
                    message.CreateNews(news, EmpireNewsType.SOMETHING);
                else
                    message.CreateNews(news, EmpireNewsType.SOMETHING, getStarSystemAt(minor.getCoordinates())
                            .getName(), minor.getCoordinates());

                major.getEmpire().addMsg(message);
                if (major.isHumanPlayer()) {
                    EventRaceKilled eventScreen = new EventRaceKilled(resourceManager, minor.getRaceId(),
                            minor.getName(), minor.getGraphicFileName());
                    major.getEmpire().pushEvent(eventScreen);
                    resourceManager.getClientWorker().setEmpireViewFor(major);
                }
            }

            major.setIsRaceContacted(minor.getRaceId(), false);
            major.setAgreement(minor.getRaceId(), DiplomaticAgreement.NONE);
        }
        //remove all ships of the minor
        for (int i = 0; i < shipMap.getSize(); i++) {
            Ships s = shipMap.getAt(i);
            if (s.getOwnerId().equals(minor.getRaceId())) {
                shipMap.eraseAt(i--);
            }
        }
    }

    private void calcTrade() {
        int taxMoney[] = { 0, 0, 0, 0, 0 };
        ArrayMap<String, Major> majors = resourceManager.getRaceController().getMajors();
        for (int i = 0; i < majors.size; i++) {
            Major major = majors.getValueAt(i);

            ///SPECIAL RESEARCH
            if (major.getEmpire().getResearch().getResearchInfo().getResearchComplex(ResearchComplexType.TRADE)
                    .getFieldStatus(1) == ResearchStatus.RESEARCHED) {
                float newTax = major.getEmpire().getResearch().getResearchInfo()
                        .getResearchComplex(ResearchComplexType.TRADE).getBonus(1);
                newTax = 1.0f + newTax / 100;
                major.getTrade().setTax(newTax);
            }
            taxMoney = major.getTrade().calculateTradeActions(major, resourceManager);
            for (int j = ResourceTypes.TITAN.getType(); j <= ResourceTypes.IRIDIUM.getType(); j++) {
                if (!resourceManager.getMonopolOwner(j).isEmpty())
                    if (resourceManager.getMonopolOwner(j).equals(major.getRaceId())
                            || major.isRaceContacted(resourceManager.getMonopolOwner(j)))
                        taxMoney[j] += major.getTrade().getTaxesFromBuying()[j];
            }
        }

        for (int i = ResourceTypes.TITAN.getType(); i <= ResourceTypes.IRIDIUM.getType(); i++) {
            String resName = StringDB.getString(ResourceTypes.fromResourceTypes(i).getName());
            if (!resourceManager.getMonopolOwner(i).isEmpty()) {
                Major major = (Major) resourceManager.getRaceController().getRace(resourceManager.getMonopolOwner(i));
                if (major != null)
                    major.getEmpire().setCredits(taxMoney[i]);
            }

            //now check if someone bought a monopoly and give it to them if they were the highest bidder
            double max = 0.0f;
            String monopolRace = "";

            for (int j = 0; j < majors.size; j++) {
                Major major = majors.getValueAt(j);
                if (major.getTrade().getMonopolBuying()[i] > max) {
                    max = major.getTrade().getMonopolBuying()[i];
                    monopolRace = major.getRaceId();
                    resourceManager.setMonopolOwner(i, monopolRace);
                }
            }

            for (int j = 0; j < majors.size; j++) {
                Major major = majors.getValueAt(j);
                String news = "";
                //give the races the money back if they were unsuccessful
                if (!major.getRaceId().equals(monopolRace) && major.getTrade().getMonopolBuying()[i] > 0) {
                    major.getEmpire().setCredits((int) major.getTrade().getMonopolBuying()[i]);
                    news = StringDB.getString("WE_DONT_GET_MONOPOLY", false, resName);
                } else if (major.getRaceId().equals(monopolRace)) {
                    news = StringDB.getString("WE_GET_MONOPOLY", false, resName);
                    if (monopolRace.equals(resourceManager.getRaceController().getPlayerRaceString()))
                        resourceManager.getAchievementManager().unlockAchievement(AchievementsList.achievementRockefeller.getAchievement());
                }

                if (!news.isEmpty()) {
                    EmpireNews message = new EmpireNews();
                    message.CreateNews(news, EmpireNewsType.SOMETHING);
                    major.getEmpire().addMsg(message);
                    resourceManager.getClientWorker().setEmpireViewFor(major);
                    major.getTrade().setMonopolBuying(i, 0);
                }

                //send messages to the empires
                if (!monopolRace.isEmpty() && !monopolRace.equals(major.getRaceId())) {
                    Major otherRace = (Major) resourceManager.getRaceController().getRace(monopolRace);
                    String race = StringDB.getString("UNKNOWN");
                    if (major.isRaceContacted(monopolRace))
                        race = otherRace.getRaceNameWithArticle();

                    news = StringDB.getString("SOMEBODY_GET_MONOPOLY", true, race, resName);
                    EmpireNews message = new EmpireNews();
                    message.CreateNews(news, EmpireNewsType.SOMETHING);
                    major.getEmpire().addMsg(message);
                    resourceManager.getClientWorker().setEmpireViewFor(major);
                }
            }
        }

        //calculate the prices on the markets
        for (int j = 0; j < majors.size; j++) {
            Major major = majors.getValueAt(j);
            major.getTrade().calculatePrices(majors, major);

            int resPrices[] = major.getTrade().getResourcePrice();
            major.getTrade().getTradeHistory().saveCurrentPrices(resPrices, major.getTrade().getTax());
        }
    }

    /**
     * this function calculates whether Alien ships are added randomly into the game
     **/
    private void calcRandomAlienEntities() {
        float frequency = resourceManager.getGamePreferences().alienFrequency;
        if (resourceManager.getGamePreferences().alienFrequency == 0)
            return;

        for (int i = 0; i < resourceManager.getShipInfos().size; i++) {
            ShipInfo shipInfo = resourceManager.getShipInfos().get(i);
            if (!shipInfo.isAlien())
                continue;

            //find the corresponding minor race
            Minor alien = Minor.toMinor(resourceManager.getRaceController().getRace(shipInfo.getOnlyInSystem()));
            if (alien != null) {
                if (!alien.isAlien())
                    continue;

                //don't build a new BattleStation
                if (alien.getRaceId().equals(Aliens.KAMPFSTATION.getId()))
                    continue;

                //check whether the alien fits into the galaxy-wide technology advancements. thus aliens with higher requirements come later in the game, while weak ones don't show up as often in the late gae
                int avgTechLevel = resourceManager.getStatistics().getAverageTechLevel();
                int researchLevels[] = new int[6];
                Arrays.fill(researchLevels, avgTechLevel);
                if (!shipInfo.isThisShipBuildableNow(researchLevels))
                    continue;

                //each level under the average techlevel reduces the probability of the alien appearing
                int avgShipTech = (shipInfo.getBioTech() + shipInfo.getEnergyTech() + shipInfo.getCompTech()
                        + shipInfo.getConstructionTech() + shipInfo.getPropulsionTech() + shipInfo.getWeaponTech()) / 6;
                int mod = Math.max(avgTechLevel - avgShipTech, 0) * 5;

                //Aliens are generated only every X + mod rounds in the game
                //X depends on the galaxy size, per sector there is a 0.01% chance that the alien is created
                int value = resourceManager.getGridSizeX() * resourceManager.getGridSizeY();
                //per techlevel is the probability smaller by 25%
                value /= ((mod * 4 + 100.0) / 100.0);
                float param = (GameConstants.MAX_ALIEN_FREQUENCY - frequency) / frequency;
                if (param != 0.0f)
                    if ((int) (RandUtil.random() * (10000 * param)) > value)
                        continue;

                //get a random sector near the edge of the map
                while (true) {
                    IntPoint p;
                    switch ((int) (RandUtil.random() * 4)) {
                        case 0:
                            p = new IntPoint(0, (int) (RandUtil.random() * resourceManager.getGridSizeY()));
                            break;
                        case 1:
                            p = new IntPoint(resourceManager.getGridSizeX() - 1,
                                    (int) (RandUtil.random() * resourceManager.getGridSizeY()));
                            break;
                        case 2:
                            p = new IntPoint((int) (RandUtil.random() * resourceManager.getGridSizeX()), 0);
                            break;
                        case 3:
                            p = new IntPoint((int) (RandUtil.random() * resourceManager.getGridSizeX()),
                                    resourceManager.getGridSizeY() - 1);
                            break;
                        default:
                            p = new IntPoint((int) (RandUtil.random() * resourceManager.getGridSizeX()),
                                    (int) (RandUtil.random() * resourceManager.getGridSizeY()));
                            break;
                    }

                    if (getStarSystemAt(p).getAnomaly() == null) {
                        int shipKey = buildShip(shipInfo.getID(), p, alien.getRaceId());
                        Ships ship = shipMap.getShipMap().get(shipKey);
                        if (alien.getRaceId().equals(Aliens.IONISIERENDES_GASWESEN.getId())) {
                            ship.setCombatTactics(CombatTactics.CT_AVOID);
                            Gdx.app.debug("UniverseMap", "new Ionizing Gasentity ingame");
                        } else if (alien.getRaceId().equals(Aliens.GABALLIANER_SEUCHENSCHIFF.getId())) {
                            ship.setCombatTactics(CombatTactics.CT_ATTACK);
                            Gdx.app.debug("UniverseMap", "new Gaballian ingame");
                        } else if (alien.getRaceId().equals(Aliens.BLIZZARD_PLASMAWESEN.getId())) {
                            ship.setCombatTactics(CombatTactics.CT_ATTACK);
                            Gdx.app.debug("UniverseMap", "new Blizzard plasmaentity ingame");
                        } else if (alien.getRaceId().equals(Aliens.MORLOCK_RAIDER.getId())) {
                            ship.setCombatTactics(CombatTactics.CT_ATTACK);
                            //randomly build more raiders. the higher the techaverage, the more raider there are
                            if (mod > 0) {
                                int count = (int) (RandUtil.random() * (mod + 1));
                                while (count > 0) {
                                    Gdx.app.debug("UniverseMap", "new Morlock Raider ingame");
                                    int fleetShipKey = buildShip(shipInfo.getID(), p, alien.getRaceId());
                                    Ships fleetShip = shipMap.getShipMap().get(fleetShipKey);
                                    ship.addShipToFleet(fleetShip);
                                    shipMap.eraseAt(shipMap.getShipMap().indexOfKey(fleetShipKey));
                                    count--;
                                }
                            }
                        } else if (alien.getRaceId().equals(Aliens.BOSEANER.getId())) {
                            ship.setCombatTactics(CombatTactics.CT_AVOID);
                            Gdx.app.debug("UniverseMap", "new Bosean ingame");
                        } else if (alien.getRaceId().equals(Aliens.KRYONITWESEN.getId())) {
                            ship.setCombatTactics(CombatTactics.CT_ATTACK);
                            Gdx.app.debug("UniverseMap", "new Kryonit Entity ingame");
                        } else if (alien.getRaceId().equals(Aliens.MIDWAY_ZEITREISENDE.getId())) {
                            ship.setCombatTactics(CombatTactics.CT_AVOID);
                            Gdx.app.debug("UniverseMap", "new Midway Battleship ingame");
                        } else if (alien.getRaceId().equals(Aliens.ANAEROBE_MAKROBE.getId())) {
                            ship.setCombatTactics(CombatTactics.CT_ATTACK);
                            //randomly build more macrobes based on the techlevel
                            if (mod > 0) {
                                int count = (int) (RandUtil.random() * (mod * 2 + 1));
                                while (count > 0) {
                                    Gdx.app.debug("UniverseMap", "new ANAEROBE_MAKROBE ingame");
                                    int fleetShipKey = buildShip(shipInfo.getID(), p, alien.getRaceId());
                                    Ships fleetShip = shipMap.getShipMap().get(fleetShipKey);
                                    ship.addShipToFleet(fleetShip);
                                    shipMap.eraseAt(shipMap.getShipMap().indexOfKey(fleetShipKey));
                                    count--;
                                }
                            }
                        } else if (alien.getRaceId().equals(Aliens.ISOTOPOSPHAERISCHES_WESEN.getId())) {
                            Gdx.app.debug("UniverseMap", "new Isotopospheric Entity ingame");
                            if ((int) (RandUtil.random() * 2) == 0)
                                ship.setCombatTactics(CombatTactics.CT_ATTACK);
                            else
                                ship.setCombatTactics(CombatTactics.CT_AVOID);
                        }
                        break;
                    }
                }
            }
        }
    }

    /**
     * this function calculates the effects of aliens on the systems they are currently in
     **/
    private void calcAlienShipEffects() {
        boolean battleStationInGame = false; //checks whether the battlestation is still in the game

        for (int i = 0; i < shipMap.getSize(); i++) {
            Ships ship = shipMap.getAt(i);
            if (!ship.isAlien())
                continue;

            Alien alien = Alien.toAlien(ship.getOwner());
            if (alien == null)
                continue;

            IntPoint coord = ship.getCoordinates();
            StarSystem system = getStarSystemAt(coord);
            Major owner = Major.toMajor(system.getOwner());

            if (alien.getRaceId().equals(Aliens.IONISIERENDES_GASWESEN.getId())) {
                if (ship.getCombatTactics() == CombatTactics.CT_RETREAT)
                    continue;
                if (!system.isMajorized())
                    continue;

                system.setDisabledProduction(WorkerType.ENERGY_WORKER.getType());

                //if energy is produced, notify about energy outage
                if (system.getProduction().getMaxEnergyProd() > 0) {
                    String s = StringDB.getString("EVENT_IONISIERENDES_GASWESEN", false, system.getName());
                    EmpireNews message = new EmpireNews();
                    message.CreateNews(s, EmpireNewsType.SOMETHING, system.getName(), coord);
                    owner.getEmpire().addMsg(message);
                    if (owner.isHumanPlayer()) {
                        EventAlienEntity eventScreen = new EventAlienEntity(resourceManager, alien.getRaceId(),
                                alien.getName(), s);
                        owner.getEmpire().pushEvent(eventScreen);
                        resourceManager.getClientWorker().setEmpireViewFor(owner);
                    }
                }
            } else if (alien.getRaceId().equals(Aliens.GABALLIANER_SEUCHENSCHIFF.getId())) {
                if (ship.getCombatTactics() == CombatTactics.CT_RETREAT)
                    continue;

                if (system.isMajorized()) {
                    system.setDisabledProduction(WorkerType.FOOD_WORKER.getType());
                    system.setFoodStore(system.getFoodStore() / 2);

                    //if there is food produced or food stored, then notify about it being spoiled
                    if (system.getProduction().getMaxFoodProd() > 0 || system.getFoodStore() > 0) {
                        String s = StringDB.getString("EVENT_GABALLIANER_SEUCHENSCHIFF", false, system.getName());
                        EmpireNews message = new EmpireNews();
                        message.CreateNews(s, EmpireNewsType.SOMETHING, system.getName(), coord);
                        owner.getEmpire().addMsg(message);
                        if (owner.isHumanPlayer()) {
                            EventAlienEntity eventScreen = new EventAlienEntity(resourceManager, alien.getRaceId(),
                                    alien.getName(), s);
                            owner.getEmpire().pushEvent(eventScreen);
                            resourceManager.getClientWorker().setEmpireViewFor(owner);
                        }
                    }
                    //if there are ships in the sector, these will also in 50% of cases become infected
                    if (system.getIsShipInSector() && (int) (RandUtil.random() * 2) == 0) {
                        for (int j = 0; j < shipMap.getSize(); j++) {
                            Ships sp = shipMap.getAt(j);
                            if (!sp.getCoordinates().equals(coord) || sp.isAlien() || sp.isStation())
                                continue;
                            Array<Ships> ships = new Array<Ships>();
                            ships.add(sp);
                            for (int k = 0; k < sp.getFleetSize(); k++)
                                ships.add(sp.getFleet().getAt(k));

                            for (int k = 0; k < ships.size; k++) {
                                Ships temp = ships.get(k);
                                if (temp.getCombatTactics() == CombatTactics.CT_RETREAT)
                                    continue;
                                Race race = temp.getOwner();
                                if (race.isMinor())
                                    continue;
                                Major shipOwner = Major.toMajor(race);

                                temp.setType(ShipType.ALIEN);
                                temp.setTargetCoord(new IntPoint());
                                temp.unsetCurrentOrder();
                                temp.setCombatTactics(CombatTactics.CT_ATTACK);
                                temp.setFlagShip(false);

                                //for each ship make an entry that it was lost
                                shipOwner.addLostShipHistory(temp.shipHistoryInfo(), StringDB.getString("COMBAT"),
                                        StringDB.getString("MISSED"), resourceManager.getCurrentRound(),
                                        temp.getCoordinates());
                                String s = String.format("%s",
                                        StringDB.getString("DESTROYED_SHIPS_IN_COMBAT", false, temp.getName()));
                                EmpireNews message = new EmpireNews();
                                message.CreateNews(s, EmpireNewsType.MILITARY, "", temp.getCoordinates());
                                shipOwner.getEmpire().addMsg(message);
                                getStarSystemAt(temp.getCoordinates()).eraseShip(temp);
                                temp.setOwner(alien.getRaceId());
                                getStarSystemAt(temp.getCoordinates()).addShip(temp);
                            }
                        }
                    }
                }
            } else if (alien.getRaceId().equals(Aliens.BLIZZARD_PLASMAWESEN.getId())) {
                if (ship.getCombatTactics() == CombatTactics.CT_RETREAT)
                    continue;

                if (!system.isMajorized())
                    continue;

                system.setDisabledProduction(WorkerType.ENERGY_WORKER.getType());

                //if there was energy then notify about outage
                if (system.getProduction().getMaxEnergyProd() > 0) {
                    String s = StringDB.getString("EVENT_BLIZZARD_PLASMAWESEN", false, system.getName());
                    EmpireNews message = new EmpireNews();
                    message.CreateNews(s, EmpireNewsType.SOMETHING, system.getName(), coord);
                    owner.getEmpire().addMsg(message);
                    if (owner.isHumanPlayer()) {
                        EventAlienEntity eventScreen = new EventAlienEntity(resourceManager, alien.getRaceId(),
                                alien.getName(), s);
                        owner.getEmpire().pushEvent(eventScreen);
                        resourceManager.getClientWorker().setEmpireViewFor(owner);
                    }
                }
            } else if (alien.getRaceId().equals(Aliens.MORLOCK_RAIDER.getId())) {
                if (ship.getCombatTactics() == CombatTactics.CT_RETREAT)
                    continue;

                if (!system.isMajorized())
                    continue;

                if (alien.getAgreement(owner.getRaceId()).getType() >= DiplomaticAgreement.FRIENDSHIP.getType())
                    continue;

                int creditProd = system.getProduction().getCreditsProd();
                //don't do anything if it's already 0: ie caused by an another morlock raider
                if (creditProd <= 0)
                    continue;

                system.getProduction().disableCreditsProduction();
                String s = StringDB.getString("EVENT_MORLOCK_RAIDER", false, "" + creditProd, system.getName());
                EmpireNews message = new EmpireNews();
                message.CreateNews(s, EmpireNewsType.SOMETHING, system.getName(), coord);
                owner.getEmpire().addMsg(message);
                if (owner.isHumanPlayer()) {
                    EventAlienEntity eventScreen = new EventAlienEntity(resourceManager, alien.getRaceId(),
                            alien.getName(), s);
                    owner.getEmpire().pushEvent(eventScreen);
                    resourceManager.getClientWorker().setEmpireViewFor(owner);
                }
            } else if (alien.getRaceId().equals(Aliens.BOSEANER.getId())) {
                //boseans are set randomly to attack (initially they are on avoid) - now they search for food
                if ((int) (RandUtil.random() * 10) == 0)
                    ship.setCombatTactics(CombatTactics.CT_ATTACK);
            } else if (alien.getRaceId().equals(Aliens.KAMPFSTATION.getId())) {
                battleStationInGame = true;
            } else if (alien.getRaceId().equals(Aliens.MIDWAY_ZEITREISENDE.getId())) {
                if (ship.getCombatTactics() == CombatTactics.CT_RETREAT)
                    continue;

                //if the midway battleship is in a major system, then in case of war it is bombarded, otherwise the relationship will decline in each turn
                //the longer the shup stays the higher the chance of a war (only if money is given to them will change it)
                if (system.isMajorized()) {
                    if (owner.getAgreement(alien.getRaceId()) != DiplomaticAgreement.WAR) {
                        alien.setRelation(owner.getRaceId(), -(int) (RandUtil.random() * 20));
                    } //in case of war the ShipAI will set the ship to ATTACK_SYSTEM
                }
            } else if (alien.getRaceId().equals(Aliens.ISOTOPOSPHAERISCHES_WESEN.getId())) {
                if (ship.getCombatTactics() == CombatTactics.CT_RETREAT)
                    continue;

                //in 25% of the cases the Isotopospheric beings teleport to a different location in the galaxy
                //if there are ships in the sector, they are drawn with them
                if ((int) (RandUtil.random() * 4) == 0) {
                    //take a random position which is not an anomaly
                    while (true) {
                        IntPoint target = new IntPoint((int) (RandUtil.random() * resourceManager.getGridSizeX()),
                                (int) (RandUtil.random() * resourceManager.getGridSizeY()));
                        if (target.equals(ship.getCoordinates()))
                            continue;
                        if (getStarSystemAt(target).getAnomaly() != null)
                            continue;

                        for (int j = 0; j < shipMap.getSize(); j++) {
                            Ships shipsInSector = shipMap.getAt(j);

                            if (!shipsInSector.getCoordinates().equals(ship.getCoordinates()))
                                continue;

                            if (shipsInSector.isAlien())
                                continue;

                            if (shipsInSector.isStation())
                                continue;

                            if (shipsInSector.getCombatTactics() == CombatTactics.CT_RETREAT)
                                continue;

                            if (shipsInSector == ship)
                                continue;

                            shipsInSector.unsetCurrentOrder();

                            shipsInSector.setTargetCoord(new IntPoint());
                            shipsInSector.getPath().clear();

                            shipsInSector.setCoordinates(target);
                        }

                        ship.setTargetCoord(new IntPoint());
                        ship.getPath().clear();
                        ship.setCoordinates(target);
                        break;
                    }

                    continue; //if it travels, no effect on the energy
                }

                if (system.isMajorized()) {
                    system.setDisabledProduction(WorkerType.ENERGY_WORKER.getType());

                    //if there was energy then notify about outage
                    if (system.getProduction().getMaxEnergyProd() > 0) {
                        String s = StringDB.getString("EVENT_ISOTOPOSPHAERISCHES_WESEN", false, system.getName());
                        EmpireNews message = new EmpireNews();
                        message.CreateNews(s, EmpireNewsType.SOMETHING, system.getName(), coord);
                        owner.getEmpire().addMsg(message);
                        if (owner.isHumanPlayer()) {
                            EventAlienEntity eventScreen = new EventAlienEntity(resourceManager, alien.getRaceId(),
                                    alien.getName(), s);
                            owner.getEmpire().pushEvent(eventScreen);
                            resourceManager.getClientWorker().setEmpireViewFor(owner);
                        }
                    }
                }
            }
        }

        if (!battleStationInGame) {
            Minor battleStation = Minor.toMinor(resourceManager.getRaceController()
                    .getRace(Aliens.KAMPFSTATION.getId()));
            if (battleStation != null) {
                calcEffectsMinorEliminated(battleStation);
                resourceManager.getRaceController().removeRace(battleStation.getRaceId());
            }
        }
    }

    private void removeShips(String race) {
        for (int i = 0; i < shipMap.getSize(); i++) { //remove all ships owned by the race also
            if (shipMap.getAt(i).getOwnerId().equals(race))
                shipMap.eraseAt(i--);
        }
    }

    private void calcEndDataForNextRound() {
        ArrayMap<String, Major> majors = resourceManager.getRaceController().getMajors();
        for (Iterator<Entry<String, Major>> iter = majors.entries().iterator(); iter.hasNext();) {
            Entry<String, Major> e = iter.next();
            Major major = e.value;
            Empire empire = major.getEmpire();
            empire.generateSystemList(starSystems, resourceManager);

            //if the empire has no more systems, it will be set to unknown for all and the game ends for it
            if (empire.countSystems() == 0) {
                for (int i = 0; i < majors.size; i++) {
                    Major otherRace = majors.getValueAt(i);
                    if (major.getRaceId().equals(otherRace.getRaceId()))
                        continue;
                    if (otherRace.isRaceContacted(major.getRaceId())) {
                        String news = StringDB.getString("ELIMINATE_MINOR", false, major.getName());
                        EmpireNews message = new EmpireNews();
                        message.CreateNews(news, EmpireNewsType.SOMETHING);
                        otherRace.getEmpire().addMsg(message);
                        if (otherRace.isHumanPlayer()) {
                            EventRaceKilled eventScreen = new EventRaceKilled(resourceManager, major.getRaceId(),
                                    major.getName(), major.getGraphicFileName());
                            otherRace.getEmpire().pushEvent(eventScreen);
                            resourceManager.getClientWorker().setEmpireViewFor(otherRace);
                        }
                    }
                }

                empire.clearMessagesAndEvents();
                resourceManager.getClientWorker().clearSoundMessages(major);

                for (int i = 0; i < resourceManager.getRaceController().getRaces().size; i++) {
                    Race race = resourceManager.getRaceController().getRaces().getValueAt(i);
                    if (major.getRaceId().equals(race.getRaceId()))
                        continue;

                    race.setIsRaceContacted(major.getRaceId(), false);
                    race.setAgreement(major.getRaceId(), DiplomaticAgreement.NONE);

                    major.setIsRaceContacted(race.getRaceId(), false);
                    major.setAgreement(race.getRaceId(), DiplomaticAgreement.NONE);

                    major.getIncomingDiplomacyNews().clear();
                    major.getOutgoingDiplomacyNews().clear();

                    for (int j = 0; j < race.getIncomingDiplomacyNews().size; j++) {
                        DiplomacyInfo info = race.getIncomingDiplomacyNews().get(j);
                        if (info.corruptedRace.equals(major.getRaceId()) || info.fromRace.equals(major.getRaceId())
                                || info.toRace.equals(major.getRaceId()) || info.warpactEnemy.equals(major.getRaceId())
                                || info.warPartner.equals(major.getRaceId()))
                            race.getIncomingDiplomacyNews().removeIndex(j--);
                    }
                    for (int j = 0; j < race.getOutgoingDiplomacyNews().size; j++) {
                        DiplomacyInfo info = race.getOutgoingDiplomacyNews().get(j);
                        if (info.corruptedRace.equals(major.getRaceId()) || info.fromRace.equals(major.getRaceId())
                                || info.toRace.equals(major.getRaceId()) || info.warpactEnemy.equals(major.getRaceId())
                                || info.warPartner.equals(major.getRaceId()))
                            race.getOutgoingDiplomacyNews().removeIndex(j--);
                    }

                    if (race.isMajor()) {
                        Major livingMajor = (Major) race;
                        livingMajor.setDefencePact(major.getRaceId(), false);
                        major.setDefencePact(livingMajor.getRaceId(), false);

                        Intelligence intel = livingMajor.getEmpire().getIntelligence();
                        intel.getAssignment().setGlobalPercentage(0, 0, livingMajor, race.getRaceId(), majors);
                        intel.getAssignment().setGlobalPercentage(1, 0, livingMajor, race.getRaceId(), majors);
                        if (intel.getResponsibleRace().equals(major.getRaceId()))
                            intel.setResponsibleRace(livingMajor.getRaceId());
                        intel.getAssignment().removeRaceFromAssignments(major.getRaceId());
                    }
                }

                //remove all ships
                for (int i = 0; i < shipMap.getSize(); i++) {
                    Ships s = shipMap.getAt(i);
                    if (s.getOwnerId().equals(major.getRaceId())) {
                        major.getShipHistory().modifyShip(s.shipHistoryInfo(),
                                getStarSystemAt(s.getCoordinates()).coordsName(true),
                                resourceManager.getCurrentRound(), StringDB.getString("UNKNOWN"),
                                StringDB.getString("DESTROYED"));
                        shipMap.eraseAt(i--);
                    }
                }

                //set sectors and system to neutral
                for (StarSystem system : starSystems) {
                    String id = major.getRaceId();
                    if (system.getOwnerId().equals(id))
                        system.changeOwner("", SystemOwningStatus.OWNING_STATUS_EMPTY);

                    system.setIsStationBuilding(ShipOrder.NONE, id);
                    system.setShipPort(false, id);
                }

                //reset monopol owner if the old owner was eliminated
                for (int i = 0; i < resourceManager.getMonopolOwners().length; i++) {
                    if (resourceManager.getMonopolOwner(i).equals(major.getRaceId()))
                        resourceManager.setMonopolOwner(i, "");
                }

                //if it's a human player, he'll get a loss screen
                if (major.isHumanPlayer()) {
                    EventGameOver eventScreen = new EventGameOver(resourceManager);
                    major.getEmpire().pushEvent(eventScreen);
                    return;
                }
            }
        }

        for (Iterator<Entry<String, Major>> iter = majors.entries().iterator(); iter.hasNext();) {
            Entry<String, Major> e = iter.next();
            Major major = e.value;
            Empire empire = major.getEmpire();
            if (empire.countSystems() == 0)
                continue;

            major.getMoraleObserver().calculateEvents(starSystems, major.getRaceId(), major.getRaceMoralNumber());
            ///SPECIAL RESEARCH
            ResearchComplex complex = empire.getResearch().getResearchInfo()
                    .getResearchComplex(ResearchComplexType.STORAGE_AND_TRANSPORT);
            if (complex.getFieldStatus(2) == ResearchStatus.RESEARCHED)
                empire.getGlobalStorage().setLosing(complex.getBonus(2));
            //calculate transfers in the global storage
            major.getEmpire().getGlobalStorage().calculate(resourceManager);
            major.getEmpire().getGlobalStorage().setMaxTakenResources(1000 * major.getEmpire().countSystems());
            //if there is some resource in the global storage, get a message
            if (major.getEmpire().getGlobalStorage().isFilled()) {
                String s = StringDB.getString("RESOURCES_IN_GLOBAL_STORAGE");
                EmpireNews message = new EmpireNews();
                message.CreateNews(s, EmpireNewsType.ECONOMY, 4);
                major.getEmpire().addMsg(message);
                resourceManager.getClientWorker().setEmpireViewFor(major);
            }

            //ship costs
            int popSupport = empire.getPopSupportCosts();
            int shipCosts = empire.getShipCosts();
            int costs = popSupport - shipCosts;
            if (costs < 0)
                empire.setCredits(costs);
        }

        for (Iterator<Entry<String, Major>> iter = majors.entries().iterator(); iter.hasNext();) {
            Entry<String, Major> e = iter.next();
            Major major = e.value;
            String id = e.key;
            if (major.getEmpire().countSystems() == 0)
                continue;
            for (StarSystem ss : starSystems) {
                IntPoint p = ss.getCoordinates();
                int x = p.x;
                int y = p.y;

                //if there is an outpost in the system, it gets one ownerpoint, if a starbase 2
                int ownerPoints = 0;
                if (ss.isMajorized() && ss.getOwnerId().equals(id))
                    ownerPoints += 1;
                if (ss.getStation(ShipType.OUTPOST, id) != null)
                    ownerPoints += 1;
                if (ss.getStation(ShipType.STARBASE, id) != null)
                    ownerPoints += 2;
                if (ownerPoints != 0)

                    if (ownerPoints > 0) {
                        for (int j = -1; j <= 1; j++)
                            for (int i = -1; i <= 1; i++)
                                if ((y + j < resourceManager.getGridSizeY() && y + j > -1)
                                        && (x + i < resourceManager.getGridSizeX() && x + i > -1))
                                    getStarSystemAt(x + i, y + j).addOwnerPoints(ownerPoints, id);
                        //vertically and horizontally 2 fields around the sector are influenced
                        if (x - 2 >= 0)
                            getStarSystemAt(x - 2, y).addOwnerPoints(ownerPoints, id);
                        if (x + 2 < resourceManager.getGridSizeX())
                            getStarSystemAt(x + 2, y).addOwnerPoints(ownerPoints, id);
                        if (y - 2 >= 0)
                            getStarSystemAt(x, y - 2).addOwnerPoints(ownerPoints, id);
                        if (y + 2 < resourceManager.getGridSizeY())
                            getStarSystemAt(x, y + 2).addOwnerPoints(ownerPoints, id);
                    }
            }
        }

        expansionSystems.clear();
        //calculate owners and variables for next round
        //to avoid #iterator can't be nested!!!
        for (int i = 0; i < starSystems.size; i++) {
            StarSystem ss = starSystems.get(i);
            ss.calculateOwner();
            Major major = Major.toMajor(ss.getOwner());
            if (ss.isMajorized() && major != null) {
                ss.calculateBuildableBuildings();
                ss.calculateBuildableShips();
                ss.calculateBuildableTroops(resourceManager.getTroopInfos(), major.getEmpire().getResearch());
                ss.calculateVariables();

                //calculate and assign the produced research and intelligence points
                int currentPoints;
                currentPoints = ss.getProduction().getResearchProd();
                major.getEmpire().addResearchPoints(currentPoints);
                currentPoints = ss.getProduction().getSecurityProd();
                major.getEmpire().addSecurityPoints(currentPoints);

                ss.executeManager(major, true);
            }

            if (ss.getAnomaly() != null) //if there is an anomaly, possibly reduce scan range
                ss.getAnomaly().reduceScanPower(ss.getCoordinates());

            if (ss.isExpansionSector(resourceManager.getRaceController().getPlayerRaceString()))
                expansionSystems.add(ss);
        }

        StarSystem.SystemSortType oldSortType = StarSystem.getSortType();
        StarSystem.setSortType(SystemSortType.BY_SECTORVALUE);
        expansionSystems.sort();
        expansionSystems.reverse();
        StarSystem.setSortType(oldSortType);

        // now we can check if we can contact new races
        calcContactNewRaces();
        for (Iterator<Entry<String, Major>> iter = majors.entries().iterator(); iter.hasNext();) {
            Entry<String, Major> e = iter.next();
            Major major = e.value;
            Empire empire = major.getEmpire();
            Research research = empire.getResearch();
            int researchLevels[] = research.getResearchLevels();
            for (int i = 0; i < resourceManager.getShipInfos().size; i++) {
                ShipInfo si = resourceManager.getShipInfos().get(i);
                if (si.getRace() == major.getRaceShipNumber()) {
                    if (si.isThisShipBuildableNow(researchLevels)) {
                        major.getWeaponObserver().checkBeamWeapons(si);
                        major.getWeaponObserver().checkTorpedoWeapons(si);
                    }
                }
            }
        }
        linkWormholes(true); //relink wormholes if needed

        victoryObserver.observe(resourceManager);
        if (victoryObserver.isVictory() && !resourceManager.isContinueAfterVictory()) {
            for (int i = 0; i < majors.size; i++) {
                Major major = majors.getValueAt(i);
                if (major.isHumanPlayer()) {
                    String imageName;
                    if (major.getRaceId().equals(victoryObserver.getVictoryRace()))
                        imageName = "Victory" + major.getRaceId();
                    else
                        imageName = "GameOver";
                    EventVictory eventScreen = new EventVictory(resourceManager, victoryObserver.getVictoryRace(),
                            victoryObserver.getVictoryType(), imageName);
                    major.getEmpire().insertEvent(eventScreen, 0);
                }
            }
        }
    }

    private void nextRound() {
        boolean combatInCurrentRound = (combatSectors.size != 0);

        ArrayMap<String, Major> majors = resourceManager.getRaceController().getMajors();
        ObjectIntMap<String> oldCredits = new ObjectIntMap<String>();
        for (int i = 0; i < majors.size; i++) {
            Major major = majors.getValueAt(i);
            oldCredits.put(majors.getKeyAt(i), major.getEmpire().getCredits());
        }

        if (!combatInCurrentRound) {
            resourceManager.getClientWorker().clearSoundMessages();
            // human players should be disconnected at this point.. check for eliminated races
            Array<String> delMajors = new Array<String>();
            for (Iterator<Entry<String, Major>> iter = resourceManager.getRaceController().getMajors().entries()
                    .iterator(); iter.hasNext();) {
                Entry<String, Major> e = iter.next();
                if (e.value.getEmpire().countSystems() == 0)
                    delMajors.add(e.key);
            }
            for (String m : delMajors) {
                removeShips(m);
                resourceManager.getRaceController().removeRace(m);
            }
            generateStarMap();

            //AI calculations
            resourceManager.getSectorAI().clear();
            resourceManager.getSectorAI().calculateDangers();
            resourceManager.getSectorAI().calculateSectorPriorities();

            ShipAI shipAI = new ShipAI(resourceManager);
            shipAI.calculateShipOrders(resourceManager.getSectorAI());

            resourceManager.getAIPrios().clear();
            resourceManager.getAIPrios().calcShipAndTroopPrios(resourceManager.getSectorAI());
            resourceManager.getAIPrios().getIntelAI().calcIntelligence(resourceManager);

            //statistics should be calculated after the sectorAI is calculated
            resourceManager.getStatistics().calcStats(resourceManager);

            calcPreDataForNextRound();
            //diplomacy has to be calculated before ship movement
            calcDiplomacy();
            calcShipOrders();
            calcShipMovement();
            if (isShipCombat()) {
                //if there is no human involved, continue
                if (!humanPlayerInCombat(shipMap, currentCombatSector))
                    nextRound();
                //in case there is a combat go out and calculate the battle
                return;
            }
        } else {//if there is combat
            calcShipCombat();
            // if there is still a battle, go out again
            if (isShipCombat()) {
                //if there is no human involved, continue
                if (!humanPlayerInCombat(shipMap, currentCombatSector))
                    nextRound();
                //in case there is a combat go out and calculate the battle
                return;
            }
        }

        //calculate minors only after a battle
        ArrayMap<String, Minor> minors = resourceManager.getRaceController().getMinors();
        for (Iterator<Entry<String, Minor>> iter = minors.entries().iterator(); iter.hasNext();) {
            Entry<String, Minor> e = iter.next();
            Minor minor = e.value;
            //calculate points for longer relationships
            minor.calcAcceptancePoints();

            //if it was subjugated or is a member somewhere, then don't continue here
            if (minor.isSubjugated())
                continue;
            if (minor.isMemberTo())
                continue;

            IntPoint coord = minor.getCoordinates();
            if (!coord.equals(new IntPoint())) {
                StarSystem system = getStarSystemAt(coord);
                if (!coord.equals(new IntPoint()) && !system.isMajorized()
                        && system.getOwnerId().equals(minor.getRaceId())) {
                    //if the minor colonizes a planet
                    if (minor.perhapsExtend()) //some inhabitants are added on those
                        system.setInhabitants(system.getCurrentInhabitants());

                    //calculate the resource consumption
                    minor.consumeResources();
                    //build ship for minor?
                    minor.perhapsBuildShip();
                }
            }
        }

        for (int i = 0; i < majors.size; i++)
            resourceManager.getRandomEventCtrl().calcEvents(majors.getValueAt(i));

        resourceManager.getMajorJoining().Calculate();

        calcSystemAttack();
        calcIntelligence();
        calcResearch();

        calcAlienShipEffects();
        calcOldRoundData(); //calculate systems (build list, morale, energy etc)
        calcRandomAlienEntities();
        calcShipEffects();
        calcNewRoundData();
        calcTrade();

        //calculate random event hull virus
        resourceManager.getRandomEventCtrl().calcShipEvents();

        calcEndDataForNextRound();
        for (int i = 0; i < majors.size; i++) {
            Major major = majors.getValueAt(i);
            major.getEmpire().setCreditsChange(major.getEmpire().getCredits() - oldCredits.get(major.getRaceId(), 0));
            finishedSpecialResearch.put(major.getRaceId(), ResearchComplexType.NONE);
            if (major.getRaceId().equals(resourceManager.getRaceController().getPlayerRaceString()) && major.getEmpire().getCredits() >= 1000000)
                resourceManager.getAchievementManager().unlockAchievement(AchievementsList.achievementCroesus.getAchievement());
        }
        combatSectors.clear();
        combatCalc = false;
    }

    public void processTurn() {
        Major playerRace = resourceManager.getRaceController().getPlayerRace();

        if (resourceManager.getGameSettings().stickyScreen)
            resourceManager.getClientWorker().setNextActiveView(resourceManager.getCurrentView(), playerRace);
        else
            resourceManager.getClientWorker().setNextActiveView(ViewTypes.GALAXY_VIEW, playerRace); // reset to galaxy by default
        oldView = resourceManager.getClientWorker().getNextActiveView(playerRace.getRaceId());

        combatOrders.put(resourceManager.getRaceController().getPlayerRaceString(), combatOrder);
        nextRound();

        while (combatCalc && playerRace.aHumanPlays()) {
            resourceManager.setEndRoundPressed(false);
            Gdx.app.postRunnable(new Runnable() {
                @Override
                public void run() {
                    resourceManager.setView(ViewTypes.COMBAT_VIEW, true);
                }
            });
            try {
                endturnSema.acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            combatOrders.put(resourceManager.getRaceController().getPlayerRaceString(), combatOrder); //the combat orders must be re-set now according to the combat screen selection
            nextRound();
        }
        generateStarMap(resourceManager.getRaceController().getPlayerRaceString());

        resourceManager.setEndRoundPressed(false);//round calculation is completed here

        while (resourceManager.getCurrentRound() < resourceManager.getGamePreferences().autoTurns) {
            Gdx.app.debug("UniverseMap", "Autoturns: " + resourceManager.getCurrentRound());
            Gdx.app.postRunnable(new Runnable() {
                @Override
                public void run() {
                    renderer.updateScreen();
                    resourceManager.setView(ViewTypes.GALAXY_VIEW);
                    endturnSema.release();
                }
            });
            try {
                endturnSema.acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            processTurn();
        }
    }

    public void afterProcessTurn() {
        Major playerRace = resourceManager.getRaceController().getPlayerRace();
        if (resourceManager.getGameSettings().stickyScreen)
            resourceManager.getClientWorker().setNextActiveView(oldView, playerRace);

        EventScreen screen = playerRace.getEmpire().firstEvent();
        if (screen != null) {
            resourceManager.setScreen(screen);
        } else {
            ViewTypes nextView = resourceManager.getClientWorker().getNextActiveView(playerRace.getRaceId());
            if (nextView != ViewTypes.NULL_VIEW) {
                DefaultScreen scr = resourceManager.setView(nextView);
                if (resourceManager.getClientWorker().getNextActiveView(playerRace.getRaceId()) == ViewTypes.EMPIRE_VIEW)
                    resourceManager.setSubMenu(scr, 0);
                else
                    resourceManager.setSubMenu(scr, scr.getSubMenu());
            }

            resourceManager.getClientWorker().commitSoundMessages(resourceManager.getSoundManager(), playerRace);
            resourceManager.getSoundManager().playMessages();
        }
        if (resourceManager.getGameSettings().autoSave) {
            autoSave();
        }
        resourceManager.getAchievementManager().popupAchievements();
    }

    private int getNextAutoSaveSlot() {
        if (nextAutoSaveIndex == -1) {
            FileHandle dirHandle = new FileHandle(GameConstants.getSaveLocation());
            if (!dirHandle.exists())
                dirHandle.mkdirs();

            Array<SaveInfo> saveInfos = new Array<SaveInfo>();

            for (FileHandle entry : dirHandle.list()) {
                if (!entry.isDirectory() && entry.extension().equals("sav")
                        && entry.nameWithoutExtension().startsWith("auto")) {
                    SaveInfo info = new SaveInfo();
                    info.fileName = entry.nameWithoutExtension();
                    Date d = new Date(entry.file().lastModified());
                    boolean isValid = false;
                    info.date = d;
                    try {
                        Input input = new Input(new InflaterInputStream(new FileInputStream(GameConstants.getSaveLocation() + entry.name())));
                        Kryo kryo = resourceManager.getKryo();
                        GamePreferences prefs = kryo.readObject(input, GamePreferences.class);
                        isValid = true;
                        info.majorID = input.readString();
                        info.currentTurn = input.readInt();
                        input.close();
                        info.height = prefs.gridSizeY;
                        info.width = prefs.gridSizeX;
                        info.shape = prefs.generateFromShape.getName();
                    } catch (IOException e) {
                        System.out.println("Error reading savegame: ");
                        e.printStackTrace();
                    } catch (KryoException e) {
                        System.out.println("Error reading savegame");
                    }
                    if (isValid)
                        saveInfos.add(info);
                }
            }
            SaveInfo.sortByName = false;
            saveInfos.sort();
            if (saveInfos.size != 0)
                nextAutoSaveIndex = Integer.parseInt(saveInfos.get(saveInfos.size - 1).fileName.substring(4, 8));
            else
                nextAutoSaveIndex = 0;
        }
        nextAutoSaveIndex = (nextAutoSaveIndex == 0) ? 1 : 0;
        return nextAutoSaveIndex;
    }

    private void autoSave() {
        resourceManager.getUniverseMap().saveUniverse("auto" + String.format("%04d", getNextAutoSaveSlot()) + ".sav");
    }

    public void drawImages(Stage stage, StarSystem starSystem) {
        if (renderer != null) {
            ArrayList<Pair<String, IntPoint>> shipSymbol = starSystem.shipSymbolInSector(resourceManager
                    .getRaceController().getPlayerRace());
            if (shipSymbol.size() != 0) {
                for (int k = 0; k < shipSymbol.size(); k++) {
                    Image shipSymbolImage = new Image();
                    String path = shipSymbol.get(k).getFirst();
                    IntPoint co = shipSymbol.get(k).getSecond();
                    TextureRegion tex = resourceManager.getSymbolTextures(path);
                    shipSymbolImage.setDrawable(new TextureRegionDrawable(tex));
                    shipSymbolImage.setBounds(co.x, co.y, GamePreferences.spriteSize / 2.56f,
                            GamePreferences.spriteSize / 2.56f);
                    starSystem.addImage(shipSymbolImage);
                    stage.addActor(shipSymbolImage);
                }
            }
        }
    }

    public Array<StarSystem> getStarSystems() {
        return starSystems;
    }

    public ShipMap getShipMap() {
        return shipMap;
    }

    public void setSelectedCoordValue(IntPoint coord) {
        selectedCoord = new IntPoint(coord);
    }

    public IntPoint getSelectedCoordValue() {
        return new IntPoint(selectedCoord);
    }

    public Ships getCurrentShip() {
        if (shipMap.currentShip() >= shipMap.getShipMap().size)
            shipMap.setCurrentShip(shipMap.getShipMap().size - 1);
        if(shipMap.currentShip() < 0)
            shipMap.setCurrentShip(0);
        return shipMap.getAt(shipMap.currentShip());
    }

    public void setCurrentShip(Ships ship) {
        int index = shipMap.getShipMap().indexOfValue(ship, true);
        shipMap.setCurrentShip(index);
    }

    public boolean searchNextIdleShipAndJumpToIt(ShipOrder order) {
        Major player = resourceManager.getRaceController().getPlayerRace();
        if (resourceManager.processingTurn())
            return false;
        if (shipMap.empty())
            return false;
        if (shipMap.getSize() <= previouslyJumpedToShip.index)
            previouslyJumpedToShip.index = shipMap.getSize() - 1;

        int startAt = 0;
        int stopAt = shipMap.getSize();

        Ships previousShip = shipMap.getAt(previouslyJumpedToShip.index);
        if (previousShip.getOwnerId().equals(player.getRaceId())
                && previousShip.getCoordinates().equals(getSelectedCoordValue())
                && previousShip.getName().equals(previouslyJumpedToShip.name)
                && (previousShip.getCurrentOrder() == ShipOrder.NONE)) {//previous ship still valid
            startAt = previouslyJumpedToShip.index;
            stopAt = previouslyJumpedToShip.index + 1;
        }

        for (int i = startAt; i < stopAt; i++) {
            Ships ship = shipMap.getAt(i);
            if (ship.getOwnerId().equals(player.getRaceId())) {
                if (ship.hasNothingToDo()) {
                    if (order != ShipOrder.NONE) {
                        ship.setCurrentOrder(order);
                        ship.setPath(new Array<IntPoint>());
                        ship.setTargetCoord(new IntPoint(), true);
                    }
                    previouslyJumpedToShip = new RememberedShip(ship.getName(), i);
                    setSelectedCoordValue(ship.getCoordinates());
                    resourceManager.setView(ViewTypes.GALAXY_VIEW);
                    renderer.show();
                    renderer.getRightSideBar().getShipRenderer().selectShip(ship);
                    renderer.showShips(true);
                    break;
                }
            }
        }
        return renderer.getRightSideBar().getShipRenderer().getSelectedShip() != null;
    }

    public UniverseRenderer getRenderer() {
        return renderer;
    }

    public VictoryObserver getVictoryObserver() {
        return victoryObserver;
    }

    public void setCombatOrder(CombatOrder order) {
        this.combatOrder = order;
    }

    public IntPoint getCurrentCombatSector() {
        return currentCombatSector;
    }

    public boolean isOnMap(IntPoint p) {
        return p.inRect(0, 0, resourceManager.getGridSizeX(), resourceManager.getGridSizeY());
    }

    public WormholeLinker getWormHole(IntPoint p) {
        return wormholeList.get(p);
    }

    public Semaphore getSemaphore() {
        return endturnSema;
    }

    public Array<StarSystem> getExpansionSystems() {
        return expansionSystems;
    }

    public void setFinishedSpecialResearch(String raceID, ResearchComplexType type) {
        finishedSpecialResearch.put(raceID, type);
    }

    @Override
    public void dispose() {
        shipMap.dispose();
    }

    /*
     * TEST CODE FOR NETWORK COMMUNICATION
     */
//    Output output = new Output(new DeflaterOutputStream(new FileOutputStream(GameConstants.getSaveLocation() + "networkstack"), new Deflater(Deflater.DEFAULT_COMPRESSION)));

    public void writeBeginGameData(Kryo kryo, Output output) {
        output.writeInt(resourceManager.getGridSizeX());
        output.writeInt(resourceManager.getGridSizeY());
        output.writeDouble(resourceManager.getGamePreferences().researchSpeedFactor);
        kryo.writeObject(output, resourceManager.getGamePreferences().expansionSpeed);
        kryo.writeObject(output, resourceManager.getGamePreferences().difficulty);

        //send over hashes of the data files to make sure they are the same for everyone
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("SHA-256");
            FileHandle handle = Gdx.files.internal("data/buildings/buildingstatlist.txt");
            String content = handle.readString(GameConstants.getCharset());
            byte[] hash = digest.digest(content.getBytes());
            kryo.writeObject(output, hash);

            handle = Gdx.files.internal("data/buildings/troopstatlist.txt");
            content = handle.readString(GameConstants.getCharset());
            hash = digest.digest(content.getBytes());
            kryo.writeObject(output, hash);

            handle = Gdx.files.internal("data/buildings/shipstatlist.txt");
            content = handle.readString(GameConstants.getCharset());
            hash = digest.digest(content.getBytes());
            kryo.writeObject(output, hash);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public void readBeginGameData(Kryo kryo, Input input) {
        resourceManager.setGridSizeX(input.readInt());
        resourceManager.setGridSizeY(input.readInt());
        resourceManager.getGamePreferences().researchSpeedFactor = input.readDouble();
        resourceManager.getGamePreferences().expansionSpeed = kryo.readObject(input, ExpansionSpeed.class);
        resourceManager.getGamePreferences().difficulty = kryo.readObject(input, Difficulties.class);
        kryo.readObject(input, byte[].class);
        kryo.readObject(input, byte[].class);
        kryo.readObject(input, byte[].class);
        //TODO: compare hashes
    }

    public void writeNextRoundData(Kryo kryo, Output output) {
        output.writeBoolean(combatCalc);
        if (combatCalc) {
            kryo.writeObject(output, currentCombatSector);
            shipMap.writeNextRoundData(kryo, output, currentCombatSector);
            return;
        }

        output.writeInt(resourceManager.getCurrentRound());
        output.writeDouble(resourceManager.getStarDate());
        kryo.writeObject(output, resourceManager.getShipInfos());
        kryo.writeObject(output, resourceManager.getMonopolOwners());
        ObjectIntMap<String> moralMap = new ObjectIntMap<String>();
        for (int i = 0; i < resourceManager.getRaceController().getMajors().size; i++) {
            Major major = resourceManager.getRaceController().getMajors().getValueAt(i);
            moralMap.put(major.getRaceId(), major.getEmpire().getMoraleEmpireWide());
        }
    }

    @SuppressWarnings("unchecked")
    public void readNextRoundData(Kryo kryo, Input input) {
        combatCalc = input.readBoolean();
        if (combatCalc) {
            currentCombatSector = kryo.readObject(input, IntPoint.class);
            shipMap.readNextRoundData(kryo, input, currentCombatSector);
            return;
        }

        resourceManager.setCurrentRound(input.readInt());
        resourceManager.setStarDate(input.readDouble());
        resourceManager.setShipInfos(kryo.readObject(input, Array.class));
        resourceManager.setMonopolOwners(kryo.readObject(input, String[].class));
        ObjectIntMap<String> moralMap = kryo.readObject(input, ObjectIntMap.class);
        for (int i = 0; i < resourceManager.getRaceController().getMajors().size; i++) {
            Major major = resourceManager.getRaceController().getMajors().getValueAt(i);
            major.getEmpire().setMoraleEmpireWide(moralMap.get(major.getRaceId(), 0));
        }
    }

    public void writeEndOfRoundData(Kryo kryo, Output output) {
        if (combatCalc) {
            kryo.writeObject(output, combatOrder);
            return;
        }
        Major player = resourceManager.getRaceController().getPlayerRace();
        for(int i = 0; i < resourceManager.getShipInfos().size; i++)
            if(resourceManager.getShipInfos().get(i).getRace() == player.getRaceShipNumber())
                kryo.writeObject(output, resourceManager.getShipInfos().get(i));

        shipMap.writeEndofRoundData(kryo, output, player.getRaceId());

        IntArray systems = new IntArray();
        for (int i = 0; i < starSystems.size; i++) {
            StarSystem ss = starSystems.get(i);
            if(ss.getOwnerId().equals(player.getRaceId()) && ss.isMajorized())
                systems.add(i);
        }
        output.writeInt(systems.size);
        for(int i = 0; i < systems.size; i++) {
            output.writeInt(systems.get(i));
            starSystems.get(systems.get(i)).write(kryo, output, true);
        }
        player.write(kryo, output);

        kryo.writeObject(output, resourceManager.getClientWorker().getNextActiveView(player.getRaceId()));
    }

    public void readEndRoundData(Kryo kryo, Input input, String race) {
        Major major = Major.toMajor(resourceManager.getRaceController().getRace(race));
        if (combatCalc) {
            CombatOrder receivedOrder = kryo.readObject(input, CombatOrder.class);
            combatOrders.put(race, receivedOrder);
            return;
        }
        for(int i = 0; i < resourceManager.getShipInfos().size; i++)
            if(resourceManager.getShipInfos().get(i).getRace() == major.getRaceShipNumber()) {
                resourceManager.getShipInfos().get(i).read(kryo, input);
            }
        shipMap.readEndofRoundData(kryo, input, race);
        int size = input.readInt();
        for (int i = 0; i < size; i++) {
            int idx = input.readInt();
            starSystems.get(idx).read(kryo, input, true);
        }
        major.read(kryo, input);

        ViewTypes nextView = kryo.readObject(input, ViewTypes.class);
        resourceManager.getClientWorker().setNextActiveView(nextView, major);
    }
}
