/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.galaxy;

import com.badlogic.gdx.utils.ArrayMap;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.constants.AnomalyType;
import com.blotunga.bote.constants.ShipOrder;
import com.blotunga.bote.constants.ShipRange;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.ships.ShipMap;
import com.blotunga.bote.ships.Ships;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.utils.IntPoint;
import com.blotunga.bote.utils.RandUtil;

public class Anomaly {
    private AnomalyType type;
    private boolean flipHorz;
    private transient ResourceManager resourceManager;

    public Anomaly() {
        type = AnomalyType.fromInt((int) (RandUtil.random() * AnomalyType.values().length));
        flipHorz = ((int) (RandUtil.random() * 2) == 1);
    }

    public Anomaly(ResourceManager manager) {
        this();
        resourceManager = manager;
    }

    public Anomaly(ResourceManager manager, int type) {
        this.type = AnomalyType.fromInt(type);
        flipHorz = ((int) (RandUtil.random() * 2) == 1);
        resourceManager = manager;
    }

    public void setResourceManager(ResourceManager manager) {
        resourceManager = manager;
    }

    public AnomalyType getType() {
        return type;
    }

    public boolean getFlipHorz() {
        return flipHorz;
    }

    public String getMapName(IntPoint coord) {
        return StringDB.getString(type.getMapName()) + " " + coord.x + ", " + coord.y;
    }

    public String getPhysicalDescription() {
        return StringDB.getString(type.getMapName() + "_DESC");
    }

    public String getGameplayDescription() {
        return StringDB.getString(type.getMapName() + "_GAME");
    }

    @Override
    public String toString() {
        return type.toString();
    }
    /**
     * @return a weight which is included in the pathfinding through the sector
     */
    public double getWaySearchWeight() {
        switch (type) {
        // nebulas
            case RADIONEBULA:
                return 5.0;
            case METNEBULA:
                return 0.0;
            case DEUTNEBULA:
                return 5.0;
            case IONSTORM:
                return 5.0;
            case BINEBULA:
                return 0.0;
            case TORIONGASNEBULA:
                return 0.0;
                // neutron stars
            case NEUTRONSTAR:
                return 10.0;
            case RADIOPULSAR:
                return 25.0;
            case XRAYPULSAR:
                return 50.0;
            case MAGNETAR:
                return 100.0;
                // distortions
            case GRAVDISTORTION:
                return 10.0;
            case CONTINUUMRIP:
                return 100.0;
            case BLACKHOLE:
                return 0.0;
                // other
            case QUASAR:
                return 0.0;
            case WORMHOLE:
                return 0.0;
        }
        return 0;
    }

    /**
     * Reduces the scanpower in this and neighboring systems
     * @param pt coordinate on map
     */
    public void reduceScanPower(IntPoint pt) {
        if (type == AnomalyType.BLACKHOLE || type == AnomalyType.RADIOPULSAR || type == AnomalyType.XRAYPULSAR
                || type == AnomalyType.MAGNETAR) {
            ArrayMap<String, Major> majors = resourceManager.getRaceController().getMajors();

            for (int i = 0; i < majors.size; i++) {
                int range = 1;
                StarSystem system = resourceManager.getUniverseMap().getStarSystemAt(pt);
                system.putScannedSquare(range, -50, majors.getValueAt(i), false, false, true);
            }
        }
    }

    public boolean isShipScannerDeactivated() {
        return (type == AnomalyType.NEUTRONSTAR || type == AnomalyType.RADIOPULSAR || type == AnomalyType.XRAYPULSAR
                || type == AnomalyType.MAGNETAR || type == AnomalyType.DEUTNEBULA || type == AnomalyType.IONSTORM);
    }

    public void calcShipEffects(Ships ship, ShipMap shipsFromFleets) {
        if (type == AnomalyType.NEUTRONSTAR || type == AnomalyType.RADIOPULSAR || type == AnomalyType.XRAYPULSAR
                || type == AnomalyType.MAGNETAR) {
            //make a shield damage, if it has a fleet for all ships in the fleet
            for (int i = 0; i < ship.getFleetSize(); i++) {
                Ships s = ship.getFleet().getAt(i);
                makeShieldDmg(500, 75, s);
            }
            makeShieldDmg(500, 75, ship);
        } else if (type == AnomalyType.DEUTNEBULA) {
            //shields are drained completely
            for (int i = 0; i < ship.getFleetSize(); i++) {
                Ships s = ship.getFleet().getAt(i);
                s.getShield().setCurrentShield(s.getShield().getCurrentShield() * (-1));
            }
            ship.getShield().setCurrentShield(ship.getShield().getCurrentShield() * (-1));
            if (ship.getCurrentOrder() == ShipOrder.EXTRACT_DEUTERIUM)
                ship.extractDeuterium(shipsFromFleets);
        } else if (type == AnomalyType.RADIONEBULA) {
            //all crew experience is lost
            for (int i = 0; i < ship.getFleetSize(); i++) {
                Ships s = ship.getFleet().getAt(i);
                s.setCrewExperience(s.getCrewExperience() * (-1));
            }
            ship.setCrewExperience(ship.getCrewExperience() * (-1));
        } else if (type == AnomalyType.CONTINUUMRIP) {
            //ship is destroyed
            for (int i = 0; i < ship.getFleetSize(); i++) {
                Ships s = ship.getFleet().getAt(i);
                s.getHull().setCurrentHull(s.getHull().getCurrentHull() * (-1));
            }
            ship.getHull().setCurrentHull(ship.getHull().getCurrentHull() * (-1));
        } else if (type == AnomalyType.GRAVDISTORTION) {
            for (int i = 0; i < ship.getFleetSize(); i++) {
                Ships s = ship.getFleet().getAt(i);
                makeHullDmg(50, 50, s);
            }
            makeHullDmg(50, 50, ship);

        } else if (type == AnomalyType.IONSTORM)
            ship.applyIonStormEffects(shipsFromFleets);
    }

    private void makeShieldDmg(int minDmgValue, int maxDmgPercent, Ships ship) {
        int currentShield = ship.getShield().getCurrentShield();
        int max = (int) (RandUtil.random() * currentShield * (maxDmgPercent + 1) / 100);
        int shieldDmg = Math.max(minDmgValue, max);
        ship.getShield().setCurrentShield(currentShield - shieldDmg);

        if (ship.getShield().getCurrentShield() == 0)
            ship.getHull().setCurrentHull(ship.getHull().getCurrentHull() * (-1));

        //X-raypulsar and Magnetar make all crew loose experience
        if (type == AnomalyType.XRAYPULSAR || type == AnomalyType.MAGNETAR)
            ship.setCrewExperience(ship.getCrewExperience() * (-1));
        if (type == AnomalyType.MAGNETAR)
            perhapsStrand(ship);
    }

    private void makeHullDmg(int minDmgValue, int maxDmgPercent, Ships ship) {
        int max = (int) (RandUtil.random() * ship.getHull().getCurrentHull() * (maxDmgPercent + 1) / 100);
        int hullDmg = Math.max(minDmgValue, max);
        ship.getHull().setCurrentHull(-hullDmg, true);
    }

    private void perhapsStrand(Ships ship) {
        int propTech = 0;
        for (int i = 0; i < resourceManager.getShipInfos().size; i++)
            if (resourceManager.getShipInfos().get(i).getShipClass().equals(ship.getShipClass())) {
                propTech = resourceManager.getShipInfos().get(i).getPropulsionTech();
                break;
            }

        // the better the propulsion technology, the lower the chance
        int random = (int) (RandUtil.random() * 100);
        if (random > propTech * 10) {
            ship.setSpeed(0);
            ship.setRange(ShipRange.SHORT);
        }
    }
}
