/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.galaxy;

import com.badlogic.gdx.utils.ObjectSet;
import com.blotunga.bote.constants.WormholeLinkType;
import com.blotunga.bote.utils.IntPoint;

public class WormholeLinker {
    private IntPoint target;
    private WormholeLinkType linktype;
    private ObjectSet<String> exploredBy;

    public WormholeLinker() {
        target = null;
        linktype = WormholeLinkType.LINK_NONE;
        exploredBy = new ObjectSet<String>();
    }

    public IntPoint getTarget() {
        return target;
    }

    public void setTarget(IntPoint target) {
        this.target = target;
    }

    public WormholeLinkType getLinktype() {
        return linktype;
    }

    public void setLinktype(WormholeLinkType linktype) {
        this.linktype = linktype;
    }

    public boolean isExplored(String race) {
        return exploredBy.contains(race);
    }

    public void explore(String race) {
        exploredBy.add(race);
    }
}
