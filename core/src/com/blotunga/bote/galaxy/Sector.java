/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.galaxy;

import java.util.Arrays;
import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap.Entry;
import com.blotunga.bote.GamePreferences;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.constants.PlanetSize;
import com.blotunga.bote.constants.PlanetType;
import com.blotunga.bote.constants.PlanetZone;
import com.blotunga.bote.constants.ResourceTypes;
import com.blotunga.bote.constants.ShipOrder;
import com.blotunga.bote.constants.StarType;
import com.blotunga.bote.events.EventRandom;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.races.Minor;
import com.blotunga.bote.races.Race;
import com.blotunga.bote.ships.Ship;
import com.blotunga.bote.ships.ShipMap;
import com.blotunga.bote.ships.Ships;
import com.blotunga.bote.utils.Pair;
import com.blotunga.bote.utils.RandUtil;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

public class Sector extends MapTile {
    protected Array<Planet> planets;
    protected String colonyOwner;
    private StarType starType;
    private String homeOf;

    public Sector() {
        this(null);
    }

    public Sector(ResourceManager res) {
        this(-1, -1, res);
    }

    public Sector(int x, int y, ResourceManager res) {
        super(x, y, res);
        colonyOwner = "";
        starType = null;
        homeOf = "";
        planets = new Array<Planet>(false, 9, Planet.class);
    }

    @Override
    public void reset(boolean callUp) {
        planets.clear();
        colonyOwner = "";
        starType = null;
        homeOf = "";
        if (callUp)
            super.reset(true);
    }

    /**
     * In the beginning of each turn we have to set the isStationBuild to false, later
     * if ships make actions these will be corrected.
     */
    @Override
    public void clearAllPoints(boolean callUp) {
        // Function should be called in the beginning of each turn!!! If no station is built
        // then we can set the used points to null

        //if the planet is terraformed, we set it to false here for now
        //to avoid #iterator can't be nested!!!
        for (int i = 0; i < planets.size; i++) {
            Planet p = planets.get(i);
            p.setIsTerraForming(false);
        }

        if (callUp) {
            super.clearAllPoints(false);
        }
    }

    public String getColonyOwner() {
        return colonyOwner;
    }

    public StarType getStarType() {
        return starType;
    }

    public Minor getMinorRace() {
        if (isHomeOf() != null)
            return Minor.toMinor(isHomeOf());
        return null;
    }

    public boolean isColonizable(String race) {
        if ((!ownerID.isEmpty()) && (!ownerID.equals(race)))
            return false;

        //to avoid #iterator can't be nested!!!
        for (int i = 0; i < planets.size; i++) {
            Planet p = planets.get(i);
            if (p.isColonizable())
                return true;
        }
        return false;
    }

    public boolean hasTerraformable(String race) {
        if ((!ownerID.isEmpty()) && (!ownerID.equals(race)))
            return false;

        //to avoid #iterator can't be nested!!!
        for (int i = 0; i < planets.size; i++) {
            Planet p = planets.get(i);
            if(p.getIsHabitable() && !p.getIsTerraformed())
                return true;
        }
        return false;
    }

    public Race isHomeOf() {
        return resourceManager.getRaceController().getRace(homeOf);
    }

    public String homeOfID() {
        return homeOf;
    }

    public int getNumberOfPlanets() {
        return planets.size;
    }

    public Planet getPlanet(int index) {
        return planets.get(index);
    }

    public Array<Planet> getPlanets() {
        return planets;
    }

    public float getCurrentInhabitants() {
        float currentInhabitants = 0.0f;
        //to avoid #iterator can't be nested!!!
        for (int i = 0; i < planets.size; i++) {
            Planet p = planets.get(i);
            currentInhabitants += p.getCurrentInhabitants();
        }
        return currentInhabitants;
    }

    public float getMaxInhabitants() {
        float maxInhabitants = 0.0f;
        //to avoid #iterator can't be nested!!!
        for (int i = 0; i < planets.size; i++) {
            Planet p = planets.get(i);
            maxInhabitants += p.getMaxInhabitants();
        }
        return maxInhabitants;
    }

    /**
     * @return the number of maximum inhabitants based on the colonized planets only
     */
    public float getCurrentMaxInhabitants() {
        float maxInhabitants = 0.0f;
        //to avoid #iterator can't be nested!!!
        for (int i = 0; i < planets.size; i++) {
            Planet p = planets.get(i);
            if(p.getIsInhabited())
                maxInhabitants += p.getMaxInhabitants();
        }
        return maxInhabitants;
    }

    public boolean[] getAvailableResources() {
        return getAvailableResources(true);
    }

    public boolean[] getAvailableResources(boolean onlyColonized) {
        boolean[] res = new boolean[ResourceTypes.DERITIUM.getType() + 1];
        Arrays.fill(res, false);

        //to avoid #iterator can't be nested!!!
        for (int i = 0; i < planets.size; i++) {
            Planet p = planets.get(i);

            if (!p.getIsHabitable())
                continue;

            if (onlyColonized && !p.getIsInhabited())
                continue;
            boolean[] exists = p.getAvailableResources();
            for (int r = ResourceTypes.TITAN.getType(); r <= ResourceTypes.DERITIUM.getType(); r++)
                res[r] |= exists[r];
        }
        return res;
    }

    public void setColonyOwner(String owner) {
        colonyOwner = owner;
    }

    public void setHomeOf(String raceId) {
        if (raceId.isEmpty()) {
            homeOf = null;
            return;
        }
        homeOf = raceId;
    }

    public void generateSector(int sunProb, int minorProb) {
        if (isSunSystem())
            return;

        if (((int) (RandUtil.random() * 100)) >= (100 - sunProb)) {
            setSunsystem(true);
            boolean isMinor = ((int) (RandUtil.random() * 100)) >= (100 - minorProb);
            Pair<String, Boolean> p = resourceManager.getStarNameGenerator().getNextRandomSectorName(coordinates,
                    isMinor);
            name = p.getFirst();
            isMinor = p.getSecond();
            if (isMinor) {
                Minor minor = resourceManager.getRaceController().getMinorRace(name);
                homeOf = minor.getRaceId();
                while (true) {
                    float currentInhabs = 0.0f;
                    int random = (int) (RandUtil.random() * 3) + 1;
                    do {
                        createPlanets();
                        currentInhabs = getCurrentInhabitants();
                        if (currentInhabs > 20.0f)
                            break;
                        if (currentInhabs > 0.0f) {
                            float maxInhabs = 0.0f;
                            for (int i = 0; i < planets.size; i++)
                                maxInhabs += planets.get(i).getMaxInhabitants();
                            if (maxInhabs > (40.0f + random * 7))
                                break;
                        }
                    } while (currentInhabs <= (15.0f / random));

                    //ensure every minor race can build type 2 shipyard so all minor ships can be built
                    if (getMaxInhabitants() >= 30.0f || (!minor.getSpaceFlightNation() && getMaxInhabitants() > 15.0f))
                        break;
                }
            } else {
                // no minors in the sector
                createPlanets();
            }
        }
    }

    void createPlanets() {
        createPlanets("");
    }

    private void createMajorRacePlanets(String majorID) {
        FileHandle handle = Gdx.files.internal("data/races/majorplanets.txt");
        String input = handle.readString("ISO-8859-1");
        String[] data = input.split("\n");
        for (int i = 0; i < data.length; i++)
            data[i] = data[i].trim();
        int index = 0;
        while (!data[index].equals(majorID))
            index++;
        index++; //skip name
        starType = StarType.fromInt(Integer.parseInt(data[index++]));
        int numPlanets = Integer.parseInt(data[index++]);
        for (int i = 0; i < numPlanets; i++) {
            Planet p = new Planet();
            p.setName(data[index++]);
            p.setType(PlanetType.fromPlanetClass(Integer.parseInt(data[index++])));
            index++; // 2 is???
            float maxInhabitants = Integer.parseInt(data[index++]) / 1000;
            p.setMaxInhabitants(maxInhabitants);
            float currentInhabitants = Integer.parseInt(data[index++]) / 1000;
            p.setCurrentInhabitants(currentInhabitants);
            index++;
            index++; // 5&6 unknown data
            p.setSize(PlanetSize.fromInt(Integer.parseInt(data[index++])));
            p.setPlanetGrowth();
            p.setGraphicType((int) (RandUtil.random() * PlanetType.GRAPHICNUMBER.getPlanetClass()));
            index++; // 8 is???
            p.setBoni(Integer.parseInt(data[index++]) != 0,
                    Integer.parseInt(data[index++]) != 0,
                    Integer.parseInt(data[index++]) != 0,
                    Integer.parseInt(data[index++]) != 0,
                    Integer.parseInt(data[index++]) != 0,
                    Integer.parseInt(data[index++]) != 0,
                    Integer.parseInt(data[index++]) != 0,
                    Integer.parseInt(data[index++]) != 0);
            p.setStartTerraformPoints(Integer.parseInt(data[index++]));
            p.setHasIndividualGraphic(true);
            planets.add(p);
        }
    }

    public void createPlanets(String majorID) {
        planets.clear();
        if (isSunSystem()) {
            starType = StarType.getNewStarOnChance();
            if (!majorID.isEmpty()) {
                createMajorRacePlanets(majorID);
            } else {
                int sizeCounter = 0;
                int upperLimit = GamePreferences.planetsInStarSystems;
                // three times random, so that the average is more often then too few or too many planets
                int number = (int) ((RandUtil.random() * upperLimit + 1 + RandUtil.random() * upperLimit + 1
                        + RandUtil.random() * upperLimit + 1 + 1) / 3);
                PlanetZone zone = PlanetZone.HOT;
                // what zone should we begin with to place planets?
                int random = (int) (RandUtil.random() * 10);
                if (random == 0)
                    zone = PlanetZone.COOL;
                else if (random < 3)
                    zone = PlanetZone.TEMPERATE;

                for (int i = 0; i < number; i++) {
                    Planet p = new Planet();
                    boolean hasMinor = (getMinorRace() != null);
                    zone = p.Create(name, zone, i, hasMinor);
                    planets.add(p);

                    // don't generate too many large planets as they won't fit into the view
                    if (p.getSize().getSize() <= 1)
                        sizeCounter++;
                    else
                        sizeCounter += 2;
                    if (sizeCounter > 10)
                        break;
                }

            }
        }
    }

    public void letPlanetsGrow() {
        for (int i = 0; i < planets.size; i++)
            planets.get(i).planetGrowth();
    }

    public void letPlanetsShrink(float value) {
        float[] habitants = new float[planets.size];
        Arrays.fill(habitants, 0.0f);
        float allHabitants = 0.0f;
        for (int i = 0; i < planets.size; i++) {
            allHabitants += planets.get(i).getCurrentInhabitants();
            habitants[i] = planets.get(i).getCurrentInhabitants();
        }
        for (int i = 0; i < planets.size; i++) {
            if (habitants[i] > 0) {
                float percent = habitants[i] / allHabitants;
                habitants[i] += percent * value;
                planets.get(i).setCurrentInhabitants(habitants[i]);
                if (planets.get(i).getCurrentInhabitants() <= 0) {
                    planets.get(i).setCurrentInhabitants(0);
                }
            }
        }
    }

    public void recalcPlanetsTerraformingStatus() {
        Array<Planet> terraformable = new Array<Planet>(false, 8);
        //to avoid #iterator can't be nested!!!
        for (int i = 0; i < planets.size; i++) {
            Planet p = planets.get(i);

            p.setIsTerraForming(false);
            if (p.getIsHabitable() && !p.getIsTerraformed())
                terraformable.add(p);
        }
        for (Iterator<Entry<String, ShipMap>> iter = ships.entries().iterator(); iter.hasNext();) {
            Entry<String, ShipMap> e = iter.next();
            for (int i = 0; i < e.value.getSize(); i++) {
                if (terraformable.size == 0)
                    return;
                Ships s = e.value.getAt(i);
                if (s.getCurrentOrder() != ShipOrder.TERRAFORM)
                    continue;
                int planet = s.getTerraformingPlanet();
                Planet p = planets.get(planet);
                if (p.getIsTerraForming() || p.getIsTerraformed())
                    continue;
                p.setIsTerraForming(true);
                terraformable.removeValue(p, true);
            }
        }
    }

    public int countOfTerraformedPlanets() {
        int count = 0;
        //to avoid #iterator can't be nested!!!
        for (int i = 0; i < planets.size; i++) {
            Planet p = planets.get(i);

            if (p.isColonizable())
                ++count;
        }
        return count;
    }

    public void distributeColonists(float colonists) {
        //The population which comes to colonize is distrbuted equally on the planets
        float oddHab = 0.0f; // extra colonists if the planet can't accomodate more
        //iterated terraformed planets and put the population on these
        //to avoid #iterator can't be nested!!!
        for (int i = 0; i < planets.size; i++) {
            Planet p = planets.get(i);

            if (!p.isColonizable())
                continue;
            float maxHab = p.getMaxInhabitants();
            if (colonists > maxHab) {
                oddHab += colonists - maxHab;
                p.setCurrentInhabitants(maxHab);
            } else
                p.setCurrentInhabitants(colonists);
        }

        if (oddHab <= 0.0f)
            return;
        //distribute the remaining colonists on the Planets
        //to avoid #iterator can't be nested!!!
        for (int i = 0; i < planets.size; i++) {
            Planet p = planets.get(i);

            float currentHab = p.getCurrentInhabitants();
            float maxHab = p.getMaxInhabitants();
            if (!p.getIsTerraformed() || currentHab <= 0.0f || currentHab >= maxHab)
                continue;
            float tryNewHab = oddHab + currentHab;
            oddHab -= maxHab - currentHab;
            if (tryNewHab > maxHab)
                p.setCurrentInhabitants(maxHab);
            else {
                p.setCurrentInhabitants(tryNewHab);
                break;
            }
        }
    }

    public void terraforming(Ship ship) {
        int planetNum = ship.getTerraformingPlanet();
        if (planetNum != -1 && planetNum < planets.size)
            planets.get(planetNum).setIsTerraForming(true);
        else
            ship.setTerraform(-1);
    }

    public boolean perhapsMinorExtends(int technologicalProgress) {
        boolean colonized = false;
        //to avoid #iterator can't be nested!!!
        for (int i = 0; i < planets.size; i++) {
            Planet p = planets.get(i);

            // if it's not yet terraformed
            if (!p.getIsTerraformed()) {
                //there is a change that it will be terraformed and colonized
                if ((RandUtil.random() * 200) >= (200 - technologicalProgress + 1)) {
                    colonized = true;
                    p.setNeededTerraformPoints(p.getNeededTerraformPoints());
                    p.setIsTerraForming(false);
                    if (p.getMaxInhabitants() < 1.0f)
                        p.setCurrentInhabitants(p.getMaxInhabitants());
                    else
                        p.setCurrentInhabitants(1.0f);
                }
            } else if (!p.getIsInhabited() && p.getIsTerraformed()) { // if it's already terraformed
                //it has a greater chance of colonizing
                if ((RandUtil.random() * 200) >= (200 - 3 * (technologicalProgress + 1))) {
                    colonized = true;
                    if (p.getMaxInhabitants() < 1.0f)
                        p.setCurrentInhabitants(p.getMaxInhabitants());
                    else
                        p.setCurrentInhabitants(1.0f);
                }
            }
        }
        return colonized;
    }

    public void createDeritiumForSpaceflightMinor() {
        boolean[] res = getAvailableResources(true);
        if (!res[ResourceTypes.DERITIUM.getType()]) {
            //to avoid #iterator can't be nested!!!
            for (int i = 0; i < planets.size; i++) {
                Planet p = planets.get(i);

                if (p.getIsInhabited()) {
                    p.setBoni(ResourceTypes.DERITIUM.getType(), true);
                    break;
                }
            }
        }
    }

    public boolean terraform(Ships ship) {
        return planets.get(ship.getTerraformingPlanet()).setNeededTerraformPoints(ship.getColonizePoints());
    }

    public void onTerraformPossibleMinor(Major major) {
        if (getMinorRace() == null)
            return;
        Minor minor = getMinorRace();
        if ((!minor.isMemberTo() || minor.isMemberTo(major.getRaceId())) && !minor.isSubjugated())
            minor.setRelation(major.getRaceId(), (int) (RandUtil.random() * 11));
    }

    public String systemEventPlanetMovement() {
        String message = "";
        int sz = planets.size;
        int planetNum = (int) (RandUtil.random() * sz);
        while (!planets.get(planetNum).getIsHabitable())
            planetNum = (int) (RandUtil.random() * sz);
        Planet p = planets.get(planetNum);
        float oldHabitants = p.getMaxInhabitants();
        float habitantsChange = oldHabitants * (((int) (RandUtil.random() * 11)) / 10.0f)
                * ((int) (RandUtil.random() * 2) == 1 ? 1 : -1);
        float newHabitants = Math.min(Math.max(oldHabitants + habitantsChange, 1), 100);
        if (oldHabitants != newHabitants) {
            p.setMaxInhabitants(newHabitants);
            String tmp = String.format("%.3f", habitantsChange);
            message = StringDB.getString("SYSTEMEVENTPLANETMOVEMENT", false, p.getPlanetName(), tmp);
        }
        return message;
    }

    public String systemEventDemographic(Major major) {
        String message = "";
        int sz = planets.size;
        for (int i = 0; i < 100; ++i) {
            Planet p = planets.get((int) (RandUtil.random() * sz));
            if (p.getIsHabitable() && p.getCurrentInhabitants() > 1) {
                float oldHabitants = p.getCurrentInhabitants();
                float habitantsChange = oldHabitants * (float) RandUtil.random() * (-1);
                float newHabitants = Math.max(oldHabitants + habitantsChange, 1);
                if (oldHabitants != newHabitants) {
                    p.setCurrentInhabitants(newHabitants);
                    String tmp = String.format("%.3f", habitantsChange * (-1));
                    message = StringDB.getString("SYSTEMEVENTPLANETDEMOGRAPHICLONG", false, p.getPlanetName(), tmp);
                    if (major.isHumanPlayer()) {
                        resourceManager.getClientWorker().setEmpireViewFor(major);
                        EventRandom event = new EventRandom(resourceManager, "demographic",
                                StringDB.getString("SYSTEMEVENTPLANETDEMOGRAPHICTITLE"), message);
                        major.getEmpire().pushEvent(event);
                    }
                }
                break;
            }
        }
        return message;
    }

    public int getCompareValue(String raceID) {
        int value = 0;
        boolean hasTerraformable = hasTerraformable(raceID);
        boolean isColonizable = isColonizable(raceID);
        double inhabitants = getCurrentInhabitants();
        double maxCurrentInhabitants = getCurrentMaxInhabitants();
        int maxInhabitantsMod = (int) (getMaxInhabitants() - maxCurrentInhabitants);
        if(isColonizable || hasTerraformable) {
            value += maxInhabitantsMod;
            if(isFree()) //systems outside our territory should be valued a bit higher as they help more in expanding the territory
                value++;
        }
        if (isColonizable)
            value++;
        if (isColonizable && !hasTerraformable) //all terraformed so it's a clear colonization target
            value += 5;
        if (hasTerraformable && (inhabitants / maxCurrentInhabitants > 0.9f)) //prioritize systems that are near the current population limit
            value += 50;
        if (isColonizable && (inhabitants > 0)) //highest priority to systems which have terraformed planets and are already colonized
            value += 100;
        return value;
    }

    @Override
    public void write(Kryo kryo, Output output) {
        super.write(kryo, output);
        output.writeString(colonyOwner);
        output.writeString(homeOf);
        if (isSunSystem()) {
            kryo.writeObject(output, starType);
            kryo.writeObject(output, planets);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public void read(Kryo kryo, Input input) {
        super.read(kryo, input);
        colonyOwner = input.readString();
        homeOf = input.readString();
        if (isSunSystem()) {
            starType = kryo.readObject(input, StarType.class);
            planets = kryo.readObject(input, Array.class);
        }
    }
}
