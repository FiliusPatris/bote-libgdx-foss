/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.galaxy;

import com.badlogic.gdx.utils.ArrayMap;
import com.blotunga.bote.constants.PlanetSize;
import com.blotunga.bote.constants.PlanetType;
import com.blotunga.bote.constants.PlanetZone;
import com.blotunga.bote.constants.ResourceTypes;
import com.blotunga.bote.ships.ShipMap;
import com.blotunga.bote.ships.Ships;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.utils.RandUtil;

/**
 * @author dragon
 * @version 1
 */
public class Planet {
    private PlanetSize size;				///< size of the planet
    private int graphicType;				///< graphic number of the planet
    private boolean hasIndividualGraphic;	///< does the planet have an individual graphic (tipically major race home planets)
    private PlanetType type;				///< type of the planet
    private float maxInhabitants;			///< maximum number of inhabitants
    private float currentInhabitants;		///< current number of inhabitants
    private float growth;					///< the growth percentage of the population (ie: 0.2%)
    private boolean isTerraforming;			///< is the planet currently terraformed?
    private String name;					///< name of the planet
    private int neededTerraformPoints;		///< number of terraform points needed to terraform the planet
    private int startTerraformPoints;		///< number of terraform points before starting terraforming (needed to show percentage)
    private boolean[] bonuses;					///< is there some bonus resource on the planet

    public Planet() {
        bonuses = new boolean[8];
        reset();
    }

    public void reset() {
        size = PlanetSize.NORMAL;
        graphicType = 0;
        hasIndividualGraphic = false;
        type = PlanetType.PLANETCLASS_I;
        maxInhabitants = 0.0f;
        currentInhabitants = 0.0f;
        growth = 0.0f;
        isTerraforming = false;
        name = "";
        neededTerraformPoints = 0;
        startTerraformPoints = 0;
        for (int i = 0; i < bonuses.length; i++)
            bonuses[i] = false;

    }

    public PlanetSize getSize() {
        return size;
    }

    public PlanetType getPlanetType() {
        return type;
    }

    public float getMaxInhabitants() {
        return maxInhabitants;
    }

    public float getCurrentInhabitants() {
        return currentInhabitants;
    }

    public String getPlanetClass() {
        return type.getTypeName();
    }

    public String getPlanetName() {
        return name;
    }

    public float getPlanetGrowth() {
        return growth;
    }

    public boolean getIsTerraformed() {
        return getIsHabitable() && (neededTerraformPoints == 0);
    }

    public boolean getIsTerraForming() {
        return isTerraforming;
    }

    public int calcTerraformRoundsRemaining(StarSystem system) {
        ArrayMap<String, ShipMap> ships = system.getShipsInSector();
        int terraformPoints = 0;
        for (int i = 0; i < ships.size; i++) {
            ShipMap sm = ships.getValueAt(i);
            for (int j = 0; j < sm.getSize(); j++) {
                Ships sh = sm.getAt(j);
                if (sh.getTerraformingPlanet() != -1 && system.getPlanet(sh.getTerraformingPlanet()).name.equals(name)) {
                    terraformPoints += sh.getColonizePoints();
                    for (int k = 0; k < sh.getFleetSize(); k++)
                        terraformPoints += sh.getFleet().getAt(k).getColonizePoints();
                }
            }
        }

        return terraformPoints != 0 ? 1 + (neededTerraformPoints - 1) / terraformPoints : 0;
    }

    public boolean getIsHabitable() {
        return maxInhabitants > 0;
    }

    public boolean getIsInhabited() {
        return currentInhabitants > 0;
    }

    public boolean isColonizable() {
        return getIsTerraformed() && !getIsInhabited();
    }

    public int getNeededTerraformPoints() {
        return neededTerraformPoints;
    }

    public int getStartTerraformPoints() {
        return startTerraformPoints;
    }

    public boolean[] getBonuses() {
        return bonuses;
    }

    public boolean hasIndividualGraphic() {
        return hasIndividualGraphic;
    }

    /**
     * Function returns the file name of the planet's graphic.
     * This is generated from the number of the graphic and the class of the planet.
     * @return the name of the graphic file inside the texture pack
     */
    private String getGraphicFile() {
        return "class" + type.getTypeName() + String.format("%02d", graphicType);
    }

    /**
     * Returns the name of the texture in the texture pack for the planet
     * @return the name of the graphic file inside the texture pack
     */
    public String getPlanetGraphicFile() {
        if (hasIndividualGraphic)
            return name;
        else
            return getGraphicFile();
    }

    public void setSize(PlanetSize size) {
        this.size = size;
    }

    public void setMaxInhabitants(float maxInhabitants) {
        this.maxInhabitants = maxInhabitants;
    }

    public void setCurrentInhabitants(float currentInhabitants) {
        this.currentInhabitants = currentInhabitants;
    }

    public void setType(PlanetType type) {
        this.type = type;
    }

    public void setGraphicType(int graphicType) {
        this.graphicType = graphicType;
    }

    public void setHasIndividualGraphic(boolean hasIndividualGraphic) {
        this.hasIndividualGraphic = hasIndividualGraphic;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPlanetGrowth() {
        growth = maxInhabitants / 4;
    }

    /**
     * Substracts the argument from the terraformpoints and if it reaches 0 the planet is
     * considered terraformed and returns true. If the planet's terraforming is not done,
     * then isTerraForming is set to true
     * @param sub
     * @return true if terraforming is complete, false otherwise
     */
    public boolean setNeededTerraformPoints(int sub) {
        int result = neededTerraformPoints - sub;
        neededTerraformPoints = Math.max(result, 0);
        if (neededTerraformPoints == 0) {
            isTerraforming = false;
            return true;
        }
        isTerraforming = true;
        return false;
    }

    public void setStartTerraformPoints() {
        setStartTerraformPoints(0);
    }

    public void setStartTerraformPoints(int startTerraformPoints) {
        this.startTerraformPoints = neededTerraformPoints = startTerraformPoints;
    }

    public void setIsTerraForming(boolean is) {
        isTerraforming = is;
    }

    public void setBoni(boolean titan, boolean deuterium, boolean duranium, boolean crystal, boolean iridium,
            boolean deritium, boolean food, boolean energy) {
        bonuses[ResourceTypes.TITAN.getType()] = titan;
        bonuses[ResourceTypes.DEUTERIUM.getType()] = deuterium;
        bonuses[ResourceTypes.DURANIUM.getType()] = duranium;
        bonuses[ResourceTypes.CRYSTAL.getType()] = crystal;
        bonuses[ResourceTypes.IRIDIUM.getType()] = iridium;
        bonuses[ResourceTypes.DERITIUM.getType()] = deritium;
        bonuses[ResourceTypes.FOOD.getType()] = food;
        bonuses[7] = energy;
    }

    public void setBoni(int res, boolean is) {
        bonuses[res] = is;
    }

    /**
     * Function creates a planet
     * @param sectorName
     * @param lastZone zone of the last created planet as the new planet can't be in the same zone
     * @param planetNumber number of planets already created in this sector
     * @param minor minor race in the sector
     * @return zone of the created planet
     */
    public PlanetZone Create(String sectorName, PlanetZone lastZone, int planetNumber, boolean minor) {
        reset();
        PlanetType randomType = PlanetType.PLANETCLASS_M;
        PlanetZone zone = lastZone; // zone where to put the planet
        // planet class is determined by zone and the number of previous planets

        // All classes can appear, but with different probabilities
        if (lastZone == PlanetZone.HOT) {
            int whatPlanet = (int) Math.floor(RandUtil.random() * 75); // random number from 0 to 74
            // planets from the cool zone
            zone = PlanetZone.COOL;
            if (whatPlanet == 0)
                randomType = PlanetType.PLANETCLASS_I;
            else if (whatPlanet == 1)
                randomType = PlanetType.PLANETCLASS_J;
            else if (whatPlanet == 2)
                randomType = PlanetType.PLANETCLASS_S;
            else if (whatPlanet == 3)
                randomType = PlanetType.PLANETCLASS_T;
            else if (whatPlanet == 4)
                randomType = PlanetType.PLANETCLASS_A;
            else if (whatPlanet == 5)
                randomType = PlanetType.PLANETCLASS_C;
            else if (whatPlanet == 6)
                randomType = PlanetType.PLANETCLASS_P;
            else {
                // planets from the temperate zone
                zone = PlanetZone.TEMPERATE;
                if (whatPlanet > 6)
                    randomType = PlanetType.PLANETCLASS_E;
                if (whatPlanet > 11)
                    randomType = PlanetType.PLANETCLASS_F;
                if (whatPlanet > 16)
                    randomType = PlanetType.PLANETCLASS_G;
                if (whatPlanet > 21)
                    randomType = PlanetType.PLANETCLASS_K;
                if (whatPlanet > 26)
                    randomType = PlanetType.PLANETCLASS_L;
                if (whatPlanet > 31)
                    randomType = PlanetType.PLANETCLASS_M; // rarer
                if (whatPlanet > 35)
                    randomType = PlanetType.PLANETCLASS_O;
                if (whatPlanet > 40)
                    randomType = PlanetType.PLANETCLASS_Q; // rare
                if (whatPlanet > 43)
                    randomType = PlanetType.PLANETCLASS_R; // rare
                if (whatPlanet > 46) {
                    randomType = PlanetType.PLANETCLASS_H;
                    zone = PlanetZone.HOT;
                }
                if (whatPlanet > 53)
                    randomType = PlanetType.PLANETCLASS_B;
                if (whatPlanet > 60)
                    randomType = PlanetType.PLANETCLASS_N;
                if (whatPlanet > 67)
                    randomType = PlanetType.PLANETCLASS_Y;
            }
        } else if (lastZone == PlanetZone.TEMPERATE) {
            int whatPlanet = (int) Math.floor(RandUtil.random() * 47);
            // planets from the cool zone
            zone = PlanetZone.COOL;
            if (whatPlanet == 0)
                randomType = PlanetType.PLANETCLASS_I;
            else if (whatPlanet == 1)
                randomType = PlanetType.PLANETCLASS_J;
            else if (whatPlanet == 2)
                randomType = PlanetType.PLANETCLASS_S;
            else if (whatPlanet == 3)
                randomType = PlanetType.PLANETCLASS_T;
            else if (whatPlanet == 4)
                randomType = PlanetType.PLANETCLASS_A;
            else if (whatPlanet == 5)
                randomType = PlanetType.PLANETCLASS_C;
            else if (whatPlanet == 6)
                randomType = PlanetType.PLANETCLASS_P;
            else {
                zone = PlanetZone.TEMPERATE;
                if (whatPlanet > 6)
                    randomType = PlanetType.PLANETCLASS_E;
                if (whatPlanet > 11)
                    randomType = PlanetType.PLANETCLASS_F;
                if (whatPlanet > 16)
                    randomType = PlanetType.PLANETCLASS_G;
                if (whatPlanet > 21)
                    randomType = PlanetType.PLANETCLASS_K;
                if (whatPlanet > 26)
                    randomType = PlanetType.PLANETCLASS_L;
                if (whatPlanet > 31)
                    randomType = PlanetType.PLANETCLASS_M; // rarer
                if (whatPlanet > 35)
                    randomType = PlanetType.PLANETCLASS_O;
                if (whatPlanet > 40)
                    randomType = PlanetType.PLANETCLASS_Q; // rare
                if (whatPlanet > 43)
                    randomType = PlanetType.PLANETCLASS_R; // rare
            }
        } else if (lastZone == PlanetZone.COOL) {
            zone = PlanetZone.COOL;
            int whatPlanet = (int) Math.floor(RandUtil.random() * 18);
            if (whatPlanet == 0)
                randomType = PlanetType.PLANETCLASS_I;
            else if (whatPlanet == 1)
                randomType = PlanetType.PLANETCLASS_J;
            else if (whatPlanet == 2)
                randomType = PlanetType.PLANETCLASS_S;
            else if (whatPlanet == 3)
                randomType = PlanetType.PLANETCLASS_T;
            else {
                if (whatPlanet > 4)
                    randomType = PlanetType.PLANETCLASS_A;
                if (whatPlanet > 8)
                    randomType = PlanetType.PLANETCLASS_C;
                if (whatPlanet > 12)
                    randomType = PlanetType.PLANETCLASS_P;
                if (whatPlanet > 17)
                    randomType = PlanetType.PLANETCLASS_R;
            }
        }

        graphicType = (int) (RandUtil.random() * PlanetType.GRAPHICNUMBER.getPlanetClass());
        size = PlanetSize.getRandomSize();
        boolean habitable = true;
        // check if planet is indeed habitable
        if (randomType == PlanetType.PLANETCLASS_A || randomType == PlanetType.PLANETCLASS_B
                || randomType == PlanetType.PLANETCLASS_E || randomType == PlanetType.PLANETCLASS_I
                || randomType == PlanetType.PLANETCLASS_J || randomType == PlanetType.PLANETCLASS_S
                || randomType == PlanetType.PLANETCLASS_T || randomType == PlanetType.PLANETCLASS_Y)
            habitable = false;

        if (randomType == PlanetType.PLANETCLASS_I || randomType == PlanetType.PLANETCLASS_J
                || randomType == PlanetType.PLANETCLASS_S || randomType == PlanetType.PLANETCLASS_T)
            size = PlanetSize.GIANT; // gas giants are always giants (duh)

        if (!habitable) {
            maxInhabitants = 0.0f;
        } else {
            if (randomType.getPlanetClass() < PlanetType.PLANETCLASS_A.getPlanetClass()) { // planets over Class A can't be colonized
                float multi = 0.0f; // multiplier 0.8, 1.0, 1.2
                int multirand = (int) (RandUtil.random() * 3);
                if (multirand == 0)
                    multi = 0.8f;
                else if (multirand == 1)
                    multi = 1.0f;
                else if (multirand == 2)
                    multi = 1.2f;

                // random number dependent on size and type which will be added
                int random = (int) (RandUtil.random() * ((size.getSize() + 1) * (12 - randomType.getPlanetClass())) / 6 + 1);
                // max inhabitants are calculated including a random component
                float inHabitants = ((((size.getSize() + 1) * (12 - randomType.getPlanetClass()) + random) * 1000)
                        * multi * 0.5f);
                // NOTE: might need revising
                maxInhabitants = inHabitants / 1000;

                // calculate the needed terraformingpoints, the better the planet class, the less we need
                // M type planets need none since they are already terraformed
                neededTerraformPoints = (int) (randomType.getPlanetClass() * (size.getSize() + 1)
                        * (RandUtil.random() * 11 + 5) / 10);
                if (neededTerraformPoints == 0 && randomType != PlanetType.PLANETCLASS_M)
                    neededTerraformPoints++;
                startTerraformPoints = neededTerraformPoints;
            } else {
                maxInhabitants = 0;
            }
        }

        // if a minor race is present, a couple of plantes are set to terraformed
        if (minor && habitable) {
            int random;
            random = (int) (RandUtil.random() * 5) + 1;
            if (random >= 5) {
                neededTerraformPoints = 0;
                startTerraformPoints = 0;
            }
        }
        // if a minor race is present, generate some population
        if (minor && getIsTerraformed()) {
            int randomDiv;
            randomDiv = (int) (RandUtil.random() * 8) + 1;
            currentInhabitants = maxInhabitants / randomDiv;
        }

        type = randomType;
        setPlanetGrowth();
        name = sectorName + " " + (planetNumber + 1);

        generateBoni();

        return zone;
    }

    private void generateBoni() {
        // probability matrix for planet types and resources
        int[][] probabilities = {
                //
                //      TITAN   DEUTERIUM  DURANIUM  CRYSTAL   IRIDIUM  DERITIUM	FOOD      ENERGY
                //+-----------------------------------------------------------------------------------+
                /*M*/{    0,        10,       0,        0,        0,       11,       50,        0,  },
                /*O*/{    0,        50,       0,        0,        0,        0,       25,        0,  },
                /*L*/{    0,        5,        0,        0,        0,        0,       10,        0,  },
                /*P*/{    5,        0,        0,        5,        5,       15,        0,        50, },
                /*H*/{    0,        0,        0,        0,        50,       0,        0,        50, },
                /*Q*/{    0,        0,        0,        50,       0,       15,        5,        0,  },
                /*K*/{    5,        0,        5,        0,        0,       15,        0,        0,  },
                /*G*/{    10,       0,        10,       25,       0,       20,        0,        0,  },
                /*R*/{    0,        0,        50,       0,        0,       15,        0,        0,  },
                /*F*/{    25,       0,        25,       0,        0,       35,        0,        0,  },
                /*C*/{    50,       0,        0,        0,        25,      60,        0,        0,  },
                /*N*/{    0,        25,       0,        0,        0,       60,        0,        50, },
                /*A*/{    0,        0,        0,        0,        0,        0,        0,        0,  },
                /*B*/{    0,        0,        0,        0,        0,        0,        0,        0,  },
                /*E*/{    0,        0,        0,        0,        0,        0,        0,        0,  },
                /*Y*/{    0,        0,        0,        0,        0,        0,        0,        0,  },
                /*I*/{    0,        0,        0,        0,        0,        0,        0,        0,  },
                /*J*/{    0,        0,        0,        0,        0,        0,        0,        0,  },
                /*S*/{    0,        0,        0,        0,        0,        0,        0,        0,  },
                /*T*/{    0,        0,        0,        0,        0,        0,        0,        0   }
        };
        for (int i = 0; i < 8; i++) {
            if ((int) (RandUtil.random() * 100) >= (100 - probabilities[type.getPlanetClass()][i]))
                bonuses[i] = true;
            else
                bonuses[i] = false;
        }
    }

    public void planetGrowth() {
        float tempCurrentInhabitants = currentInhabitants;
        if (currentInhabitants < maxInhabitants)
            currentInhabitants = currentInhabitants + currentInhabitants * growth / 100;
        if ((currentInhabitants < (tempCurrentInhabitants + 0.1f)) && getIsInhabited())
            currentInhabitants = tempCurrentInhabitants + 0.1f; // there should be always at least some growth
        if (currentInhabitants > maxInhabitants)
            currentInhabitants = maxInhabitants;
    }

    public boolean[] getAvailableResources() {
        boolean[] res = new boolean[ResourceTypes.DERITIUM.getType() + 1];
        for (int i = 0; i < res.length; i++)
            res[i] = false;

        // kept = false values only for a better overview
        if (type == PlanetType.PLANETCLASS_C) {
            res[ResourceTypes.TITAN.getType()] = true;
            res[ResourceTypes.DEUTERIUM.getType()] = false;
            res[ResourceTypes.DURANIUM.getType()] = false;
            res[ResourceTypes.CRYSTAL.getType()] = false;
            res[ResourceTypes.IRIDIUM.getType()] = true;
        } else if (type == PlanetType.PLANETCLASS_F) {
            res[ResourceTypes.TITAN.getType()] = true;
            res[ResourceTypes.DEUTERIUM.getType()] = false;
            res[ResourceTypes.DURANIUM.getType()] = true;
            res[ResourceTypes.CRYSTAL.getType()] = false;
            res[ResourceTypes.IRIDIUM.getType()] = false;
        } else if (type == PlanetType.PLANETCLASS_G) {
            res[ResourceTypes.TITAN.getType()] = true;
            res[ResourceTypes.DEUTERIUM.getType()] = false;
            res[ResourceTypes.DURANIUM.getType()] = true;
            res[ResourceTypes.CRYSTAL.getType()] = true;
            res[ResourceTypes.IRIDIUM.getType()] = false;
        } else if (type == PlanetType.PLANETCLASS_H) {
            res[ResourceTypes.TITAN.getType()] = false;
            res[ResourceTypes.DEUTERIUM.getType()] = false;
            res[ResourceTypes.DURANIUM.getType()] = false;
            res[ResourceTypes.CRYSTAL.getType()] = false;
            res[ResourceTypes.IRIDIUM.getType()] = true;
        } else if (type == PlanetType.PLANETCLASS_K) {
            res[ResourceTypes.TITAN.getType()] = true;
            res[ResourceTypes.DEUTERIUM.getType()] = false;
            res[ResourceTypes.DURANIUM.getType()] = true;
            res[ResourceTypes.CRYSTAL.getType()] = false;
            res[ResourceTypes.IRIDIUM.getType()] = false;
        } else if (type == PlanetType.PLANETCLASS_L) {
            res[ResourceTypes.TITAN.getType()] = true;
            res[ResourceTypes.DEUTERIUM.getType()] = true;
            res[ResourceTypes.DURANIUM.getType()] = false;
            res[ResourceTypes.CRYSTAL.getType()] = false;
            res[ResourceTypes.IRIDIUM.getType()] = false;
        } else if (type == PlanetType.PLANETCLASS_M) {
            res[ResourceTypes.TITAN.getType()] = true;
            res[ResourceTypes.DEUTERIUM.getType()] = true;
            res[ResourceTypes.DURANIUM.getType()] = true;
            res[ResourceTypes.CRYSTAL.getType()] = true;
            res[ResourceTypes.IRIDIUM.getType()] = true;
        } else if (type == PlanetType.PLANETCLASS_N) {
            res[ResourceTypes.TITAN.getType()] = false;
            res[ResourceTypes.DEUTERIUM.getType()] = true;
            res[ResourceTypes.DURANIUM.getType()] = false;
            res[ResourceTypes.CRYSTAL.getType()] = false;
            res[ResourceTypes.IRIDIUM.getType()] = false;
        } else if (type == PlanetType.PLANETCLASS_O) {
            res[ResourceTypes.TITAN.getType()] = false;
            res[ResourceTypes.DEUTERIUM.getType()] = true;
            res[ResourceTypes.DURANIUM.getType()] = false;
            res[ResourceTypes.CRYSTAL.getType()] = false;
            res[ResourceTypes.IRIDIUM.getType()] = false;
        } else if (type == PlanetType.PLANETCLASS_P) {
            res[ResourceTypes.TITAN.getType()] = true;
            res[ResourceTypes.DEUTERIUM.getType()] = false;
            res[ResourceTypes.DURANIUM.getType()] = false;
            res[ResourceTypes.CRYSTAL.getType()] = true;
            res[ResourceTypes.IRIDIUM.getType()] = true;
        } else if (type == PlanetType.PLANETCLASS_Q) {
            res[ResourceTypes.TITAN.getType()] = false;
            res[ResourceTypes.DEUTERIUM.getType()] = false;
            res[ResourceTypes.DURANIUM.getType()] = false;
            res[ResourceTypes.CRYSTAL.getType()] = true;
            res[ResourceTypes.IRIDIUM.getType()] = false;
        } else if (type == PlanetType.PLANETCLASS_R) {
            res[ResourceTypes.TITAN.getType()] = false;
            res[ResourceTypes.DEUTERIUM.getType()] = false;
            res[ResourceTypes.DURANIUM.getType()] = true;
            res[ResourceTypes.CRYSTAL.getType()] = false;
            res[ResourceTypes.IRIDIUM.getType()] = false;
        }

        if (getBonuses()[ResourceTypes.DERITIUM.getType()])
            res[ResourceTypes.DERITIUM.getType()] = true;
        return res;
    }
}
