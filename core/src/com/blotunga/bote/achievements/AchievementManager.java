/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.achievements;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.AlphaAction;
import com.badlogic.gdx.scenes.scene2d.actions.MoveByAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;
import com.badlogic.gdx.utils.viewport.ScalingViewport;
import com.blotunga.bote.GamePreferences;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.constants.GameConstants;

public class AchievementManager implements Disposable {
    private ResourceManager game;
    private boolean initialized;
    private Array<Achievement> scheduledAchievements;
    private Stage stage;

    public AchievementManager(ResourceManager game) {
        this.game = game;
        initialized = false;
        scheduledAchievements = new Array<Achievement>();
    }

    public void initialize() {
        initialized = true;
        Gdx.app.postRunnable(new Runnable() {

            @Override
            public void run() {
                final OrthographicCamera camera = new OrthographicCamera();
                stage = new Stage(new ScalingViewport(Scaling.fit, GamePreferences.sceneWidth, GamePreferences.sceneHeight, camera));
            }
        });
    }

    public boolean isInitialized() {
        return initialized;
    }

    public void unlockAchievement(Achievement achievement) {
        if (!game.getGamePreferences().achievementsEnabled)
            return;
        if (!achievement.getType().isUnlocked(game.getGameSettings().achievements[achievement.getType().ordinal()])) {
            game.getGameSettings().achievements[achievement.getType().ordinal()] = achievement.getType().getValue();
            scheduleAchievement(achievement);
        }
        game.getPlatformApiIntegration().unlockAchievement(achievement.getType());
    }

    public void incrementAchievement(Achievement achievement, int value) {
        if (!game.getGamePreferences().achievementsEnabled)
            return;
        if (!achievement.getType().isUnlocked(game.getGameSettings().achievements[achievement.getType().ordinal()])) {
            game.getGameSettings().achievements[achievement.getType().ordinal()] += value;
            if (achievement.getType().isUnlocked(value))
                scheduleAchievement(achievement);
        }
        game.getPlatformApiIntegration().incrementAchievement(achievement.getType(), value);
    }

    private void scheduleAchievement(Achievement achievement) {
        scheduledAchievements.add(achievement);
    }

    public void popupAchievements() {
        for (int i = 0; i < scheduledAchievements.size; i++) {
            final Achievement achievement = scheduledAchievements.get(i);
            Rectangle rect = GameConstants.coordsToRelative(250, 100, 940, 100);
            final Table container = new Table();
            Sprite sp = new Sprite(new TextureRegion(game.loadTextureImmediate(GameConstants.UI_BG_SIMPLE)));
            sp.setColor(new Color(0.0f, 0.0f, 0.0f, 0.8f));
            container.setBackground(new SpriteDrawable(sp));
            container.setBounds(rect.x, rect.y, rect.width, rect.height);
            float itemHeight = GameConstants.wToRelative(50);
            Skin skin = game.getSkin();
            Color normalColor = game.getRaceController().getPlayerRace().getRaceDesign().clrSecondText;
            Color color = game.getRaceController().getPlayerRace().getRaceDesign().clrNormalText;
            container.clear();
            Label label = new Label("Achievement unlocked!", skin, "hugeFont", normalColor);
            container.add(label);
            container.row();
            Table table = new Table();
            Image image = new Image(game.loadTextureImmediate(achievement.getType().getImgPath()));
            table.add(image).height(itemHeight).width(itemHeight);
            label = new Label(achievement.getName(), skin, "xlFont", color);
            table.add(label).width(GameConstants.wToRelative(300)).spaceLeft(GameConstants.wToRelative(10));
            label = new Label(achievement.getDescription(), skin, "xlFont", color);
            label.setWrap(true);
            table.add(label).width(GameConstants.wToRelative(580));
            container.add(table);
            final float duration = 5f;
            Task startTask = new Task() {
                @Override
                public void run() {
                    stage.addActor(container);
                    AlphaAction actionFadeOut = new AlphaAction();
                    actionFadeOut.setActor(container);
                    actionFadeOut.setAlpha(0.0f);
                    actionFadeOut.setDuration(duration + 1);
                    MoveByAction actionMoveUp = new MoveByAction();
                    actionMoveUp.setActor(container);
                    actionMoveUp.setAmountY(GameConstants.hToRelative(150));
                    actionMoveUp.setDuration(duration);
                    container.addAction(actionFadeOut);
                    container.addAction(actionMoveUp);
                }
            };
            Timer.schedule(startTask, duration * i);
            Task clearTask = new Task() {
                @Override
                public void run() {
                    container.remove();
                    Gdx.graphics.requestRendering();
                }
            };
            Timer.schedule(clearTask, duration * (i + 1));
        }
        scheduledAchievements.clear();
    }

    public void updateSize(int screenWidth, int screenHeight, boolean centerCamera) {
        if (stage != null)
            stage.getViewport().update(screenWidth, screenHeight, centerCamera);
    }

    public void render() {
        if (stage != null) {
            stage.act(1 / 30.0f);
            stage.draw();
        }
    }

    @Override
    public void dispose() {
        if (stage != null) {
            stage.clear();
            stage.dispose();
            stage = null;
        }
    }
}
