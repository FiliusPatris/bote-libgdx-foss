/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.achievements;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Json;

public enum AchievementsList {
    achievementPeaceToTheGalaxy("CgkIge37yogUEAIQAg","major1", 1),
    achievementSupremeInc("CgkIge37yogUEAIQAw", "major2", 1),
    achievementTrueWarriors("CgkIge37yogUEAIQBA", "major3", 1),
    achievementWeWatchEverything("CgkIge37yogUEAIQBQ", "major4", 1),
    achievementKneelBeforeUs("CgkIge37yogUEAIQBg", "major5", 1),
    achievementAllHaveBeenEliminated("CgkIge37yogUEAIQBw", "major6", 1),
    achievementYouHaveToStartSomewhere("CgkIge37yogUEAIQCA", "easy", 1),
    achievementICanDoThis("CgkIge37yogUEAIQCQ", "normal",1 ),
    achievementItsGettingHarder("CgkIge37yogUEAIQCg", "hard", 1),
    achievementDoesThisGetAnyHarder("CgkIge37yogUEAIQCw", "veryhard", 1),
    achievementGodlike("CgkIge37yogUEAIQDA", "godlike", 1),
    achievementVanquisher("CgkIge37yogUEAIQDQ", "vanquisher", 1),
    achievementConqueror("CgkIge37yogUEAIQDg", "conqueror", 1),
    achievementGrandAdmiral("CgkIge37yogUEAIQDw", "grand-admiral", 1),
    achievementPeacemaker("CgkIge37yogUEAIQEA", "peacemaker", 1),
    achievementTechnomage("CgkIge37yogUEAIQEQ", "technomage", 1),
    achievementShadow("CgkIge37yogUEAIQEg", "shadow", 1),
    achievementTerraNova("CgkIge37yogUEAIQEw", "terranova", 1),
    achievementColonizer("CgkIge37yogUEAIQFA", "colonizer", 1000),
    achievementGenesis("CgkIge37yogUEAIQFQ", "genesis", 1),
    achievementToBoldlyGo("CgkIge37yogUEAIQFg", "toboldlygo", 1),
    achievementExplorer("CgkIge37yogUEAIQFw", "explorer", 10000),
    achievementFoothold("CgkIge37yogUEAIQGA", "foothold", 1),
    achievementShipwright("CgkIge37yogUEAIQGQ", "shipwright", 10000),
    achievementWhenDiplomacyFails("CgkIge37yogUEAIQJQ", "whendiplofails", 1),
    achievementGunsBlazing("CgkIge37yogUEAIQJg", "gunsblazing", 1),
    achievementFirstContact("CgkIge37yogUEAIQJw", "firstcontact", 1),
    achievementRockefeller("CgkIge37yogUEAIQKg", "rockefeller", 1),
    achievementCroesus("CgkIge37yogUEAIQKQ", "croesus", 1, true),
    achievementLostInSpace("CgkIge37yogUEAIQKA", "lostinspace", 1, true),
    achievementPasteur("CgkIge37yogUEAIQGg", "pasteur", 1),
    achievementTesla("CgkIge37yogUEAIQGw", "tesla", 1),
    achievementVonNeumann("CgkIge37yogUEAIQHA", "vonneumann", 1),
    achievementBessemer("CgkIge37yogUEAIQHQ", "bessemer", 1),
    achievementVonBraun("CgkIge37yogUEAIQHg", "vonbraun", 1),
    achievementOppenheimer("CgkIge37yogUEAIQHw", "oppenheimer", 1),
    achievementGalacticBarony("CgkIge37yogUEAIQIA", "barony", 1),
    achievementGalacticDuchy("CgkIge37yogUEAIQIQ", "duchy", 1),
    achievementGalacticKingdom("CgkIge37yogUEAIQIg", "kingdom", 1),
    achievementGalacticRepublic("CgkIge37yogUEAIQIw", "republic", 1),
    achievementGalacticEmpire("CgkIge37yogUEAIQJA", "empire", 1);

    private String id;
    private String imgName;
    private int valueNeeded;
    private boolean hidden;
    private static Achievement[] achievementNameList = null;

    AchievementsList(String id, String imgName, int valueNeeded) {
        this(id, imgName, valueNeeded, false);
    }

    AchievementsList(String id, String imgName, int valueNeeded, boolean hidden) {
        this.id = id;
        this.imgName = imgName;
        this.valueNeeded = valueNeeded;
        this.hidden = hidden;
    }

    public String getId() {
        return id;
    }

    public int getValue() {
        return valueNeeded;
    }

    public boolean isHidden(){
        return hidden;
    }

    public String getImgPath() {
        return "graphics/achievements/" + imgName + ".png";
    }

    public boolean isUnlocked(int value) {
        return value >= valueNeeded;
    }

    public String getName() {
        return achievementNameList[this.ordinal()].getName();
    }

    public String getDescription() {
        return achievementNameList[this.ordinal()].getDescription();
    }

    public static AchievementsList fromId(String id) {
        for (AchievementsList ach : AchievementsList.values())
            if (ach.getId().equals(id))
                return ach;
        return achievementPeaceToTheGalaxy;
    }

    public static AchievementsList fromEnumName(String name) {
        for (AchievementsList ach : AchievementsList.values())
            if (ach.toString().equals(name))
                return ach;
        return achievementPeaceToTheGalaxy;
    }

    public static void initAchievements() {
        if (achievementNameList == null) {
            FileHandle fh = Gdx.files.internal("data/achievements/achievements.txt");
            Json json = new Json();
            achievementNameList = json.fromJson(Achievement[].class, fh);
        }
    }

    public Achievement getAchievement() {
        return achievementNameList[this.ordinal()];
    }
}
