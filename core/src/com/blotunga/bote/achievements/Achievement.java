/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.achievements;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;

public class Achievement implements Json.Serializable {
    private AchievementsList type;
    private String name;
    private String description;

    public Achievement() {
        this(AchievementsList.achievementAllHaveBeenEliminated, "", "");
    }

    public Achievement(AchievementsList type) {
        this(type, "", "");
    }

    public Achievement(AchievementsList type, String name, String description) {
        this.type = type;
        this.name = name;
        this.description = description;
    }

    @Override
    public void write(Json json) {
        json.writeObjectStart(type.name());
        json.writeValue(name, description);
        json.writeObjectEnd();
    }

    @Override
    public void read(Json json, JsonValue jsonData) {
        type = AchievementsList.fromEnumName(jsonData.child().name());
        name = jsonData.child().child().name();
        description = jsonData.child().child().asString();
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public AchievementsList getType() {
        return type;
    }
}
