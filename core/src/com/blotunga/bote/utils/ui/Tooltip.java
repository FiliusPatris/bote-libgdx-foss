/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.utils.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;

public class Tooltip<T extends Actor> extends BaseTooltip<T> {

    public Tooltip (T contents) {
        super(contents);
    }

    @Override
    protected EventListener createListener () {
        return new InputListener() {
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                if (isInstant()) {
                    getContainer().toFront();
                    return false;
                }
                getManager().touchDown(Tooltip.this);
                return false;
            }

            public boolean mouseMoved (InputEvent event, float x, float y) {
                if (getContainer().hasParent())
                    return false;
                setContainerPosition(event.getListenerActor(), x, y);
                return true;
            }

            public void enter (InputEvent event, float x, float y, int pointer, Actor fromActor) {
                if (pointer != -1)
                    return;
                if (Gdx.input.isTouched())
                    return;
                Actor actor = event.getListenerActor();
                if (fromActor != null && fromActor.isDescendantOf(actor))
                    return;
                setContainerPosition(actor, x, y);
                getManager().enter(Tooltip.this);
            }

            public void exit (InputEvent event, float x, float y, int pointer, Actor toActor) {
                if (toActor != null && toActor.isDescendantOf(event.getListenerActor()))
                    return;
                hide();
            }

            @Override
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {

            }
        };
    }
}