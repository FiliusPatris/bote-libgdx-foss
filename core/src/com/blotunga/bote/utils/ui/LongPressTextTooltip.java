/*******************************************************************************
 * Copyright 2015 See AUTHORS file.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package com.blotunga.bote.utils.ui;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Value;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

/** A tooltip that shows a label.
 * @author Nathan Sweet */
public class LongPressTextTooltip extends LongPressTooltip<Label> {
    public LongPressTextTooltip (String text, Skin skin) {
        this(text, skin.get(TextTooltipStyle.class));
    }

    public LongPressTextTooltip (String text, Skin skin, String styleName) {
        this(text, skin.get(styleName, TextTooltipStyle.class));
    }

    public LongPressTextTooltip (String text,  TextTooltipStyle style) {
        super(null);

        style.label.background = style.background;
        Label label = new Label(text, style.label);
        label.setWrap(true);

        container.setActor(label);
        container.width(new Value() {
            public float get (Actor context) {
                return Math.min(getManager().maxWidth, container.getActor().getGlyphLayout().width);
            }
        });

        setStyle(style);
    }

    public void setStyle (TextTooltipStyle style) {
        if (style == null) throw new NullPointerException("style cannot be null");
        if (!(style instanceof TextTooltipStyle)) throw new IllegalArgumentException("style must be a TextTooltipStyle.");
        container.getActor().setStyle(style.label);
    }

    /** The style for a text tooltip, see {@link LongPressTextTooltip}.
     * @author Nathan Sweet */
    static public class TextTooltipStyle {
        public LabelStyle label;
        /** Optional. */
        public Drawable background;

        public TextTooltipStyle () {
        }

        public TextTooltipStyle (LabelStyle label, Drawable background) {
            this.label = label;
            this.background = background;
        }

        public TextTooltipStyle (TextTooltipStyle style) {
            this.label = new LabelStyle(style.label);
            background = style.background;
        }
    }
}