/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.utils;

import com.badlogic.gdx.utils.IntArray;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

public class IntArraySerializer extends Serializer<IntArray> {
    public IntArraySerializer() {
        setAcceptsNull(true);
    }

    @Override
    public void write(Kryo kryo, Output output, IntArray object) {
        int length = object.size;
        output.writeInt(length, true);
        if (length == 0)
            return;
        for (int i = 0; i < length; i++)
            output.writeInt(object.get(i));
    }

    @Override
    public IntArray read(Kryo kryo, Input input, Class<IntArray> type) {
        IntArray array = new IntArray();
        kryo.reference(array);
        int length = input.readInt(true);
        array.ensureCapacity(length);
        for (int i = 0; i < length; i++)
            array.add(input.readInt());
        return array;
    }

}
