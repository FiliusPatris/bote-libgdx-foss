/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;

public class StarNameGenerator {
    private String[] starNames; // this will be kept intact to generate new names if needed
    private String[] availableNames;
    private Array<String> minorRaceNames;
    private int numAvailable;
    private int extraIndex;

    public StarNameGenerator() {
        minorRaceNames = new Array<String>();
        FileHandle handle = Gdx.files.internal("data/star/starnames.txt");
        String names = handle.readString("ISO-8859-1");
        starNames = names.split("\n");
        availableNames = starNames.clone();
        numAvailable = availableNames.length;
        extraIndex = 1;
    }

    public void InitMinorRacesNames(Array<String> names) {
        minorRaceNames.addAll(names);
    }

    public Pair<String, Boolean> getNextRandomSectorName(IntPoint coord, boolean isMinor) {
        String name;
        Pair<String, Boolean> p = new Pair<String, Boolean>("", isMinor);
        if (isMinor && minorRaceNames.size != 0) {
            int random = (int) (RandUtil.random() * minorRaceNames.size);
            name = minorRaceNames.get(random);
            minorRaceNames.removeIndex(random);
            p.setFirst(name);
            return p;
        }
        p.setSecond(false);
        name = getNewName();
        p.setFirst(name);
        return p;
    }

    private String getNewName() {
        String name = "";
        if (numAvailable == 0) {
            extraIndex++;
            for (int i = 0; i < starNames.length; i++) {
                availableNames[i] = starNames[i] + " " + extraIndex;
            }
            numAvailable = availableNames.length;
        }

        int r = (int) (RandUtil.random() * numAvailable);
        name = availableNames[r];
        availableNames[r] = availableNames[numAvailable - 1];
        numAvailable--;
        return name.trim();
    }
}
