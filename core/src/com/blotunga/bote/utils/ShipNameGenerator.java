/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.utils;

import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.ObjectIntMap;
import com.badlogic.gdx.utils.ObjectMap.Entry;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.races.Race;

public class ShipNameGenerator {
    private ArrayMap<String, String[]> shipNames;
    private ArrayMap<String, String[]> stillAvailableNames;
    private ObjectIntMap<String> counter;

    public void Init(ResourceManager res) {
        shipNames = new ArrayMap<String, String[]>(false, 6);
        stillAvailableNames = new ArrayMap<String, String[]>(false, 6);
        counter = new ObjectIntMap<String>(6);

        for (Iterator<Entry<String, Race>> iter = res.getRaceController().getRaces().entries().iterator(); iter
                .hasNext();) {
            Entry<String, Race> e = iter.next();
            String sID = e.key;
            counter.put(sID, 0);
            if (e.value.isMajor()) {
                FileHandle handle = Gdx.files.internal("data/names/" + sID + "ShipNames.txt");
                String names = handle.readString("ISO-8859-1");
                String[] nameList = names.split("\n");
                shipNames.put(sID, nameList);
                stillAvailableNames.put(sID, nameList.clone());
            }
        }
    }

    public String generateShipName(String raceID, String raceName, boolean isStation) {
        Array<String> availNames = null;
        Array<String> names = null;
        if (stillAvailableNames.containsKey(raceID)) {
            availNames = new Array<String>(stillAvailableNames.get(raceID));
            names = new Array<String>(shipNames.get(raceID));
            if (availNames.size == 0) {
                counter.put(raceID, counter.get(raceID, 0) + 1);
                availNames = new Array<String>(names.toArray().clone());
            }
        } else {
            counter.put(raceID, counter.get(raceID, 0) + 1);
        }
        int cnt = counter.get(raceID, 0);
        String sufix = "";
        if (cnt >= 1) {
            if (cnt <= 26)
                sufix = String.format("%c", 64 + cnt);
            else
                sufix = String.format("%d", cnt);
        }
        String name = "";
        if (availNames == null) {
            name = raceName;
        } else {
            int random = (int) (RandUtil.random() * availNames.size);
            name = availNames.get(random);
            availNames.removeIndex(random);
            stillAvailableNames.put(raceID, availNames.toArray());
        }
        name = name.trim();
        if (!sufix.isEmpty())
            name = name + " " + sufix;
        if (isStation)
            name += " Station";
        return name;
    }
}
