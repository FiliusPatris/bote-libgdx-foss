/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.utils;

import com.badlogic.gdx.utils.ArrayMap;

/** This class was created to overload the shuffle function to use our own random where we have control over the seed **/
@SuppressWarnings("rawtypes")
public class ArrayMapX<K, V> extends ArrayMap<K, V> {
    public ArrayMapX() {
        super();
    }

    public ArrayMapX(ArrayMap array) {
        super(array);
    }

    public ArrayMapX(boolean ordered, int capacity, Class keyArrayType, Class valueArrayType) {
        super(ordered, capacity, keyArrayType, valueArrayType);
    }

    public ArrayMapX(boolean ordered, int capacity) {
        super(ordered, capacity);
    }

    public ArrayMapX(Class keyArrayType, Class valueArrayType) {
        super(keyArrayType, valueArrayType);
    }

    public ArrayMapX(int capacity) {
        super(capacity);
    }

    @Override
    public void shuffle () {
        for (int i = size - 1; i >= 0; i--) {
            int ii = RandUtil.random(i);
            K tempKey = keys[i];
            keys[i] = keys[ii];
            keys[ii] = tempKey;

            V tempValue = values[i];
            values[i] = values[ii];
            values[ii] = tempValue;
        }
    }
}
