/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.utils;

import com.badlogic.gdx.math.RandomXS128;

public class RandUtil {
    private static RandomXS128 randomNumberGenerator;
    private static long seed0 = 0;
    private static long seed1 = 0;

    private static synchronized RandomXS128 getRNG() {
        RandomXS128 rnd = randomNumberGenerator;
        return (rnd == null) ? (randomNumberGenerator = (seed0 == 0 && seed1 == 0) ? new RandomXS128()
                : new RandomXS128(seed0, seed1)) : rnd;
    }

    public static double random() {
        return getRNG().nextDouble();
    }

    public static void setSeed(long seed) {
        RandomXS128 rnd = new RandomXS128(seed);
        seed0 = rnd.getState(0);
        seed1 = rnd.getState(1);
        if (randomNumberGenerator != null && seed != 0)
            randomNumberGenerator.setState(seed0, seed1);
    }

    public static void setState(long s0, long s1) {
        RandUtil.seed0 = s0;
        RandUtil.seed1 = s1;
        getRNG().setState(seed0, seed1);
    }

    /** Returns a random number between 0 (inclusive) and the specified value (inclusive). */
    static public int random (int range) {
        return nextInt(range + 1);
    }

    public static int nextInt(int n) {
        return getRNG().nextInt(n);
    }

    public static Pair<Long, Long> getState() {
        long s0 = 0;
        long s1 = 0;
        s0 = getRNG().getState(0);
        s1 = getRNG().getState(1);

        return new Pair<Long, Long>(s0, s1);
    }
}
