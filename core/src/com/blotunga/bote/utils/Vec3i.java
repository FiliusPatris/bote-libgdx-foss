/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.utils;

import java.io.Serializable;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector;
import com.badlogic.gdx.math.Vector3;

public class Vec3i implements Serializable, Vector<Vec3i> {
    private static final long serialVersionUID = 5155554589595372522L;

    /** the x-component of this vector **/
    public int x;
    /** the y-component of this vector **/
    public int y;
    /** the z-component of this vector **/
    public int z;

    public final static Vec3i X = new Vec3i(1, 0, 0);
    public final static Vec3i Y = new Vec3i(0, 1, 0);
    public final static Vec3i Z = new Vec3i(0, 0, 1);
    public final static Vec3i Zero = new Vec3i(0, 0, 0);

    /** Constructs a vector at (0,0,0) */
    public Vec3i() {
    }

    /** Creates a vector with the given components
     * @param x The x-component
     * @param y The y-component
     * @param z The z-component */
    public Vec3i(int x, int y, int z) {
        this.set(x, y, z);
    }

    /** Creates a vector from the given vector
     * @param vector The vector */
    public Vec3i(final Vec3i vector) {
        this.set(vector);
    }

    /** Creates a vector from the given array. The array must have at least 3 elements.
     *
     * @param values The array */
    public Vec3i(final int[] values) {
        this.set(values[0], values[1], values[2]);
    }

    /** Sets the vector to the given components
     *
     * @param x The x-component
     * @param y The y-component
     * @param z The z-component
     * @return this vector for chaining */
    public Vec3i set(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
        return this;
    }

    @Override
    public Vec3i set(final Vec3i vector) {
        return this.set(vector.x, vector.y, vector.z);
    }

    /** Sets the components from the array. The array must have at least 3 elements
     *
     * @param values The array
     * @return this vector for chaining */
    public Vec3i set(final int[] values) {
        return this.set(values[0], values[1], values[2]);
    }

    @Override
    public Vec3i cpy() {
        return new Vec3i(this);
    }

    @Override
    public Vec3i add(final Vec3i vector) {
        return this.add(vector.x, vector.y, vector.z);
    }

    /** Adds the given vector to this component
     * @param x The x-component of the other vector
     * @param y The y-component of the other vector
     * @param z The z-component of the other vector
     * @return This vector for chaining. */
    public Vec3i add(int x, int y, int z) {
        return this.set(this.x + x, this.y + y, this.z + z);
    }

    /** Adds the given value to all three components of the vector.
     *
     * @param values The value
     * @return This vector for chaining */
    public Vec3i add(int values) {
        return this.set(this.x + values, this.y + values, this.z + values);
    }

    @Override
    public Vec3i sub(final Vec3i a_vec) {
        return this.sub(a_vec.x, a_vec.y, a_vec.z);
    }

    /** Subtracts the other vector from this vector.
     *
     * @param x The x-component of the other vector
     * @param y The y-component of the other vector
     * @param z The z-component of the other vector
     * @return This vector for chaining */
    public Vec3i sub(int x, int y, int z) {
        return this.set(this.x - x, this.y - y, this.z - z);
    }

    /** Subtracts the given value from all components of this vector
     *
     * @param value The value
     * @return This vector for chaining */
    public Vec3i sub(int value) {
        return this.set(this.x - value, this.y - value, this.z - value);
    }

    @Override
    public Vec3i scl(float scalar) {
        int sc = (int) scalar;
        return this.set(this.x * sc, this.y * sc, this.z * sc);
    }

    @Override
    public Vec3i scl(final Vec3i other) {
        return this.set(x * other.x, y * other.y, z * other.z);
    }

    /** Scales this vector by the given values
     * @param vx X value
     * @param vy Y value
     * @param vz Z value
     * @return This vector for chaining */
    public Vec3i scl(int vx, int vy, int vz) {
        return this.set(this.x * vx, this.y * vy, this.z * vz);
    }

    @Override
    public Vec3i mulAdd(Vec3i vec, float scalar) {
        int sc = (int) scalar;
        this.x += vec.x * sc;
        this.y += vec.y * sc;
        this.z += vec.z * sc;
        return this;
    }

    @Override
    public Vec3i mulAdd(Vec3i vec, Vec3i mulVec) {
        this.x += vec.x * mulVec.x;
        this.y += vec.y * mulVec.y;
        this.z += vec.z * mulVec.z;
        return this;
    }

    /** @return The euclidian length */
    public static int len(final int x, final int y, final int z) {
        return (int) Math.sqrt(x * x + y * y + z * z);
    }

    @Override
    public float len() {
        return (int) Math.sqrt(x * x + y * y + z * z);
    }

    /** @return The squared euclidian length */
    public static int len2(final int x, final int y, final int z) {
        return x * x + y * y + z * z;
    }

    @Override
    public float len2() {
        return x * x + y * y + z * z;
    }

    /** @param vector The other vector
     * @return Wether this and the other vector are equal */
    public boolean idt(final Vec3i vector) {
        return x == vector.x && y == vector.y && z == vector.z;
    }

    /** @return The euclidian distance between the two specified vectors */
    public static int dst(final int x1, final int y1, final int z1, final int x2, final int y2, final int z2) {
        final int a = x2 - x1;
        final int b = y2 - y1;
        final int c = z2 - z1;
        return (int) Math.sqrt(a * a + b * b + c * c);
    }

    @Override
    public float dst(final Vec3i vector) {
        final int a = vector.x - x;
        final int b = vector.y - y;
        final int c = vector.z - z;
        return (int) Math.sqrt(a * a + b * b + c * c);
    }

    /** @return the distance between this point and the given point */
    public int dst(int x, int y, int z) {
        final int a = x - this.x;
        final int b = y - this.y;
        final int c = z - this.z;
        return (int) Math.sqrt(a * a + b * b + c * c);
    }

    /** @return the squared distance between the given points */
    public static int dst2(final int x1, final int y1, final int z1, final int x2, final int y2, final int z2) {
        final int a = x2 - x1;
        final int b = y2 - y1;
        final int c = z2 - z1;
        return a * a + b * b + c * c;
    }

    @Override
    public float dst2(Vec3i point) {
        final int a = point.x - x;
        final int b = point.y - y;
        final int c = point.z - z;
        return a * a + b * b + c * c;
    }

    /** Returns the squared distance between this point and the given point
     * @param x The x-component of the other point
     * @param y The y-component of the other point
     * @param z The z-component of the other point
     * @return The squared distance */
    public int dst2(int x, int y, int z) {
        final int a = x - this.x;
        final int b = y - this.y;
        final int c = z - this.z;
        return a * a + b * b + c * c;
    }

    @Override
    public Vec3i nor() {
        final float len2 = this.len2();
        if (len2 == 0f || len2 == 1f)
            return this;
        return this.scl(1f / (int) Math.sqrt(len2));
    }

    /** @return The dot product between the two vectors */
    public static int dot(int x1, int y1, int z1, int x2, int y2, int z2) {
        return x1 * x2 + y1 * y2 + z1 * z2;
    }

    @Override
    public float dot(final Vec3i vector) {
        return x * vector.x + y * vector.y + z * vector.z;
    }

    /** Returns the dot product between this and the given vector.
     * @param x The x-component of the other vector
     * @param y The y-component of the other vector
     * @param z The z-component of the other vector
     * @return The dot product */
    public int dot(int x, int y, int z) {
        return this.x * x + this.y * y + this.z * z;
    }

    /** Sets this vector to the cross product between it and the other vector.
     * @param vector The other vector
     * @return This vector for chaining */
    public Vec3i crs(final Vec3i vector) {
        return this.set(y * vector.z - z * vector.y, z * vector.x - x * vector.z, x * vector.y - y * vector.x);
    }

    /** Sets this vector to the cross product between it and the other vector.
     * @param x The x-component of the other vector
     * @param y The y-component of the other vector
     * @param z The z-component of the other vector
     * @return This vector for chaining */
    public Vec3i crs(int x, int y, int z) {
        return this.set(this.y * z - this.z * y, this.z * x - this.x * z, this.x * y - this.y * x);
    }

    /** Left-multiplies the vector by the given 4x3 column major matrix. The matrix should be composed by a 3x3 matrix representing
     * rotation and scale plus a 1x3 matrix representing the translation.
     * @param matrix The matrix
     * @return This vector for chaining */
    public Vec3i mul4x3(int[] matrix) {
        return set(x * matrix[0] + y * matrix[3] + z * matrix[6] + matrix[9], x * matrix[1] + y * matrix[4] + z
                * matrix[7] + matrix[10], x * matrix[2] + y * matrix[5] + z * matrix[8] + matrix[11]);
    }

    @Override
    public boolean isUnit() {
        return isUnit(0.000000001f);
    }

    @Override
    public boolean isUnit(final float margin) {
        return Math.abs(len2() - 1f) < margin;
    }

    @Override
    public boolean isZero() {
        return x == 0 && y == 0 && z == 0;
    }

    @Override
    public boolean isZero(final float margin) {
        return len2() < margin;
    }

    @Override
    public boolean isOnLine(Vec3i other, float epsilon) {
        return len2(y * other.z - z * other.y, z * other.x - x * other.z, x * other.y - y * other.x) <= epsilon;
    }

    @Override
    public boolean isOnLine(Vec3i other) {
        return len2(y * other.z - z * other.y, z * other.x - x * other.z, x * other.y - y * other.x) <= MathUtils.FLOAT_ROUNDING_ERROR;
    }

    @Override
    public boolean isCollinear(Vec3i other, float epsilon) {
        return isOnLine(other, epsilon) && hasSameDirection(other);
    }

    @Override
    public boolean isCollinear(Vec3i other) {
        return isOnLine(other) && hasSameDirection(other);
    }

    @Override
    public boolean isCollinearOpposite(Vec3i other, float epsilon) {
        return isOnLine(other, epsilon) && hasOppositeDirection(other);
    }

    @Override
    public boolean isCollinearOpposite(Vec3i other) {
        return isOnLine(other) && hasOppositeDirection(other);
    }

    @Override
    public boolean isPerpendicular(Vec3i vector) {
        return MathUtils.isZero(dot(vector));
    }

    @Override
    public boolean isPerpendicular(Vec3i vector, float epsilon) {
        return MathUtils.isZero(dot(vector), epsilon);
    }

    @Override
    public boolean hasSameDirection(Vec3i vector) {
        return dot(vector) > 0;
    }

    @Override
    public boolean hasOppositeDirection(Vec3i vector) {
        return dot(vector) < 0;
    }

    @Override
    public Vec3i lerp(final Vec3i target, float alpha) {
        scl(1.0f - alpha);
        add((int) (target.x * alpha), (int) (target.y * alpha), (int) (target.z * alpha));
        return this;
    }

    @Override
    public Vec3i interpolate(Vec3i target, float alpha, Interpolation interpolator) {
        return lerp(target, interpolator.apply(0f, 1f, alpha));
    }

    @Override
    public String toString() {
        return "[" + x + ", " + y + ", " + z + "]";
    }

    @Override
    public Vec3i limit(float limit) {
        return limit2(limit * limit);
    }

    @Override
    public Vec3i limit2(float limit2) {
        float len2 = len2();
        if (len2 > limit2) {
            scl(limit2 / len2);
        }
        return this;
    }

    @Override
    public Vec3i setLength(float len) {
        return setLength2(len * len);
    }

    @Override
    public Vec3i setLength2(float len2) {
        float oldLen2 = len2();
        return (oldLen2 == 0 || oldLen2 == len2) ? this : scl((int) Math.sqrt(len2 / oldLen2));
    }

    @Override
    public Vec3i clamp(float min, float max) {
        final float len2 = len2();
        if (len2 == 0f)
            return this;
        float max2 = max * max;
        if (len2 > max2)
            return scl((int) Math.sqrt(max2 / len2));
        float min2 = min * min;
        if (len2 < min2)
            return scl((int) Math.sqrt(min2 / len2));
        return this;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + x;
        result = prime * result + y;
        result = prime * result + z;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Vec3i other = (Vec3i) obj;
        if (x != other.x)
            return false;
        if (y != other.y)
            return false;
        if (z != other.z)
            return false;
        return true;
    }

    @Override
    public boolean epsilonEquals(final Vec3i other, float epsilon) {
        if (other == null)
            return false;
        if (Math.abs(other.x - x) > epsilon)
            return false;
        if (Math.abs(other.y - y) > epsilon)
            return false;
        if (Math.abs(other.z - z) > epsilon)
            return false;
        return true;
    }

    /** Compares this vector with the other vector, using the supplied epsilon for fuzzy equality testing.
     * @return whether the vectors are the same. */
    public boolean epsilonEquals(int x, int y, int z, int epsilon) {
        if (Math.abs(x - this.x) > epsilon)
            return false;
        if (Math.abs(y - this.y) > epsilon)
            return false;
        if (Math.abs(z - this.z) > epsilon)
            return false;
        return true;
    }

    @Override
    public Vec3i setZero() {
        this.x = 0;
        this.y = 0;
        this.z = 0;
        return this;
    }

    public Vector3 toVector3() {
        return new Vector3(x, y, z);
    }

    @Override
    public Vec3i setToRandomDirection() {
        float u = MathUtils.random();
        float v = MathUtils.random();

        float theta = MathUtils.PI2 * u; // azimuthal angle
        float phi = (float)Math.acos(2f * v - 1f); // polar angle

        return this.setFromSpherical(theta, phi);
    }

    private Vec3i setFromSpherical(float azimuthalAngle, float polarAngle) {
        float cosPolar = MathUtils.cos(polarAngle);
        float sinPolar = MathUtils.sin(polarAngle);

        float cosAzim = MathUtils.cos(azimuthalAngle);
        float sinAzim = MathUtils.sin(azimuthalAngle);

        return this.set((int)(cosAzim * sinPolar), (int)(sinAzim * sinPolar), (int)cosPolar);
    }
}