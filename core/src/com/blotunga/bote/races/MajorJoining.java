/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.races;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectFloatMap;
import com.badlogic.gdx.utils.ObjectSet;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.constants.Difficulties;
import com.blotunga.bote.constants.DiplomaticAgreement;
import com.blotunga.bote.constants.SystemOwningStatus;
import com.blotunga.bote.general.EmpireNews;
import com.blotunga.bote.general.GameStatistics;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.general.EmpireNews.EmpireNewsType;
import com.blotunga.bote.ships.ShipMap;
import com.blotunga.bote.ships.Ships;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.utils.IntPoint;
import com.blotunga.bote.utils.Pair;
import com.blotunga.bote.utils.RandUtil;

public class MajorJoining {
    private transient ResourceManager manager;
    private int timesOccured;
    private int startTurn;
    private int risingTurns;
    private int pause;
    private int randomizeBy;
    private ObjectSet<String> joined;

    public MajorJoining() {
        this(null);
    }

    public MajorJoining(ResourceManager manager) {
        this.manager = manager;
        timesOccured = 0;
        startTurn = 120;
        risingTurns = 140;
        pause = 80;
        randomizeBy = 20;
        joined = new ObjectSet<String>();
    }

    public void setResourceManager(ResourceManager manager) {
        this.manager = manager;
    }

    private int randomizeBy(int amount) {
        int sign = (int) (RandUtil.random() * 2) == 0 ? 1 : -1;
        return sign * ((int) (RandUtil.random() * (amount + 1)));
    }

    public void Calculate() {
        int turn = manager.getCurrentRound();
        GameStatistics stats = manager.getStatistics();
        RaceController raceCtrl = manager.getRaceController();
        ShipMap shipMap = manager.getUniverseMap().getShipMap();
        Array<StarSystem> systems = manager.getUniverseMap().getStarSystems();
        Difficulties difficulty = manager.getGamePreferences().difficulty;

        if (turn == 2)
            startTurn += randomizeBy(randomizeBy);

        ObjectFloatMap<String> marks = stats.getSortedMarks();

        Major bestAI = null;
        Major worstAI = null;
        int countOfAIs = 0;
        int countOfHumans = 0;
        float humanAverageMark = 0.0f;
        Array<Pair<Major, Float>> majors = new Array<Pair<Major, Float>>();
        for (ObjectFloatMap.Entries<String> iter = marks.entries().iterator(); iter.hasNext();) {
            ObjectFloatMap.Entry<String> e = iter.next();
            Major major = Major.toMajor(raceCtrl.getRace(e.key));
            if (major != null) {
                majors.add(new Pair<Major, Float>(major, e.value));
                if (major.isHumanPlayer()) {
                    ++countOfHumans;
                    humanAverageMark += e.value;
                } else {
                    ++countOfAIs;
                    worstAI = major;
                    if (bestAI == null) {
                        if (!joined.contains(major.getRaceId()) || (int) (RandUtil.random() * 100) >= 70)
                            bestAI = major;
                    }
                }
            }
        }

        if (countOfHumans != 0)
            humanAverageMark /= countOfHumans;
        if (countOfAIs < 2)
            return;
        if (bestAI == null || bestAI.equals(worstAI))
            return;

        if (turn >= 20)
            calcStartTurnChangeDueToHumansStrength(humanAverageMark, majors.size, difficulty);

        if (!shouldHappenNow(turn))
            return;

        joined.add(bestAI.getRaceId());
        timesOccured++;
        startTurn = turn + pause + randomizeBy(randomizeBy);

        int credits = worstAI.getEmpire().getCredits();
        if (credits > 0)
            bestAI.getEmpire().setCredits(credits);

        for (int i = 0; i < shipMap.getSize(); i++) {
            Ships ship = shipMap.getAt(i);
            if (ship.getOwnerId().equals(worstAI.getRaceId())) {
                manager.getUniverseMap().getStarSystemAt(ship.getCoordinates()).eraseShip(ship);
                ship.setOwner(bestAI.getRaceId());
                manager.getUniverseMap().getStarSystemAt(ship.getCoordinates()).addShip(ship);
                ship.setFlagShip(false);
                ship.unsetCurrentOrder();
                ship.setCombatTacticsAccordingToType();
                ship.setTargetCoord(new IntPoint());
                ship.addSpecialResearchBoni(bestAI);
            }
        }

        bestAI.getShipHistory().getShipHistoryArray().addAll(worstAI.getShipHistory().getShipHistoryArray());

        for (int i = 0; i < systems.size; i++) {
            StarSystem ss = systems.get(i);
            if (!ss.isMajorized() || !ss.getOwnerId().equals(worstAI.getRaceId()))
                continue;

            boolean taken = ss.isTaken();
            ss.changeOwner(bestAI.getRaceId(), taken ? SystemOwningStatus.OWNING_STATUS_TAKEN
                    : SystemOwningStatus.OWNING_STATUS_COLONIZED_MEMBERSHIP_OR_HOME, false);
            ss.setFullKnown(bestAI.getRaceId());
            Race homeOf = raceCtrl.getRace(ss.homeOfID());
            if (homeOf != null && homeOf.isMinor()) {
                Minor minor = Minor.toMinor(homeOf);
                minor.setOwner(bestAI.getRaceId());

                if (!taken) {
                    minor.setRelation(bestAI.getRaceId(), minor.getRelation(worstAI.getRaceId()));
                    minor.setAgreement(worstAI.getRaceId(), DiplomaticAgreement.NONE);
                    minor.setAgreement(bestAI.getRaceId(), DiplomaticAgreement.MEMBERSHIP);
                }
            }
        }

        EmpireNews message1 = new EmpireNews();
        String msgText = StringDB.getString("MAJOR_JOINING_PART_ONE", false, worstAI.getRaceNameWithArticle(),
                bestAI.getRaceNameWithArticle());
        message1.CreateNews(msgText, EmpireNewsType.DIPLOMACY);
        EmpireNews message2 = new EmpireNews();
        msgText = StringDB.getString("MAJOR_JOINING_PART_TWO", false, worstAI.getHomeSystemName(),
                bestAI.getRaceNameWithArticle());
        message2.CreateNews(msgText, EmpireNewsType.DIPLOMACY);
        for (int i = 0; i < majors.size; i++) {
            majors.get(i).getFirst().getEmpire().addMsg(message1);
            majors.get(i).getFirst().getEmpire().addMsg(message2);
        }
    }

    private boolean shouldHappenNow(int turn) {
        if (timesOccured >= manager.getGamePreferences().majorJoinTimesShouldOccur)
            return false;
        if (turn < startTurn)
            return false;
        if (turn > startTurn + risingTurns)
            return true;

        int prob = (int) (100.0f * (Math.pow(2.0f, Math.pow((turn - startTurn) / (float) risingTurns, 3.0f)) - 1));
        return (int) (RandUtil.random() * 100) < prob;
    }

    private void calcStartTurnChangeDueToHumansStrength(float humanAverageMark, int countOfMajors,
            Difficulties difficulty) {
        float fraction = 0.0f;
        int turnChange = 0;
        if (difficulty == Difficulties.NORMAL) {
            fraction = 0.15f;
            turnChange = 1;
        } else if (difficulty == Difficulties.HARD) {
            fraction = 0.3f;
            turnChange = 1;
        } else if (difficulty == Difficulties.VERY_HARD) {
            fraction = 0.45f;
            turnChange = 2;
        } else if (difficulty == Difficulties.IMPOSSIBLE) {
            fraction = 0.6f;
            turnChange = 2;
        }

        if (humanAverageMark > fraction * countOfMajors)
            return;

        startTurn -= turnChange;
        startTurn = Math.max(startTurn, 1);
    }
}
