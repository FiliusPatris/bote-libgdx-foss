/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.races;

import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.ai.MinorAI;
import com.blotunga.bote.constants.PlayerRaces;
import com.blotunga.bote.constants.RaceProperty;

public class Alien extends Minor {
    public Alien() {
        this(null);
    }

    public Alien(ResourceManager res) {
        super(res, RaceType.RACE_TYPE_ALIEN);
    }

    @Override
    public int create(String[] info, int pos) {
        raceID = info[pos++].trim().replaceFirst(":", "");

        graphicFile = info[pos++].trim().replaceFirst(".jpg", "");
        technologicalProgress = Integer.parseInt(info[pos++].trim());

        String[] raceProps = info[pos++].trim().split(",");
        for (int i = 0; i < raceProps.length; i++) {
            RaceProperty prop = RaceProperty.fromInt(Integer.parseInt(raceProps[i].trim()));
            setRaceProperty(prop, true);
        }
        specialAbility = Integer.parseInt(info[pos++].trim());
        spaceFlight = Integer.parseInt(info[pos++].trim()) != 0;
        corruptibility = Integer.parseInt(info[pos++].trim());
        shipNumber = PlayerRaces.MINORNUMBER.getType();
        diplomacyAI = new MinorAI(resourceManager, this);
        return pos;
    }

    public static Alien toAlien(Race race) {
        if (race != null && race.isAlien())
            return (Alien) race;
        else
            return null;
    }
}
