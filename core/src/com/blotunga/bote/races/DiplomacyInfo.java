/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.races;

import java.util.Arrays;

import com.blotunga.bote.constants.AnswerStatus;
import com.blotunga.bote.constants.DiplomaticAgreement;
import com.blotunga.bote.constants.ResourceTypes;
import com.blotunga.bote.utils.IntPoint;

public class DiplomacyInfo {
    public String toRace;		/// to which race is the offer
    public String fromRace;		/// from which race is the offer
    public String text;			/// text of the offer

    public int flag;					/// offer, message or something else
    public DiplomaticAgreement type;	/// type of the agreement
    public int sendRound;				/// round in which it was sent
    public int credits;					/// how many credits are transferred/asked for
    public int[] resources;				/// which resources are offered/demanded
    public IntPoint coord;				/// coordinate of the system from which the resources originated

    public int duration;				/// duration of the offer
    public String warpactEnemy;			/// the enemy in case of a warpact
    public String corruptedRace;		/// major race which should be involved when bribing

    public AnswerStatus answerStatus;	/// was the offer accepted/declined or ignored
    public String headLine;				/// headline in case of an answer
    public String warPartner;			/// war because of a partner

    public DiplomacyInfo() {
        resources = new int[ResourceTypes.DERITIUM.getType() + 1];
        reset();
    }

    public void reset() {
        credits = 0;
        duration = 0;
        flag = -1;
        Arrays.fill(resources, 0);
        sendRound = Integer.MAX_VALUE;
        type = DiplomaticAgreement.NONE;
        coord = new IntPoint();
        corruptedRace = "";
        fromRace = "";
        headLine = "";
        text = "";
        toRace = "";
        warpactEnemy = "";
        warPartner = "";
        answerStatus = AnswerStatus.NOT_REACTED;
    }

    public DiplomacyInfo(DiplomacyInfo other) {
        toRace = other.toRace;
        fromRace = other.fromRace;
        text = other.text;

        flag = other.flag;
        type = other.type;
        sendRound = other.sendRound;
        credits = other.credits;
        resources = other.resources.clone();
        coord = new IntPoint(other.coord);

        duration = other.duration;
        warpactEnemy = other.warpactEnemy;
        corruptedRace = other.corruptedRace;

        answerStatus = other.answerStatus;
        headLine = other.headLine;
        warPartner = other.warPartner;
    }
}