/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.races;

import java.util.Iterator;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.ObjectIntMap;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.ObjectSet;
import com.badlogic.gdx.utils.ObjectSet.ObjectSetIterator;
import com.blotunga.bote.GamePreferences;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.ai.DiplomacyAI;
import com.blotunga.bote.ai.MajorAI;
import com.blotunga.bote.constants.*;
import com.blotunga.bote.general.InGameEntity;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.ships.Ship;
import com.blotunga.bote.ships.ShipHistoryStruct;
import com.blotunga.bote.ships.Ships;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.utils.IntPoint;
import com.blotunga.bote.utils.ObjectSetSerializer;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

public abstract class Race extends InGameEntity implements Comparable<Race> {
    public enum RaceType {
        RACE_TYPE_MAJOR(0),
        RACE_TYPE_MINOR(1),
        RACE_TYPE_ALIEN(2);

        int type;

        RaceType(int type) {
            this.type = type;
        }
    }

    private enum RaceProperties {
        RACE_AGRARIAN(1),
        RACE_FINANCIAL(2),
        RACE_HOSTILE(4),
        RACE_INDUSTRIAL(8),
        RACE_PACIFIST(16),
        RACE_PRODUCER(32),
        RACE_SCIENTIFIC(64),
        RACE_SECRET(128),
        RACE_SNEAKY(256),
        RACE_SOLOING(512),
        RACE_WARLIKE(1024);

        private int type;

        RaceProperties(int type) {
            this.type = type;
        }

        public int getType() {
            return type;
        }
    }

    public enum RaceSpecialAbilities {
        SPECIAL_NEED_NO_FOOD(1),		///< Race needs no food
        SPECIAL_NO_DIPLOMACY(2),		///< Race has no diplomacy (no first contact)
        SPECIAL_ALIEN_DIPLOMACY(4);		///< Only alien diplomacy possible (NAP, Friendship and War)

        private int ability;

        RaceSpecialAbilities(int ability) {
            this.ability = ability;
        }

        public int getAbility() {
            return ability;
        }
    }

    protected String raceID; ///<!!! ID of the race
    protected String homeSystem; ///<!!! name of the home system
    protected String nameArticle; ///<!!! article of race name
    protected RaceType raceType; ///<!!! type of race (Major, Minor, Alien)
    protected int property; ///<!!! race properties
    protected int shipNumber; ///<!!! shows what types of ships should be used
    protected int buildingNumber; ///<!!! shows what types of buildings should be used
    protected int moralNumber; ///<!!! shows what type of moral values should be used
    protected int specialAbility; ///< special abilities of the race

    protected ObjectIntMap<String> relations; ///< map of relations (Race name, value of relation)
    protected ObjectMap<String, DiplomaticAgreement> agreements; ///< status with other races
    protected ObjectSet<String> inContact; ///< race knows other race

    protected Array<DiplomacyInfo> diplomacyNewsIn;	///< array with all incoming diplomatic messages
    protected Array<DiplomacyInfo> diplomacyNewsOut; ///< array with all outgoing diplomatic messages
    protected ObjectMap<String, DiplomacyInfo> lastOffers; ///< map with the offers made in the last 2 rounds

    protected String graphicFile; ///< name of the graphic storage

    transient protected DiplomacyAI diplomacyAI;
    private boolean deleted;

    public Race(RaceType type, ResourceManager res) {
        super(res);
        this.type = EntityType.RACE;
        this.raceType = type;
        raceID = "";
        homeSystem = "";
        nameArticle = "";
        property = 0;
        shipNumber = 0;
        buildingNumber = 0;
        moralNumber = 0;
        specialAbility = 0;
        relations = new ObjectIntMap<String>();
        agreements = new ObjectMap<String, DiplomaticAgreement>();
        inContact = new ObjectSet<String>();
        diplomacyNewsIn = new Array<DiplomacyInfo>();
        diplomacyNewsOut = new Array<DiplomacyInfo>();
        lastOffers = new ObjectMap<String, DiplomacyInfo>();
        deleted = false;
    }

    public Race(Race other) {
        super(other);
        this.type = other.type;
        this.raceType = other.raceType;
        this.raceID = other.raceID;
        this.homeSystem = other.homeSystem;
        this.nameArticle = other.nameArticle;
        this.property = other.property;
        this.shipNumber = other.shipNumber;
        this.buildingNumber = other.buildingNumber;
        this.moralNumber = other.moralNumber;
        this.specialAbility = other.specialAbility;
        this.relations = new ObjectIntMap<String>(other.relations);
        this.agreements = new ObjectMap<String, DiplomaticAgreement>(other.agreements);
        this.inContact = new ObjectSet<String>(other.inContact);
        this.diplomacyNewsIn = new Array<DiplomacyInfo>(other.diplomacyNewsIn);
        this.diplomacyNewsOut = new Array<DiplomacyInfo>(other.diplomacyNewsOut);
        this.lastOffers = new ObjectMap<String, DiplomacyInfo>(other.lastOffers);
        this.deleted = other.deleted;
        this.graphicFile = other.graphicFile;
    }

    public Empire getEmpire() {
        return null;
    }

    public String getRaceId() {
        return raceID;
    }

    /**
     * Function returns whether an another race was contacted
     * @param raceID is the ID of the other race
     * @return true if the contact exists
     */
    public boolean isRaceContacted(String raceID) {
        return inContact.contains(raceID);
    }

    public boolean isEnemyOf(String other) {
        if (other.equals(raceID))
            return false;
        DiplomaticAgreement agreement = getAgreement(other);
        if (agreement.getType() >= DiplomaticAgreement.FRIENDSHIP.getType())
            return false;
        return agreement != DiplomaticAgreement.NAP;
    }

    /**
     * Function to find out race properties
     * @param prop property
     * @return true if the race has the property otherwise false
     */
    public boolean isRaceProperty(RaceProperty prop) {
        switch (prop) {
            case AGRARIAN:
                return (property & RaceProperties.RACE_AGRARIAN.getType()) != 0;
            case FINANCIAL:
                return (property & RaceProperties.RACE_FINANCIAL.getType()) != 0;
            case HOSTILE:
                return (property & RaceProperties.RACE_HOSTILE.getType()) != 0;
            case INDUSTRIAL:
                return (property & RaceProperties.RACE_INDUSTRIAL.getType()) != 0;
            case PACIFIST:
                return (property & RaceProperties.RACE_PACIFIST.getType()) != 0;
            case PRODUCER:
                return (property & RaceProperties.RACE_PRODUCER.getType()) != 0;
            case SCIENTIFIC:
                return (property & RaceProperties.RACE_SCIENTIFIC.getType()) != 0;
            case SECRET:
                return (property & RaceProperties.RACE_SECRET.getType()) != 0;
            case SNEAKY:
                return (property & RaceProperties.RACE_SNEAKY.getType()) != 0;
            case SOLOING:
                return (property & RaceProperties.RACE_SOLOING.getType()) != 0;
            case WARLIKE:
                return (property & RaceProperties.RACE_WARLIKE.getType()) != 0;
            default:
                break;
        }
        return false;
    }

    public void setHomeSystemName(String homeSystem) {
        this.homeSystem = homeSystem;
    }

    public String getHomeSystemName() {
        return homeSystem;
    }

    public String getRaceNameWithArticle() {
        return nameArticle + " " + name;
    }

    public void setRaceType(RaceType raceType) {
        this.raceType = raceType;
    }

    public RaceType getType() {
        return raceType;
    }

    public boolean isMajor() {
        return raceType == RaceType.RACE_TYPE_MAJOR;
    }

    public boolean isMinor() {
        return raceType == RaceType.RACE_TYPE_MINOR || raceType == RaceType.RACE_TYPE_ALIEN;
    }

    /**
     * Function returns whether a race an alien one is. Alien races have no homesystems
     * @return true if it's an alien race
     */
    public boolean isAlien() {
        return raceType == RaceType.RACE_TYPE_ALIEN;
    }

    /**
     * Returns the number which is mapped to this race
     * @return race specific number
     */
    public int getRaceShipNumber() {
        return shipNumber;
    }

    /**
     * Returns the number which is mapped to this race
     * @return race specific number
     */
    public int getRaceBuildingNumber() {
        return buildingNumber;
    }

    /**
     * Returns the number which is mapped to this race
     * @return race specific number
     */
    public int getRaceMoralNumber() {
        return moralNumber;
    }

    /**
     * Function returns the relation value to another race
     * @param otherRaceID second race
     * @return value of relation (0-100)
     */
    public int getRelation(String otherRaceID) {
        return relations.get(otherRaceID, 0);
    }

    /**
     * @return the relationship map (needed by the AI)
     */
    public ObjectIntMap<String> getRelations() {
        return relations;
    }

    /**
     * Function set the relationship to a second race
     * @param otherRaceID ID of the race with whom the relationship should be altered
     * @param add value which should be added to the old relation
     */
    public void setRelation(String otherRaceID, int add) {
        int relation = relations.get(otherRaceID, 0) + add;
        if (relation > 100)
            relation = 100;
        else if (relation < 0)
            relation = 0;

        if (relation == 0)
            relations.remove(otherRaceID, 0);
        else
            relations.put(otherRaceID, relation);
    }

    /**
     * Returns the diplomatic status to other races
     * @param otherRaceID id of the other race
     * @return type of agreement (in the range of -5 to 6)
     */
    public DiplomaticAgreement getAgreement(String otherRaceID) {
        if (agreements.containsKey(otherRaceID))
            return agreements.get(otherRaceID);
        else
            return DiplomaticAgreement.NONE;
    }

    /**
     * @return - the array with the outgoing diplomatic news
     */
    public Array<DiplomacyInfo> getOutgoingDiplomacyNews() {
        return diplomacyNewsOut;
    }

    /**
     * @return - the array with the incoming diplomatic news
     */
    public Array<DiplomacyInfo> getIncomingDiplomacyNews() {
        return diplomacyNewsIn;
    }

    /**
     * Returns the last offer from the last 2 rounds made to the race
     * @param raceID
     * @return
     */
    public DiplomacyInfo getLastOffer(String raceID) {
        return lastOffers.get(raceID, null);
    }

    /**
     * Returns the race specific graphic file name including extension
     * @return name of the graphic file
     */
    public String getGraphicFileName() {
        return graphicFile;
    }

    public void setGraphicFileName(String name) {
        this.graphicFile = name;
    }

    public Array<String> getPropertiesAsStrings() {
        Array<String> properties = new Array<String>();
        if (isRaceProperty(RaceProperty.FINANCIAL))
            properties.add(StringDB.getString("FINANCIAL"));
        if (isRaceProperty(RaceProperty.WARLIKE))
            properties.add(StringDB.getString("WARLIKE"));
        if (isRaceProperty(RaceProperty.AGRARIAN))
            properties.add(StringDB.getString("AGRARIAN"));
        if (isRaceProperty(RaceProperty.INDUSTRIAL))
            properties.add(StringDB.getString("INDUSTRIAL"));
        if (isRaceProperty(RaceProperty.SECRET))
            properties.add(StringDB.getString("SECRET"));
        if (isRaceProperty(RaceProperty.SCIENTIFIC))
            properties.add(StringDB.getString("SCIENTIFIC"));
        if (isRaceProperty(RaceProperty.PRODUCER))
            properties.add(StringDB.getString("PRODUCER"));
        if (isRaceProperty(RaceProperty.PACIFIST))
            properties.add(StringDB.getString("PACIFIST"));
        if (isRaceProperty(RaceProperty.SNEAKY))
            properties.add(StringDB.getString("SNEAKY"));
        if (isRaceProperty(RaceProperty.SOLOING))
            properties.add(StringDB.getString("SOLOING"));
        if (isRaceProperty(RaceProperty.HOSTILE))
            properties.add(StringDB.getString("HOSTILE"));
        if (properties.size == 0)
            properties.add(StringDB.getString("NONE"));
        return properties;
    }

    public String getPropertiesAsString() {
        Array<String> properties = getPropertiesAsStrings();
        String result = "";
        for (String property : properties) {
            result += property;
            if (!property.equals(properties.get(properties.size - 1)))
                result += ", ";
        }
        return result;
    }

    /**
     * Sets a race property
     * @param prop
     * @param is true to set, false to unset
     */
    public void setRaceProperty(RaceProperty prop, boolean is) {
        switch (prop) {
            case NOTHING_SPECIAL:
                if (is)
                    property = 0;
                break;
            case AGRARIAN:
                property = GameConstants.setAttributes(is, RaceProperties.RACE_AGRARIAN.getType(), property);
                break;
            case FINANCIAL:
                property = GameConstants.setAttributes(is, RaceProperties.RACE_FINANCIAL.getType(), property);
                break;
            case HOSTILE:
                property = GameConstants.setAttributes(is, RaceProperties.RACE_HOSTILE.getType(), property);
                break;
            case INDUSTRIAL:
                property = GameConstants.setAttributes(is, RaceProperties.RACE_INDUSTRIAL.getType(), property);
                break;
            case PACIFIST:
                property = GameConstants.setAttributes(is, RaceProperties.RACE_PACIFIST.getType(), property);
                break;
            case PRODUCER:
                property = GameConstants.setAttributes(is, RaceProperties.RACE_PRODUCER.getType(), property);
                break;
            case SCIENTIFIC:
                property = GameConstants.setAttributes(is, RaceProperties.RACE_SCIENTIFIC.getType(), property);
                break;
            case SECRET:
                property = GameConstants.setAttributes(is, RaceProperties.RACE_SECRET.getType(), property);
                break;
            case SNEAKY:
                property = GameConstants.setAttributes(is, RaceProperties.RACE_SNEAKY.getType(), property);
                break;
            case SOLOING:
                property = GameConstants.setAttributes(is, RaceProperties.RACE_SOLOING.getType(), property);
                break;
            case WARLIKE:
                property = GameConstants.setAttributes(is, RaceProperties.RACE_WARLIKE.getType(), property);
                break;
        }
    }

    /**
     * Sets if a race knows another race
     * @param race the id of the other race
     * @param known true if they are known, false otherwise
     */
    public void setIsRaceContacted(String race, boolean known) {
        // search for race
        if (inContact.contains(race)) {
            // if it should become known but is already in the list, return
            if (known)
                return;
            // else delete from list
            else {
                inContact.remove(race);
                return;
            }
        }
        // if it should be added and it's not yet in the list, simply add
        if (known)
            inContact.add(race);
    }

    /**
     * Function sets the diplomatic status to a different race
     * @param race the other race
     * @param newAgreement the new agreement
     */
    public void setAgreement(String race, DiplomaticAgreement newAgreement) {
        if (newAgreement == DiplomaticAgreement.NONE)
            agreements.remove(race);
        else
            agreements.put(race, newAgreement);
    }

    /**
     * The function lets the AI make offers to other races
     */
    public void makeOffersAI(ArrayMap<String, Race> races) {
        ObjectSet<String> removingOffers = new ObjectSet<String>();
        for (Iterator<ObjectMap.Entry<String, DiplomacyInfo>> iter = lastOffers.entries().iterator(); iter.hasNext();) {
            ObjectMap.Entry<String, DiplomacyInfo> e = iter.next();
            if (e.value.sendRound + 2 < resourceManager.getCurrentRound())
                removingOffers.add(e.key);
        }

        for (ObjectSetIterator<String> iter = removingOffers.iterator(); iter.hasNext();)
            lastOffers.remove(iter.next());

        if (isMajor()) {
            if (((Major) this).aHumanPlays())
                return;
            ((MajorAI) diplomacyAI).calcFavoriteMinors();
        }

        for (int i = 0; i < races.size; i++) {
            String otherID = races.getKeyAt(i);
            Race otherRace = races.getValueAt(i);
            if (!otherID.equals(getRaceId())) {
                //minors can't offer to minors
                if (isMinor() && otherRace.isMinor())
                    continue;

                //if there was an offer made in the last 2 rounds
                if (getLastOffer(otherID) != null)
                    continue;

                DiplomacyInfo info = new DiplomacyInfo();
                if (diplomacyAI.makeOffer(otherID, info)) {
                    diplomacyNewsOut.add(info);
                    //store the offer for the next 2 rounds
                    if (info.flag == DiplomacyInfoType.DIPLOMACY_OFFER.getType()
                            && info.type.getType() > DiplomaticAgreement.NONE.getType())
                        lastOffers.put(otherID, info);
                }
            }
        }
    }

    /**
     * Lets the AI react on offers
     * @param offer
     * @return result of reactions
     */
    public DiplomacyInfo reactOnOfferAI(DiplomacyInfo offer) {
        if (offer.flag == DiplomacyInfoType.DIPLOMACY_OFFER.getType()) {
            AnswerStatus result = diplomacyAI.reactOnOffer(offer);
            offer.answerStatus = result;
        }
        return offer;
    }

    public DiplomacyInfo getPendingOffer(String otherRaceID) {
        for (int i = 0; i < diplomacyNewsOut.size; i++) {
            DiplomacyInfo di = diplomacyNewsOut.get(i);
            if (di.flag == DiplomacyInfoType.DIPLOMACY_OFFER.getType() && di.fromRace.equals(raceID)
                    && di.toRace.equals(otherRaceID))
                return new DiplomacyInfo(di);
        }
        return null;
    }

    public void setRaceShipNumber(int number) {
        shipNumber = number;
    }

    /**
     * Query if a race has a special ability
     * @param ability racial special ability
     * @return true if the race has a special ability
     */
    public boolean hasSpecialAbility(int ability) {
        return (specialAbility & ability) > 0;
    }

    //needed by the editor
    public int getSpecialAbility() {
        return specialAbility;
    }

    public boolean canBeContactedBy(String raceID) {
        return !isRaceContacted(raceID) && !hasSpecialAbility(RaceSpecialAbilities.SPECIAL_NO_DIPLOMACY.getAbility());
    }

    public void contact(Race race, StarSystem s) {
        setIsRaceContacted(race.getRaceId(), true);
    }

    public void addLostShipHistory(ShipHistoryStruct ship, String event, String status, int round, IntPoint coord) {
    }

    public void lostFlagShip(Ship ship) {
    }

    public void lostStation(ShipType type) {
    }

    public void lostShipToAnomaly(Ships ship, String anomaly) {
    }

    /**
     * Function to set the special abilities of a race
     * @param ability the ability
     * @param is true or false
     */
    public void setSpecialAbility(int ability, boolean is) {
        if (ability == 0 && is)
            specialAbility = 0;
        else
            specialAbility = GameConstants.setAttributes(is, ability, specialAbility);
    }

    public void delete() {
        deleted = true;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setRaceCoordinates(IntPoint pos) {
        assert (pos.inRect(0, 0, resourceManager.getGridSizeX() * GamePreferences.spriteSize,
                resourceManager.getGridSizeY() * GamePreferences.spriteSize));
        coordinates = pos;
    }

    public void setRaceID(String id) {
        raceID = id;
    }
    /**
     * Function to create a race
     * The function reads the info from a file
     * @param info the array containing the race informations
     * @param pos reference of the position in the array
     */
    public abstract int create(String[] info, int pos);

    public void getTooltip(Table table, Skin skin, String headerFont, Color headerColor, String textFont, Color textColor, boolean withDescription) {
        table.clearChildren();
        String text = name;
        Label l = new Label(text, skin, headerFont, headerColor);
        table.add(l);
        table.row();

        text = StringDB.getString("PROPERTIES");
        l = new Label(text, skin, textFont, headerColor);
        table.add(l);
        table.row();

        Array<String> properties = getPropertiesAsStrings();
        for(int i = 0; i < properties.size; i++) {
            text = properties.get(i);
            l = new Label(text, skin, textFont, textColor);
            table.add(l);
            table.row();
        }
        if(withDescription) {
            text = getDescription();
            l = new Label(text, skin, textFont, textColor);
            l.setWrap(true);
            l.setAlignment(Align.center);
            table.add(l).width(GameConstants.wToRelative(600));
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null || obj.getClass() != ((Object) this).getClass())
            return false;
        return raceID.equals(((Race) obj).getRaceId());
    }

    public void setDiplomacyAI(DiplomacyAI ai) {
        this.diplomacyAI = ai;
    }

    @Override
    public int compareTo(Race o) {
        return raceID.compareTo(o.raceID);
    }

    @Override
    public void write(Kryo kryo, Output output) {
        super.write(kryo, output);
        output.writeString(raceID);
        output.writeString(homeSystem);
        output.writeString(nameArticle);
        kryo.writeObject(output, raceType);
        output.writeInt(property);
        output.writeInt(shipNumber);
        output.writeInt(buildingNumber);
        output.writeInt(moralNumber);
        output.writeInt(specialAbility);
        kryo.writeObject(output, relations);
        kryo.writeObject(output, agreements);
        kryo.writeObject(output, inContact, new ObjectSetSerializer());
        kryo.writeObject(output, diplomacyNewsIn);
        kryo.writeObject(output, diplomacyNewsOut);
        kryo.writeObject(output, lastOffers);
        output.writeString(graphicFile);
        output.writeBoolean(deleted);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void read(Kryo kryo, Input input) {
        super.read(kryo, input);
        raceID = input.readString();
        homeSystem = input.readString();
        nameArticle = input.readString();
        raceType = kryo.readObject(input, RaceType.class);
        property = input.readInt();
        shipNumber = input.readInt();
        buildingNumber = input.readInt();
        moralNumber = input.readInt();
        specialAbility = input.readInt();
        relations = kryo.readObject(input, ObjectIntMap.class);
        agreements = kryo.readObject(input, ObjectMap.class);
        inContact = kryo.readObject(input, ObjectSet.class);
        diplomacyNewsIn = kryo.readObject(input, Array.class);
        diplomacyNewsOut = kryo.readObject(input, Array.class);
        lastOffers = kryo.readObject(input, ObjectMap.class);
        graphicFile = input.readString();
        deleted = input.readBoolean();
    }
}
