/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.races;

import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.ObjectMap.Entry;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.ai.MajorAI;
import com.blotunga.bote.ai.MinorAI;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.RaceProperty;
import com.blotunga.bote.utils.ArrayMapX;
import com.blotunga.bote.utils.IntPoint;
import com.blotunga.bote.utils.RandUtil;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

public class RaceController {
    private ArrayMap<String, Race> races;
    private ArrayMap<String, Major> majors;
    private ArrayMap<String, Minor> minors;
    private ResourceManager resourceManager;
    private String playerRace;

    public RaceController(ResourceManager res) {
        resourceManager = res;
        races = new ArrayMap<String, Race>(true, 16, String.class, Race.class);
        majors = new ArrayMap<String, Major>(true, 6, String.class, Race.class);
        minors = new ArrayMap<String, Minor>(true, 16, String.class, Race.class);
        playerRace = "MAJOR1";
    }

    public Race getRace(String race) {
        if (races.containsKey(race))
            return races.get(race);
        return null;
    }

    public ArrayMap<String, Race> getRaces() {
        return races;
    }

    public ArrayMap<String, Race> getRandomRaces() {
        ArrayMap<String, Race> racesCopy = new ArrayMapX<String, Race>(races);
        racesCopy.shuffle();
        return racesCopy;
    }

    public ArrayMap<String, Major> getMajors() {
        return majors;
    }

    public ArrayMap<String, Major> getRandomMajors() {
        ArrayMap<String, Major> majorsCopy = new ArrayMapX<String, Major>(majors);
        majorsCopy.shuffle();
        return majorsCopy;
    }

    public ArrayMap<String, Minor> getMinors() {
        return minors;
    }

    public boolean init() {
        if ((initMajors() & initMinors() & initAlienEntities()) == false)
            return false;
        initRelations();
        return true;
    }

    public void reInit() {
        races.clear();
        majors.clear();
        minors.clear();
        init();
    }

    public boolean initMajors() {
        FileHandle handle = Gdx.files.internal("data" + GameConstants.getLocalePrefix() + "/races/majorraces.txt");
        int infoCount = 29;
        @SuppressWarnings("unused")
        String version;
        String content = handle.readString(GameConstants.getCharset());
        String[] input = content.split("\n");
        if (input.length != 0) {
            int startPos = 0;
            version = input[startPos++];
            while (startPos < input.length) {
                Major newMajor = new Major(resourceManager);
                startPos = newMajor.create(input, startPos);
                if ((startPos - 1) % infoCount != 0) {
                    Gdx.app.error("RaceController:", "Error in majorraces.txt");
                    return false;
                }
                newMajor.setHumanPlayer(newMajor.getRaceId().equals(playerRace));
                if (races.containsKey(newMajor.getRaceId()))
                    Gdx.app.error("RaceController:", "Error in majorraces.txt, the race-ID exists twice");
                else {
                    races.put(newMajor.getRaceId(), newMajor);
                    majors.put(newMajor.getRaceId(), newMajor);
                }
            }
        }
        return true;
    }

    //Needed by the editor
    public ArrayMap<String, Minor> initMinorsAndAliensFromFile(String locale) {
        initMinors(locale);
        initAlienEntities(locale);
        return minors;
    }

    private boolean initMinors() {
        return initMinors(GameConstants.getLocalePrefix());
    }

    private boolean initMinors(String locale) {
        if (locale.equals("/en"))
            locale = "";

        FileHandle handle = Gdx.files.internal("data/races/minorstatlist.txt");
        int infoCount = 6;
        String content = handle.readString(GameConstants.getCharset());
        String[] input = content.split("\n");
        if (input.length != 0) {
            int startPos = 0;
            while (startPos < input.length) {
                Minor newMinor = new Minor(resourceManager);
                startPos = newMinor.create(input, startPos);
                if (startPos % infoCount != 0) {
                    Gdx.app.error("RaceController:", "Error in minorraces.txt");
                    return false;
                }
                if (races.containsKey(newMinor.getRaceId()))
                    Gdx.app.error("RaceController:", "Error in minorraces.txt, the race-ID exists twice");
                else {
                    races.put(newMinor.getRaceId(), newMinor);
                    minors.put(newMinor.getRaceId(), newMinor);
                }
            }
        }

        FileHandle infoHandle = Gdx.files.internal("data" + locale + "/races/minorinfolist.txt");
        String infoContent = infoHandle.readString(GameConstants.getCharset(locale));
        String[] infoInput = infoContent.split("\n");
        String infoData[] = new String[3];
        int i = 0;
        for (int j = 0; j < infoInput.length; j++) {
            infoData[i++] = infoInput[j].trim();
            if (i == 3) {
                i = 0;
                int k = 0;
                Minor minor = minors.get(infoData[k++].trim().replaceFirst(":", ""));
                minor.setName(infoData[k++]);
                minor.setDescription(infoData[k++]);
            }
        }

        return true;
    }

    private boolean initAlienEntities() {
        return initAlienEntities(GameConstants.getLocalePrefix());
    }

    private boolean initAlienEntities(String locale) {
        if (locale.equals("/en"))
            locale = "";

        FileHandle handle = Gdx.files.internal("data/races/alienentitiesstatlist.txt");
        int infoCount = 7;
        String content = handle.readString(GameConstants.getCharset());
        String[] input = content.split("\n");
        if (input.length != 0) {
            int startPos = 0;
            while (startPos < input.length) {
                Alien newAlien = new Alien(resourceManager);
                startPos = newAlien.create(input, startPos);
                if (startPos % infoCount != 0) {
                    Gdx.app.error("RaceController:", "Error in alienentities.txt");
                    return false;
                }
                if (races.containsKey(newAlien.getRaceId()))
                    Gdx.app.error("RaceController:", "Error in alienentities.txt, the race-ID exists twice");
                else {
                    races.put(newAlien.getRaceId(), newAlien);
                    minors.put(newAlien.getRaceId(), newAlien);
                }
            }
        }

        FileHandle infoHandle = Gdx.files.internal("data" + locale + "/races/alienentitiesinfolist.txt");
        String infoContent = infoHandle.readString(GameConstants.getCharset(locale));
        String[] infoInput = infoContent.split("\n");
        String infoData[] = new String[3];
        int i = 0;
        for (int j = 0; j < infoInput.length; j++) {
            infoData[i++] = infoInput[j].trim();
            if (i == 3) {
                i = 0;
                int k = 0;
                Minor minor = minors.get(infoData[k++].trim().replaceFirst(":", ""));
                minor.setName(infoData[k++]);
                minor.setDescription(infoData[k++]);
            }
        }
        return true;
    }

    /**
     * The function initializes the relations between the races, namely between majors and minors and the majors themselves
     */
    private void initRelations() {
        for (int i = 0; i < races.size; i++)
            for (int j = 0; j < races.size; j++) {
                String firstKey = races.getKeyAt(i);
                String secondKey = races.getKeyAt(j);
                Race firstRace = races.getValueAt(i);
                Race secondRace = races.getValueAt(j);
                if (!firstKey.equals(secondKey) && secondRace.isMajor()) {
                    int base = 50;
                    int bonus = 0; //will be set by race properties
                    if (firstRace.isRaceProperty(RaceProperty.FINANCIAL)) {
                        base += 10;
                        if (secondRace.isRaceProperty(RaceProperty.FINANCIAL))
                            bonus += 25;
                        if (secondRace.isRaceProperty(RaceProperty.WARLIKE))
                            bonus += 5;
                        if (secondRace.isRaceProperty(RaceProperty.AGRARIAN))
                            bonus += 10;
                        if (secondRace.isRaceProperty(RaceProperty.INDUSTRIAL))
                            bonus += 15;
                        if (secondRace.isRaceProperty(RaceProperty.SECRET))
                            bonus -= 15;
                        if (secondRace.isRaceProperty(RaceProperty.SCIENTIFIC))
                            bonus += 5;
                        if (secondRace.isRaceProperty(RaceProperty.PRODUCER))
                            bonus += 15;
                        if (secondRace.isRaceProperty(RaceProperty.PACIFIST))
                            bonus -= 25;
                        if (secondRace.isRaceProperty(RaceProperty.SNEAKY))
                            bonus -= 20;
                        if (secondRace.isRaceProperty(RaceProperty.SOLOING))
                            bonus -= 25;
                        if (secondRace.isRaceProperty(RaceProperty.HOSTILE))
                            bonus -= 40;
                    }
                    if (firstRace.isRaceProperty(RaceProperty.WARLIKE)) {
                        base -= 20;
                        if (secondRace.isRaceProperty(RaceProperty.FINANCIAL))
                            bonus += 5;
                        if (secondRace.isRaceProperty(RaceProperty.WARLIKE))
                            bonus += 40;
                        if (secondRace.isRaceProperty(RaceProperty.AGRARIAN))
                            bonus -= 10;
                        if (secondRace.isRaceProperty(RaceProperty.INDUSTRIAL))
                            bonus -= 5;
                        if (secondRace.isRaceProperty(RaceProperty.SECRET))
                            bonus -= 5;
                        if (secondRace.isRaceProperty(RaceProperty.SCIENTIFIC))
                            bonus -= 5;
                        if (secondRace.isRaceProperty(RaceProperty.PRODUCER))
                            bonus -= 5;
                        if (secondRace.isRaceProperty(RaceProperty.PACIFIST))
                            bonus -= 50;
                        if (secondRace.isRaceProperty(RaceProperty.SNEAKY))
                            bonus -= 10;
                        if (secondRace.isRaceProperty(RaceProperty.SOLOING))
                            bonus -= 15;
                        if (secondRace.isRaceProperty(RaceProperty.HOSTILE))
                            bonus -= 20;
                    }
                    if (firstRace.isRaceProperty(RaceProperty.AGRARIAN)) {
                        base += 10;
                        if (secondRace.isRaceProperty(RaceProperty.FINANCIAL))
                            bonus += 10;
                        if (secondRace.isRaceProperty(RaceProperty.WARLIKE))
                            bonus -= 15;
                        if (secondRace.isRaceProperty(RaceProperty.AGRARIAN))
                            bonus += 30;
                        if (secondRace.isRaceProperty(RaceProperty.INDUSTRIAL))
                            bonus += 5;
                        if (secondRace.isRaceProperty(RaceProperty.SECRET))
                            bonus -= 15;
                        if (secondRace.isRaceProperty(RaceProperty.SCIENTIFIC))
                            bonus += 5;
                        if (secondRace.isRaceProperty(RaceProperty.PRODUCER))
                            bonus += 25;
                        if (secondRace.isRaceProperty(RaceProperty.PACIFIST))
                            bonus += 25;
                        if (secondRace.isRaceProperty(RaceProperty.SNEAKY))
                            bonus -= 20;
                        if (secondRace.isRaceProperty(RaceProperty.SOLOING))
                            bonus -= 25;
                        if (secondRace.isRaceProperty(RaceProperty.HOSTILE))
                            bonus -= 40;
                    }
                    if (firstRace.isRaceProperty(RaceProperty.INDUSTRIAL)) {
                        base += 5;
                        if (secondRace.isRaceProperty(RaceProperty.FINANCIAL))
                            bonus += 10;
                        if (secondRace.isRaceProperty(RaceProperty.WARLIKE))
                            bonus += 5;
                        if (secondRace.isRaceProperty(RaceProperty.AGRARIAN))
                            bonus += 5;
                        if (secondRace.isRaceProperty(RaceProperty.INDUSTRIAL))
                            bonus += 25;
                        if (secondRace.isRaceProperty(RaceProperty.SECRET))
                            bonus -= 10;
                        if (secondRace.isRaceProperty(RaceProperty.SCIENTIFIC))
                            bonus += 0;
                        if (secondRace.isRaceProperty(RaceProperty.PRODUCER))
                            bonus += 20;
                        if (secondRace.isRaceProperty(RaceProperty.PACIFIST))
                            bonus += 0;
                        if (secondRace.isRaceProperty(RaceProperty.SNEAKY))
                            bonus -= 10;
                        if (secondRace.isRaceProperty(RaceProperty.SOLOING))
                            bonus -= 15;
                        if (secondRace.isRaceProperty(RaceProperty.HOSTILE))
                            bonus -= 20;
                    }
                    if (firstRace.isRaceProperty(RaceProperty.SECRET)) {
                        base -= 10;
                        if (secondRace.isRaceProperty(RaceProperty.FINANCIAL))
                            bonus += 10;
                        if (secondRace.isRaceProperty(RaceProperty.WARLIKE))
                            bonus -= 15;
                        if (secondRace.isRaceProperty(RaceProperty.AGRARIAN))
                            bonus += 10;
                        if (secondRace.isRaceProperty(RaceProperty.INDUSTRIAL))
                            bonus += 0;
                        if (secondRace.isRaceProperty(RaceProperty.SECRET))
                            bonus += 15;
                        if (secondRace.isRaceProperty(RaceProperty.SCIENTIFIC))
                            bonus += 5;
                        if (secondRace.isRaceProperty(RaceProperty.PRODUCER))
                            bonus += 0;
                        if (secondRace.isRaceProperty(RaceProperty.PACIFIST))
                            bonus -= 5;
                        if (secondRace.isRaceProperty(RaceProperty.SNEAKY))
                            bonus += 10;
                        if (secondRace.isRaceProperty(RaceProperty.SOLOING))
                            bonus += 5;
                        if (secondRace.isRaceProperty(RaceProperty.HOSTILE))
                            bonus -= 20;
                    }
                    if (firstRace.isRaceProperty(RaceProperty.SCIENTIFIC)) {
                        base += 5;
                        if (secondRace.isRaceProperty(RaceProperty.FINANCIAL))
                            bonus += 10;
                        if (secondRace.isRaceProperty(RaceProperty.WARLIKE))
                            bonus -= 15;
                        if (secondRace.isRaceProperty(RaceProperty.AGRARIAN))
                            bonus += 0;
                        if (secondRace.isRaceProperty(RaceProperty.INDUSTRIAL))
                            bonus += 15;
                        if (secondRace.isRaceProperty(RaceProperty.SECRET))
                            bonus -= 25;
                        if (secondRace.isRaceProperty(RaceProperty.SCIENTIFIC))
                            bonus += 25;
                        if (secondRace.isRaceProperty(RaceProperty.PRODUCER))
                            bonus += 15;
                        if (secondRace.isRaceProperty(RaceProperty.PACIFIST))
                            bonus += 5;
                        if (secondRace.isRaceProperty(RaceProperty.SNEAKY))
                            bonus -= 20;
                        if (secondRace.isRaceProperty(RaceProperty.SOLOING))
                            bonus -= 25;
                        if (secondRace.isRaceProperty(RaceProperty.HOSTILE))
                            bonus -= 40;
                    }
                    if (firstRace.isRaceProperty(RaceProperty.PRODUCER)) {
                        base += 5;
                        if (secondRace.isRaceProperty(RaceProperty.FINANCIAL))
                            bonus += 0;
                        if (secondRace.isRaceProperty(RaceProperty.WARLIKE))
                            bonus += 10;
                        if (secondRace.isRaceProperty(RaceProperty.AGRARIAN))
                            bonus += 0;
                        if (secondRace.isRaceProperty(RaceProperty.INDUSTRIAL))
                            bonus += 15;
                        if (secondRace.isRaceProperty(RaceProperty.SECRET))
                            bonus -= 10;
                        if (secondRace.isRaceProperty(RaceProperty.SCIENTIFIC))
                            bonus += 0;
                        if (secondRace.isRaceProperty(RaceProperty.PRODUCER))
                            bonus += 35;
                        if (secondRace.isRaceProperty(RaceProperty.PACIFIST))
                            bonus += 0;
                        if (secondRace.isRaceProperty(RaceProperty.SNEAKY))
                            bonus -= 10;
                        if (secondRace.isRaceProperty(RaceProperty.SOLOING))
                            bonus -= 15;
                        if (secondRace.isRaceProperty(RaceProperty.HOSTILE))
                            bonus -= 20;
                    }
                    if (firstRace.isRaceProperty(RaceProperty.PACIFIST)) {
                        base += 25;
                        if (secondRace.isRaceProperty(RaceProperty.FINANCIAL))
                            bonus += 10;
                        if (secondRace.isRaceProperty(RaceProperty.WARLIKE))
                            bonus -= 55;
                        if (secondRace.isRaceProperty(RaceProperty.AGRARIAN))
                            bonus += 30;
                        if (secondRace.isRaceProperty(RaceProperty.INDUSTRIAL))
                            bonus += 0;
                        if (secondRace.isRaceProperty(RaceProperty.SECRET))
                            bonus -= 10;
                        if (secondRace.isRaceProperty(RaceProperty.SCIENTIFIC))
                            bonus += 0;
                        if (secondRace.isRaceProperty(RaceProperty.PRODUCER))
                            bonus += 0;
                        if (secondRace.isRaceProperty(RaceProperty.PACIFIST))
                            bonus += 35;
                        if (secondRace.isRaceProperty(RaceProperty.SNEAKY))
                            bonus -= 35;
                        if (secondRace.isRaceProperty(RaceProperty.SOLOING))
                            bonus -= 25;
                        if (secondRace.isRaceProperty(RaceProperty.HOSTILE))
                            bonus -= 100;
                    }
                    if (firstRace.isRaceProperty(RaceProperty.SNEAKY)) {
                        base -= 20;
                        if (secondRace.isRaceProperty(RaceProperty.FINANCIAL))
                            bonus += 0;
                        if (secondRace.isRaceProperty(RaceProperty.WARLIKE))
                            bonus += 0;
                        if (secondRace.isRaceProperty(RaceProperty.AGRARIAN))
                            bonus += 0;
                        if (secondRace.isRaceProperty(RaceProperty.INDUSTRIAL))
                            bonus += 0;
                        if (secondRace.isRaceProperty(RaceProperty.SECRET))
                            bonus += 20;
                        if (secondRace.isRaceProperty(RaceProperty.SCIENTIFIC))
                            bonus += 0;
                        if (secondRace.isRaceProperty(RaceProperty.PRODUCER))
                            bonus += 0;
                        if (secondRace.isRaceProperty(RaceProperty.PACIFIST))
                            bonus += 0;
                        if (secondRace.isRaceProperty(RaceProperty.SNEAKY))
                            bonus += 40;
                        if (secondRace.isRaceProperty(RaceProperty.SOLOING))
                            bonus -= 10;
                        if (secondRace.isRaceProperty(RaceProperty.HOSTILE))
                            bonus -= 20;
                    }
                    if (firstRace.isRaceProperty(RaceProperty.SOLOING)) {
                        base -= 25;
                        if (secondRace.isRaceProperty(RaceProperty.FINANCIAL))
                            bonus -= 20;
                        if (secondRace.isRaceProperty(RaceProperty.WARLIKE))
                            bonus += 0;
                        if (secondRace.isRaceProperty(RaceProperty.AGRARIAN))
                            bonus += 0;
                        if (secondRace.isRaceProperty(RaceProperty.INDUSTRIAL))
                            bonus += 0;
                        if (secondRace.isRaceProperty(RaceProperty.SECRET))
                            bonus += 15;
                        if (secondRace.isRaceProperty(RaceProperty.SCIENTIFIC))
                            bonus += 0;
                        if (secondRace.isRaceProperty(RaceProperty.PRODUCER))
                            bonus += 0;
                        if (secondRace.isRaceProperty(RaceProperty.PACIFIST))
                            bonus += 5;
                        if (secondRace.isRaceProperty(RaceProperty.SNEAKY))
                            bonus += 0;
                        if (secondRace.isRaceProperty(RaceProperty.SOLOING))
                            bonus += 40;
                        if (secondRace.isRaceProperty(RaceProperty.HOSTILE))
                            bonus -= 40;
                    }
                    if (firstRace.isRaceProperty(RaceProperty.HOSTILE)) {
                        base -= 40;
                        if (secondRace.isRaceProperty(RaceProperty.PACIFIST))
                            bonus -= 10;
                    }
                    int rel = base + bonus;
                    if (rel > 101)
                        rel = 101;
                    else if (rel < 1)
                        rel = 1;

                    //iterate 7 times and make an average
                    int value = 0;
                    for (int k = 0; k < 7; k++)
                        value += (int) (RandUtil.random() * rel) * 2;
                    value /= 7;
                    firstRace.setRelation(secondKey, value);
                }
            }
    }

    /**
     * Returns the minor race which inhabits a certain sector
     * @param homeSector
     * @return
     */
    public Minor getMinorRace(String homeSector) {
        for (Iterator<Entry<String, Race>> iter = races.entries().iterator(); iter.hasNext();) {
            Entry<String, Race> e = iter.next();
            if (e.value.isMinor() && (e.value.getHomeSystemName().equals(homeSector)))
                return (Minor) e.value;
        }
        return null;
    }

    public void removeRace(String raceID) {
        Race r = races.get(raceID);
        r.delete();
        if (r.isMajor())
            majors.removeKey(raceID);
        else
            minors.removeKey(raceID);
        IntPoint coord = r.getCoordinates();
        if (!coord.equals(new IntPoint(-1, -1)))
            resourceManager.getUniverseMap().getStarSystemAt(coord.x, coord.y).setHomeOf("");
        races.removeKey(raceID);
    }

    public void setPlayerRace(String race) {
        playerRace = race;
    }

    public Major getPlayerRace() {
        return Major.toMajor(races.get(playerRace));
    }

    public String getPlayerRaceString() {
        return playerRace;
    }

    public void saveRaceController(Kryo kryo, Output output) {
        kryo.writeObject(output, races);
    }

    @SuppressWarnings("unchecked")
    public void loadRaceController(Kryo kryo, Input input) {
        races.clear();
        races = kryo.readObject(input, ArrayMap.class);
        minors.clear();
        majors.clear();
        for (Iterator<Entry<String, Race>> iter = races.entries().iterator(); iter.hasNext();) {
            Entry<String, Race> e = iter.next();
            e.value.setResourceManager(resourceManager);
            if (e.value.isMajor()) {
                majors.put(e.key, (Major) e.value);
                e.value.setDiplomacyAI(new MajorAI(resourceManager, e.value));
            } else {
                minors.put(e.key, (Minor) e.value);
                e.value.setDiplomacyAI(new MinorAI(resourceManager, e.value));
            }
        }
    }
}
