/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.races;

import java.util.Arrays;

import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.ResearchComplexType;
import com.blotunga.bote.constants.ResearchStatus;
import com.blotunga.bote.constants.ResearchType;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.utils.RandUtil;

public class Research {
    // current research levels:
    private int bioTech;			// current biotech level of the empire
    private int energyTech;			// current energy technology level of the empire
    private int computerTech;		// current computer technology level of the empire
    private int propulsionTech;		// current propulsion technology level of the empire
    private int constructionTech;	// current construction technology level of the empire
    private int weaponTech;			// current weapon technology level of the empire
    private int uniqueTech;			// current unique technology level of the empire
    //percentile balances of each of the research areas
    private int bioPercentage;
    private int energyPercentage;
    private int computerPercentage;
    private int propulsionPercentage;
    private int constructionPercentage;
    private int weaponPercentage;
    private int uniquePercentage;
    //current research points for the different areas
    private float bioRP;
    private float energyRP;
    private float computerRP;
    private float propulsionRP;
    private float constructionRP;
    private float weaponRP;
    private float uniqueRP;
    //if the research is locked, we can't modify the percentile value
    private boolean bioLocked;
    private boolean energyLocked;
    private boolean computerLocked;
    private boolean propulsionLocked;
    private boolean constructionLocked;
    private boolean weaponLocked;
    private boolean uniqueLocked;
    //bonus for the research areas
    private int bioTechBoni;
    private int energyTechBoni;
    private int computerTechBoni;
    private int propulsionTechBoni;
    private int constructionTechBoni;
    private int weaponTechBoni;
    private ResearchInfo researchInfo;
    private boolean uniqueReady;	// is a unique research ready, we can start the next one
    private int numberOfUnique;		// counter of the unique research
    String[] strMessage; // shows what was researched

    public Research() {
        researchInfo = new ResearchInfo();
        strMessage = new String[8];
        reset();
    }

    private void reset() {
        bioTech = 0;
        energyTech = 0;
        computerTech = 0;
        propulsionTech = 0;
        constructionTech = 0;
        weaponTech = 0;
        uniqueTech = 0;

        for (int i = 0; i < ResearchType.UNIQUE.getType(); i++)
            researchInfo.setTechInfos(i, 1);
        bioPercentage = 16;
        energyPercentage = 16;
        computerPercentage = 17;
        propulsionPercentage = 17;
        constructionPercentage = 17;
        weaponPercentage = 17;
        uniquePercentage = 0;

        bioRP = 0;
        energyRP = 0;
        computerRP = 0;
        propulsionRP = 0;
        constructionRP = 0;
        weaponRP = 0;
        uniqueRP = 0;

        bioLocked = false;
        energyLocked = false;
        computerLocked = false;
        constructionLocked = false;
        propulsionLocked = false;
        weaponLocked = false;

        bioTechBoni = 0;
        energyTechBoni = 0;
        computerTechBoni = 0;
        propulsionTechBoni = 0;
        constructionTechBoni = 0;
        weaponTechBoni = 0;

        uniqueReady = true;
        numberOfUnique = 1;

        Arrays.fill(strMessage, "");
        for (int i = 0; i < ResearchComplexType.COMPLEX_COUNT.getType(); i++)
            researchInfo.researchComplex[i].reset();
        researchInfo.choiceTaken = false;
        researchInfo.currentComplex = ResearchComplexType.NONE;
    }

    public int getBioTech() {
        return bioTech;
    }

    public int getEnergyTech() {
        return energyTech;
    }

    public int getComputerTech() {
        return computerTech;
    }

    public int getPropulsionTech() {
        return propulsionTech;
    }

    public int getConstructionTech() {
        return constructionTech;
    }

    public int getWeaponTech() {
        return weaponTech;
    }

    public int getUniqueTech() {
        return uniqueTech;
    }

    public int getBioPercentage() {
        return bioPercentage;
    }

    public int getEnergyPercentage() {
        return energyPercentage;
    }

    public int getComputerPercentage() {
        return computerPercentage;
    }

    public int getPropulsionPercentage() {
        return propulsionPercentage;
    }

    public int getConstructionPercentage() {
        return constructionPercentage;
    }

    public int getWeaponPercentage() {
        return weaponPercentage;
    }

    public int getUniquePercentage() {
        return uniquePercentage;
    }

    public int getBioRP() {
        return (int) bioRP;
    }

    public int getEnergyRP() {
        return (int) energyRP;
    }

    public int getComputerRP() {
        return (int) computerRP;
    }

    public int getPropulsionRP() {
        return (int) propulsionRP;
    }

    public int getConstructionRP() {
        return (int) constructionRP;
    }

    public int getWeaponRP() {
        return (int) weaponRP;
    }

    public int getUniqueRP() {
        return (int) uniqueRP;
    }

    public int getBioTechBoni() {
        return bioTechBoni;
    }

    public int getEnergyTechBoni() {
        return energyTechBoni;
    }

    public int getComputerTechBoni() {
        return computerTechBoni;
    }

    public int getPropulsionTechBoni() {
        return propulsionTechBoni;
    }

    public int getConstructionTechBoni() {
        return constructionTechBoni;
    }

    public int getWeaponTechBoni() {
        return weaponTechBoni;
    }

    public boolean getLockStatus(ResearchType tech) {
        switch (tech) {
            case BIO:
                return bioLocked;
            case ENERGY:
                return energyLocked;
            case COMPUTER:
                return computerLocked;
            case CONSTRUCTION:
                return constructionLocked;
            case PROPULSION:
                return propulsionLocked;
            case WEAPON:
                return weaponLocked;
            case UNIQUE:
                return uniqueLocked;
            default:
                return false;
        }
    }

    public boolean isUniqueReady() {
        return uniqueReady;
    }

    public int getNumberOfUnique() {
        return numberOfUnique;
    }

    public ResearchInfo getResearchInfo() {
        return researchInfo;
    }

    public void setBioTech(int bioTech) {
        this.bioTech = bioTech;
    }

    public void setEnergyTech(int energyTech) {
        this.energyTech = energyTech;
    }

    public void setComputerTech(int computerTech) {
        this.computerTech = computerTech;
    }

    public void setPropulsionTech(int propulsionTech) {
        this.propulsionTech = propulsionTech;
    }

    public void setConstructionTech(int constructionTech) {
        this.constructionTech = constructionTech;
    }

    public void setWeaponTech(int weaponTech) {
        this.weaponTech = weaponTech;
    }

    public void setPercentage(ResearchType tech, int percentage) {
        int diff = 0;  // difference of the modified part
        int numberoflocks = 0; // number of locked fields
        // check which fields are locked, and subtract or add these point to the difference
        int difflock = 0;
        if (bioLocked) {
            difflock += bioPercentage;
            numberoflocks++;
        }
        if (energyLocked) {
            difflock += energyPercentage;
            numberoflocks++;
        }
        if (computerLocked) {
            difflock += computerPercentage;
            numberoflocks++;
        }
        if (constructionLocked) {
            difflock += constructionPercentage;
            numberoflocks++;
        }
        if (propulsionLocked) {
            difflock += propulsionPercentage;
            numberoflocks++;
        }
        if (weaponLocked) {
            difflock += weaponPercentage;
            numberoflocks++;
        }
        if (uniqueLocked) {
            difflock += uniquePercentage;
            if (!uniqueReady)
                numberoflocks++;
        }

        // if the unique is not selected and there are 5 fields locked, then we can't modify anything
        if (uniqueReady && (numberoflocks == 5))
            tech = ResearchType.NONE;
        // if the unique is selected and 6 fields are locked, we can't modify anything
        if (!uniqueReady && (numberoflocks == 6))
            tech = ResearchType.NONE;
        switch (tech) {
            case BIO:
                if (bioLocked == false) {
                    diff = bioPercentage;
                    diff = percentage - diff;
                    bioPercentage = percentage;
                    if (bioPercentage > (100 - difflock))
                        bioPercentage = 100 - difflock;
                }
                break;
            case ENERGY:
                if (energyLocked == false) {
                    diff = energyPercentage;
                    diff = percentage - diff;
                    energyPercentage = percentage;
                    if (energyPercentage > (100 - difflock))
                        energyPercentage = 100 - difflock;
                }
                break;
            case COMPUTER:
                if (computerLocked == false) {
                    diff = computerPercentage;
                    diff = percentage - diff;
                    computerPercentage = percentage;
                    if (computerPercentage > (100 - difflock))
                        computerPercentage = 100 - difflock;
                }
                break;
            case CONSTRUCTION:
                if (constructionLocked == false) {
                    diff = constructionPercentage;
                    diff = percentage - diff;
                    constructionPercentage = percentage;
                    if (constructionPercentage > (100 - difflock))
                        constructionPercentage = 100 - difflock;
                }
                break;
            case PROPULSION:
                if (propulsionLocked == false) {
                    diff = propulsionPercentage;
                    diff = percentage - diff;
                    propulsionPercentage = percentage;
                    if (propulsionPercentage > (100 - difflock))
                        propulsionPercentage = 100 - difflock;
                }
                break;
            case WEAPON:
                if (weaponLocked == false) {
                    diff = weaponPercentage;
                    diff = percentage - diff;
                    weaponPercentage = percentage;
                    if (weaponPercentage > (100 - difflock))
                        weaponPercentage = 100 - difflock;
                }
                break;
            case UNIQUE:
                if (uniqueLocked == false && uniqueReady == false) {
                    diff = uniquePercentage;
                    diff = percentage - diff;
                    uniquePercentage = percentage;
                    if (uniquePercentage > (100 - difflock))
                        uniquePercentage = 100 - difflock;
                }
                break;
            default:
                diff = 0;
                break;
        }
        // if we have raised a research, take the points from the others
        while (diff > 0) {
            int changediff = diff;
            if (tech != ResearchType.BIO && bioPercentage > 0 && bioLocked == false) {
                bioPercentage--;
                diff--;
                if (diff == 0)
                    break;
            }
            if (tech != ResearchType.ENERGY && energyPercentage > 0 && energyLocked == false) {
                energyPercentage--;
                diff--;
                if (diff == 0)
                    break;
            }
            if (tech != ResearchType.COMPUTER && computerPercentage > 0 && computerLocked == false) {
                computerPercentage--;
                diff--;
                if (diff == 0)
                    break;
            }
            if (tech != ResearchType.CONSTRUCTION && constructionPercentage > 0 && constructionLocked == false) {
                constructionPercentage--;
                diff--;
                if (diff == 0)
                    break;
            }
            if (tech != ResearchType.PROPULSION && propulsionPercentage > 0 && propulsionLocked == false) {
                propulsionPercentage--;
                diff--;
                if (diff == 0)
                    break;
            }
            if (tech != ResearchType.WEAPON && weaponPercentage > 0 && weaponLocked == false) {
                weaponPercentage--;
                diff--;
                if (diff == 0)
                    break;
            }
            if (tech != ResearchType.UNIQUE && uniquePercentage > 0 && uniqueReady == false && uniqueLocked == false) {
                uniquePercentage--;
                diff--;
                if (diff == 0)
                    break;
            }
            // avoid deadlock
            if (changediff == diff)
                break;
        }
        // if we have lowered a research, give the points from the others
        while (diff < 0 && diff <= difflock) {
            int changediff = diff;
            if (tech != ResearchType.BIO && bioPercentage < 100 && bioLocked == false) {
                bioPercentage++;
                diff++;
                if (diff == 0)
                    break;
            }
            if (tech != ResearchType.ENERGY && energyPercentage < 100 && energyLocked == false) {
                energyPercentage++;
                diff++;
                if (diff == 0)
                    break;
            }
            if (tech != ResearchType.COMPUTER && computerPercentage < 100 && computerLocked == false) {
                computerPercentage++;
                diff++;
                if (diff == 0)
                    break;
            }
            if (tech != ResearchType.CONSTRUCTION && constructionPercentage < 100 && constructionLocked == false) {
                constructionPercentage++;
                diff++;
                if (diff == 0)
                    break;
            }
            if (tech != ResearchType.PROPULSION && propulsionPercentage < 100 && propulsionLocked == false) {
                propulsionPercentage++;
                diff++;
                if (diff == 0)
                    break;
            }
            if (tech != ResearchType.WEAPON && weaponPercentage < 100 && weaponLocked == false) {
                weaponPercentage++;
                diff++;
                if (diff == 0)
                    break;
            }
            if (tech != ResearchType.UNIQUE && uniquePercentage < 100 && uniqueReady == false && uniqueLocked == false) {
                uniquePercentage++;
                diff++;
                if (diff == 0)
                    break;
            }
            // avoid deadlock
            if (changediff == diff)
                break;
        }
    }

    public void setLock(ResearchType tech, boolean locked) {
        switch (tech) {
            case BIO:
                bioLocked = locked;
                break;
            case ENERGY:
                energyLocked = locked;
                break;
            case COMPUTER:
                computerLocked = locked;
                break;
            case CONSTRUCTION:
                constructionLocked = locked;
                break;
            case PROPULSION:
                propulsionLocked = locked;
                break;
            case WEAPON:
                weaponLocked = locked;
                break;
            case UNIQUE:
                uniqueLocked = locked;
                break;
            default:
                break;
        }
    }

    /**
     * Function sets the already researched research points in a field to the value from the second param
     * @param tech
     * @param rp
     */
    public void setRP(ResearchType tech, int rp) {
        switch (tech) {
            case BIO:
                bioRP = rp;
                break;
            case ENERGY:
                energyRP = rp;
                break;
            case COMPUTER:
                computerRP = rp;
                break;
            case CONSTRUCTION:
                constructionRP = rp;
                break;
            case PROPULSION:
                propulsionRP = rp;
                break;
            case WEAPON:
                weaponRP = rp;
                break;
            case UNIQUE:
                uniqueRP = rp;
                break;
            default:
                break;
        }
    }

    public void setResearchBoni(int[] researchBoni) {
        bioTechBoni = researchBoni[0];
        energyTechBoni = researchBoni[1];
        computerTechBoni = researchBoni[2];
        propulsionTechBoni = researchBoni[3];
        constructionTechBoni = researchBoni[4];
        weaponTechBoni = researchBoni[5];
    }

    private void calcExtraBonuses() {
        if (researchInfo.getResearchComplex(ResearchComplexType.RESEARCH).getFieldStatus(1) == ResearchStatus.RESEARCHED) {
            bioTechBoni += researchInfo.getResearchComplex(ResearchComplexType.RESEARCH).getBonus(1);
            energyTechBoni += researchInfo.getResearchComplex(ResearchComplexType.RESEARCH).getBonus(1);
        } else if (researchInfo.getResearchComplex(ResearchComplexType.RESEARCH).getFieldStatus(2) == ResearchStatus.RESEARCHED) {
            computerTechBoni += researchInfo.getResearchComplex(ResearchComplexType.RESEARCH).getBonus(2);
            propulsionTechBoni += researchInfo.getResearchComplex(ResearchComplexType.RESEARCH).getBonus(2);
        } else if (researchInfo.getResearchComplex(ResearchComplexType.RESEARCH).getFieldStatus(3) == ResearchStatus.RESEARCHED) {
            constructionTechBoni += researchInfo.getResearchComplex(ResearchComplexType.RESEARCH).getBonus(3);
            weaponTechBoni += researchInfo.getResearchComplex(ResearchComplexType.RESEARCH).getBonus(3);
        }
    }

    /**
     * This function calculates everything which is related to the research of an empire.
     * This is called in the NextRound function, and the parameters are the current research points
     * of the empire.
     * @param rp
     * @return messages[] string array
     */
    public String[] calculateResearch(int rp, ResourceManager manager, String raceID) {
        // calculate extra bonuses
        double researchSpeedFactor = manager.getGamePreferences().researchSpeedFactor;
        calcExtraBonuses();
        Arrays.fill(strMessage, "");
        bioRP += rp * bioPercentage / 100.0f + (rp * bioPercentage) / 100.0f * bioTechBoni / 100.0f;
        energyRP += rp * energyPercentage / 100.0f + (rp * energyPercentage) / 100.0f * energyTechBoni / 100.0f;
        computerRP += rp * computerPercentage / 100.0f + (rp * computerPercentage) / 100.0f * computerTechBoni / 100.0f;
        propulsionRP += rp * propulsionPercentage / 100.0f + (rp * propulsionPercentage) / 100.0f * propulsionTechBoni
                / 100.0f;
        constructionRP += rp * constructionPercentage / 100.0f + (rp * constructionPercentage) / 100.0f
                * constructionTechBoni / 100.0f;
        weaponRP += rp * weaponPercentage / 100.0f + (rp * weaponPercentage) / 100.0f * weaponTechBoni / 100.0f;

        if (researchInfo.getChoiceTaken())
            uniqueRP += rp * uniquePercentage / 100.0f;

        // check if a new level was reached
        if ((int) (RandUtil.random() * 100 + 1) <= (Math.pow(
                (bioRP / researchInfo.getBio(bioTech, researchSpeedFactor)), 10) * 100)) {
            strMessage[ResearchType.BIO.getType()] = StringDB.getString(ResearchType.BIO.getName());
            bioTech++;
            bioRP = 0;
            researchInfo.setTechInfos(ResearchType.BIO.getType(), bioTech + 1);
        }
        if ((int) (RandUtil.random() * 100 + 1) <= (Math.pow(
                (energyRP / researchInfo.getEnergy(energyTech, researchSpeedFactor)), 10) * 100)) {
            strMessage[ResearchType.ENERGY.getType()] = StringDB.getString(ResearchType.ENERGY.getName());
            energyTech++;
            energyRP = 0;
            researchInfo.setTechInfos(ResearchType.ENERGY.getType(), energyTech + 1);
        }
        if ((int) (RandUtil.random() * 100 + 1) <= (Math.pow(
                (computerRP / researchInfo.getComputer(computerTech, researchSpeedFactor)), 10) * 100)) {
            strMessage[ResearchType.COMPUTER.getType()] = StringDB.getString(ResearchType.COMPUTER.getName());
            computerTech++;
            computerRP = 0;
            researchInfo.setTechInfos(ResearchType.COMPUTER.getType(), computerTech + 1);
        }
        if ((int) (RandUtil.random() * 100 + 1) <= (Math.pow(
                (propulsionRP / researchInfo.getPropulsion(propulsionTech, researchSpeedFactor)), 10) * 100)) {
            strMessage[ResearchType.PROPULSION.getType()] = StringDB.getString(ResearchType.PROPULSION.getName());
            propulsionTech++;
            propulsionRP = 0;
            researchInfo.setTechInfos(ResearchType.PROPULSION.getType(), propulsionTech + 1);
        }
        if ((int) (RandUtil.random() * 100 + 1) <= (Math.pow(
                (constructionRP / researchInfo.getConstruction(constructionTech, researchSpeedFactor)), 10) * 100)) {
            strMessage[ResearchType.CONSTRUCTION.getType()] = StringDB.getString(ResearchType.CONSTRUCTION.getName());
            constructionTech++;
            constructionRP = 0;
            researchInfo.setTechInfos(ResearchType.CONSTRUCTION.getType(), constructionTech + 1);
        }
        if ((int) (RandUtil.random() * 100 + 1) <= (Math.pow(
                (weaponRP / researchInfo.getWeapon(weaponTech, researchSpeedFactor)), 10) * 100)) {
            strMessage[ResearchType.WEAPON.getType()] = StringDB.getString(ResearchType.WEAPON.getName());
            weaponTech++;
            weaponRP = 0;
            researchInfo.setTechInfos(ResearchType.WEAPON.getType(), weaponTech + 1);
        }
        //handle unique research
        if ((int) (RandUtil.random() * 100 + 1) <= (Math.pow(
                (uniqueRP / ((researchInfo.getBio(numberOfUnique, researchSpeedFactor)
                        + researchInfo.getEnergy(numberOfUnique, researchSpeedFactor)
                        + researchInfo.getComputer(numberOfUnique, researchSpeedFactor)
                        + researchInfo.getPropulsion(numberOfUnique, researchSpeedFactor)
                        + researchInfo.getConstruction(numberOfUnique, researchSpeedFactor) + researchInfo.getWeapon(
                        numberOfUnique, researchSpeedFactor)) / GameConstants.SPECIAL_RESEARCH_DIV)), 10) * 100)) {
            uniqueRP = 0;
            manager.getUniverseMap().setFinishedSpecialResearch(raceID, researchInfo.getCurrentResearchComplexType());
            researchInfo.changeStatusOfComplex(ResearchStatus.RESEARCHED);
            // here has to be called the setPercentage() function to set the unique research back to 0
            // if all fields are locked, then unlock all automatically
            if (bioLocked && energyLocked && computerLocked && propulsionLocked && constructionLocked && weaponLocked) {
                bioLocked = false;
                energyLocked = false;
                computerLocked = false;
                constructionLocked = false;
                propulsionLocked = false;
                weaponLocked = false;
            }
            uniqueLocked = false;
            setPercentage(ResearchType.UNIQUE, 0);
            numberOfUnique++;
            strMessage[ResearchType.UNIQUE.getType()] = StringDB.getString(ResearchType.UNIQUE.getName());
            uniqueReady = true;
            // if "Research" was researched, then recalculate bonuses so that we have those stored
            if (researchInfo.getResearchComplex(ResearchComplexType.RESEARCH).getComplexStatus() == ResearchStatus.RESEARCHED)
                calcExtraBonuses();
        }
        // check if we have all research at the next level, if yes then select a new unique research
        if (uniqueReady && uniqueTech < bioTech && uniqueTech < energyTech && uniqueTech < computerTech
                && uniqueTech < propulsionTech && uniqueTech < constructionTech && uniqueTech < weaponTech) {
            if (numberOfUnique < ResearchComplexType.COMPLEX_COUNT.getType()) {
                uniqueReady = false;
                uniqueTech++;
                researchInfo.chooseUniqueResearch();
                strMessage[ResearchType.UNIQUE.getType() + 1] = StringDB.getString("SPECIAL_READY");
            }
        }
        return strMessage;
    }

    public int getResearchBoni(ResearchType rt) {
        switch (rt) {
            case BIO:
                return bioTechBoni;
            case COMPUTER:
                return computerTechBoni;
            case CONSTRUCTION:
                return constructionTechBoni;
            case ENERGY:
                return energyTechBoni;
            case PROPULSION:
                return propulsionTechBoni;
            case WEAPON:
                return weaponTechBoni;
            default:
                return 0;
        }
    }

    public int getResearchLevel(ResearchType rt) {
        switch (rt) {
            case BIO:
                return bioTech;
            case COMPUTER:
                return computerTech;
            case CONSTRUCTION:
                return constructionTech;
            case ENERGY:
                return energyTech;
            case PROPULSION:
                return propulsionTech;
            case WEAPON:
                return weaponTech;
            case UNIQUE:
                return uniqueTech;
            default:
                return 0;
        }
    }

    public int[] getResearchLevels() {
        int researchLevels[] = { bioTech, energyTech, computerTech, propulsionTech, constructionTech, weaponTech };
        return researchLevels;
    }

    public int getProgress(ResearchType rt, double researchSpeedFactor) {
        switch (rt) {
            case BIO:
                return (int) ((bioRP * 100) / researchInfo.getBio(bioTech, researchSpeedFactor));
            case COMPUTER:
                return (int) ((computerRP * 100) / researchInfo.getComputer(computerTech, researchSpeedFactor));
            case CONSTRUCTION:
                return (int) ((constructionRP * 100) / researchInfo.getConstruction(constructionTech,
                        researchSpeedFactor));
            case ENERGY:
                return (int) ((energyRP * 100) / researchInfo.getEnergy(energyTech, researchSpeedFactor));
            case PROPULSION:
                return (int) ((propulsionRP * 100) / researchInfo.getPropulsion(propulsionTech, researchSpeedFactor));
            case WEAPON:
                return (int) ((weaponRP * 100) / researchInfo.getWeapon(weaponTech, researchSpeedFactor));
            case UNIQUE:
                int level = getNumberOfUnique();
                long allOther = researchInfo.getBio(level, researchSpeedFactor)
                        + researchInfo.getComputer(level, researchSpeedFactor)
                        + researchInfo.getConstruction(level, researchSpeedFactor)
                        + researchInfo.getEnergy(level, researchSpeedFactor)
                        + researchInfo.getPropulsion(level, researchSpeedFactor)
                        + researchInfo.getWeapon(level, researchSpeedFactor);
                allOther /= GameConstants.SPECIAL_RESEARCH_DIV;
                return (int) ((uniqueRP * 100) / allOther);
            default:
                return 0;
        }
    }

    public int getPercentage(ResearchType rt) {
        switch (rt) {
            case BIO:
                return bioPercentage;
            case COMPUTER:
                return computerPercentage;
            case CONSTRUCTION:
                return constructionPercentage;
            case ENERGY:
                return energyPercentage;
            case PROPULSION:
                return propulsionPercentage;
            case WEAPON:
                return weaponPercentage;
            case UNIQUE:
                return uniquePercentage;
            default:
                return 0;
        }
    }

    public int getRP(ResearchType rt) {
        switch (rt) {
            case BIO:
                return (int) bioRP;
            case COMPUTER:
                return (int) computerRP;
            case CONSTRUCTION:
                return (int) constructionRP;
            case ENERGY:
                return (int) energyRP;
            case PROPULSION:
                return (int) propulsionRP;
            case WEAPON:
                return (int) weaponRP;
            case UNIQUE:
                return (int) uniqueRP;
            default:
                return 0;
        }
    }
}
