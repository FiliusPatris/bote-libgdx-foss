/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.races;

import java.util.Iterator;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.ObjectIntMap;
import com.badlogic.gdx.utils.ObjectMap.Entry;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.ai.MinorAI;
import com.blotunga.bote.constants.CombatTactics;
import com.blotunga.bote.constants.DiplomaticAgreement;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.PlayerRaces;
import com.blotunga.bote.constants.RaceProperty;
import com.blotunga.bote.constants.ResourceTypes;
import com.blotunga.bote.general.EmpireNews;
import com.blotunga.bote.general.StringDB;
import com.blotunga.bote.general.EmpireNews.EmpireNewsType;
import com.blotunga.bote.ships.ShipInfo;
import com.blotunga.bote.ships.Ships;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.utils.IntPoint;
import com.blotunga.bote.utils.RandUtil;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

public class Minor extends Race {
    // read from file
    protected int technologicalProgress;
    protected int corruptibility;
    protected boolean spaceFlight;

    private boolean subjugated;
    private ObjectIntMap<String> acceptance;

    public enum RaceModType {
        RACE_MOD_TYPE_FOOD,
        RACE_MOD_TYPE_INDUSTRY,
        RACE_MOD_TYPE_ENERGY,
        RACE_MOD_TYPE_SECURITY,
        RACE_MOD_TYPE_RESEARCH,
        RACE_MOD_TYPE_ALL_RESOURCES,
        RACE_MOD_TYPE_CREDITS
    }

    public Minor() {
        this((ResourceManager) null);
    }

    public Minor(ResourceManager res, RaceType rt) {
        super(rt, res);
        acceptance = new ObjectIntMap<String>();
        technologicalProgress = 0;
        corruptibility = 0;
        spaceFlight = false;
        subjugated = false;
    }

    public Minor(ResourceManager res) {
        super(RaceType.RACE_TYPE_MINOR, res);
        acceptance = new ObjectIntMap<String>();
        technologicalProgress = 0;
        corruptibility = 0;
        spaceFlight = false;
        subjugated = false;
    }

    public Minor(Minor other) {
        super(other);
        this.acceptance = new ObjectIntMap<String>(other.acceptance);
        this.technologicalProgress = other.technologicalProgress;
        this.corruptibility = other.corruptibility;
        this.spaceFlight = other.spaceFlight;
        this.subjugated = other.subjugated;
    }

    @Override
    public int create(String[] info, int pos) {
        raceID = info[pos++].trim().replaceFirst(":", "");
        homeSystem = raceID.toLowerCase();
        String firstLetter = homeSystem.substring(0, 1);
        homeSystem = homeSystem.replaceFirst(firstLetter, firstLetter.toUpperCase());
        graphicFile = info[pos++].trim().replaceFirst(".jpg", "");
        technologicalProgress = Integer.parseInt(info[pos++].trim());
        String raceProperties = info[pos++].trim();
        String[] racePropertyArray = raceProperties.split(",");
        for (int i = 0; i < racePropertyArray.length; i++) {
            RaceProperty prop = RaceProperty.fromInt(Integer.parseInt(racePropertyArray[i].trim()));
            setRaceProperty(prop, true);
        }

        spaceFlight = Integer.parseInt(info[pos++].trim()) != 0;
        corruptibility = Integer.parseInt(info[pos++].trim());
        shipNumber = PlayerRaces.MINORNUMBER.getType();
        diplomacyAI = new MinorAI(resourceManager, this);
        return pos;
    }

    public int getTechnologicalProgress() {
        return technologicalProgress;
    }

    public String getTechnologicalProgressAsString() {
        return getTechnologicalProgressAsString(technologicalProgress);
    }

    public static String getTechnologicalProgressAsString(int value) {
        String progress = "";
        switch (value) {
            case 0:
                progress = StringDB.getString("VERY_UNDERDEVELOPED");
                break;
            case 1:
                progress = StringDB.getString("UNDERDEVELOPED");
                break;
            case 2:
                progress = StringDB.getString("NORMAL_DEVELOPED");
                break;
            case 3:
                progress = StringDB.getString("DEVELOPED");
                break;
            case 4:
                progress = StringDB.getString("VERY_DEVELOPED");
                break;
        }
        return progress;
    }

    public int getCorruptibility() {
        return corruptibility;
    }

    public String getCorruptibilityAsString() {
        return getCorruptibilityAsString(corruptibility);
    }

    public static String getCorruptibilityAsString(int value) {
        String corruptibilityS = "";
        switch (value) {
            case 0:
                corruptibilityS = StringDB.getString("VERY_LOW_CORRUPTIBILITY");
                break;
            case 1:
                corruptibilityS = StringDB.getString("LOW_CORRUPTIBILITY");
                break;
            case 2:
                corruptibilityS = StringDB.getString("NORMAL_CORRUPTIBILITY");
                break;
            case 3:
                corruptibilityS = StringDB.getString("HIGH_CORRUPTIBILITY");
                break;
            case 4:
                corruptibilityS = StringDB.getString("VERY_HIGH_CORRUPTIBILITY");
                break;
        }
        return corruptibilityS;
    }

    public boolean getSpaceFlightNation() {
        return spaceFlight;
    }

    public boolean isSubjugated() {
        return subjugated;
    }

    public int getAcceptancePoints(String raceId) {
        return acceptance.get(raceId, 0);
    }

    @Override
    public void getTooltip(Table table, Skin skin, String headerFont, Color headerColor, String textFont, Color textColor, boolean withDescription) {
        super.getTooltip(table, skin, headerFont, headerColor, textFont, textColor, false); //false because we want the description at the end
        String text = StringDB.getString("TECHNICAL_PROGRESS");
        Label l = new Label(text, skin, textFont, headerColor);
        table.add(l);
        table.row();

        text = getTechnologicalProgressAsString();
        l = new Label(text, skin, textFont, textColor);
        table.add(l);
        table.row();

        text = StringDB.getString("CORRUPTIBILITY");
        l = new Label(text, skin, textFont, headerColor);
        table.add(l);
        table.row();

        text = getCorruptibilityAsString();
        l = new Label(text, skin, textFont, textColor);
        table.add(l);
        table.row();

        if(withDescription) {
            text = getDescription();
            l = new Label(text, skin, textFont, textColor);
            l.setWrap(true);
            l.setAlignment(Align.center);
            table.add(l).width(GameConstants.wToRelative(600));
        }
    }

    public void setTechnologicalProgress(int technologicalProgress) {
        this.technologicalProgress = technologicalProgress;
    }

    public void setCorruptibility(int corruptibility) {
        this.corruptibility = corruptibility;
    }

    public void setSpaceFlight() {
        setSpaceFlight(true);
    }

    public void setSpaceFlight(boolean spaceFlight) {
        this.spaceFlight = spaceFlight;
    }

    public void setAcceptancePoints(String raceID, int add) {
        if (add == 0)
            return;
        int tmpAcceptance = acceptance.get(raceID, 0) + add;
        tmpAcceptance = Math.min(5000, tmpAcceptance);
        tmpAcceptance = Math.max(0, tmpAcceptance);
        if (tmpAcceptance == 0)
            acceptance.remove(raceID, 0);
        else
            acceptance.put(raceID, tmpAcceptance);
    }

    public void setSubjugated(boolean subjugated) {
        this.subjugated = subjugated;
    }

    /**
     * This function lets a minor race to colonize a planet in its home system with a degree of probability
     * @return
     */
    public boolean perhapsExtend() {
        StarSystem system = resourceManager.getUniverseMap().getStarSystemAt(coordinates);
        if (!system.getOwnerId().equals(raceID))
            return false;
        boolean colonized = system.perhapsMinorExtends(technologicalProgress);
        if (colonized)
            system.setInhabitants(system.getCurrentInhabitants());
        return colonized;
    }

    /**
     * This function decides if a minor race can build a ship or not
     */
    public void perhapsBuildShip() {
        if (!spaceFlight)
            return;
        if (coordinates.equals(new IntPoint()))
            return;

        StarSystem system = resourceManager.getUniverseMap().getStarSystemAt(coordinates);
        if (!system.getOwnerId().equals(raceID))
            return;
        for (int i = 0; i < resourceManager.getShipInfos().size; i++) {
            ShipInfo shipInfo = resourceManager.getShipInfos().get(i);
            if (shipInfo.getRace() == PlayerRaces.MINORNUMBER.getType()) {
                if (shipInfo.getOnlyInSystem().equals(getHomeSystemName())) {
                    int avgTechLevel = resourceManager.getStatistics().getAverageTechLevel();
                    int techLevel = avgTechLevel + technologicalProgress / 2;

                    int[] researchLevels = { techLevel, techLevel, techLevel, techLevel, techLevel, techLevel };
                    if (shipInfo.isThisShipBuildableNow(researchLevels)) {
                        //calculate probability
                        int number = 1;
                        for (int j = 0; j < resourceManager.getUniverseMap().getShipMap().getSize(); j++) {
                            Ships ship = resourceManager.getUniverseMap().getShipMap().getAt(j);
                            if (ship.getOwnerId().equals(raceID) && ship.getShipClass().equals(shipInfo.getShipClass()))
                                number++;
                        }
                        number *= 5;
                        int rnd = (int) (RandUtil.random() * number);
                        if (rnd == 0) {
                            int key = resourceManager.getUniverseMap().buildShip(shipInfo.getID(), getCoordinates(),
                                    raceID);
                            Ships ship = resourceManager.getUniverseMap().getShipMap().getShipMap().get(key);
                            //for non-agressive races set to AVOID
                            if (!isRaceProperty(RaceProperty.HOSTILE)
                                    && !isRaceProperty(RaceProperty.WARLIKE)
                                    && !isRaceProperty(RaceProperty.SNEAKY)
                                    && !isRaceProperty(RaceProperty.SECRET)) {
                                ship.unsetCurrentOrder();
                                ship.setCombatTactics(CombatTactics.CT_AVOID);
                            }
                            return;
                        }
                    }
                }
            }
        }
    }

    /**
     * Function calculates the number of acceptance points that a majorrace gets for a longer relationship
     * The more points they have, the harder it is to influence this race by a different major
     */
    public void calcAcceptancePoints() {
        ArrayMap<String, Major> majors = resourceManager.getRaceController().getMajors();
        for (Iterator<Entry<String, Major>> iter = majors.entries().iterator(); iter.hasNext();) {
            Entry<String, Major> e = iter.next();
            if (!isRaceContacted(e.key))
                continue;

            DiplomaticAgreement agreement = getAgreement(e.key);
            int accPoints = 0;
            switch (agreement) {
                case NONE: //if there is no current agreement, then the points are reduced slowly
                    accPoints -= ((int) (RandUtil.random() * 80) + 1);
                    break;
                case FRIENDSHIP:
                    accPoints += 10;
                    break;
                case COOPERATION:
                    accPoints += 20;
                    break;
                case ALLIANCE:
                    accPoints += 30;
                    break;
                case MEMBERSHIP:
                    accPoints += 40;
                    //in case of membership the relation might also get better
                    setRelation(e.key, (int) (RandUtil.random() * 2));
                    break;
                case WAR:
                    accPoints -= getAcceptancePoints(e.key);
                    break;
                default:
                    break;
            }
            setAcceptancePoints(e.key, accPoints);
        }
    }

    /**
     * Function calculates the number of recources per turn (called in nextRound()) consumed by a minor race
     * If the minor race can produce the resource by themselves then the consumption os lower of course
     */
    public void consumeResources() {
        StarSystem system = resourceManager.getUniverseMap().getStarSystemAt(coordinates);
        boolean[] exist = system.getAvailableResources(true);
        int div;
        int value;

        for (int i = 0; i < ResourceTypes.DERITIUM.getType(); i++) {
            int min = i == 0 ? 1000 : 1500; //Titan only with 1000;
            div = exist[i] ? min : 4000;
            value = (int) (RandUtil.random() * div);
            value = Math.min(3000, value);
            system.addResourceStore(i, -value);
        }
        value = (int) (RandUtil.random() * 2);
        system.addResourceStore(ResourceTypes.DERITIUM.getType(), -value);
    }

    /**
     * Function check whether the minor race can accept the proposal based on other existing contracts
     * @param majorID - the race who proposes the offer
     * @param type - type of contract offered
     */
    public boolean canAcceptOffer(String majorID, DiplomaticAgreement type) {
        if (this.getAgreement(majorID).getType() >= type.getType())
            return false;
        //check if we can accept the offer, for example if a race is already member of a different race, then it can accept only a small number of offers like War, Gifts or Bribe
        DiplomaticAgreement othersAgreement = DiplomaticAgreement.NONE;
        ArrayMap<String, Major> majors = resourceManager.getRaceController().getMajors();
        for (int i = 0; i < majors.size; i++) {
            String otherId = majors.getKeyAt(i);
            if (!otherId.equals(majorID)) {
                DiplomaticAgreement temp = getAgreement(otherId);
                if (temp.getType() > othersAgreement.getType())
                    othersAgreement = temp;
            }
        }

        if ((type == DiplomaticAgreement.COOPERATION || type == DiplomaticAgreement.ALLIANCE || type == DiplomaticAgreement.MEMBERSHIP)
                && othersAgreement.getType() > DiplomaticAgreement.FRIENDSHIP.getType())
            return false;
        if (type == DiplomaticAgreement.TRADE && othersAgreement.getType() > DiplomaticAgreement.ALLIANCE.getType())
            return false;
        if (type == DiplomaticAgreement.FRIENDSHIP
                && othersAgreement.getType() > DiplomaticAgreement.COOPERATION.getType())
            return false;

        return true;
    }

    private void cancelAgreement(DiplomaticAgreement agreement, String otherRaceID, Major major, String s) {
        if ((isSubjugated() && agreement != DiplomaticAgreement.WAR) || !s.isEmpty()) { //war stays
            setAgreement(otherRaceID, DiplomaticAgreement.NONE);
            major.setAgreement(raceID, DiplomaticAgreement.NONE);
        }
        if (!s.isEmpty()) {
            EmpireNews message = new EmpireNews();
            message.CreateNews(s, EmpireNewsType.DIPLOMACY);
            major.getEmpire().addMsg(message);
        }
    }

    /**
     * Function checks the consistency of the diplomacy and generates the corresponding message to the majors in case an agreement is canceled
     */
    public void checkDiplomaticConsistency() {
        ArrayMap<String, Major> majors = resourceManager.getRaceController().getMajors();
        for (Iterator<Entry<String, Major>> iter = majors.entries().iterator(); iter.hasNext();) {
            Entry<String, Major> e = iter.next();
            //if unknown race, continue
            if (!isRaceContacted(e.key))
                continue;

            //if the race was subjugated, all contracts are cancelled
            if (isSubjugated()) {
                String s = "";
                DiplomaticAgreement agreement = getAgreement(e.key);
                if (agreement.getType() >= DiplomaticAgreement.TRADE.getType()
                        && agreement.getType() <= DiplomaticAgreement.MEMBERSHIP.getType())
                    s = StringDB.getString("CANCEL_" + agreement.getdbName(), false, getName());

                cancelAgreement(agreement, e.key, e.value, s);
                continue;
            }

            DiplomaticAgreement majorsAgreement = getAgreement(e.key);
            if (majorsAgreement.getType() >= DiplomaticAgreement.COOPERATION.getType()
                    && majorsAgreement.getType() <= DiplomaticAgreement.MEMBERSHIP.getType())
                for (int i = 0; i < majors.size; i++) {
                    String otherRaceId = majors.getKeyAt(i);
                    if (otherRaceId.equals(e.key))
                        continue;

                    String s = "";
                    DiplomaticAgreement agreement = getAgreement(otherRaceId);
                    if (majorsAgreement == DiplomaticAgreement.MEMBERSHIP) {
                        if (agreement.getType() >= DiplomaticAgreement.TRADE.getType()
                                && agreement.getType() <= DiplomaticAgreement.MEMBERSHIP.getType())
                            s = StringDB.getString("CANCEL_" + agreement.getdbName(), false, getName());
                    } else if (majorsAgreement == DiplomaticAgreement.ALLIANCE) {
                        if (agreement.getType() >= DiplomaticAgreement.FRIENDSHIP.getType()
                                && agreement.getType() <= DiplomaticAgreement.ALLIANCE.getType())
                            s = StringDB.getString("CANCEL_" + agreement.getdbName(), false, getName());
                    } else if (majorsAgreement == DiplomaticAgreement.COOPERATION) {
                        if (agreement == DiplomaticAgreement.COOPERATION)
                            s = StringDB.getString("CANCEL_" + agreement.getdbName(), false, getName());
                    }
                    cancelAgreement(majorsAgreement, otherRaceId, majors.getValueAt(i), s);
                }
        }
    }

    /**
     * Function checks the relationships with other races and if these are too far from the agreement, this could be cancelled
     */
    public void perhapsCancelAgreement() {
        ArrayMap<String, Major> majors = resourceManager.getRaceController().getMajors();
        for (Iterator<Entry<String, Major>> iter = majors.entries().iterator(); iter.hasNext();) {
            Entry<String, Major> e = iter.next();
            //25% chance
            if (((int) (RandUtil.random() * 4)) != 0)
                continue;
            //if unknown race, continue
            if (!isRaceContacted(e.key))
                continue;

            int relation = getRelation(e.key);
            DiplomaticAgreement agreement = getAgreement(e.key);
            if (relation < agreement.getType() * 12 && agreement.getType() >= DiplomaticAgreement.TRADE.getType()
                    && agreement.getType() <= DiplomaticAgreement.MEMBERSHIP.getType()) {
                String s = "";
                s = StringDB.getString("CANCEL_" + agreement.getdbName(), false, getName());
                cancelAgreement(agreement, e.key, e.value, s);
            }
        }
    }

    public boolean isMemberTo() {
        return isMemberTo("");
    }

    public boolean isMemberTo(String majorID) {
        if (!majorID.isEmpty())
            return getAgreement(majorID) == DiplomaticAgreement.MEMBERSHIP;
        for (Iterator<Entry<String, DiplomaticAgreement>> iter = agreements.entries().iterator(); iter.hasNext();) {
            Entry<String, DiplomaticAgreement> e = iter.next();
            if (e.value == DiplomaticAgreement.MEMBERSHIP)
                return true;
        }
        return false;
    }

    public double getRaceMod(RaceModType type) {
        double bonus = 0;
        switch (type) {
            case RACE_MOD_TYPE_FOOD:
                if (isRaceProperty(RaceProperty.AGRARIAN))
                    bonus += 25;
                if (isRaceProperty(RaceProperty.PACIFIST))
                    bonus += 15;
                break;
            case RACE_MOD_TYPE_INDUSTRY:
                if (isRaceProperty(RaceProperty.WARLIKE) || isRaceProperty(RaceProperty.HOSTILE))
                    bonus += 15;
                if (isRaceProperty(RaceProperty.INDUSTRIAL))
                    bonus += 25;
                if (isRaceProperty(RaceProperty.SNEAKY))
                    bonus += 10;
                break;
            case RACE_MOD_TYPE_ENERGY:
                if (isRaceProperty(RaceProperty.WARLIKE) || isRaceProperty(RaceProperty.HOSTILE))
                    bonus += 10;
                break;
            case RACE_MOD_TYPE_SECURITY:
                if (isRaceProperty(RaceProperty.SECRET))
                    bonus += 25;
                if (isRaceProperty(RaceProperty.SNEAKY))
                    bonus += 15;
                break;
            case RACE_MOD_TYPE_RESEARCH:
                if (isRaceProperty(RaceProperty.SCIENTIFIC))
                    bonus += 25;
                if (isRaceProperty(RaceProperty.PACIFIST))
                    bonus += 10;
                break;
            case RACE_MOD_TYPE_ALL_RESOURCES:
                if (isRaceProperty(RaceProperty.PRODUCER))
                    bonus += 10;
                break;
            case RACE_MOD_TYPE_CREDITS:
                if (isRaceProperty(RaceProperty.FINANCIAL))
                    bonus += 25;
                break;
        }
        return bonus;
    }

    public static Minor toMinor(Race race) {
        if (race != null && race.isMinor())
            return (Minor) race;
        else
            return null;
    }

    @Override
    public void write(Kryo kryo, Output output) {
        super.write(kryo, output);
        output.writeInt(technologicalProgress);
        output.writeInt(corruptibility);
        output.writeBoolean(spaceFlight);
        output.writeBoolean(subjugated);
        kryo.writeObject(output, acceptance);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void read(Kryo kryo, Input input) {
        super.read(kryo, input);
        technologicalProgress = input.readInt();
        corruptibility = input.readInt();
        spaceFlight = input.readBoolean();
        subjugated = input.readBoolean();
        acceptance = kryo.readObject(input, ObjectIntMap.class);
    }
}
