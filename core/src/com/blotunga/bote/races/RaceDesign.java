/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.races;

import com.badlogic.gdx.graphics.Color;

public class RaceDesign {
    public Color clrSector;					///<!!! Sector color
    public Color clrSmallBtn;
    public Color clrLargeBtn;
    public Color clrSmallText;
    public Color clrNormalText;
    public Color clrSecondText;
    public Color clrGalaxySectorText;
    public Color clrListMarkTextColor;		///<!!! color for a text if selected
    public Color clrListMarkPenColor;		///<!!! color of margin of an element in a list
    public Color clrRouteColor;				///<!!! trade or resource route color
    public String fontName;

    public RaceDesign() {
        reset();
    }

    public void reset() {
        clrSector = Color.valueOf("C8C8C8FF");
        clrSmallBtn = Color.BLACK;
        clrLargeBtn = Color.BLACK;
        clrSmallText = Color.BLACK;
        clrNormalText = Color.BLACK;
        clrSecondText = Color.BLACK;
        clrGalaxySectorText = Color.WHITE;
        clrListMarkTextColor = Color.valueOf("DCDCDCFF");
        clrListMarkPenColor = Color.valueOf("8CC4CBFF");
        clrRouteColor = Color.WHITE;
        fontName = "YoungSerif-Regular";
    }

    public static Color stringToColor(String RGB) {
        String[] strings = RGB.split(",");
        int[] values = new int[3];
        for (int i = 0; i < strings.length; i++)
            values[i] = Integer.parseInt(strings[i]);
        return new Color(values[0] / 255f, values[1] / 255f, values[2] / 255f, 1.0f);
    }
}
