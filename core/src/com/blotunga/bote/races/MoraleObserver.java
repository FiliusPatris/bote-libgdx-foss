/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.races;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.IntArray;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.PlayerRaces;
import com.blotunga.bote.starsystem.StarSystem;

public class MoraleObserver {
    public final static int EVENTNUMBER = 61;
    private static transient int[][] moraleMatrix;
    private static transient String[][] textMatrix;
    private IntArray events;

    public MoraleObserver() {
        events = new IntArray();
    }

    /**
     * Function loads the morale values from file
     */
    public static void initMoraleMatrix() {
        moraleMatrix = new int[EVENTNUMBER][PlayerRaces.MAJOR6.getType()];
        textMatrix = new String[EVENTNUMBER][PlayerRaces.MAJOR6.getType()];

        FileHandle handle = Gdx.files.internal("data/other/moral.txt");
        String content = handle.readString("ISO-8859-1");
        String[] input = content.split("\n");
        for (int i = 0; i < input.length; i++) {
            String line = input[i].trim();
            int startPos = line.indexOf(":");
            line = line.substring(startPos + 1);
            line = line.trim();
            String[] col = line.split("\t");
            for (int j = 0; j < col.length; j++) {
                String elem = col[j].trim();
                moraleMatrix[i][j] = Integer.parseInt(elem);
            }
        }

        handle = Gdx.files.internal("data" + GameConstants.getLocalePrefix() + "/other/moralevents.txt");
        content = handle.readString(GameConstants.getCharset());
        input = content.split("\n");
        int i = -1;
        int j = 0;
        for (String line : input) {
            line = line.trim();
            if (!line.startsWith("#")) {
                textMatrix[i][j] = line.trim();
                j++;
            } else {
                i++;
                j = 0;
            }
        }
    }

    /**
     * Function returns a specific morale value
     * @param mappedRaceNumber
     * @param event
     * @return morale value
     */
    public static int getMoraleValue(int mappedRaceNumber, int event) {
        return moraleMatrix[event][mappedRaceNumber - 1];
    }

    public String addEvent(int event, int major) {
        return addEvent(event, major, "");
    }

    /**
     * Function adds the event into the MoraleObserver so that it can be used later when calculating morale.
     * @param event - number of the event
     * @param major - race number
     * @param param - optionally param has to be passed, which is replacing $param$ values in some messages
     * @return - the text which can be added in the message overview
     */
    public String addEvent(int event, int major, String param) {
        events.add(event);
        return generateText(event, major, param);
    }

    public String generateText(int event, int major) {
        return generateText(event, major, "");
    }

    /**
     * Function generates a text which can be displayed in the empire messages
     */
    public String generateText(int event, int major, String param) {
        String text = textMatrix[event][major - 1];
        text = text.replace("$param$", param);

        int moralValue = getMoraleValue(major, event);
        String s = "";
        if (moralValue > 0)
            s = String.format(" (+%d)", moralValue);
        else if (moralValue < 0)
            s = String.format(" (%d)", moralValue);
        else
            s = " (+- 0)";

        text += s;
        return text;
    }

    /**
     * Function calculates the morale effects on all systems based on the events which affected the major race
     * @param systems - all starsystems
     * @param raceId - raceID of the race
     * @param mappedRaceNumber - mappedNumber of the race
     */
    public void calculateEvents(Array<StarSystem> systems, String raceId, int mappedRaceNumber) {
        while (events.size != 0) {
            int moral = moraleMatrix[events.get(0)][mappedRaceNumber - 1];
            for (int i = 0; i < systems.size; i++) {
                StarSystem system = systems.get(i);
                if (system.isMajorized() && system.getOwnerId().equals(raceId))
                    system.setMorale(moral);
            }
            events.removeIndex(0);
        }
    }
}
