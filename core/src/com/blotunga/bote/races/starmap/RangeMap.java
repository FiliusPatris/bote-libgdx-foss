/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.races.starmap;

public class RangeMap {
    int[] range;			///< keeps the range matrix
    int w;					///< number of columns in the range matrix
    int h;					///< number of lines in the range matrix
    int x0;					///< zero based index of a column; the matrix will be set up so that the column is over the sector of an outpost
    int y0;					///< zero based index of a line; the matrix will be set up so that the line is over the sector of an outpost

    public RangeMap() {
        w = 0;
        h = 0;
        x0 = 0;
        y0 = 0;
        range = null;
    }
}