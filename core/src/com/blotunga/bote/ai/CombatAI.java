/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ai;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.ObjectSet;
import com.badlogic.gdx.utils.ObjectSet.ObjectSetIterator;
import com.blotunga.bote.constants.CombatOrder;
import com.blotunga.bote.constants.CombatTactics;
import com.blotunga.bote.constants.RaceProperty;
import com.blotunga.bote.galaxy.Anomaly;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.races.Race;
import com.blotunga.bote.races.RaceController;
import com.blotunga.bote.ships.Combat;
import com.blotunga.bote.ships.Ships;
import com.blotunga.bote.utils.RandUtil;

public class CombatAI {
    /**
     * This function calculates the tactics of all ships which are involved in the battle
     * @param involvedShips
     * @param races
     * @param combatOrders
     * @param anomaly
     * @return - true if a battle can be fought
     */
    public static boolean calcCombatTactics(Array<Ships> involvedShips, RaceController races,
            ArrayMap<String, CombatOrder> combatOrders, Anomaly anomaly) {
        applyCombatOrders(involvedShips, races, combatOrders, anomaly);
        boolean allHailingFrequencies = true;

        for (int i = 0; i < combatOrders.size; i++) {
            if (combatOrders.getValueAt(i) != CombatOrder.HAILING) {
                allHailingFrequencies = false;
                break;
            }
        }
        if (allHailingFrequencies)
            return false;

        applyShipTactics(involvedShips, combatOrders);
        return true;
    }

    /**
     * The function sets the tactics for the race before the battle
     * @param involvedShips
     * @param races
     * @param combatOrders
     * @param anomaly
     */
    private static void applyCombatOrders(Array<Ships> involvedShips, RaceController races,
            ArrayMap<String, CombatOrder> combatOrders, Anomaly anomaly) {
        ObjectSet<String> involvedRaces = new ObjectSet<String>();
        for (int i = 0; i < involvedShips.size; i++)
            involvedRaces.add(involvedShips.get(i).getOwnerId());
        ObjectSet<String> involvedRacesCpy = new ObjectSet<String>(involvedRaces);//so that we can iterate nested
        for (ObjectSetIterator<String> it = involvedRaces.iterator(); it.hasNext();) {
            String entry = it.next();
            //AI majors and minors have to set orders
            if (combatOrders.containsKey(entry))
                continue;

            Race race = races.getRace(entry);
            if (race == null)
                continue;

            int minRelation = 100;
            for (ObjectSetIterator<String> it2 = involvedRacesCpy.iterator(); it2.hasNext();) {
                String entry2 = it2.next();
                if (!entry.equals(entry2)) {
                    Race otherRace = races.getRace(entry2);
                    if (otherRace == null)
                        continue;

                    if (!otherRace.isMajor() || (otherRace.isMajor() && !((Major) otherRace).aHumanPlays()))
                        minRelation = Math.min(minRelation, otherRace.getRelation(race.getRaceId()));
                    else if (!race.isMajor() || (race.isMajor() && !((Major) race).aHumanPlays()))
                        minRelation = Math.min(minRelation, race.getRelation(otherRace.getRaceId()));
                }
            }

            int raceMod = 0;
            if (race.isRaceProperty(RaceProperty.HOSTILE))
                raceMod -= 50;
            if (race.isRaceProperty(RaceProperty.WARLIKE))
                raceMod -= 25;
            if (race.isRaceProperty(RaceProperty.SOLOING))
                raceMod -= 10;
            if (race.isRaceProperty(RaceProperty.SNEAKY))
                raceMod -= 10;
            if (race.isRaceProperty(RaceProperty.FINANCIAL))
                raceMod += 10;
            if (race.isRaceProperty(RaceProperty.SCIENTIFIC))
                raceMod += 10;
            if (race.isRaceProperty(RaceProperty.AGRARIAN))
                raceMod += 25;
            if (race.isRaceProperty(RaceProperty.PACIFIST))
                raceMod += 50;

            //if the relationship is good it's probably that hailing frequencies are opened
            if (RandUtil.random() * 100 < minRelation + raceMod) {
                combatOrders.put(entry, CombatOrder.HAILING);
                continue;
            }
            ObjectSet<String> friends = new ObjectSet<String>();
            ObjectSet<String> enemies = new ObjectSet<String>();
            double winningChance = Combat
                    .getWinningChance(race, involvedShips, races, friends, enemies, anomaly, false);

            if (winningChance > 0.75)
                combatOrders.put(entry, CombatOrder.AUTOCOMBAT);
            else if (winningChance < 0.25)
                combatOrders.put(entry, CombatOrder.RETREAT);
            else {
                if (RandUtil.random() * 100 < winningChance * 100 - raceMod / 2)
                    combatOrders.put(entry, CombatOrder.AUTOCOMBAT);
                else
                    combatOrders.put(entry, CombatOrder.RETREAT);
            }
        }

        //logging
        for (ObjectSetIterator<String> it = involvedRaces.iterator(); it.hasNext();) {
            String entry = it.next();
            String tactic = "";
            switch (combatOrders.get(entry)) {
                case NONE:
                    tactic = "- (error)";
                    break;
                case USER:
                    tactic = "User";
                    break;
                case HAILING:
                    tactic = "Hailing Frequencies";
                    break;
                case RETREAT:
                    tactic = "Retreat";
                    break;
                case AUTOCOMBAT:
                    tactic = "Auto";
                    break;
            }
            Gdx.app.debug("CombatAI", String.format("Race %s is involved in combat. Tactic: %s", entry, tactic));
        }
    }

    private static void applyShipTactics(Array<Ships> involvedShips, ArrayMap<String, CombatOrder> combatOrders) {
        for (int i = 0; i < involvedShips.size; i++) {
            Ships ship = involvedShips.get(i);
            String owner = ship.getOwnerId();
            if (combatOrders.containsKey(owner)) {
                CombatOrder order = combatOrders.get(owner);
                if (order == CombatOrder.AUTOCOMBAT) {
                    ship.setCombatTactics(CombatTactics.CT_ATTACK);
                    if (ship.isNonCombat()) {
                        if (ship.getCompleteOffensivePower() <= 1)
                            ship.setCombatTactics(CombatTactics.CT_AVOID);
                    }
                } else if (order == CombatOrder.HAILING) {
                    boolean avoid = true;
                    for (int j = 0; j < combatOrders.size; j++) {
                        if (!combatOrders.getKeyAt(j).equals(owner))
                            if (combatOrders.getValueAt(j) != CombatOrder.HAILING
                                    && combatOrders.getValueAt(j) != CombatOrder.RETREAT) {
                                avoid = false;
                                break;
                            }
                    }
                    if (avoid) {
                        ship.setCombatTactics(CombatTactics.CT_AVOID);
                    } else {
                        ship.setCombatTactics(CombatTactics.CT_ATTACK);
                        if (ship.isNonCombat()) {
                            if (ship.getCompleteOffensivePower() <= 1)
                                ship.setCombatTactics(CombatTactics.CT_AVOID);
                        }
                    }
                } else if (order == CombatOrder.RETREAT) {
                    if (ship.getManeuverabilty() == 0 && ship.isNonCombat())
                        ship.setCombatTactics(CombatTactics.CT_AVOID);
                    else if (ship.getManeuverabilty() == 0)
                        ship.setCombatTactics(CombatTactics.CT_ATTACK);
                    else
                        ship.setCombatTactics(CombatTactics.CT_RETREAT);
                } else if (order == CombatOrder.USER) {
                    //don't change anything
                }
            }
        }
    }
}
