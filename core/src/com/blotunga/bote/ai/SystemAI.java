/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ai;

import java.util.Arrays;
import java.util.Iterator;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.ObjectMap.Entry;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.constants.DiplomaticAgreement;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.ResearchComplexType;
import com.blotunga.bote.constants.ResearchStatus;
import com.blotunga.bote.constants.ResourceTypes;
import com.blotunga.bote.constants.ShipType;
import com.blotunga.bote.constants.WorkerType;
import com.blotunga.bote.galaxy.Planet;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.races.Minor;
import com.blotunga.bote.races.Race;
import com.blotunga.bote.races.Research;
import com.blotunga.bote.ships.ShipInfo;
import com.blotunga.bote.starsystem.Building;
import com.blotunga.bote.starsystem.BuildingInfo;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.starsystem.SystemProd;
import com.blotunga.bote.troops.TroopInfo;
import com.blotunga.bote.utils.IntPoint;
import com.blotunga.bote.utils.Pair;
import com.blotunga.bote.utils.RandUtil;

public class SystemAI {
    private ResourceManager manager;
    private IntPoint coord; //coordinate of the current system where the calculations are made
    private int[] priorities; //priorities of the worker buildings
    private boolean calcedPrio; //if the priorities were calculated true, otherwise false
    private Major major; //major race for which we do the calculations
    private StarSystem system;

    public SystemAI(ResourceManager manager) {
        this.manager = manager;
        priorities = new int[10];
        Arrays.fill(priorities, 0);
        calcedPrio = false;
        coord = new IntPoint();
        major = null;
        system = null;
    }

    /**
     * This function executes the calculations for the AI in a system and makes all entries and modifications
     * @param coord - coordinates of the system
     */
    public void executeSystemAI(IntPoint coord) {
        system = manager.getUniverseMap().getStarSystemAt(coord);
        Race race = manager.getUniverseMap().getStarSystemAt(coord).getOwner();
        if (race == null || !(race instanceof Major)) {
            System.err.println("Error in CSystemAI::ExecuteSystemAI(): no race controls system " + coord);
            return;
        }

        major = (Major) race;
        this.coord = coord;
        perhapsBuy();
        calcPriorities();
        assignWorkers();
        scrapBuildings();
        applyTradeRoutes();
    }

    /**
     * This function buys under some circumstances the current build order. This accelerates progress
     */
    private void perhapsBuy() {
        int id = system.getAssemblyList().getAssemblyListEntry(0).id;
        if (id == 0)
            return;
        int roundToBuild = system.neededRoundsToBuild(0, true);
        if (id > 0 && id < 10000 && manager.getBuildingInfo(id).getNeverReady())
            roundToBuild = 0;

        //if it's higher than 1 then we can think about buying
        if (roundToBuild > 1) {
            ///SPECIAL RESEARCH
            int bonus = major.getEmpire().getResearch().getResearchInfo()
                    .isResearchedThenGetBonus(ResearchComplexType.FINANCES, 1);
            system.getAssemblyList().calculateBuildCosts(major.getTrade().getResourcePriceAtRoundStart(), bonus);
            int costs = system.getAssemblyList().getBuildCosts();
            int value = (major.getEmpire().getCredits() / costs) * 5;
            //the more credits an empire has, the higher the chance of buying. Also in case of low morale we try to buy faster to implement martial law in the next turn
            if ((int) (RandUtil.random() * 100) < value
                    || (value > 0 && system.getMorale() < (int) (RandUtil.random() * 21 + 60))) {
                costs = system.getAssemblyList().buyBuilding(major.getEmpire().getCredits());
                if (costs != 0) {
                    system.getAssemblyList().setWasBuildingBought(true);
                    major.getEmpire().setCredits(-costs);
                    for (int res = ResourceTypes.TITAN.getType(); res < ResourceTypes.DERITIUM.getType(); res++)
                        major.getTrade().buyResource(res,
                                system.getAssemblyList().getNeededResourceInAssemblyList(0, res),
                                system.getCoordinates(), major.getEmpire().getCredits(), true);
                }
            }
        }
    }

    /**
     * This function sets the priorities for a type of work building that should be built
     */
    private void calcPriorities() {
        int entry = system.getAssemblyList().getAssemblyListEntry(0).id;
        //check if it's a ship or a troop that's in the list, and if there is no shipyard/barrack then cancel the order
        if ((entry >= 10000 && entry < 20000 && !system.getProduction().isShipyard())
                || (entry >= 20000 && !system.getProduction().isBarrack())) {
            system.getAssemblyList().clearAssemblyList(coord, manager);
            system.calculateVariables();
        }

        entry = system.getAssemblyList().getAssemblyListEntry(0).id; //redo this as it might have been erased previously
        //if the morale is too low then we will try to prioritize a morale producing building
        if (system.getMorale() < (int) (RandUtil.random() * 16 + 70)) {
            if (entry > 0 && entry < 10000)
                if (manager.getBuildingInfo(entry).getMoraleProd() > 0
                        && manager.getBuildingInfo(entry).getNeverReady())
                    return;//we already have what we want in the list

            // if the building needs less than 3-6 rounds then it can also stay
            int ip = system.getProduction().getIndustryProd();
            if (entry != 0 && ip > 0) {
                int roundToBuild = system.neededRoundsToBuild(0, true);
                if (entry > 0 && entry < 10000 && manager.getBuildingInfo(entry).getNeverReady())
                    roundToBuild = 0;
                //if it takes longer than 3-6 rounds or the morale is under 50
                if (roundToBuild > (int) (RandUtil.random() * 4 + 3) || system.getMorale() < 50) {
                    system.getAssemblyList().clearAssemblyList(coord, manager);
                    system.calculateVariables();
                }
            }

            IntArray buildings = new IntArray();
            for (int i = 0; i < system.getBuildableBuildings().size; i++) {
                int id = system.getBuildableBuildings().get(i);
                if (manager.getBuildingInfo(id).getMoraleProd() > 0 && manager.getBuildingInfo(id).getNeverReady())
                    buildings.add(id);
            }

            while (buildings.size > 0) {
                int random = (int) (RandUtil.random() * buildings.size);
                if (makeEntryInAssemblyList(buildings.get(random))) {
                    return;
                } else
                    buildings.removeIndex(random);
            }
        }

        //check if we have something in the assembly list, if yes then return
        if (system.getAssemblyList().getAssemblyListEntry(0).id != 0)
            return;

        double maxHab = Math.max(1.0, system.getMaxInhabitants());

        priorities[WorkerType.FOOD_WORKER.getType()] = getFoodPrio(maxHab);
        priorities[WorkerType.INDUSTRY_WORKER.getType()] = getIndustryPrio(maxHab);
        priorities[WorkerType.ENERGY_WORKER.getType()] = getEnergyPrio(maxHab);
        priorities[WorkerType.SECURITY_WORKER.getType()] = getIntelPrio(maxHab);
        priorities[WorkerType.RESEARCH_WORKER.getType()] = getResearchPrio(maxHab);
        for (int i = WorkerType.TITAN_WORKER.getType(); i <= WorkerType.IRIDIUM_WORKER.getType(); i++) {
            WorkerType worker = WorkerType.fromWorkerType(i);
            priorities[i] = getResourcePrio(worker, maxHab);
        }

        calcedPrio = true;
        int id = chooseBuilding();
        if (id == 0) {
            //try to upgrade
            if (system.getBuildableUpdates().size > 0) {
                int random = (int) (RandUtil.random() * system.getBuildableUpdates().size);
                id = system.getBuildableUpdates().get(random) * (-1);
                if (!makeEntryInAssemblyList(id)) {
                    //if no updates can be built, try to build ships again
                    Pair<Integer, boolean[]> shipPrios = getShipOrTroopBuildPrios();
                    int prio = shipPrios.getFirst();
                    boolean chooseCombatship = shipPrios.getSecond()[0];
                    boolean chooseColoship = shipPrios.getSecond()[1];
                    boolean chooseTransport = shipPrios.getSecond()[2];
                    boolean chooseTroop = shipPrios.getSecond()[3];
                    id = chooseShipOrTroop(prio, chooseCombatship, chooseColoship, chooseTransport, chooseTroop);
                }
            }
        }
        //check assembly list if no ship was built
        //do check only if we have an update or a building we want to cap
        if (id != 0 && id < 10000) {
            if (id < 0)
                id *= (-1);
            if (manager.getBuildingInfo(id).getMaxInEmpire() > 0) {
                manager.getGlobalBuildings().addGlobalBuilding(major.getRaceId(), id);

                for (StarSystem ss : manager.getUniverseMap().getStarSystems())
                    if (major.getRaceBuildingNumber() == manager.getBuildingInfo(id).getOwnerOfBuilding()
                            && ss.getOwnerId().equals(major.getRaceId()))
                        ss.assemblyListCheck(manager.getBuildingInfos());
            }
        }
    }

    private boolean checkMorale(BuildingInfo bi, boolean build) {
        //yes, there is a minor with such useless building...
        if (bi.getMoraleProdEmpire() < 0)
            return false;
        int mp = bi.getMoraleProd();
        if (mp >= 0)
            return true;
        // I would usually only build, if I could still maintain moral +5 in this system
        // don't know empire wide moral prod, usually at least +1
        if (build)
            return 2 <= system.getProduction().getMoraleProd() + mp;
        //in practice, AI will take minus moral buildings online very seldom, and that's better
        return system.getMorale() > 173;
    }

    /**
     * This function chooses a building from the list of buildable buildings
     * Only buildings which are in the priorities list are selected
     * @return - id of the build order - if 0 then we have found none
     */
    private int chooseBuilding() {
        //from the priority list the order is selected which has the highest priority
        WorkerType choosenPrio = WorkerType.NONE;
        Pair<Integer, boolean[]> shipPrios = getShipOrTroopBuildPrios();
        int min = shipPrios.getFirst();
        boolean chooseCombatship = shipPrios.getSecond()[0];
        boolean chooseColoship = shipPrios.getSecond()[1];
        boolean chooseTransport = shipPrios.getSecond()[2];
        boolean chooseTroop = shipPrios.getSecond()[3];
        int shipBuildPrio = min;
        //Gdx.app.debug("SystemAI", String.format("ChooseBuilding(): min priority after ships: %d", min));

        //if updates are available then we lower other priorities a bit so that updates are prioritized
        //except if we have free workers of course
        int updates = (int) (RandUtil.random() * (system.getBuildableUpdates().size + 1));
        min -= system.getWorker(WorkerType.FREE_WORKER);
        //Gdx.app.debug("SystemAI", String.format("ChooseBuilding(): min priority after ships, updates and workers: %d", min));
        for (int i = WorkerType.FOOD_WORKER.getType(); i <= WorkerType.IRIDIUM_WORKER.getType(); i++) {
            WorkerType worker = WorkerType.fromWorkerType(i);
            if (priorities[i] > min) {
                int random = (int) (RandUtil.random() * (priorities[i] + 1));
                if (random - updates + system.getWorker(WorkerType.FREE_WORKER) > 0) {
                    min = priorities[i];
                    choosenPrio = worker;
                }
            }
        }

        if (choosenPrio != WorkerType.NONE) {
            //Gdx.app.debug("SystemAI", String.format("ChooseBuilding(): choosen prio: %d", choosenPrio.getType()));
            for (int i = system.getBuildableBuildings().size - 1; i >= 0; i--) {
                int id = system.getBuildableBuildings().get(i);
                BuildingInfo bi = manager.getBuildingInfo(id);
                //if the moralemalus of the building is higher then our morale production, then we won't build it
                if (!checkMorale(bi, true))
                    continue;
                //if we find a shipyard, we try to build it
                if (bi.isShipYard())
                    if (makeEntryInAssemblyList(id))
                        return id;
                switch (choosenPrio) {
                    case FOOD_WORKER:
                        if (bi.getFoodProd() > 0 || bi.getFoodBonus() > 0)
                            if (makeEntryInAssemblyList(id))
                                return id;
                        break;
                    case INDUSTRY_WORKER:
                        if (bi.getIndustryPointsProd() > 0 || bi.getIndustryBonus() > 0)
                            if (makeEntryInAssemblyList(id))
                                return id;
                        break;
                    case ENERGY_WORKER:
                        if (bi.getEnergyProd() > 0 || bi.getEnergyBonus() > 0)
                            if (makeEntryInAssemblyList(id))
                                return id;
                        break;
                    case SECURITY_WORKER:
                        if (bi.getSecurityPointsProd() > 0 || bi.getSecurityBonus() > 0)
                            if (makeEntryInAssemblyList(id))
                                return id;
                        break;
                    case RESEARCH_WORKER:
                        if (bi.getResearchPointsProd() > 0 || bi.getResearchBonus() > 0)
                            if (makeEntryInAssemblyList(id))
                                return id;
                        break;
                    case TITAN_WORKER:
                        if (bi.getTitanProd() > 0 || bi.getTitanBonus() > 0)
                            if (makeEntryInAssemblyList(id))
                                return id;
                        break;
                    case DEUTERIUM_WORKER:
                        if (bi.getDeuteriumProd() > 0 || bi.getDeuteriumBonus() > 0)
                            if (makeEntryInAssemblyList(id))
                                return id;
                        break;
                    case DURANIUM_WORKER:
                        if (bi.getDuraniumProd() > 0 || bi.getDuraniumBonus() > 0)
                            if (makeEntryInAssemblyList(id))
                                return id;
                        break;
                    case CRYSTAL_WORKER:
                        if (bi.getCrystalProd() > 0 || bi.getCrystalBonus() > 0)
                            if (makeEntryInAssemblyList(id))
                                return id;
                        break;
                    case IRIDIUM_WORKER:
                        if (bi.getIridiumProd() > 0 || bi.getIridiumBonus() > 0)
                            if (makeEntryInAssemblyList(id))
                                return id;
                        break;
                    default:
                        return 0;
                }
            }
        } else if ((int) (RandUtil.random() * (shipBuildPrio + 1)) <= (int) (RandUtil.random() * 4)) {
            //if there was no building matching our priority found, then we select one which needs no workers
            //the more updates there are, the more often we jump out
            if ((int) (RandUtil.random() * (updates + 1)) > 0)
                return 0;
            //the more free workers we have, the more chance that we jump out
            if ((int) (RandUtil.random() * (system.getWorker(WorkerType.FREE_WORKER) + 1)) / 2 > 0)
                return 0;

            //create a list of id's
            IntArray buildings = new IntArray();
            for (int i = 0; i < system.getBuildableBuildings().size; i++) {
                int id = system.getBuildableBuildings().get(i);
                if (!manager.getBuildingInfo(id).getWorker() && !manager.getBuildingInfo(id).getNeverReady())
                    buildings.add(id);
            }

            while (buildings.size > 0) {
                int random = (int) (RandUtil.random() * buildings.size);
                //buildings with negative morale are not built if it can't be balanced
                BuildingInfo bi = manager.getBuildingInfo(buildings.get(random));
                if ((bi.getMoraleProd() < 0 && Math.abs(bi.getMoraleProd()) > system.getProduction().getMoraleProd()) || (bi.getMoraleProdEmpire() < 0)) {
                    buildings.removeIndex(random);
                    continue;
                }
                if (bi.getCreditsBonus() < 0
                        && system.getProduction().getCreditsProd() > manager.getStatistics().getDemographicsBSP(
                                major.getRaceId()).value / 2) { //these buildings could hinder AI production (especially the Cartare War Bonds department), build them only when the other systems are sufficiently developed
                    buildings.removeIndex(random);
                    continue;
                }

                if (makeEntryInAssemblyList(buildings.get(random)))
                    return buildings.get(random);
                else
                    buildings.removeIndex(random);
            }
        }
        return chooseShipOrTroop(shipBuildPrio, chooseCombatship, chooseColoship, chooseTransport, chooseTroop);
    }

    /**
     * This function selects a ship from the buildable ships list
     * @param prio - priority of the ship
     * @param chooseCombatship
     * @param chooseColoship
     * @param chooseTransportship
     * @return - id of the selected ship (or 0 if none was selected)
     */
    private int chooseShipOrTroop(int prio, boolean chooseCombatship, boolean chooseColoship,
            boolean chooseTransportship, boolean chooseTroop) {
        int min = prio;
        Race race = system.getOwner();
        if (race == null)
            return 0;

        if (!race.isMajor())
            return 0;

        Major major = (Major) race;

        Research research = major.getEmpire().getResearch();
        int[] researchLevels = research.getResearchLevels();

        //don't continue spitting out ships if we're already on negative income
        if (major.getEmpire().getCreditsChange() < manager.getCurrentRound())
            if (chooseCombatship
                    || research.getResearchInfo().getResearchComplex(ResearchComplexType.PEACEFUL_SHIP_TECHNOLOGY)
                            .getFieldStatus(2) != ResearchStatus.RESEARCHED)
                return 0;

        //if a colonyship should be built
        if (chooseColoship && min > 0) {
            for (int j = 0; j < manager.getShipInfos().size; j++)
                if (major.canBuildShip(ShipType.COLONYSHIP, researchLevels, manager.getShipInfos().get(j))) {
                    int id = manager.getShipInfos().get(j).getID();
                    for (int i = 0; i < system.getBuildableShips().size; i++)
                        if (system.getBuildableShips().get(i) == id)
                            if (makeEntryInAssemblyList(id)) {
                                manager.getAIPrios().choseColoShipPrio(major.getRaceId());
                                return id;
                            }
                }
        } else if (chooseTransportship && min > 0) {
            for (int j = 0; j < manager.getShipInfos().size; j++)
                if (major.canBuildShip(ShipType.TRANSPORTER, researchLevels, manager.getShipInfos().get(j))) {
                    int id = manager.getShipInfos().get(j).getID();
                    for (int i = 0; i < system.getBuildableShips().size; i++)
                        if (system.getBuildableShips().get(i) == id)
                            if (makeEntryInAssemblyList(id)) {
                                manager.getAIPrios().choseTransportShipPrio(major.getRaceId());
                                return id;
                            }
                }
        } else if (chooseCombatship && min > 0) {
            class ShipList implements Comparable<ShipList> {
                int id;
                int strength;

                public ShipList(int id, int strength) {
                    this.id = id;
                    this.strength = strength;
                }

                @Override
                public int compareTo(ShipList o) {
                    if (strength < o.strength)
                        return -1;
                    else if (strength == o.strength)
                        return 0;
                    else
                        return 1;
                }
            }

            Array<ShipList> ships = new Array<ShipList>();
            for (int i = 0; i < system.getBuildableShips().size; i++) {
                int id = system.getBuildableShips().get(i);
                ShipInfo si = manager.getShipInfos().get(id - 10000);
                if (si.getShipType().getType() > ShipType.COLONYSHIP.getType()) {
                    int strenght = si.getCompleteOffensivePower() + si.getCompleteDefensivePower() / 2;
                    ships.add(new ShipList(id, strenght));
                }
            }
            ships.sort();
            ships.reverse();
            for (int i = 0; i < ships.size; i++) {
                if ((int) (RandUtil.random() * 4) < 3 || i == (ships.size - 1)) {
                    if (makeEntryInAssemblyList(ships.get(i).id)) {
                        manager.getAIPrios().choseCombatShipPrio(major.getRaceId());
                        return ships.get(i).id;
                    }
                } else
                    ships.removeIndex(i--);
            }
        } else if (chooseTroop && min > 0) {
            class TroopList implements Comparable<TroopList> {
                int id;
                int strength;

                public TroopList(int id, int strength) {
                    this.id = id;
                    this.strength = strength;
                }

                @Override
                public int compareTo(TroopList o) {
                    return GameConstants.compare(strength, o.strength);
                }
            }
            Array<TroopList> troops = new Array<TroopList>();
            for (int i = 0; i < system.getBuildableTroops().size; i++) {
                int id = system.getBuildableTroops().get(i);
                TroopInfo ti = manager.getTroopInfos().get(id);
                troops.add(new TroopList(id + 20000, ti.getOffense())); //the defense is irrelevant to the AI
            }
            troops.sort();
            //the AI should build always the strongest available troops
            if (troops.size != 0) {
                int id = troops.get(troops.size - 1).id;
                if (makeEntryInAssemblyList(id)) {
                    manager.getAIPrios().choseTroopPrio(major.getRaceId());
                    return id;
                }
            }
        }
        return 0;
    }

    /**
     * Function checks if the building cane be added to the assembly list
     * @param id - building/ship id
     * @return - true if successful
     */
    private boolean makeEntryInAssemblyList(int id) {
        //difficulty is added here
        float difficulty = manager.getGamePreferences().difficulty.getLevel();
        //if it's a human he shouldn't get a bonus from the difficulty
        if (major.isHumanPlayer() && system.getAutoBuild())
            difficulty = 1.0f;
        //if it's neverready, then the difficulty is 1
        if (id > 0 && id < 10000 && manager.getBuildingInfo(id).getNeverReady())
            difficulty = 1.0f;

        int runningNumber = id;
        if (id < 0)
            runningNumber *= (-1);
        if (id >= 10000 && id < 20000) {
            system.getAssemblyList().calculateNeededResources(null, manager.getShipInfos().get(id - 10000), null,
                    system.getAllBuildings(), id, major.getEmpire().getResearch().getResearchInfo(), difficulty);
        } else if (id >= 20000) {
            system.getAssemblyList().calculateNeededResources(null, null, manager.getTroopInfos().get(id - 20000),
                    system.getAllBuildings(), id, major.getEmpire().getResearch().getResearchInfo(), difficulty);
        } else {
            system.getAssemblyList().calculateNeededResources(manager.getBuildingInfo(runningNumber), null, null,
                    system.getAllBuildings(), id, major.getEmpire().getResearch().getResearchInfo(), difficulty);
        }
        return system.getAssemblyList().makeEntry(id, coord, manager);
    }

    /**
     * Function fills the buildings with workers
     */
    private void assignWorkers() {
        //Food production is the first priority, it will be occupied until the production is positive. If it's positive from the
        //beginning then we will try to occupy the minimum needed
        while (system.getProduction().getFoodProd() > 5) {
            if (system.getWorker(WorkerType.FOOD_WORKER) == 0)
                break;
            system.decrementWorker(WorkerType.FOOD_WORKER);
            calcProd();
        }

        while (system.getProduction().getFoodProd() < 0) {
            if (system.getWorker(WorkerType.FREE_WORKER) > 0) {
                if (system.getWorker(WorkerType.FOOD_WORKER) >= system.getNumberOfWorkBuildings(WorkerType.FOOD_WORKER,
                        0))
                    break;
                system.incrementWorker(WorkerType.FOOD_WORKER);
                calcProd();
            } else { //try to get  worker from somewhere else
                if (!dumpWorker())
                    break;
            }
        }

        //The next in importance is the energy supply of the buildings. We have to produce as much as it's enough to operate the buildings which need it
        int neededEnergy = 0;
        int usedEnergy = 0;
        for (int i = 0; i < system.getAllBuildings().size; i++) {
            Building building = system.getAllBuildings().get(i);
            BuildingInfo buildingInfo = manager.getBuildingInfo(building.getRunningNumber());
            if (buildingInfo.getNeededEnergy() > 0) {
                if (checkMorale(buildingInfo, false)) {
                    neededEnergy += buildingInfo.getNeededEnergy();
                    if (!building.getIsBuildingOnline())
                        if (system.getProduction().getEnergyProd() >= buildingInfo.getNeededEnergy() + usedEnergy) {
                            system.setIsBuildingOnline(i, true);
                            usedEnergy += buildingInfo.getNeededEnergy();
                        }
                } else {
                    //if it reduces morale, stop it
                    system.setIsBuildingOnline(i, false);
                }
            }
        }
        while (system.getProduction().getMaxEnergyProd() > (neededEnergy + 15)) {
            if (system.getWorker(WorkerType.ENERGY_WORKER) == 0)
                break;
            system.decrementWorker(WorkerType.ENERGY_WORKER);
            calcProd();
        }
        while (system.getProduction().getMaxEnergyProd() < neededEnergy) {
            if (system.getWorker(WorkerType.FREE_WORKER) > 0) {
                if (system.getWorker(WorkerType.ENERGY_WORKER) >= system.getNumberOfWorkBuildings(
                        WorkerType.ENERGY_WORKER, 0))
                    break;
                system.incrementWorker(WorkerType.ENERGY_WORKER);
                calcProd();
            } else { //try to get a worker from a different place
                if (!dumpWorker())
                    break;
            }
        }
        //from here set the workers on the other buildings, taking into account the priorities
        if (calcedPrio) {
            //collect the priorities and then distribute procentually
            int allPrio = 0;
            for (int i = WorkerType.INDUSTRY_WORKER.getType(); i <= WorkerType.IRIDIUM_WORKER.getType(); i++) {
                WorkerType worker = WorkerType.fromWorkerType(i);
                if (worker != WorkerType.ENERGY_WORKER) {
                    allPrio += priorities[i];
                    system.setWorker(worker, StarSystem.SetWorkerMode.SET_WORKER_MODE_SET, 0);
                }
            }

            int percentage[] = new int[WorkerType.ALL_WORKER.getType()];
            Arrays.fill(percentage, 0);
            if (allPrio != 0) {
                for (int i = WorkerType.INDUSTRY_WORKER.getType(); i <= WorkerType.IRIDIUM_WORKER.getType(); i++) {
                    WorkerType worker = WorkerType.fromWorkerType(i);
                    if (worker != WorkerType.ENERGY_WORKER)
                        percentage[i] = (priorities[i] * 100) / allPrio;
                }
            }

            //now distribute the workers according to percentages
            for (int i = WorkerType.INDUSTRY_WORKER.getType(); i <= WorkerType.IRIDIUM_WORKER.getType(); i++) {
                WorkerType worker = WorkerType.fromWorkerType(i);
                if (worker != WorkerType.ENERGY_WORKER) {
                    int freeWorkers = system.getWorker(WorkerType.FREE_WORKER);
                    int workers = (freeWorkers * percentage[i]) / 100;
                    for (int j = 0; j < workers; j++) {
                        if (system.getNumberOfWorkBuildings(worker, 0) > system.getWorker(worker)) {
                            system.incrementWorker(worker);
                            if (system.getWorker(worker) == system.getNumberOfWorkBuildings(worker, 0))
                                break;
                        }
                    }
                }
            }
        }

        //if there is something in the assembly list, then remove all workers except food and energy and fill industry first
        if (system.getAssemblyList().getAssemblyListEntry(0).id != 0) {
            if (system.getWorker(WorkerType.FREE_WORKER) > 0)
                while (system.getNumberOfWorkBuildings(WorkerType.INDUSTRY_WORKER, 0) > system
                        .getWorker(WorkerType.INDUSTRY_WORKER)) {
                    system.incrementWorker(WorkerType.INDUSTRY_WORKER);
                    if (system.getWorker(WorkerType.FREE_WORKER) == 0)
                        break;
                }
        } else { //if nothing is in the queue then we set the industry workers to 0
            system.setWorker(WorkerType.INDUSTRY_WORKER, StarSystem.SetWorkerMode.SET_WORKER_MODE_SET, 0);
        }

        //now distribute the remaining workers into the rest of the buildings
        int numberOfWorkBuildings = 0;
        int workers = 0;
        for (int i = WorkerType.SECURITY_WORKER.getType(); i <= WorkerType.IRIDIUM_WORKER.getType(); i++) {
            WorkerType worker = WorkerType.fromWorkerType(i);
            numberOfWorkBuildings += system.getNumberOfWorkBuildings(worker, 0);
            workers += system.getWorker(worker);
        }

        while (system.getWorker(WorkerType.FREE_WORKER) > 0 && workers < numberOfWorkBuildings) {
            //random worker
            int i = WorkerType.SECURITY_WORKER.getType()
                    + (int) (RandUtil.random() * (WorkerType.ALL_WORKER.getType() - WorkerType.SECURITY_WORKER
                            .getType()));
            WorkerType worker = WorkerType.fromWorkerType(i);
            if (system.getNumberOfWorkBuildings(worker, 0) > system.getWorker(worker)) {
                workers++;
                system.incrementWorker(worker);
            }
        }

        //if there are still workers left we give them to industry because this generates credits
        while (system.getWorker(WorkerType.FREE_WORKER) > 0) {
            if (system.getNumberOfWorkBuildings(WorkerType.INDUSTRY_WORKER, 0) > system
                    .getWorker(WorkerType.INDUSTRY_WORKER))
                system.incrementWorker(WorkerType.INDUSTRY_WORKER);
            else
                break;
        }
    }

    /**
     * This function tries to substract a worker from somewhere except food and energy
     * @return true if we could dump some workers
     */
    public boolean dumpWorker() {
        boolean getWorker = false;
        for (int i = WorkerType.INDUSTRY_WORKER.getType(); i <= WorkerType.IRIDIUM_WORKER.getType(); i++) {
            WorkerType worker = WorkerType.fromWorkerType(i);
            if (worker != WorkerType.ENERGY_WORKER) {
                if (system.getWorker(worker) > 0) {
                    getWorker = true;
                    system.decrementWorker(worker);
                    break;
                }
            }
        }
        return getWorker;
    }

    /**
     * This function removes unnecessary food and energy buildings
     */
    private void scrapBuildings() {
        //The AI deletes food and energy buildings which are not needed
        if (system.getProduction().getFoodProd() >= 0) {
            //if the system is at least 80% full
            float currentHab = system.getCurrentInhabitants();
            float maxHab = system.getMaxInhabitants();
            if (currentHab > (maxHab * 0.8)) {
                int n = system.getNumberOfWorkBuildings(WorkerType.FOOD_WORKER, 0)
                        - system.getWorker(WorkerType.FOOD_WORKER);
                int id = system.getNumberOfWorkBuildings(WorkerType.FOOD_WORKER, 1);
                while (n > 1) {
                    system.setBuildingDestroy(id, true);
                    n--;
                }
            }
        }

        if (system.getProduction().getEnergyProd() > 0) {
            int n = system.getNumberOfWorkBuildings(WorkerType.ENERGY_WORKER, 0)
                    - system.getWorker(WorkerType.ENERGY_WORKER);
            int id = system.getNumberOfWorkBuildings(WorkerType.ENERGY_WORKER, 1);
            while (n > 3) {
                system.setBuildingDestroy(id, true);
                n--;
            }
        }
    }

    /**
     * Function that calculates the priority with which a type of ship is built
     * @return the type of ship that should be built and the priority
     */
    Pair<Integer, boolean[]> getShipOrTroopBuildPrios() {
        boolean chooseCombatship = false;
        boolean chooseColoship = false;
        boolean chooseTransport = false;
        boolean chooseTroop = false;
        int min = 0;

        Race race = system.getOwner();
        if (race == null) {
            boolean[] prios = { chooseCombatship, chooseColoship, chooseTransport };
            return new Pair<Integer, boolean[]>(min, prios);
        }
        String raceID = race.getRaceId();

        if (system.getProduction().isShipyard() && system.getBuildableShips().size > 0) {
            if (manager.getAIPrios().getColoShipPrio(raceID) > 0) {
                min = (int) (RandUtil.random() * (manager.getAIPrios().getColoShipPrio(race.getRaceId()) + 1));
                chooseColoship = true;
            }
            if (manager.getAIPrios().getTransportShipPrio(raceID) > 0) {
                int random = (int) (RandUtil.random() * (manager.getAIPrios().getTransportShipPrio(race.getRaceId()) + 1));
                if (random > min) {
                    min = random;
                    chooseColoship = false;
                    chooseTransport = true;
                }
            }
            if (manager.getAIPrios().getCombatShipPrio(raceID) > 0) {
                int random = (int) (RandUtil.random() * (manager.getAIPrios().getCombatShipPrio(race.getRaceId()) + 1));
                if (random > min) {
                    int shipCosts = major.getEmpire().getPopSupportCosts() - major.getEmpire().getShipCosts();
                    //if the empire would make a negative credit and if this would be higher than 5% of all credits, then no ship is built
                    if ((shipCosts < 0 && Math.abs(shipCosts) > (int) (major.getEmpire().getCredits() * 0.05))
                            || major.getEmpire().getCredits() < 0) {
                        chooseCombatship = false;
                    } else {
                        min = random;
                        chooseColoship = false;
                        chooseTransport = false;
                        chooseCombatship = true;
                    }
                }
            }
        }
        if (system.getProduction().isBarrack() && system.getBuildableTroops().size > 0) {
            if (manager.getAIPrios().getTroopPrio(raceID) > 0) {
                int random = (int) (RandUtil.random() * (manager.getAIPrios().getTroopPrio(race.getRaceId()) + 1));
                //TODO: the support costs for troops is right now not implemented, fix this when it's done
                if (random > min) {
                    min = random;
                    chooseColoship = false;
                    chooseTransport = false;
                    chooseCombatship = false;
                    chooseTroop = true;
                }
            }
        }

        boolean[] prios = { chooseCombatship, chooseColoship, chooseTransport, chooseTroop };
        return new Pair<Integer, boolean[]>(min, prios);
    }

    /**
     * This function calculates the new Food, Industry and Energy production. It should be called when workers were assigned to these fields
     * Take note that all others will be deleted.
     */
    private void calcProd() {
        SystemProd production = system.getProduction();

        //set everything to 0
        production.reset();

        int foodWorker = system.getWorker(WorkerType.FOOD_WORKER);
        int industryWorker = system.getWorker(WorkerType.INDUSTRY_WORKER);
        int energyWorker = system.getWorker(WorkerType.ENERGY_WORKER);

        //calculate the productions
        int numberOfBuildings = system.getAllBuildings().size;
        for (int i = 0; i < numberOfBuildings; i++) {
            Building building = system.getAllBuildings().get(i);
            BuildingInfo bi = manager.getBuildingInfo(building.getRunningNumber());

            //put buildings offline
            if (bi.getWorker()) {
                building.setIsBuildingOnline(false);
                //if possible set them online
                if (bi.getFoodProd() > 0 && foodWorker > 0) {
                    building.setIsBuildingOnline(true);
                    foodWorker--;
                } else if (bi.getIndustryPointsProd() > 0 && industryWorker > 0) {
                    building.setIsBuildingOnline(true);
                    industryWorker--;
                } else if (bi.getEnergyProd() > 0 && energyWorker > 0) {
                    building.setIsBuildingOnline(true);
                    energyWorker--;
                }
            }
            //calculate the productions (without bonuses), check if they are online
            production.calculateProduction(bi, building.getIsBuildingOnline());
        }

        //if there are disabled productions, set them to 0
        production.disableProduction(system.getDisabledProductions());

        //calculate bonuses for the productions
        Research research = major.getEmpire().getResearch();
        int tmpFoodBonus = (int) (research.getBioTech() * GameConstants.TECH_PROD_BONUS);
        int tmpIndustryBonus = (int) (research.getConstructionTech() * GameConstants.TECH_PROD_BONUS);
        int tmpEnergyBonus = (int) (research.getEnergyTech() * GameConstants.TECH_PROD_BONUS);

        int neededEnergy = 0;
        for (int i = 0; i < numberOfBuildings; i++) {
            BuildingInfo buildingInfo = manager.getBuildingInfos().get(
                    system.getAllBuildings().get(i).getRunningNumber() - 1);
            Building building = system.getAllBuildings().get(i);
            // calculate needed energy by the buildings
            if (building.getIsBuildingOnline() && buildingInfo.getNeededEnergy() > 0)
                neededEnergy += buildingInfo.getNeededEnergy();
            if (building.getIsBuildingOnline()) {
                tmpFoodBonus += buildingInfo.getFoodBonus();
                tmpIndustryBonus += buildingInfo.getIndustryBonus();
                tmpEnergyBonus += buildingInfo.getEnergyBonus();
            }
        }
        for (Planet p : system.getPlanets()) {
            if (p.getIsInhabited()) {
                if (p.getBonuses()[ResourceTypes.FOOD.getType()])
                    tmpFoodBonus += (p.getSize().getSize() + 1) * 25;
                if (p.getBonuses()[7]) //energy
                    tmpEnergyBonus += (p.getSize().getSize() + 1) * 25;
            }
        }

        production.addFoodProd(tmpFoodBonus * production.getFoodProd() / 100);
        production.addIndustryProd(tmpIndustryBonus * production.getIndustryProd() / 100);
        production.addEnergyProd(tmpEnergyBonus * production.getEnergyProd() / 100);

        if (system.getBlockade() > 0) {
            production.addEnergyProd((-1) * (system.getBlockade() * production.getIndustryProd() / 100));
        }
        ///SPECIAL RESEARCH
        int bonus = research.getResearchInfo().isResearchedThenGetBonus(ResearchComplexType.ECONOMY, 1);
        if (bonus != 0)
            production.addIndustryProd(bonus * production.getIndustryProd() / 100);
        bonus = research.getResearchInfo().isResearchedThenGetBonus(ResearchComplexType.PRODUCTION, 1);
        if (bonus != 0)
            production.addFoodProd(bonus * production.getFoodProd() / 100);
        bonus = research.getResearchInfo().isResearchedThenGetBonus(ResearchComplexType.PRODUCTION, 3);
        if (bonus != 0)
            production.addEnergyProd(bonus * production.getEnergyProd() / 100);

        production.setMaxEnergyProd(production.getEnergyProd());
        production.addEnergyProd(-neededEnergy);

        production.addMoraleProd(major.getEmpire().getMoraleEmpireWide());
        production.includeSystemMorale(system.getMorale());
        //substract food
        if (!major.hasSpecialAbility(Race.RaceSpecialAbilities.SPECIAL_NEED_NO_FOOD.getAbility()))
            production.addFoodProd((-1) * (int) Math.ceil(system.getInhabitants() * 10));
        else
            production.setFoodProd(production.getMaxFoodProd());
    }

    /**
     * This function connects possibly a traderoute to a minor or major. Relationships with minors get improved by these
     */
    private void applyTradeRoutes() {
        String race = system.getOwnerId();
        if (system.canAddTradeRoute(major.getEmpire().getResearch().getResearchInfo())) {
            //first to minors
            ArrayMap<String, Minor> minors = manager.getRaceController().getMinors();
            for (Iterator<Entry<String, Minor>> iter = minors.entries().iterator(); iter.hasNext();) {
                Entry<String, Minor> e = iter.next();
                Minor minor = e.value;
                //not to minors without a home
                if (minor.isAlien() || minor.getCoordinates().equals(new IntPoint()))
                    continue;

                if (minor.getAgreement(race).getType() >= DiplomaticAgreement.TRADE.getType()
                        && minor.getAgreement(race).getType() < DiplomaticAgreement.MEMBERSHIP.getType())
                    if (!system.addTradeRoute(minor.getCoordinates(), manager.getUniverseMap().getStarSystems(), major
                            .getEmpire().getResearch().getResearchInfo()))
                        break;
            }
        }
        if (system.canAddTradeRoute(major.getEmpire().getResearch().getResearchInfo())) {
            for (int i = 0; i < manager.getUniverseMap().getStarSystems().size; i++) {
                StarSystem ss = manager.getUniverseMap().getStarSystems().get(i);
                if (ss.isMajorized() && !ss.getOwnerId().equals(race))
                    if (major.getAgreement(ss.getOwnerId()).getType() >= DiplomaticAgreement.TRADE.getType())
                        if (!system.addTradeRoute(ss.getCoordinates(), manager.getUniverseMap().getStarSystems(), major
                                .getEmpire().getResearch().getResearchInfo()))
                            break;
            }
        }
    }

    /**
     * Function checks whether a workertype exists in the list of buildable buildings
     * @return true if yes, otherwise the priority will be 0
     */
    private boolean checkBuilding(WorkerType worker) {
        for (int i = 0; i < system.getBuildableBuildings().size; i++) {
            int id = system.getBuildableBuildings().get(i);
            BuildingInfo building = manager.getBuildingInfo(id);

            if (worker == WorkerType.FOOD_WORKER && building.getFoodProd() > 0)
                return true;
            if (worker == WorkerType.INDUSTRY_WORKER && building.getIndustryPointsProd() > 0)
                return true;
            if (worker == WorkerType.ENERGY_WORKER && building.getEnergyProd() > 0)
                return true;
            if (worker == WorkerType.SECURITY_WORKER && building.getSecurityPointsProd() > 0)
                return true;
            if (worker == WorkerType.RESEARCH_WORKER && building.getResearchPointsProd() > 0)
                return true;
            if (worker == WorkerType.TITAN_WORKER && building.getTitanProd() > 0)
                return true;
            if (worker == WorkerType.DEUTERIUM_WORKER && building.getDeuteriumProd() > 0)
                return true;
            if (worker == WorkerType.DURANIUM_WORKER && building.getDuraniumProd() > 0)
                return true;
            if (worker == WorkerType.CRYSTAL_WORKER && building.getCrystalProd() > 0)
                return true;
            if (worker == WorkerType.IRIDIUM_WORKER && building.getIridiumProd() > 0)
                return true;
        }
        return false;
    }

    /**
     * Function calculates the priority of food production in the system
     * @param maxHab - maxium inhabitants of the system
     * @return - priority of the food production
     */
    private int getFoodPrio(double maxHab) {
        //if the maximum number of inhabitants is 1.25 times higher than the current number then the priority doubles
        double currHab = Math.max(1.0, system.getInhabitants());
        //calculate the remaining of the workers (divided by 1.5 thus making it a bit less important)
        int restWorkers = (int) ((maxHab - currHab) / 1.5);

        if (system.getWorker(WorkerType.ALL_WORKER) + restWorkers < system.getNumberOfWorkBuildings(
                WorkerType.FOOD_WORKER, 0))
            return 0;

        //check if there are buildings that help the priority
        if (!checkBuilding(WorkerType.FOOD_WORKER))
            return 0;

        //check if the production is positive, then we don't need more food
        int foodProd = system.getProduction().getFoodProd();
        if (foodProd >= 0)
            return 0;

        int prio = 0;
        //check the current stores and compare with the production
        foodProd *= (-5);
        int div = system.getFoodStore() + 1;
        if (div > 0)
            prio = foodProd * 100 / div;
        else
            prio = foodProd * 100;
        prio *= 4;

        return Math.min(prio, 255);
    }

    /**
     * Function returns the number of critical buildings (food energy)
     * @return - for now food worker buildings + energy worker buildings
     */
    private int getNumberOfCriticalWorkBuildings() {
        return system.getNumberOfWorkBuildings(WorkerType.FOOD_WORKER, 0)
                + system.getNumberOfWorkBuildings(WorkerType.ENERGY_WORKER, 0);
    }

    /**
     * Function calculates the priority of industry production in the system
     * @param maxHab - maxium inhabitants of the system
     * @return - priority of the industry production
     */
    private int getIndustryPrio(double maxHab) {
        //if the maximum number of inhabitants is 1.25 times higher than the current number then the priority doubles
        double currHab = Math.max(1.0, system.getInhabitants());
        //calculate the remaining of the workers (divided by 1.5 thus making it a bit less important)
        int restWorkers = (int) ((maxHab - currHab) / 1.5);

        if (system.getWorker(WorkerType.ALL_WORKER) + restWorkers < system.getNumberOfWorkBuildings(
                WorkerType.INDUSTRY_WORKER, 0) + getNumberOfCriticalWorkBuildings())
            return 0;

        //check if there are buildings that help the priority
        if (!checkBuilding(WorkerType.INDUSTRY_WORKER))
            return 0;

        //here we check the ipprod for all buildings (except upgrades and troops) and compare with the maximum possible industry output
        int ipProd = system.getProduction().getIndustryProd();

        //set the workers temporarily to max
        int workers = system.getWorker(WorkerType.INDUSTRY_WORKER);
        int number = system.getNumberOfWorkBuildings(WorkerType.INDUSTRY_WORKER, 0);
        if (workers == 0) {
            int id = system.getNumberOfWorkBuildings(WorkerType.INDUSTRY_WORKER, 1);
            if (id > 0)
                ipProd = manager.getBuildingInfo(id).getIndustryPointsProd() * number;
        } else {
            ipProd = ipProd * number / workers;
        }

        int midIPCosts = 0;
        int size = 0;
        for (int i = 0; i < system.getBuildableBuildings().size; i++) {
            int id = system.getBuildableBuildings().get(i);
            if (!manager.getBuildingInfo(id).getNeverReady()) {
                midIPCosts += manager.getBuildingInfo(id).getNeededIndustry();
                size++;
            }
        }

        for (int i = 0; i < system.getBuildableShips().size; i++) {
            int id = system.getBuildableShips().get(i);
            midIPCosts += manager.getShipInfos().get(id - 10000).getNeededIndustry();
            size++;
        }

        if (size > 0)
            midIPCosts /= size;

        int prio = 0;
        if (ipProd > 0)
            prio = midIPCosts / ipProd;
        else
            prio = midIPCosts;

        //if we can have more inhabitants then we can build more buildings
        double habMod = Math.max(1.0, maxHab / currHab);
        prio = (int) (prio * habMod + restWorkers);

        return Math.min(prio, 255);
    }

    /**
     * Function calculates the priority of energy production in the system
     * @param maxHab - maxium inhabitants of the system
     * @return - priority of the energy production
     */
    private int getEnergyPrio(double maxHab) {
        //if the maximum number of inhabitants is 1.25 times higher than the current number then the priority doubles
        double currHab = Math.max(1.0, system.getInhabitants());
        //calculate the remaining of the workers (divided by 1.5 thus making it a bit less important)
        int restWorkers = (int) ((maxHab - currHab) / 1.5);

        if (system.getWorker(WorkerType.ALL_WORKER) + restWorkers < system.getNumberOfWorkBuildings(
                WorkerType.ENERGY_WORKER, 0) + system.getNumberOfWorkBuildings(WorkerType.FOOD_WORKER, 0))
            return 0;

        //check if there are buildings that help the priority
        if (!checkBuilding(WorkerType.ENERGY_WORKER))
            return 0;

        //check the needed energy compared to the maximum energy
        int energyProd = system.getProduction().getMaxEnergyProd();
        //set workers to max temporarily
        int workers = system.getWorker(WorkerType.ENERGY_WORKER);
        int number = system.getNumberOfWorkBuildings(WorkerType.ENERGY_WORKER, 0);
        if (workers == 0) {
            int id = system.getNumberOfWorkBuildings(WorkerType.ENERGY_WORKER, 1);
            if (id > 0)
                energyProd = manager.getBuildingInfo(id).getEnergyProd() * number;
        } else {
            energyProd = energyProd * number / workers;
        }

        int neededEnergy = 0;
        for (int i = 0; i < system.getAllBuildings().size; i++) {
            BuildingInfo bi = manager.getBuildingInfo(system.getAllBuildings().get(i).getRunningNumber());
            neededEnergy += bi.getNeededEnergy();
        }

        if (energyProd >= neededEnergy)
            return 0;

        int prio = 0;
        if (energyProd > 0)
            prio = 10 * neededEnergy / energyProd;
        else
            prio = neededEnergy;

        return Math.min(prio, 255);
    }

    /**
     * Function calculates the priority of security production in the system
     * @param maxHab - maxium inhabitants of the system
     * @return - priority of the security production
     */
    private int getIntelPrio(double maxHab) {
        //if the maximum number of inhabitants is 1.25 times higher than the current number then the priority doubles
        double currHab = Math.max(1.0, system.getInhabitants());
        //calculate the remaining of the workers (divided by 1.5 thus making it a bit less important)
        int restWorkers = (int) ((maxHab - currHab) / 1.5);

        if (system.getWorker(WorkerType.ALL_WORKER) + restWorkers < system.getNumberOfWorkBuildings(
                WorkerType.SECURITY_WORKER, 0) + getNumberOfCriticalWorkBuildings())
            return 0;

        //check if there are buildings that help the priority
        if (!checkBuilding(WorkerType.SECURITY_WORKER))
            return 0;

        String raceId = system.getOwnerId();
        int prio = manager.getAIPrios().getIntelAI().getIntelPrio(raceId);

        return Math.min(prio, 255);
    }

    /**
     * Function calculates the priority of research production in the system
     * @param maxHab - maxium inhabitants of the system
     * @return - priority of the research production
     */
    private int getResearchPrio(double maxHab) {
        //if the maximum number of inhabitants is 1.25 times higher than the current number then the priority doubles
        double currHab = Math.max(1.0, system.getInhabitants());
        //calculate the remaining of the workers (divided by 1.5 thus making it a bit less important)
        int restWorkers = (int) ((maxHab - currHab) / 1.5);

        if (system.getWorker(WorkerType.ALL_WORKER) + restWorkers < system.getNumberOfWorkBuildings(
                WorkerType.RESEARCH_WORKER, 0) + getNumberOfCriticalWorkBuildings())
            return 0;

        //check if there are buildings that help the priority
        if (!checkBuilding(WorkerType.RESEARCH_WORKER))
            return 0;

        int prio = (int) (RandUtil.random() * 3);
        double habMod = Math.max(1.0, maxHab / currHab);
        prio = (int) (prio * habMod + restWorkers);

        return Math.min(prio, 255);
    }

    /**
     * Function calculates the priority of a resource production in the system
     * @param worker - type of resource worker
     * @param maxHab - max inhabitants of the system
     * @return - priority
     */
    private int getResourcePrio(WorkerType worker, double maxHab) {
        //check the existing resources, if a resource is inexistent, set the priority to 0
        boolean resExist[] = system.getAvailableResources(true);

        int res = worker.getResource().getType();
        if (!resExist[res])
            return 0;

        //if the maximum number of inhabitants is 1.25 times higher than the current number then the priority doubles
        double currHab = Math.max(1.0, system.getInhabitants());
        //calculate the remaining of the workers (divided by 1.5 thus making it a bit less important)
        int restWorkers = (int) ((maxHab - currHab) / 1.5);

        if (system.getWorker(WorkerType.ALL_WORKER) + restWorkers < system.getNumberOfWorkBuildings(worker, 0)
                + getNumberOfCriticalWorkBuildings())
            return 0;

        //check if there are buildings that help the priority
        if (!checkBuilding(worker))
            return 0;

        int resProd = system.getProduction().getResourceProd(worker.getResource());
        //set the workers temporarily to max
        int workers = system.getWorker(worker);
        int number = system.getNumberOfWorkBuildings(worker, 0);
        if (workers == 0) {
            int id = system.getNumberOfWorkBuildings(worker, 1);
            if (id > 0)
                resProd = manager.getBuildingInfo(id).getResourceProd(res) * number;
        } else {
            resProd = resProd * number / workers;
        }

        int midResCosts = 0;
        int size = 0;

        for (int j = 0; j < system.getBuildableBuildings().size; j++) {
            int id = system.getBuildableBuildings().get(j);
            if (!manager.getBuildingInfo(id).getNeverReady())
                midResCosts += manager.getBuildingInfo(id).getNeededResource(res);
        }

        for (int j = 0; j < system.getBuildableUpdates().size; j++) {
            int id = system.getBuildableUpdates().get(j);
            int preNumber = system.getNumberofBuilding(manager.getBuildingInfo(id).getPredecessorId());
            midResCosts += (manager.getBuildingInfo(id).getNeededResource(res) * preNumber);
            size++;
        }

        for (int j = 0; j < system.getBuildableShips().size; j++) {
            int id = system.getBuildableShips().get(j);
            if (manager.getShipInfos().get(id - 10000).getNeededResource(res) > 0) {
                midResCosts += manager.getShipInfos().get(id - 10000).getNeededResource(res);
                size++;
            }
        }

        size /= 3.0;
        if (size > 0)
            midResCosts /= size;

        int prio = 0;
        int resStoreDiv = (system.getResourceStore(res) + 1);
        if (resStoreDiv <= 0) //this can happen if there is a resource distributor and the stores are empty while building stuff
            resStoreDiv = 1;

        if (resProd > 0)
            prio = (100 * midResCosts / (resStoreDiv * resProd));
        else
            prio = ((100 * midResCosts / (resStoreDiv * (number + 1))));

        double habMod = Math.max(1.0, maxHab / currHab);
        prio = (int) (prio * habMod + restWorkers);

        return Math.min(prio, 255);
    }
}
