/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ai;

import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.ObjectMap;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.constants.AnswerStatus;
import com.blotunga.bote.constants.DiplomacyInfoType;
import com.blotunga.bote.constants.DiplomaticAgreement;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.RaceProperty;
import com.blotunga.bote.constants.ResourceTypes;
import com.blotunga.bote.races.DiplomacyInfo;
import com.blotunga.bote.races.GenDiploMessage;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.races.Minor;
import com.blotunga.bote.races.Race;
import com.blotunga.bote.races.Race.RaceSpecialAbilities;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.utils.IntPoint;
import com.blotunga.bote.utils.RandUtil;

public class MajorAI extends DiplomacyAI {
    private String favoriteMinor;	//favorite minor of the race

    public MajorAI(ResourceManager manager, Race race) {
        super(manager, race);
        favoriteMinor = "";
    }

    @Override
    public AnswerStatus reactOnOffer(DiplomacyInfo info) {
        if (!race.getRaceId().equals(info.toRace)) {
            Gdx.app.debug("MajorAI", String.format("ReactOnOffer(): Warning: Race-ID %s difference from Info-ID %s",
                    race.getRaceId(), info.toRace));
            return AnswerStatus.NOT_REACTED;
        }

        Race fromRace = manager.getRaceController().getRace(info.fromRace);
        if (fromRace == null)
            return AnswerStatus.NOT_REACTED;
        if (fromRace.isMajor())
            return reactOnMajorOffer(info);
        else if (fromRace.isMinor())
            return reactOnMinorOffer(info);

        return AnswerStatus.NOT_REACTED;
    }

    @Override
    public boolean makeOffer(String otherRaceID, DiplomacyInfo info) {
        Race otherRace = manager.getRaceController().getRace(otherRaceID);
        if (otherRace == null)
            return false;

        if (otherRace.isMajor())
            return makeMajorOffer(otherRaceID, info);
        else if (otherRace.isMinor())
            return makeMinorOffer(otherRaceID, info);
        else
            return false;
    }

    public void calcFavoriteMinors() {
        favoriteMinor = "";
        Major ourRace = Major.toMajor(race);
        if (ourRace == null)
            return;
        class MinorList implements Comparable<MinorList> {
            public String id;
            public int relation;

            public MinorList(String id, int relation) {
                this.id = id;
                this.relation = relation;
            }

            @Override
            public int compareTo(MinorList o) {
                if (relation < o.relation)
                    return -1;
                else if (relation == o.relation)
                    return 0;
                else
                    return 1;
            }
        }

        Array<MinorList> favoriteMinors = new Array<MinorList>();
        ArrayMap<String, Minor> minors = manager.getRaceController().getMinors();
        for (Iterator<ObjectMap.Entry<String, Minor>> iter = minors.entries().iterator(); iter.hasNext();) {
            ObjectMap.Entry<String, Minor> e = iter.next();
            Minor minor = e.value;
            if (ourRace.isRaceContacted(minor.getRaceId()) && !minor.isSubjugated() && !minor.isAlien())//do we know the other race?
                if (minor.getRelation(ourRace.getRaceId()) < 95
                        & minor.getAgreement(ourRace.getRaceId()).getType() < DiplomaticAgreement.MEMBERSHIP.getType()) { //is the relationship smaller than 95 and no member?
                    //is the acceptance with other majors to high?
                    boolean highAcceptance = false;
                    ArrayMap<String, Major> majors = manager.getRaceController().getMajors();
                    for (int i = 0; i < majors.size; i++) {
                        String otherMajorId = majors.getKeyAt(i);
                        if (!otherMajorId.equals(ourRace.getRaceId()))
                            if ((minor.isMemberTo(otherMajorId) && minor.getAcceptancePoints(otherMajorId) > 3000)
                                    || minor.getAcceptancePoints(otherMajorId) > 4000) {
                                highAcceptance = true;
                                break;
                            }
                    }

                    //if the minor has already a high acceptance with others then it can't be a favorite
                    if (highAcceptance)
                        continue;
                    favoriteMinors.add(new MinorList(minor.getRaceId(), minor.getRelation(ourRace.getRaceId())));
                }
        }
        favoriteMinors.sort();
        favoriteMinors.reverse();

        //10% chance of picking the 3rd
        if (favoriteMinors.size >= 3 && (int) (RandUtil.random() * 10) == 0)
            favoriteMinor = favoriteMinors.get(2).id;
        //33% chance of the 2nd
        else if (favoriteMinors.size >= 2 && (int) (RandUtil.random() * 3) == 0)
            favoriteMinor = favoriteMinors.get(1).id;
        else if (favoriteMinors.size > 0)
            favoriteMinor = favoriteMinors.get(0).id;
        Gdx.app.debug("MajorAI",
                String.format("Favorite Minorrace of Major: %s is %s", race.getRaceId(), favoriteMinor));
    }

    /**
     * This function calculates how the AI responds to a minor offer
     * @param info
     * @return
     */
    private AnswerStatus reactOnMinorOffer(DiplomacyInfo info) {
        if (info.type == DiplomaticAgreement.WAR)
            return AnswerStatus.ACCEPTED;

        //to 89% we accept, 9% not reacted and 2% rejected
        int result = (int) (RandUtil.random() * 100);
        if (result > 10)
            return AnswerStatus.ACCEPTED;
        if (result > 1)
            return AnswerStatus.NOT_REACTED;
        return AnswerStatus.DECLINED;
    }

    /**
     * This function calculates how an AI responds to an offer of another major
     * @param info
     * @return
     */
    private AnswerStatus reactOnMajorOffer(DiplomacyInfo info) {
        //here we calculated the relation change by  a present
        if (info.type == DiplomaticAgreement.PRESENT) {
            reactOnDowry(info);
            return AnswerStatus.ACCEPTED;
        }

        //check if the offer is accepted
        int neededRelation = 100;
        //First we calculate the value that has to be reached by random. The higher this value the less likely to accept the offer. This value depends on the offer and our race type
        if (info.type == DiplomaticAgreement.NAP) {
            neededRelation = 35;
            if (race.isRaceProperty(RaceProperty.HOSTILE))
                neededRelation += 30;
            if (race.isRaceProperty(RaceProperty.WARLIKE))
                neededRelation += 20;
            if (race.isRaceProperty(RaceProperty.SECRET))
                neededRelation += 10;
            if (race.isRaceProperty(RaceProperty.SCIENTIFIC))
                neededRelation -= 10;
            if (race.isRaceProperty(RaceProperty.SOLOING))
                neededRelation -= 15;
            if (race.isRaceProperty(RaceProperty.PACIFIST))
                neededRelation -= 25;
        } else if (info.type == DiplomaticAgreement.TRADE) {
            neededRelation = 55;
            if (race.isRaceProperty(RaceProperty.HOSTILE))
                neededRelation += 30;
            if (race.isRaceProperty(RaceProperty.SOLOING))
                neededRelation += 20;
            if (race.isRaceProperty(RaceProperty.SECRET))
                neededRelation += 5;
            if (race.isRaceProperty(RaceProperty.PRODUCER))
                neededRelation -= 10;
            if (race.isRaceProperty(RaceProperty.FINANCIAL))
                neededRelation -= 25;
        } else if (info.type == DiplomaticAgreement.FRIENDSHIP) {
            neededRelation = 70;
            if (race.isRaceProperty(RaceProperty.HOSTILE))
                neededRelation += 20;
            if (race.isRaceProperty(RaceProperty.SOLOING))
                neededRelation += 15;
            if (race.isRaceProperty(RaceProperty.WARLIKE))
                neededRelation += 10;
            if (race.isRaceProperty(RaceProperty.SECRET))
                neededRelation += 5;
            if (race.isRaceProperty(RaceProperty.FINANCIAL))
                neededRelation -= 10;
            if (race.isRaceProperty(RaceProperty.SCIENTIFIC))
                neededRelation -= 20;
            if (race.isRaceProperty(RaceProperty.PACIFIST))
                neededRelation -= 25;
        } else if (info.type == DiplomaticAgreement.DEFENCEPACT) {
            neededRelation = 70;
            if (race.isRaceProperty(RaceProperty.SOLOING))
                neededRelation += 10;
            if (race.isRaceProperty(RaceProperty.SCIENTIFIC))
                neededRelation -= 5;
            if (race.isRaceProperty(RaceProperty.AGRARIAN))
                neededRelation -= 10;
            if (race.isRaceProperty(RaceProperty.FINANCIAL))
                neededRelation -= 15;
        } else if (info.type == DiplomaticAgreement.COOPERATION) {
            neededRelation = 80;
            if (race.isRaceProperty(RaceProperty.HOSTILE))
                neededRelation += 20;
            if (race.isRaceProperty(RaceProperty.SOLOING))
                neededRelation += 15;
            if (race.isRaceProperty(RaceProperty.SECRET))
                neededRelation += 10;
            if (race.isRaceProperty(RaceProperty.AGRARIAN))
                neededRelation -= 5;
            if (race.isRaceProperty(RaceProperty.FINANCIAL))
                neededRelation -= 10;
            if (race.isRaceProperty(RaceProperty.SCIENTIFIC))
                neededRelation -= 15;
            if (race.isRaceProperty(RaceProperty.PACIFIST))
                neededRelation -= 20;
        } else if (info.type == DiplomaticAgreement.ALLIANCE) {
            neededRelation = 90;
            if (race.isRaceProperty(RaceProperty.HOSTILE))
                neededRelation += 15;
            if (race.isRaceProperty(RaceProperty.SOLOING))
                neededRelation += 10;
            if (race.isRaceProperty(RaceProperty.SECRET))
                neededRelation += 5;
            if (race.isRaceProperty(RaceProperty.SCIENTIFIC))
                neededRelation -= 5;
            if (race.isRaceProperty(RaceProperty.FINANCIAL))
                neededRelation -= 10;
            if (race.isRaceProperty(RaceProperty.PACIFIST))
                neededRelation -= 15;
        } else if (info.type == DiplomaticAgreement.WARPACT) {
            int relationValue = race.getRelation(info.warpactEnemy);

            //if it's a sneaky race then the relation is only a quarter of it's worth
            if (race.isRaceProperty(RaceProperty.SNEAKY) && relationValue > 0)
                relationValue /= 4;

            neededRelation = 40 + relationValue;
            if (race.isRaceProperty(RaceProperty.PACIFIST))
                neededRelation += 270;
            if (race.isRaceProperty(RaceProperty.AGRARIAN))
                neededRelation += 20;
            if (race.isRaceProperty(RaceProperty.SOLOING))
                neededRelation += 15;
            if (race.isRaceProperty(RaceProperty.SCIENTIFIC))
                neededRelation += 10;
            if (race.isRaceProperty(RaceProperty.SECRET))
                neededRelation += 5;
            if (race.isRaceProperty(RaceProperty.FINANCIAL))
                neededRelation -= 5;
            if (race.isRaceProperty(RaceProperty.SNEAKY))
                neededRelation -= 15;
            if (race.isRaceProperty(RaceProperty.WARLIKE))
                neededRelation -= 25;
            if (race.isRaceProperty(RaceProperty.HOSTILE))
                neededRelation -= 40;
        } else if (info.type == DiplomaticAgreement.REQUEST)
            return calcDiplomacyRequest(info);

        //if a gift was included, improve the relation
        int oldRelation = race.getRelation(info.fromRace);
        reactOnDowry(info);

        //if we have also made an offer that's higher or equal in the last 2 rounds then we can accept it as is
        if (info.type.getType() > DiplomaticAgreement.NONE.getType()
                && info.flag == DiplomacyInfoType.DIPLOMACY_OFFER.getType()) {
            DiplomacyInfo lastOffer = race.getLastOffer(info.fromRace);
            if (lastOffer != null)
                if (info.type.getType() <= lastOffer.type.getType())
                    return AnswerStatus.ACCEPTED;
        }

        //here we compare the ship strengths of the races - if the other race is stronger, we have a higher chance of accepting
        int ourShipPower = manager.getStatistics().getShipPower(info.toRace);
        int theirShipPower = manager.getStatistics().getShipPower(info.fromRace);

        if (ourShipPower > 0) {
            float compare = (float) theirShipPower / (float) ourShipPower;
            if (compare > 1.0f) {
                int minus = (int) (compare * (RandUtil.random() * 6 + 5));
                neededRelation -= minus;
            }
        }

        int relation = race.getRelation(info.fromRace);
        double multi = (relation + 125.0) / 100.0;
        double value = relation * multi;
        int random = (int) value;
        random = Math.max(random, 1);
        int temp = 0;
        for (int i = 0; i < 5; i++)
            temp += (int) (RandUtil.random() * random + 1);
        random = temp / 5;

        //If the offer comes from a weak race and we are also weak, we probably will accept the offer. This is because we shouldn't make weak races even weaker, but rather they should cooperate to battle a stronger threat
        if ((int) (RandUtil.random() * 2) == 0) {
            int maxShipPower = 0;
            ArrayMap<String, Major> majors = manager.getRaceController().getMajors();
            for (int i = 0; i < majors.size; i++) {
                int shipPower = manager.getStatistics().getShipPower(majors.getKeyAt(i));
                maxShipPower = Math.max(maxShipPower, shipPower);
            }

            //if our ship power and the power of the race who made the offer are less than half of the max ship power, we'll take the offer
            if (theirShipPower < maxShipPower * 0.5f)
                if (ourShipPower < maxShipPower * 0.5f)
                    neededRelation = 0;
        }

        if (random >= neededRelation)
            return AnswerStatus.ACCEPTED;

        //restore relationship
        int currentRelation = race.getRelation(info.fromRace);
        if (currentRelation > oldRelation)
            race.setRelation(info.fromRace, oldRelation - currentRelation);

        if ((int) (RandUtil.random() * 10) > 0)
            return AnswerStatus.DECLINED;
        else
            return AnswerStatus.NOT_REACTED;
    }

    /**
     * This function calculates how a major reacts to a demand
     * @param info
     * @return
     */
    private AnswerStatus calcDiplomacyRequest(DiplomacyInfo info) {
        //First we check if the relationship between our two races and what type of treaties do we have. The better the relationship and higher the treaty the higher the chacne
        //Also we have to have more by a percentage than the average resources. Also we check where we have the most resources and if we can accept then it will be accepted.

        //10% chance of not reacting at all
        if ((int) (RandUtil.random() * 10) == 0)
            return AnswerStatus.NOT_REACTED;

        Major major = Major.toMajor(race);
        if (major == null)
            return AnswerStatus.NOT_REACTED;

        int relation = major.getRelation(info.fromRace);
        int agreement = major.getAgreement(info.fromRace).getType();

        AnswerStatus answer = AnswerStatus.ACCEPTED;
        //(14- (our relationship/10 + treaty))/10, 14 is the maximum that can be reached
        double value = (14 - ((double) relation / 10 + agreement)) / 10;

        //here we also compare our relative ship strengths
        int ourShipPower = manager.getStatistics().getShipPower(info.toRace);
        int theirShipPower = manager.getStatistics().getShipPower(info.fromRace);
        if (ourShipPower > 10) {
            float compare = (float) theirShipPower / (float) ourShipPower;
            if (compare > 1.0f)
                value -= compare;
            else
                value += compare;
        }

        value = Math.max(value, 0);
        //the lower the value the higher the chance of accepting
        value += 1;

        if (info.credits > 0) {

            int credits = 0;
            int races = 0;

            ArrayMap<String, Major> majors = manager.getRaceController().getMajors();
            for (int i = 0; i < majors.size; i++) {
                Major m = majors.getValueAt(i);
                credits += m.getEmpire().getCredits();
                races++;
            }

            if (races != 0)
                credits /= races;

            //check if we have more than the average credits
            if (major.getEmpire().getCredits() > (credits * value))
                answer = AnswerStatus.ACCEPTED;
            else
                answer = AnswerStatus.DECLINED;
        }

        //if resources are asked
        if (answer == AnswerStatus.ACCEPTED) {
            int res = -1;
            for (int i = ResourceTypes.TITAN.getType(); i <= ResourceTypes.DERITIUM.getType(); i++) {
                if (info.resources[i] != 0)
                    res = i;
            }

            if (res != -1) {
                if (major.getEmpire().getStorage()[res] > (manager.getStatistics().getAverageResourceStorages()[res] * value)) {
                    //now we can normally accept, but first we have to go trough our systems and check if we have enough of the resource
                    int maxRes = 0;
                    IntPoint coord = new IntPoint();
                    for (int i = 0; i < manager.getUniverseMap().getStarSystems().size; i++) {
                        StarSystem ss = manager.getUniverseMap().getStarSystems().get(i);
                        if (ss.getOwnerId().equals(info.toRace)) {
                            maxRes = Math.max(maxRes, ss.getResourceStore(res));
                            coord = ss.getCoordinates();
                        }
                    }

                    if (!coord.equals(new IntPoint()) && maxRes >= info.resources[res]) {
                        manager.getUniverseMap().getStarSystemAt(coord).addResourceStore(res, -info.resources[res]);
                    } else
                        answer = AnswerStatus.DECLINED;
                } else
                    answer = AnswerStatus.DECLINED;
            }
        }

        //now we can also subtract the credits
        if (answer == AnswerStatus.ACCEPTED && info.credits > 0)
            major.getEmpire().setCredits(-info.credits);
        return answer;
    }

    /**
     * This function creates an offer to a minor
     * @param raceID
     * @return
     */
    private boolean makeMinorOffer(String raceID, DiplomacyInfo info) {
        Major ourRace = Major.toMajor(race);
        if (ourRace == null)
            return false;

        Minor minor = Minor.toMinor(manager.getRaceController().getRace(raceID));
        if (minor == null)
            return false;

        DiplomaticAgreement offer = DiplomaticAgreement.NONE;
        if (ourRace.isRaceContacted(raceID) && !minor.isSubjugated()) {
            int theirRelationToUs = minor.getRelation(ourRace.getRaceId());
            DiplomaticAgreement agreement = ourRace.getAgreement(raceID);

            //if our relationship is high enough and we don't have the type of treaty then it's possible that we will make an offer
            //if we can make an offer but we can't because of treaties with other majors then we will try to bribe them if it's our favorite and the relationship is high enough
            DiplomaticAgreement othersAgreement = DiplomaticAgreement.NONE;
            String corruptedMajor = "";

            boolean isAlienDiplomacy = false;
            if (ourRace.hasSpecialAbility(RaceSpecialAbilities.SPECIAL_ALIEN_DIPLOMACY.getAbility())
                    || minor.hasSpecialAbility(RaceSpecialAbilities.SPECIAL_ALIEN_DIPLOMACY.getAbility()))
                isAlienDiplomacy = true;

            ArrayMap<String, Major> majors = manager.getRaceController().getMajors();
            for (int i = 0; i < majors.size; i++) {
                String otherID = majors.getKeyAt(i);
                DiplomaticAgreement temp = minor.getAgreement(otherID);
                if (!ourRace.getRaceId().equals(otherID) && temp.getType() > othersAgreement.getType()) {
                    othersAgreement = temp;
                    corruptedMajor = otherID;
                }
            }

            if (theirRelationToUs > 90 && agreement.getType() < DiplomaticAgreement.MEMBERSHIP.getType()
                    && othersAgreement.getType() < DiplomaticAgreement.COOPERATION.getType())
                offer = DiplomaticAgreement.MEMBERSHIP;
            else if (theirRelationToUs > 85 && agreement.getType() < DiplomaticAgreement.ALLIANCE.getType()
                    && othersAgreement.getType() < DiplomaticAgreement.COOPERATION.getType())
                offer = DiplomaticAgreement.ALLIANCE;
            else if (theirRelationToUs > 80 && agreement.getType() < DiplomaticAgreement.COOPERATION.getType()
                    && othersAgreement.getType() < DiplomaticAgreement.COOPERATION.getType())
                offer = DiplomaticAgreement.COOPERATION;
            else if (theirRelationToUs > 70 && agreement.getType() < DiplomaticAgreement.FRIENDSHIP.getType()
                    && othersAgreement.getType() < DiplomaticAgreement.ALLIANCE.getType())
                offer = DiplomaticAgreement.FRIENDSHIP;
            else if (theirRelationToUs > 55 && agreement.getType() < DiplomaticAgreement.TRADE.getType()
                    && othersAgreement.getType() < DiplomaticAgreement.MEMBERSHIP.getType())
                offer = DiplomaticAgreement.TRADE;

            //now we check the race type and if for example we are hostile then we might declare war
            //also check whether the minor has an ally, because if our relation to the other major is good then we won't declare war
            if (othersAgreement.getType() < DiplomaticAgreement.MEMBERSHIP.getType()
                    && offer == DiplomaticAgreement.NONE) {
                int minRel = 0;
                if (ourRace.isRaceProperty(RaceProperty.HOSTILE))
                    minRel += 25;
                if (ourRace.isRaceProperty(RaceProperty.WARLIKE))
                    minRel += 15;
                if (ourRace.isRaceProperty(RaceProperty.SNEAKY))
                    minRel += 5;
                if (ourRace.isRaceProperty(RaceProperty.FINANCIAL))
                    minRel -= 5;
                if (ourRace.isRaceProperty(RaceProperty.SCIENTIFIC))
                    minRel -= 5;
                if (ourRace.isRaceProperty(RaceProperty.PACIFIST))
                    minRel -= 55;

                if (theirRelationToUs < minRel && agreement != DiplomaticAgreement.WAR)
                    offer = DiplomaticAgreement.WAR;

                //check for allies
                if (offer == DiplomaticAgreement.WAR) {
                    for (int i = 0; i < majors.size; i++) {
                        String otherID = majors.getKeyAt(i);
                        if (!otherID.equals(ourRace.getRaceId())
                                && minor.getAgreement(otherID) == DiplomaticAgreement.ALLIANCE)
                            if (ourRace.getRelation(otherID) > (int) (RandUtil.random() * 21) + 40) {
                                offer = DiplomaticAgreement.NONE;
                                break;
                            }
                    }
                }
            }

            //set the important parameters
            info.flag = DiplomacyInfoType.DIPLOMACY_OFFER.getType();
            info.fromRace = ourRace.getRaceId();
            info.toRace = raceID;
            info.sendRound = manager.getCurrentRound() - 1;

            if (!isAlienDiplomacy) {
                //now we have a treaty
                boolean isFavorite = minor.getRaceId().equals(favoriteMinor);
                //if it's a favorite then we also give gifts
                if (!isAlienDiplomacy && isFavorite && offer.getType() > DiplomaticAgreement.NONE.getType())
                    giveDowry(info);

                //if we made no offer, or we can't make any offer then we might give a present or a bribe
                if (!isAlienDiplomacy && isFavorite && offer == DiplomaticAgreement.NONE && theirRelationToUs <= 91)
                    if(giveDowry(info))
                        offer = DiplomaticAgreement.PRESENT;

                //if we can't make a present to the favorite race then maybe we can bribe it
                if (isFavorite && offer == DiplomaticAgreement.NONE) {
                    //we can only bribe if we have at least Friendship with the minor or it has an alliance with someone
                    if (othersAgreement.getType() >= DiplomaticAgreement.ALLIANCE.getType()) {
                        if (giveDowry(info)) {
                            offer = DiplomaticAgreement.CORRUPTION;
                            info.corruptedRace = corruptedMajor;
                        }
                    } else if (minor.getAgreement(ourRace.getRaceId()).getType() >= DiplomaticAgreement.FRIENDSHIP
                            .getType()
                            && minor.getAgreement(ourRace.getRaceId()) != DiplomaticAgreement.MEMBERSHIP
                            && othersAgreement.getType() > DiplomaticAgreement.NONE.getType()) {
                        //here we must find a target for the corruption
                        Array<String> corrupted = new Array<String>();
                        for (int i = 0; i < majors.size; i++) {
                            String otherId = majors.getKeyAt(i);
                            if (!otherId.equals(ourRace.getRaceId())
                                    && minor.getAgreement(otherId).getType() > DiplomaticAgreement.NONE.getType())
                                corrupted.add(otherId);
                        }
                        if (corrupted.size != 0) {
                            corruptedMajor = corrupted.get((int) (RandUtil.random() * corrupted.size));
                            if (giveDowry(info)) {
                                offer = DiplomaticAgreement.CORRUPTION;
                                info.corruptedRace = corruptedMajor;
                            }
                        }
                    }
                }
            } else {
                //in alien diplomacy only friendship and war can be offered
                if (offer.getType() >= DiplomaticAgreement.FRIENDSHIP.getType()) {
                    if (agreement.getType() < DiplomaticAgreement.FRIENDSHIP.getType())
                        offer = DiplomaticAgreement.FRIENDSHIP;
                    else
                        offer = DiplomaticAgreement.NONE;
                }
                if (offer != DiplomaticAgreement.WAR && offer != DiplomaticAgreement.FRIENDSHIP)
                    offer = DiplomaticAgreement.NONE;
            }
        }

        if (offer != DiplomaticAgreement.NONE) {
            info.type = offer;
            GenDiploMessage.generateMajorOffer(manager, info);
            Gdx.app.debug("MajorAI", String.format("Major: %s makes offer %s to Minor %s", info.fromRace,
                    info.type.getdbName(), info.toRace));
            return true;
        } else
            return false;
    }

    /**
     * @param raceID
     * @return
     */
    private boolean makeMajorOffer(String raceID, DiplomacyInfo info) {
        Major ourRace = Major.toMajor(race);
        if (ourRace == null)
            return false;

        Major theirRace = Major.toMajor(manager.getRaceController().getRace(raceID));
        if (theirRace == null)
            return false;

        if (ourRace.isRaceContacted(raceID)) {
            int ourRelationToThem = ourRace.getRelation(raceID);
            int theirRelationToUs = theirRace.getRelation(ourRace.getRaceId());
            DiplomaticAgreement agreement = ourRace.getAgreement(raceID);

            double multi = (ourRelationToThem + 100) / 100.0;
            double value = ourRelationToThem * multi;
            int random = (int) value;
            int temp = 0;
            for (int i = 0; i < 3; i++)
                temp += (int) (RandUtil.random() * random + 1);
            random = temp / 3;

            //here we compare our ship strengths - the higher the strength, the higher the chance of offering
            int ourShipPower = manager.getStatistics().getShipPower(ourRace.getRaceId());
            int theirShipPower = manager.getStatistics().getShipPower(raceID);

            float compare = 0.0f;
            if (ourShipPower != 0) {
                compare = (float) theirShipPower / (float) ourShipPower;
                if (compare > 1.0f)
                    random += (int) (compare * 5);
                else if ((random - (int) (compare * 5)) > 0)
                    random -= (int) (compare * 5);
                else
                    random = 0;
            }

            //in case of alien diplomacy only war, friendship and NAP can be offered
            boolean isAlienDiplomacy = false;
            if (ourRace.hasSpecialAbility(RaceSpecialAbilities.SPECIAL_ALIEN_DIPLOMACY.getAbility())
                    || theirRace.hasSpecialAbility(RaceSpecialAbilities.SPECIAL_ALIEN_DIPLOMACY.getAbility()))
                isAlienDiplomacy = true;

            //33% chance of offering something
            DiplomaticAgreement offer = DiplomaticAgreement.NONE;
            if ((int) (RandUtil.random() * 3) == 0) {
                if (random > getMinOfferValue(DiplomaticAgreement.ALLIANCE)
                        && agreement.getType() < DiplomaticAgreement.ALLIANCE.getType())
                    offer = DiplomaticAgreement.ALLIANCE;
                else if (random > getMinOfferValue(DiplomaticAgreement.COOPERATION)
                        && agreement.getType() < DiplomaticAgreement.COOPERATION.getType())
                    offer = DiplomaticAgreement.COOPERATION;
                else if (random > getMinOfferValue(DiplomaticAgreement.DEFENCEPACT) && !ourRace.getDefencePact(raceID)
                        && agreement != DiplomaticAgreement.WAR && agreement != DiplomaticAgreement.ALLIANCE)
                    offer = DiplomaticAgreement.DEFENCEPACT;
                else if (random > getMinOfferValue(DiplomaticAgreement.FRIENDSHIP)
                        && agreement.getType() < DiplomaticAgreement.FRIENDSHIP.getType())
                    offer = DiplomaticAgreement.FRIENDSHIP;
                else if (random > getMinOfferValue(DiplomaticAgreement.TRADE)
                        && agreement.getType() < DiplomaticAgreement.TRADE.getType())
                    offer = DiplomaticAgreement.TRADE;
                else if (random > getMinOfferValue(DiplomaticAgreement.NAP)
                        && agreement.getType() < DiplomaticAgreement.NAP.getType())
                    offer = DiplomaticAgreement.NAP;

                //just to make sure
                if (agreement.getType() >= offer.getType() && offer != DiplomaticAgreement.DEFENCEPACT) //defencepact is negative
                    offer = DiplomaticAgreement.NONE;

                //Now check that the relationships are not too far off. A human wouldn't offer anything to another if he likes them but the the other doesn't likes him so he'll probably reject
                //The higher the relationship, the higher it can be off. The higher the offer, the less it can be off
                //40 + mod - abs(offer*5)*ourRelation/100 - this is how much it can differ - 40 is an experimental value
                //mod - if our military is weaker, the difference can be higher
                int mod = 0;
                if (compare > 1.0f)
                    mod = (int) (compare * 5);
                if (Math.abs(theirRelationToUs - ourRelationToThem) > (double) (40 + mod + Math
                        .abs(offer.getType() * 5)) * (double) (ourRelationToThem) / 100.0) {
                    offer = DiplomaticAgreement.NONE;
                }

                //take into account the type of the race
                int minRel = 15;
                if (ourRace.isRaceProperty(RaceProperty.HOSTILE))
                    minRel += 25;
                if (ourRace.isRaceProperty(RaceProperty.WARLIKE))
                    minRel += 15;
                if (ourRace.isRaceProperty(RaceProperty.SNEAKY))
                    minRel += 5;
                if (ourRace.isRaceProperty(RaceProperty.FINANCIAL))
                    minRel -= 5;
                if (ourRace.isRaceProperty(RaceProperty.SCIENTIFIC))
                    minRel -= 10;
                if (ourRace.isRaceProperty(RaceProperty.PACIFIST))
                    minRel -= 15;

                float modi = 0.0f;
                if (theirShipPower > 0)
                    modi = (float) ourShipPower / (float) theirShipPower;
                if (modi > 1.0f)
                    modi *= 3.0f;

                //if we're stronger
                if (compare < 1.0f)
                    minRel += (int) modi;	//this way sometimes war is declared even if the relationship is still ok
                if (modi > 1.0f && random + Math.abs(agreement.getType() * 2) < minRel
                        && ourRelationToThem + Math.abs(agreement.getType() * 2) < minRel
                        && agreement != DiplomaticAgreement.WAR)
                    offer = DiplomaticAgreement.WAR;

                //here it's possible that we offer a WarPact - it can be only if we don't yet have war with the race
                if (!isAlienDiplomacy && offer == DiplomaticAgreement.WAR && (int) (RandUtil.random() * 4) > 0) { //75% chance
                    String likeBest = "";
                    String likeSecond = "";
                    int rtemp = 0;
                    ArrayMap<String, Major> majors = manager.getRaceController().getMajors();
                    for (int i = 0; i < majors.size; i++) {
                        String otherID = majors.getKeyAt(i);
                        Major other = majors.getValueAt(i);
                        if (!otherID.equals(ourRace.getRaceId()) && !otherID.equals(raceID))
                            if (other.isRaceContacted(ourRace.getRaceId()) && other.isRaceContacted(raceID)) {
                                int rel = ourRace.getRelation(otherID);
                                if (rel > rtemp && rel > (int) (RandUtil.random() * 50)) {
                                    //if no offer was made
                                    boolean wasOffer = false;
                                    for (int j = 0; j < race.getOutgoingDiplomacyNews().size; j++) {
                                        DiplomacyInfo oldOffer = race.getOutgoingDiplomacyNews().get(j);
                                        if (oldOffer.flag == DiplomacyInfoType.DIPLOMACY_OFFER.getType()
                                                && oldOffer.toRace.equals(otherID)
                                                && oldOffer.fromRace.equals(race.getRaceId())) {
                                            wasOffer = true;
                                            break;
                                        }
                                    }
                                    if (wasOffer)
                                        continue;

                                    rtemp = rel;
                                    if (!likeBest.isEmpty())
                                        likeSecond = likeBest;
                                    likeBest = otherID;
                                }
                            }
                    }

                    String warPactPartner = "";
                    //decide between best and secondBest
                    if (!likeBest.isEmpty() && !likeSecond.isEmpty())
                        warPactPartner = (int) (RandUtil.random() * 3) > 0 ? likeBest : likeSecond;
                    else if (!likeBest.isEmpty())
                        warPactPartner = likeBest;

                    if (!warPactPartner.isEmpty()) {
                        //check to be sure that it still exists
                        Major partner = Major.toMajor(manager.getRaceController().getRace(warPactPartner));
                        if (partner != null && partner.getEmpire().countSystems() > 0) {
                            info.warpactEnemy = raceID;
                            //Now we change the raceID where the offer goes. We don't declare war directly but we send a Warpact offer to a partner
                            raceID = warPactPartner;
                            theirShipPower = manager.getStatistics().getShipPower(raceID);
                            agreement = ourRace.getAgreement(raceID);
                            offer = DiplomaticAgreement.WARPACT;
                        }
                    }
                }
            }

            //if until now there was no offer made, then check if it makes sense to ally against a powerful empire or to send a friendship or cooperation offer
            //this has the effect that weaker empires work together, and they won't make eachother weak. Especially in the late game if one race is more powerful than all other
            if (!isAlienDiplomacy && offer == DiplomaticAgreement.NONE
                    && agreement.getType() < DiplomaticAgreement.FRIENDSHIP.getType()
                    && ((int) (RandUtil.random() * 5) == 0)) {
                //calculate maximum ship power
                int maxShipPower = 0;
                ArrayMap<String, Major> majors = manager.getRaceController().getMajors();
                for (int i = 0; i < majors.size; i++) {
                    String majorId = majors.getKeyAt(i);
                    int shipPower = manager.getStatistics().getShipPower(majorId);
                    maxShipPower = Math.max(maxShipPower, shipPower);
                }

                //if our shippower is smaller than the 3rd of the maximum ship power and the maximum ship power is not that of the race that we want to make an offer to
                if (maxShipPower != theirShipPower)
                    if (ourShipPower < maxShipPower * 0.33f) {
                        if ((int) (RandUtil.random() * 2) == 0)
                            offer = DiplomaticAgreement.FRIENDSHIP;
                        else
                            offer = DiplomaticAgreement.COOPERATION;
                    }
            }

            info.flag = DiplomacyInfoType.DIPLOMACY_OFFER.getType();
            info.fromRace = ourRace.getRaceId();
            info.toRace = raceID;
            info.sendRound = manager.getCurrentRound() - 1;

            if (!isAlienDiplomacy) {
                //here we can give gifts with offers - credits we can always give, while resources only if we have at least a trade treaty
                //also we can make a present or a request
                if (offer != DiplomaticAgreement.NONE) {
                    //now we have to set the length of the treaty. the higher the treaty, the longer it should last - 0 is unlimited
                    if (offer != DiplomaticAgreement.WAR && offer != DiplomaticAgreement.WARPACT) {
                        int duration = (int) (RandUtil.random() * (11 + Math.abs(offer.getType())));
                        if (duration > 10)
                            duration = 0;
                        info.duration = duration * 10;
                    }

                    info.type = offer;
                    if (offer != DiplomaticAgreement.WAR)
                        giveDowry(info);

                } else if ((int) (RandUtil.random() * 30) == 0 && agreement != DiplomaticAgreement.WAR) { //if no offer was made then there is a 3.33% chance to a present
                    if (giveDowry(info))
                        info.type = DiplomaticAgreement.PRESENT;
                } else if (agreement != DiplomaticAgreement.WAR) {//if there was no offer and no present, maybe we have a request
                    //the modifier based on relative strengths of ships modifies the probability of the request
                    //the stronger our ships the higher the likelihood.
                    float modi = 0.0f;
                    if (theirShipPower != 0)
                        modi = (float) ourShipPower / (float) theirShipPower;

                    modi *= 4;
                    int a = (int) (RandUtil.random() * 100);
                    int b = (int) (RandUtil.random() * 100);
                    if (a < 2 + modi && b < 7 + modi) {
                        Gdx.app.debug("MajorAI", String.format(
                                "rand: %d - a = %d - b = %d - Modi = %f (we %s (%d) - enemy %s (%d)",
                                (int) (100 / modi), a, b, modi, ourRace.getRaceId(), ourShipPower, raceID,
                                theirShipPower));
                        if (claimRequest(info))
                            info.type = DiplomaticAgreement.REQUEST;
                    }
                }
            } else if (offer != DiplomaticAgreement.NONE) {
                //alien diplomacy can be only War, Friendship and NAP
                if (offer.getType() >= DiplomaticAgreement.FRIENDSHIP.getType()
                        && agreement.getType() < DiplomaticAgreement.FRIENDSHIP.getType())
                    offer = DiplomaticAgreement.FRIENDSHIP;
                else if (offer == DiplomaticAgreement.NAP && agreement.getType() < DiplomaticAgreement.NAP.getType())
                    offer = DiplomaticAgreement.NAP;
                else if (offer == DiplomaticAgreement.DEFENCEPACT)
                    offer = DiplomaticAgreement.NONE;
                else if (offer != DiplomaticAgreement.WAR)
                    offer = DiplomaticAgreement.NONE;
                info.type = offer;
            }
        }

        if (info.type != DiplomaticAgreement.NONE) {
            GenDiploMessage.generateMajorOffer(manager, info);
            Gdx.app.debug("MajorAI", String.format("Major: %s makes offer %s to Major %s, credits = %d, resources = %d %d %d %d %d %d", info.fromRace,
                    info.type.getdbName(), info.toRace, info.credits, info.resources[0], info.resources[1], info.resources[2], info.resources[3], info.resources[4], info.resources[5]));
            return true;
        } else
            return false;
    }

    /**
     * The function returns a value that must be met so that a race offers a diplomatic agreement
     * @param offerType
     * @return
     */
    private int getMinOfferValue(DiplomaticAgreement offerType) {
        int neededRelation = Integer.MAX_VALUE;
        switch (offerType) {
            case NAP:
                neededRelation = 50;
                if (race.isRaceProperty(RaceProperty.HOSTILE))
                    neededRelation += 50;
                if (race.isRaceProperty(RaceProperty.WARLIKE))
                    neededRelation += 20;
                if (race.isRaceProperty(RaceProperty.SECRET))
                    neededRelation += 10;
                if (race.isRaceProperty(RaceProperty.SCIENTIFIC))
                    neededRelation -= 10;
                if (race.isRaceProperty(RaceProperty.SOLOING))
                    neededRelation -= 15;
                if (race.isRaceProperty(RaceProperty.PACIFIST))
                    neededRelation -= 25;
                break;
            case TRADE:
                neededRelation = 70;
                if (race.isRaceProperty(RaceProperty.HOSTILE))
                    neededRelation += 40;
                if (race.isRaceProperty(RaceProperty.SOLOING))
                    neededRelation += 15;
                if (race.isRaceProperty(RaceProperty.SECRET))
                    neededRelation += 10;
                if (race.isRaceProperty(RaceProperty.PRODUCER))
                    neededRelation -= 10;
                if (race.isRaceProperty(RaceProperty.FINANCIAL))
                    neededRelation -= 20;
                break;
            case FRIENDSHIP:
                neededRelation = 80;
                if (race.isRaceProperty(RaceProperty.HOSTILE))
                    neededRelation += 30;
                if (race.isRaceProperty(RaceProperty.SOLOING))
                    neededRelation += 15;
                if (race.isRaceProperty(RaceProperty.WARLIKE))
                    neededRelation += 10;
                if (race.isRaceProperty(RaceProperty.SECRET))
                    neededRelation += 10;
                if (race.isRaceProperty(RaceProperty.FINANCIAL))
                    neededRelation -= 10;
                if (race.isRaceProperty(RaceProperty.SCIENTIFIC))
                    neededRelation -= 20;
                if (race.isRaceProperty(RaceProperty.PACIFIST))
                    neededRelation -= 30;
                break;
            case DEFENCEPACT:
                neededRelation = 100;
                if (race.isRaceProperty(RaceProperty.HOSTILE))
                    neededRelation += 20;
                if (race.isRaceProperty(RaceProperty.AGRARIAN))
                    neededRelation -= 10;
                if (race.isRaceProperty(RaceProperty.SCIENTIFIC))
                    neededRelation -= 15;
                if (race.isRaceProperty(RaceProperty.FINANCIAL))
                    neededRelation -= 20;
                if (race.isRaceProperty(RaceProperty.PACIFIST))
                    neededRelation -= 25;
                if (race.isRaceProperty(RaceProperty.SOLOING))
                    neededRelation -= 30;
                break;
            case COOPERATION:
                neededRelation = 115;
                if (race.isRaceProperty(RaceProperty.HOSTILE))
                    neededRelation += 30;
                if (race.isRaceProperty(RaceProperty.SOLOING))
                    neededRelation += 10;
                if (race.isRaceProperty(RaceProperty.SECRET))
                    neededRelation += 5;
                if (race.isRaceProperty(RaceProperty.SCIENTIFIC))
                    neededRelation -= 10;
                if (race.isRaceProperty(RaceProperty.FINANCIAL))
                    neededRelation -= 20;
                if (race.isRaceProperty(RaceProperty.PACIFIST))
                    neededRelation -= 25;
                break;
            case ALLIANCE:
                neededRelation = 145;
                if (race.isRaceProperty(RaceProperty.HOSTILE))
                    neededRelation += 15;
                if (race.isRaceProperty(RaceProperty.SOLOING))
                    neededRelation += 10;
                if (race.isRaceProperty(RaceProperty.SECRET))
                    neededRelation += 5;
                if (race.isRaceProperty(RaceProperty.PACIFIST))
                    neededRelation -= 5;
                if (race.isRaceProperty(RaceProperty.SCIENTIFIC))
                    neededRelation -= 15;
                if (race.isRaceProperty(RaceProperty.FINANCIAL))
                    neededRelation -= 25;
                break;
            default:
                break;
        }
        return neededRelation;
    }

    /**
     * The function calculates what extra the major gives with an offer
     * @param info
     * @return
     */
    private boolean giveDowry(DiplomacyInfo info) {
        boolean giveDowry = false;
        Major ourRace = Major.toMajor(race);
        if (ourRace == null)
            return false;

        Race toRace = manager.getRaceController().getRace(info.toRace);
        if (toRace == null)
            return false;

        //first the major gives only money, but only if it was 10% more than the global average
        int credits = 0;
        int counter = 0;
        ArrayMap<String, Major> majors = manager.getRaceController().getMajors();
        for (int i = 0; i < majors.size; i++) {
            Major m = majors.getValueAt(i);
            if (m != null && m.getEmpire().countSystems() > 0) {
                credits += m.getEmpire().getCredits();
                counter++;
            }
        }

        if (counter > 0)
            credits /= counter;

        if ((toRace.isMinor() && ourRace.getEmpire().getCredits() > 3000 && (int) (RandUtil.random() * 3) == 0)
                || (ourRace.getEmpire().getCredits() >= (int) (credits * 1.2f) && ourRace.getEmpire().getCredits() > 1000)
                || (ourRace.getEmpire().getCredits() > 25000 && (int) (RandUtil.random() * 2) > 0)) {
            credits = (int) (RandUtil.random() * (ourRace.getEmpire().getCredits() + 1));
            credits = (credits / 250) * 250; //round to 250
            if (credits > 5000)
                credits = 5000;
            if (credits > 0) {
                info.credits = credits;
                ourRace.getEmpire().setCredits(-credits);
                giveDowry = true;
            }
        }

        //resources can be given only if we have a trade agreement
        if (ourRace.getAgreement(info.toRace).getType() >= DiplomaticAgreement.TRADE.getType()) {
            //we give resources only if we have 10% more than the global average
            //first decide the resource, this is based on the average techlevel of the universe. in the beginning only Titanium will be gifted, later other resources can be added
            int whichRes = ResourceTypes.TITAN.getType();
            do {
                whichRes = (int) (RandUtil.random() * manager.getStatistics().getAverageTechLevel() + 1);
            } while (whichRes > ResourceTypes.DERITIUM.getType());

            //check if we have more than 20% of the resource as the average
            boolean canGive = false;
            if (ourRace.getEmpire().getStorage()[whichRes] >= (int) (manager.getStatistics()
                    .getAverageResourceStorages()[whichRes] * 1.2))
                canGive = true;

            if (canGive) {
                int mostRes = 0;
                IntPoint pt = new IntPoint();
                for (int i = 0; i < manager.getUniverseMap().getStarSystems().size; i++) {
                    StarSystem ss = manager.getUniverseMap().getStarSystems().get(i);
                    if (ss.getOwnerId().equals(ourRace.getRaceId())) {
                        mostRes = Math.max(ss.getResourceStore(whichRes), mostRes);
                        pt = ss.getCoordinates();
                    }
                }

                int giveRes = (int) (RandUtil.random() * (mostRes + 1));
                if (whichRes != ResourceTypes.DERITIUM.getType())
                    giveRes = (giveRes / 1000) * 1000;

                if (giveRes > 20000)
                    giveRes = 20000;
                if (giveRes > 0) {
                    info.resources[whichRes] = giveRes;
                    info.coord = pt;
                    manager.getUniverseMap().getStarSystemAt(pt).addResourceStore(whichRes, -giveRes);
                    giveDowry = true;
                }
            }
        }

        return giveDowry;
    }

    /**
     * The function calculates if and what type of request a major makes
     * @param info
     * @return
     */
    private boolean claimRequest(DiplomacyInfo info) {
        boolean claimRequest = false;
        Major ourRace = Major.toMajor(race);
        if (ourRace == null)
            return false;

        Race toRace = manager.getRaceController().getRace(info.toRace);
        if (toRace == null)
            return false;

        //we make a demand only if we have too few resources or credits - too few is based on global averages
        int credits = 0;
        int counter = 0;
        ArrayMap<String, Major> majors = manager.getRaceController().getMajors();
        for (int i = 0; i < majors.size; i++) {
            Major major = majors.getValueAt(i);
            if (major != null && major.getEmpire().countSystems() > 0) {
                credits += major.getEmpire().getCredits();
                counter++;
            }
        }
        if (counter > 0)
            credits /= counter;

        //we also compare ship strengths
        boolean stronger = false;
        int ourShipPower = manager.getStatistics().getShipPower(ourRace.getRaceId());
        int theirShipPower = manager.getStatistics().getShipPower(toRace.getRaceId());

        if (ourShipPower > 0) {
            //if our relationship is high enough, or we have a friendship treaty are the ship powers not compared
            if (ourRace.getRelation(info.toRace) < 75
                    || ourRace.getAgreement(info.toRace).getType() < DiplomaticAgreement.FRIENDSHIP.getType()) {
                float compare = (float) theirShipPower / (float) ourShipPower;
                if (compare < 0.6f && (int) (RandUtil.random() * 2) == 0)
                    stronger = true;
            }
        }
        //we make a request only if we have Random*30 percent of the value of the average credits
        if (stronger || ourRace.getEmpire().getCredits() <= (credits * (int) (RandUtil.random() * 30 + 1) / 100)) {
            //the number of credits depends on the techlevel
            credits = (int) (RandUtil.random() * (500 + manager.getStatistics().getAverageTechLevel() * 500));
            credits = (credits / 250) * 250;
            if (credits > 5000)
                credits = 5000;
            if (credits != 0) {
                info.credits = credits;
                claimRequest = true;
            }
        }

        //now we can request resources also, also together with credits and we don't even need a trade treaty for that
        //the resource is random depending on the techlevel
        boolean canGive = false;
        int whichRes = ResourceTypes.TITAN.getType();
        int count = 0;

        do {
            whichRes = (int) (RandUtil.random() * (manager.getStatistics().getAverageTechLevel() / 2 + 1));
            if (whichRes > ResourceTypes.DERITIUM.getType())
                whichRes = ResourceTypes.DERITIUM.getType();

            //check that we have less than Random*50% of the resource than the average
            if (stronger
                    || (ourRace.getEmpire().getStorage()[whichRes] <= (manager.getStatistics()
                            .getAverageResourceStorages()[whichRes] * ((int) (RandUtil.random() * 50 + 1)) / 100) && ourRace
                            .getEmpire().getStorage()[whichRes] != 0)) {
                canGive = true;
            }
            count++;
        } while (count < 20 && !canGive);

        //if we have too few, we make a request
        if (canGive) {
            int claimRes = 0;
            if (whichRes != ResourceTypes.DERITIUM.getType()) {
                claimRes = (int) (RandUtil.random() * (1000 + manager.getStatistics().getAverageTechLevel() * 1250));
                claimRes = (claimRes / 1000) * 1000;
            } else
                claimRes = (int) (RandUtil.random() * (10 + manager.getStatistics().getAverageTechLevel() * 5));

            if (claimRes > 20000)
                claimRes = 20000;

            if (claimRes > 0) {
                info.resources[whichRes] = claimRes;
                claimRequest = true;
            }
        }

        return claimRequest;
    }

    /**
     * The function calculates how a major reacts to an offer
     * @param info
     */
    @Override
    protected void reactOnDowry(DiplomacyInfo info) {
        Major ourRace = Major.toMajor(race);
        if (ourRace == null)
            return;

        int creditsFromRes = calcResInCredits(info);
        int credits = info.credits + creditsFromRes;
        if (credits == 0)
            return;

        //Here it's much simpler than with the minors. What matters is the ammount of credits we have and the race properties abit
        //It should be relatively simple so that the game isn't directed into a direction, every time it can be different

        //calculate the upper bound which results from the credits
        int randomCredits = 0;
        for (int i = 0; i < 3; i++)
            randomCredits += (int) (RandUtil.random() * credits * 2 + 1);
        randomCredits = randomCredits / 3;

        //the value which we need to reach with randomCredits to get a better relation
        int neededValue = GameConstants.DIPLOMACY_PRESENT_VALUE;
        if (ourRace.isRaceProperty(RaceProperty.SOLOING))
            neededValue += 150;
        if (ourRace.isRaceProperty(RaceProperty.HOSTILE))
            neededValue += 100;
        if (ourRace.isRaceProperty(RaceProperty.SECRET))
            neededValue += 75;
        if (ourRace.isRaceProperty(RaceProperty.WARLIKE))
            neededValue += 50;
        if (ourRace.isRaceProperty(RaceProperty.INDUSTRIAL))
            neededValue -= 25;
        if (ourRace.isRaceProperty(RaceProperty.FINANCIAL))
            neededValue -= 50;

        float bonus = 0.0f;
        if (randomCredits > neededValue)
            bonus = (float) randomCredits / (float) neededValue;

        float value = (info.credits / (float) (ourRace.getEmpire().getCredits() + 0.001));
        if (value > 1.0f)
            value = 1.0f;
        else if (value < 0.0f)
            value = 0.0f;

        bonus *= value;
        ourRace.setRelation(info.fromRace, (int) bonus);
    }

    /**
     * The function calculates how much the resources are worth in credits
     * @param info
     * @return
     */
    private int calcResInCredits(DiplomacyInfo info) {
        Major ourRace = Major.toMajor(race);
        if (ourRace == null)
            return 0;

        //first we have to calculated how much the resources are worth as credits
        //Titan is exchanged at a rate 5:1
        //Deuterium is exchanged at a rate 4.5:1
        //Duranium is exchanged at a rate 4:1
        //Crystal is exchanged at a rate 3.25:1
        //Iridium is exchanged at a rate 2.5:1
        //Deritium is exchanged at a rate 1:50

        float value = 0.0f;
        float div = 0.0f;

        if (info.resources[ResourceTypes.TITAN.getType()] != 0) {
            value = info.resources[ResourceTypes.TITAN.getType()] / 2.5f;
            div = info.resources[ResourceTypes.TITAN.getType()]
                    / (float) (ourRace.getEmpire().getStorage()[ResourceTypes.TITAN.getType()] + 0.00001);
        } else if (info.resources[ResourceTypes.DEUTERIUM.getType()] != 0) {
            value = info.resources[ResourceTypes.DEUTERIUM.getType()] / 2.75f;
            div = info.resources[ResourceTypes.DEUTERIUM.getType()]
                    / (float) (ourRace.getEmpire().getStorage()[ResourceTypes.DEUTERIUM.getType()] + 0.00001);
        } else if (info.resources[ResourceTypes.DURANIUM.getType()] != 0) {
            value = info.resources[ResourceTypes.DURANIUM.getType()] / 2f;
            div = info.resources[ResourceTypes.DURANIUM.getType()]
                    / (float) (ourRace.getEmpire().getStorage()[ResourceTypes.DURANIUM.getType()] + 0.00001);
        } else if (info.resources[ResourceTypes.CRYSTAL.getType()] != 0) {
            value = info.resources[ResourceTypes.CRYSTAL.getType()] / 1.625f;
            div = info.resources[ResourceTypes.CRYSTAL.getType()]
                    / (float) (ourRace.getEmpire().getStorage()[ResourceTypes.CRYSTAL.getType()] + 0.00001);
        } else if (info.resources[ResourceTypes.IRIDIUM.getType()] != 0) {
            value = info.resources[ResourceTypes.IRIDIUM.getType()] / 1.25f;
            div = info.resources[ResourceTypes.IRIDIUM.getType()]
                    / (float) (ourRace.getEmpire().getStorage()[ResourceTypes.IRIDIUM.getType()] + 0.00001);
        } else if (info.resources[ResourceTypes.DERITIUM.getType()] != 0) {
            value = info.resources[ResourceTypes.DERITIUM.getType()] * 100;
            div = info.resources[ResourceTypes.DERITIUM.getType()]
                    / (float) (ourRace.getEmpire().getStorage()[ResourceTypes.DERITIUM.getType()] + 0.00001);
        }

        if (div > 1.0f)
            div = 1.0f;
        else if (div < 0.0f)
            div = 0.0f;

        value *= div;

        //now we take other factors into account... the more of a resource a race has, the less it appreciates the same resource
        if (value != 0.0f) {
            //take race properties into account
            if (ourRace.isRaceProperty(RaceProperty.PRODUCER))
                value *= 1.5f;
            if (ourRace.isRaceProperty(RaceProperty.INDUSTRIAL))
                value *= 1.35f;
            if (ourRace.isRaceProperty(RaceProperty.WARLIKE))
                value *= 1.2f;
            if (ourRace.isRaceProperty(RaceProperty.FINANCIAL))
                value *= 1.1f;
            if (ourRace.isRaceProperty(RaceProperty.SECRET))
                value *= 0.9f;
            if (ourRace.isRaceProperty(RaceProperty.AGRARIAN))
                value *= 0.8f;

            value = Math.max(0.0f, value);
        }
        return (int) value;
    }
}
