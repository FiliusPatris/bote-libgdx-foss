/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ai;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.ObjectIntMap;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.RaceProperty;
import com.blotunga.bote.constants.ShipRange;
import com.blotunga.bote.constants.ShipType;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.races.Research;
import com.blotunga.bote.ships.ShipInfo;
import com.blotunga.bote.utils.RandUtil;

public class AIPrios {
    private ObjectIntMap<String> coloShipPrio;	//stores the priority of a race to build colonyships
    private ObjectIntMap<String> transportPrio;	//stores the priority of a race to build troop transports
    private ObjectIntMap<String> combatShipPrio;  //stores the priority of a race to build combat ships
    private ObjectIntMap<String> troopPrio;		//stores the priority of a race to build troops
    private IntelAI intelAI;
    private ResourceManager manager;

    public AIPrios(ResourceManager manager) {
        this.manager = manager;
        coloShipPrio = new ObjectIntMap<String>();
        transportPrio = new ObjectIntMap<String>();
        combatShipPrio = new ObjectIntMap<String>();
        troopPrio = new ObjectIntMap<String>();
        intelAI = new IntelAI();
        clear();
    }

    public void clear() {
        coloShipPrio.clear();
        transportPrio.clear();
        combatShipPrio.clear();
        troopPrio.clear();
        intelAI.reset();
    }

    /**
     * @param raceID
     * @return - the priority of building a colony ship
     */
    public int getColoShipPrio(String raceID) {
        return coloShipPrio.get(raceID, 0);
    }

    /**
     * @param raceID
     * @return - the priority of building a transport ship
     */
    public int getTransportShipPrio(String raceID) {
        return transportPrio.get(raceID, 0);
    }

    /**
     * @param raceID
     * @return - the priority of building a combat ship
     */
    public int getCombatShipPrio(String raceID) {
        return combatShipPrio.get(raceID, 0);
    }

    /**
     * @param raceID
     * @return - the priority of building a troop
     */
    public int getTroopPrio(String raceID) {
        return troopPrio.get(raceID, 0);
    }

    public IntelAI getIntelAI() {
        return intelAI;
    }

    /**
     * The function should be called if this priority was chosen. It also gets smaller
     * @param raceID
     */
    public void choseColoShipPrio(String raceID) {
        int value = coloShipPrio.get(raceID, 0);
        coloShipPrio.put(raceID, value / 2);
    }

    /**
     * The function should be called if this priority was chosen. It also gets smaller
     * @param raceID
     */
    public void choseTransportShipPrio(String raceID) {
        int value = transportPrio.get(raceID, 0);
        transportPrio.put(raceID, value / 2);
    }

    /**
     * The function should be called if this priority was chosen. It also gets smaller
     * @param raceID
     */
    public void choseCombatShipPrio(String raceID) {
        int value = combatShipPrio.get(raceID, 0);
        combatShipPrio.put(raceID, value / 2);
    }

    /**
     * The function should be called if this priority was chosen. It also gets smaller
     * @param raceID
     */
    public void choseTroopPrio(String raceID) {
        int value = troopPrio.get(raceID, 0);
        troopPrio.put(raceID, value / 2);
    }

    /**
     * This function calculates the priorities of the races to know when to built what type of ship
     * @param sectorAI
     */
    public void calcShipAndTroopPrios(SectorAI sectorAI) {
        clear();
        int max = 0;
        ObjectIntMap<String> shipPower = new ObjectIntMap<String>();
        ArrayMap<String, Major> majors = manager.getRaceController().getMajors();
        for (int i = 0; i < majors.size; i++) {
            String majorID = majors.getKeyAt(i);
            Major major = majors.getValueAt(i);
            Research research = major.getEmpire().getResearch();
            int[] researchLevels = research.getResearchLevels();

            ShipRange range = ShipRange.SHORT;
            //calculate what is the max range of the colony ship of the race
            for (int j = 0; j < manager.getShipInfos().size; j++) {
                ShipInfo si = manager.getShipInfos().get(j);
                if (si.getRace() == major.getRaceShipNumber())
                    if (si.getShipType() == ShipType.COLONYSHIP && si.getRange().getRange() > range.getRange()
                            && si.isThisShipBuildableNow(researchLevels)) {
                        range = si.getRange();
                        if (range == ShipRange.LONG)
                            break;
                    }
            }
            //Now go through the sectors which should be terraformed and check if they are in range
            int value = 0;
            for (int j = 0; j < sectorAI.getSectorsToTerraform(majorID).size; j++)
                if (major.getStarMap().getRange(sectorAI.getSectorsToTerraform(majorID).get(j).p) <= range.getRange()) {
                    value = coloShipPrio.get(majorID, 0) + 1;
                    coloShipPrio.put(majorID, value);
                }
            //In case of 0 or 1 colony ships, the priority remains the same as the number of sectors that can be terraformed
            //If there are 2 ships, the maximum priority 7, for 3 - 8, for 4 - 9, 5 - 10
            //if there are more than 5 ships it will be halved
            //if the priority + 2 is smaller than the number of colony ships, set it to 0
            if ((getColoShipPrio(majorID) + 1) < sectorAI.getNumberOfColoships(majorID)) {
                value = 0;
                coloShipPrio.put(majorID, value);
            } else if (sectorAI.getNumberOfColoships(majorID) > 1 && sectorAI.getNumberOfColoships(majorID) < 6
                    && getColoShipPrio(majorID) > (sectorAI.getNumberOfColoships(majorID) + 5)) {
                value = sectorAI.getNumberOfColoships(majorID) + 5;
                coloShipPrio.put(majorID, value);
            } else if (sectorAI.getNumberOfColoships(majorID) > 12) {
                value = (int) (RandUtil.random() * 2);
                coloShipPrio.put(majorID, value);
            } else if (sectorAI.getNumberOfColoships(majorID) > 5) {
                value = getColoShipPrio(majorID) / 4;
                coloShipPrio.put(majorID, value);
            }

            //now transport ships
            if (sectorAI.getStationBuildSector(majorID) != null
                    && sectorAI.getStationBuildSector(majorID).points > GameConstants.MINBASEPOINTS) {
                boolean buildableStation = false;
                for (int j = 0; j < manager.getShipInfos().size; j++) {
                    ShipInfo si = manager.getShipInfos().get(j);
                    if (si.getRace() == major.getRaceShipNumber())
                        if (si.isStation() && si.isThisShipBuildableNow(researchLevels)) {
                            buildableStation = true;
                            break;
                        }
                }
                if (buildableStation) {
                    if (sectorAI.getNumberOfTransportShips(majorID) == 0)
                        value = (int) (sectorAI.getStationBuildSector(majorID).points / (GameConstants.MINBASEPOINTS * 0.05));
                    else
                        value = (int) (sectorAI.getStationBuildSector(majorID).points / (GameConstants.MINBASEPOINTS * 0.15));
                    transportPrio.put(majorID, value);

                    if (getTransportShipPrio(majorID) < sectorAI.getNumberOfTransportShips(majorID) * 4)
                        transportPrio.put(majorID, 0);

                    //if the colony ship priority is low (for example if there are too few planets to terraform), the priority will be increased.
                    //if the colony ship priority is high, the transport ship priority will be lowered
                    if (getColoShipPrio(majorID) == 0 && sectorAI.getNumberOfTransportShips(majorID) == 0) {
                        value = getTransportShipPrio(majorID) * 7;
                        transportPrio.put(majorID, value);
                    } else if (getColoShipPrio(majorID) < 2) {
                        value = getTransportShipPrio(majorID) * 2;
                        transportPrio.put(majorID, value);
                    } else if (getColoShipPrio(majorID) > 5) {
                        value /= 3;
                        transportPrio.put(majorID, value);
                    }
                }
            }
            //more transport ships for troops - but not too many
            value = major.countTroops() * 2; //have transports ready for troops
            value += getTransportShipPrio(majorID);
            if (getTransportShipPrio(majorID) < sectorAI.getNumberOfTransportShips(majorID) * 4)
                transportPrio.put(majorID, 0);
            else
                transportPrio.put(majorID, value);

            value = GameConstants.MIN_TROOPS_FOR_INVASION;
            if (major.isRaceProperty(RaceProperty.HOSTILE))
                value += 1;
            else if (major.isRaceProperty(RaceProperty.WARLIKE))
                value += 1;
            troopPrio.put(majorID, Math.max(0, value * 4 - major.countTroops()));

            shipPower.put(majorID, sectorAI.getCompleteDanger(majorID));
            if (max < shipPower.get(majorID, 0))
                max = shipPower.get(majorID, 0);
        }

        //		System.out.println("Max combatship power = " + max);
        for (int i = 0; i < majors.size; i++) {
            String majorID = majors.getKeyAt(i);
            int value = 255;
            int power = shipPower.get(majorID, 0);
            if (power > 0)
                value = Math.min(255, (int) ((float) max / (float) power * 1.5f));
            combatShipPrio.put(majorID, value);
            Gdx.app.debug("AIPrios", "Calc priorities: " + majorID + " combatship: " + getCombatShipPrio(majorID)
                    + " transport: " + getTransportShipPrio(majorID) + " colony: " + getColoShipPrio(majorID)
                    + " troop: " + getTroopPrio(majorID));
            //			System.out.println("Number of ships: " + majorID + " colony: " + sectorAI.getNumberOfColoships(majorID) + " transport: " + sectorAI.getNumberOfTransportShips(majorID));
        }
    }
}
