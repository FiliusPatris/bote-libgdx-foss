/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ai;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.ObjectIntMap;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.constants.Aliens;
import com.blotunga.bote.constants.CombatTactics;
import com.blotunga.bote.constants.DiplomaticAgreement;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.RaceProperty;
import com.blotunga.bote.constants.ShipOrder;
import com.blotunga.bote.constants.ShipType;
import com.blotunga.bote.galaxy.Planet;
import com.blotunga.bote.galaxy.Sector;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.races.MoraleObserver;
import com.blotunga.bote.races.Race;
import com.blotunga.bote.races.RaceController;
import com.blotunga.bote.ships.Ships;
import com.blotunga.bote.starsystem.StarSystem;
import com.blotunga.bote.utils.IntPoint;
import com.blotunga.bote.utils.RandUtil;

public class ShipAI {
    private ResourceManager manager;
    private SectorAI sectorAI;
    private ArrayMap<String, IntPoint> attackSector;
    private ArrayMap<String, IntPoint> bombardSector;
    private ObjectIntMap<String> averageMorale;

    public ShipAI(ResourceManager manager) {
        this.manager = manager;
        RaceController raceCtrl = manager.getRaceController();
        attackSector = new ArrayMap<String, IntPoint>();
        bombardSector = new ArrayMap<String, IntPoint>();
        averageMorale = new ObjectIntMap<String>();

        ObjectIntMap<String> moraleAll = new ObjectIntMap<String>();
        ObjectIntMap<String> systems = new ObjectIntMap<String>();
        for (int i = 0; i < manager.getUniverseMap().getStarSystems().size; i++) {
            StarSystem ss = manager.getUniverseMap().getStarSystems().get(i);
            if (ss.isMajorized()) {
                int origValue = moraleAll.get(ss.getOwnerId(), 0);
                moraleAll.put(ss.getOwnerId(), origValue + ss.getMorale());
                int sysCount = systems.get(ss.getOwnerId(), 0);
                systems.put(ss.getOwnerId(), sysCount + 1);
            }
        }

        for (int i = 0; i < raceCtrl.getRaces().size; i++) {
            String raceID = raceCtrl.getRaces().getKeyAt(i);
            attackSector.put(raceID, new IntPoint());
            bombardSector.put(raceID, new IntPoint());

            if (systems.get(raceID, 0) > 0)
                averageMorale.put(raceID, moraleAll.get(raceID, 0) / systems.get(raceID, 0));
            else
                averageMorale.put(raceID, 0);
        }
    }

    /**
     * Gives orders to all AI controlled ships
     * @param sectorAI
     */
    public void calculateShipOrders(SectorAI sectorAI) {
        this.sectorAI = sectorAI;

        //calculate a possible attack sector
        calcAttackSector();
        //calculate a possible bombard sector
        calcBombardSector();

        for (int i = 0; i < manager.getUniverseMap().getShipMap().getSize(); i++) {
            Ships ship = manager.getUniverseMap().getShipMap().getAt(i);
            if (ship.isAlien()) {
                calculateAlienShipOrders(ship);
                continue;
            }

            String ownerID = ship.getOwnerId();
            Major owner = Major.toMajor(manager.getRaceController().getRace(ownerID));
            if (owner == null || !owner.isMajor())
                continue;

            if (owner.aHumanPlays())
                continue;

            //try to create a fleet
            doMakeFleet(i);

            //if we have a target which must be attacked
            if (doAttackMove(ship, owner)) {
                doCamouflage(ship);
                continue;
            }

            if (ship.isStation()) //stations can't move so ignore them from here
                continue;

            //if colony ships have a terraforming target which recently was colonized by someone else, then the target will be removed
            if (ship.getShipType() == ShipType.COLONYSHIP) {
                IntPoint coord = ship.getCoordinates();
                //if a ship has a terraform order and someone else already owns the system, cancel the order
                if (ship.getCurrentOrder() == ShipOrder.TERRAFORM) {
                    StarSystem ss = manager.getUniverseMap().getStarSystemAt(coord);
                    if (!ss.isFree() && !ss.getOwnerId().equals(ownerID)) {
                        ship.unsetCurrentOrder();
                        ship.setCombatTactics(CombatTactics.CT_AVOID);
                    }
                }

                IntPoint target = ship.getTargetCoord();
                if (!target.equals(new IntPoint()) && !manager.getUniverseMap().getStarSystemAt(target).isFree()
                        && !manager.getUniverseMap().getStarSystemAt(target).getOwnerId().equals(ownerID)) {
                    ship.setTargetCoord(new IntPoint());
                    ship.getPath().clear();
                }
            }

            //if the ship has no path, then here we try to set one
            if (ship.getPath().size == 0) {
                if (ship.getShipType().getType() > ShipType.COLONYSHIP.getType()) {
                    Array<IntPoint> minorRaceSectors = sectorAI.getMinorRaceSectors(ownerID);

                    boolean set = false;
                    int count = minorRaceSectors.size * 2;
                    while ((minorRaceSectors.size != 0) && (count-- != 0)) {
                        int j = (int) (RandUtil.random() * minorRaceSectors.size);
                        IntPoint coord = minorRaceSectors.get(j);
                        //if the danger of others is smaller than mine
                        if (sectorAI.getCompleteDanger(ownerID, coord) == 0
                                || (sectorAI.getCompleteDanger(ownerID, coord) <= sectorAI
                                        .getDangerOnlyFromCombatShips(ownerID, ship.getCoordinates())))
                            if (owner.getStarMap().getRange(coord) <= ship.getRange(false).getRange()) {
                                ship.setTargetCoord(coord);
                                Gdx.app.debug(
                                        "ShipAI",
                                        String.format("Race %s: Ship to Minor: %s (%s) - Target: %d,%d", ownerID,
                                                ship.getName(), ship.getShipTypeAsString(), coord.x, coord.y));
                                minorRaceSectors.removeIndex(j--);
                                set = true;
                                break;
                            }
                    }

                    if (set) {
                        doCamouflage(ship);
                        continue;
                    }
                }

                //Send colony ships to terraform. Other ships also fly there if they have no other orders
                if (ship.getShipType().getType() >= ShipType.COLONYSHIP.getType()
                        && ship.getCurrentOrder() != ShipOrder.TERRAFORM) {
                    Array<SectorAI.SectorToTerraform> sectorsToTerraform = sectorAI.getSectorsToTerraform(ownerID);
                    for (int j = 0; j < sectorsToTerraform.size; j++) {
                        IntPoint coord = sectorsToTerraform.get(j).p;
                        //if the colonyship is already in place, it doesn't has to move further
                        if (ship.getShipType() == ShipType.COLONYSHIP && ship.getCoordinates().equals(coord))
                            break;

                        if (sectorAI.getCompleteDanger(ownerID, coord) == 0
                                || (sectorAI.getCompleteDanger(ownerID, coord) < sectorAI.getDanger(ownerID,
                                        ship.getCoordinates()))) {
                            if (owner.getStarMap().getRange(coord) <= ship.getRange(false).getRange()) {
                                //set the target for the ship
                                ship.setTargetCoord(coord.equals(ship.getCoordinates()) ? new IntPoint() : coord);
                                Gdx.app.debug("ShipAI", String.format(
                                        "Race %s: Ship: %s (%s) has terraforming target: %d,%d", ownerID,
                                        ship.getName(), ship.getShipTypeAsString(), coord.x, coord.y));
                                break;
                            }
                        }
                    }
                }
                //Transporters should fly to sectors where they build an outpost
                if (sectorAI.getStationBuildSector(ownerID) != null
                        && sectorAI.getStationBuildSector(ownerID).points > GameConstants.MINBASEPOINTS
                        && ship.getCurrentOrder() != ShipOrder.BUILD_OUTPOST) {
                    //only transporters and other ships without a target fly here, but no colony ships!
                    if (ship.getShipType() == ShipType.TRANSPORTER
                            || (ship.getShipType() != ShipType.COLONYSHIP && !ship.hasTarget())) {
                        IntPoint coord = new IntPoint(sectorAI.getStationBuildSector(ownerID).position);
                        //if the danger from others is smaller than mine
                        if (sectorAI.getCompleteDanger(ownerID, coord) == 0
                                || (sectorAI.getCompleteDanger(ownerID, coord) < sectorAI.getDanger(ownerID,
                                        ship.getCoordinates()))) {
                            if (owner.getStarMap().getRange(coord) <= ship.getRange(false).getRange()) {
                                ship.setTargetCoord(coord.equals(ship.getCoordinates()) ? new IntPoint() : coord);
                                Gdx.app.debug("ShipAI", String.format(
                                        "Race %s: Ship: %s (%s) has stationbuild target: %d,%d", ownerID,
                                        ship.getName(), ship.getShipTypeAsString(), coord.x, coord.y));
                            }
                        }
                    }
                }

                doCamouflage(ship);
                if (manager.getUniverseMap().getStarSystemAt(ship.getCoordinates()).isSunSystem()) {
                    if (!doTerraform(ship))
                        doColonize(ship);
                }

                doStationBuild(ship);
            } else {
                doCamouflage(ship);
            }
        }
    }

    /**
     * The function tries to build an outpost
     * @param ship
     * @return
     */
    private boolean doStationBuild(Ships ship) {
        if (ship == null)
            return false;

        if (ship.getStationBuildPoints() <= 0 || ship.getCurrentOrder() == ShipOrder.BUILD_OUTPOST)
            return false;

        String raceID = ship.getOwnerId();
        if (sectorAI.getStationBuildSector(raceID) == null
                || sectorAI.getStationBuildSector(raceID).points <= GameConstants.MINBASEPOINTS)
            return false;

        IntPoint coord = new IntPoint(sectorAI.getStationBuildSector(raceID).position);
        if (!ship.getCoordinates().equals(coord))
            return false;

        if (manager.getUniverseMap().getStarSystemAt(coord).isFree()
                || manager.getUniverseMap().getStarSystemAt(coord).getOwnerId().equals(raceID)) {
            ship.setTargetCoord(new IntPoint());
            ship.setCurrentOrder(ShipOrder.BUILD_OUTPOST);
            Gdx.app.debug("ShipAI", "Building station in " + ship.getCoordinates());
            return true;
        }
        return false;
    }

    /**
     * The function sends attack ships to a target. Under some circumstances the order to attack is also set. In other cases it groups ships together
     * @param ship
     * @param major
     * @return
     */
    private boolean doAttackMove(Ships ship, Major major) {
        if (ship == null)
            return false;

        if (major == null)
            return false;

        if (ship.getShipType().getType() == ShipType.COLONYSHIP.getType()
                || ship.getShipType().getType() >= ShipType.OUTPOST.getType())
            return false;

        if (doBombardSystem(ship))
            return true;

        String raceID = major.getRaceId();
        int ourDanger = sectorAI.getDangerOnlyFromCombatShips(raceID, ship.getCoordinates());

        //if there is a bombard target, it will have the highest priority
        if (!bombardSector.get(raceID).equals(new IntPoint())) {
            //check that we have supremacy in the bombard sector
            if (ourDanger > sectorAI.getCompleteDanger(raceID, bombardSector.get(raceID))) {
                if (major.getStarMap().getRange(bombardSector.get(raceID)) <= ship.getRange(false).getRange()) {
                    if (ship.getShipType() != ShipType.TRANSPORTER
                            || (ship.getShipType() == ShipType.TRANSPORTER && ship.hasNothingToDo()
                                    && (((int) (RandUtil.random() * 2)) == 0) && (major.countTroops() > 0))) {
                        ship.setTargetCoord(bombardSector.get(raceID).equals(ship.getCoordinates()) ? new IntPoint()
                                : bombardSector.get(raceID));
                        Gdx.app.debug(
                                "ShipAI",
                                String.format("Race %s: BOMBARDTARGET in sector: %d,%d", raceID,
                                        bombardSector.get(raceID).x, bombardSector.get(raceID).y));
                        if (ship.getShipType() == ShipType.TRANSPORTER) {
                            fillTransportsWithTroops(ship);
                            for (int j = 0; j < ship.getFleetSize(); j++) {
                                Ships t = ship.getFleet().getAt(j);
                                fillTransportsWithTroops(t);
                            }
                            Gdx.app.debug("ShipAI", "Moving transporter into position");
                        }
                        return true;
                    }
                }
            }
        }

        if (ship.getShipType() == ShipType.TRANSPORTER) //there is no bombard target so transporters have nothing else to do here
            return false;

        //if there is no target in the neighborhood, or it wasn't good for attacking then if possible send the ships to the point which is selected as attacking target
        if (!attackSector.get(raceID).equals(new IntPoint())) {
            int enemyDander = sectorAI.getCompleteDanger(raceID, attackSector.get(raceID));
            boolean attack = ourDanger > enemyDander;
            //check if we have 75-90% of our fleet in position
            if (!attack) {
                if (ourDanger > enemyDander / 2)
                    attack = ourDanger > (sectorAI.getCompleteDanger(raceID) * (int) (RandUtil.random() * 16 + 75) / 100);
            }

            if (attack) {
                if (major.getStarMap().getRange(attackSector.get(raceID)) <= ship.getRange(false).getRange()) {
                    ship.setTargetCoord(attackSector.get(raceID).equals(ship.getCoordinates()) ? new IntPoint()
                            : attackSector.get(raceID));
                    Gdx.app.debug("ShipAI", String.format("Race %s: GLOBAL ATTACK in sector: %d,%d", raceID,
                            attackSector.get(raceID).x, attackSector.get(raceID).y));
                    return true;
                }
            } else if (!sectorAI.getHighestShipDanger(raceID).equals(new IntPoint())) {
                IntPoint p = sectorAI.getHighestShipDanger(raceID);
                if (major.getStarMap().getRange(p) <= ship.getRange(false).getRange()) {
                    ship.setTargetCoord(p.equals(ship.getCoordinates()) ? new IntPoint() : p);
                    Gdx.app.debug("ShipAI", String.format("Race %s: COLLECT ships in sector: %d,%d", raceID, p.x, p.y));
                    return true;
                }
            }
        }
        return false;
    }

    private boolean doBombardSystem(Ships ship) {
        if (ship.getCoordinates().equals(bombardSector.get(ship.getOwnerId()))) {
            ship.setTargetCoord(new IntPoint());
            if (ship.getShipType() == ShipType.TRANSPORTER) {
                    fillTransportsWithTroops(ship);
                    if (ship.getTroops().size == 0)
                        return false;
            }

            //if it's cloaked then we have to decloak first
            if (!ship.canHaveOrder(ShipOrder.ATTACK_SYSTEM, false)) {
                ship.setCurrentOrder(ShipOrder.DECLOAK);
                return true;
            }

            //if the defensive strength of the ships of the attacker in the system is higher than the system defending value of the target then we bombard
            int shipDefend = manager.getUniverseMap().getStarSystemAt(ship.getCoordinates()).getProduction()
                    .getShipDefend();

            int shipValue = 0;
            for (int i = 0; i < manager.getUniverseMap().getShipMap().getSize(); i++) {
                Ships s = manager.getUniverseMap().getShipMap().getAt(i);
                if (!s.getOwnerId().equals(ship.getOwnerId()))
                    continue;

                if (!s.getCoordinates().equals(ship.getCoordinates()))
                    continue;

                shipValue += s.getHull().getCurrentHull();
                for (int j = 0; j < s.getFleetSize(); j++) {
                    Ships t = s.getFleet().getAt(j);
                    shipValue += t.getHull().getCurrentHull();
                }

                if (shipValue > shipDefend)
                    break;
            }

            if (shipValue > shipDefend) {
                Gdx.app.debug(
                        "ShipAI",
                        String.format("Race %s: Ship %s (%s) is bombarding system: %d,%d", ship.getOwnerId(),
                                ship.getName(), ship.getShipTypeAsString(), ship.getCoordinates().x,
                                ship.getCoordinates().y));
                ship.setCurrentOrder(ShipOrder.ATTACK_SYSTEM);
                return true;
            }
        }
        return false;
    }

    private void fillTransportsWithTroops(Ships ship) {
        if (ship.getStorageRoom() == 0)
            return;
        //magically fill up transports with troops here!
        Major owner = Major.toMajor(ship.getOwner());
        if (owner == null)
            return;

        if (ship.getStorageRoom() != 0)
            Gdx.app.debug("ShipAI",
                    "Filling transport in " + ship.getCoordinates() + " with target " + ship.getTargetCoord());
        Array<IntPoint> systems = owner.getEmpire().getSystemList();
        for (int i = 0; i < systems.size; i++) {
            StarSystem ss = manager.getUniverseMap().getStarSystemAt(systems.get(i));
            if (ss.getTroops().size > 0)
                for (int j = 0; j < ss.getTroops().size; j++)
                    if ((ship.getStorageRoom() - ship.getUsedStorageRoom(manager.getTroopInfos())) >= manager
                            .getTroopInfos().get(ss.getTroops().get(j).getID()).getSize()) {
                        ship.getTroops().add(ss.getTroops().get(j));
                        ss.getTroops().removeIndex(j--);
                    }
        }
    }

    /**
     * Gives ship orders to alien ships
     * @param ship
     */
    private void calculateAlienShipOrders(Ships ship) {
        if (ship.getOwnerId().equals(Aliens.MIDWAY_ZEITREISENDE.getId())) {
            IntPoint coord = ship.getCoordinates();
            StarSystem system = manager.getUniverseMap().getStarSystemAt(coord);
            if (system.isSunSystem()) {
                Race owner = system.getOwner();
                if (owner != null && owner.isMajor()
                        && owner.getAgreement(Aliens.MIDWAY_ZEITREISENDE.getId()) == DiplomaticAgreement.WAR) {
                    //because the ship shouldn't bombard the system forever, here a bit of random
                    if (ship.getCurrentOrder() != ShipOrder.ATTACK_SYSTEM || (int) (RandUtil.random() * 100) > 20) {
                        ship.setTargetCoord(new IntPoint());
                        ship.setCurrentOrder(ShipOrder.ATTACK_SYSTEM);
                        return;
                    }
                }
            }
        }

        ship.unsetCurrentOrder();
    }

    /**
     * The ship sets a terraform command if it's possible
     * @param ship
     * @return
     */
    private boolean doTerraform(Ships ship) {
        if (ship == null)
            return false;

        //All planets are consider if it takes less than 8 turns to terraform them. Other planets are only terraformed if there are no other planets for colonizing present in the sector
        int terraPoints = ship.getColonizePoints();
        if (terraPoints <= 0)
            return false;

        Sector sector = manager.getUniverseMap().getStarSystemAt(ship.getCoordinates());
        if (!sector.isFree() && !sector.getOwnerId().equals(ship.getOwnerId()))
            return false;

        int minTerraPoints = Integer.MAX_VALUE;
        int planet = -1;

        boolean colonizable = false;
        for (int j = 0; j < sector.getNumberOfPlanets(); j++) {
            Planet p = sector.getPlanet(j);
            if (!p.getIsHabitable())
                continue;

            if (p.getIsTerraformed()) {
                if (!p.getIsInhabited())
                    colonizable = true;
            } else if (p.getNeededTerraformPoints() < minTerraPoints) {
                minTerraPoints = p.getNeededTerraformPoints();
                planet = j;
            }
        }

        //if a suitable terraforming target was found and it would take less than 8 turns to terraform, or there are no planets to colonize, then terraform the found planet
        if (planet != -1 && (!colonizable || minTerraPoints / terraPoints < 8)) {
            ship.setTargetCoord(new IntPoint());
            ship.setTerraform(planet);
            return true;
        }
        return false;
    }

    /**
     * The function tries to colonize the current system of the ship
     * @param ship
     * @return
     */
    private boolean doColonize(Ships ship) {
        if (ship == null)
            return false;

        if (ship.getShipType() != ShipType.COLONYSHIP)
            return false;

        Sector sector = manager.getUniverseMap().getStarSystemAt(ship.getCoordinates());
        if (!sector.isFree() && !sector.getOwnerId().equals(ship.getOwnerId()))
            return false;

        for (int i = 0; i < sector.getNumberOfPlanets(); i++) {
            if (sector.getPlanet(i).isColonizable()) {
                ship.setTargetCoord(new IntPoint());
                ship.setCurrentOrder(ShipOrder.COLONIZE);
                return true;
            }
        }
        return false;
    }

    private boolean doCamouflage(Ships ship) {
        return doCamouflage(ship, true);
    }

    /**
     * This function cloaks/decloaks AI ship
     * @param ship
     * @param camouflage
     * @return
     */
    private boolean doCamouflage(Ships ship, boolean camouflage) {
        if (ship == null)
            return false;

        if (camouflage == ship.isCloakOn())
            return false;

        if (ship.canCloak(false) && ship.getCurrentOrder() != ShipOrder.ATTACK_SYSTEM) {
            ship.setCurrentOrder(camouflage ? ShipOrder.ENCLOAK : ShipOrder.DECLOAK);
            return true;
        }
        return false;
    }

    /**
     * The function creates a fleet - ships are added only if the correspond to some criteria
     * @param index
     */
    private void doMakeFleet(int index) {
        Ships s = manager.getUniverseMap().getShipMap().getAt(index);
        if (s.isStation())
            return;

        boolean increment = true;
        int i = index;
        for (;;) {
            if (increment)
                ++i;
            increment = true;
            if (i == manager.getUniverseMap().getShipMap().getSize())
                break;

            Ships ship = manager.getUniverseMap().getShipMap().getAt(i);
            if (!ship.getOwnerId().equals(s.getOwnerId()))
                continue;

            if (!ship.getCoordinates().equals(s.getCoordinates()))
                continue;

            if (ship.getRange(false) != s.getRange(false))
                continue;

            if (ship.isStation())
                continue;

            //the ship shouldn't have a target calculated
            if (ship.getPath().size > 0)
                continue;

            //cloaked ships shouldn't mix with uncloaked ones
            if (ship.isCloakOn() != s.isCloakOn())
                continue;

            //if the leader can cloak, also should the fleet
            if (ship.canCloak(false) != s.canCloak(false))
                continue;

            //the ships must be either combat ships or only transports or only colony ships
            if ((!ship.isNonCombat() && !s.isNonCombat())
                    || (ship.getShipType() == ShipType.TRANSPORTER && s.getShipType() == ShipType.TRANSPORTER && ship
                            .getCurrentOrder().getType() < ShipOrder.BUILD_OUTPOST.getType())
                    || (ship.getShipType() == ShipType.COLONYSHIP && s.getShipType() == ShipType.COLONYSHIP && ship
                            .getCurrentOrder().getType() < ShipOrder.COLONIZE.getType())) {
                s.addShipToFleet(ship);
                manager.getUniverseMap().getShipMap().eraseAt(i);
                increment = false;
            }
        }
    }

    /**
     * The function calculates a possible attack sector which can be attacked by a group of ships later
     */
    private void calcAttackSector() {
        ArrayMap<String, Major> majors = manager.getRaceController().getMajors();
        for (int i = 0; i < majors.size; i++) {
            String raceID = majors.getKeyAt(i);
            int nearestSector = Integer.MAX_VALUE;
            IntPoint sector = new IntPoint();
            for (int j = 0; j < sectorAI.getOffensiveTargets(raceID).size; j++) {
                int space = Math.max(
                        Math.abs(sectorAI.getHighestShipDanger(raceID).x
                                - sectorAI.getOffensiveTargets(raceID).get(j).x),
                        Math.abs(sectorAI.getHighestShipDanger(raceID).y
                                - sectorAI.getOffensiveTargets(raceID).get(j).y));
                if (space < nearestSector) {
                    //if this target is a smaller threat then our entire fleet or we have at least 50% of our ships gathered here, then we give it as target
                    if ((sectorAI.getDangerOnlyFromCombatShips(raceID, sectorAI.getHighestShipDanger(raceID)) > sectorAI
                            .getCompleteDanger(raceID, sectorAI.getOffensiveTargets(raceID).get(j)))
                            || (sectorAI.getDangerOnlyFromCombatShips(raceID, sectorAI.getHighestShipDanger(raceID)) > (sectorAI
                                    .getCompleteDanger(raceID) * 50 / 100))) {
                        nearestSector = space;
                        sector = sectorAI.getOffensiveTargets(raceID).get(j);
                    }
                }
            }
            attackSector.put(raceID, sector);
            Gdx.app.debug("ShipAI", String.format("Race %s: global attack sector is %d,%d", raceID,
                    attackSector.get(raceID).x, attackSector.get(raceID).y));
        }
    }

    /**
     * The function calculates a possible bombard sector. If one is found then it will have highest priority
     */
    private void calcBombardSector() {
        ArrayMap<String, Major> majors = manager.getRaceController().getMajors();
        for (int i = 0; i < majors.size; i++) {
            String raceID = majors.getKeyAt(i);
            Major major = majors.getValueAt(i);
            //pacifist races never bombard
            if (major.isRaceProperty(RaceProperty.PACIFIST))
                continue;

            //Not all races bombard for the same time. Ie. the Federation can't bombard for long without loosing morale
            int moraleValue = MoraleObserver.getMoraleValue(major.getRaceMoralNumber(), 19);
            if (moraleValue < 0) {
                moraleValue = Math.abs(moraleValue);
                if (averageMorale.get(raceID, 0) < 80 + moraleValue * 5)
                    continue;
            }

            //if the attack sector is also a bombard sector, then go for it
            for (int j = 0; j < sectorAI.getBombardTargets(raceID).size; j++) {
                if (sectorAI.getBombardTargets(raceID).get(j).equals(attackSector.get(raceID))) {
                    bombardSector.put(raceID, sectorAI.getBombardTargets(raceID).get(j));
                    break;
                }
            }

            //if the bombardsector wasn't yet found, then we try to find one which is nearest to our highest ship accumulation
            if (bombardSector.get(raceID).equals(new IntPoint())) {
                int nearestSector = Integer.MAX_VALUE;

                IntPoint sector = new IntPoint();
                for (int j = 0; j < sectorAI.getBombardTargets(raceID).size; j++) {
                    IntPoint possibleTarget = sectorAI.getBombardTargets(raceID).get(j);
                    int space = Math.max(Math.abs(sectorAI.getHighestShipDanger(raceID).x - possibleTarget.x),
                            Math.abs(sectorAI.getHighestShipDanger(raceID).y - possibleTarget.y));
                    if (space < nearestSector) {
                        if ((sectorAI.getDangerOnlyFromCombatShips(raceID, sectorAI.getHighestShipDanger(raceID)) > sectorAI
                                .getCompleteDanger(raceID, possibleTarget))) {
                            nearestSector = space;
                            sector = possibleTarget;
                        } else {
                            StarSystem ss = manager.getUniverseMap().getStarSystemAt(possibleTarget);
                            if (ss.getOwner() != null && ss.getOwner().isMinor()) {
                                nearestSector = space;
                                sector = possibleTarget;
                            }
                        }
                    }
                }
                bombardSector.put(raceID, sector);
            }
            Gdx.app.debug("ShipAI", String.format("Race %s: global bombard sector is %d,%d", raceID,
                    bombardSector.get(raceID).x, bombardSector.get(raceID).y));
        }
    }
}
