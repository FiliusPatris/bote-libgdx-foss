/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ai;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.FloatArray;
import com.badlogic.gdx.utils.ObjectFloatMap;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.constants.ResearchType;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.races.Research;
import com.blotunga.bote.races.ResearchInfo;
import com.blotunga.bote.utils.RandUtil;

public class ResearchAI {
    public static void calc(ResourceManager manager) {
        ArrayMap<String, Major> majors = manager.getRaceController().getMajors();
        ObjectFloatMap<Major> techlevelsMap = new ObjectFloatMap<Major>();
        FloatArray techlevelsList = new FloatArray();
        for (int i = 0; i < majors.size; i++) {
            Major major = majors.getValueAt(i);
            //for humans do nothing
            if (major == null || major.aHumanPlays())
                continue;
            Research research = major.getEmpire().getResearch();
            double techLevel = 0;
            for (int j = ResearchType.BIO.getType(); j < ResearchType.UNIQUE.getType(); j++) {
                ResearchType rt = ResearchType.fromType(j);
                techLevel += research.getResearchLevel(rt);
            }
            techLevel /= 6.0f;
            techlevelsList.add((float) techLevel);

            if (!research.isUniqueReady())
                if (!research.getResearchInfo().getChoiceTaken())
                    techlevelsMap.put(major, (float) techLevel);
        }
        techlevelsList.sort();
        techlevelsList.reverse();
        int cnt = Math.min(techlevelsList.size, 2);
        //only the first two majors with the best or second best techlevel will try to research a special field
        for (int i = 0; i < cnt; i++) {
            for (ObjectFloatMap.Entries<Major> iter = techlevelsMap.entries().iterator(); iter.hasNext();) {
                ObjectFloatMap.Entry<Major> e = iter.next();
                if (GameConstants.Equals(techlevelsList.get(i), e.value)) {
                    Major major = e.key;
                    Research research = major.getEmpire().getResearch();
                    ResearchInfo ri = research.getResearchInfo();
                    if (ri.getChoiceTaken())
                        break;

                    int uniqueTech = research.getUniqueTech() + 1;
                    if (uniqueTech < research.getBioTech() && uniqueTech < research.getEnergyTech()
                            && uniqueTech < research.getComputerTech() && uniqueTech < research.getConstructionTech()
                            && uniqueTech < research.getPropulsionTech() && uniqueTech < research.getWeaponTech()) {
                        int complex = (int) (RandUtil.random() * 3 + 1);
                        ri.setUniqueResearchChoosePossibility(complex);
                        research.setPercentage(ResearchType.UNIQUE, 100);
                        String name = ri.getCurrentResearchComplex().getComplexName();
                        Gdx.app.debug("ResearchAI", String.format(
                                "calc(): %s choose in unique complex '%s' field %d and set level to 100%%",
                                major.getRaceId(), name, complex));
                    }
                }
            }
        }
    }
}
