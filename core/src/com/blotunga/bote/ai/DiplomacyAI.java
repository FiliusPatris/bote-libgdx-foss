/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ai;

import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.constants.AnswerStatus;
import com.blotunga.bote.races.DiplomacyInfo;
import com.blotunga.bote.races.Race;

public abstract class DiplomacyAI {
    protected Race race; //the race to which this object belongs to
    protected ResourceManager manager;

    public DiplomacyAI(ResourceManager manager, Race race) {
        this.manager = manager;
        this.race = race;
    }

    /**
     * This function calculates how an AI controlled race reacts on an offer
     * @param info - the info of the offer
     * @return - Accepted/Declined or Not_Reacted
     */
    public abstract AnswerStatus reactOnOffer(DiplomacyInfo info);

    /**
     * Function which creates a diplomatic message
     * @param raceID - id of the race to whom it makes the offer
     * @return - null if no offer was made, otherwise a diplomacyInfo instance
     */
    public abstract boolean makeOffer(String otherRaceID, DiplomacyInfo info);

    /**
     * Function calculates the improvement of the relationship if credits or resources were transferred
     * The credits are not transferred here effectively, but the relation is improved
     * @param info - reference on a diplomatic info
     */
    protected abstract void reactOnDowry(DiplomacyInfo info);
}
