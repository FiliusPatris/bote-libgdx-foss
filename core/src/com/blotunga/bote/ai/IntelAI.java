/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.ai;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.ObjectIntMap;
import com.blotunga.bote.ResourceManager;
import com.blotunga.bote.constants.DiplomaticAgreement;
import com.blotunga.bote.constants.RaceProperty;
import com.blotunga.bote.intel.IntelObject;
import com.blotunga.bote.intel.Intelligence;
import com.blotunga.bote.races.Major;
import com.blotunga.bote.utils.RandUtil;

public class IntelAI {
    private ObjectIntMap<String> intelPrio;

    public IntelAI() {
        intelPrio = new ObjectIntMap<String>();
    }

    public void reset() {
        intelPrio.clear();
    }

    public int getIntelPrio(String race) {
        return intelPrio.get(race, 0);
    }

    public void calcIntelligence(ResourceManager manager) {
        class IntelListItem implements Comparable<IntelListItem> {
            String race;
            int points;

            public IntelListItem(String race, int points) {
                this.race = race;
                this.points = points;
            }

            @Override
            public int compareTo(IntelListItem o) {
                return (points < o.points) ? -1 : ((points == o.points) ? 0 : 1);
            }
        }

        Array<IntelListItem> intellist = new Array<IntelListItem>();
        ArrayMap<String, Major> majors = manager.getRaceController().getMajors();
        for (int i = 0; i < majors.size; i++) {
            String raceID = majors.getKeyAt(i);
            Intelligence intel = majors.getValueAt(i).getEmpire().getIntelligence();
            int points = intel.getSecurityPoints() + intel.getInnerSecurityStorage();
            for (int j = 0; j < majors.size; j++) {
                String otherID = majors.getKeyAt(j);
                if (!raceID.equals(otherID))
                    points += (intel.getSPStorage(0, otherID) + intel.getSPStorage(1, otherID));
                intellist.add(new IntelListItem(raceID, points));
            }
        }
        intellist.sort();
        intellist.reverse();
        //our priority is the index of the race in the array
        //if the points differ by less than 10% or 100SP then the previous index is used
        intelPrio.put(intellist.get(0).race, (int) (RandUtil.random() * 2));
        for (int i = 1; i < intellist.size; i++) {
            if (intellist.get(i - 1).points - intellist.get(i).points > 100
                    && (intellist.get(i).points * 100 / (intellist.get(i - 1).points + 1) < 90))
                intelPrio.put(intellist.get(i).race, i);
            else
                intelPrio.put(intellist.get(i).race, intelPrio.get(intellist.get(i - 1).race, 0));
        }

        //every report which targets us from the last 5 turns raises the priority by one if it was a sabotage action
        ObjectIntMap<String> badReports = new ObjectIntMap<String>();
        for (int i = 0; i < majors.size; i++) {
            String majorID = majors.getKeyAt(i);
            Major major = majors.getValueAt(i);
            if (!major.aHumanPlays()) {
                Intelligence intel = major.getEmpire().getIntelligence();
                for (int l = 0; l < intel.getIntelReports().getNumberOfReports(); l++) {
                    IntelObject intelObj = intel.getIntelReports().getReport(l);
                    if (intelObj.getEnemy().equals(majorID) && manager.getCurrentRound() - intelObj.getRound() < 6
                            && intelObj.isSabotage())
                        badReports.put(majorID, (badReports.get(majorID, 0) + 1));
                }
                intelPrio.put(majorID, (intelPrio.get(majorID, 0) + badReports.get(majorID, 0)));
            }
            Gdx.app.debug("IntelAI",
                    String.format("Intel-AI: Intel Prio of %s is %d", majorID, intelPrio.get(majorID, 0)));
        }

        //now we can make the AI act
        for (int i = 0; i < majors.size; i++) {
            String majorID = majors.getKeyAt(i);
            Major major = majors.getValueAt(i);
            if (!major.aHumanPlays()) {
                Intelligence intel = major.getEmpire().getIntelligence();
                //if we were targeted in the last 5 rounds, then maximize inner security
                if (badReports.get(majorID, 0) > 0) {
                    intel.setAssignment().setGlobalPercentage(2, 100, major, "", majors);
                    continue;
                }

                //otherwise maybe we can get active - the AI uses only sabotage
                //First find a target
                // - the worst relationship to the target
                // - relationship under 50% or the contract is smaller then friendship and not a defense pact
                int worstRel = Integer.MAX_VALUE;
                Major worstRace = null;
                for (int j = 0; j < majors.size; j++) {
                    String otherID = majors.getKeyAt(j);
                    Major otherRace = majors.getValueAt(j);
                    if (!otherID.equals(majorID) && major.isRaceContacted(otherID)) {
                        if ((major.getAgreement(otherID).getType() < DiplomaticAgreement.FRIENDSHIP.getType() && !major
                                .getDefencePact(otherID)) || major.getRelation(otherID) < 50) {
                            //calculate worst relationship
                            if (major.getRelation(otherID) < worstRel) {
                                worstRel = major.getRelation(otherID);
                                worstRace = otherRace;
                            } else if (major.getRelation(otherID) == worstRel && (int) (RandUtil.random() * 2) == 0) {
                                worstRel = major.getRelation(otherID);
                                worstRace = otherRace;
                            }
                        }

                        //set a random responsible race
                        intel.setResponsibleRace(majorID);
                        if (otherRace.getEmpire().countSystems() > 0 && (int) (RandUtil.random() * 3) == 0) {
                            intel.setResponsibleRace(otherID);
                            break;
                        }
                    }
                }

                //now assign security points
                if (worstRace != null) {
                    Intelligence worstIntel = worstRace.getEmpire().getIntelligence();
                    Gdx.app.debug("IntelAI", String.format("Intel-AI: assigned intel victim of %s is %s", majorID,
                            worstRace.getRaceId()));
                    //every race leaves a a certain percentage in the inner security
                    int innerSecPerc = 25;

                    if (major.isRaceProperty(RaceProperty.FINANCIAL))
                        innerSecPerc += 15;
                    if (major.isRaceProperty(RaceProperty.WARLIKE))
                        innerSecPerc += 25;
                    if (major.isRaceProperty(RaceProperty.AGRARIAN))
                        innerSecPerc += 25;
                    if (major.isRaceProperty(RaceProperty.INDUSTRIAL))
                        innerSecPerc += 10;
                    if (major.isRaceProperty(RaceProperty.SECRET))
                        innerSecPerc -= 10;
                    if (major.isRaceProperty(RaceProperty.SCIENTIFIC))
                        innerSecPerc += 0;
                    if (major.isRaceProperty(RaceProperty.PRODUCER))
                        innerSecPerc += 5;
                    if (major.isRaceProperty(RaceProperty.PACIFIST))
                        innerSecPerc += 35;
                    if (major.isRaceProperty(RaceProperty.SNEAKY))
                        innerSecPerc -= 15;
                    if (major.isRaceProperty(RaceProperty.SOLOING))
                        innerSecPerc += 40;
                    if (major.isRaceProperty(RaceProperty.HOSTILE))
                        innerSecPerc += 0;

                    if (innerSecPerc > 100)
                        innerSecPerc = 100;
                    else if (innerSecPerc < 0)
                        innerSecPerc = 0;

                    if (intel.getAssignment().getGlobalSabotagePercentage(worstRace.getRaceId()) != 100 - innerSecPerc)
                        intel.setAssignment().setGlobalPercentage(2, 100, major, "", majors);
                    intel.setAssignment().setGlobalPercentage(1, 100 - innerSecPerc, major, worstRace.getRaceId(),
                            majors);

                    //when will the action start?
                    // - if our security pints + storage points > enemy inner security + storage

                    int type = (int) (RandUtil.random() * 4); //type of action
                    int ourPoints = intel.getSecurityPoints()
                            * intel.getAssignment().getGlobalSabotagePercentage(worstRace.getRaceId()) / 100
                            + intel.getSPStorage(1, worstRace.getRaceId())
                            * intel.getAssignment().getSabotagePercentages(worstRace.getRaceId(), type) / 100;
                    ourPoints += ourPoints * intel.getBonus(type, 1) / 100;

                    int enemyPoints = worstIntel.getSecurityPoints()
                            * worstIntel.getAssignment().getInnerSecurityPercentage() / 100;
                    // + their bonus
                    enemyPoints += enemyPoints * worstIntel.getInnerSecurityBonus() / 100;
                    // + storage
                    enemyPoints += worstIntel.getInnerSecurityStorage();
                    if (ourPoints > enemyPoints + (int) (RandUtil.random() * 1500)) {
                        //first set 100 percent for the storage so that we can set then 100% to the target
                        intel.setAssignment().setSabotagePercentage(4, 100, worstRace.getRaceId());
                        intel.setAssignment().setSabotagePercentage(type, 100, worstRace.getRaceId());
                    } else
                        intel.setAssignment().setSabotagePercentage(4, 100, worstRace.getRaceId());
                    Gdx.app.debug("IntelAI",
                            String.format("Intel-AI: our SP: %d - enemies SP: %d", ourPoints, enemyPoints));
                } else {
                    intel.setAssignment().setGlobalPercentage(2, 100, major, "", majors);
                }
            }
        }
    }
}
