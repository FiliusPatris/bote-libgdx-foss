/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.constants;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.utils.IntMap;
import com.blotunga.bote.utils.Pair;

public enum DiplomaticAgreement {
    WARPACT(-5, "WAR_PACT"),         // if partner agrees common war declaration
    DEFENCEPACT(-2, "DEFENCE_PACT"), // it can be offered except in alliance or war
    WAR(-1, "WAR"),		             // declaration of war
    NONE(0, ""),
    NAP(1, "NON_AGGRESSION"),
    TRADE(2, "TRADE_AGREEMENT"),     // trade possible with resources etc.
    FRIENDSHIP(3, "FRIENDSHIP"),     // diplomatic agreements with other races become visible
    COOPERATION(4, "COOPERATION"),   // shipyards and outposts can be used
    ALLIANCE(5, "ALLIANCE"),         // cooperation on a diplomatic level
    MEMBERSHIP(6, "MEMBERSHIP"),
    PRESENT(10, "PRESENT"),
    CORRUPTION(11, ""),
    REQUEST(12, "REQUEST");

    private int type;
    private String dbName;
    private static final IntMap<DiplomaticAgreement> intToTypeMap = new IntMap<DiplomaticAgreement>();

    DiplomaticAgreement(int type, String name) {
        this.type = type;
        this.dbName = name;
    }

    static {
        for (DiplomaticAgreement da : DiplomaticAgreement.values()) {
            intToTypeMap.put(da.type, da);
        }
    }

    public int getType() {
        return type;
    }

    public String getdbName() {
        return dbName;
    }

    public static DiplomaticAgreement fromInt(int i) {
        DiplomaticAgreement da = intToTypeMap.get(i);
        return da;
    }

    public static Pair<String, Color> getDiplomacyStatus(DiplomaticAgreement agreement) {
        Color color = new Color(1.0f, 1.0f, 1.0f, 0.25f);
        String text = agreement.getdbName().isEmpty() ? "NONE" : agreement.getdbName();
        switch (agreement) {
            case NAP:
                color = new Color(139 / 255.0f, 175 / 255.0f, 172 / 255.0f, 0.25f);
                break;
            case TRADE:
                color = new Color(233 / 255.0f, 183 / 255.0f, 12 / 255.0f, 0.25f);
                break;
            case FRIENDSHIP:
                color = new Color(6 / 255.0f, 187 / 255.0f, 34 / 255.0f, 0.25f);
                break;
            case COOPERATION:
                color = new Color(37 / 255.0f, 159 / 255.0f, 250 / 255.0f, 0.25f);
                break;
            case ALLIANCE:
                color = new Color(29 / 255.0f, 29 / 255.0f, 248 / 255.0f, 0.25f);
                break;
            case MEMBERSHIP:
                color = new Color(115 / 255.0f, 12 / 255.0f, 228 / 255.0f, 0.25f);
                break;
            case WAR:
                color = new Color(220 / 255.0f, 15 / 255.0f, 15 / 255.0f, 0.25f);
                break;
            default:
                break;
        }

        return new Pair<String, Color>(text, color);
    }
}
