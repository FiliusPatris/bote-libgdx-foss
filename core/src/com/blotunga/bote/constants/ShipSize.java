/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.constants;

import com.badlogic.gdx.utils.IntMap;

public enum ShipSize {
    SMALL(0, "SMALL"),
    NORMAL(1, "MIDDLE"),
    BIG(2, "BIG"),
    HUGE(3, "HUGE");

    int size;
    String name; // StringDB entry
    private static final IntMap<ShipSize> intToTypeMap = new IntMap<ShipSize>();

    ShipSize(int size, String name) {
        this.size = size;
        this.name = name;
    }

    static {
        for (ShipSize sz : ShipSize.values()) {
            intToTypeMap.put(sz.size, sz);
        }
    }

    public int getSize() {
        return size;
    }

    public String getName() {
        return name;
    }

    public static ShipSize fromInt(int i) {
        ShipSize sz = intToTypeMap.get(i);
        return sz;
    }
}
