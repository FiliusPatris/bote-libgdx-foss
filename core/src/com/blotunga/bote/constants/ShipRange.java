/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.constants;

import com.badlogic.gdx.utils.IntMap;

public enum ShipRange {
    SHORT(0, "SHORT"),
    MIDDLE(1, "MIDDLE"),
    LONG(2, "LONG");

    private int range;
    private String name; // index in StringDB
    private static final IntMap<ShipRange> intToTypeMap = new IntMap<ShipRange>();

    ShipRange(int range, String name) {
        this.range = range;
        this.name = name;
    }

    static {
        for (ShipRange sr : ShipRange.values()) {
            intToTypeMap.put(sr.range, sr);
        }
    }

    public int getRange() {
        return range;
    }

    public String getName() {
        return name;
    }

    public static ShipRange fromInt(int i) {
        ShipRange sr = intToTypeMap.get(i);
        return sr;
    }
}
