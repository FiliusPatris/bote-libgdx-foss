/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.constants;

public enum Difficulties {
    EASY(1.5f, "easy"),
    NORMAL(1.0f, "normal"),
    HARD(0.5f, "hard"),
    VERY_HARD(0.33f, "very hard"),
    IMPOSSIBLE(0.2f, "impossible");

    private float level;
    private String name;

    Difficulties(float level, String name) {
        this.level = level;
        this.name = name;
    }

    public float getLevel() {
        return level;
    }

    public String getName() {
        return name;
    }
}
