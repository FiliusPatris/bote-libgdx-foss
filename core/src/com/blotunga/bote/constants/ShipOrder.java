/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.constants;

public enum ShipOrder {
    NONE(-1, "NO_SHIP_ORDER"),
    AVOID(0, "AVOID_ORDER"),
    ATTACK(1, "ATTACK_ORDER"),
    ENCLOAK(2, "CLOAK_ORDER"),
    ATTACK_SYSTEM(3, "ATTACK_SYSTEM_ORDER"),
    RAID_SYSTEM(4, "RAID_SYSTEM_ORDER"),
    BLOCKADE_SYSTEM(5, "BLOCKADE_SYSTEM_ORDER"),
    DESTROY_SHIP(6, "DESTROY_SHIP_ORDER"),
    COLONIZE(7, "COLONIZE_ORDER"),
    TERRAFORM(8, "TERRAFORM_ORDER"),
    BUILD_OUTPOST(9, "BUILD_OUTPOST_ORDER"),
    BUILD_STARBASE(10, "BUILD_STARBASE_ORDER"),
    ASSIGN_FLAGSHIP(11, "ASSIGN_FLAGSHIP_ORDER"),
    CREATE_FLEET(12, "CREATE_FLEET_ORDER"),
    TRANSPORT(13, "TRANSPORT_ORDER"),
    AUTO_EXPLORE(14, "EXPLORE_ORDER"),
    TRAIN_SHIP(15, "TRAIN_SHIP_ORDER"), //keep this to maintain save compatibility
    WAIT_SHIP_ORDER(16, "WAIT_SHIP_ORDER"),
    SENTRY_SHIP_ORDER(17, "SENTRY_SHIP_ORDER"),
    REPAIR(18, "REPAIR_SHIP_ORDER"),
    DECLOAK(19, "DECLOAK_ORDER"),
    CANCEL(20, ""),
    IMPROVE_SHIELDS(21, "IMPROVE_SHIELDS_SHIP_ORDER"),
    UPGRADE_OUTPOST(22, "UPGRADE_OUTPOST_ORDER"),
    UPGRADE_STARBASE(23, "UPGRADE_STARBASE_ORDER"),
    EXTRACT_DEUTERIUM(24, "EXTRACT_DEUTERIUM_SHIP_ORDER"),
    EXPLORE_WORMHOLE(25, "EXPLORE_WORMHOLE_SHIP_ORDER");

    private int type;
    private String name; // name in the stringDB

    ShipOrder(int type, String name) {
        this.type = type;
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public String getName() {
        return name;
    }
}
