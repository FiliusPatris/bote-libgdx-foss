/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.constants;

import com.badlogic.gdx.utils.IntMap;
import com.blotunga.bote.utils.RandUtil;

public enum PlanetSize {
    SMALL(0, 20.0f),
    NORMAL(1, 17.0f),
    BIG(2, 13.0f),
    GIANT(3, 10.0f);

    private int size;
    private float scale;
    private static final IntMap<PlanetSize> intToTypeMap = new IntMap<PlanetSize>();

    PlanetSize(int size, float scale) {
        this.size = size;
        this.scale = scale;
    }

    static {
        for (PlanetSize ps : PlanetSize.values()) {
            intToTypeMap.put(ps.size, ps);
        }
    }

    public int getSize() {
        return size;
    }

    public float getScale() {
        return scale;
    }

    public static PlanetSize getRandomSize() {
        return fromInt((int) Math.floor(RandUtil.random() * PlanetSize.values().length));
    }

    public static PlanetSize fromInt(int i) {
        PlanetSize ps = intToTypeMap.get(i);
        return ps;
    }
}
