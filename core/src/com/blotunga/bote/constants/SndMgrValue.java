/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.constants;

public enum SndMgrValue {
    SNDMGR_SOUND_ENDOFROUND("RoundEnd"),
    SNDMGR_SOUND_SHIPTARGET("affirmative"),
    SNDMGR_SOUND_ERROR("error"),
    SNDMGR_MSG_ALIENCONTACT("alienContact"),
    SNDMGR_MSG_CLAIMSYSTEM("claimSystem"),
    SNDMGR_MSG_COLONIZING("colonizing"),
    SNDMGR_MSG_DIPLOMATICNEWS("diplomatNews"),
    SNDMGR_MSG_FIRSTCONTACT("firstContact"),
    SNDMGR_MSG_INTELNEWS("intelNews"),
    SNDMGR_MSG_NEWTECHNOLOGY("newTechnology"),
    SNDMGR_MSG_OUTPOST_CONSTRUCT("outpostConstruct"),
    SNDMGR_MSG_OUTPOST_READY("outpostReady"),
    SNDMGR_MSG_SCIENTISTNEWS("scientistNews"),
    SNDMGR_MSG_STARBASE_CONSTRUCT("starbaseConstruct"),
    SNDMGR_MSG_STARBASE_READY("starbaseReady"),
    SNDMGR_MSG_TERRAFORM_COMPLETE("terraformComplete"),
    SNDMGR_MSG_TERRAFORM_SELECT("terraformSelect"),
    SNDMGR_MSG_RACESELECT("raceSelect");
    private String path;

    SndMgrValue(String path) {
        this.path = path;
    }

    public String getPath() {
        return path + ".ogg";
    }
}
