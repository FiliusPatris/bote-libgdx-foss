/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.constants;

import com.blotunga.bote.editors.ResourceEditor;
import com.blotunga.bote.events.EventScreen;
import com.blotunga.bote.ui.screens.CombatScreen;
import com.blotunga.bote.ui.screens.CombatSimulator;
import com.blotunga.bote.ui.screens.DiplomacyScreen;
import com.blotunga.bote.ui.screens.EmpireScreen;
import com.blotunga.bote.ui.screens.DatabaseScreen;
import com.blotunga.bote.ui.screens.FleetScreen;
import com.blotunga.bote.ui.screens.IntelScreen;
import com.blotunga.bote.ui.screens.LoadingScreen;
import com.blotunga.bote.ui.screens.MainMenu;
import com.blotunga.bote.ui.screens.OptionsScreen;
import com.blotunga.bote.ui.screens.ResearchScreen;
import com.blotunga.bote.ui.screens.ShipDesignScreen;
import com.blotunga.bote.ui.screens.SystemBuildScreen;
import com.blotunga.bote.ui.screens.TradeScreen;
import com.blotunga.bote.ui.screens.TransportScreen;
import com.blotunga.bote.ui.screens.TutorialScreen;
import com.blotunga.bote.ui.screens.UniverseRenderer;

public enum ViewTypes {
    NULL_VIEW(null),
    GALAXY_VIEW(UniverseRenderer.class),
    SYSTEM_VIEW(SystemBuildScreen.class),
    RESEARCH_VIEW(ResearchScreen.class),
    INTEL_VIEW(IntelScreen.class),
    DIPLOMACY_VIEW(DiplomacyScreen.class),
    TRADE_VIEW(TradeScreen.class),
    EMPIRE_VIEW(EmpireScreen.class),
    FLEET_VIEW(FleetScreen.class),
    SHIPDESIGN_VIEW(ShipDesignScreen.class),
    TRANSPORT_VIEW(TransportScreen.class),
    EVENT_VIEW(EventScreen.class),
    COMBAT_VIEW(CombatScreen.class),
    OPTIONS_VIEW(OptionsScreen.class),
    MAIN_MENU(MainMenu.class),
    LOADING_SCREEN(LoadingScreen.class),
    TUTORIAL_SCREEN(TutorialScreen.class),
    ENCYCLOPEDIA_SCREEN(DatabaseScreen.class),
    RESOURCE_EDITOR(ResourceEditor.class),
    COMBAT_SIMULATOR(CombatSimulator.class);

    private Class<?> classType;

    ViewTypes(Class<?> classType) {
        this.classType = classType;
    }

    public Class<?> getClassType() {
        return classType;
    }
}
