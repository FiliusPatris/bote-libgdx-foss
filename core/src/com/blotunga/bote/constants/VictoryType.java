/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.constants;

public enum VictoryType {
    VICTORYTYPE_ELIMINATION("VC_ELIMINATION"),	///< only surviving race
    VICTORYTYPE_DIPLOMACY("VC_DIPLOMACY"),		///< over 50% of races are members or alliance (at least 10)
    VICTORYTYPE_CONQUEST("VC_CONQUEST"),		///< over 50% of the inhabited systems are conquered (at least 10)
    VICTORYTYPE_RESEARCH("VC_RESEARCH"),		///< special research #10 was researched
    VICTORYTYPE_COMBATWINS("VC_COMBAT"),		///< current round / 4 ship combat wins (at least 50)
    VICTORYTYPE_SABOTAGE("VC_SECURITY");		///< current round * 2 successful sabotage actions (at least 500)

    private String dbName;

    VictoryType(String dbName) {
        this.dbName = dbName;
    }

    public String getDBName() {
        return dbName;
    }
}
