/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.constants;

import com.badlogic.gdx.utils.IntMap;

public enum PlanetType {
    PLANETCLASS_M(0, "M", 2048),
    PLANETCLASS_O(1, "O", 8192),
    PLANETCLASS_L(2, "L", 1024),
    PLANETCLASS_P(3, "P", 16384),
    PLANETCLASS_H(4, "H", 64),
    PLANETCLASS_Q(5, "Q", 32768),
    PLANETCLASS_K(6, "K", 512),
    PLANETCLASS_G(7, "G", 32),
    PLANETCLASS_R(8, "R", 65536),
    PLANETCLASS_F(9, "F", 16),
    PLANETCLASS_C(10, "C", 4),
    PLANETCLASS_N(11, "N", 4096),
    PLANETCLASS_A(12, "A", 1),
    PLANETCLASS_B(13, "B", 2),
    PLANETCLASS_E(14, "E", 8),
    PLANETCLASS_Y(15, "Y", 524288),
    PLANETCLASS_I(16, "I", 128),
    PLANETCLASS_J(17, "J", 256),
    PLANETCLASS_S(18, "S", 131072),
    PLANETCLASS_T(19, "T", 262144),
    GRAPHICNUMBER(31, "", 0);

    private int planetClass;
    private int type; // A = 1, B = 2 ...etc.
    private String typeName; //name of the planet in the texture pack
    private static final IntMap<PlanetType> intToPlanetClassMap = new IntMap<PlanetType>();
    private static final IntMap<PlanetType> intToTypeMap = new IntMap<PlanetType>();

    PlanetType(int planetClass, String typeName, int type) {
        this.planetClass = planetClass;
        this.typeName = typeName;
        this.type = type;
    }

    static {
        for (PlanetType pt : PlanetType.values()) {
            intToPlanetClassMap.put(pt.planetClass, pt);
        }
        for (PlanetType pt : PlanetType.values()) {
            intToTypeMap.put(pt.type, pt);
        }
    }

    public String getTypeName() {
        return typeName;
    }

    public int getPlanetClass() {
        return planetClass;
    }

    public int getType() {
        return type;
    }

    public String getDBName() {
        return "CLASS_" + typeName + "_TYPE";
    }

    public static PlanetType fromPlanetClass(int i) {
        PlanetType pt = intToPlanetClassMap.get(i);
        return pt;
    }

    public static PlanetType fromPlanetType(int i) {
        PlanetType pt = intToTypeMap.get(i);
        return pt;
    }
}
