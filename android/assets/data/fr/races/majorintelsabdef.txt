MAJOR1:0:0:Un acte d�sastreux de saboteurs ennemis a caus� la destruction de $number$ infrastructures de type "$building$" dans notre syst�me $system$.
MAJOR2:0:0:Un acte d�sastreux de saboteurs ennemis a caus� la destruction de $number$ infrastructures de type "$building$" dans notre syst�me $system$.
MAJOR3:0:0:Un acte d�sastreux de saboteurs ennemis a caus� la destruction de $number$ infrastructures de type "$building$" dans notre syst�me $system$.
MAJOR4:0:0:Un acte d�sastreux de saboteurs ennemis a caus� la destruction de $number$ infrastructures de type "$building$" dans notre syst�me $system$.
MAJOR5:0:0:Un acte d�sastreux de saboteurs ennemis a caus� la destruction de $number$ infrastructures de type "$building$" dans notre syst�me $system$.
MAJOR6:0:0:Un acte d�sastreux de saboteurs ennemis a caus� la destruction de $number$ infrastructures de type "$building$" dans notre syst�me $system$.
MAJOR1:0:1:Notre service de renseignement rapporte la perte de $credits$ Cr�dits. Aucune trace de vol n'a pu �tre trouv�e.
MAJOR2:0:1:Notre service de renseignement rapporte la perte de $credits$ Cr�dits. Aucune trace de vol n'a pu �tre trouv�e. Cela signifie notre ruine certaine !
MAJOR3:0:1:Notre service de renseignement rapporte la perte de $credits$ Cr�dits. Aucune trace de vol n'a pu �tre trouv�e.
MAJOR4:0:1:Le Taq'rhiar rapporte la perte de $credits$ Cr�dits. Aucune trace de vol n'a pu �tre trouv�e.
MAJOR5:0:1: L'Ordre Cartare rapporte la perte de $credits$ Cr�dits. Aucune trace de vol n'a pu �tre trouv�e.
MAJOR6:0:1:Notre service de renseignement rapporte la perte de $credits$ Cr�dits. Aucune trace de vol n'a pu �tre trouv�e.
MAJOR1:0:2:Des aliments empoisonn�s ont �t� signal�s du syst�me $system$. Le nombre de personnes d�c�d�es dans l'incident s'�l�ve � $number$ milliards.
MAJOR2:0:2:Des aliments empoisonn�s ont �t� signal�s du syst�me $system$. Le nombre de personnes d�c�d�es dans l'incident s'�l�ve � $number$ milliards.
MAJOR3:0:2:Des aliments empoisonn�s ont �t� signal�s du syst�me $system$. Le nombre de personnes d�c�d�es dans l'incident s'�l�ve � $number$ milliards.
MAJOR4:0:2:Des aliments empoisonn�s ont �t� signal�s du syst�me $system$. Le nombre de personnes d�c�d�es dans l'incident s'�l�ve � $number$ milliards.
MAJOR5:0:2:Des aliments empoisonn�s ont �t� signal�s du syst�me $system$. Le nombre de personnes d�c�d�es dans l'incident s'�l�ve � $number$ milliards.
MAJOR6:0:2:Des aliments empoisonn�s ont �t� signal�s du syst�me $system$. Le nombre de personnes d�c�d�es dans l'incident s'�l�ve � $number$ milliards.
MAJOR1:1:0:Un acte d�sastreux de saboteurs ennemis a caus� la destruction de $number$ infrastructures de type "$building$" dans notre syst�me $system$.
MAJOR2:1:0:Un acte d�sastreux de saboteurs ennemis a caus� la destruction de $number$ infrastructures de type "$building$" dans notre syst�me $system$.
MAJOR3:1:0:Un acte d�sastreux de saboteurs ennemis a caus� la destruction de $number$ infrastructures de type "$building$" dans notre syst�me $system$.
MAJOR4:1:0:Un acte d�sastreux de saboteurs ennemis a caus� la destruction de $number$ infrastructures de type "$building$" dans notre syst�me $system$.
MAJOR5:1:0:Un acte d�sastreux de saboteurs ennemis a caus� la destruction de $number$ infrastructures de type "$building$" dans notre syst�me $system$.
MAJOR6:1:0:Un acte d�sastreux de saboteurs ennemis a caus� la destruction de $number$ infrastructures de type "$building$" dans notre syst�me $system$.
MAJOR1:1:1:Les espions ennemis ont r�ussi � acc�der et � voler nos derni�res donn�es de recherche scientifique. Nous ne pouvons plus acc�der � ces fichiers et avons donc perdu $FP$ de points de recherche.
MAJOR2:1:1:Les espions ennemis ont r�ussi � acc�der et � voler nos derni�res donn�es de recherche scientifique. Nous ne pouvons plus acc�der � ces fichiers et avons donc perdu $FP$ de points de recherche.
MAJOR3:1:1:Les espions ennemis ont r�ussi � acc�der et � voler nos derni�res donn�es de recherche scientifique. Nous ne pouvons plus acc�der � ces fichiers et avons donc perdu $FP$ de points de recherche.
MAJOR4:1:1:Les espions ennemis ont r�ussi � acc�der et � voler nos derni�res donn�es de recherche scientifique. Nous ne pouvons plus acc�der � ces fichiers et avons donc perdu $FP$ de points de recherche.
MAJOR5:1:1:Les espions ennemis ont r�ussi � acc�der et � voler nos derni�res donn�es de recherche scientifique. Nous ne pouvons plus acc�der � ces fichiers et avons donc perdu $FP$ de points de recherche.
MAJOR6:1:1:Les espions ennemis ont r�ussi � acc�der et � voler nos derni�res donn�es de recherche scientifique. Nous ne pouvons plus acc�der � ces fichiers et avons donc perdu $FP$ de points de recherche.
MAJOR1:1:2:Des agents �trangers ont r�ussi � saboter l'une de nos exp�riences de plus haut niveau dans le domaine de $techtype$. Toutes les donn�es acquises jusqu'� l'incident ont �t� compl�tement perdues.
MAJOR2:1:2:Des agents �trangers ont r�ussi � saboter l'une de nos exp�riences de plus haut niveau dans le domaine de $techtype$. Toutes les donn�es acquises jusqu'� l'incident ont �t� compl�tement perdues.
MAJOR3:1:2:Des agents �trangers ont r�ussi � saboter l'une de nos exp�riences de plus haut niveau dans le domaine de $techtype$. Toutes les donn�es acquises jusqu'� l'incident ont �t� compl�tement perdues.
MAJOR4:1:2:Des agents �trangers ont r�ussi � saboter l'une de nos exp�riences de plus haut niveau dans le domaine de $techtype$. Toutes les donn�es acquises jusqu'� l'incident ont �t� compl�tement perdues.
MAJOR5:1:2:Des agents �trangers ont r�ussi � saboter l'une de nos exp�riences de plus haut niveau dans le domaine de $techtype$. Toutes les donn�es acquises jusqu'� l'incident ont �t� compl�tement perdues.
MAJOR6:1:2:Des agents �trangers ont r�ussi � saboter l'une de nos exp�riences de plus haut niveau dans le domaine de $techtype$. Toutes les donn�es acquises jusqu'� l'incident ont �t� compl�tement perdues.
MAJOR1:2:0:Des saboteurs ennemis ont d�truit $nombre$ b�timents de type "$building$" dans notre syst�me $system$.
MAJOR2:2:0:Des saboteurs ennemis ont d�truit $nombre$ b�timents de type "$building$" dans notre syst�me $system$.
MAJOR3:2:0:Des saboteurs ennemis ont d�truit $nombre$ b�timents de type "$building$" dans notre syst�me $system$.
MAJOR4:2:0:Des saboteurs ennemis ont d�truit $nombre$ b�timents de type "$building$" dans notre syst�me $system$.
MAJOR5:2:0:Des saboteurs ennemis ont d�truit $nombre$ b�timents de type "$building$" dans notre syst�me $system$.
MAJOR6:2:0:Des saboteurs ennemis ont d�truit $nombre$ b�timents de type "$building$" dans notre syst�me $system$.
MAJOR1:2:1:Un rapport de renseignement vient d'arriver sur des agents ennemis ayant d�pos� de grandes quantit�s de mati�res radioactives dans le syst�me $system$. Selon nos informations actuelles, nous devons pleurer la perte de $number$ $troop$ sur le site.
MAJOR2:2:1:Un rapport de renseignement vient d'arriver sur des agents ennemis ayant d�pos� de grandes quantit�s de mati�res radioactives dans le syst�me $system$. Selon nos informations actuelles, nous devons pleurer la perte de $number$ $troop$ sur le site.
MAJOR3:2:1:Un rapport de renseignement vient d'arriver sur des agents ennemis ayant d�pos� de grandes quantit�s de mati�res radioactives dans le syst�me $system$. Selon nos informations actuelles, nous devons pleurer la perte de $number$ $troop$ sur le site.
MAJOR4:2:1:Un rapport de renseignement du Taq'rhiar vient d'arriver sur des agents ennemis ayant d�pos� de grandes quantit�s de mati�res radioactives dans le syst�me $system$. Selon nos informations actuelles, nous devons pleurer la perte de $number$ $troop$ sur le site.
MAJOR5:2:1:Un rapport de renseignement de l'Ordre Cartare vient d'arriver sur des agents ennemis ayant d�pos� de grandes quantit�s de mati�res radioactives dans le syst�me $system$. Selon nos informations actuelles, nous devons pleurer la perte de $number$ $troop$ sur le site.
MAJOR6:2:1:Un rapport de renseignement vient d'arriver sur des agents ennemis ayant d�pos� de grandes quantit�s de mati�res radioactives dans le syst�me $system$. Selon nos informations actuelles, nous devons pleurer la perte de $number$ $troop$ sur le site.
MAJOR1:2:2:Des saboteurs ennemis ont r�ussi � faire exploser un $ship$ dans le syst�me $system$.
MAJOR2:2:2:Des saboteurs ennemis ont r�ussi � faire exploser un $ship$ dans le syst�me $system$.
MAJOR3:2:2:Des saboteurs ennemis ont r�ussi � faire exploser un $ship$ dans le syst�me $system$.
MAJOR4:2:2:Des saboteurs ennemis ont r�ussi � faire exploser un $ship$ dans le syst�me $system$.
MAJOR5:2:2:Des saboteurs ennemis ont r�ussi � faire exploser un $ship$ dans le syst�me $system$.
MAJOR6:2:2:Des saboteurs ennemis ont r�ussi � faire exploser un $ship$ dans le syst�me $system$.
MAJOR1:2:3:Les agents ennemis ont pu prendre le contr�le de l'un de nos navires dans le secteur de $system$. Ce $ship$ n'est plus sur nos scanners.
MAJOR2:2:3:Les agents ennemis ont pu prendre le contr�le de l'un de nos navires dans le secteur de $system$. Ce $ship$ n'est plus sur nos scanners.
MAJOR3:2:3:Les agents ennemis ont pu prendre le contr�le de l'un de nos navires dans le secteur de $system$. Ce $ship$ n'est plus sur nos scanners.
MAJOR4:2:3:Les agents ennemis ont pu prendre le contr�le de l'un de nos navires dans le secteur de $system$. Ce $ship$ n'est plus sur nos scanners.
MAJOR5:2:3:Les agents ennemis ont pu prendre le contr�le de l'un de nos navires dans le secteur de $system$. Ce $ship$ n'est plus sur nos scanners.
MAJOR6:2:3:Les agents ennemis ont pu prendre le contr�le de l'un de nos navires dans le secteur de $system$. Ce $ship$ n'est plus sur nos scanners.
MAJOR1:2:4:Le renseignement rapporte que des explosifs ont �t� cach�s sur un navire dans le secteur $ system $. Nous avons �t� en mesure de retirer la plupart d'entre eux avant la d�tonation causant seulement des dommages l�gers � la coque.
MAJOR2:2:4:Le renseignement rapporte que des explosifs ont �t� cach�s sur un navire dans le secteur $ system $. Nous avons �t� en mesure de retirer la plupart d'entre eux avant la d�tonation causant seulement des dommages l�gers � la coque.
MAJOR3:2:4:Le renseignement rapporte que des explosifs ont �t� cach�s sur un navire dans le secteur $ system $. Nous avons �t� en mesure de retirer la plupart d'entre eux avant la d�tonation causant seulement des dommages l�gers � la coque.
MAJOR4:2:4:Le Taq'rhiar rapporte que des explosifs ont �t� cach�s sur un navire dans le secteur $ system $. Nous avons �t� en mesure de retirer la plupart d'entre eux avant la d�tonation causant seulement des dommages l�gers � la coque.
MAJOR5:2:4:L'Ordre Cartare Le renseignement rapporte que des explosifs ont �t� cach�s sur un navire dans le secteur $ system $. Nous avons �t� en mesure de retirer la plupart d'entre eux avant la d�tonation causant seulement des dommages l�gers � la coque.
MAJOR6:2:4:Le renseignement rapporte que des explosifs ont �t� cach�s sur un navire dans le secteur $ system $. Nous avons �t� en mesure de retirer la plupart d'entre eux avant la d�tonation causant seulement des dommages l�gers � la coque.
MAJOR1:3:0:Nos relations avec $race$ se sont am�lior�es, bien que nous n'en comprenions pas la raison.
MAJOR2:3:0:Nos relations avec $race$ se sont am�lior�es, bien que nous n'en comprenions pas la raison.
MAJOR3:3:0:Nos relations avec $race$ se sont am�lior�es, bien que nous n'en comprenions pas la raison.
MAJOR4:3:0:Nos relations avec $race$ se sont am�lior�es, bien que nous n'en comprenions pas la raison.
MAJOR5:3:0:Nos relations avec $race$ se sont am�lior�es, bien que nous n'en comprenions pas la raison.
MAJOR6:3:0:Nos relations avec $race$ se sont am�lior�es, bien que nous n'en comprenions pas la raison.
MAJOR1:3:1:De la propagande contre nous par le renseignement ennemi a entra�n� une d�t�rioration de notre relation avec $major$.
MAJOR2:3:1:De la propagande contre nous par le renseignement ennemi a entra�n� une d�t�rioration de notre relation avec $major$.
MAJOR3:3:1:De la propagande contre nous par le renseignement ennemi a entra�n� une d�t�rioration de notre relation avec $major$.
MAJOR4:3:1:De la propagande contre nous par le renseignement ennemi a entra�n� une d�t�rioration de notre relation avec $major$.
MAJOR5:3:1:De la propagande contre nous par le renseignement ennemi a entra�n� une d�t�rioration de notre relation avec $major$.
MAJOR6:3:1:De la propagande contre nous par le renseignement ennemi a entra�n� une d�t�rioration de notre relation avec $major$.
MAJOR1:3:3:Des informations falsifi�es r�parties parmi les $minor$ ont nui � notre relation avec eux.
MAJOR2:3:3:Des informations falsifi�es r�parties parmi les $minor$ ont nui � notre relation avec eux.
MAJOR3:3:3:Des informations falsifi�es r�parties parmi les $minor$ ont nui � notre relation avec eux.
MAJOR4:3:3:Des informations falsifi�es r�parties parmi les $minor$ ont nui � notre relation avec eux.
MAJOR5:3:3:Des informations falsifi�es r�parties parmi les $minor$ ont nui � notre relation avec eux.
MAJOR6:3:3:Des informations falsifi�es r�parties parmi les $minor$ ont nui � notre relation avec eux.
