/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.android;

import java.util.Iterator;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.blotunga.bote.DriveIntegration;
import com.blotunga.bote.PlatformApiIntegration;
import com.blotunga.bote.PlatformCallback;
import com.blotunga.bote.achievements.AchievementsList;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.achievement.Achievement;
import com.google.android.gms.games.achievement.AchievementBuffer;
import com.google.android.gms.games.achievement.Achievements;
import com.google.android.gms.games.achievement.Achievements.LoadAchievementsResult;

public class AndroidPlatformApiIntegrator implements PlatformApiIntegration {
    //TODO: amazon implementation if I ever have the time
    GoogleApiClient mGoogleApiClient;
    AndroidApplication launcher;
    private boolean mInSignInFlow = false;
    private boolean mExplicitSignOut = false;
    private final static int requestCode = 9099;
    private PlatformCallback callback;
    private boolean triedSignIn = false;
    private DriveIntegration driveIntegration;
    private StorageIntegration storageIntegration;

    class AchievementClass implements ResultCallback<Achievements.LoadAchievementsResult> {

        @Override
        public void onResult(LoadAchievementsResult arg0) {
            AchievementBuffer aBuffer = null;
            AndroidLauncher gameLauncher = (AndroidLauncher) launcher;
            try {
                Achievement ach;
                aBuffer = arg0.getAchievements();
                Iterator<Achievement> aIterator = aBuffer.iterator();

                while (aIterator.hasNext()) {
                    ach = aIterator.next();
                    AchievementsList alItem = AchievementsList.fromId(ach.getAchievementId());
                    if (ach.getState() != Achievement.STATE_UNLOCKED) {
                        if (ach.getType() == Achievement.TYPE_INCREMENTAL) {
                            int diff = ach.getCurrentSteps() - gameLauncher.getGame().getGameSettings().achievements[alItem.ordinal()];
                            if (diff < 0)
                                incrementAchievement(alItem, Math.abs(diff));
                            else
                                gameLauncher.getGame().getGameSettings().achievements[alItem.ordinal()] = ach.getCurrentSteps();
                        } else if (alItem.isUnlocked(gameLauncher.getGame().getGameSettings().achievements[alItem.ordinal()])) {
                            unlockAchievement(alItem);
                        }
                    } else if (ach.getState() == Achievement.STATE_UNLOCKED) {
                        gameLauncher.getGame().getGameSettings().achievements[alItem.ordinal()] = alItem.getValue();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (aBuffer != null)
                aBuffer.close();
            if (callback != null)
                callback.call();
            if (mInSignInFlow)
                setInSignInFlow(false);
        }
    }

    public AndroidPlatformApiIntegrator(GoogleApiClient client, AndroidApplication launcher) {
        this.mGoogleApiClient = client;
        this.launcher = launcher;
        if (client != null)
            this.driveIntegration = new GoogleCloudIntegration(this);
        this.storageIntegration = new AndroidStorageIntegration(launcher);
    }

    @Override
    public void signIn() {
        triedSignIn = true;
        if (mGoogleApiClient != null && !mInSignInFlow) {
            mInSignInFlow = true;
            mGoogleApiClient.connect();
            Games.Achievements.load(mGoogleApiClient, false).setResultCallback(new AchievementClass());
        }
    }

    @Override
    public void signOut() {
        signOut(false);
    }

    @Override
    public void signOut(boolean explicite) {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mExplicitSignOut = explicite;
            Games.signOut(mGoogleApiClient);
            mGoogleApiClient.disconnect();
        }
        triedSignIn = false;
    }

    public boolean isSignInFlow() {
        return mInSignInFlow;
    }

    public boolean isExplicitSignOut() {
        return mExplicitSignOut;
    }

    public void setInSignInFlow(boolean val) {
        mInSignInFlow = val;
    }

    @Override
    public boolean isConnected() {
        return triedSignIn && mGoogleApiClient != null && mGoogleApiClient.isConnected();
    }

    @Override
    public void rateGame() {
        if (isConnected()) {
            Uri uri = Uri.parse("market://details?id=" + launcher.getContext().getPackageName());
            Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
            // To count with Play market backstack, After pressing back button, 
            // to taken back to our application, we need to add following flags to intent. 
            goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
            try {
                launcher.startActivity(goToMarket);
            } catch (ActivityNotFoundException e) {
                launcher.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id="
                        + launcher.getContext().getPackageName())));
            }
        }
    }

    public void unlockAchievement(AchievementsList achievement) {
        if (isConnected() == true) {
            Games.Achievements.reveal(mGoogleApiClient, achievement.getId());
            Games.Achievements.unlock(mGoogleApiClient, achievement.getId());
        }
    }

    @Override
    public void incrementAchievement(AchievementsList achievement, int value) {
        if (isConnected() == true) {
            Games.Achievements.increment(mGoogleApiClient, achievement.getId(), value);
        }
    }

    @Override
    public void showAchievements() {
        if (isConnected() == true) {
            launcher.startActivityForResult(Games.Achievements.getAchievementsIntent(mGoogleApiClient), requestCode);
        }
    }

    @Override
    public PlatformType getPlatformType() {
        if (launcher instanceof AndroidLauncher)
            return ((AndroidLauncher) launcher).getPlatformType();
        else
            return ((EditorLauncher) launcher).getPlatformType();
    }

    @Override
    public boolean networkAvailable() {
        ConnectivityManager cm =
                (ConnectivityManager)launcher.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                              activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }

    @Override
    public void setCallback(PlatformCallback callback) {
        this.callback = callback;
    }

    @Override
    public DriveIntegration getDriveIntegration() {
        return driveIntegration;
    }

    @Override
    public StorageIntegration getStorageIntegration() {
        return storageIntegration;
    }
}
