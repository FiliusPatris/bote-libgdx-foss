/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.android;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.Semaphore;

import android.os.Handler;
import android.widget.Toast;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import com.blotunga.bote.DriveIntegration;
import com.blotunga.bote.constants.GameConstants;
import com.blotunga.bote.ui.optionsview.SaveInfo;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi.DriveContentsResult;
import com.google.android.gms.drive.DriveApi.MetadataBufferResult;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.DriveFolder.DriveFileResult;
import com.google.android.gms.drive.DriveFolder.DriveFolderResult;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.Metadata;
import com.google.android.gms.drive.MetadataBuffer;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.drive.query.Filter;
import com.google.android.gms.drive.query.Filters;
import com.google.android.gms.drive.query.Query;
import com.google.android.gms.drive.query.SearchableField;

public class GoogleCloudIntegration implements DriveIntegration {
    AndroidPlatformApiIntegrator apiIntegrator;
    private Array<String> filenames;
    private DriveId directoryId;
    private String fileName;
    private Semaphore waitSema;
    private DriveIntegrationCallback callback;
    final String folderName = "BirthOfTheEmpires";
    private boolean toastShown;

    public GoogleCloudIntegration(AndroidPlatformApiIntegrator apiIntegrator) {
        this.apiIntegrator = apiIntegrator;
        waitSema = new Semaphore(1);
        toastShown = false;
    }

    private GoogleApiClient getGoogleApiClient() {
        return apiIntegrator.mGoogleApiClient;
    }

    @Override
    public void writeSavesToDrive(Array<SaveInfo> saveInfos) {
        toastShown = false;
        if (callback != null)
            callback.signalBusy(true);
        filenames = new Array<String>();
        for (SaveInfo si : saveInfos) {
            String imgPath = GameConstants.getSaveLocation() + si.fileName + ".png";
            filenames.add(si.fileName + ".sav");
            FileHandle fh = new FileHandle(imgPath);
            if (fh.exists())
                filenames.add(si.fileName + ".png");
        }
        filenames.add("settings.ini");

        Drive.DriveApi.requestSync(getGoogleApiClient());
        new Thread() {
            @Override
            public void run() {
                DriveId driveId = null;
                while (driveId == null) {
                    driveId = exists(folderName, null, Drive.DriveApi.getRootFolder(getGoogleApiClient()));
                    if (driveId == null) {
                        final MetadataChangeSet changeSet = new MetadataChangeSet.Builder().setTitle(folderName).build();
                        DriveFolderResult result = Drive.DriveApi.getRootFolder(getGoogleApiClient())
                                .createFolder(getGoogleApiClient(), changeSet).await();
                        driveId = result.getDriveFolder().getDriveId();
                        System.err.println("Created folder: " + driveId);
                        try {
                            sleep(100);
                        } catch (InterruptedException e) {
                            if (!toastShown) {
                                showToastOnMainThread(apiIntegrator.launcher.getString(R.string.cloud_sync_error), Toast.LENGTH_LONG);
                                toastShown = true;
                            }
                            e.printStackTrace();
                        }
                    }
                }
                writeToDriveId(driveId);
            }
        }.start();
    }

    private void writeToDriveId(DriveId driveId) {
        this.directoryId = driveId;
        for (String fn : filenames) {
            try {
                waitSema.acquire();
            } catch (InterruptedException e) {
            }
            GoogleCloudIntegration.this.fileName = fn;
            Drive.DriveApi.newDriveContents(getGoogleApiClient()).setResultCallback(driveContentsCallback);
        }
        if (callback != null)
            callback.signalBusy(false);
    }

    protected void showToastOnMainThread(final String text, final int length) {
        Handler mainHandler = new Handler(apiIntegrator.launcher.getMainLooper());

        Runnable toastRunnable = new Runnable() {
            @Override
            public void run() {
                Toast.makeText(apiIntegrator.launcher.getContext(), text, length).show();
            } // This is your code
        };
        mainHandler.post(toastRunnable);
    }

    final private ResultCallback<DriveContentsResult> driveContentsCallback = new ResultCallback<DriveContentsResult>() {
        @Override
        public void onResult(DriveContentsResult result) {
            if (!result.getStatus().isSuccess()) {
                if (!toastShown) {
                    Toast.makeText(apiIntegrator.launcher.getContext(),
                            apiIntegrator.launcher.getString(R.string.cloud_sync_error), Toast.LENGTH_LONG).show();
                    toastShown = true;
                }
                System.err.println("Error while trying to create new file contents");
                waitSema.release();
                return;
            }
            final DriveContents driveContents = result.getDriveContents();

            new Thread() {
                @Override
                public void run() {

                    // write content to DriveContents
                    DriveId fileId = exists(fileName, null, directoryId.asDriveFolder());
                    OutputStream outputStream = driveContents.getOutputStream();
                    DriveContentsResult driveContentsResult = null;
                    if (fileId != null) {
                        driveContentsResult = fileId.asDriveFile().open(getGoogleApiClient(), DriveFile.MODE_WRITE_ONLY, null)
                                .await();
                        if (driveContentsResult == null || driveContentsResult.getDriveContents() == null) {
                            if (!toastShown) {
                                showToastOnMainThread(apiIntegrator.launcher.getString(R.string.cloud_sync_error), Toast.LENGTH_LONG);
                                toastShown = true;
                            }
                            waitSema.release();
                            return;
                        }
                        outputStream = driveContentsResult.getDriveContents().getOutputStream();
                    }
                    try {
                        String path = fileName.endsWith(".ini") ? Gdx.files.getLocalStoragePath() : GameConstants
                                .getSaveLocation();
                        FileInputStream fi = new FileInputStream(path + fileName);
                        byte[] buffer = new byte[1024];
                        int len = fi.read(buffer);
                        while (len != -1) {
                            outputStream.write(buffer, 0, len);
                            len = fi.read(buffer);
                        }
                        outputStream.close();
                        fi.close();
                    } catch (IOException e) {
                        if (!toastShown) {
                            showToastOnMainThread(apiIntegrator.launcher.getString(R.string.cloud_sync_error), Toast.LENGTH_LONG);
                            toastShown = true;
                        }
                        System.err.println(e.getMessage());
                    }

                    MetadataChangeSet changeSet = new MetadataChangeSet.Builder().setTitle(fileName).setStarred(false).build();

                    if (fileId == null) {
                        // create a file on root folder
                        directoryId.asDriveFolder().createFile(getGoogleApiClient(), changeSet, driveContents)
                                .setResultCallback(fileCallback);
                    } else {
                        changeSet = new MetadataChangeSet.Builder().setStarred(false).setLastViewedByMeDate(new Date()).build();

                        if (driveContentsResult != null)
                            driveContentsResult.getDriveContents().commit(getGoogleApiClient(), changeSet).await();
                        waitSema.release();
                    }
                }
            }.start();
        }
    };

    final private ResultCallback<DriveFileResult> fileCallback = new ResultCallback<DriveFileResult>() {
        @Override
        public void onResult(DriveFileResult result) {
            waitSema.release();
            if (!result.getStatus().isSuccess()) {
                if (!toastShown) {
                    Toast.makeText(apiIntegrator.launcher.getContext(),
                            apiIntegrator.launcher.getString(R.string.cloud_sync_error), Toast.LENGTH_LONG).show();
                    toastShown = true;
                }
                System.err.println("Error while trying to create the file");
                return;
            }
        }
    };

    final ResultCallback<DriveFolderResult> folderCreateCallback = new ResultCallback<DriveFolderResult>() {
        @Override
        public void onResult(DriveFolderResult result) {
            if (!result.getStatus().isSuccess()) {
                if (!toastShown) {
                    Toast.makeText(apiIntegrator.launcher.getContext(),
                            apiIntegrator.launcher.getString(R.string.cloud_sync_error), Toast.LENGTH_LONG).show();
                    toastShown = true;
                }
                System.err.println("Error while trying to create the folder");
                return;
            }
            System.err.println("Created a folder: " + result.getDriveFolder().getDriveId());
            writeToDriveId(result.getDriveFolder().getDriveId());
        }
    };

    private DriveId exists(String title, String mime, DriveFolder fldr) {
        ArrayList<Filter> fltrs = new ArrayList<Filter>();
        fltrs.add(Filters.eq(SearchableField.TRASHED, false));
        if (title != null)
            fltrs.add(Filters.eq(SearchableField.TITLE, title));
        if (mime != null)
            fltrs.add(Filters.eq(SearchableField.MIME_TYPE, mime));
        Query qry = new Query.Builder().addFilter(Filters.and(fltrs)).build();
        MetadataBufferResult rslt = (fldr == null) ? Drive.DriveApi.query(getGoogleApiClient(), qry).await() : fldr
                .queryChildren(getGoogleApiClient(), qry).await();
        if (rslt.getStatus().isSuccess()) {
            MetadataBuffer mdb = null;
            try {
                mdb = rslt.getMetadataBuffer();
                if (mdb == null)
                    return null;
                for (Metadata md : mdb) {
                    if ((md == null) || md.isTrashed())
                        continue;
                    return md.getDriveId();
                }
            } finally {
                if (mdb != null)
                    mdb.close();
            }
        }
        return null;
    }

    @Override
    public void readSavesFromDrive() {
        System.err.println(("readSavesFromDrive()"));
        if (callback != null)
            callback.signalBusy(true);
        Drive.DriveApi.requestSync(getGoogleApiClient());
        DriveId driveId = exists(folderName, null, Drive.DriveApi.getRootFolder(getGoogleApiClient()));
        if (driveId != null) {
            DriveFolder folder = driveId.asDriveFolder();
            folder.listChildren(getGoogleApiClient()).setResultCallback(listChildrenResult);
            /*            ArrayList<Filter> fltrs = new ArrayList<Filter>();
                        fltrs.add(Filters.eq(SearchableField.TRASHED, false));
                        fltrs.add(Filters.eq(SearchableField.MIME_TYPE, "application/octet-stream"));
                        fltrs.add(Filters.eq(SearchableField.MIME_TYPE, "image/png"));
                        Query qry = new Query.Builder().addFilter(Filters.and(fltrs)).build();

                        folder.queryChildren(getGoogleApiClient(), qry).setResultCallback(listChildrenResult);*/
        } else if (callback != null)
            callback.signalBusy(false);
    }

    final private ResultCallback<MetadataBufferResult> listChildrenResult = new ResultCallback<MetadataBufferResult>() {
        @Override
        public void onResult(MetadataBufferResult result) {
            final MetadataBuffer files = result.getMetadataBuffer();
            System.err.println("METADATA: " + files.getCount());
            new Thread() {
                @Override
                public void run() {
                    try {
                        waitSema.acquire();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    for (Metadata file : files) {
                        DriveContentsResult driveContentsResult = file.getDriveId().asDriveFile()
                                .open(getGoogleApiClient(), DriveFile.MODE_READ_ONLY, null).await();
                        System.err.println("Reading file: " + file.getTitle());
                        if (driveContentsResult == null || driveContentsResult.getDriveContents() == null) //something went wrong
                            continue;
                        InputStream fi = driveContentsResult.getDriveContents().getInputStream();
                        String path = file.getFileExtension().equals(".ini") ? Gdx.files.getLocalStoragePath() : GameConstants
                                .getSaveLocation();
                        FileOutputStream fo;
                        try {
                            fo = new FileOutputStream(path + file.getTitle());
                            byte[] buffer = new byte[1024];
                            int len = fi.read(buffer);
                            while (len != -1) {
                                fo.write(buffer, 0, len);
                                len = fi.read(buffer);
                            }
                            fo.close();
                            fi.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    waitSema.release();
                    if (callback != null)
                        callback.signalBusy(false);
                }
            }.start();
        }
    };

    @Override
    public void setCallback(DriveIntegrationCallback callback) {
        this.callback = callback;
    }
}
