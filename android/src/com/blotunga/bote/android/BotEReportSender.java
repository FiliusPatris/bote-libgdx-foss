/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.android;

import org.acra.collector.CrashReportData;
import org.acra.config.ACRAConfiguration;
import org.acra.sender.ReportSender;
import org.acra.sender.ReportSenderException;
import org.acra.sender.ReportSenderFactory;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class BotEReportSender implements ReportSender, ReportSenderFactory {
    @Override
    public void send(Context context, CrashReportData errorContent) throws ReportSenderException {
        Intent me = context.getPackageManager().getLaunchIntentForPackage(context.getPackageName());

        PendingIntent pi = PendingIntent.getActivity(context, 0, me, Intent.FLAG_ACTIVITY_NEW_TASK);

        AlarmManager mgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 5000, pi);
        Log.e("CrashReportSender", "-->> set to Restart Application after 5 sec...");
    }

    @Override
    public ReportSender create(Context arg0, ACRAConfiguration arg1) {
        return new BotEReportSender();
    }
}
