/*
 * Copyright (C) 2014-2017 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.android;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.WindowManager;

import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.blotunga.bote.AndroidIntegration;
import com.blotunga.bote.BotEResourceEditor;
import com.blotunga.bote.PlatformApiIntegration.PlatformType;
import com.blotunga.bote.android.util.AndroidApkExpansionFileSetter;
import com.blotunga.bote.android.util.ZipFileHandleResolver;
import com.blotunga.bote.utils.ApkExpansionSetter;

public class EditorLauncher extends AndroidApplication implements AndroidIntegration {
    private AndroidApplicationConfiguration config;
    private FileHandleResolver resolver;
    private ApkExpansionSetter setter;
    private BotEResourceEditor bote;
    private AndroidPlatformApiIntegrator integrator;

    private ActivityManager.MemoryInfo mInfo;
    
    @SuppressLint("InlinedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int sdkInt = Build.VERSION.SDK_INT;
        if (sdkInt >= Build.VERSION_CODES.GINGERBREAD /* 9 */
                && sdkInt < Build.VERSION_CODES.JELLY_BEAN_MR2 /* 18 */) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        } else if (sdkInt < Build.VERSION_CODES.GINGERBREAD /* 9 */) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else if (sdkInt >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER_LANDSCAPE);
        }

        ActivityManager actvityManager = (ActivityManager) getSystemService(Activity.ACTIVITY_SERVICE);
        mInfo = new ActivityManager.MemoryInfo();
        actvityManager.getMemoryInfo(mInfo);

        config = new AndroidApplicationConfiguration();
        config.useImmersiveMode = true;
        config.numSamples = 2;
        resolver = null;
        setter = null;

        if (getPlatformType() == PlatformType.PlatformGoogle) {
            resolver = new ZipFileHandleResolver();
            setter = new AndroidApkExpansionFileSetter();
        }
        integrator = new AndroidPlatformApiIntegrator(null, this);

        bote = new BotEResourceEditor(this, integrator);
        initialize(bote, config);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    protected void onStart() {
        super.onStart();
        //integrator.signIn();
    }

    @Override
    protected void onStop() {
        super.onStop();
        //integrator.signOut();
    }

    public PlatformType getPlatformType() {
        String[] fileNames = { "data/googlemarker", "data/amazonmarker" };
        for (String fileName : fileNames) {
            try {
                this.getAssets().open(fileName).close(); // Check if file exists.
                if (fileName.contains("google"))
                    return PlatformType.PlatformGoogle;
                else if (fileName.contains("amazon"))
                    return PlatformType.PlatformAmazon;
            } catch (Exception ex) {
            }
        }
        return PlatformType.PlatformStandalone;
    }

    @Override
    public ApkExpansionSetter getApkExpansionSetter() {
        return setter;
    }

    @Override
    public FileHandleResolver getFileHandleResolver() {
        return resolver;
    }

    @Override
    public long getAvailableMemInfo() {
        return mInfo.availMem;
    }
}
