/*
 * Copyright (C) 2014-2016 Blotunga
 *
 * This file is part of the Birth of the Empires project.
 *
 * Licensed under Mozilla Public License, v. 2.0. (the "License").
 * You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *
 * http://mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blotunga.bote.desktop;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.blotunga.bote.BotE;
import com.blotunga.bote.BotEResourceEditor;
import com.blotunga.bote.PlatformApiIntegration;
import com.blotunga.bote.constants.GameConstants;

public class DesktopLauncher {
    enum AppType {
        EDITOR,
        GAME;
    }

    public static void main(String[] arg) {
        Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
        config.setTitle("Birth of the Empires");
        //config.width = 1440;//1440;//1280;//480;
        //config.height = 810;//900;//720;//320;
        //config.forceExit = false;
        int width = 1440, height = 810;
        boolean windowed = false;
        AppType type = AppType.GAME;
        for (int i = 0; i < arg.length; i++) {
            if (arg[i].startsWith("--res=")) {
                String[] resStr = arg[i].substring(6, arg[i].length()).split("x");
                width = Integer.parseInt(resStr[0]);
                height = Integer.parseInt(resStr[1]);
                windowed = true;
            }
            if (arg[i].startsWith("--lang="))
                GameConstants.setLocale(arg[i].substring(7, arg[i].length()));
            if (arg[i].equals("--editor"))
                type = AppType.EDITOR;
        }
        if (windowed)
            config.setWindowedMode(width, height);
        else
            config.setFullscreenMode(Lwjgl3ApplicationConfiguration.getDisplayMode());
        config.setBackBufferConfig(8, 8, 8, 8, 24, 24, 6);
        config.setTitle("Birth of the Empires");
        config.setWindowIcon("graphics/ui/iconl.png", "graphics/ui/iconm.png", "graphics/ui/icons.png");
        ApplicationListener mainClass;
        PlatformApiIntegration integrator = new DesktopPlatformApiIntegration();

        switch (type) {
            case EDITOR:
                mainClass = new BotEResourceEditor(arg, integrator);
                break;
            default:
                mainClass = new BotE(arg, integrator);
                break;
        }

        new Lwjgl3Application(mainClass, config);
    }
}